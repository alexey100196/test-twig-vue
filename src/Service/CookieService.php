<?php

namespace App\Service;

use App\Entity\CookieValidityType;
use App\Entity\Cookie;
use App\Repository\CookieRepository;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use App\Exception\CookieValueIsNullException;
use Symfony\Component\HttpFoundation\JsonResponse;

class CookieService{

    private $cookieRepository;
    public function __construct(CookieRepository $cookieRepository)
    {
        $this->cookieRepository = $cookieRepository;
    }

    public function isDateValidity(Cookie $cookie ): bool
    {
        if ($cookie->getValidityType() == CookieValidityType::DATE) {
           return true;
        }
        return false;
    }

    public function isTimeSpanValidity(Cookie $cookie ): bool
    {
        if (in_array($cookie->getValidityType(), CookieValidityType::TIMESPAN)) {
            return true;
        }
        return false;
    }

    public function isUsageValidity(Cookie $cookie ): bool
    {
        if (in_array($cookie->getValidityType(), CookieValidityType::USAGE)) {
           return true;
        }
        return false;
    }

    public function fetch(UuidInterface $uuid): Cookie
    {
        return $this->cookieRepository->findOneByValue($uuid);
    }

    public function isValid(Cookie $cookie): bool
    {
        if($this->isDateValidity($cookie)){
            $validity = $cookie->getValidity();
            $datetime = $this->parseDateValidity($validity);
            return new DateTime() < $datetime;
        }elseif($this->isTimeSpanValidity($cookie)){

        }elseif ($this->isUsageValidity($cookie)){
            $validity = $cookie->getValidity();
            $validityAsNumber = (int)$validity;
            return $validityAsNumber <= $cookie->getConsumed() + 1;
        }
    }
    private function parseDateValidity($validity): \DateTime
    {
        $datetime = DateTime::createFromFormat(Cookie::DATE_VALIDITY_FORMAT, $validity);
        return $datetime;
    }

    public function setCookie(Cookie $cookie, JsonResponse $jsonResponse ): void
    {
        //check if is valid???
        if($cookie->getValue() === null){
            $cookie->setValue(Uuid::uuid4());
        }
        $this->cookieRepository->save($cookie);
        $jsonResponse->headers->setCookie($cookie->getKey(), $cookie->getValue());
    }

    public function dropCookie(Cookie $cookie, JsonResponse $jsonResponse ): void
    {
        if($cookie->getValue() !== null){
            $cookieToDrop = $this->fetch($cookie->getValue());
            $this->cookieRepository->getEntityManager()->remove($cookieToDrop);
            $jsonResponse->headers->removeCookie($cookie->getKey());
        }
    }

    public function consume(Cookie $cookie)
    {
        $cookie->setConsumed($cookie->getConsumed() + 1);
        $this->cookieRepository->save($cookie);
    }
 }