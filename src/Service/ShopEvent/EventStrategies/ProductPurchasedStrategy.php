<?php


namespace App\Service\ShopEvent\EventStrategies;

use App\Entity\Currency;
use App\Entity\Product;
use App\Entity\ProductPurchase;
use App\Entity\Shop;
use App\Entity\ShopConversionStatus;
use App\Entity\ShopPurchase;
use App\Event\Conversion\ShopPurchaseCreated;
use App\Manager\ProductManager;
use App\Repository\CurrencyRepository;
use App\Service\Exchange\CurrencyExchangeInterface;
use App\Service\Exchange\ExchangeService;
use App\Service\ShopEvent\Interfaces\EventStrategyInterface;
use App\Service\SystemCommission\ApplierInterface;
use App\Service\SystemCommission\ShopCommission\Applier;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ProductPurchasedStrategy extends AbstractStrategy implements EventStrategyInterface
{
    /**
     * @var Applier
     */
    private $feeApplier;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var CurrencyExchangeInterface
     */
    private $currencyExchange;

    /**
     * @param ApplierInterface $applier
     */
    public function setFeeApplier(ApplierInterface $applier)
    {
        $this->feeApplier = $applier;
    }

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param CurrencyExchangeInterface $currencyExchange
     */
    public function setCurrencyExchange(CurrencyExchangeInterface $currencyExchange)
    {
        $this->currencyExchange = $currencyExchange;
    }

    public function process()
    {
        $em = $this->doctrine->getEntityManager();

        $productRepository = $this->doctrine->getRepository(Product::class);

        /** @var CurrencyRepository $currencyRepository */
        $currencyRepository = $this->doctrine->getRepository(Currency::class);

        /** @var Shop $shop */
        $shop = $this->eventEntry->getShop();

        $shopPurchase = new ShopPurchase();
        $shopPurchase->setShop($shop);

        $currencyName = $this->eventEntry->getPayload('currency')?: Currency::EUR;

        $currency = $currencyRepository->findOneByName($currencyName);

        $shopPurchase->setOriginalCurrency($currency);
        $shopPurchase->setOriginalPrice($price = (float)$this->eventEntry->getPayload('price'));
        $shopPurchase->setPrice($this->calculatePriceDependingOnCurrency($price, $currency->getName()));
        $shopPurchase->setAmount((int)$this->eventEntry->getPayload('amount'));

        $shopPurchase->setReferrerLink($this->eventEntry->getReferrerLink());
        $shopPurchase->setStatus($em->getReference(ShopConversionStatus::class, ShopConversionStatus::UNVERIFIED));
        $shopPurchase->setSourceLink($shop->getWebsite());

        // If product given then below data will be overwritten by product offer.
        $shopPurchase->applyOffer($shop->getCpsOffer());

        // Only E-commerce can have products.
        if ($shop->isEcommerce()) {
            if ($number = (string)$this->eventEntry->getPayload('product')) {
                /** @var Product $product */
                if ($product = $productRepository->findOneBy(['shop' => $shop, 'number' => $number])) {
                    $shopPurchase->setProduct($product);
                    $shopPurchase->applyOffer($product->getCpsOffer());
                    $shopPurchase->setSourceLink($product->getFullUrl());
                }
            }
        }

        // Apply system commission. It is common for each shop.
        $taxedAmount = $this->feeApplier->apply($shop, $shopPurchase->getTotal());
        $shopPurchase->setSystemCommission($taxedAmount->getAmount());

        $shopPurchase->setEventEntry($this->eventEntry);

        $em->persist($shopPurchase);
        $em->flush();

        $this->eventDispatcher->dispatch(ShopPurchaseCreated::NAME, new ShopPurchaseCreated($shopPurchase));
    }

    private function calculatePriceDependingOnCurrency(float $fromAmount, string $fromCurrency)
    {
        if ($fromCurrency === Currency::EUR) {
            return $fromAmount;
        }

        return $this->currencyExchange->exchange($fromAmount, $fromCurrency,Currency::EUR);
    }
}