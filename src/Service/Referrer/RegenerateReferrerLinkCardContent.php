<?php

namespace App\Service\Referrer;

use App\Entity\ReferrerLink;
use App\Entity\ReferrerLinkCard;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment as Twig;

class RegenerateReferrerLinkCardContent
{
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \Twig_Environment
     */
    private $templating;

    public function __construct(
        UrlGeneratorInterface $urlGenerator,
        EntityManagerInterface $entityManager,
        Twig $templating
    )
    {
        $this->urlGenerator = $urlGenerator;
        $this->entityManager = $entityManager;
        $this->templating = $templating;
    }

    public function __invoke(ReferrerLinkCard $referrerLinkCard): void
    {
        $referrerLinkCard->setContent(
            $this->templating->render('referrer-link/template.html.twig', [
                'card' => $referrerLinkCard,
                'reflinkUrl' => $this->generateReflinkUrl($referrerLinkCard->getReferrerLink())
            ])
        );
    }

    private function generateReflinkUrl(ReferrerLink $referrerLink): string
    {
        return $this->urlGenerator->generate('home_reflink_card', [
            'shopName' => $referrerLink->getShopUrlName(),
            'nickname' => $referrerLink->getNickname(),
            'reflinkSlug' => $referrerLink->getSlug(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);
    }
}