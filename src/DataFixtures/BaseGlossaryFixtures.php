<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

abstract class BaseGlossaryFixtures extends Fixture
{
    public function getOrder()
    {
        return 1;
    }

    abstract protected function getData(): array;

    abstract protected function getEntity();

    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->getData() as $id => $name) {
            $glossaryEntity = $this->getEntity();
            $glossaryEntity->setId($id);
            $glossaryEntity->setName($name);
            $manager->persist($glossaryEntity);

            $this->addReference(get_class($glossaryEntity) . '-' . $name, $glossaryEntity);
        }

        $manager->flush();
    }
}
