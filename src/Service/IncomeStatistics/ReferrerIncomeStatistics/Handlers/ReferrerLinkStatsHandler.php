<?php


namespace App\Service\IncomeStatistics\ReferrerIncomeStatistics\Handlers;

use App\Entity\ShopConversion;
use App\Service\IncomeStatistics\CommandInterface;
use App\Service\IncomeStatistics\ReferrerIncomeStatistics\Entities\ReferrerLinkStatsNew;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ReferrerLinkStatsHandler extends AbstractHandler
{
    public function __construct(RegistryInterface $doctrine)
    {
        parent::__construct($doctrine);
    }

    /**
     * @param CommandInterface $command
     * @return ArrayCollection
     */
    public function handle(CommandInterface $command)
    {
        $rows = $this->doctrine
            ->getRepository(ShopConversion::class)
            ->getReferrerLinkStats(
                $command->getReferrerLink(),
                $command->getStartDate(),
                $command->getEndDate()
            );

        return new ArrayCollection(array_map([$this, 'prepareStatsItem'], $rows));
    }

    /**
     * @param $item
     * @return ReferrerLinkStatsNew
     */
    private function prepareStatsItem($item): ReferrerLinkStatsNew
    {
        $productStats = new ReferrerLinkStatsNew();
        $productStats->setOfferType($item['offerType']);
        $productStats->setCommissionType($item['commissionType']);
        $productStats->setCommission($item['commission']);
        $productStats->setTransactionsCount((int)$item['transactionsCount']);
        $productStats->setPurchasesAmount((int)$item['purchasesAmount']);
        $productStats->setPurchasesPrice((float)$item['purchasesPrice']);
        $productStats->setReferrerProfit((float)$item['referrerProfit']);
        $productStats->setSourceLink($item['sourceLink']);
        $productStats->setHasExchanges($item['originalTotal'] != $item['purchasesTotal']);

        return $productStats;
    }
}
