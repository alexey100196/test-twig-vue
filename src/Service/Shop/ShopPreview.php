<?php


namespace App\Service\Shop;


use App\Entity\Shop;
use App\Helper\ShopCompletion\ShopCompletionResolver;
use App\Helper\WidgetCompletion\WidgetCompletionResolver;

class ShopPreview
{
    /**
     * @param Shop $shop
     * @return bool
     * @throws \Exception
     */
    public function canPreviewShop(Shop $shop): bool
    {
        $shopCompletion = new ShopCompletionResolver($shop);
        $shopCompletion->resolve();

        foreach (['BasicData', 'MainOffer', 'OptionalOffer'] as $step) {
            if ($shopCompletion->getResult($step)->getScore() < 100) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param Shop $shop
     * @return bool
     * @throws \Exception
     */
    public function canPreviewReferral(Shop $shop): bool
    {
        $shopCompletion = new WidgetCompletionResolver($shop->getWidgetSetting());
        $shopCompletion->resolve();

        foreach (['LayoutData', 'CouponsData', 'RewardsData', 'EmailData', 'IconData'] as $step) {
            if ($shopCompletion->getResult($step)->getScore() < 100) {
                return false;
            }
        }

        return true;
    }
}