<?php

namespace App\Helper\WidgetCompletion\Resolvers;

use App\Helper\WidgetCompletion\Resolvers\AbstractResolver;

class RewardsData extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        $campaigns = $this->widgetShopSetting->getShop()->getCampaigns();
        foreach($campaigns as $campaign){
           if  ($campaign->getId() > 0 ) {
               $this->addScore(100);
               return;
           }
        }
    }
}