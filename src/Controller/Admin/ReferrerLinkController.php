<?php

namespace App\Controller\Admin;

use App\Entity\ReferrerLink;
use App\Entity\ShopStatus;
use App\Form\Admin\Shop\EditShopType;
use App\Repository\ReferrerLinkRepository;
use App\Util\Consts;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReferrerLinkController extends AbstractController
{
    const PER_PAGE = 25;

    /**
     * @var ReferrerLinkRepository
     */
    private $referrerLinkRepository;

    /**
     * @param ReferrerLinkRepository $referrerLinkRepository
     */
    public function __construct(ReferrerLinkRepository $referrerLinkRepository)
    {
        $this->referrerLinkRepository = $referrerLinkRepository;
    }

    /**
     * @Route("/admin/referrer-links", name="admin_referrer_link_index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $links = $paginator->paginate(
            $this->referrerLinkRepository->createSoftDeleteQueryBuilder()->getQuery(),
            $request->query->getInt('page', 1),
            self::PER_PAGE
        );

        return $this->render('admin/link/index.html.twig', ['links' => $links]);
    }

    /**
     * @Route("/admin/referrer-links/{referrerLink}", name="admin_referrer_link_delete", methods={"DELETE"})
     * @param ReferrerLink $referrerLink
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function delete(ReferrerLink $referrerLink)
    {
        $em = $this->getDoctrine()->getManager();
        $referrerLink->delete();
        $em->flush();

        $this->addFlash(Consts::SUCCESS, 'Link removed');

        return $this->redirectToRoute('admin_referrer_link_index');
    }
}
