<?php


namespace App\Service\Subscription;


use App\Entity\EventEntry;
use App\Entity\EventEntryType;
use App\Entity\Shop;
use App\Entity\Subscription\Plan;
use App\Entity\Subscription\PlanSubscription;
use App\Entity\User;
use App\Repository\EventEntryTypeRepository;
use App\Repository\Subscription\PlanSubscriptionRepository;
use App\Service\ShopEvent\EventService;
use App\Service\Subscription\PlanTypeStrategy\PlanTypeStrategy;
use App\Service\Subscription\SubscriptionValidation\SubscriptionValidator;
use App\Service\Subscription\ValueObject\Period;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;

class SubscriptionService
{
    private $em;

    private $planSubscriptionRepository;

    private $planTypeStrategy;

    private $subscriptionValidator;

    private $eventService;

    private $eventEntryTypeRepository;

    public function __construct(
        EntityManagerInterface $em,
        PlanSubscriptionRepository $planSubscriptionRepository,
        PlanTypeStrategy $planTypeStrategy,
        SubscriptionValidator $subscriptionValidator,
        EventService $eventService,
        EventEntryTypeRepository $eventEntryTypeRepository
    )
    {
        $this->em = $em;
        $this->planSubscriptionRepository = $planSubscriptionRepository;
        $this->planTypeStrategy = $planTypeStrategy;
        $this->subscriptionValidator = $subscriptionValidator;
        $this->eventService = $eventService;
        $this->eventEntryTypeRepository = $eventEntryTypeRepository;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllExpiredSubscriptions()
    {
        return $this->planSubscriptionRepository->findActiveSubscriptionsWithEndedPeriod();
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canUserUseTrialPeriod(User $user): bool
    {
        $userCurrentPlan = $user->getCurrentPlan();

        // If user is already on trial then ok.
        if ($userCurrentPlan && $userCurrentPlan->isOnTrial()) {
            return true;
        }

        // If user never had plan then ok.
        return null === $this->planSubscriptionRepository->findOneBy(['user' => $user]);
    }

    /**
     * @param User $user
     * @param Plan $plan
     * @throws \Exception
     */
    public function subscribe(User $user, Plan $plan)
    {
        $subscription = new PlanSubscription();
        $subscription->setPlan($plan);
        $subscription->setUser($user);

        // Apply default trial plan to subscription.
        $trialPeriod = $this->planTrialToPeriod($plan);

        $trialPeriod = $this->adjustTrialPeriodToUser($trialPeriod, $user);
        $planPeriod = $this->planToPeriod($plan, $trialPeriod->getEnd());

        $subscription->setTrialEndsAt($trialPeriod->getEnd());
        $subscription = $this->applyPeriodToSubscription($subscription, $planPeriod);

        $this->subscriptionValidator->validate($user, $plan, SubscriptionValidator::SUBSCRIBE);

        $subscription->setEndsAt($subscription->getStartsAt());

        $this->removeAllUserSubscriptions($user);

        $this->planTypeStrategy->process($subscription);

        $this->em->persist($subscription);
        $this->em->flush();
    }


    public function renewPlan(PlanSubscription $subscription)
    {
        if ($subscription->isCanceled()) {
            throw new LogicException('Can\'t renew canceled ended subscription.');
        }

        if ($subscription->getTrialEndsAt() == $subscription->getEndsAt()) {
            $planPeriod = $this->planToPeriod($subscription->getPlan());
        } else if ($subscription->getEndsAt() > new \DateTime()) {
            $planPeriod = $this->planToPeriod($subscription->getPlan(), $subscription->getEndsAt());
        } else {
            $planPeriod = $this->planToPeriod($subscription->getPlan());
        }

        $subscription = $this->applyPeriodToSubscription($subscription, $planPeriod);
        $subscription->setCanceledAt(null);

        $this->em->flush();

        $this->registerConversionForSubscription($subscription);
    }

    /**
     * @param PlanSubscription $subscription
     */
    public function cancelSubscription(PlanSubscription $subscription)
    {
        $subscription->cancel(true);

        $this->em->flush();
    }

    /**
     * @param PlanSubscription $subscription
     * @param Plan $plan
     */
    public function changePlan(PlanSubscription $subscription, Plan $plan)
    {
        if (
            $subscription->getPlan()->getPlanInterval() !== $plan->getPlanInterval() ||
            $subscription->getPlan()->getPlanPeriod() !== $plan->getPlanPeriod()
        ) {
            $subscription = $this->applyPeriodToSubscription($subscription, $this->planToPeriod($plan));
        }

        $subscription->setPlan($plan);

        $this->em->flush();
    }

    /**
     * @param PlanSubscription $subscription
     * @param Period $period
     * @return PlanSubscription
     */
    private function applyPeriodToSubscription(PlanSubscription $subscription, Period $period)
    {
        $subscription->setStartsAt($period->getStart());
        $subscription->setEndsAt($period->getEnd());

        return $subscription;
    }

    /**
     * @param User $user
     */
    private function removeAllUserSubscriptions(User $user)
    {
//        $userCurrentPlan = $user->getCurrentPlan();
//
//        if ($userCurrentPlan && $userCurrentPlan->isOnTrial()) {
////            foreach ($user->getSubscriptions() as $subscription) {
////                $this->em->remove($subscription);
////            }
//        } else {
//        }
        $user->cancelAllSubscriptions(true);
    }

    /**
     * @param Plan $plan
     * @return Period
     */
    private function planTrialToPeriod(Plan $plan)
    {
        return new Period($plan->getTrialInterval(), $plan->getTrialPeriod(), new \DateTime());
    }

    /**
     * @param Plan $plan
     * @param \DateTime|null $trialEnd
     * @return Period
     */
    private function planToPeriod(Plan $plan, \DateTime $trialEnd = null)
    {
        return new Period($plan->getPlanInterval(), $plan->getPlanPeriod(), $trialEnd);
    }

    /**
     * @param PlanSubscription $subscription
     * @throws \Exception
     */
    private function registerConversionForSubscription(PlanSubscription $subscription)
    {
        $shop = $subscription->getUser()->getShop();

        if ($shop && $shop->getReferredBy() && $shop->getReferredBy()->getShop()->getCpsOffer()) {
            $eventEntry = $this->createConversionEventEntryObjectForShop($shop);
            $payload = ['price' => $subscription->getPlan()->getPrice(), 'amount' => 1];
            $eventEntry->setPayload($payload);
            $this->eventService->process($eventEntry);
        }
    }

    /**
     * @param Shop $shop
     * @return EventEntry
     * @throws \Exception
     */
    private function createConversionEventEntryObjectForShop(Shop $shop): EventEntry
    {
        $eventPurchaseType = $this->eventEntryTypeRepository->findOneBy(['name' => EventEntryType::TYPE_PURCHASE]);

        $eventEntry = new EventEntry();
        $eventEntry->setReferrer($shop->getReferredBy()->getReferrer());
        $eventEntry->setShop($shop->getReferredBy()->getShop());
        $eventEntry->setType($eventPurchaseType);
        $eventEntry->setCreatedAt(new \DateTime());
        $eventEntry->setReferrerLink($shop->getReferredBy());

        return $eventEntry;
    }

    private function adjustTrialPeriodToUser(Period $trialPeriod, User $user)
    {
        $now = Carbon::now();

        if (!$this->canUserUseTrialPeriod($user)) {
            $trialPeriod->setEnd($now);
        } else if ($user->getCurrentPlan() && $user->getCurrentPlan()->isOnTrial()) {
            $trialPeriod->setEnd(Carbon::instance($user->getCurrentPlan()->getTrialEndsAt()));
        }

        return $trialPeriod;
    }
}
