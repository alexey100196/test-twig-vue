<?php

namespace App\Form\Shop\Widget\CouponPackageType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class CsvUploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('csv', FileType::class, [
                'constraints' => [
                    new File([
                        'mimeTypes' => ['text/csv', 'text/plain']
                    ]),
                    new NotBlank()
                ],
                'help' => 'Use CSV file to upload coupons',
            ]);
    }
}
