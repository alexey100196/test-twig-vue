<?php

namespace App\Controller\Admin;

use App\Entity\ReferrerLink;
use App\Entity\ShopStatus;
use App\Form\Admin\ReferrerLink\CardContentType;
use App\Form\Admin\ReferrerLink\StoreCardContentType;
use App\Form\Admin\Shop\EditShopType;
use App\Repository\ReferrerLinkRepository;
use App\Traits\Controller\FormResponseArray;
use App\Util\Consts;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReferrerLinkContentController extends AbstractController
{
    use FormResponseArray;

    const PER_PAGE = 25;

    /**
     * @var ReferrerLinkRepository
     */
    private $referrerLinkRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param ReferrerLinkRepository $referrerLinkRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(ReferrerLinkRepository $referrerLinkRepository, EntityManagerInterface $em)
    {
        $this->referrerLinkRepository = $referrerLinkRepository;
        $this->em = $em;
    }

    /**
     * @Route("/admin/referrer-links/content", name="admin_referrer_link_content_index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $links = $paginator->paginate(
            $this->referrerLinkRepository->createQueryBuilder('l')->getQuery(),
            $request->query->getInt('page', 1),
            self::PER_PAGE
        );

        return $this->render('admin/link-content/index.html.twig', ['links' => $links]);
    }

    /**
     * @Route(
     *     "/admin/referrer-links/{referrerLink}/card-content",
     *     name="admin_referrer_link_card_content_update",
     *     methods={"POST"}
     * )
     * @param ReferrerLink $referrerLink
     * @param Request $request
     * @return JsonResponse
     */
    public function updateCardContent(ReferrerLink $referrerLink, Request $request)
    {
        $form = $this->createForm(CardContentType::class, $card = $referrerLink->getCard());
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $card->incrementAdminContentUpdates();
            $this->em->flush();
        } else {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(['message' => 'Card content changed'], Response::HTTP_OK);
    }

    /**
     * @Route(
     *     "/admin/referrer-links/{referrerLink}/store-content",
     *     name="admin_referrer_link_store_content_update",
     *     methods={"POST"}
     * )
     * @param ReferrerLink $referrerLink
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateMyStoreContent(ReferrerLink $referrerLink, Request $request)
    {
        $form = $this->createForm(StoreCardContentType::class, $storeCard = $referrerLink->getStoreCard());
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $storeCard->incrementAdminContentUpdates();
            $this->em->flush();
        } else {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(['message' => 'MyStore banner content saved'], Response::HTTP_OK);
    }
}
