<?php

namespace App\Form\Shop;

use App\Entity\Offer;
use App\Entity\Shop;
use App\Validator\Constraints\ShopOffer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;

class ShopOfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cpsOffer', CpsOfferType::class, ['data_class' => Offer::class, 'validate_min_amount' => $options['validate_min_amount_cps']])
            ->add('cplOffer', CplOfferType::class, ['data_class' => Offer::class, 'validate_min_amount' => $options['validate_min_amount_cpl']])
            ->add('rewardsMode', IntegerType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Please, choose one option.']),
                    new GreaterThanOrEqual(Shop::REWARD_MODE_COUPON_CODES),
                    new LessThanOrEqual(Shop::REWARD_MODE_COUPON_CODES_AND_CASH),
                ]
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Shop::class,
            'constraints' => [
                new ShopOffer()
            ],
            'validate_min_amount_cps' => true,
            'validate_min_amount_cpl' => true
        ]);
    }
}