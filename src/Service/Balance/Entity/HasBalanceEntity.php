<?php


namespace App\Service\Balance\Entity;


use App\Entity\Balance;
use App\Entity\Currency;
use Doctrine\Common\Collections\Collection;

interface HasBalanceEntity
{
    public function getBalances(): Collection;

    public function findOrCreateBalance(Currency $currency): Balance;

    public function createBalance(): Balance;
}