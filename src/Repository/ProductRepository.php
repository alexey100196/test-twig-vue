<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\ProductInteraction;
use App\Entity\ProductPurchase;
use App\Entity\ProductView;
use App\Entity\Shop;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @param Shop $shop
     * @param User $user
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return array
     */
    public function getShopProductsPurchaseStatisticsForReferrer(Shop $shop, User $user, \DateTime $startDate, \DateTime $endDate)
    {
        return $this->createQueryBuilder('p')
            ->select('
                p as product,
                sum(pp.referrerProfit) as referrer_profit, 
                sum(pp.price * pp.amount) as total_purchase_amount, 
                count(pp.id) as purchases_count, 
                count(pv.id) as views_count
            ')
            ->leftJoin(
                ProductPurchase::class,
                'pp',
                Join::WITH,
                'pp.product = p AND pp.referrer = :user'
            )
            ->leftJoin(
                ProductView::class,
                'pv',
                Join::WITH,
                'pv.product = p AND pp.referrer = :user'
            )
            ->where('p.shop = :shop')
            ->andWhere('pp.createdAt >= :startDate')
            ->andWhere('p.createdAt <= :endDate')
            ->groupBy('p.id')
            ->setParameters(compact('user', 'shop', 'startDate', 'endDate'))
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Shop $shop
     * @param string $website
     * @param bool $strict
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findShopProductByWebsite(Shop $shop, string $website, $strict = true)
    {
        $query = $this->createQueryBuilder('p')
            ->leftJoin(Shop::class, 's', Join::WITH, 'p.shop = s')
            ->andWhere('s = :shop');

        $query = $strict ? $query->andWhere('p.website = :website') : $query->andWhere('p.website LIKE :website');

        return $query->setParameters(compact('shop', 'website'))
            ->getQuery()
            ->getOneOrNullResult();
    }
}
