<?php

namespace App\Repository;

use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\ShopSignUp;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopSignUp|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopSignUp|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopSignUp[]    findAll()
 * @method ShopSignUp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopSignUpRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopSignUp::class);
    }

    /**
     * @param ArrayCollection $shops
     * @param User $referrer
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return mixed
     */
    public function getShopsGlobalIncomeStats(ArrayCollection $shops, User $referrer, \DateTime $startDate, \DateTime $endDate)
    {
        return $this->createQueryBuilder('ssu')
            ->select('
                s as shop,
                rl.slug as referrer_link_slug,
                rl.destinationUrl as referrer_link_destination_url,
                COUNT(ssu.id) as signups_count,
                SUM(ssu.commission) as signups_total_value,
                SUM(ssu.referrerProfit) as referrer_profit,
                MAX(ssu.createdAt) as last_signup
            ')
            ->innerJoin(ReferrerLink::class, 'rl', Join::WITH, 'ssu.referrerLink = rl')
            ->innerJoin(Shop::class, 's', Join::WITH, 'ssu.shop = s')
            ->andWhere('ssu.createdAt >= :startDate')
            ->andWhere('ssu.createdAt <= :endDate')
            ->andWhere('ssu.referrer = :referrer')
            ->andWhere('ssu.shop IN (:shops)')
            ->setParameters(compact('referrer', 'shops', 'startDate', 'endDate'))
            ->groupBy('ssu.referrerLink')
            ->getQuery()
            ->getResult();
    }
}
