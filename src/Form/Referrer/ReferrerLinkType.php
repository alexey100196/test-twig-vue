<?php

namespace App\Form\Referrer;

use App\Entity\Product;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Form\Type\WebsiteType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ReferrerLinkType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('slug', TextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 2, 'max' => 255]),
                ],
            ])
            ->add('destinationUrl', WebsiteType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 2, 'max' => 255]),
                ],
            ]);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($options) {
            /** @var Shop $shop */
            $shop = $options['shop'];
            /** @var ReferrerLink $data */
            $data = $event->getData();
            $form = $event->getForm();

            $shopWebsite = parse_url($shop->getWebsite());
            $urlWebsite = parse_url($data->getDestinationUrl());

            if (
                isset($shopWebsite['host']) &&
                isset($urlWebsite['host']) &&
                $shopWebsite['host'] == $urlWebsite['host']
            ) {
                return;
            }

            $form->get('destinationUrl')->addError(
                new FormError(
                    sprintf('Invalid URL address. Address should be under \'%s\' domain.', $shop->getWebsite())
                )
            );
        });

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReferrerLink::class,
            'shop' => null,
            'constraints' => [
                new UniqueEntity([
                    'fields' => ['shop', 'referrer', 'slug'],
                    'repositoryMethod' => 'findBy',
                    'message' => 'Given slug is already taken.',
                ]),
            ],
        ]);
    }
}
