<?php


namespace App\Controller\Account\Shop;

use App\Entity\Balance;
use App\Entity\Coupon\Coupon;
use App\Entity\Coupon\CouponPackage;
use App\Entity\Media;
use App\Entity\Shop;
use App\Entity\Widget\WidgetShopSetting;
use App\Form\Shop\Widget\CouponPackageType;
use App\Form\Shop\Widget\WidgetButtonType;
use App\Form\Shop\Widget\WidgetHeaderType;
use App\Form\Shop\Widget\WidgetRewardSettingsType;
use App\Repository\BalanceRepository;
use App\Repository\Coupon\CouponPackageRepository;
use App\Repository\UserBalanceRepository;
use App\Repository\Widget\WidgetButtonRepository;
use App\Repository\Widget\WidgetShopSettingRepository;
use App\Service\Coupon\CreateCouponPackage;
use App\Service\MediaUploader;
use App\Service\Widget\WidgetButtonService;
use App\Traits\Controller\FormResponseArray;
use App\Util\Consts;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ShopIncomeController
 *
 * @Route("/account/shop/widget")
 * @Security("has_role('ROLE_SHOP')")
 */
class ShopWidgetController extends AbstractController
{
    use FormResponseArray;

    /**
     * @var CreateCouponPackage
     */
    private $createCouponPackage;

    public function __construct(CreateCouponPackage $createCouponPackage)
    {
        $this->createCouponPackage = $createCouponPackage;
    }

    /**
     * @Route("/header", name="account_shop_header")
     * @param Request $request
     * @param WidgetShopSettingRepository $widgetShopSettingRepository
     * @param EntityManagerInterface $entityManager
     * @param MediaUploader $mediaUploader
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function widgetHeader(
        Request $request,
        WidgetShopSettingRepository $widgetShopSettingRepository,
        EntityManagerInterface $entityManager,
        MediaUploader $mediaUploader
    )
    {
        /** @var Shop $shop */
        $shop = $this->getUser()->getShop();

        if ((int) number_format($shop->getCompletion()->getCompletionPercent(), 0) !== 100) {
//            @TODO: Uncomment the redirect below
//            return $this->redirectToRoute('account_modals_list');
        }

        if (!$widgetShopSetting = $widgetShopSettingRepository->findOneBy(['shop' => $shop])) {
            $widgetShopSetting = $shop->getWidgetSettingOrCreateNew();
        }

        $wss_tab = $widgetShopSettingRepository->getAllByShop($shop);
        $form = $this->createForm(WidgetHeaderType::class, $widgetShopSetting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var WidgetShopSetting $settings */
            $settings = $form->getData();

            foreach ($wss_tab as $wss){
                $wss->setWelcomePicturePosX($settings->getWelcomePicturePosX());
                $wss->setWelcomePicturePosY($settings->getWelcomePicturePosY());
                $wss->setWelcomePictureHeight($settings->getWelcomePictureHeight());
                $wss->setWelcomePictureWidth($settings->getWelcomePictureWidth());
                $wss->setWelcomePictureRatio($settings->getWelcomePictureRatio());
                $wss->setWelcomePicture($settings->getWelcomePicture());
                $entityManager->persist($wss);
            }

            if ($settings->getWelcomePicture()) {
                if ($settings->getWelcomePicture()->getFile()) {
                    $mediaUploader->upload($settings->getWelcomePicture());
                    $media = $widgetShopSetting->getWelcomePicture();
                   if ($media){
                      /*  $media->setWidgetShopSetting($widgetShopSetting);*/
                    }
                } else if ($settings->getWelcomePicture()->getId() === null) {
                    $settings->setWelcomePicture(null);
                }
            }

            $entityManager->persist($settings);
            $entityManager->flush();
            $this->addFlash(Consts::SUCCESS, 'Widget header saved');
            return $this->redirectToRoute('account_shop_icon');
        }

        $translation = $this->getTranslation($shop);
        $campaigns = $shop->getCampaigns();
        $activeCampaign = null;

        foreach($campaigns as $campaign ){
            if ( $campaign->getActivatedAt() != null ){
                $activeCampaign = $campaign;
            }
        }

        return $this->render('account/shop/widget/header.html.twig', [
            'camp' => $activeCampaign,
            'form' => $form->createView(),
            'shop' => $shop,
            'translation' => $translation
        ]);
    }

    /**
     * @Route("/coupons", name="account_shop_coupons", methods={"GET", "POST"})
     * @param CouponPackageRepository $couponPackageRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function widgetCoupons(
        CouponPackageRepository $couponPackageRepository,
        Request $request)
    {
        $shop = $this->getUser()->getShop();

        if ((int) number_format($shop->getCompletion()->getCompletionPercent(), 0) !== 100) {
//            @TODO: Uncomment the redirect below
//            return $this->redirectToRoute('account_modals_list');
        }

        $oneTimeReferrerCoupons = $couponPackageRepository->findByShopAndMultipleUsageAndAllocation($shop, 0, CouponPackage::ALLOCATION_REFERRER);
        $multipleReferrerCoupons = $couponPackageRepository->findByShopAndMultipleUsageAndAllocation($shop, 1, CouponPackage::ALLOCATION_REFERRER);
        $oneTimeInviteeCoupons = $couponPackageRepository->findByShopAndMultipleUsageAndAllocation($shop, 0, CouponPackage::ALLOCATION_INVITEE);
        $multipleInviteeCoupons = $couponPackageRepository->findByShopAndMultipleUsageAndAllocation($shop, 1, CouponPackage::ALLOCATION_INVITEE);

        $form = $this->createForm(CouponPackageType::class, null, ['referer_coupons_provided' => $oneTimeReferrerCoupons || $multipleReferrerCoupons, 'invitee_coupons_provided' => $oneTimeInviteeCoupons || $multipleInviteeCoupons]);
        $form->handleRequest($request);

        $packages_rm = $couponPackageRepository->findByShopAndMultipleUsageAndAllocation($shop, 1, CouponPackage::ALLOCATION_REFERRER);
        $packages_rs = $couponPackageRepository->findByShopAndMultipleUsageAndAllocation($shop, 0, CouponPackage::ALLOCATION_REFERRER);
        $packages_im = $couponPackageRepository->findByShopAndMultipleUsageAndAllocation($shop, 1, CouponPackage::ALLOCATION_INVITEE);
        $packages_is = $couponPackageRepository->findByShopAndMultipleUsageAndAllocation($shop, 0, CouponPackage::ALLOCATION_INVITEE);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $couponName = '';
            try {
                if ($data['referrerCouponType'] === CouponPackageType::ONE_TIME_OPTION && isset($data['referrerOneTimeCoupons']['csv'])) {
                    $couponName = ' referrer';
                    if (sizeof($packages_rs)  >= CouponPackage::MAX_SINGLE_COUPON_PACKAGE) {
                    } else {
                        $this->createReferrerOneTimeCoupon($shop, $data);
                    }
                }

                if ($data['referrerCouponType'] === CouponPackageType::MULTIPLE_OPTION && isset($data['referrerMultipleUsageCouponCode'])) {
                    $couponName = ' referrer';
                    if (sizeof($packages_rm) >= CouponPackage::MAX_MULTIPLE_COUPON_PACKAGE) {
                    } else {
                        $this->createReferrerMultipleUsageCoupon($shop, $data);
                    }
                }

                if ($data['inviteeCouponType'] === CouponPackageType::MULTIPLE_OPTION && isset($data['inviteeMultipleUsageCouponCode'])) {
                    $couponName = ' invitee';
                    if (sizeof($packages_im)  >= CouponPackage::MAX_MULTIPLE_COUPON_PACKAGE) {
                    } else {
                        $this->createInviteeMultipleUsageCoupon($shop, $data);
                    }
                }

                if ($data['inviteeCouponType'] === CouponPackageType::ONE_TIME_OPTION && isset($data['inviteeOneTimeCoupons']['csv'])) {
                    $couponName = ' invitee';
                    if (sizeof($packages_is) >= CouponPackage::MAX_SINGLE_COUPON_PACKAGE) {
                    } else {
                        $this->createInviteeOneTimeCoupon($shop, $data);
                    }
                }
                unset($form);
                $form = $this->createForm(CouponPackageType::class);
            } catch (\LogicException $ex) {
                $form->addError(new FormError($ex->getMessage() . $couponName));
            }

            if (sizeof($packages_rm)  >= CouponPackage::MAX_SINGLE_COUPON_PACKAGE && isset($data['referrerMultipleUsageCouponCode'])) {
                $this->addFlash(Consts::SUCCESS, 'Referrer multiple-usage-coupon can not be added. Limit is: '.CouponPackage::MAX_SINGLE_COUPON_PACKAGE);
            }
            elseif (sizeof($packages_rs)  >= CouponPackage::MAX_SINGLE_COUPON_PACKAGE && isset($data['referrerOneTimeCoupons']['csv']) ) {
                $this->addFlash(Consts::SUCCESS, 'Referrer single-usage-coupon can not be added. Limit is:' .CouponPackage::MAX_SINGLE_COUPON_PACKAGE);
            }
            elseif (sizeof($packages_im)  >= CouponPackage::MAX_MULTIPLE_COUPON_PACKAGE && isset($data['inviteeMultipleUsageCouponCode'])) {
                $this->addFlash(Consts::SUCCESS, 'Invitee multi-usage-coupon can not be added. Limit is:'.CouponPackage::MAX_MULTIPLE_COUPON_PACKAGE);
            }
            elseif (sizeof($packages_is)  >= CouponPackage::MAX_SINGLE_COUPON_PACKAGE && isset($data['inviteeOneTimeCoupons']['csv'])) {
                $this->addFlash(Consts::SUCCESS, 'Invitee single-usage-coupon can not be added. Limit is:'.CouponPackage::MAX_SINGLE_COUPON_PACKAGE);
            }
            else {
                $this->addFlash(Consts::SUCCESS, 'Your coupons saved');
            }
            /*return $this->redirectToRoute('account_shop_rewards');*/
        }

        return $this->render('account/shop/widget/coupons.html.twig', [
            'form' => $form->createView(),
            'shop' => $shop,
            'oneTimeReferrerCoupons' => $couponPackageRepository->findByShopAndMultipleUsageAndAllocation($shop, 0, CouponPackage::ALLOCATION_REFERRER),
            'multipleReferrerCoupons' => $couponPackageRepository->findByShopAndMultipleUsageAndAllocation($shop, 1, CouponPackage::ALLOCATION_REFERRER),
            'oneTimeInviteeCoupons' => $couponPackageRepository->findByShopAndMultipleUsageAndAllocation($shop, 0, CouponPackage::ALLOCATION_INVITEE),
            'multipleInviteeCoupons' => $couponPackageRepository->findByShopAndMultipleUsageAndAllocation($shop, 1, CouponPackage::ALLOCATION_INVITEE),
        ]);
    }

    /**
     * @Route("/rewards", name="account_shop_rewards", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param BalanceRepository $balanceRepository
     * @param UserBalanceRepository $userBalanceRepository
     * @param WidgetShopSettingRepository $widgetShopSettingRepository
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function widgetRewards(
        Request $request,
        WidgetShopSettingRepository $widgetShopSettingRepository,
        BalanceRepository $balanceRepository,
        UserBalanceRepository $userBalanceRepository
    )
    {
        /** @var Shop $shop */
        $shop = $this->getUser()->getShop();

        if ((int) number_format($shop->getCompletion()->getCompletionPercent(), 0) !== 100) {
//            @TODO: Uncomment the redirect below
//            return $this->redirectToRoute('account_modals_list');
        }

        if (!$widgetShopSetting = $widgetShopSettingRepository->findOneBy(['shop' => $shop])) {
            $widgetShopSetting = $shop->getWidgetSettingOrCreateNew();
        }


        $usrBalance = $userBalanceRepository->findOneBy(['id' => $shop]);
        $balValue = $balanceRepository->findOneBy(['id' => $usrBalance]);

        $readForm = $this->createForm(WidgetRewardSettingsType::class, $widgetShopSetting, [WidgetRewardSettingsType::MODIFY_STRUCTURE_OPTION => false]);
        $readForm->handleRequest($request);
        if (!$balValue) {
            $balValue = new Balance();
        }
        return $this->render('account/shop/widget/rewards.html.twig', [
            'form' => $readForm->createView(),
            'balance' => $balValue,
            'shop' => $shop,
            'campaigns' => $shop->getCampaignsArray()
        ]);
    }

    /**
     * @Route("/email", name="account_shop_email")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function widgetEmail(Request $request, EntityManagerInterface $entityManager)
    {
        /** @var Shop $shop */
        $shop = $this->getUser()->getShop();
        /** @var WidgetShopSetting $shopWidgetSetting */
        $shopWidgetSetting = $shop->getWidgetSetting();

        if (!$shopWidgetSetting->getWizardStep()) {
            $shopWidgetSetting->setWizardStep(1);
            $entityManager->persist($shopWidgetSetting);
            $entityManager->flush();
        }

        $translation = $this->getTranslation($shop);

        return $this->render('account/shop/widget/email.html.twig', [
            'shop' => $shop,
            'translation' => $translation
        ]);
    }

    /**
     * @Route("/icon", name="account_shop_icon", methods={"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function widgetIcon(
        Request $request,
        WidgetButtonRepository $widgetButtonRepository,
        WidgetButtonService $widgetButtonService,
        EntityManagerInterface $entityManager
    )
    {
        $shop = $this->getUser()->getShop();

        if (!$widgetButton = $widgetButtonRepository->findOneBy(['shop' => $shop])) {
            $widgetButton = $widgetButtonService->createWidgetButtonForShop($shop);
        }

        $form = $this->createForm(WidgetButtonType::class, $widgetButton);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($widgetButton);
            $entityManager->flush();
            $this->addFlash(Consts::SUCCESS, 'Widget icon saved');
            return $this->redirectToRoute('account_shop_widget_refferal');
        }

        return $this->render('account/shop/widget/icon.html.twig', [
            'form' => $form->createView(),
            'shop' => $shop
        ]);
    }

    /**
     * @Route("/icon/update", name="account_shop_icon_update", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function widgetIconUpdate(
        Request $request,
        WidgetButtonRepository $widgetButtonRepository,
        WidgetButtonService $widgetButtonService,
        EntityManagerInterface $entityManager
    )
    {
        $shop = $this->getUser()->getShop();

        if (!$widgetButton = $widgetButtonRepository->findOneBy(['shop' => $shop])) {
            $widgetButton = $widgetButtonService->createWidgetButtonForShop($shop);
        }

        $form = $this->createForm(WidgetButtonType::class, $widgetButton);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        try {
            $entityManager->persist($widgetButton);
            $entityManager->flush();
        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse([
            'message' => 'Icon settings updated successfully'
        ]);
    }

    /**
     * @Route("/refferal", name="account_shop_widget_refferal")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function widgetRefferal(Request $request, EntityManagerInterface $entityManager)
    {
        /** @var Shop $shop */
        $shop = $this->getUser()->getShop();
        /** @var WidgetShopSetting $shopWidgetSetting */
        $shopWidgetSetting = $shop->getWidgetSetting();

        if ($shopWidgetSetting->getWizardStep() < 3) {
            $shopWidgetSetting->setWizardStep(3);
            $entityManager->persist($shopWidgetSetting);
            $entityManager->flush();
        }

//        $translation = $this->getTranslation($shop);

        return $this->render('account/shop/widget/refferal.html.twig', [
            'shop' => $shop
        ]);
    }

    /**
     * @Route("/finish", name="account_shop_widget_finish")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function widgetFinish(Request $request, EntityManagerInterface $entityManager)
    {
        /** @var Shop $shop */
        $shop = $this->getUser()->getShop();
        /** @var WidgetShopSetting $shopWidgetSetting */
        $shopWidgetSetting = $shop->getWidgetSetting();

        if ($shopWidgetSetting->getWizardStep() < 4) {
            $shopWidgetSetting->setWizardStep(4);
            $entityManager->persist($shopWidgetSetting);
            $entityManager->flush();
        }

//        $translation = $this->getTranslation($shop);

        return $this->render('account/shop/widget/finish.html.twig', [
            'shop' => $shop
        ]);
    }

    /**
     * @param bool $multipleUsage
     * @param int $allocation
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderWidgetCouponForm(bool $multipleUsage, int $allocation)
    {
        $package = new CouponPackage();
        $package->setMultipleUsage($multipleUsage);
        $package->setAllocation($allocation);

        $form = $this->createForm(CouponPackageType::class, $package, [
            'action' => $this->generateUrl('upload_widiget_package')
        ]);

        return $this->render('account/shop/widget/parts/coupons_upload_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/coupon-package/{id}", name="delete_widiget_package", methods={"DELETE"})
     * @param int $id
     * @param CouponPackageRepository $couponPackageRepository
     * @param EntityManagerInterface $em
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteCouponPackage(int $id, CouponPackageRepository $couponPackageRepository, EntityManagerInterface $em)
    {
        $couponPackage = $couponPackageRepository->findOneBy(['shop' => $this->getUser()->getShop(), 'id' => $id]);

        if (!$couponPackage) {
            return new JsonResponse(['message' => 'Coupon package not found.'], Response::HTTP_BAD_REQUEST);

        }

        if ($couponPackage->isActive()){
            return new JsonResponse(['message' => 'Can not delete active coupon package.'], Response::HTTP_BAD_REQUEST);
        }

        try {
            $em->remove($couponPackage);
            $em->flush();
        } catch (\Exception $e) {
            return new JsonResponse(['message' => 'Cannot delete coupon package.'], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(null, Response::HTTP_OK);
    }

    /**
     * @Route("/coupon-package", name="account_shop_coupon_packages", methods={"GET"})
     * @return JsonResponse
     */
    public function packageIndex(Request $request)
    {
        /** @var Shop $shop */
        $shop = $this->getUser()->getShop();

        $packages = [];
        foreach ($shop->getCouponPackagesWithCoupons() as $package) {
            $packages[] = $this->couponPackageToArray($package);
        }

        if ($allocationFilter = $request->query->get('allocation')) {
            $packages = array_filter($packages, function ($package) use ($allocationFilter) {
                return $package['allocation'] == $allocationFilter;
            });
        }

        return new JsonResponse(array_values($packages));
    }

    /**
     * @Route("/coupon-package/{package}/coupons", name="account_shop_coupon_package_coupons", methods={"GET"})
     * @param CouponPackage $package
     * @return JsonResponse
     */
    public function couponsIndex(CouponPackage $package)
    {
        if ($package->getShop() !== $this->getUser()->getShop()) {
            return new JsonResponse(['message' => 'Coupon package not found.'], Response::HTTP_NOT_FOUND);
        }

        $coupons = [];

        foreach ($package->getCoupons() as $coupon) {
            $coupons[] = ['id' => $coupon->getId(), 'code' => $coupon->getCode(), 'conversion_value' => $coupon->getConversionValue()];
        }

        return new JsonResponse($coupons);
    }

    /**
     * @Route("/coupon-package/{package}", name="account_shop_coupon_package", methods={"GET"})
     * @param CouponPackage $package
     * @return JsonResponse
     */
    public function show(CouponPackage $package)
    {
        if ($package->getShop() !== $this->getUser()->getShop()) {
            return new JsonResponse(['message' => 'Coupon package not found.'], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($this->couponPackageToArray($package));
    }

    private function couponPackageToArray(CouponPackage $package): array
    {
        return [
            'id' => $package->getId(),
            'multiple' => $package->getMultipleUsage(),
            'discount_type' => (string)$package->getDiscountType(),
            'discount' => $package->getDiscount(),
            'allocation' => $package->getAllocationName(),
            'active' => $package->isActive(),
        ];
    }

    /**
     * @Route("/coupon-package/{package}/coupons/{coupon}", name="account_shop_coupon_package_coupon_delete", methods={"DELETE"})
     * @param CouponPackage $package
     * @param Coupon $coupon
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function deleteCouponPackageCoupon(CouponPackage $package, Coupon $coupon, EntityManagerInterface $em)
    {
        if ($package->getShop() !== $this->getUser()->getShop() || $coupon->getPackage() !== $package) {
            return new JsonResponse(['message' => 'Coupon not found.'], Response::HTTP_NOT_FOUND);
        }

        if ($package->isActive() && $package->getCouponsCount() === 1) {
            return new JsonResponse(['message' => 'Cannot delete last coupon of active package.'], Response::HTTP_NOT_FOUND);
        }

        try {
            $em->remove($coupon);
            $em->flush();
        } catch (\Exception $e) {
            return new JsonResponse(['message' => 'Cannot delete coupon.'], Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse();
    }

    private function updateRewardsMode(Shop $shop, WidgetShopSetting $widgetShopSetting)
    {
        if ($widgetShopSetting->getReferrerRewardCashEnabled() && $widgetShopSetting->getReferrerRewardCouponEnabled()) {
            $rewardsMode = Shop::REWARD_MODE_COUPON_CODES_AND_CASH;
        } else {
            $rewardsMode = $widgetShopSetting->getReferrerRewardCashEnabled() ? Shop::REWARD_MODE_CASH : Shop::REWARD_MODE_COUPON_CODES;
        }

        if ($rewardsMode === Shop::REWARD_MODE_COUPON_CODES && $shop->getUser()->hasAnyFunds('EUR')) {
            $rewardsMode = Shop::REWARD_MODE_COUPON_CODES_AND_CASH;
        }

        $shop->setRewardsMode($rewardsMode);
    }

    private function createReferrerMultipleUsageCoupon(Shop $shop, array $data)
    {

        $this->createCouponPackage->create(
            [$data['referrerMultipleUsageCouponCode']],
            (float)$data['referrerCouponDiscount'],
            $data['referrerCouponDiscountType'],
            true,
            $shop,
            CouponPackage::ALLOCATION_REFERRER
        );
    }

    private function createInviteeMultipleUsageCoupon(Shop $shop, array $data)
    {
        $this->createCouponPackage->create(
            [$data['inviteeMultipleUsageCouponCode']],
            (float)$data['inviteeCouponDiscount'],
            $data['inviteeCouponDiscountType'],
            true,
            $shop,
            CouponPackage::ALLOCATION_INVITEE
        );
    }

    private function createReferrerOneTimeCoupon(Shop $shop, array $data)
    {
        $this->createCouponPackage->createFromCsv(
            $data['referrerOneTimeCoupons']['csv'],
            (float)$data['referrerCouponDiscount'],
            $data['referrerCouponDiscountType'],
            false,
            $shop,
            CouponPackage::ALLOCATION_REFERRER
        );
    }

    private function createInviteeOneTimeCoupon(Shop $shop, array $data)
    {
        $this->createCouponPackage->createFromCsv(
            $data['inviteeOneTimeCoupons']['csv'],
            (float)$data['inviteeCouponDiscount'],
            $data['inviteeCouponDiscountType'],
            false,
            $shop,
            CouponPackage::ALLOCATION_INVITEE
        );
    }

    private function getTranslation(Shop $shop): array
    {
        $lang = $shop->getWidgetSetting() && $shop->getWidgetSetting()->getLanguage() ? $shop->getWidgetSetting()->getLanguage()->getCode() : 'en';

        return Yaml::parseFile(__DIR__ . '/../../../../translations/widget.' . $lang . '.yaml');
    }
}