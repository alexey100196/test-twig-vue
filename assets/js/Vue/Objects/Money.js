export function moneyFactory(currency, amount) {

    const symbols = {
        'USD': {'symbol': '$', 'position': 'pre'},
        'EUR': {'symbol': '€', 'position': 'pre'},
        'PLN': {'symbol': 'zł', 'position': 'post'}
    };

    function _getHumanAmount(amount, symbolDef) {
        if (symbolDef.position == 'pre') {
            return symbolDef.symbol + amount;
        } else if (symbolDef.position == 'post') {
            return amount + symbolDef.symbol
        }
    }

    function Money(currency, amount) {
        this.currency = currency;
        this.amount = parseFloat(amount);
        this.symbol = symbols[this.currency].symbol;
        this.human_amount = _getHumanAmount(this.amount.toFixed(2), symbols[this.currency]);
    }

    Money.prototype.add = function (money) {
        this.amount += money.amount;
        this.human_amount = _getHumanAmount(this.amount.toFixed(2), symbols[this.currency]);
        return this;
    };

    Money.prototype.sub = function (money) {
        this.amount -= money.amount;
        this.human_amount = _getHumanAmount(this.amount.toFixed(2), symbols[this.currency]);
        return this;
    };

    return new Money(currency, amount);
}