<?php


namespace App\Command;

use App\Service\Exchange\ExchangeService;
use App\Service\Shop\ShopConversionStatusService;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AcceptConversionsWhenTimeForAcceptanceIsOverCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:accept-conversions-when-time-for-acceptance-is-over';

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ShopConversionStatusService $service */
        $service = $this->getContainer()->get(ShopConversionStatusService::class);

        $start = Carbon::now()->subMonth()->startOfMonth();
        $end = Carbon::now()->subMonth()->endOfMonth();

        $service->acceptConversionsDatePeriod($start, $end);
    }
}