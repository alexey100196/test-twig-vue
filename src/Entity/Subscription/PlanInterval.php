<?php

namespace App\Entity\Subscription;

use App\Entity\BaseGlossary;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class PlanInterval extends BaseGlossary
{
    const DAY = 1;
    const MONTH = 2;
    const YEAR = 3;

    /**
     * @ORM\Column(type="integer")
     */
    protected $daysCount;

    /**
     * @return mixed
     */
    public function getDaysCount()
    {
        return $this->daysCount;
    }
}
