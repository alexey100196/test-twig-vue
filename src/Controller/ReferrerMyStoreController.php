<?php


namespace App\Controller;


use App\Entity\ReferrerMyStore;
use App\Repository\ReferrerMyStoreRepository;
use App\Repository\ReferrerStoreNonReferableLinkRepository;
use App\Service\Referrer\ReferrerLinkStoreCardService;
use App\Service\Referrer\ReferrerMyStoreService;
use App\Service\Referrer\ReferrerShop;
use App\Service\Referrer\ReferrerStoreNonReferableLinkService;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ReferrerMyStoreController extends AbstractController
{
    /**
     * @var ReferrerMyStoreService
     */
    private $referrerMyStoreService;

    /**
     * @var ReferrerLinkStoreCardService
     */
    private $referrerLinkStoreCardService;

    /**
     * @var ReferrerStoreNonReferableLinkService
     */
    private $referrerStoreNonReferableLinkService;

    /**
     * @var ReferrerStoreNonReferableLinkRepository
     */
    private $nonReferableLinkRepository;

    /**
     * @var ReferrerMyStoreRepository
     */
    private $referrerMyStoreRepository;

    /**
     * ReferrerLinkController constructor.
     * @param ReferrerMyStoreService $referrerMyStoreService
     * @param ReferrerLinkStoreCardService $referrerLinkStoreCardService
     * @param ReferrerStoreNonReferableLinkService $referrerStoreNonReferableLinkService
     * @param ReferrerStoreNonReferableLinkRepository $nonReferableLinkRepository
     * @param ReferrerMyStoreRepository $referrerMyStoreRepository
     */
    public function __construct(
        ReferrerMyStoreService $referrerMyStoreService,
        ReferrerLinkStoreCardService $referrerLinkStoreCardService,
        ReferrerStoreNonReferableLinkService $referrerStoreNonReferableLinkService,
        ReferrerStoreNonReferableLinkRepository $nonReferableLinkRepository,
        ReferrerMyStoreRepository $referrerMyStoreRepository
    ) {
        $this->referrerMyStoreService = $referrerMyStoreService;
        $this->referrerLinkStoreCardService = $referrerLinkStoreCardService;
        $this->referrerStoreNonReferableLinkService = $referrerStoreNonReferableLinkService;
        $this->nonReferableLinkRepository = $nonReferableLinkRepository;
        $this->referrerMyStoreRepository = $referrerMyStoreRepository;
    }

    /**
     * @Route("/store/", name="referrer_my_store_create")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create()
    {
        $user = $this->getUser();
        if ($user && $user->getReferrerMyStore()) {
            return $this->redirectToRoute('referrer_show_me');
        }
        return $this->render('notlogged/mystore-create.html.twig', [
            'link' => $url = $this->generateUrl('referrer_my_store_preview', [
                'slug' => bin2hex(random_bytes(8))
            ], UrlGeneratorInterface::ABSOLUTE_URL)
        ]);
    }

    /**
     * @Route("/store/{slug}", name="referrer_my_store_preview")
     *
     * @param string $slug
     * @param ReferrerShop $referrerShop
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function preview(string $slug, ReferrerShop $referrerShop)
    {
        $myStore = $this->referrerMyStoreRepository->findOneBy([
            'link' => $this->generateUrl('referrer_my_store_preview', [
                'slug' => $slug
            ], UrlGeneratorInterface::ABSOLUTE_URL)
        ]);
        if (!$myStore) throw new NotFoundHttpException('Referrer store not found.');
        $user = $myStore->getReferrer();
        if (!$user) throw new NotFoundHttpException('User not found.');
        $sort = (Criteria::create())->orderBy(['createdAt' => Criteria::DESC]);

        $nonReferableLinksArray = $this->referrerStoreNonReferableLinkService->loadItemsToArray(
            $this->nonReferableLinkRepository->findBy(['mystore' => $myStore], ['orderColumn' => 'DESC'])
        );
        $myStoreReferrerLinksArray = $this->referrerLinkStoreCardService->loadItemsToArray(
            $user,
            $user->getMyStoreReferrerLinks()->matching($sort)
        );
        usort($myStoreReferrerLinksArray, [$this, 'sortByOrderColumn']);
        $companies = $referrerShop->getReferrerShops($user);

        $companiesArray = [];
        foreach($companies as $company) {
            $companiesArray[] = [
                'id' => $company->getId(),
                'name' => $company->getName(),
            ];
        }

        return $this->render('notlogged/mystore.html.twig', [
            'referrer' => $user,
            'companies' => $companiesArray,
            'referrerMyStore' => $this->referrerMyStoreService->objectToArray($user->getReferrerMyStore()),
            'nonReferableLinksArray' => $nonReferableLinksArray,
            'myStoreReferrerLinksArray' => $myStoreReferrerLinksArray
        ]);
    }

    private function sortByOrderColumn($a, $b) {
        if ($a['orderColumn'] == $b['orderColumn']) {
            return 0;
        }
        return ($a['orderColumn'] < $b['orderColumn']) ? -1 : 1;
    }
}