<?php

namespace App\Controller\Admin;

use App\Entity\Shop;
use App\Entity\ShopStatus;
use App\Event\Shop\ShopStatusChangedByAdmin;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ShopStatusController extends AbstractController
{
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/admin/company/{shop}/status", name="admin_shop_status_update", methods={"POST"})
     * @param Shop $shop
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Shop $shop, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $shop->setStatus($em->getReference(ShopStatus::class, $request->request->getInt('status')));
        $em->flush();

        if ($shop->isActive() || $shop->isRejected()) {
            $this->eventDispatcher->dispatch(ShopStatusChangedByAdmin::NAME, new ShopStatusChangedByAdmin($shop));
            $this->addFlash(Consts::SUCCESS, 'Status changed!');
        }

        return new JsonResponse([], Response::HTTP_OK);
    }
}
