<?php

namespace App\Validator\Constraints;

use App\Entity\Media;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ImageFileOrUrlRequiredValidator extends ConstraintValidator
{
    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        // If image url or image file not provided then return err.
        if (($this->valueHasImageUrl($value) || $this->valueHasImageFile($value) || $this->valueHasAlreadyUploadedMedia($value)) === false) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }

    private function valueHasAlreadyUploadedMedia($value)
    {
        return $value->getImage() instanceof Media && $value->getImage()->getId();
    }

    private function valueHasImageFile($value)
    {
        return $value->getImage() && $value->getImage()->getFile();
    }

    private function valueHasImageUrl($value)
    {
        return $value->getImageUrl();
    }
}