<?php


namespace App\EventSubscriber;

use App\Interfaces\Entity\HasBackground;
use App\Service\FileUploader;
use App\Service\UrlFileUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class BackgroundUploadSubscriber
{
    private $uploader;
    private $urlFileUploader;

    public function __construct(FileUploader $uploader, UrlFileUploader $urlFileUploader)
    {
        $this->uploader = $uploader;
        $this->urlFileUploader = $urlFileUploader;
    }

    private function hasBackground($entity)
    {
        return $entity instanceof HasBackground;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();

        $this->uploadFile($entity);
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (!$this->hasBackground($entity)) {
            return;
        }

        if ($fileName = $entity->getBackground()) {
            $entity->setBackground(new File($this->uploader->getTargetDirectory() . '/' . $fileName));
        }
    }

    private function uploadFile($entity)
    {
        // upload only works for Module entities
        if (!$this->hasBackground($entity)) {
            return;
        }

        $file = $entity->getBackground();

        // only upload new files
        if ($file instanceof UploadedFile) {
            $fileName = $this->uploader->upload($file);
            $entity->setBackground($fileName);
        } elseif ($url = $entity->getBackgroundUrl()) {
            $fileName = $this->urlFileUploader->upload($url);
            $entity->setBackground($fileName);
        } elseif ($file instanceof File) {
            // prevents the full file path being saved on updates
            // as the path is set on the postLoad listener
            $entity->setBackground($file->getFilename());
        }
    }
}