<?php


namespace App\Service\ShortLinks;


use App\Entity\ShortLinks\AffiliateShortLinkStats;
use Doctrine\ORM\EntityManagerInterface;

class AffiliateShortLinkStatsService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param AffiliateShortLinkStats $affiliateShortLinkStats
     * @return AffiliateShortLinkStats
     */
    public function save(AffiliateShortLinkStats $affiliateShortLinkStats)
    {
        $this->em->persist($affiliateShortLinkStats);
        $this->em->flush();

        return $affiliateShortLinkStats;
    }

    /**
     * @param AffiliateShortLinkStats $affiliateShortLinkStats
     */
    public function remove(AffiliateShortLinkStats $affiliateShortLinkStats)
    {
        $this->em->remove($affiliateShortLinkStats);
        $this->em->flush();
    }

}