<?php


namespace App\Service\ShopEvent\Events;


use App\Service\ShopEvent\Interfaces\EventInterface;

class TestConnection extends Event implements EventInterface
{
    /**
     * @param array $array
     * @return Event
     */
    public function createFromArray(array $array = []): Event
    {
         return new TestConnection();
    }
}