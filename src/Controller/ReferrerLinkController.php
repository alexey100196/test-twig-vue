<?php


namespace App\Controller;


use App\Entity\ReferrerLink;
use App\Service\Referrer\ReferrerLinkCardService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ReferrerLinkController extends AbstractController
{
    /**
     * @var ReferrerLinkCardService
     */
    private $referrerLinkCardService;

    /**
     * ReferrerLinkController constructor.
     * @param ReferrerLinkCardService $referrerLinkCardService
     */
    public function __construct(ReferrerLinkCardService $referrerLinkCardService) {
        $this->referrerLinkCardService = $referrerLinkCardService;
    }

    /**
     * @Route("/referrer-link/{referrerLink}/card", name="referrer_link_card")
     *
     * @param ReferrerLink $referrerLink
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function card(ReferrerLink $referrerLink)
    {
        if (!$card = $referrerLink->getCard()) {
            throw new NotFoundHttpException('Invalid card.');
        }

        $response = new Response(
            $this->renderView('account/referrer/card/preview.html.twig', [
                'referrerLink' => $referrerLink,
                'discount' => $card->getSubtitle(),
                'referrerLinkCard' => $this->referrerLinkCardService->convertObjectToArray($card),
            ]),
            Response::HTTP_OK
        );
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }
}