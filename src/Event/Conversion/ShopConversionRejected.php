<?php


namespace App\Event\Conversion;

use App\Entity\ShopConversion;
use App\Entity\ShopSignUp;
use Symfony\Component\EventDispatcher\Event;

class ShopConversionRejected extends Event implements ConversionEventInterface
{
    /** @var string */
    const NAME = 'conversion.shop-conversion-rejected';

    private $conversion;

    public function __construct(ShopConversion $conversion)
    {
        $this->conversion = $conversion;
    }

    /**
     * @return ShopSignUp
     */
    public function getConversion(): ShopConversion
    {
        return $this->conversion;
    }
}