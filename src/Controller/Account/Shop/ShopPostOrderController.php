<?php

namespace App\Controller\Account\Shop;

use App\Entity\Shop;
use App\Form\Shop\PostOrder\ActivationFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ShopPostOrderController extends AbstractController
{
    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/post-order/activation", name="account_shop_post_order_activation")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function activation(Request $request, EntityManagerInterface $entityManager)
    {
        /** @var Shop $shop */
        $shop = $this->getUser()->getShop();

        $form = $this->createForm(ActivationFormType::class, $shop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($shop);
            $entityManager->flush();
        }

        return $this->render('account/shop/post-order/activation.html.twig', [
            'shop' => $shop,
            'form' => $form->createView(),
        ]);
    }
}