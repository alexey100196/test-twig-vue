<?php


namespace App\Service\Referrer;


use App\Entity\CustomDomain;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Form;
use function PHPUnit\throwException;

class CustomDomainService
{

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param CustomDomain $customDomain
     * @return CustomDomain
     */
    public function save(CustomDomain $customDomain)
    {
        $this->em->persist($customDomain);
        $this->em->flush();

        return $customDomain;
    }

    /**
     * @param User $user
     * @param CustomDomain $customDomain
     * @return bool
     * @throws \Exception
     */
    public function remove(User $user, CustomDomain $customDomain)
    {
        if ($user->isAdmin() || $user->getCustomDomains()->contains($customDomain)) {
            $this->em->remove($customDomain);
            $this->em->flush();

            return true;
        }

        throw new \Exception('You don\'t have permission to remove this custom domain.');
    }
}