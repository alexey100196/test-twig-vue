<?php

namespace App\Repository;

use App\Entity\ShopConversionStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopConversionStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopConversionStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopConversionStatus[]    findAll()
 * @method ShopConversionStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopConversionStatusRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopConversionStatus::class);
    }
}
