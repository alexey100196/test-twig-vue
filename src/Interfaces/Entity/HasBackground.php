<?php


namespace App\Interfaces\Entity;


interface HasBackground
{
    public function setBackground($background);

    public function getBackground();

    public function getBackgroundUrl();
}