<?php


namespace App\Service\ShopEvent\EventStrategies;


use App\Entity\EventEntry;
use App\Entity\Product;
use App\Entity\ProductView;
use App\Entity\ShopView;
use App\Manager\EventEntryManager;
use App\Service\ShopEvent\Auth;
use App\Service\ShopEvent\Interfaces\EventStrategyInterface;

class ViewStrategy extends AbstractStrategy implements EventStrategyInterface
{
    public function process()
    {
        $shopView = new ShopView();
        $shopView->setShop($this->eventEntry->getShop());
        $shopView->setReferrerLink($this->eventEntry->getReferrerLink());

        $productRepository = $this->doctrine->getRepository(Product::class);

        if ($number = (string)$this->eventEntry->getPayload('product')) {
            $product = $productRepository->findOneBy(['shop' => $this->eventEntry->getShop(), 'number' => $number]);

            if ($product) {
                $shopView->setProduct($product);
            }
        }

        $shopView->setEventEntry($this->eventEntry);

        $entityManager = $this->doctrine->getEntityManager();
        $entityManager->persist($shopView);
        $entityManager->flush();
    }
}