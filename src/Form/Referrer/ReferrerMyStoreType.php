<?php


namespace App\Form\Referrer;


use App\Entity\Media;
use FOS\RestBundle\Validator\Constraints\Regex;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Url;

class ReferrerMyStoreType extends AbstractType
{
    protected $groupOpt = [
        'required' => true,
        'by_reference' => null,
        'data_class' => null
    ];
    protected $urlOpt = [
        'default_protocol' => 'https',
        'required' => false,
    ];

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'required' => false,
            ])
            ->add('subtitle', TextType::class, [
                'required' => true,
            ])
            ->add('avatar', FileType::class, [
                'constraints' => [
                    new Image([
                        'maxSize' => '1M',
                        'maxSizeMessage' => 'Max avatar size is 1MB',
                    ])
                ],
                'invalid_message' => 'Invalid uploaded avatar',
                'mapped' => false
            ])
            ->add(
                $builder->create('contactUrls', FormType::class, $this->groupOpt)
                ->add('fb_messenger', UrlType::class, [
                    'required' => false,
                    'default_protocol' => 'https',
                    'constraints' => [
                        new Url(),
                        new Regex([
                            'match' => true,
                            'message' => 'This value must be valid messenger link [m.me links].',
                            'pattern' => '/^(http|https):\/\/([\w\.]*).*m\.me.*$/'
                        ])
                    ],
                ])
                ->add('mail', EmailType::class, [
                    'required' => false,
                    'constraints' => [
                        new Email([
                            'message' => 'This value is not valid email: {{ value }}'
                        ]),
                    ],
                ])
                ->add('skype', TextType::class, [
                    'required' => false
                ])
                ->add('whatsapp', TelType::class, [
                    'required' => false,
                    'constraints' => [
                        new Regex([
                            'match' => true,
                            'message' => 'This value must be valid phone number.',
                            'pattern' => '/^[0-9+ ]{0,6}[0-9-]*$/'
                        ])
                    ]
                ])
                ->add('whatsapp_title', TextType::class, [
                    'required' => false
                ])
            )
            ->add(
                $builder->create('socials', FormType::class, $this->groupOpt)
                    ->add('facebook', UrlType::class, array_merge($this->urlOpt, [
                        'constraints' => [
                            new Url(),
                            new Regex([
                                'match' => true,
                                'message' => 'This value must be valid facebook link.',
                                'pattern' => '/^(http|https):\/\/([\w\.]*)\..*facebook\..*$/'
                            ])
                        ]
                    ]))
                    ->add('instagram', UrlType::class, array_merge($this->urlOpt, [
                        'constraints' => [
                            new Url(),
                            new Regex([
                                'match' => true,
                                'message' => 'This value must be valid instagram link.',
                                'pattern' => '/^(http|https):\/\/([\w\.]*)\..*instagram\..*$/'
                            ])
                        ]
                    ]))
                    ->add('linkedin', UrlType::class, array_merge($this->urlOpt, [
                        'constraints' => [
                            new Url(),
                            new Regex([
                                'match' => true,
                                'message' => 'This value must be valid linkedin link.',
                                'pattern' => '/^(http|https):\/\/([\w\.]*)\..*linkedin\..*$/'
                            ])
                        ]
                    ]))
                    ->add('twitter', UrlType::class, array_merge($this->urlOpt, [
                        'constraints' => [
                            new Url(),
                            new Regex([
                                'match' => true,
                                'message' => 'This value must be valid twitter link.',
                                'pattern' => '/^(http|https):\/\/([\w\.]*)\..*twitter\..*$/'
                            ])
                        ]
                    ]))
                    ->add('youtube', UrlType::class, array_merge($this->urlOpt, [
                        'constraints' => [
                            new Url(),
                            new Regex([
                                'match' => true,
                                'message' => 'This value must be valid youtube link.',
                                'pattern' => '/^(http|https):\/\/([\w\.]*)\..*youtube\..*$/'
                            ])
                        ]
                    ]))
            )
            ->add(
                $builder->create('config', FormType::class, $this->groupOpt)
                    ->add('link', UrlType::class, [
                        'required' => true,
                        'constraints' => [
                            new Url([
                                'message' => 'This value is not valid url: {{ value }}'
                            ]),
                            new NotBlank()
                        ],
                    ])
                    ->add('template', TextType::class, [
                        'required' => false
                    ])
                    ->add(
                        $builder->create('pixels', FormType::class, $this->groupOpt)
                            ->add('facebook', TextType::class, [
                                'required' => false
                            ])
                            ->add('twitter', TextType::class, [
                                'required' => false
                            ])
                            ->add('pinterest', TextType::class, [
                                'required' => false
                            ])
                            ->add('linkedin', TextType::class, [
                                'required' => false
                            ])
                            ->add('google', TextType::class, [
                                'required' => false
                            ])
                            ->add('quora', TextType::class, [
                                'required' => false
                            ])
                    )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
            'allow_extra_fields' => false
        ));
    }
}
