<?php


namespace App\Service\IncomeStatistics\ReferrerIncomeStatistics\Handlers;


use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Cache\Adapter\MemcachedAdapter;

abstract class AbstractHandler
{
    /**
     * @var RegistryInterface
     */
    protected $doctrine;

    /**
     * AbstractHandler constructor.
     * @param RegistryInterface $doctrine
     */
    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine = $doctrine;
    }
}