<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Entities;

use App\Entity\DocumentReferrer\DocumentReferrer;

class ShopDocumentReferrerStatistics
{
    /**
     * @var DocumentReferrer
     */
    public $documentReferrer;

    /**
     * @var \DateTime|null
     */
    public $lastPurchaseDate;

    /**
     * @var \DateTime|null
     */
    public $lastSignUpDate;

    /**
     * @var int
     */
    public $purchaseCount;

    /**
     * @var int
     */
    public $signUpCount;

    /**
     * @var int
     */
    public $viewsCount;

    /**
     * @var float
     */
    public $signUpReferrerProfit;

    /**
     * @var float
     */
    public $purchaseReferrerProfit;

    /**
     * @var float
     */
    public $systemCommission;

    /**
     * @var float
     */
    public $purchaseTotal;

    public function __construct(
        DocumentReferrer $documentReferrer,
        \DateTime $lastPurchaseDate = null,
        \DateTime $lastSignUpDate = null,
        int $purchaseCount,
        int $signUpCount,
        int $viewsCount,
        float $signUpReferrerProfit,
        float $purchaseReferrerProfit,
        float $systemCommission,
        float $purchaseTotal
    )
    {
        $this->documentReferrer = $documentReferrer;
        $this->lastPurchaseDate = $lastPurchaseDate;
        $this->lastSignUpDate = $lastSignUpDate;
        $this->purchaseCount = $purchaseCount;
        $this->signUpCount = $signUpCount;
        $this->viewsCount = $viewsCount;
        $this->signUpReferrerProfit = $signUpReferrerProfit;
        $this->purchaseReferrerProfit = $purchaseReferrerProfit;
        $this->systemCommission = $systemCommission;
        $this->purchaseTotal = $purchaseTotal;
    }

    /**
     * @return null|string
     */
    protected function lastPurchaseDateString()
    {
        if ($this->lastPurchaseDate) {
            return $this->lastPurchaseDate->format('Y-m-d H:i:s');
        }

        return null;
    }

    /**
     * @return null|string
     */
    protected function lastSignUpDateString()
    {
        if ($this->lastSignUpDate) {
            return $this->lastSignUpDate->format('Y-m-d H:i:s');
        }

        return null;
    }

    public function toArray()
    {
        return [
            'document_referrer' => [
                'id' => $this->documentReferrer->getId(),
                'referrer' => $this->documentReferrer->getReferrer(),
            ],
            'last_purchase_date' => $this->lastPurchaseDateString(),
            'last_sign_up_date' => $this->lastSignUpDateString(),
            'purchase_count' => $this->purchaseCount,
            'sign_up_count' => $this->signUpCount,
            'views_count' => $this->viewsCount,
            'sign_up_referrer_profit' => $this->signUpReferrerProfit,
            'purchase_referrer_profit' => $this->purchaseReferrerProfit,
            'system_commission' => $this->systemCommission,
            'purchase_total' => $this->purchaseTotal,
        ];
    }
}