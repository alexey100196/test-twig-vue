<?php


namespace App\Form\Admin\ReferrerLink;


use App\Entity\ReferrerLinkCard;
use App\Entity\ReferrerLinkStoreCard;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class StoreCardContentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'constraints' => [
                    new Length(['min' => 3, 'max' => 100]),
                    new NotBlank(),
                ],
            ])
            ->add('description', null, [
                'constraints' => [
                    new Length(['min' => 3, 'max' => 10000]),
                    new NotBlank()
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReferrerLinkStoreCard::class,
        ]);
    }
}