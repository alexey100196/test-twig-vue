<?php

namespace App\Controller\Auth;

use App\Service\Referrer\ReferrerCompletion;
use App\Service\Shop\ShopCompletion;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class LoginController extends AbstractController
{
    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * LoginController constructor.
     * @param SessionInterface $session
     */
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/remember-login", name="remember_and_login", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function rememberAndLogin(Request $request)
    {
        $response = $this->redirectToRoute('login');

        if ($targetPath = $request->request->get('target_path')) {
            $response->headers->setCookie(new Cookie('loginTargetPath', $targetPath));
        }

        return $response;
    }

    /**
     * @Route("/remember-logout", name="remember_and_logout", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function rememberAndLogout(Request $request)
    {
        $response = $this->redirectToRoute('logout');

        if ($targetPath = $request->request->get('target_path')) {
            $response->headers->setCookie(new Cookie('loginTargetPath', $targetPath));
        }

        return $response;
    }

    /**
     * @Route("/login", name="login")
     * @param AuthenticationUtils $authenticationUtils
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(AuthenticationUtils $authenticationUtils, Request $request)
    {


        if ($this->getUser()) {
            return $this->redirectToRoute('logged');
        }

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
        $targetPath = $this->getLoginTargetPathFromCookie($request);

        $response = $this->render('auth/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'targetPath' => $targetPath,
        ]);

        $response->headers->clearCookie('loginTargetPath');

        return $response;
    }

    /**
     * @Route("/login/ajax", name="login_ajax")
     * @param Request $request
     * @return JsonResponse
     */
    public function loginAjax(Request $request)
    {
        return new JsonResponse(['id' => $request->cookies->get('PHPSESSID')]);
    }

    /**
     * @return mixed|null
     */
    protected function getLoginTargetPathFromCookie(Request $request)
    {
        if ($targetPath = $request->cookies->get('loginTargetPath')) {

            return $targetPath;
        }

        return null;
    }

    /**
     * @Route("/logged", name="logged")
     * @Security("has_role('ROLE_USER')")
     * @param ShopCompletion $shopCompletion
     * @param ReferrerCompletion $referrerCompletion
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logged(ShopCompletion $shopCompletion, ReferrerCompletion $referrerCompletion, Request $request)
    {
        // @todo: this method require refactoring.

        if ($this->getUser()->isAdmin()) {
            return $this->redirectToRoute('admin_submission_index');
        }

        if ($redirect = $this->subscriptionPlanChosen($request)) {
            return $redirect;
        }

        if ($shop = $this->getUser()->getShop()) {
            if (!$shopCompletion->isShopFullyCompleted($shop)) {
                return $this->redirectToRoute('account_shop_edit');
            } else {
                return $this->redirectToRoute('account_shop_home');
            }
        } else {
            if ($referrerCompletion->isReferrerFullyCompleted($this->getUser())) {
                return $this->redirectToRoute('account_referrer_home_index');
            } else {
                return $this->redirectToRoute('account_referrer_settings_billing');
            }
        }

        return $this->redirectToRoute('home_index');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function subscriptionPlanChosen(Request $request)
    {
        if ($request->cookies->has(Consts::GUEST_SUBSCRIPTION_PLAN_COOKIE_NAME)) {
            return $this->redirectToRoute('subscription_subscribe_continue');
        }

        return null;
    }

    /**
     * @Route("/logout", name="logout", methods={"GET", "POST"})
     */
    public function logout()
    {
        die('ok');
    }
}
