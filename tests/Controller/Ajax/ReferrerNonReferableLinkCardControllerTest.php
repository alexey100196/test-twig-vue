<?php

namespace App\Tests\Controller\Ajax;

use App\Entity\ReferrerLink;
use App\Entity\ReferrerMyStore;
use App\Entity\ReferrerStoreNonReferableLink;
use App\Entity\User;
use App\Repository\ReferrerMyStoreRepository;
use App\Repository\ReferrerNonReferableLinkCardRepository;
use App\Repository\ReferrerStoreNonReferableLinkRepository;
use App\Service\Referrer\ReferrerMyStoreService;
use App\Service\Referrer\ReferrerNonReferableLinkCardService;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ReferrerNonReferableLinkCardControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var ReferrerStoreNonReferableLinkRepository
     */
    protected $referrerStoreNonReferableLinkRepository;

    /**
     * @var ReferrerNonReferableLinkCardService
     */
    protected $referrerNonReferableLinkCardService;

    /**
     * @var ReferrerStoreNonReferableLink
     */
    protected $referrerStoreNonReferableLink;

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->em = $this->client->getKernel()->getContainer()->get('doctrine')
            ->getManager();

        $this->referrerNonReferableLinkCardService = $this->client->getKernel()->getContainer()->get(ReferrerNonReferableLinkCardService::class);
        $this->referrerStoreNonReferableLinkRepository = $this->client->getKernel()->getContainer()->get(ReferrerStoreNonReferableLinkRepository::class);
        $referrerStoreNonReferableLink = new ReferrerStoreNonReferableLink();
        $user = $this->em->getRepository(User::class)->findOneBy(['name' => 'john']);
        $referrerMyStore = new ReferrerMyStore();
        $referrerMyStore->setTitle('-');
        $referrerMyStore->setLink('http://marfil-app.local/store/'.bin2hex(random_bytes(8)));
        $referrerStoreNonReferableLink->setMyStore(
            $this->client->getKernel()->getContainer()->get(ReferrerMyStoreService::class)->save($referrerMyStore)
        );
        $referrerStoreNonReferableLink->setTitle('');
        $referrerStoreNonReferableLink->setDestinationUrl('http://nike.com');
        $user->setReferrerMyStore($referrerMyStore);
        $this->em->persist($referrerStoreNonReferableLink);
        $this->em->persist($user);
        $this->em->flush();
        $this->referrerStoreNonReferableLink = $referrerStoreNonReferableLink;
    }

    /**
     * @testdox Check user isn't login
     */
    public function testNotLoggedIn()
    {
        $client = static::createClient();
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $url = $client->getKernel()->getContainer()->getParameter('url');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/ajax/referrer-custom-link/'.$this->referrerStoreNonReferableLink->getId().'/card');

        $this->assertResponseRedirects($url.'/login', Response::HTTP_FOUND);
    }

    /**
     * @testdox Check user isn't login
     */
    public function testNotLoggedInCreate()
    {
        $client = static::createClient();
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $url = $client->getKernel()->getContainer()->getParameter('url');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/ajax/referrer-custom-link/card/create');

        $this->assertResponseRedirects($url.'/login', Response::HTTP_FOUND);
    }

    /**
     * @testdox Check request without data
     */
    public function testWithoutData() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', 'john'));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/ajax/referrer-custom-link/'.$this->referrerStoreNonReferableLink->getId().'/card', []);

        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"0":"The CSRF token is invalid. Please try to resubmit the form.","width":["This value should be between 200 and 1000."]}', $client->getResponse()->getContent());
    }

    /**
     * @testdox Check create request without data
     */
    public function testWithoutDataCreate()
    {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', 'john'));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/ajax/referrer-custom-link/card/create', []);

        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
        $this->assertEquals(
            '{"0":"The CSRF token is invalid. Please try to resubmit the form.","width":["This value should be between 200 and 1000."]}',
            $client->getResponse()->getContent()
        );
    }

    /**
     * @testdox Check request with data
     */
    public function testWithData() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', 'john'));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/ajax/referrer-custom-link/'.$this->referrerStoreNonReferableLink->getId().'/card', [
            'description' => "Cytryna ( Citrus limon ) Burm. - gatunek roślin z rodziny rutowatych (Rutaceae Juss.). Pochodzi z",
            'iframe' => null,
            'image' => "http://sklep6339634.homesklep.pl/environment/cache/images/500_500_productGfx_386fbc3636f6ff239500c4fdb720aed0.jpg",
            'layout' => "col",
            'logo' => "http://sklep6339634.homesklep.pl/favicon.ico",
            'name' => "homesklep",
            'showCompanyName' => true,
            'showShopLogo' => false,
            'socialMedia' => true,
            'title' => "Cytryny",
            'url' => "http://homesklep.pl",
            'width' => 530,
            '_token' => $client->getContainer()->get('security.csrf.token_manager')->getToken('referrer_non_referable_link_card')->getValue()
        ]);

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertTrue($response['success']);
    }

    /**
     * @testdox Check create request with data
     */
    public function testWithDataCreate()
    {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', 'john'));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/ajax/referrer-custom-link/card/create', [
            'description' => "Cytryna ( Citrus limon ) Burm. - gatunek roślin z rodziny rutowatych (Rutaceae Juss.). Pochodzi z",
            'iframe' => null,
            'image' => "http://sklep6339634.homesklep.pl/environment/cache/images/500_500_productGfx_386fbc3636f6ff239500c4fdb720aed0.jpg",
            'layout' => "col",
            'logo' => "http://sklep6339634.homesklep.pl/favicon.ico",
            'name' => "homesklep",
            'showCompanyName' => true,
            'showShopLogo' => false,
            'socialMedia' => true,
            'title' => "Cytryny",
            'url' => "http://homesklep.pl",
            'width' => 530,
            '_token' => $client->getContainer()->get('security.csrf.token_manager')->getToken('referrer_non_referable_link_card')->getValue()
        ]);

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertTrue($response['success']);
    }

    /**
     * @param string $filter
     * @param $value
     * @return Cookie
     */
    private function logIn(string $filter, $value)
    {
        $session = self::$container->get('session');
        $em = self::$container->get('doctrine.orm.entity_manager');

        $user = $em->getRepository(User::class)->findOneBy([$filter => $value]);

        $firewallName = 'main';

        $token = new UsernamePasswordToken($user, null, $firewallName, $user->getRoles());
        $session->set('_security_'.$firewallName, serialize($token));
        $session->save();

        return new Cookie($session->getName(), $session->getId());
    }
}
