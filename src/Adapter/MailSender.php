<?php

namespace App\Adapter;


class MailSender
{
    /**
     * @var \Swift_Mailer
     */
    private $swiftMailer;

    /**
     * MailSender constructor.
     * @param \Swift_Mailer $swiftMailer
     */
    public function __construct(\Swift_Mailer $swiftMailer)
    {
        $this->swiftMailer = $swiftMailer;
    }

    /**
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param string $body
     */
    public function send(string $from, string $to, string $subject, string $body): void
    {
        $this->swiftMailer->send((new \Swift_Message($subject))
            ->setFrom($from)
            ->setTo($to)
            ->setBody($body, 'text/html'));
    }
}
