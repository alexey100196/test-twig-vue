<?php

namespace App\Helper\WidgetCompletion\Resolvers;

use App\Helper\WidgetCompletion\Resolvers\AbstractResolver;

class EmailData extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        if ($this->widgetShopSetting->getWizardStep()) {
            $this->addScore(100);
        }
    }
}