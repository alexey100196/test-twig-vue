(function () {
    "use strict";

    function iframeEl (id) {
        this.id = id;
        this.obj = document.getElementById(this.id);
        this.objWidth = parseFloat(this.obj.width);
        this.objHeight = parseFloat(this.obj.height);
    }

    function iframeParent (iframeEl) {
        this.iframeEl = iframeEl;
        this.obj = this.iframeEl.parentNode.parentNode;
        this.objWidth = parseFloat(this.obj.offsetWidth);
    }

    function iframe (id) {
        this.id = id;
        this.$el = new iframeEl(this.id);
        this.parent = new iframeParent(this.$el.obj);
    }

    var iframeId = document.currentScript.getAttribute('data-iframeid');

    var iframe = new iframe(iframeId);
    var maxIframeWidth = iframe.$el.objWidth;
    var maxIframeHeight = iframe.$el.objHeight;

    var resizeiframe = function() {

        iframe.parent.objWidth = parseFloat(iframe.parent.obj.offsetWidth);

        if (iframe.$el.objWidth > iframe.parent.objWidth) {
            var scale = iframe.parent.objWidth / iframe.$el.objWidth;

            iframe.$el.obj.style.transform = 'scale(' + scale + ')';
        } else {
            iframe.$el.obj.style.transform = 'scale(' + 1 + ')';
        }
        iframe.parent.iframeEl.parentNode.style.height = iframe.$el.obj.getBoundingClientRect().height + "px";

    };

    resizeiframe();

    window.addEventListener('resize', function(){
        resizeiframe();
    }, true);
})();