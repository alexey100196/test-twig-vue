<?php


namespace App\Tests\Helper;


use App\Helper\Number;
use App\Helper\UrlHelpers;
use PHPUnit\Framework\TestCase;

class NumberTest extends TestCase
{
    public function testToString()
    {
        $this->assertTrue(Number::toString(2) === '2.00');
    }
}