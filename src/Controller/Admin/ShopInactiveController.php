<?php

namespace App\Controller\Admin;

use App\Entity\Currency;
use App\Entity\ShopStatus;
use App\Entity\UserBalance;
use App\Form\Admin\Shop\EditShopType;
use App\Form\Balance\UserTransactionType;
use App\Helper\DatePeriodGenerator;
use App\Repository\ShopRepository;
use App\Service\Balance\UserBalanceService;
use App\Util\Consts;
use App\ValueObject\DatePeriod;
use Carbon\Carbon;
use Knp\Component\Pager\PaginatorInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ShopInactiveController extends AbstractController
{
    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * @param ShopRepository $shopRepository
     */
    public function __construct(ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    /**
     * @Route("/admin/company-inactive/{year}/{month}", name="admin_shop_inactive_index")
     * @param int|null $year
     * @param int|null $month
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(int $year = null, int $month = null)
    {
        $datePeriod = DatePeriodGenerator::generateFromYearAndMonth($year, $month);

        $shops = $this->shopRepository
            ->findAllWithStatusBetweenDates(
                [ShopStatus::ACCEPTED_NAME, ShopStatus::SUSPENDED_NAME],
                null,
                $datePeriod->getEnd()
            );

        return $this->render('admin/shop-inactive/index.html.twig', [
            'shops' => $shops,
            'currentPeriod' => $datePeriod,
        ]);
    }
}
