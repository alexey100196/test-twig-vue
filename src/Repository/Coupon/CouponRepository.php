<?php

namespace App\Repository\Coupon;

use App\Entity\Coupon\Coupon;
use App\Entity\Coupon\CouponPackage;
use App\Entity\Shop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Coupon|null find($id, $lockMode = null, $lockVersion = null)
 * @method Coupon|null findOneBy(array $criteria, array $orderBy = null)
 * @method Coupon[]    findAll()
 * @method Coupon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CouponRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Coupon::class);
    }

    /**
     * @param Shop $shop
     * @param int $allocation
     * @return Coupon|null
     */
    public function findOneCouponByShopAndAllocation(Shop $shop, int $allocation): ?Coupon
    {
        try {
            return $this->createQueryBuilder('c')
                ->join('c.package', 'p', Join::WITH, 'c.package = p')
                ->andWhere('p.shop = :shop')
                ->andWhere('p.allocation = :allocation')
                ->andWhere('(c.used = 0 AND p.multipleUsage = 0) OR p.multipleUsage = 1')
                ->setParameter('shop', $shop)
                ->setParameter('allocation', $allocation)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findFreeCouponInCouponPackage(CouponPackage $couponPackage): ?Coupon
    {
        try {
            $qb = $this->createQueryBuilder('c')->andWhere('c.package = :package');

            if (!$couponPackage->getMultipleUsage()) {
                $qb->andWhere('c.used = 0');
            }

            return $qb->setParameter('package', $couponPackage)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
