<?php


namespace App\Tests\Service\Shop;


use App\Entity\Shop;
use App\Repository\ShopRepository;
use App\Service\Shop\ShopCookie;
use App\Util\Consts;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class ShopCookieTest extends TestCase
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ShopRepository
     */
    private $shopRepository;

    protected function setUp(): void
    {
        $this->em = $this->createMock(EntityManagerInterface::class);
        $this->shopRepository = $this->getMockBuilder(ShopRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testUpdateCookieLifeTime()
    {
        $this->em->expects($this->once())->method('persist')->with($this->isInstanceOf(Shop::class));
        $this->em->expects($this->once())->method('flush');

        $service = new ShopCookie($this->em, $this->shopRepository);

        $shop = $service->updateCookieLifeTime(new Shop(), 1, 2);

        $this->assertEquals(1, $shop->getCookieLifetime());
        $this->assertEquals(2, $shop->getBannerCookieLifetime());
    }

    public function testGetShopCookieLifeTime()
    {
        $shop = new Shop();
        $service = new ShopCookie($this->em, $this->shopRepository);

        $shop->setCookieLifetime(1);
        $this->assertEquals(1, $service->getShopCookieLifeTime($shop));

        $shop->setCookieLifetime(null);
        $this->assertEquals(Consts::SNIPPET_DEFAULT_COOKIE_LIFETIME_DAYS, $service->getShopCookieLifeTime($shop));
    }

    public function testGetShopBannerCookieLifeTime()
    {
        $shop = new Shop();
        $service = new ShopCookie($this->em, $this->shopRepository);

        $shop->setBannerCookieLifetime(1);
        $this->assertEquals(1, $service->getShopBannerCookieLifeTime($shop));

        $shop->setBannerCookieLifetime(null);
        $this->assertEquals(Consts::SNIPPET_DEFAULT_COOKIE_LIFETIME_DAYS, $service->getShopBannerCookieLifeTime($shop));
    }
}