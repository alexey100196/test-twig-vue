<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Simple Shop</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        
        <div id="buy">BUY</div>
        <div id="register">REGISTER</div>

         <script src="ValueLink.js"></script>
         <script>
            document.getElementById('buy').onclick = function(e) {
                SuperInterface.push({
                    "event" : "boughtProduct",
                    "data": {
                        "productName": "Apple iPad",
                        "price": "999",
                        "dateOfBought": "03.03.1990"
                    }
                });
            };
        </script>
    </body>
</html>