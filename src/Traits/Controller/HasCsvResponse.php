<?php

namespace App\Traits\Controller;

use Symfony\Component\HttpFoundation\Response;

trait HasCsvResponse
{
    /**
     * @param $content
     * @param array $headers
     * @param string $fileName
     * @return Response
     */
    public function csvResponse($content, $headers = [], string $fileName = 'download.csv')
    {
        $response = new Response($this->get('serializer')->encode($content, 'csv'));
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $fileName . '"');

        foreach ($headers as $name => $header) {
            $response->headers->set($name, $header);
        }

        return $response;
    }
}