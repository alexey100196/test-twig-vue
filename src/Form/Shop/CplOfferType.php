<?php

namespace App\Form\Shop;

use App\Form\Type\OfferType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;

class CplOfferType extends OfferType
{
    /**
     * @return null
     */
    public function getBlockPrefix()
    {
        return \App\Entity\OfferType::CPL;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $commissionOptions = [
            'required' => true,
            'constraints' => [
                new GreaterThan(['value' => 0]),
            ],
        ];

        if (!$options['validate_min_amount']) {
            unset($commissionOptions['constraints']);
        }

        $builder->add('commission', NumberType::class, $commissionOptions);
    }
}
