<?php

namespace App\Event\Conversion;

use Symfony\Component\EventDispatcher\Event;

class ConversionTimeForAcceptanceExpired extends Event
{
    /** @var string */
    const NAME = 'conversion.time-for-acceptance-expired';

    /**
     * @var \DateTime
     */
    private $start;

    /**
     * @var \DateTime
     */
    private $end;

    public function __construct(\DateTime $start, \DateTime $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @return \DateTime
     */
    public function getStart(): \DateTime
    {
        return $this->start;
    }

    /**
     * @return \DateTime
     */
    public function getEnd(): \DateTime
    {
        return $this->end;
    }
}