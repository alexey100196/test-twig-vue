<?php


namespace App\Service\Referrer;


use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class ReferrerNickname
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * ReferrerNickname constructor.
     * @param EntityManagerInterface $em
     * @param UserRepository $userRepository
     */
    public function __construct(EntityManagerInterface $em, UserRepository $userRepository)
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $user
     * @return User
     */
    public function updateNickname(User $user)
    {
        $number = null;

        while ($this->userRepository->findNickNameExceptUser($user, $nickname = $this->generateNickname($user, $number))) {
            $number = $number === null ? 1 : $number + 1;
        }

        $user->setNickname($nickname);
        $this->em->flush();

        return $user;
    }

    /**
     * @param User $user
     * @param int|null $number
     * @return string
     */
    private function generateNickname(User $user, int $number = null)
    {
        $nickname = $user->getName() . '.' . $user->getSurname();
        $nickname = strtolower($nickname);

        return $nickname . ($number ? $number : '');
    }
}