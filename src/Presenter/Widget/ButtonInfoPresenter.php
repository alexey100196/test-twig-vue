<?php

namespace App\Presenter\Widget;

use App\Entity\Campaign;
use App\Entity\Shop;
use App\Entity\User;
use App\Entity\Widget\WidgetShopSetting;

class ButtonInfoPresenter
{
    /**
     * @var Shop
     */
    private $shop;

    /**
     * @var User|null
     */
    private $user;

    /**
     * @var Campaign|null
     */
    private $campaign;

    public function __construct(Shop $shop, ?User $user = null, ?Campaign $campaign = null)
    {
        $this->shop = $shop;
        $this->user = $user;
        $this->campaign = $campaign;
    }

    public function toArray(): array
    {
        $buttonSettings = $this->shop->getWidgetButton();
        $widgetShopSetting = $this->shop->getWidgetSetting();
        $campaignWidgetShopSetting = $this->campaign->getWidgetShopSetting();

        $isButtonVisible = $this->getIsButtonVisible();
        $canEnableWidget = $campaignWidgetShopSetting->couponRewardsEnabled() || $campaignWidgetShopSetting->cashRewardsEnabled();

        $language = $widgetShopSetting->getLanguage() ? $widgetShopSetting->getLanguage()->getCode() : 'en';

        return [
            'background_size' => $buttonSettings->getBackgroundSize(),
            'icon_size' => $buttonSettings->getIconSize(),
            'is_hide_background' => $buttonSettings->isHideBackgroundColor(),
            'background_color' => $buttonSettings->getBackgroundColor(),
            'text_color' => $buttonSettings->getTextColor(),
            'times_referred' => (int)$buttonSettings->getShowNumberOfReferrals(),
            'image' => $buttonSettings->getIcon() ? '/images/widget/' . $buttonSettings->getIcon()->getImage() : null,
            'position' => $buttonSettings->getPosition(),
            'coords' => $buttonSettings->getCoordsArray(),
            'enabled' => $canEnableWidget && $isButtonVisible,
            'sonar' => (bool)$buttonSettings->getBlinkEffect(),
            'heading_text' => $language === 'en' ? 'Reffer & Get Rewards' : 'Polecaj & Otrzymuj nagrody',
        ];
    }

    private function getIsButtonVisible(): bool
    {
        if ($this->user !== null) {
            return false;
        } else {
            return true;
        }
    }
}