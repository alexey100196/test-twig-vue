<?php


namespace App\Tests\Service\IncomeStatistics\ShopIncomeStatistics\Commands;


use App\Entity\Shop;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateSummaryStatistics;
use PHPUnit\Framework\TestCase;

class GenerateSummaryStatisticsTest extends TestCase
{
    public function testCommand()
    {
        $shop = new Shop();

        $command = new GenerateSummaryStatistics($shop);

        $this->assertEquals($shop, $command->getShop());
    }
}