<?php


namespace App\Manager;


use App\Entity\EventEntry;

class EventEntryManager extends AbstractManager
{
    public function save(EventEntry $eventEntry): EventEntry
    {
        $this->entityManager->persist($eventEntry);
        $this->entityManager->flush();

        return $eventEntry;
    }
}
