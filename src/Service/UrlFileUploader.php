<?php


namespace App\Service;


class UrlFileUploader
{
    /**
     * @var string
     */
    private $targetDirectory;

    /**
     * UrlFileUploader constructor.
     * @param $targetDirectory
     */
    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    /**
     * @param string $url
     * @param array $allowedMimeTypes
     * @return string
     * @throws \Exception
     */
    public function upload(string $url, array $allowedMimeTypes = [])
    {
        if (!$stream = @fopen($url, 'r')) {
            throw new \Exception('Unreachable url');
        }

        $temporaryFile = tempnam(sys_get_temp_dir(), 'marfil');
        file_put_contents($temporaryFile, $stream);

        $this->guardAgainstInvalidMimeType($temporaryFile, $allowedMimeTypes);

        $filename = md5(uniqid());
        $mediaExtension = explode('/', mime_content_type($temporaryFile));
        $filename = "{$filename}.{$mediaExtension[1]}";

        $target = $this->targetDirectory . '/' . $filename;

        copy($temporaryFile, $target);
        @chmod($target, 0666 & ~umask());

        return $filename;
    }

    /**
     * @param string $file
     * @param array $allowedMimeTypes
     * @throws \Exception
     */
    protected function guardAgainstInvalidMimeType(string $file, array $allowedMimeTypes = [])
    {
        if (empty($allowedMimeTypes)) {
            return;
        }

        if (!in_array(mime_content_type($file), $allowedMimeTypes)) {
            throw new \Exception('Invalid file.');
        }
    }
}