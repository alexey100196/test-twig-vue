<?php


namespace App\Helper;


class UrlHelpers
{
    /**
     * @param string $url
     * @param string $name
     * @param $value
     * @return string
     */
    public static function addGetParam(string $url, string $name, $value)
    {
        $query = parse_url($url, PHP_URL_QUERY);
        $glue = $query ? '&' : '?';

        return $url . $glue . $name . '=' . $value;
    }

    /**
     * @param string $left
     * @param string $right
     * @param bool $secure
     * @return bool
     */
    public static function compareUrls(string $left, string $right, bool $secure = false)
    {
        $left = parse_url($left);
        $right = parse_url($right);

        if (($left['host'] ?? '') && ($right['host'] ?? '')) {
            if ($secure && $left['scheme'] !== $right['scheme']) {
                return false;
            }

            return $left['host'] === $right['host'];
        }

        return false;
    }

    public static function hasScheme(string $url)
    {
        return strpos($url, 'http://') === 0 || strpos($url, 'https://') === 0;
    }
}