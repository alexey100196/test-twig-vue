<?php /**/?><!--
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>SNIPPET</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
        <script src="ValueLink.js"></script>
        <script>
            const SuperInterfaceID = "D885F3037EFEACD02ABD";
        </script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <style>
        
            body{
                display: flex;
                background: #c6c6c6;
                align-items: center;
                justify-content: center;
            }

            body > *{
                padding: 25px;
                width: 50%;
                border: 1px solid white;
                border-radius: 5px;
            }

            .purchase,
            .signup{
                display: flex;
                justify-content: center;
                align-items: center;
                flex-direction: column;
                margin:0 15px;
            }

            .purchase input[type='text'],
            .signup input[type='text']{
                width: 50%;
                height: 35px;
                margin: 5px 0;
                padding: 0 10px;
                border-radius: 15px;
                border: 1px solid white;
            }

            .purchase div,
            .signup div{
                padding: 5px 15px;
                border: 1px solid white;
                background-color: lightgray;
                cursor: pointer;
                margin-top: 15px;
            }
        
        </style>


        <div class="purchase">
            <input type="text" placeholder="Product" id="pProduct">
            <input type="checkbox" id="pTest">Test
            <input type="text" placeholder="Amount" id="pAmount">
            <input type="text" placeholder="Price" id="pPrice">

            <div id="buy-product">BUY PRODUCT</div>
        </div>


        <div class="signup">

            <input type="checkbox" id="sTest">Test
            <div id="register">REGISTER</div>
        </div>
        
        <script>
            document.getElementById('buy-product').onclick = function(e) {

                var pProduct = document.querySelector('#pProduct').value;
                var pAmount = document.querySelector('#pAmount').value;
                var pPrice = document.querySelector('#pPrice').value;
                var pTest = document.querySelector('#pTest').checked;

                SuperInterface.push({
                    "event" : "purchase",
                    "test" : pTest,
                    "data": {
                        "product": pProduct,
                        "price": pPrice,
                        "amount": pAmount
                    }
                });
            };

            document.getElementById('register').onclick = function(e) {
                var sTest = document.querySelector('#sTest').checked;
                SuperInterface.push({
                    "event" : "signup",
                    "test" : sTest
                });
            };
        </script>
    </body>
</html>-->