<?php

namespace App\Repository\Coupon;

use App\Entity\Coupon\CouponPackage;
use App\Entity\Shop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CouponPackage|null find($id, $lockMode = null, $lockVersion = null)
 * @method CouponPackage|null findOneBy(array $criteria, array $orderBy = null)
 * @method CouponPackage[]    findAll()
 * @method CouponPackage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CouponPackageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CouponPackage::class);
    }

    /**
     * @param Shop $shop
     * @param bool $multipleUsage
     * @param int $allocation
     * @return mixed
     */
    public function findByShopAndMultipleUsageAndAllocation(Shop $shop, bool $multipleUsage, int $allocation)
    {
        $qb = $this->createQueryBuilder('cp');
        $qb->where('cp.shop = :shop');
        $qb->andWhere('cp.multipleUsage = :multipleUsage');
        $qb->andWhere('cp.allocation = :allocation');

        $qb->setParameters([
            'shop' => $shop,
            'multipleUsage' => (int) $multipleUsage,
            'allocation' => $allocation
        ]);

        $qb->leftJoin('cp.referrerWidgetShopSettings', 'rwss')
            ->leftJoin('cp.inviteeWidgetShopSettings', 'iwss')
            ->addOrderBy('rwss.id', 'DESC')
            ->addOrderBy('iwss.id', 'DESC');

        return $qb->getQuery()->getResult();
    }
}
