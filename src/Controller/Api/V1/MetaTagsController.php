<?php


namespace App\Controller\Api\V1;

use Embed\Embed;
use App\Controller\Api\ApiController;
use App\Repository\ShopRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MetaTagsController extends ApiController
{
    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * CheckEmailController constructor.
     * @param ShopRepository $shopRepository
     */
    public function __construct(ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    /**
     * @Route("/api/v1/meta", name="api_v1_check_meta")
     * @param Request $request
     * @return JsonResponse
     */
    public function checkMeta(Request $request)
    {
        $link = $request->get('link', null);
        $headers = [];

        $validator = $this->container->get('validator');
        $constraints = array(
            new \Symfony\Component\Validator\Constraints\Url(),
            new \Symfony\Component\Validator\Constraints\NotBlank()
        );

        $violations = $validator->validate($link, $constraints);

        if ($violations->count() > 0) {
            return $this->validationErrorResponse([
                'link' => $violations->get(0)->getMessage()
            ]);
        }

        try {
            $linkMeta = $this->getSiteOg($link);
        } catch (\Exception $e) {
            return $this->validationErrorResponse([
                'link' => is_string($e) ? $e : $e->getMessage()
            ]);
        }

        return $this->makeResponse($linkMeta, Response::HTTP_OK, $headers);
    }

    private function getSiteOG( $url ){
        $info = Embed::create($url);
        $data = [
            "title" => $info->title,
            "image" => $info->image,
            "iframe" => $info->code,
            "description" => $info->description,
            "keywords" => $info->tags,
            "type" => $info->type,
            "url" => $info->url,
            "company" => $info->providerName,
            "companyUrl" => $info->providerUrl,
            "companyLogo" => $info->providerIcon,
        ];

        return $data;
    }
}
