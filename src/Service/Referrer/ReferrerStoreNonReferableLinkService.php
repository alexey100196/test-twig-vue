<?php


namespace App\Service\Referrer;


use App\Entity\ReferrerMyStore;
use App\Entity\ReferrerStoreNonReferableLink;
use Doctrine\ORM\EntityManagerInterface;

class ReferrerStoreNonReferableLinkService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ReferrerStoreNonReferableLink $referrerStoreNonReferableLink
     * @return ReferrerStoreNonReferableLink
     */
    public function save(ReferrerStoreNonReferableLink $referrerStoreNonReferableLink)
    {
        $this->em->persist($referrerStoreNonReferableLink);
        $this->em->flush();

        return $referrerStoreNonReferableLink;
    }

    /**
     * @param ReferrerStoreNonReferableLink $referrerStoreNonReferableLink
     * @return ReferrerStoreNonReferableLink
     */
    public function create(ReferrerStoreNonReferableLink $referrerStoreNonReferableLink)
    {
        return $this->save($referrerStoreNonReferableLink);
    }

    /**
     * @param ReferrerStoreNonReferableLink $referrerStoreNonReferableLink
     * @return bool
     */
    public function remove(ReferrerStoreNonReferableLink $referrerStoreNonReferableLink)
    {
        $this->em->remove($referrerStoreNonReferableLink);
        $this->em->flush();

        return true;
    }

    public function saveItemsFromArray(array $newData, array $updatedData) {
        if ($updatedCount = count($updatedData) > $newCount = count($newData)) {
            for (;$newCount<$updatedCount;$newCount++) {
                $this->remove($updatedData[$newCount]);
            }
            array_splice($updatedData, count($newData));
        }

        foreach($updatedData as $link) {
            $this->save($link);
        }
    }

    /**
     * @param array $nonReferableLinks
     * @return array
     */
    public function loadItemsToArray(array $nonReferableLinks = []) {
        $results = [];
        foreach($nonReferableLinks as $link) {
            array_push($results, $this->convertItemToArray($link));
        }

        return $results;
    }

    /**
     * @param ReferrerStoreNonReferableLink $link
     * @return array
     */
    public function convertItemToArray(ReferrerStoreNonReferableLink $link) {
        return [
            'id' => (string)$link->getId(),
            'destinationUrl' => (string)$link->getDestinationUrl(),
            'title' => (string)$link->getTitle(),
            'layout' => (string)$link->getlayout(),
            'image' => (string)$link->getImage(),
            'video' => (string)$link->getVideo(),
            'customTitle' => (string)$link->getCustomTitle(),
        ];
    }
}