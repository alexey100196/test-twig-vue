<?php

namespace App\DataFixtures;

use App\Entity\CommissionType;
use App\Entity\DocumentReferrer\DocumentReferrer;
use App\Entity\Offer;
use App\Entity\OfferType;
use App\Entity\Product;
use App\Entity\Role;
use App\Entity\Shop;
use App\Entity\ShopStatus;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppDemoFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param string $class
     * @param string $name
     * @return mixed
     */
    private function getGlossaryReference(string $class, string $name)
    {
        return $this->getReference($class . '-' . $name);
    }

    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $this->loadReferrers($manager);
        $this->loadShop($manager);
    }

    private function loadReferrers(ObjectManager $manager)
    {
        $documentReferrer = (new DocumentReferrer('Others'));
        $manager->persist($documentReferrer);

        foreach ($this->getReferrersData() as [$email, $name, $surname, $nickname, $password]) {
            $user = (new User())
                ->setEmail($email)
                ->setName($name)
                ->setSurname($surname)
                ->setNickname($nickname)
                ->setTermsAcceptedAt(new \DateTime())
                ->setRole($this->getGlossaryReference(Role::class, Role::ROLE_REFERRER));

            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
            $user->setEmailConfirmedAt(new \DateTime());

            $manager->persist($user);
        }

        $manager->flush();
    }

    private function loadShop(ObjectManager $manager)
    {
        $user = (new User())
            ->setEmail('john.shop@example.com')
            ->setRole($this->getGlossaryReference(Role::class, Role::ROLE_SHOP))
            ->setTermsAcceptedAt(new \DateTime())
            ->setPhone('+48 123 123 123')
            ->setEmailConfirmedAt(new \DateTime());

        $user->setPassword($this->passwordEncoder->encodePassword($user, 'secret'));

        $cpsOffer = (new Offer())
            ->setType($this->getGlossaryReference(OfferType::class, OfferType::CPS))
            ->setCommissionType($this->getGlossaryReference(CommissionType::class, CommissionType::PERCENT))
            ->setCommission(1);

        $cplOffer = (new Offer())
            ->setType($this->getGlossaryReference(OfferType::class, OfferType::CPL))
            ->setCommissionType($this->getGlossaryReference(CommissionType::class, CommissionType::FIXED))
            ->setCommission(5);

        $shop = (new Shop())
            ->setUser($user)
            ->setName('Demo')
            ->setWebsite('https://google.com/')
            ->setCookieLifetime(30)
            ->setUrlName('demo')
            ->setBannerCookieLifetime(40)
            ->setSystemCommission(1)
            ->setStatus($this->getGlossaryReference(ShopStatus::class, ShopStatus::ACTIVE_NAME))
            ->setCpsOffer($cpsOffer)
            ->setCplOffer($cplOffer)
            ->setDescription('Apple Computers')
            ->setService('Macbook, iPhone');
        $shopLogo = new UploadedFile(dirname(__FILE__).'/../../public/images/about.jpg', 'logo.jpg');
        $base64 = base64_encode(file_get_contents($shopLogo));
        $logoMimeType = $shopLogo->getMimeType();
        $shop->setLogo('data:'.$logoMimeType.';base64,'.$base64);

        $productOffer = (new Offer())
            ->setType($this->getGlossaryReference(OfferType::class, OfferType::CPS))
            ->setCommissionType($this->getGlossaryReference(CommissionType::class, CommissionType::PERCENT))
            ->setCommission(2);

        $product = (new Product())
            ->setName('Macbook Pro 2017')
            ->setDescription('Apple Computer')
            ->setWebsite('macbook-pro')
            ->setCpsOffer($productOffer);

        $shop->addProduct($product);

        $manager->persist($user);
        $manager->persist($shop);

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['demo'];
    }

    public function getReferrersData()
    {
        return [
            // email, name, surname, nickname, password
            ['john.referrer@example.com', 'John', 'Referrer', 'john', 'secret'],
            ['kate.referrer@example.com', 'Kate', 'Referrer', 'kate', 'secret'],
        ];
    }

    public function getDependencies()
    {
        return array(
            CommisionTypeFixtures::class,
            CurrencyFixtures::class,
            EventEntryTypeFixtures::class,
            OfferTypeFixtures::class,
            RoleFixtures::class,
            ShopStatusFixtures::class,
            TransactionTypeFixtures::class,
        );
    }
}
