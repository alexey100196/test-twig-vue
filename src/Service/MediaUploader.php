<?php

namespace App\Service;

use App\Entity\Media;
use App\Service\FileUploader\FileUploader;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class MediaUploader
{
    /**
     * @var FileUploader
     */
    private $fileUploader;

    /**
     * @param FileUploader $fileUploader
     */
    public function __construct(FileUploader $fileUploader)
    {
        $this->fileUploader = $fileUploader;
    }

    /**
     * Upload media file.
     * @param Media $media
     * @return Media
     */
    public function upload(Media $media): Media
    {
        $fileName = md5(uniqid()) . '.' . $media->getFile()->guessExtension();
        $media->setFileName($fileName);
        $media->setFilesystem('public');
        return $media;
    }
}
