<?php

namespace App\Service\Shop;

use App\Entity\PushNotification;
use App\Entity\Shop;
use Doctrine\ORM\EntityManagerInterface;

class ShopRegistrationPushNotifications
{
    const NOTIFICATIONS = [
/*        [
            '/images/trophy.png',
            ':people people <strong>signed up for trial</strong> at :company in last :days days',
            '{"people":20,"days":10}',
            'mainpage'
        ],
        [
            '/images/trophy.png',
            ':people <strong>people are visiting</strong> :company page right now',
            '{"people":40}',
            'mainpage'
        ],
        [
            '/images/save-money.png',
            ':people people referred this product and got <strong>:amount$ OFF</strong> coupon code in last :days days',
            '{"people": 70, "amount": 20, "days": 3}',
            'landingpage'
        ],
        [
            '/images/save-money.png',
            '<strong>€:amount commission</strong> was paid out via :company in past :days days',
            '{"people": 30, "amount": 45000, "days": 5}',
            'landingpage'
        ],
        [
            '/images/trophy.png',
            ':people <strong>people purchased year plan</strong> for trial at :company in last :days Days',
            '{"people":30,"days":10}',
            'mainpage'
        ],
        [
            '/images/trophy.png',
            'Joanna from Warsaw <strong>purchased 12 months</strong> plan at :company :daysh ago',
            '{"people":30,"days":4}',
            'mainpage'
        ],
        [
            '/images/trophy.png',
            'Someone from New York <strong>purchased 3 months</strong> plan at :company  :daysh ago',
            '{"people":30,"days":2}',
            'mainpage'
        ],*/
    ];

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function create(Shop $shop)
    {
       /* foreach (self::NOTIFICATIONS as $notification) {
            $pushNotification = new PushNotification();
            $pushNotification->setUser($shop->getUser());
            $pushNotification->setIcon($notification[0]);
            $pushNotification->setText($notification[1]);
            $pushNotification->setTextVariables($notification[2]);
            $pushNotification->setType($notification[3]);

            $this->entityManager->persist($pushNotification);
        }*/
    }
}