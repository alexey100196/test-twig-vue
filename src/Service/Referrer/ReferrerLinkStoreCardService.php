<?php


namespace App\Service\Referrer;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ReferrerLinkStoreCardService
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var UrlGeneratorInterface */
    private $router;

    public function __construct(
        EntityManagerInterface $em,
        UrlGeneratorInterface $router
    )
    {
        $this->em = $em;
        $this->router = $router;
    }

    /**
     * @param ReferrerLinkStoreCardService $card
     * @throws \Exception
     */
    public function save(\App\Entity\ReferrerLinkStoreCard $card)
    {
        $this->em->persist($card);
        $this->em->flush();
    }

    public function saveItemsFromArray(array $updatedData) {
        foreach($updatedData as $card) {
            $this->save($card);
        }
    }

    /**
     * @param User $user
     * @param array $myStoreReferrerLinks
     * @return array
     */
    public function loadItemsToArray(User $user, $myStoreReferrerLinks = []) {
        $results = [];
        foreach($myStoreReferrerLinks as $link) {
            $shop = $link->getShop();
            $storeCard = $link->getStoreCard();
            $referrerCoupon = $user->getReferrerCouponByShop($shop);

            if ($link && $link->getShowInMyStore() && $shop) {
                array_push($results, [
                    'shop' => [
                        'id' => $shop->getId(),
                        'name' => $shop->getName()
                    ],
                    'url' => $this->router->generate('home_reflink_mystore', [
                        'shopName' => $shop->getUrlName(),
                        'nickname' => $link->getNickname(),
                        'reflinkSlug' => $link->getSlug()
                    ]),
                    'referrerCoupon' => [
                        'myStoreEnabledAndNotExpired' => $referrerCoupon ? $referrerCoupon->getMyStoreEnabledAndNotExpired() : null,
                        'valueWithType' => $referrerCoupon ? $referrerCoupon->getValueWithType() : null
                    ],
                    'showInMyStoreAsCard' => $link->getShowInMyStoreAsCard(),
                    'storeCard' => [
                        'title' => $storeCard ? $storeCard->getTitle() : null,
                        'description' => $storeCard ? $storeCard->getDescription() : null,
                        'image' => $storeCard ? $storeCard->getImage() : 'https://via.placeholder.com/400x400.png?text=ValueLink',
                        'imageWidth' => $storeCard ? $storeCard->getImageWidth() : null,
                    ],
                    'referrerCouponValue' => $referrerCoupon ? $referrerCoupon->getValueWithType() : null,
                    'destinationUrl' => $link->getDestinationUrl() ? $link->getDestinationUrl()->getProtocol().'://'.$link->getDestinationUrl()->getUrl() : '',
                    'orderColumn' => $storeCard ? $storeCard->getOrderColumn() : 0
                ]);
            }
        }

        return $results;
    }
}