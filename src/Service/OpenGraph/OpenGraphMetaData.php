<?php


namespace App\Service\OpenGraph;

class OpenGraphMetaData
{
    /**
     * @var int (in seconds)
     */
    protected $connectTimeout = 3;

    /**
     * @var array
     */
    protected $tags = [];

    /**
     * @param $url
     * @return null|\Psr\Http\Message\StreamInterface
     */
    protected function getContent($url)
    {
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $url, ['connect_timeout' => $this->connectTimeout]);
            $content = $response->getBody();
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return null;
        }

        return $content;
    }

    /**
     * @param $content
     */
    protected function extractTagsFromContent($content)
    {
        $doc = new \DomDocument();
        @$doc->loadHTML($content);
        $xpath = new \DOMXPath($doc);
        $query = '//*/meta';
        $metas = $xpath->query($query);
        $this->tags = [];
        foreach ($metas as $meta) {
            $property = $meta->getAttribute('property');
            $content = $meta->getAttribute('content');
            if (!empty($property) && preg_match('#^og:#', $property)) {
                $this->tags[$property][] = $content;
            }
        }
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    private function getTagByName(string $name)
    {
        if (isset($this->tags[$name])) {
            if (count($this->tags[$name]) === 1) {
                return $this->tags[$name][0];
            }

            return $this->tags[$name];
        }

        return null;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function fetch(string $url)
    {
        if ($content = $this->getContent($url)) {
            $this->extractTagsFromContent($content);
        }

        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getImage()
    {
        if ($ogImage = $this->getTagByName('og:image')) {
            if (is_array($ogImage)) {
                $ogImage = $ogImage[0];
            }

            return filter_var($ogImage, FILTER_VALIDATE_URL) ? $ogImage : null;
        }

        return null;
    }

    public function getTitle()
    {
        if ($ogTitle = $this->getTagByName('og:title')) {
            if (is_array($ogTitle)) {
                $ogTitle = $ogTitle[0];
            }

            return trim((string)$ogTitle);
        }
    }

    public function getDescription()
    {
        if ($ogDescription = $this->getTagByName('og:description')) {
            if (is_array($ogDescription)) {
                $ogDescription = $ogDescription[0];
            }

            return trim((string)$ogDescription);
        }
    }
}