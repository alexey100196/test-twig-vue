<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Commands;


use App\Entity\Shop;
use App\Service\IncomeStatistics\CommandInterface;

class GenerateSummaryStatistics implements CommandInterface
{
    /**
     * @var Shop
     */
    private $shop;

    /**
     * GenerateSummaryStatistics constructor.
     * @param Shop $shop
     */
    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }
}