<?php

namespace App\Controller\Account\Shop;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ShopPayoutController
 *
 * @Security("has_role('ROLE_SHOP')")
 */
class ShopPayoutController extends AbstractController
{
    /**
     * @Route("/account/company/payout", name="account_shop_payout")
     */
    public function index()
    {
        return $this->render('account/shop/payout/index.html.twig');
    }
}