<?php


namespace App\Controller\Account\Referrer;


use App\Entity\ReferrerLink;
use App\Entity\ReferrerLinkCard;
use App\Entity\ReferrerMyStore;
use App\Entity\Shop;
use App\Form\Referrer\ReferrerLinkCardType;
use App\Repository\ReferrerLinkRepository;
use App\Repository\ReferrerStoreNonReferableLinkRepository;
use App\Service\OpenGraph\OpenGraphMetaData;
use App\Service\Referrer\ReferrerLinkCardService;
use App\Service\Referrer\ReferrerMyStoreService;
use App\Service\Referrer\ReferrerShop;
use App\Service\Referrer\ReferrerStoreNonReferableLinkService;
use App\Service\Referrer\RegenerateReferrerLinkCardContent;
use App\Traits\Controller\FormResponseArray;
use App\Util\Consts;
use App\ValueObject\DatePeriod;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class ReferrerShopCardController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerShopCardController extends AbstractController
{
    use FormResponseArray;

    /**
     * @var ReferrerMyStoreService
     */
    private $referrerMyStoreService;

    /**
     * @var ReferrerLinkCardService
     */
    private $referrerLinkCardService;

    /**
     * @var OpenGraphMetaData
     */
    private $graphMetaData;

    /**
     * @var ReferrerShop
     */
    private $referrerShop;

    /**
     * @var ReferrerLinkRepository
     */
    private $referrerLinkRepository;

    /**
     * @var ReferrerStoreNonReferableLinkRepository
     */
    private $referrerStoreNonReferableLinkRepository;

    /**
     * @var ReferrerStoreNonReferableLinkService
     */
    private $referrerStoreNonReferableLinkService;

    /**
     * @var RegenerateReferrerLinkCardContent
     */
    private $regenerateReferrerLinkCardContent;

    /**
     * ReferrerShopCardController constructor.
     * @param ReferrerMyStoreService $referrerMyStoreService
     * @param ReferrerLinkCardService $referrerLinkCardService
     * @param OpenGraphMetaData $graphMetaData
     * @param ReferrerShop $referrerShop
     * @param ReferrerLinkRepository $referrerLinkRepository
     * @param ReferrerStoreNonReferableLinkRepository $referrerStoreNonReferableLinkRepository
     * @param ReferrerStoreNonReferableLinkService $referrerStoreNonReferableLinkService
     * @param RegenerateReferrerLinkCardContent $regenerateReferrerLinkCardContent
     */
    public function __construct(
        ReferrerMyStoreService $referrerMyStoreService,
        ReferrerLinkCardService $referrerLinkCardService,
        OpenGraphMetaData $graphMetaData,
        ReferrerShop $referrerShop,
        ReferrerLinkRepository $referrerLinkRepository,
        ReferrerStoreNonReferableLinkRepository $referrerStoreNonReferableLinkRepository,
        ReferrerStoreNonReferableLinkService $referrerStoreNonReferableLinkService,
        RegenerateReferrerLinkCardContent $regenerateReferrerLinkCardContent
    )
    {
        $this->referrerMyStoreService = $referrerMyStoreService;
        $this->referrerLinkCardService = $referrerLinkCardService;
        $this->graphMetaData = $graphMetaData;
        $this->referrerShop = $referrerShop;
        $this->referrerLinkRepository = $referrerLinkRepository;
        $this->referrerStoreNonReferableLinkRepository = $referrerStoreNonReferableLinkRepository;
        $this->referrerStoreNonReferableLinkService = $referrerStoreNonReferableLinkService;
        $this->regenerateReferrerLinkCardContent = $regenerateReferrerLinkCardContent;
    }

    /**
     * @param null $year
     * @param null $month
     * @return DatePeriod
     */
    private function buildDatePeriodFromYearAndMonth($year = null, $month = null): DatePeriod
    {
        $year = $year ? intval($year) : date('Y');
        $month = $month ? intval($month) : date('m');

        return new DatePeriod(
            Carbon::create($year, $month)->startOfMonth(),
            Carbon::create($year, $month)->endOfMonth()
        );
    }

    /**
     * @Route("/account/referrer/company/{shop}/card/{referrerLink}/edit", name="account_referrer_shop_card_edit")
     * @param Shop $shop
     * @param ReferrerLink $referrerLink
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Shop $shop, ReferrerLink $referrerLink, Request $request)
    {
        $userShop = $this->referrerShop->findReferrerShopWithStats($this->getUser(), $shop);

        if (!$userShop) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        $form = $this->createForm(ReferrerLinkCardType::class, $card = $referrerLink->getCardOrCreate());
        /** @var OpenGraphMetaData $og */
        $og = $this->graphMetaData->fetch((string) $referrerLink->getDestinationUrl());

        if (!$card->getTitle() && $card->isNew() && $ogTitle = $og->getTitle()) {
            $card->setTitle($ogTitle);
        }

        $ogImage = $og->getImage();

        return $this->render('account/referrer/shop/show-card.html.twig', [
            'referrerLink' => $referrerLink,
            'referrerLinkCard' => $this->referrerLinkCardService->convertObjectToArray($card),
            'ogImage' => $ogImage,
            'form' => $form->createView(),
            'shop' => $shop,
            'item' => $userShop,
        ]);
    }

    /**
     * @Route("/account/referrer/company/{shop}/card/{year}/{month}", name="account_referrer_shop_card_show")
     * @param Shop $shop
     * @param int|null $year
     * @param int|null $month
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(Shop $shop, int $year = null, int $month = null)
    {
        $user = $this->getUser();
        $myStore = $user->getReferrerMyStore();
        $datePeriod = $this->buildDatePeriodFromYearAndMonth($year, $month);
        $referrerLinks = $this->referrerLinkRepository
            ->findByReferrerAndCardCreationDateRange(
                $user,
                $datePeriod->getStart(),
                $datePeriod->getEnd(),
                $shop,
                false
            );

        if (!$myStore) {
            $myStore = new ReferrerMyStore();
            $this->referrerMyStoreService->generate($user, $myStore);
        }

        $customLinks = $this->referrerStoreNonReferableLinkRepository->findBy([
            'mystore' => $myStore
        ], ['orderColumn' => 'ASC', 'updatedAt' => 'DESC']);

        $userShop = $this->referrerShop->findReferrerShopWithStats($user, $shop);

        if (!$userShop) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        return $this->render('account/referrer/shop/show-cards.html.twig', [
            'shop' => $shop,
            'item' => $userShop,
            'referrerLinks' => $referrerLinks,
            'customLinks' => $this->referrerStoreNonReferableLinkService->loadItemsToArray($customLinks),
            'currentPeriod' => $datePeriod,
        ]);
    }
}