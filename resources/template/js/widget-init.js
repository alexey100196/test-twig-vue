//editable
class widgetIframe {
    constructor() {
        this.id = ValueLinkID;
        this.url = document.getElementsByName('app_url')[0].content + "/";
        //this.url = 'http://marfil.junior.dev.dige.com.pl/';
        this.iframe = document.createElement('iframe');
        this.iframe.setAttribute("sandbox", "allow-storage-access-by-user-activation allow-scripts allow-same-origin")
        this.iframeWrapper = document.createElement('div');
        this.iframeWrapper.id = 'iframeWrapper';
        this.size = {
            width: "400px",
            widthWithButton: "440px",
            height: "540px"
        };
        window.addEventListener("message", event => {
            if (event.origin.startsWith(this.url)) {
                localStorage.setItem(event.data.split('=')[0], event.data.split('=')[1])
                console.log('sessionStorage set');
            }
        })
    }

    getOptions() {
        let req = new XMLHttpRequest();
        req.open('GET', this.url + 'widget/button-info/' + this.id, false);
        req.send(null);
        if (req.status === 200) {
            let parsedResponse = JSON.parse(req.response);
            if (parsedResponse.enabled === true) {
                return parsedResponse;
            } else {
                return null
            }
        }
    }

    buildIframe(data) {
        //iframe style
        this.iframe.style.width = this.size.width;
        this.iframe.style.maxWidth = '100%';
        this.iframe.style.height = 'calc(100vh - 41px)';
        this.iframe.style.maxHeight = 'calc(100vh - 41px)';
        this.iframe.style.backgroundColor = '#fff';
        this.iframe.style.marginBottom = '-1px';

        this.iframe.src = this.url + 'widget/iframe/' + this.id;

        let iframeClose = document.createElement('div');

        iframeClose.innerHTML = `<div style="background: white;border: 2px solid grey; border-bottom: 0; border-top: 0; padding: 10px 20px;display: flex;justify-content: space-between;line-height: 1.2" class="closeWrapper"><span style="color: grey; font-weight: bold; font-size: 14px">${data.heading_text}</span><span style="cursor: pointer; color: grey; font-weight: bold; font-size: 18px">x</span></div>`;
        let iFrameCloseWrapper = iframeClose.querySelector('.closeWrapper')

        iframeClose.onclick = () => {
            console.log('iframe close')

            if (window.innerWidth < 1199) {
                const scrollY = document.body.style.top;
                document.body.style.position = '';
                document.body.style.top = '';
                window.scrollTo(0, parseInt(scrollY || '0') * -1);
            }

            switch (data.position) {
                case 1: //top left
                    this.iframeWrapper.style.left = "-" + this.size.widthWithButton;
                    this.iframeWrapper.style.right = window.innerWidth*5 + "px";
                    break;
                case 3: //bottom left
                    this.iframeWrapper.style.left = "-" + this.size.widthWithButton;
                    this.iframeWrapper.style.right = window.innerWidth*5 + "px";
                    break;
                case 2: //top right
                    this.iframeWrapper.style.left = window.innerWidth*5 + "px";
                    this.iframeWrapper.style.right = "-" + this.size.widthWithButton;
                    break;
                case 4: //bottom right
                    this.iframeWrapper.style.left = window.innerWidth*5 + "px";
                    this.iframeWrapper.style.right = "-" + this.size.widthWithButton;
                    break;
            }
        };

        switch (data.position) {
            case 1: //top left
                this.iframeWrapper.style.left = "-" + this.size.widthWithButton;
                this.iframeWrapper.style.right = window.innerWidth*5 + "px";

                this.iframeWrapper.style.top = '0';
                iFrameCloseWrapper.style.borderLeft = '0';
                if (window.innerWidth < 1113) {
                    iFrameCloseWrapper.style.borderLeft = '2px solid grey';
                    iFrameCloseWrapper.style.borderRight = '2px solid grey';
                }
                break;
            case 2: //top right
                this.iframeWrapper.style.right = "-" + this.size.widthWithButton;
                this.iframeWrapper.style.left = window.innerWidth*5 + "px";
                this.iframeWrapper.style.top = '0';
                iFrameCloseWrapper.style.borderRight = '0';
                if (window.innerWidth < 1113) {
                    iFrameCloseWrapper.style.borderLeft = '2px solid grey';
                    iFrameCloseWrapper.style.borderRight = '2px solid grey';
                }
                break;
            case 3: //bottom left
                this.iframeWrapper.style.left = "-" + this.size.widthWithButton;
                this.iframeWrapper.style.right = window.innerWidth*5 + "px";

                this.iframeWrapper.style.bottom = '0';
                iFrameCloseWrapper.style.borderLeft = '0';
                if (window.innerWidth < 1113) {
                    iFrameCloseWrapper.style.borderLeft = '2px solid grey';
                    iFrameCloseWrapper.style.borderRight = '2px solid grey';
                }
                break;
            case 4: //bottom right
                this.iframeWrapper.style.right = "-" + this.size.widthWithButton;
                this.iframeWrapper.style.left = window.innerWidth*5 + "px";
                this.iframeWrapper.style.bottom = '0';
                iFrameCloseWrapper.style.borderRight = '0';
                if (window.innerWidth < 1113) {
                    iFrameCloseWrapper.style.borderLeft = '2px solid grey';
                    iFrameCloseWrapper.style.borderRight = '2px solid grey';
                }
                break;
            default:
                this.iframeWrapper.style.right = "-" + this.size.widthWithButton;
                this.iframeWrapper.style.left = window.innerWidth*5 + "px";

                this.iframeWrapper.style.bottom = '0';
                iFrameCloseWrapper.style.borderRight = '0';
                if (window.innerWidth < 1113) {
                    iFrameCloseWrapper.style.borderLeft = '2px solid grey';
                    iFrameCloseWrapper.style.borderRight = '2px solid grey';
                }
                break;
        }

        //iframe wrapper style
        this.iframeWrapper.style.position = 'fixed';
        this.iframeWrapper.style.transition = ".5s all ease-in-out";
        this.iframeWrapper.style.zIndex = 9999999;
        this.iframeWrapper.style.margin = "0";
        this.iframeWrapper.style.marginBottom = "-9px";
        this.iframeWrapper.style.height = "100%";
        this.iframeWrapper.style.minWidth = '320px';

        this.iframeWrapper.appendChild(iframeClose);
        this.iframeWrapper.appendChild(this.iframe);
        document.body.appendChild(this.iframeWrapper);
    }

    buildButton(data) {
        let button = document.createElement('div');
        button.id = 'iframeButton'

        switch (data.position) {
            case 1: //top left
                button.style.left = '30px';
                if (window.innerWidth < 1199) {
                    button.style.bottom = '30px';
                } else {
                    button.style.top = '220px';
                }
                break;
            case 2: //top right
                button.style.right = '30px';
                if (window.innerWidth < 1199) {
                    button.style.bottom = '30px';
                } else {
                    button.style.top = '220px';
                }
                break;
            case 3: //bottom left
                button.style.left = '30px';
                button.style.bottom = '30px';
                break;
            case 4: //bottom right
                button.style.right = '30px';
                button.style.bottom = '30px';
                break;
            default:
                button.style.right = '30px';
                button.style.bottom = '30px';
                break;
        }

        button.style.display = 'flex';
        button.style.justifyContent = 'flex-start';
        button.style.alignContent = 'center';
        button.style.padding = '20px';
        button.style.flexDirection = 'row';
        button.style.position = 'fixed';
        button.style.backgroundColor = data.background_color;
        button.style.zIndex = 999999;
        button.style.userSelect = 'none';
        button.style.cursor = 'pointer';
        button.style.borderRadius = '100%';
        button.style.width = '50px';
        button.style.height = '50px';
        button.style.display = 'flex';
        button.style.alignItems = 'center';
        button.style.justifyContent = 'center';
        button.style.boxSizing = 'border-box'

        button.onclick = () => {
            if (window.innerWidth < 1199) {
                document.body.style.top = `-${window.scrollY}px`;
                document.body.style.position = 'fixed';

                window.addEventListener('resize', () => {
                    if (this.iframeWrapper.style.right === "0px" && this.iframeWrapper.style.left === "0px") {
                        document.querySelector('.closeWrapper').click();
                        button.click();
                    }
                });
            }

            switch (data.position) {
                case 1: //top left
                    this.iframe.style.border = 'none';
                    this.iframe.style.borderRight = '2px solid grey';
                    this.iframeWrapper.style.left = "0px";
                    this.iframeWrapper.style.margin = "0 0";
                    this.iframeWrapper.style.right = "unset";

                    this.iframeWrapper.border = 'none';
                    this.iframeWrapper.top = 'unset';
                    this.iframeWrapper.overflowY = 'auto';
                    this.iframeWrapper.transform = 'unset';

                    if (window.innerWidth < 1199) {
                        this.iframe.style.height = "100%";
                        this.iframe.style.maxHeight = "100vh";
                        this.iframe.style.border = '2px solid grey';
                        this.iframe.style.borderTop = '0';
                        this.iframe.style.width = "400px";

                    }
                    if (window.innerWidth < 590) {
                        this.iframeWrapper.style.margin = "0 0";
                        this.iframe.style.width = "calc(100% - 0px)";
this.iframe.style.boxSizing = "border-box";
                        this.iframe.style.height = "calc(100vh - 41px)";
                        this.iframe.style.border = '2px solid grey';
                        this.iframe.style.borderTop = '0';
                        this.iframeWrapper.style.left = "0px";
                        this.iframeWrapper.style.right = "0px";
                    }
                    if (window.innerWidth < 480) {
                      this.iframeWrapper.style.right = "0px";
                      this.iframeWrapper.style.left = "0px";
                    }
                    break;
                case 3: //bottom left
                    this.iframe.style.border = 'none';
                    this.iframeWrapper.style.left = "0px";
                    this.iframeWrapper.style.margin = "0 0";
                    this.iframe.style.borderRight = '2px solid grey';
                    this.iframeWrapper.style.right = "unset";

                    this.iframeWrapper.border = 'none';
                    this.iframeWrapper.top = 'unset';
                    this.iframeWrapper.overflowY = 'auto';
                    this.iframeWrapper.transform = 'unset';

                    if (window.innerWidth < 1199) {
                        this.iframeWrapper.style.top = "0px";
                        this.iframe.style.height = "100%";
                        this.iframe.style.maxHeight = "100vh";
                        this.iframe.style.border = '2px solid grey';
                        this.iframe.style.borderTop = '0';
                        this.iframe.style.width = "400px";
                        this.iframe.style.paddingBottom = "30px";
                    }

                    if (window.innerWidth < 590) {
                        this.iframeWrapper.style.margin = "0 0";
                        this.iframeWrapper.style.left = "0px";
                        this.iframeWrapper.style.right = "0px";
                        this.iframeWrapper.style.top = "0px";
                        this.iframe.style.width = "calc(100% - 0px)";
this.iframe.style.boxSizing = "border-box";
                        // this.iframe.style.height = "calc(100vh - 41px)";
                        this.iframe.style.border = '2px solid grey';
                        this.iframe.style.borderTop = '0';
                        this.iframe.style.paddingBottom = "0";

                        this.iframe.style.width = "calc(100% - 0px)";
this.iframe.style.boxSizing = "border-box";
                        this.iframe.style.height = "calc(100vh - 41px)";
                    }

                    if (window.innerWidth < 480) {
                      this.iframeWrapper.style.right = "0px";
                      this.iframeWrapper.style.left = "0px";
                    }
                    break;
                case 2: //top right
                    this.iframe.style.border = 'none';
                    this.iframe.style.borderLeft = '2px solid grey';
                    this.iframeWrapper.style.right = "0px";
                    this.iframeWrapper.style.margin = "0 0";
                    this.iframeWrapper.style.left = "unset";

                    this.iframeWrapper.border = 'none';
                    this.iframeWrapper.top = 'unset';
                    this.iframeWrapper.overflowY = 'auto';
                    this.iframeWrapper.transform = 'unset';

                    if (window.innerWidth < 1199) {
                        this.iframe.style.height = "100%";
                        this.iframe.style.maxHeight = "100vh";
                        this.iframe.style.width = "400px";
                        this.iframe.style.paddingBottom = "30px";
                        this.iframe.style.border = '2px solid grey';
                        this.iframe.style.borderTop = '0';
                    }
                    if (window.innerWidth < 590) {
                        this.iframeWrapper.style.left = "0px";
                        this.iframeWrapper.style.right = "0px";
                        this.iframeWrapper.style.margin = "0 0";
                        this.iframe.style.width = "calc(100% - 0px)";
this.iframe.style.boxSizing = "border-box";
                        this.iframe.style.height = "calc(100vh - 41px)";
                        this.iframe.style.paddingBottom = "0";
                        this.iframe.style.border = '2px solid grey';
                        this.iframe.style.borderTop = '0';
                    }
                    if (window.innerWidth < 480) {
                      this.iframeWrapper.style.right = "0px";
                      this.iframeWrapper.style.left = "0px";
                    }
                    break;
                case 4: //bottom right
                    this.iframe.style.border = 'none';
                    this.iframe.style.borderLeft = '2px solid grey';
                    this.iframeWrapper.style.right = "0px";
                    this.iframeWrapper.style.left = "unset";
                    this.iframeWrapper.style.margin = "0 0";

                    this.iframeWrapper.border = 'none';
                    this.iframeWrapper.top = 'unset';
                    this.iframeWrapper.overflowY = 'auto';
                    this.iframeWrapper.transform = 'unset';

                    if (window.innerWidth < 1199) {
                        this.iframe.style.height = "100%";
                        this.iframe.style.maxHeight = "100vh";
                        this.iframe.style.width = "400px";
                        this.iframe.style.paddingBottom = "30px";
                        this.iframe.style.border = '2px solid grey';
                        this.iframe.style.borderTop = '0';
                    }
                    if (window.innerWidth < 590) {
                        this.iframe.style.width = "calc(100% - 0px)";
this.iframe.style.boxSizing = "border-box";
                        this.iframe.style.height = "calc(100vh - 41px)";
                        this.iframe.style.paddingBottom = "0";
                        this.iframeWrapper.style.left = "0px";
                        this.iframeWrapper.style.right = "0px";
                        this.iframe.style.border = '2px solid grey';
                        this.iframe.style.borderTop = '0';
                    }
                    if (window.innerWidth < 480) {
                      this.iframeWrapper.style.right = "0px";
                      this.iframeWrapper.style.left = "0px";
                    }
                    break;
            }
            if (window.innerWidth < 1113) {
                this.iframe.style.border = '2px solid grey';
                this.iframe.style.borderTop = '0';
            }

            if (window.innerWidth < 996) {
                document.querySelector('.closeWrapper').style.borderLeft = '2px solid grey'
                document.querySelector('.closeWrapper').style.borderRight = '2px solid grey'
            }

        };

        button.innerHTML = `
      <img src="${this.url}${data.image}" alt="icon" style="width:37px;height:37px;display:block;position: absolute;" />
      ${data.sonar ? `<div class="su__widget-sonar-effect" style="box-shadow:0 0 0 2px ${data.background_color}, 0 0 10px 10px ${data.background_color}, 0 0 0 10px ${data.background_color}"></div>` : ''}
      
      <style>
        .su__widget-sonar-effect {
          pointer-events: none;
          position: absolute;
          width: 100%;
          height: 100%;
          border-radius: 50%;
          content: '';
          -webkit-box-sizing: content-box;
          -moz-box-sizing: content-box;
          box-sizing: content-box;
          top: 0;
          left: 0;
          right:0;
          bottom:0;
          margin:auto;
          padding: 0;
          z-index: -1;
          opacity: 0;
          -webkit-transform: scale(0.9);
          -moz-transform: scale(0.9);
          -ms-transform: scale(0.9);
          transform: scale(0.9);
          -webkit-animation: su__widget-sonar-effect 3.5s ease-out infinite;
          -moz-animation: su__widget-sonar-effect 3.5s ease-out infinite;
          animation: su__widget-sonar-effect 3.5s ease-out infinite;
          -webkit-animation-delay: 2s;
          -moz-animation-delay: 2s;
          animation-delay: 2s;
        }
        
        .su__widget-sonar-effect:before {
          speak: none;
          font-size: 48px;
          line-height: 90px;
          font-style: normal;
          font-weight: normal;
          font-variant: normal;
          text-transform: none;
          display: block;
          -webkit-font-smoothing: antialiased;
        }
        
        @keyframes su__widget-sonar-effect {
          0% {
            opacity: 0.5;
          } 
          10% {
            opacity: 0.7;
          }
          40% {
            transform: scale(1.3);
            opacity: 0;
          }
          100% {
            transform: scale(1.3);
            opacity: 0;
          }
        }
      </style>
    `;

        document.body.appendChild(button);
    }

    build(data) {
        if (data) {
            this.buildButton(data);
            this.buildIframe(data);
        }
    }

    init() {
        let data = this.getOptions();
        this.build(data);
    }
}

var widget = new widgetIframe();
widget.init();
