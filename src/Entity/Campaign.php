<?php

namespace App\Entity;

use App\Entity\Widget\WidgetShopSetting;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CampaignRepository")
 * @ORM\Table(name="campaigns")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Campaign
{
    const MAX_CAMPAING_LIST= 8;

    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Widget\WidgetShopSetting", inversedBy="campaign", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="setting_id",nullable=true)
     */
    private $widgetShopSetting;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="campaigns")
     * @ORM\JoinColumn(nullable=true)
     */
    private $shop;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\WhatsappCampaignVerify", mappedBy="campaign")
     * @ORM\OrderBy({"updatedAt" = "DESC", "createdAt" = "DESC"})
     */
    private $whatsappCampaignVerifies;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $rewardsMode;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $activatedAt;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getWidgetShopSetting()
    {
        return $this->widgetShopSetting;
    }

    /**
     * @param mixed $widgetShopSetting
     */
    public function setWidgetShopSetting($widgetShopSetting): void
    {
        $this->widgetShopSetting = $widgetShopSetting;
    }

    public function getWidgetShopSettingOrCreateNew(): ?WidgetShopSetting
    {
        if (!$this->widgetShopSetting) {
            $this->widgetShopSetting = new WidgetShopSetting();
            $this->widgetShopSetting->setShop($this->shop);
            $this->widgetShopSetting->setCampaign($this);
            $wss = $this->shop->getWidgetSetting();
            $this->widgetShopSetting->setWelcomePicture($wss->getWelcomePicture());
            $this->widgetShopSetting->setWelcomePicturePosX($wss->getWelcomePicturePosX());
            $this->widgetShopSetting->setWelcomePicturePosY($wss->getWelcomePicturePosY());
            $this->widgetShopSetting->setWelcomePictureHeight($wss->getWelcomePictureHeight());
            $this->widgetShopSetting->setWelcomePictureWidth($wss->getWelcomePictureWidth());
            $this->widgetShopSetting->setWelcomePictureRatio($wss->getWelcomePictureRatio());
        }
        return $this->widgetShopSetting;
    }

    /**
     * @return mixed
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     */
    public function setShop($shop): void
    {
        $this->shop = $shop;
    }

    /**
     * @return mixed
     */
    public function getWhatsappCampaignVerifies()
    {
        return $this->whatsappCampaignVerifies;
    }

    /**
     * @return mixed
     */
    public function getWhatsappCampaignVerifiesArray()
    {
        $res = [];
        if (isset($this->whatsappCampaignVerifies) && $this->whatsappCampaignVerifies->count() > 0) {
            foreach($this->whatsappCampaignVerifies->getValues() as $item) {
                $res[] = $item->toArray();
            }
        }
        return $res;
    }

    /**
     * @param mixed $whatsappCampaignVerifies
     */
    public function setWhatsappCampaignVerifies($whatsappCampaignVerifies): void
    {
        $this->whatsappCampaignVerifies = $whatsappCampaignVerifies;
    }

    public function setRewardsMode(int $rewardsMode)
    {
        $this->rewardsMode = $rewardsMode;
    }

    public function getRewardsMode(): ?int
    {
        return $this->rewardsMode;
    }

    public function getRewardsModeName(): ?string
    {
        if (!$this->getRewardsMode()) {
            return null;
        }

        return Shop::REWARD_MODE_NAMES[$this->getRewardsMode()];
    }

    /**
     * @return \DateTime
     */
    public function getDeletedAt(): \DateTime
    {
        return $this->deletedAt;
    }

    /**
     * @param \DateTime $deletedAt
     */
    public function setDeletedAt(\DateTime $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getActivatedAt()
    {
        return $this->activatedAt;
    }

    /**
     * @param mixed $activatedAt
     */
    public function setActivatedAt($activatedAt): void
    {
        $this->activatedAt = $activatedAt;
    }

    public function toArray() {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'createdAt' => $this->createdAt ? $this->createdAt->format('Y-m-d H:i:s') : null,
            'updatedAt' => $this->updatedAt ? $this->updatedAt->format('Y-m-d H:i:s') : null,
            'deletedAt' => $this->deletedAt ? $this->deletedAt->format('Y-m-d H:i:s') : null,
            'activatedAt' => $this->activatedAt ? $this->activatedAt->format('Y-m-d H:i:s') : null,
            'setting' => $this->getWidgetShopSettingOrCreateNew()->toArray()
        ];
    }

}
