<?php

namespace App\Manager;

use App\Entity\Shop;
use App\Entity\User;
use App\Entity\UserShop;
use App\Manager\AbstractManager;

class UserManager extends AbstractManager
{
    /**
     * Join to shop program (become a referrar).
     * @param User $user
     * @param Shop $shop
     * @return UserShop
     */
    public function joinToShopProgram(User $user, Shop $shop): UserShop
    {
        $userShop = (new UserShop)
            ->setUser($user)
            ->setShop($shop);

        $this->entityManager->persist($userShop);
        $this->entityManager->flush();

        return $userShop;
    }
}
