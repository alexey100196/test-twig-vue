<?php


namespace App\Tests\Service\User;


use App\Entity\User;
use App\Service\User\ChangePassword;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class ChangePasswordTest extends TestCase
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    protected function setUp(): void
    {
        $this->em = $this->createMock(EntityManagerInterface::class);
    }

    public function testIfPasswordChangeIsWorking()
    {
        $user = new User();
        $plainPassword = 'anyPlainPassword';

        $this->em->expects($this->once())->method('persist')->with($this->isInstanceOf(User::class));
        $this->em->expects($this->once())->method('flush');

        $userPasswordEncoder = $this->getMockBuilder(UserPasswordEncoder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $userPasswordEncoder->expects($this->once())->method('encodePassword')
            ->with($user, $plainPassword)
            ->willReturn('anyRandomHash');


        $service = new ChangePassword($this->em, $userPasswordEncoder);
        $service->changePassword($user, $plainPassword);
    }
}