<?php

namespace App\Controller\Auth;

use App\Adapter\MailSender;
use App\Controller\AjaxAbstractController;
use App\Entity\CommissionType;
use App\Entity\Offer;
use App\Entity\OfferType;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\ShopStatus;
use App\Entity\User;
use App\Event\User\ReferrerRegistered;
use App\Event\User\UserRegistered;
use App\Form\RegisterReferrerType;
use App\Form\RegisterShopType;
use App\Form\RegisterType;
use App\Repository\CommissionTypeRepository;
use App\Repository\OfferTypeRepository;
use App\Repository\ReferrerLinkRepository;
use App\Repository\ShopStatusRepository;
use App\Repository\UserRepository;
use App\Service\Shop\ShopRegistrationPushNotifications;
use App\Service\User\EmailConfirmation;
use App\Util\Consts;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var ShopStatusRepository
     */
    private $shopStatusRepository;

    /**
     * @var EmailConfirmation
     */
    private $emailConfirmation;

    /**
     * @var MailSender
     */
    private $mailSender;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var ReferrerLinkRepository
     */
    private $referrerLinkRepository;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param ShopStatusRepository $shopStatusRepository
     * @param EmailConfirmation $emailConfirmation
     * @param MailSender $mailSender
     * @param SessionInterface $session
     * @param EventDispatcherInterface $eventDispatcher
     * @param ReferrerLinkRepository $referrerLinkRepository
     */
    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        ShopStatusRepository $shopStatusRepository,
        EmailConfirmation $emailConfirmation,
        MailSender $mailSender,
        SessionInterface $session,
        EventDispatcherInterface $eventDispatcher,
        ReferrerLinkRepository $referrerLinkRepository
    )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->shopStatusRepository = $shopStatusRepository;
        $this->emailConfirmation = $emailConfirmation;
        $this->mailSender = $mailSender;
        $this->session = $session;
        $this->eventDispatcher = $eventDispatcher;
        $this->referrerLinkRepository = $referrerLinkRepository;
    }

    /**
     * @param Request $request
     * @return ReferrerLink|null
     */
    private function getReferrerLinkFromCookie(Request $request): ?ReferrerLink
    {
        foreach ($request->cookies->all() as $name => $value) {
            if (strpos($name, Consts::SNIPPET_COOKIE_NAME . '_') === 0) {
                return $value ? $this->referrerLinkRepository->findNotDeletedBySlug($value) : null;
            }
        }

        return null;
    }

    /**
     * @Route("/register/company", name="register_shop")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerShop(
        Request $request,
        EntityManagerInterface $entityManager,
        CommissionTypeRepository $commissionTypeRepository,
        OfferTypeRepository $offerTypeRepository,
        ShopRegistrationPushNotifications $shopRegistrationPushNotifications
    )
    {
        $form = $this->createForm(RegisterShopType::class, $user = new User());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shop = $user->getShop();
            $shop->setStatus($this->shopStatusRepository->findOneBy(['name' => ShopStatus::INACTIVE_NAME]));
            $shop->setSystemCommission($this->getParameter('shop_transaction_fee'));

            $commissionType = $commissionTypeRepository->find(CommissionType::FIXED_ID);
            $offerType = $offerTypeRepository->findOneBy(['name' => OfferType::CPS]);

            $cpsOffer = new Offer();
            $cpsOffer->setCommission(0);
            $cpsOffer->setCommissionType($commissionType);
            $cpsOffer->setType($offerType);
            $shop->setCpsOffer($cpsOffer);

            if ($referrerLink = $this->getReferrerLinkFromCookie($request)) {
                $shop->setReferredBy($referrerLink);
            }

            $shopRegistrationPushNotifications->create($shop);

            return $this->userRegistered($user = $this->storeUser($user));
        }

        return $this->render('auth/register-shop.html.twig', [
            'form' => $form->createView(),
            'subscriptionChosen' => $request->cookies->has(Consts::GUEST_SUBSCRIPTION_PLAN_COOKIE_NAME)
        ]);
    }

    /**
     * @Route("/register/referrer", name="register_referrer")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerReferrer(Request $request)
    {
        $redirectTo = $request->get('_target_path');
        $form = $this->createForm(RegisterReferrerType::class, $user = new User());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->storeUser($user);
            $this->eventDispatcher->dispatch(ReferrerRegistered::NAME, new ReferrerRegistered($user));
            return $this->userRegistered($user, $redirectTo);
        }

        if ($redirectTo) {
            $errors = $form->getErrors(true, true);
            $errorString = $this->getErrorString($errors);
            $errString = '';
            if ($errors->count() > 0) {
                $errString = '?error='.urlencode('<h4>Register form errors</h4>'.$errorString);
            }
//            dump($redirectTo.$errString);exit;
            return $this->redirect($redirectTo.$errString);
        }

        return $this->render('auth/register-referrer.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/register/confirm-email", name="register_confirm_email")
     */
    public function confirmEmailView()
    {
        if (!$email = $this->session->get('registeredUser.email')) {
            return $this->redirectToRoute('login');
        }

        return $this->render('auth/register-confirm.html.twig', ['email' => $email]);
    }

    /**
     * @Route("/register/email-verification/{token}/{user}", name="register_email_verification")
     */
    public function confirmEmail(string $token, int $user)
    {
        try {
            $this->emailConfirmation->activateUserByToken($token, $user);
            if ($this->session->has('registeredUser.email')) {
                $this->session->remove('registeredUser.email');
            }
        } catch (\LogicException $e) {
            $this->addFlash(Consts::ERROR, $e->getMessage());
            return $this->redirectToRoute('login');
        }

        $this->addFlash(Consts::SUCCESS, 'Thank you for registration. Please login to your account.');

        return $this->redirectToRoute('login');
    }

    /**
     * @Route("/register/confirm-email/resend", name="register_email_verification_resend", methods={"POST"})
     */
    public function resendConfirmationEmail(UserRepository $userRepository)
    {
        if ($email = $this->session->get('registeredUser.email')) {
            if ($user = $userRepository->findOneByEmail($email)) {
                $this->sendNewConfirmationEmailToUser($user);
            }
        }

        return $this->redirectToRoute('register_confirm_email');
    }

    /**
     * Store user in db.
     *
     * @param User $user
     * @return User
     */
    protected function storeUser(User $user): User
    {
        $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPlainPassword()));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return $user;
    }

    /**
     * What to do after registration.
     * @param User $user
     * @param string|null $redirectTo
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function userRegistered(User $user, ?string $redirectTo = null)
    {
        $this->session->set('registeredUser.email', $user->getEmail());

        $this->sendNewConfirmationEmailToUser($user);

        if ($redirectTo) return $this->redirect($redirectTo.'?success=A verification link has been sent to your email');
        return $this->redirectToRoute('register_confirm_email');
    }

    /**
     * @param User $user
     */
    protected function sendNewConfirmationEmailToUser(User $user)
    {
        $token = $this->emailConfirmation->createToken($user);

        $this->mailSender->send('hello@superinterface.com',
            $user->getEmail(),
            'Verify Email Address.',
            $this->render('email/email-confirmation.html.twig', [
                'token' => $token->getToken(),
                'user' => $user
            ])
        );
    }

    /**
     * Return error html string based errors list.
     * @param FormErrorIterator $errors
     * @return string
     */
    protected function getErrorString(FormErrorIterator $errors) {
        try {
            $errorTxt = '';
            foreach($errors as $error) {
                $errorTxt .= '<strong>' . ucfirst($error->getOrigin()->getName()) . ': </strong>'.$error->getMessage().'<br>';
            }
        } catch (\Exception $e) {
            return '<strong>(Error code'.$e->getCode().')</strong>'.$e->getMessage();
        }
        return $errorTxt;
    }
}
