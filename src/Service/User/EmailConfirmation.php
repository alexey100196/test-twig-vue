<?php


namespace App\Service\User;


use App\Entity\ConfirmationEmailToken;
use App\Entity\User;
use App\Event\User\UserRegistered;
use App\Repository\ConfirmationEmailTokenRepository;
use App\Util\TokenGenerator\TokenGeneratorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EmailConfirmation
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TokenGeneratorInterface
     */
    protected $tokenGenerator;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @var ConfirmationEmailTokenRepository
     */
    protected $confirmationEmailTokenRepository;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     * @param EventDispatcherInterface $eventDispatcher
     * @param ConfirmationEmailTokenRepository $confirmationEmailTokenRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        EventDispatcherInterface $eventDispatcher,
        ConfirmationEmailTokenRepository $confirmationEmailTokenRepository
    )
    {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
        $this->confirmationEmailTokenRepository = $confirmationEmailTokenRepository;
    }

    /**
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function setTokenGenerator(TokenGeneratorInterface $tokenGenerator)
    {
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * @param User $user
     * @return ConfirmationEmailToken
     */
    public function createToken(User $user): ConfirmationEmailToken
    {
        $token = (new ConfirmationEmailToken())
            ->setUser($user)
            ->setToken($this->tokenGenerator->generate(6))
            ->setCreatedAt($now = new \DateTime())
            ->setExpiresAt($now->modify('+1 day'));

        $this->em->persist($token);
        $this->em->flush();

        return $token;
    }

    /**
     * @param string $token
     * @param int $userId
     * @return User|null
     */
    public function activateUserByToken(string $token, int $userId)
    {
        if (!$tokenEntity = $this->findActiveToken($token, $userId)) {
            throw new \LogicException('Email confirmation token expired.');
        }

        if ($tokenEntity->getUser()->isEmailConfirmed()) {
            throw new \LogicException('This user is active.');
        }

        $tokenEntity->getUser()->setEmailConfirmedAt(new \DateTime());
        $this->em->remove($tokenEntity);
        $this->em->flush();

        $this->eventDispatcher->dispatch(UserRegistered::NAME, new UserRegistered($tokenEntity->getUser()));

        return $tokenEntity->getUser();
    }

    /**
     * @param string $token
     * @param int $userId
     * @return ConfirmationEmailToken|null
     */
    protected function findActiveToken(string $token, int $userId): ?ConfirmationEmailToken
    {
        return $this->confirmationEmailTokenRepository->findNotExpiredTokenByTokenAndUserId($token, $userId);
    }
}