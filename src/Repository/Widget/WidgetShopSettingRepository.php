<?php

namespace App\Repository\Widget;

use App\Entity\Widget\WidgetShopSetting;
use App\Entity\Shop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WidgetShopSetting|null find($id, $lockMode = null, $lockVersion = null)
 * @method WidgetShopSetting|null findOneBy(array $criteria, array $orderBy = null)
 * @method WidgetShopSetting[]    findAll()
 * @method WidgetShopSetting[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WidgetShopSettingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WidgetShopSetting::class);
    }

    /**
     * @param Shop $shop
     * @return WidgetShopSetting[] Returns an array of Campaign objects
     */
    public function getAllByShop(Shop $shop)
    {
        $query = $this->createQueryBuilder('c')
            ->Where('c.shop = :shop')
            ->setParameter('shop', $shop)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult();
        return (array) $query;
    }

    // /**
    //  * @return WidgetShopSetting[] Returns an array of WidgetShopSetting objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WidgetShopSetting
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
