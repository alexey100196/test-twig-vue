<?php

namespace App\Repository\DocumentReferrer;

use App\Entity\DocumentReferrer\DocumentReferrerGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DocumentReferrerGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentReferrerGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentReferrerGroup[]    findAll()
 * @method DocumentReferrerGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentReferrerGroupRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DocumentReferrerGroup::class);
    }

    // /**
    //  * @return DocumentReferrerGroup[] Returns an array of DocumentReferrerGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DocumentReferrerGroup
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
