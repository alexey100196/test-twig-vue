<?php

namespace App\Repository;

use App\Entity\ReferrerNonReferableLinkCard;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReferrerNonReferableLinkCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferrerNonReferableLinkCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferrerNonReferableLinkCard[]    findAll()
 * @method ReferrerNonReferableLinkCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferrerNonReferableLinkCardRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReferrerNonReferableLinkCard::class);
    }
}
