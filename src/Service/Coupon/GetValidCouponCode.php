<?php


namespace App\Service\Coupon;


use App\Entity\Coupon\Coupon;
use App\Entity\Coupon\CouponPackage;
use App\Entity\Shop;
use App\Repository\Coupon\CouponRepository;

class GetValidCouponCode
{
    private $couponRepository;

    public function __construct(
        CouponRepository $couponRepository
    )
    {
        $this->couponRepository = $couponRepository;
    }

    private function getCouponPackageByShopAndAllocation(Shop $shop, int $allocation): CouponPackage
    {
        if (!$shop->getWidgetSetting()) {
            throw new \LogicException('Coupon does not exists.');
        }

        $package = null;
        $activeCampaign =  $shop->getCampaigns();
        $activeCampaign = ($activeCampaign) ? $activeCampaign[0] : $activeCampaign;
        if ($allocation === CouponPackage::ALLOCATION_INVITEE) {
            $package = $activeCampaign->getWidgetShopSetting()->getInviteeRewardCouponPackage();
        } elseif ($allocation === CouponPackage::ALLOCATION_REFERRER) {
            $package = $activeCampaign->getWidgetShopSetting()->getReferrerRewardCouponPackage();
        }

        if (!$package) {
            throw new \LogicException('Coupon does not exists.');
        }

        return $package;
    }

    public function __invoke(Shop $shop, int $allocation)
    {
        $package = $this->getCouponPackageByShopAndAllocation($shop, $allocation);
        $coupon = $this->couponRepository->findFreeCouponInCouponPackage($package);

        if (!$coupon) {
            throw new \LogicException('Coupon does not exists.');
        }

        $package = $coupon->getPackage();

        if (!$package->getMultipleUsage()) {
            $this->validateSingleUsageCoupon($coupon);
        }

        return $coupon;
    }

    private function validateSingleUsageCoupon(Coupon $coupon)
    {
        if ($coupon->getUsed() > 0) {
            throw new \LogicException('Coupon already used.');
        }
    }
}