<?php


namespace App\Form\Referrer;


use App\Entity\CardSet\CardSet;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotNull;

class CardSetType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'constraints' => [
                    new NotNull(),
                    new Length(['max' => 128])
                ],
            ])
            ->add('columnsCount', IntegerType::class, [
                'constraints' => [
                    new NotNull(),
                    new GreaterThanOrEqual(2),
                    new LessThanOrEqual(3),
                ]
            ])
            ->add('cardsWidth', IntegerType::class, [
                'constraints' => [
                    new NotNull(),
                    new GreaterThan(0),
                    new LessThanOrEqual(5000),
                ]
            ]);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CardSet::class,
            // @todo: fix it
            'csrf_protection' => false,
        ]);
    }
}