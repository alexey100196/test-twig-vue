<?php


namespace App\Service;


use App\Entity\Campaign;
use App\Entity\Product;
use App\Entity\Shop;
use App\Entity\User;
use App\Entity\Widget\WidgetShopSetting;
use App\Repository\Coupon\CouponPackageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CampaignService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /** @var CouponPackageRepository */
    private $couponPackageRepository;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, CouponPackageRepository $couponPackageRepository)
    {
        $this->em = $em;
        $this->couponPackageRepository = $couponPackageRepository;
    }

    /**
     * @param Campaign $campaign
     * @return Campaign
     */
    public function save(Campaign $campaign)
    {
        $this->em->persist($campaign->getWidgetShopSetting());
  /*    $this->em->flush();*/
        $this->em->persist($campaign);
        $this->em->flush();
        return $campaign;
    }

    /**
     * @param Campaign $campaign
     */
    public function remove(Campaign $campaign)
    {
        $wss = $campaign->getWidgetShopSetting();
        $campaign->setWidgetShopSetting(null);
        $campaign->setShop(null);
        $this->em->persist($campaign);
        $this->em->flush();

        $this->em->remove($campaign);
        $this->em->flush();

        $this->removeCouponPackage($wss);
        $this->em->flush();
    }

    /**
     * @param Campaign $campaign
     */
    public function activate(Campaign $campaign)
    {
        if (!$campaign->getActivatedAt()) {
            $campaign->setActivatedAt(new \DateTime());
            $this->em->persist($campaign);
            $this->em->flush();
        }
        return $campaign;
    }

    /**
     * @param Campaign $campaign
     */
    public function deactivate(Campaign $campaign)
    {
        if ($campaign->getActivatedAt()) {
            $campaign->setActivatedAt(null);
            $this->em->persist($campaign);
            $this->em->flush();
        }
    }


    public function removeCouponPackage(WidgetShopSetting $wss){
        $couponPackage = null;
        $couponPackage = $this->couponPackageRepository->findOneBy(['shop' => $wss->getShop(), 'id' => $wss->getReferrerRewardCouponPackage()->getId()]);
        if (!$couponPackage) {
            return new JsonResponse(['message' => 'Coupon package not found.'], Response::HTTP_BAD_REQUEST);
        }
        if ($couponPackage->isActive()){
            return new JsonResponse(['message' => 'Can not delete active coupon package.'], Response::HTTP_BAD_REQUEST);
        }
        try {
            $this->em->remove($couponPackage);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => 'Cannot delete coupon package.'], Response::HTTP_BAD_REQUEST);
        }

        $couponPackage = null;
        $couponPackage = $this->couponPackageRepository->findOneBy(['shop' =>  $wss->getShop(), 'id' => $wss->getInviteeRewardCouponPackage()->getId()]);
        if (!$couponPackage) {
            return new JsonResponse(['message' => 'Coupon package not found.'], Response::HTTP_BAD_REQUEST);
        }
        if ($couponPackage->isActive()){
            return new JsonResponse(['message' => 'Can not delete active coupon package.'], Response::HTTP_BAD_REQUEST);
        }
        try {
            $this->em->remove($couponPackage);
        } catch (\Exception $e) {
            return new JsonResponse(['message' => 'Cannot delete coupon package.'], Response::HTTP_BAD_REQUEST);
        }
    }
}