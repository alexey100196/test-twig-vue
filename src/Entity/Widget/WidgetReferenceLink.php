<?php

namespace App\Entity\Widget;

use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Widget\WidgetReferenceLinkRepository")
 * @UniqueEntity("slug")
 */
class WidgetReferenceLink
{
    use TimestampableEntity;

    const TYPE_CASH_REWARD = 1;
    const TYPE_COUPON_REWARD = 2;

    const TYPE_NAMES = [
        'cash' => self::TYPE_CASH_REWARD,
        'coupon' => self::TYPE_COUPON_REWARD,
    ];

    const COUPON_REWARD_COOKIE_PREFIX = 'widgetCouponReward';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $campaignId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ReferrerLink", inversedBy="widgetReferenceLink", cascade={"persist", "remove"})
     */
    private $referrerLink;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $destinationUrl;

    /**
     * @var string
     * @ORM\Column(type="string", length=30, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Widget\WidgetReferenceLinkBrowserFingerprint", mappedBy="widgetReferenceLink", cascade={"persist", "remove"})
     */
    private $browserFingerprints;

    public function __construct()
    {
        $this->generateSlug();
        $this->browserFingerprints = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCampaignId(): ?int
    {
        return $this->campaignId;
    }

    public function setCampaignId(?int $campaignId): self
    {
        $this->campaignId = $campaignId;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    public function getReferrerLink(): ?ReferrerLink
    {
        return $this->referrerLink;
    }

    public function setReferrerLink(?ReferrerLink $referrerLink): self
    {
        $this->referrerLink = $referrerLink;
        return $this;
    }


    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getFullUrl(): string
    {
        if ($this->getReferrerLink()) {
            return $this->getReferrerLink()->getFullUrl();
        }

        return '/share/' . $this->getSlug();
    }

    /**
     * @return string|null
     */
    public function getDestinationUrl(): ?string
    {
        return $this->destinationUrl;
    }

    /**
     * @return string|null
     */
    public function getShareableDestinationUrl(): ?string
    {
        if ($this->destinationUrl) {
            return $this->destinationUrl;
        }

        return $this->getShop()->getWebsite();
    }

    /**
     * @param string|null $destinationUrl
     */
    public function setDestinationUrl(?string $destinationUrl): void
    {
        $this->destinationUrl = $destinationUrl;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    private function generateSlug(): void
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 30; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $this->slug = $randomString;
    }

    /**
     * @return Collection|WidgetReferenceLinkBrowserFingerprint[]
     */
    public function getBrowserFingerprints(): Collection
    {
        return $this->browserFingerprints;
    }

    private function hasBrowserFingerPrintWithContent(string $content): bool
    {
        foreach ($this->getBrowserFingerprints() as $browserFingerprint) {
            if ($browserFingerprint->getContent() == $content) {
                return true;
            }
        }

        return false;
    }

    public function addBrowserFingerprint(WidgetReferenceLinkBrowserFingerprint $browserFingerprint): self
    {
        if (!$this->hasBrowserFingerPrintWithContent($browserFingerprint->getContent())) {
            $this->browserFingerprints[] = $browserFingerprint;
            $browserFingerprint->setWidgetReferenceLink($this);
        }

        return $this;
    }

    public function removeBrowserFingerprint(WidgetReferenceLinkBrowserFingerprint $browserFingerprint): self
    {
        if ($this->browserFingerprints->contains($browserFingerprint)) {
            $this->browserFingerprints->removeElement($browserFingerprint);
            // set the owning side to null (unless already changed)
            if ($browserFingerprint->getWidgetReferenceLink() === $this) {
                $browserFingerprint->setWidgetReferenceLink(null);
            }
        }
        return $this;
    }
}
