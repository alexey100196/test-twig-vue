<?php

namespace App\Tests\Controller\Api\V1;


use App\Entity\Shop;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CheckEmailControllerTest extends WebTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var Shop
     */
    protected $shop;

    /**
     * @var User
     */
    protected $user;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->em = $this->client->getKernel()->getContainer()->get('doctrine')
            ->getManager();

        $this->shop = $this->em->getRepository(Shop::class)->findOneBy(['name' => 'Demo']);
        $this->user = $this->em->getRepository(User::class)->findOneBy(['nickname' => 'john']);
    }

    /**
     * @testdox Redirect to login if user isn't referrer
     */
    public function testCheckEmail()
    {
        $client = static::createClient();
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);

        // Without params
        $client->request('POST', '/api/v1/check/email', []);
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
        $this->assertEquals([
            'error' => 'Shop not found'
        ], json_decode($client->getResponse()->getContent(), true));

        // With companyId
        $client->request('POST', '/api/v1/check/email', [
            'companyId' => $this->shop->getPartnerNumber()
        ]);
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
        $this->assertEquals([
            'errors' => [
                'email' => 'This value should not be blank.'
            ]
        ], json_decode($client->getResponse()->getContent(), true));

        // Invalid email
        $client->request('POST', '/api/v1/check/email', [
            'companyId' => $this->shop->getPartnerNumber(),
            'email' => 'fesefsfsefawd-grdrdgdrg.pl'
        ]);
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
        $this->assertEquals([
            'errors' => [
                'email' => 'This value is not a valid email address.'
            ]
        ], json_decode($client->getResponse()->getContent(), true));

        // Valuelink type
        $client->request('POST', '/api/v1/check/email', [
            'companyId' => $this->shop->getPartnerNumber(),
            'email' => $this->user->getEmail()
        ]);
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertEquals([
            'accountType' => 'valuelink'
        ], json_decode($client->getResponse()->getContent(), true));

        // Paypal type
        $client->request('POST', '/api/v1/check/email', [
            'companyId' => $this->shop->getPartnerNumber(),
            'email' => 'testpaypal@test.com'
        ]);
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $result = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('paypal', $result['accountType']);
        $this->assertTrue(isset($result['paypalUrl']) && !empty(isset($result['paypalUrl'])));
    }
}
