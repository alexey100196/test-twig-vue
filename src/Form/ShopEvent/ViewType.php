<?php


namespace App\Form\ShopEvent;

use App\Entity\EventEntry;
use App\Entity\EventEntryType;
use App\Entity\Shop;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class ViewType extends AbstractEventEntryType
{
    /**
     * @var array
     */
    private $rawData = [];

    /**
     * @param array $data
     */
    protected function rememberRawData(array $data)
    {
        $this->rawData = $data;
    }

    /**
     * @return array
     */
    protected function getRawData()
    {
        return $this->rawData;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('payload', ViewPayloadType::class, [
                'by_reference' => true,
            ])
            ->add('test', CheckboxType::class, [
                'required' => false,
                'empty_data' => false,
                'false_values' => ['0', 'false', 0, false],
            ])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                $data['type'] = EventEntryType::TYPE_VIEW;
                $this->rememberRawData($data);
                $event->setData($data);
            })
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                $data->setPayload($this->getRawData()['payload'] ?? []);
                $event->setData($data);
            });
    }
}