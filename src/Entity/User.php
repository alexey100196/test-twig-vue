<?php

namespace App\Entity;

use App\Entity\Coupon\UserCoupon;
use App\Entity\Subscription\Plan;
use App\Entity\Subscription\PlanSubscription;
use App\Entity\CardSet\CardSet;
use App\Service\Balance\Entity\HasBalance;
use App\Service\Balance\Entity\HasBalanceEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\EntityListeners;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use phpDocumentor\Reflection\Types\String_;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\EntityListeners({"App\Listener\Entity\UserListener"})
 */
class User implements UserInterface, HasBalanceEntity
{
    use TimestampableEntity;
    use HasBalance;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"shop"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=24, nullable=true)
     * @Groups({"shop"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     * @Groups({"shop"})
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * @Groups({"shop"})
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\Email()
     * @Groups({"shop"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Shop", mappedBy="user", cascade={"persist", "remove"})
     */
    private $shop;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Role")
     * @ORM\JoinColumn(nullable=false)
     */
    private $role;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ReferrerMyStore", inversedBy="referrer")
     * @ORM\JoinColumn(nullable=true)
     */
    private $my_store;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CustomDomain", mappedBy="user")
     * @ORM\OrderBy({"updatedAt" = "DESC"})
     */
    private $customDomains;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserShop", mappedBy="user")
     */
    private $userShops;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserBalance", mappedBy="user", orphanRemoval=true)
     */
    private $balances;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ReferrerLink", mappedBy="referrer")
     */
    private $referrerLinks;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $termsAcceptedAt;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $ibanNumber;

    /**
     * @ORM\Column(type="string", length=11, nullable=true)
     */
    private $swiftNumber;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $paypalAccount;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Groups({"shop"})
     */
    private $nickname;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $emailConfirmedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserTransaction", mappedBy="user", cascade={"persist", "remove"})
     */
    private $transactions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Subscription\PlanSubscription", mappedBy="user")
     */
    private $subscriptions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CardSet\CardSet", mappedBy="user")
     */
    private $cardSets;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=2)
     */
    private $lastMonthIncome = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $wantContinueSubscription;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebookPixel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $twitterPixel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pinterestPixel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $linkedinPixel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googlePixel;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $quoraPixel;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Coupon\UserCoupon", mappedBy="user")
     */
    private $coupons;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $pendingPayoutAmount;

    /**
     * @var ReferrerCoupon[]
     * @ORM\OneToMany(targetEntity="App\Entity\ReferrerCoupon", mappedBy="referrer")
     */
    private $referrerCoupons;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\PushNotification", mappedBy="user")
     */
    private $pushNotifications;


    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $youtube;

    public function __construct()
    {
        $this->userShops = new ArrayCollection();
        $this->balances = new ArrayCollection();
        $this->referrerLinks = new ArrayCollection();
        $this->transactions = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
        $this->cardSets = new ArrayCollection();
        $this->coupons = new ArrayCollection();
        $this->pushNotifications = new ArrayCollection();
        $this->referrerCoupons = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone = null): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getRoles()
    {
        return [(string)$this->getRole(), Role::ROLE_FULL_PROFILE];
    }

    public function isAdmin()
    {
        return in_array(Role::ROLE_ADMIN, $this->getRoles());
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getSalt()
    {
        return '';
    }

    public function eraseCredentials()
    {
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(Shop $shop): self
    {
        $this->shop = $shop;

        // set the owning side of the relation if necessary
        if ($this !== $shop->getUser()) {
            $shop->setUser($this);
        }

        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getReferrerMyStore(): ?ReferrerMyStore
    {
        if ($this->isReferrer()) {
            return $this->my_store;
        }

        return null;
    }

    public function setReferrerMyStore(?ReferrerMyStore $my_store): self
    {
        if ($this->isReferrer()) {
            $this->my_store = $my_store;
        }

        return $this;
    }

    public function getFullName()
    {
        return trim($this->getName() . ' ' . $this->getSurname());
    }

    /**
     * @return Collection|UserShop[]
     */
    public function getUserShops(): Collection
    {
        return $this->userShops;
    }

    /**
     * @return Collection|CustomDomain[]
     */
    public function getCustomDomains(): Collection
    {
        return $this->customDomains;
    }

    /**
     * Returns all joined shops.
     * @return ArrayCollection
     */
    public function getJoinedShops(): ArrayCollection
    {
        return $this->getUserShops()->map(function ($item) {
            return $item->getShop();
        });
    }

    public function addUserShop(UserShop $userShop): self
    {
        if (!$this->userShops->contains($userShop)) {
            $this->userShops[] = $userShop;
            $userShop->setUser($this);
        }

        return $this;
    }

    public function removeUserShop(UserShop $userShop): self
    {
        if ($this->userShops->contains($userShop)) {
            $this->userShops->removeElement($userShop);
            // set the owning side to null (unless already changed)
            if ($userShop->getUser() === $this) {
                $userShop->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return UserBalance
     */
    public function createBalance(): Balance
    {
        $balance = new UserBalance();
        $balance->setUser($this);

        return $balance;
    }

    /**
     * @return Collection|ReferrerLink[]
     */
    public function getReferrerLinks(): Collection
    {
        return $this->referrerLinks;
    }

    public function MAX_AFFILIATE_LINKS(ReferrerLink $referrerLink): self
    {
        if (!$this->referrerLinks->contains($referrerLink)) {
            $this->referrerLinks[] = $referrerLink;
            $referrerLink->setReferrer($this);
        }

        return $this;
    }

    public function removeReferrerLink(ReferrerLink $referrerLink): self
    {
        if ($this->referrerLinks->contains($referrerLink)) {
            $this->referrerLinks->removeElement($referrerLink);
            // set the owning side to null (unless already changed)
            if ($referrerLink->getReferrer() === $this) {
                $referrerLink->setReferrer(null);
            }
        }

        return $this;
    }

    /**
     * @param ReferrerLink $referrerLink
     * @return bool
     */
    public function hasReferrerLink(ReferrerLink $referrerLink)
    {
        return $this->referrerLinks->contains($referrerLink);
    }

    /**
     * @param Shop $shop
     * @return ReferrerLink
     */
    public function getNewReferrerLink(Shop $shop)
    {
        $referrerLink = new ReferrerLink();
        $referrerLink->setReferrer($this);
        $referrerLink->setShop($shop);

        return $referrerLink;
    }

    public function getTermsAcceptedAt(): ?\DateTimeInterface
    {
        return $this->termsAcceptedAt;
    }

    public function setTermsAcceptedAt(?\DateTimeInterface $termsAcceptedAt): self
    {
        $this->termsAcceptedAt = $termsAcceptedAt;

        return $this;
    }

    public function getIbanNumber(): ?string
    {
        return $this->ibanNumber;
    }

    public function setIbanNumber(?string $ibanNumber): self
    {
        $this->ibanNumber = $ibanNumber;

        return $this;
    }

    public function getSwiftNumber(): ?string
    {
        return $this->swiftNumber;
    }

    public function setSwiftNumber(?string $swiftNumber): self
    {
        $this->swiftNumber = $swiftNumber;

        return $this;
    }

    public function getPaypalAccount(): ?string
    {
        return $this->paypalAccount;
    }

    public function setPaypalAccount(?string $paypalAccount): self
    {
        $this->paypalAccount = $paypalAccount;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(?string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * @return bool
     */
    public function isReferrer()
    {
        return $this->getRole() && $this->getRole()->getName() === Role::ROLE_REFERRER;
    }

    public function getEmailConfirmedAt(): ?\DateTimeInterface
    {
        return $this->emailConfirmedAt;
    }

    public function setEmailConfirmedAt(?\DateTimeInterface $emailConfirmedAt): self
    {
        $this->emailConfirmedAt = $emailConfirmedAt;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmailConfirmed(): bool
    {
        return null !== $this->emailConfirmedAt;
    }

    /**
     * @return Collection
     */
    public function getMyStoreReferrerLinks()
    {
        return $this->getReferrerLinks()
            ->filter(function (ReferrerLink $referrerLink) {
                return $referrerLink->getShowInMyStore();
            });
    }

    /**
     * @return Collection|UserTransaction[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    /**
     * @return Collection|PlanSubscription[]
     */
    public function getSubscriptions(): Collection
    {
        return $this->subscriptions;
    }

    public function getCurrentPlan(): ?PlanSubscription
    {
        return $this->getSubscriptions()->count() > 0 ? $this->getSubscriptions()->last() : null;
    }

    /**
     * @return bool
     */
    public function isSubscribed(): bool
    {
        foreach ($this->getSubscriptions() as $subscription) {
            if ($subscription->isActive()) {
                return true;
            }
        }

        return false;
    }

    public function willContinueSubscriptionAfterTrial()
    {
        foreach ($this->getSubscriptions() as $subscription) {
            if ($subscription->willContinueAfterTrial()) {
                return true;
            }
        }

        return false;
    }

    public function cancelAllSubscriptions(bool $immediately = false)
    {
        foreach ($this->getSubscriptions() as $subscription) {
            if (!$subscription->isEnded() && !$subscription->isCanceled()) {
                $subscription->cancel($immediately);
            }
        }
    }

    public function isSubscribedButNotOnTrial()
    {
        foreach ($this->getSubscriptions() as $subscription) {
            if ($subscription->isActiveButNotOnTrial()) {
                return true;
            }
        }

        return false;
    }

    public function getSubscriptionsToPay()
    {
        return $this->getSubscriptions()
            ->filter(function ($subscription) {
                return ($subscription->isEnded() || $subscription->isOnTrial()) && !$subscription->isCanceled();
            });
    }

    public function addSubscription(PlanSubscription $subscription): self
    {
        if (!$this->subscriptions->contains($subscription)) {
            $this->subscriptions[] = $subscription;
            $subscription->setUser($this);
        }
    }

    /**
     * @return Collection|CardSet[]
     */
    public function getCardSets(): Collection
    {
        return $this->cardSets;
    }

    public function addCardSet(CardSet $cardSet): self
    {
        if (!$this->cardSets->contains($cardSet)) {
            $this->cardSets[] = $cardSet;
            $cardSet->setUser($this);
        }

        return $this;
    }


    public function removeSubscription(PlanSubscription $subscription): self
    {
        if ($this->subscriptions->contains($subscription)) {
            $this->subscriptions->removeElement($subscription);
            // set the owning side to null (unless already changed)
            if ($subscription->getUser() === $this) {
                $subscription->setUser(null);
            }
        }
    }

    public function removeCardSet(CardSet $cardSet): self
    {
        if ($this->cardSets->contains($cardSet)) {
            $this->cardSets->removeElement($cardSet);
            // set the owning side to null (unless already changed)
            if ($cardSet->getUser() === $this) {
                $cardSet->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @param Plan $plan
     * @return bool
     */
    public function subscribesPlan(Plan $plan): bool
    {
        return $this->getActiveSubscriptionForPlan($plan) !== null;
    }

    /**
     * @param Plan $plan
     * @return PlanSubscription|null
     */
    public function getActiveSubscriptionForPlan(Plan $plan): ?PlanSubscription
    {
        foreach ($this->getSubscriptions() as $subscription) {
            if ($subscription->getPlan() === $plan && !$subscription->isEnded()) {
                return $subscription;
            }
        }

        return null;
    }

    public function getLastMonthIncome()
    {
        return $this->lastMonthIncome;
    }

    public function setLastMonthIncome(float $lastMonthIncome): self
    {
        $this->lastMonthIncome = $lastMonthIncome;

        return $this;
    }

    public function getWantContinueSubscription(): ?bool
    {
        return $this->wantContinueSubscription;
    }

    public function setWantContinueSubscription(?bool $wantContinueSubscription): self
    {
        $this->wantContinueSubscription = $wantContinueSubscription;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFacebookPixel()
    {
        return $this->facebookPixel;
    }

    /**
     * @param mixed $facebookPixel
     */
    public function setFacebookPixel($facebookPixel): void
    {
        $this->facebookPixel = $facebookPixel;
    }

    /**
     * @return mixed
     */
    public function getTwitterPixel()
    {
        return $this->twitterPixel;
    }

    /**
     * @param mixed $twitterPixel
     */
    public function setTwitterPixel($twitterPixel): void
    {
        $this->twitterPixel = $twitterPixel;
    }

    /**
     * @return mixed
     */
    public function getPinterestPixel()
    {
        return $this->pinterestPixel;
    }

    /**
     * @param mixed $pinterestPixel
     */
    public function setPinterestPixel($pinterestPixel): void
    {
        $this->pinterestPixel = $pinterestPixel;
    }

    /**
     * @return mixed
     */
    public function getLinkedinPixel()
    {
        return $this->linkedinPixel;
    }

    /**
     * @param mixed $linkedinPixel
     */
    public function setLinkedinPixel($linkedinPixel): void
    {
        $this->linkedinPixel = $linkedinPixel;
    }

    /**
     * @return mixed
     */
    public function getGooglePixel()
    {
        return $this->googlePixel;
    }

    /**
     * @param mixed $googlePixel
     */
    public function setGooglePixel($googlePixel): void
    {
        $this->googlePixel = $googlePixel;
    }

    /**
     * @return mixed
     */
    public function getQuoraPixel()
    {
        return $this->quoraPixel;
    }

    /**
     * @param mixed $quoraPixel
     */
    public function setQuoraPixel($quoraPixel): void
    {
        $this->quoraPixel = $quoraPixel;
    }

    /**
     * @return Collection|UserCoupon[]
     */
    public function getCoupons(): Collection
    {
        return $this->coupons;
    }

    public function addCoupon(UserCoupon $coupon): self
    {
        if (!$this->coupons->contains($coupon)) {
            $this->coupons[] = $coupon;
            $coupon->setUser($this);
        }

        return $this;
    }

    public function removeCoupon(UserCoupon $coupon): self
    {
        if ($this->coupons->contains($coupon)) {
            $this->coupons->removeElement($coupon);
            // set the owning side to null (unless already changed)
            if ($coupon->getUser() === $this) {
                $coupon->setUser(null);
            }
        }

        return $this;
    }

    public function getPendingPayoutAmount(): ?float
    {
        return $this->pendingPayoutAmount;
    }

    public function setPendingPayoutAmount(?float $pendingPayoutAmount): self
    {
        $this->pendingPayoutAmount = $pendingPayoutAmount;

        return $this;
    }

    public function changePendingPayoutAmountBy(float $amount)
    {
        $this->pendingPayoutAmount += $amount;
    }

    public function hasAnyFunds(string $currency)
    {
        $balance = $this->getCurrencyBalance($currency);

        return $balance && $balance->getAmount() > 0;
    }

    public function getOrCreateReferrerCoupon(Shop $shop): ReferrerCoupon
    {
        $referrerCoupon = $this->getReferrerCouponByShop($shop);

        if (!$referrerCoupon) {
            $referrerCoupon = new ReferrerCoupon();
            $referrerCoupon->setReferrer($this);
            $referrerCoupon->setExpiresAt(new \DateTime());
            $referrerCoupon->setShop($shop);
            $this->referrerCoupons->add($referrerCoupon);
        }

        return $referrerCoupon;
    }

    public function hasActiveReferrerCoupon(Shop $shop, string $type): bool
    {
        $referrerCoupon = $this->getReferrerCouponByShop($shop);

        return $referrerCoupon && $referrerCoupon->isEnabled($type);
    }

    /**
     * @return Collection
     */
    public function getPushNotifications(): Collection
    {
        return $this->pushNotifications;
    }

    /**
     * @return Collection|ReferrerCoupon[]
     */
    public function getReferrerCoupons(): Collection
    {
        return $this->referrerCoupons;
    }

    /**
     * @param Shop $shop
     * @return ReferrerCoupon|null
     */
    public function getReferrerCouponByShop(Shop $shop): ?ReferrerCoupon
    {
        foreach ($this->getReferrerCoupons() as $referrerCoupon) {
            if ($referrerCoupon->getShop() === $shop) {
                return $referrerCoupon;
            }
        }

        return null;
    }

    public function addReferrerCoupon(ReferrerCoupon $referrerCoupon): self
    {
        if (!$this->referrerCoupons->contains($referrerCoupon)) {
            $this->referrerCoupons[] = $referrerCoupon;
        }

        return $this;
    }

    public function removeReferrerCoupon(ReferrerCoupon $referrerCoupon): self
    {
        if ($this->referrerCoupons->contains($referrerCoupon)) {
            $this->referrerCoupons->removeElement($referrerCoupon);
        }

        return $this;
    }

    public function getYoutube(): ?string
    {
        return $this->youtube;
    }

    public function setTest(string $youtube): self
    {
        $this->youtube = $youtube;

        return $this;
    }

    public function getPixels() {
        return [
            'facebook_pixel' => $this->getFacebookPixel(),
            'twitter_pixel' => $this->getTwitterPixel(),
            'pinterest_pixel' => $this->getPinterestPixel(),
            'linkedin_pixel' => $this->getLinkedinPixel(),
            'google_pixel' => $this->getGooglePixel(),
            'quora_pixel' => $this->getQuoraPixel(),
        ];
    }
}
