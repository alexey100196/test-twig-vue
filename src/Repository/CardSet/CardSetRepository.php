<?php

namespace App\Repository\CardSet;

use App\Entity\CardSet\CardSet;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardSet|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardSet|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardSet[]    findAll()
 * @method CardSet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardSetRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardSet::class);
    }

    /**
     * @param User $user
     * @param \DateTime $start
     * @param \DateTime $end
     * @param string|null $name
     * @return mixed
     */
    public function findAllWithStatusBetweenDates(User $user, \DateTime $start, \DateTime $end, string $name = null)
    {
        $qb = $this->createQueryBuilder('cs');
        $qb->andWhere('cs.createdAt >= :start');
        $qb->andWhere('cs.createdAt <= :end');
        $qb->andWhere('cs.user = :user');

        if (null !== $name) {
            $qb->andWhere('cs.name LIKE :name')
                ->setParameter('name', sprintf('%%%s%%', $name));
        }

        return $qb->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
