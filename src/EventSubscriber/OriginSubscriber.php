<?php


namespace App\EventSubscriber;

use App\Helper\UrlHelpers;
use App\Interfaces\Controller\OriginAuthenticatedController;
use App\Repository\ShopRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class OriginSubscriber implements EventSubscriberInterface
{
    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * OriginSubscriber constructor.
     * @param ShopRepository $shopRepository
     */
    public function __construct(ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof OriginAuthenticatedController) {
            return;
            $shopPartnerNumber = $event->getRequest()->request->get('shop');

            if (!$shopPartnerNumber) {
                throw new AccessDeniedHttpException('Invalid shop partner number passed.');
            }

            if (!$shop = $this->shopRepository->findOneBy(['partnerNumber' => $shopPartnerNumber])) {
                throw new AccessDeniedHttpException('Invalid shop.');
            }

            if (!UrlHelpers::compareUrls((string)$shop->getWebsite(), (string)($_SERVER['HTTP_ORIGIN'] ?? ''))) {
                throw new AccessDeniedHttpException('Invalid origin');
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => 'onKernelController',
        );
    }
}