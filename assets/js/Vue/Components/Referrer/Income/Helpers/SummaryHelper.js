'use strict';

const moneyFactory = require('../../../../Objects/Money').moneyFactory;

function SummaryHelper(items) {
    this.items = items;
    this.purchase_count = this.sumScalar('purchase_count');
    this.used_coupons_count = this.sumScalar('used_coupons_count');
    this.invite_friend_emails_count = this.sumScalar('invite_friend_emails_count');
    this.totalCps = this.sumMoney('total_purchase_value_generated');
    this.totalCpl = this.sumMoney('total_sign_ups_value_generated');
    this.views_count = this.sumScalar('total_number_of_views');
    this.totalReferrerProfit = this.sumMoney('profit');
    this.totalSIProfit =  0;
}

SummaryHelper.prototype.sumScalar = function (fieldName) {
    return _.reduce(this.items, function (memo, item) {
        return memo + item[fieldName];
    }, 0)
};

SummaryHelper.prototype.sumMoney = function (fieldName) {
    return _.reduce(this.items, function (memo, item) {
        return memo.add(item[fieldName] || 0);
    }, moneyFactory('EUR', 0))
};

SummaryHelper.prototype.getAll = function () {
    return {
        totalCps: this.totalCps,
        totalCpl: this.totalCpl,
        totalReferrerProfit: this.totalReferrerProfit,
        totalSIProfit: this.totalSIProfit,
        views_count: this.views_count,
        purchase_count: this.purchase_count,
        used_coupons_count: this.used_coupons_count,
        invite_friend_emails_count: this.invite_friend_emails_count,

    }
};

module.exports = SummaryHelper;