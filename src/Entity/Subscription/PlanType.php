<?php

namespace App\Entity\Subscription;

use App\Entity\BaseGlossary;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class PlanType extends BaseGlossary
{
    const SHOP_SAAS = 1;
    const SHOP_ECOMMERCE = 2;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Subscription\Plan", mappedBy="type")
     */
    private $plans;

    public function __construct()
    {
        $this->plans = new ArrayCollection();
    }

    /**
     * @return Collection|Plan[]
     */
    public function getPlans(): Collection
    {
        return $this->plans;
    }
}
