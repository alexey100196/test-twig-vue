<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\InheritanceType("JOINED")
 * @ORM\Entity(repositoryClass="App\Repository\ShopConversionRepository")
 */
abstract class ShopConversion extends ShopInteraction
{
    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $commission;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CommissionType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $commissionType;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $referrerProfit;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $systemCommission;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OfferType")
     */
    private $offerType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sourceLink;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShopConversionStatusChange", mappedBy="shopConversion")
     */
    private $statusChanges;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShopConversionStatus")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $status;

    public function __construct()
    {
        $this->statusChanges = new ArrayCollection();
    }

    public function getCommission()
    {
        return $this->commission;
    }

    public function setCommission($commission): self
    {
        $this->commission = $commission;

        return $this;
    }

    public function getCommissionType(): ?CommissionType
    {
        return $this->commissionType;
    }

    public function setCommissionType(?CommissionType $commissionType): self
    {
        $this->commissionType = $commissionType;

        return $this;
    }

    public function getReferrerProfit()
    {
        return $this->referrerProfit;
    }

    public function setReferrerProfit($referrerProfit): self
    {
        $this->referrerProfit = $referrerProfit;

        return $this;
    }

    /**
     * @param ReferrerLink|null $referrerLink
     * @return $this|ShopInteraction
     */
    public function setReferrerLink(?ReferrerLink $referrerLink)
    {
        $this->referrerLink = $referrerLink;

        $this->setReferrer($referrerLink->getReferrer());

        return $this;
    }


    public function getSystemCommission()
    {
        return $this->systemCommission;
    }

    public function setSystemCommission($systemCommission): self
    {
        $this->systemCommission = $systemCommission;

        return $this;
    }

    public function getOfferType(): ?OfferType
    {
        return $this->offerType;
    }

    public function setOfferType(?OfferType $offerType): self
    {
        $this->offerType = $offerType;

        return $this;
    }

    public function getSourceLink(): ?string
    {
        return $this->sourceLink;
    }

    public function setSourceLink(?string $sourceLink): self
    {
        $this->sourceLink = $sourceLink;

        return $this;
    }

    public function getStatus(): ?ShopConversionStatus
    {
        return $this->status;
    }

    public function setStatus(?ShopConversionStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatusChanges()
    {
        return $this->statusChanges;
    }

    public function isRejected()
    {
        return $this->getStatus()->getId() === ShopConversionStatus::REJECTED;
    }
}
