<?php

namespace App\Repository;

use App\Entity\CommissionType;
use App\Entity\Product;
use App\Entity\ProductPurchase;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @method ProductPurchase|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductPurchase|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductPurchase[]    findAll()
 * @method ProductPurchase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductPurchaseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductPurchase::class);
    }

    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return mixed
     */
    public function getStats(\DateTime $startDate, \DateTime $endDate)
    {
        return $this->createQueryBuilder('pp')
            ->select('s.id, SUM(pp.amount * pp.price)')
            ->join(Product::class, 'p', Expr\Join::WITH, 'pp.product = p')
            ->join(Shop::class, 's', Expr\Join::WITH, 'p.shop = s')
            ->groupBy('s')
            ->where('pp.createdAt >= :start AND pp.createdAt <= :end')
            ->setParameters(['start' => $startDate, 'end' => $endDate])
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Product $product
     * @param User $user
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return mixed
     */
    public function getAllUserBetweenDates(Product $product, User $user, \DateTime $startDate, \DateTime $endDate)
    {
        return $this->createQueryBuilder('pp')
            ->where('pp.createdAt >= :start AND pp.createdAt <= :end')
            ->andWhere('pp.referrer = :referrer')
            ->andWhere('pp.product = :product')
            ->setParameters([
                'start' => $startDate,
                'end' => $endDate,
                'referrer' => $user,
                'product' => $product,
            ])
            ->getQuery()
            ->getResult();
    }

    public function getProductsStatisticsForReferrer(Product $product, User $user, \DateTime $startDate, \DateTime $endDate)
    {
        return $this->createQueryBuilder('pp')
            ->select('
                rl.slug as referrer_link_slug,
                rl.destinationUrl as referrer_link_destination_url,
                ct.name as commission_type_name,
                pp.price,
                sum(pp.price * pp.amount) as total_purchase,
                sum(pp.amount) as purchase_count,
                sum(pp.referrerProfit) as referrer_profit
            ')
            ->join(
                ReferrerLink::class,
                'rl',
                Join::WITH,
                'pp.referrerLink = rl'
            )
            ->join(
                CommissionType::class,
                'ct',
                Join::WITH,
                'pp.commissionType = ct'
            )
            ->join(
                Product::class,
                'p',
                Join::WITH,
                'pp.product = p'
            )
            ->where('pp.product = :product')
            ->andWhere('pp.createdAt >= :startDate')
            ->andWhere('pp.createdAt <= :endDate')
            ->andWhere('pp.referrer <= :user')
            ->setParameters(compact('product', 'user', 'startDate', 'endDate'))
            ->groupBy('pp.commissionType, pp.price, pp.referrerLink')
            ->getQuery()
            ->getResult();
    }
}
