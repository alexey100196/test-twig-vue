(function () {
    function include(filename) {
        var head = document.getElementsByTagName('head')[0];
        script = document.createElement('script');
        script.src = filename;
        script.type = 'text/javascript';
        head.appendChild(script)
    }
    // var URL = document.getElementsByName('app_url')[0].content + "/";
    var URL = '//marfil-app.local/';
    include(URL + 'snippet/cookie.js');
    window.onload = function() {
        include(URL + "snippet/snippet.js");
        include(URL + 'snippet/widget-init.js');
    }
})();