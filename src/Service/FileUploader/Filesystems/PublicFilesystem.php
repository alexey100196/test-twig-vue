<?php

namespace App\Service\FileUploader\Filesystems;

use App\Service\FileUploader\Filesystems\FilesystemInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 *
 */
class PublicFilesystem implements FilesystemInterface
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path)
    {
        $this->path = $path;
    }

    /**
     * Store file in filesystem.
     * @param UploadedFile $file
     * @param string $name
     * @return File
     */
    public function store(UploadedFile $file, string $name): File
    {
        return $file->move($this->getPath(), $name);
    }
}
