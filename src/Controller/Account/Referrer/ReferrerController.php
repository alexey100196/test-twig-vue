<?php


namespace App\Controller\Account\Referrer;

use App\Form\Account\UpdatePixelsType;
use App\Service\Referrer\ReferrerShop;
use App\Traits\Controller\FormResponseArray;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerIncomeController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerController extends AbstractController
{
    use FormResponseArray;

    /**
     * @var ReferrerShop $referrerShop
     */
    private $referrerShop;

    /**
     * ReferrerShopController constructor.
     * @param ReferrerShop $referrerShop
     */
    public function __construct(ReferrerShop $referrerShop)
    {
        $this->referrerShop = $referrerShop;
    }

    /**
     * @Route("/account/referrer", name="account_referrer_home_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $userShops = $this->referrerShop->getReferrerShopsWithStats($this->getUser());
        $globalStats  = $this->referrerShop->getReferrerStats($this->getUser());

        return $this->render('account/referrer/home/index.html.twig', compact('userShops', 'globalStats'));
    }

    /**
     * @Route("/account/referrer/update-pixels", name="account_referrer_update_pixels", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function updatePixels(Request $request, EntityManagerInterface $entityManager)
    {
        try {
            $user = $this->getUser();

            $form = $this->createForm(UpdatePixelsType::class, $user);
            $form->submit($request->request->all());

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager->persist($form->getData());
                $entityManager->flush();

                return new JsonResponse('Pixels have been saved successfully.', Response::HTTP_OK);
            }

            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return new JsonResponse([$e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}