<?php

namespace App\Entity\CardSet;

use App\Entity\Media;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardSet\CardSetCardBackgroundRepository")
 */
class CardSetCardBackground
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"card-sets", "referrer"})
     */
    private $media;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"card-sets", "referrer"})
     */
    private $width;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"card-sets", "referrer"})
     */
    private $positionLeft;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"card-sets", "referrer"})
     */
    private $positionTop;

    /**
     * @var string
     */
    private $url;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\CardSet\CardSetCard", mappedBy="background", cascade={"persist", "remove"})
     */
    private $card;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMedia(): ?Media
    {
        return $this->media;
    }

    public function setMedia(?Media $media): self
    {
        $this->media = $media;

        return $this;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(?int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getPositionLeft(): ?int
    {
        return $this->positionLeft;
    }

    public function setPositionLeft(int $positionLeft): self
    {
        $this->positionLeft = $positionLeft;

        return $this;
    }

    public function getPositionTop(): ?int
    {
        return $this->positionTop;
    }

    public function setPositionTop(int $positionTop): self
    {
        $this->positionTop = $positionTop;

        return $this;
    }

    public function getCard(): ?CardSetCard
    {
        return $this->card;
    }

    public function setCard(CardSetCard $card): self
    {
        $this->card = $card;

        // set the owning side of the relation if necessary
        if ($this !== $card->getBackground()) {
            $card->setBackground($this);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }
}
