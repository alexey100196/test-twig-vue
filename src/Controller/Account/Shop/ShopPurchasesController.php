<?php

namespace App\Controller\Account\Shop;

use App\Entity\Shop;
use App\Entity\User;
use App\Repository\CampaignRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ShopPurchasesController
 *
 * @Security("has_role('ROLE_SHOP')")
 */
class ShopPurchasesController extends AbstractController
{
    /**
     * @var CampaignRepository
     */
    private $campaignRepository;

    /**
     * ShopPurchasesController constructor.
     * @param CampaignRepository $campaignRepository
     */
    public function __construct(CampaignRepository $campaignRepository)
    {
        $this->campaignRepository = $campaignRepository;
    }

    /**
     * @Route("/account/company/purchases", name="account_shop_purchases")
     */
    public function index()
    {
        /** @var Shop $shop */
        $shop = $this->getUser()->getShop();
        return $this->render('account/shop/purchases/index.html.twig', [
            'campaigns' => $this->campaignRepository->getAllByShopArray($shop)
        ]);
    }
}