<?php


namespace App\Service\IncomeStatistics\ReferrerIncomeStatistics\Entities;


use App\Entity\CommissionType;
use App\Entity\OfferType;
use App\Entity\Product;
use App\Entity\Shop;
use App\Helper\Math;

class ReferrerLinkStatsNew
{
    /**
     * @var string
     */
    private $offerType;

    /**
     * @var string
     */
    private $commissionType;

    /**
     * @var float
     */
    private $commission;

    /**
     * @var int
     */
    private $transactionsCount;

    /**
     * @var int
     */
    private $purchasesAmount;

    /**
     * @var float
     */
    private $purchasesPrice;

    /**
     * @var float
     */
    private $referrerProfit;

    /**
     * @var string
     */
    private $sourceLink;

    /**
     * @var bool
     */
    private $hasExchanges = false;

    /**
     * @param string $offerType
     */
    public function setOfferType(string $offerType): void
    {
        $this->offerType = $offerType;
    }

    /**
     * @param string $commissionType
     */
    public function setCommissionType(string $commissionType): void
    {
        $this->commissionType = $commissionType;
    }

    /**
     * @param float $commission
     */
    public function setCommission(float $commission): void
    {
        $this->commission = $commission;
    }

    /**
     * @param int $transactionsCount
     */
    public function setTransactionsCount(int $transactionsCount): void
    {
        $this->transactionsCount = $transactionsCount;
    }

    /**
     * @param int $purchasesAmount
     */
    public function setPurchasesAmount(int $purchasesAmount): void
    {
        $this->purchasesAmount = $purchasesAmount;
    }

    /**
     * @param float $purchasesPrice
     */
    public function setPurchasesPrice(float $purchasesPrice): void
    {
        $this->purchasesPrice = $purchasesPrice;
    }

    /**
     * @param float $referrerProfit
     */
    public function setReferrerProfit(float $referrerProfit): void
    {
        $this->referrerProfit = $referrerProfit;
    }

    /**
     * @param string $sourceLink
     */
    public function setSourceLink(string $sourceLink): void
    {
        $this->sourceLink = $sourceLink;
    }

    /**
     * @param bool $hasExchanges
     */
    public function setHasExchanges(bool $hasExchanges): void
    {
        $this->hasExchanges = $hasExchanges;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'offer_type' => $this->offerType,
            'commission_type' => $this->commissionType,
            'commission' => $this->commission,
            'transactions_count' => $this->transactionsCount,
            'purchases_amount' => $this->purchasesAmount,
            'purchases_price' => $this->purchasesPrice,
            'referrer_profit' => $this->referrerProfit,
            'source_link' => $this->sourceLink,
            'has_exchanges' => $this->hasExchanges,
        ];
    }
}
