<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Commands;


use App\Entity\Shop;
use App\Service\IncomeStatistics\CommandInterface;

class GenerateReferrersStatistics implements CommandInterface
{
    /**
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @var Shop
     */
    protected $shop;

    /**
     * @var array
     */
    protected $productsIds;

    /**
     * @var bool
     */
    protected $includeMainOffer;

    /**
     * @var string
     */
    protected $filterBy;

    /**
     * @var array
     */
    protected $campaigns;

    /**
     * GenerateStats constructor.
     * @param Shop $shop
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param array $productsIds
     * @param bool $includeMainOffer
     * @param string $filterBy
     * @param null|array $campaigns
     */
    public function __construct(
        Shop $shop,
        \DateTime $startDate,
        \DateTime $endDate,
        array $productsIds = [],
        bool $includeMainOffer = false,
        string $filterBy = '',
        ?array $campaigns = null
    )
    {
        $this->shop = $shop;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->productsIds = $productsIds;
        $this->includeMainOffer = $includeMainOffer;
        $this->filterBy = $filterBy;
        $this->campaigns = $campaigns;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @return array
     */
    public function getProductsIds(): array
    {
        return $this->productsIds;
    }

    /**
     * @return bool
     */
    public function getIncludeMainOffer(): bool
    {
        return $this->includeMainOffer;
    }

    /**
     * @return string
     */
    public function getFilterBy(): string
    {
        return $this->filterBy;
    }

    /**
     * @return array|null
     */
    public function getCampaigns(): array
    {
        return $this->campaigns;
    }
}