<?php


namespace App\Controller\Subscription;

use App\Entity\Subscription\Plan;
use App\Repository\Subscription\PlanRepository;
use App\Service\Subscription\SubscriptionContinuationService;
use App\Service\Subscription\SubscriptionService;
use App\Service\Subscription\SubscriptionValidation\Validators\SubscriptionValidationException;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/subscription")
 */
class SubscriptionController extends AbstractController
{

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * SubscriptionController constructor.
     * @param SubscriptionService $subscriptionService
     */
    public function __construct(SubscriptionService $subscriptionService)
    {
        $this->subscriptionService = $subscriptionService;
    }

    /**
     * @Route("/subscribe/continue", name="subscription_subscribe_continue", methods={"GET"})
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @param PlanRepository $planRepository
     *
     * @return RedirectResponse
     */
    public function subscribeContinueAction(Request $request, PlanRepository $planRepository)
    {
        $planId = (int)$request->cookies->get(Consts::GUEST_SUBSCRIPTION_PLAN_COOKIE_NAME);

        if ($planId && $plan = $planRepository->find($planId)) {
            return $this->subscribeAction($plan, $request);
        }

        return $this->redirectToRoute('home_pricing');
    }

    /**
     * @Route("/subscribe/{plan}", name="subscription_subscribe", methods={"POST"})
     * @param Plan $plan
     * @param Request $request
     * @return RedirectResponse
     */
    public function subscribeAction(Plan $plan, Request $request)
    {
        // @todo: check if user can subscribe plan!

        if ($user = $this->getUser()) {

            try {
                $this->subscriptionService->subscribe($user, $plan);
            } catch (SubscriptionValidationException $exception) {
                return $this->notSubscribedResponse($exception->getMessage(), Consts::SUCCESS);
            } catch (\Exception $exception) {
                return $this->notSubscribedResponse('Global error.');
            }

            return $this->subscribedResponse();
        }

        return $this->rememberPlanAndMoveToLogin($plan, $request->request->get('_target'));
    }

    /**
     * @Route("/expired", name="subscription_expired", methods={"GET"})
     */
    public function expiredAction()
    {
        return $this->render('subscription/expired.html.twig');
    }

    /**
     * @Route("/{plan}/cancel", name="subscription_cancel", methods={"POST"})
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Plan $plan
     * @return JsonResponse
     */
    public function cancelAction(Plan $plan)
    {
        if (!$subscription = $this->getUser()->getActiveSubscriptionForPlan($plan)) {
            $this->addFlash(Consts::ERROR, 'You don\'t have active subscription for this plan.');
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }

        if ($subscription->isCanceled()) {
            $this->addFlash(Consts::ERROR, 'This plan is already canceled.');
            return new JsonResponse([], Response::HTTP_BAD_REQUEST);
        }

        $this->subscriptionService->cancelSubscription($subscription);

        $this->addFlash(Consts::SUCCESS, 'Plan has been canceled');

        return new JsonResponse([], Response::HTTP_OK);
    }

    /**
     * @Route("/continuation/{decision}", name="subscription_continuation", methods={"POST"})
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param int $decision
     * @param SubscriptionContinuationService $continuationService
     *
     * @return JsonResponse
     */
    public function subscriptionContinuation(int $decision, SubscriptionContinuationService $continuationService)
    {
        $user = $this->getUser();

        if ($decision) {
            $continuationService->userWantContinue($user);
        } else {
            $continuationService->userDoesNotWantContinue($user);
        }

        $this->addFlash(Consts::SUCCESS, 'Decision saved');

        return new JsonResponse([]);

    }

    /**
     * @param Plan $plan
     * @param $targetPath string|null
     * @return RedirectResponse
     */
    private function rememberPlanAndMoveToLogin(Plan $plan, string $targetPath = null)
    {
        $targetPath = $targetPath ?: $this->generateUrl('register_shop', ['source' => 'pricing']);
        $response = $this->redirect($targetPath);
        $response->headers->setCookie(new Cookie(Consts::GUEST_SUBSCRIPTION_PLAN_COOKIE_NAME, $plan->getId()));

        return $response;
    }

    /**
     * @return RedirectResponse
     */
    private function subscribedResponse()
    {
        // For now only shop has subscription plans
        // so we redirect user straight to shop account settings.

        return $this->addCookieClearingToResponse($this->redirectToRoute('account_shop_edit'));
    }

    /**
     * @param string $reason
     * @param string $status
     * @return RedirectResponse
     */
    private function notSubscribedResponse(string $reason, string $status = Consts::ERROR)
    {
        $this->addFlash($status, $reason);

        return $this->addCookieClearingToResponse($this->redirectToRoute('home_pricing'));
    }

    private function addCookieClearingToResponse($response)
    {
        $response->headers->clearCookie(Consts::GUEST_SUBSCRIPTION_PLAN_COOKIE_NAME);

        return $response;
    }
}