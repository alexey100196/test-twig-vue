<?php

namespace App\Helper\ShopCompletion\Resolvers;

use App\Helper\ShopCompletion\Resolvers\AbstractResolver;

class OptionalOffer extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        foreach (['BasicData', 'MainOffer'] as $step) {
            if ($this->getResolver()->getResult($step)->getScore() < 100) {
                return;
            }
        }

        if ($this->shop->hasOnlyCouponRewards()) {
            $this->addScore(100);
            return;
        }

        $this->addScore(100);
    }
}
