<?php

namespace App\Repository;

use App\Entity\ReferrerLink;
use App\Entity\ReferrerLinkCard;
use App\Entity\Shop;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReferrerLink|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferrerLink|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferrerLink[]    findAll()
 * @method ReferrerLink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferrerLinkRepository extends ServiceEntityRepository
{
    /**
     * ReferrerLinkRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReferrerLink::class);
    }

    public function findForReferrer(User $user)
    {
        $qb = $this->createQueryBuilder('r');
        $qb = $qb->andWhere('r.referrer = :referrer')->setParameter('referrer', $user);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $slug
     * @return ReferrerLink|null
     */
    public function findNotDeletedBySlug(string $slug):? ReferrerLink
    {
        $qb = $this->createQueryBuilder('r');
        $qb->andWhere('r.slug = :slug')->setParameter('slug', $slug);
        $qb->andWhere('r.deletedAt IS NULL');

        try {
            return $qb->getQuery()->getSingleResult();
        } catch (NoResultException | NonUniqueResultException $exception) {
            return null;
        }
    }

    /**
     * @param User $referrer
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param Shop|null $shop
     * @param bool $includeDeleted
     * @return mixed
     */
    public function findByReferrerAndCardCreationDateRange(
        User $referrer,
        \DateTime $startDate,
        \DateTime $endDate,
        Shop $shop = null,
        bool $includeDeleted = true
    )
    {
        $qb = $this->createQueryBuilder('r')
            ->andWhere('r.referrer = :referrer')
            ->andWhere('r.createdAt >= :startDate')
            ->andWhere('r.createdAt <= :endDate');

        $qb->setParameters(compact('referrer', 'startDate', 'endDate'));

        if (null !== $shop) {
            $qb->andWhere('r.shop = :shop')->setParameter('shop', $shop);
        }

        if (false === $includeDeleted) {
            $qb->andWhere('r.deletedAt IS NULL');
        }

        return $qb->orderBy('r.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function createSoftDeleteQueryBuilder()
    {
        return $this->createQueryBuilder('l')->where('l.deletedAt IS NULL');
    }
}
