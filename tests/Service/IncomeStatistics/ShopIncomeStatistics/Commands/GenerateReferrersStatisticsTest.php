<?php


namespace App\Tests\Service\IncomeStatistics\ShopIncomeStatistics\Commands;


use App\Entity\Shop;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateReferrersStatistics;
use PHPUnit\Framework\TestCase;

class GenerateReferrersStatisticsTest extends TestCase
{
    public function testCommand()
    {
        $start = new \DateTime('2018-01-01 10:01:01');
        $end = new \DateTime('2019-02-01 12:00:01');
        $shop = new Shop();

        $command = new GenerateReferrersStatistics($shop, $start, $end);

        $this->assertEquals($start, $command->getStartDate());
        $this->assertEquals($end, $command->getEndDate());
        $this->assertEquals($shop, $command->getShop());
    }
}