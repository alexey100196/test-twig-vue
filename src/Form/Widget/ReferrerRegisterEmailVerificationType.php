<?php

namespace App\Form\Widget;

use App\Entity\User;
use App\Form\RegisterType;
use App\Form\Widget\FormData\ReferrerRegisterEmailVerification;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class ReferrerRegisterEmailVerificationType extends AbstractType
{
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', EntityType::class, [
                'class' => User::class,
                'choice_value' => 'email'
            ])
            ->add('token');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReferrerRegisterEmailVerification::class,
            'csrf_protection' => false
        ]);
    }
}