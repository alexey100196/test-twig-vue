<?php


namespace App\Controller\Account\Referrer;


use App\Service\Referrer\ReferrerShop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerProgramController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerProgramController extends AbstractController
{
    /**
     * @var ReferrerShop
     */
    protected $referrerShop;

    /**
     * ReferrerShopController constructor.
     * @param ReferrerShop $referrerShop
     */
    public function __construct(ReferrerShop $referrerShop)
    {
        $this->referrerShop = $referrerShop;
    }

    /**
     * Show referrer programs list.
     *
     * @Route("/account/referrer/programs", name="account_referrer_programs_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $userShops = $this->referrerShop->getReferrerShopsWithStats($this->getUser());
        $globalStats = $this->referrerShop->getReferrerStats($this->getUser());

        return $this->render('account/referrer/shop/index.html.twig', [
            'userShops' => $userShops,
            'globalStats' => $globalStats,
        ]);
    }
}