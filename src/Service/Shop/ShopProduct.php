<?php


namespace App\Service\Shop;

use App\Entity\Product;
use App\Entity\Shop;
use App\Event\Shop\ShopChangedSpecialOffer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ShopProduct
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        EntityManagerInterface $em,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Product $product
     * @return Product
     */
    public function save(Product $product)
    {
        $this->em->persist($product);

        if ($this->productOfferChanged($product)) {
            $this->eventDispatcher->dispatch(
                ShopChangedSpecialOffer::NAME,
                new ShopChangedSpecialOffer($product->getShop())
            );
        }

        $this->em->flush();

        return $product;
    }

    private function productOfferChanged(Product $product)
    {
        $uow = $this->em->getUnitOfWork();
        $uow->computeChangeSets();
        $offer = $uow->getEntityChangeSet($product->getCpsOffer());

        return (isset($offer['commission']) && $offer['commission'][0] != $offer['commission'][1]);
    }

    /**
     * @param Shop $shop
     * @param Product $product
     * @return bool
     */
    public function canShopEditProduct(Shop $shop, Product $product)
    {
        return $shop === $product->getShop();
    }

}