<?php

namespace App\Form\Shop;

use App\Form\Type\OfferType;

class CpsOfferType extends OfferType
{
    /**
     * @return null
     */
    public function getBlockPrefix()
    {
        return \App\Entity\OfferType::CPS;
    }
}
