<?php


namespace App\Service\Shop;


use App\Entity\Shop;
use App\Helper\ShopCompletion\ShopCompletionResolver;

class ShopCompletion
{
    /**
     * @param Shop $shop
     * @return bool
     */
    public function isShopFullyCompleted(Shop $shop): bool
    {
        $shopCompletion = new ShopCompletionResolver($shop);
        $shopCompletion->resolve();

        return $shopCompletion->getCompletionPercent() == 100;
    }
}