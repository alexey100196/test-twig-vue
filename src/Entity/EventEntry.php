<?php

namespace App\Entity;

use App\Entity\DocumentReferrer\DocumentReferrer;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventEntryRepository")
 */
class EventEntry
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $payload;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $referrer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="eventEntries", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EventEntryType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ReferrerLink")
     */
    private $referrerLink;

    /**
     * @ORM\Column(type="boolean")
     */
    private $test;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $errors;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DocumentReferrer\DocumentReferrer")
     */
    private $documentReferrer;

    /**
     * @var string|null
     */
    private $httpReferer;

    public function __construct()
    {
        $this->test = 0;

        $this->referrer = null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string|null $key
     * @return null
     */
    public function getPayload(string $key = null)
    {
        if ($key === null) {
            return json_decode($this->payload, 1);
        }

        $payload = json_decode($this->payload, 1);

        return $payload[$key] ?? null;
    }

    public function setPayload($payload): self
    {
        $this->payload = json_encode($payload);

        return $this;
    }

    public function getReferrer(): ?User
    {
        return $this->referrer;
    }

    public function setReferrer(User $referrer): self
    {
        $this->referrer = $referrer;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function getType(): ?EventEntryType
    {
        return $this->type;
    }

    public function setType(?EventEntryType $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Sets createdAt.
     *
     * @param  \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getReferrerLink(): ?ReferrerLink
    {
        return $this->referrerLink;
    }

    public function setReferrerLink(?ReferrerLink $referrerLink): self
    {
        $this->referrerLink = $referrerLink;

        $this->setReferrer($this->referrerLink->getReferrer());

        return $this;
    }

    public function getTest(): bool
    {
        return (bool)$this->test;
    }

    public function setTest(bool $test): self
    {
        $this->test = $test;

        return $this;
    }

    public function getErrors()
    {
        if ($this->errors) {
            return json_decode($this->errors);
        }

        return null;
    }

    public function setErrors(array $errors): self
    {
        $this->errors = json_encode($errors);

        return $this;
    }

    public function getHttpReferer(): ?string
    {
        return $this->httpReferer;
    }

    public function setHttpReferer(?string $httpReferer): self
    {
        $this->httpReferer = $httpReferer;

        return $this;
    }

    public function getDocumentReferrer(): ?DocumentReferrer
    {
        return $this->documentReferrer;
    }

    public function setDocumentReferrer(?DocumentReferrer $documentReferrer): self
    {
        $this->documentReferrer = $documentReferrer;

        return $this;
    }

}
