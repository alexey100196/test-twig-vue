<?php

namespace App\Controller\Account\Shop;

use App\Entity\Shop;
use App\Service\Shop\ShopPreview;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ShopPreviewController extends AbstractController
{
    /**
     * @var ShopPreview
     */
    protected $shopPreview;

    /**
     * ShopPreviewController constructor.
     * @param ShopPreview $shopPreview
     */
    public function __construct(ShopPreview $shopPreview)
    {
        $this->shopPreview = $shopPreview;
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/edit/preview", name="account_shop_edit_preview", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit()
    {
        $shop = $this->getUser()->getShop();

        if ($shop->setWizardStep(4)) {
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->render('account/shop/edit-preview.html.twig', [
            'shop' => $this->getUser()->getShop()
        ]);
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/preview", name="account_shop_preview_show", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function show()
    {
        $shop = $this->getUser()->getShop();

        if (!$this->shopPreview->canPreviewShop($shop)) {
            $this->addFlash(Consts::ERROR, 'You have to complete first 3 steps.');
            return $this->redirectToRoute('account_shop_edit_preview');
        }

        return $this->render('account/shop/preview.html.twig', [
            'shop' => $shop,
            'showShortenedFooter' => true,
        ]);
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/shop/widget/referral/preview", name="account_shop_referral_preview_show", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function showReferral()
    {
        $shop = $this->getUser()->getShop();

        if (!$this->shopPreview->canPreviewReferral($shop)) {
            $this->addFlash(Consts::ERROR, 'You have to complete first 6 steps.');
            return $this->redirectToRoute('account_shop_widget_refferal');
        }

        return $this->render('account/shop/preview-referral.html.twig', [
            'shop' => $shop,
            'showShortenedFooter' => true,
            'company_id' => $shop->getPartnerNumber()
        ]);
    }
}