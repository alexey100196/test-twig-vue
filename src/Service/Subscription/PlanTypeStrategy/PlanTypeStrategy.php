<?php


namespace App\Service\Subscription\PlanTypeStrategy;


use App\Entity\Subscription\PlanSubscription;
use App\Entity\Subscription\PlanType;
use App\Service\Subscription\PlanTypeStrategy\Strategies\ShopEcommerce;
use App\Service\Subscription\PlanTypeStrategy\Strategies\ShopSaas;
use App\Service\Subscription\PlanTypeStrategy\Strategies\StrategyInterface;
use Doctrine\ORM\EntityManagerInterface;

class PlanTypeStrategy
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * PlanTypeStrategy constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @var array|StrategyInterface[]
     */
    protected $strategies = [
        PlanType::SHOP_SAAS => ShopSaas::class,
        PlanType::SHOP_ECOMMERCE => ShopEcommerce::class,
    ];

    /**
     * @param PlanSubscription $planSubscription
     * @return mixed
     */
    public function process(PlanSubscription $planSubscription)
    {
        /** @var PlanType $type */
        $type = $planSubscription->getPlan()->getType();

        if (null !== $type && isset($this->strategies[$type->getId()])) {
            return $this->getStrategyInstance($type->getId())->process($planSubscription);
        }
    }

    /**
     * If you need strategies as DI then override this method.
     *
     * @param int $id
     * @return StrategyInterface
     */
    private function getStrategyInstance(int $id): StrategyInterface
    {
        return new $this->strategies[$id]($this->em);
    }
}
