<?php


namespace App\Service\Subscription\SubscriptionValidation\Validators;

use App\Entity\Subscription\Plan;
use App\Entity\User;

class CanSubscribeIfAlreadySubscribed implements ValidatorInterface
{
    /**
     * {@inheritdoc}
     */
    public function validate(User $user, Plan $plan): void
    {
        if ($user->isSubscribedButNotOnTrial()) {
            throw new SubscriptionValidationException('You are already on paid plan. Wait till the end.');
        }
    }
}