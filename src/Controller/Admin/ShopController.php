<?php

namespace App\Controller\Admin;

use App\Entity\Currency;
use App\Entity\ShopStatus;
use App\Entity\UserBalance;
use App\Form\Admin\Shop\EditShopType;
use App\Form\Balance\UserTransactionType;
use App\Repository\ShopRepository;
use App\Service\Balance\UserBalanceService;
use App\Util\Consts;
use App\ValueObject\DatePeriod;
use Carbon\Carbon;
use Knp\Component\Pager\PaginatorInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ShopController extends AbstractController
{
    const PER_PAGE = 25;

    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * @param ShopRepository $shopRepository
     */
    public function __construct(ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    /**
     * @Route("/admin/company/{year}/{month}", name="admin_shop_index", methods={"GET"})
     * @param int|null $year
     * @param int|null $month
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(int $year = null, int $month = null)
    {
        $datePeriod = $this->buildDatePeriodFromYearAndMonth($year, $month);

        $shops = $this->shopRepository
            ->findAllWithStatusBetweenDates(
                [ShopStatus::ACTIVE_NAME, ShopStatus::SUSPENDED_NAME],
                null,
                $datePeriod->getEnd()
            );

        return $this->render('admin/shop/index.html.twig', [
            'shops' => $shops,
            'currentPeriod' => $datePeriod,
        ]);
    }

    /**
     * @param null $year
     * @param null $month
     * @return DatePeriod
     */
    private function buildDatePeriodFromYearAndMonth($year = null, $month = null): DatePeriod
    {
        $year = $year ? intval($year) : date('Y');
        $month = $month ? intval($month) : date('m');

        return new DatePeriod(
            Carbon::create($year, $month)->startOfMonth(),
            Carbon::create($year, $month)->endOfMonth()
        );
    }
}
