<?php


namespace App\Service\Shop;


use App\Entity\Shop;
use App\Repository\ShopRepository;
use Doctrine\ORM\EntityManagerInterface;

class ShopSubdomain
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * ReferrerNickname constructor.
     * @param EntityManagerInterface $em
     * @param ShopRepository $shopRepository
     */
    public function __construct(EntityManagerInterface $em, ShopRepository $shopRepository)
    {
        $this->em = $em;
        $this->shopRepository = $shopRepository;
    }

    /**
     * @param Shop $shop
     * @return Shop
     */
    public function updateSubdomain(Shop $shop)
    {
        $number = null;

        while ($this->shopRepository->findBy(['urlName' => $subdomain = $this->generateSubdomain($shop, $number)])) {
            $number = $number === null ? 1 : $number + 1;
        }

        $shop->setUrlName($subdomain);
        $this->em->flush();

        return $shop;
    }

    /**
     * @param Shop $shop
     * @param int|null $number
     * @return string
     */
    private function generateSubdomain(Shop $shop, int $number = null)
    {
        $subdomain = str_replace(' ', '-', $shop->getName());
        $subdomain = preg_replace('/[^A-Za-z0-9\-\_]/', '', $subdomain);
        $subdomain = strtolower($subdomain);

        return $subdomain . ($number ? '-' . $number : '');
    }
}