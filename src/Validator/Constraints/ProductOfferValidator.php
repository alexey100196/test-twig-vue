<?php


namespace App\Validator\Constraints;


use App\Entity\Product;
use App\Repository\OfferRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ProductOfferValidator extends ConstraintValidator
{
    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $productOffer = $value->getCpsOffer();
        $shopOffer = $value->getShop()->getCpsOffer();

        // If offer exists...
        if ($productOffer && $shopOffer) {
            // And if offer commission type is the same...
            if ($productOffer->getCommissionType() === $shopOffer->getCommissionType()) {
                // And if product offer is less advantageous than shop offer...
                if ($productOffer->getCommission() < $shopOffer->getCommission()) {
                    // Then not allow.
                    $this->context
                        ->buildViolation($constraint->message)
                        ->addViolation();
                }
            }
        }

        unset($productOffer, $shopOffer);
    }
}