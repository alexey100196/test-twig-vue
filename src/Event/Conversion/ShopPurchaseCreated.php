<?php

namespace App\Event\Conversion;

use App\Entity\ShopConversion;
use App\Entity\ShopPurchase;
use Symfony\Component\EventDispatcher\Event;

class ShopPurchaseCreated extends Event implements ConversionEventInterface
{
    /** @var string */
    const NAME = 'conversion.shop-purchase-created';

    private $shopPurchase;

    public function __construct(ShopPurchase $shopPurchase)
    {
        $this->shopPurchase = $shopPurchase;
    }

    /**
     * @return ShopPurchase
     */
    public function getConversion(): ShopConversion
    {
        return $this->shopPurchase;
    }
}