<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class ProductOffer extends Constraint
{
    public $message = 'Special Offer should be greater than main offer.';

    public function validatedBy()
    {
        return ProductOfferValidator::class;
    }
}