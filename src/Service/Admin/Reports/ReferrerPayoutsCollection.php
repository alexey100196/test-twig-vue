<?php


namespace App\Service\Admin\Reports;


use Doctrine\Common\Collections\ArrayCollection;

class ReferrerPayoutsCollection extends ArrayCollection
{
    /**
     * @var float
     */
    private $total = 0;

    /**
     * {@inheritdoc}
     */
    public function __construct(array $elements = [])
    {
        parent::__construct($elements);
        foreach ($elements as $element) {
            $this->add($element);
        }
    }

    /**
     * @param ReferrerPayout $element
     */
    public function add($element)
    {
        if (null !== $index = $this->getIndexByReferrerEmail($element->getReferrerEmail())) {
            $item = $this->offsetGet($index);
            $item->addAmount($element->getAmount());
            $this->offsetSet($index, $item);
        } else {
            parent::add($element);
        }

        $this->total += $element->getAmount();
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    private function getIndexByReferrerEmail(string $email)
    {
        foreach ($this->toArray() as $index => $element) {
            if ($element->getReferrerEmail() === $email) {
                return $index;
            }
        }
    }
}