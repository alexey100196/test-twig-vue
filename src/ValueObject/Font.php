<?php


namespace App\ValueObject;


class Font
{
    const FONTS = [
        'Laila',
        'Lato',
        'Montserrat',
        'Open Sans',
        'Oswald',
        'Raleway',
        'Roboto',
        'Source Sans',
        'Srisakdi',
    ];

    public function __construct(string $font)
    {
        if (!in_array($font, self::FONTS)) {
            throw new \Exception('Invalid font given. Allowed fonts: ' . implode(', ', self::FONTS));
        }

        $this->font = $font;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->font;
    }

    public function __toString()
    {
        return $this->getName();
    }
}