<?php

namespace App\Helper\ShopCompletion\Resolvers;

use App\Helper\ShopCompletion\Resolvers\AbstractResolver;

class Budget extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        if ($this->shop->hasOnlyCouponRewards()) {
            $this->addScore(100);
            return;
        }

        if ($this->shop->getBudgets()->count()) {
            $this->addScore(100);
        }
    }
}
