<?php

namespace App\Helper\WidgetCompletion\Resolvers;

use App\Helper\WidgetCompletion\Resolvers\AbstractResolver;

class IconData extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        if ($this->widgetShopSetting->getShop()->getWidgetButton()) {
            $this->addScore(100);
        }
    }
}