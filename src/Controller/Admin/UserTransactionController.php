<?php

namespace App\Controller\Admin;

use App\Entity\Currency;
use App\Entity\TransactionType;
use App\Entity\User;
use App\Event\Shop\ShopStatusChangedByAdmin;
use App\Form\Balance\UserTransactionType;
use App\Service\Balance\UserBalanceService;
use App\Service\Shop\ShopStatusService;
use App\Traits\Controller\FormResponseArray;
use App\Util\Consts;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserTransactionController extends AbstractController
{
    use FormResponseArray;

    private $balanceService;

    private $em;

    private $shopStatusService;

    private $eventDispatcher;

    public function __construct(
        UserBalanceService $balanceService,
        EntityManagerInterface $em,
        ShopStatusService $shopStatusService,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->balanceService = $balanceService;
        $this->em = $em;
        $this->shopStatusService = $shopStatusService;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/admin/user/{user}/transaction", name="admin_user_transaction_store")
     * @param User $user
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function store(User $user, Request $request)
    {
        $form = $this->createForm(UserTransactionType::class);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_BAD_REQUEST);
        }

        $data = $form->getData();

        /** @var Uuid $uuid */
        $uuid = Uuid::uuid4();

        /** @var TransactionType $depositType */
        $depositType = $this->em->getReference(TransactionType::class, TransactionType::SHOP_DEPOSIT);

        $this->balanceService->addTransaction(
            $uuid,
            $user,
            $data['currency'],
            (string)$data['amount'],
            $depositType
        );

        if ($user->getShop() && $this->balanceService->hasFunds($user, $data['currency'])) {
            $this->shopStatusService->makeShopActiveIfSubscriptionActive($user->getShop());
            $this->eventDispatcher->dispatch(ShopStatusChangedByAdmin::NAME, new ShopStatusChangedByAdmin($user->getShop()));
        }

        $this->addFlash(Consts::SUCCESS, 'Transaction processed');


        return new JsonResponse(['uuid' => (string)$uuid]);
    }
}
