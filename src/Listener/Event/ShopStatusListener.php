<?php


namespace App\Listener\Event;


use App\Adapter\MailSender;
use App\Entity\Shop;
use App\Event\Shop\ShopStatusChangedByAdmin;
use App\Service\Shop\ShopSubdomain;
use Psr\Log\LoggerInterface;

class ShopStatusListener
{
    /**
     * @var MailSender
     */
    private $mailSender;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ShopSubdomain
     */
    private $shopSubdomain;

    public function __construct(
        \Twig_Environment $twig,
        MailSender $mailSender,
        LoggerInterface $logger,
        ShopSubdomain $shopSubdomain
    )
    {
        $this->twig = $twig;
        $this->mailSender = $mailSender;
        $this->logger = $logger;
        $this->shopSubdomain = $shopSubdomain;
    }

    /**
     * @param ShopStatusChangedByAdmin $event
     */
    public function onStatusChanged(ShopStatusChangedByAdmin $event)
    {
        try {
            if ($event->getShop()->isActive() && null === $event->getShop()->getUrlName()) {
                $this->generateUrlName($event->getShop());
            }
            $this->sendEmailToShop($event->getShop());
        } catch (\Exception $e) {
            $this->logger->critical('Can\'t send email after shop status change by admin.', [
                'exc' => $e->getMessage(),
                'shop' => $event->getShop()->getId(),
            ]);
        }
    }

    protected function generateUrlName(Shop $shop)
    {
        $this->shopSubdomain->updateSubdomain($shop);
    }

    protected function sendEmailToShop(Shop $shop)
    {
        if ($shop->isActive()) {
            $this->sendEmailToShopAboutAcceptance($shop);
        } elseif ($shop->isRejected()) {
            $this->sendEmailToShopAboutRejection($shop);
        }
    }


    protected function sendEmailToShopAboutRejection(Shop $shop)
    {
        $this->mailSender->send(
            'hello@superinterface.com',
            $shop->getUser()->getEmail(),
            'Your submission has been rejected.',
            $this->twig->render('email/shop/submission-rejected.html.twig', ['user' => $shop->getUser()])
        );
    }

    protected function sendEmailToShopAboutAcceptance(Shop $shop)
    {
        $this->mailSender->send(
            'hello@superinterface.com',
            $shop->getUser()->getEmail(),
            'Your submission has been accepted.',
            $this->twig->render('email/shop/submission-accepted.html.twig', ['user' => $shop->getUser()])
        );
    }
}