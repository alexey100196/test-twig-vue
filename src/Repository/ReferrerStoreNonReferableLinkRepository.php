<?php

namespace App\Repository;

use App\Entity\ReferrerStoreNonReferableLink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReferrerStoreNonReferableLink|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferrerStoreNonReferableLink|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferrerStoreNonReferableLink[]    findAll()
 * @method ReferrerStoreNonReferableLink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferrerStoreNonReferableLinkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReferrerStoreNonReferableLink::class);
    }

    // /**
    //  * @return ReferrerStoreNonReferableLink[] Returns an array of ReferrerStoreNonReferableLink objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReferrerStoreNonReferableLink
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
