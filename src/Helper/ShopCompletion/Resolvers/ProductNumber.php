<?php

namespace App\Helper\ShopCompletion\Resolvers;

use App\Helper\ShopCompletion\Resolvers\AbstractResolver;

class ProductNumber extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        if ($this->getResolver()->getResult('OptionalOffer')->getScore() < 100) {
            return;
        }

        if ($this->shop->hasOnlyCouponRewards()) {
            $this->addScore(100);
            return;
        }

        $this->addScore(100);
    }
}
