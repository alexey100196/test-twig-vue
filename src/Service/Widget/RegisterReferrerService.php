<?php


namespace App\Service\Widget;


use App\Adapter\MailSender;
use App\Entity\PasswordReminder;
use App\Entity\Role;
use App\Entity\User;
use App\Repository\RoleRepository;
use App\Service\User\EmailConfirmation;
use App\Util\TokenGenerator\NumericTokenGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Yaml\Yaml;

class RegisterReferrerService
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EmailConfirmation
     */
    private $emailConfirmation;

    /**
     * @var MailSender
     */
    private $mailSender;

    /**
     * @var \Twig_Environment
     */
    private $templating;
    /**
     * @var RoleRepository
     */
    private $roleRepository;
    /**
     * @var NumericTokenGenerator
     */
    private $numericTokenGenerator;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        EmailConfirmation $emailConfirmation,
        EntityManagerInterface $em,
        MailSender $mailSender,
        \Twig_Environment $templating,
        RoleRepository $roleRepository,
        NumericTokenGenerator $numericTokenGenerator
    )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->emailConfirmation = $emailConfirmation;
        $this->em = $em;
        $this->mailSender = $mailSender;
        $this->templating = $templating;
        $this->roleRepository = $roleRepository;
        $this->numericTokenGenerator = $numericTokenGenerator;
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function generateRandomPassword(): string
    {
        return bin2hex(random_bytes(10));
    }

    private function getTranslations(string $lang)
    {
        return Yaml::parseFile(__DIR__ . '/../../../translations/widget.' . $lang . '.yaml');
    }

    /**
     * @param User $user
     * @param string $lang
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function createAndSendEmailConfirmationToken(User $user, string $lang = 'en')
    {
        //DISABLE EMAIL CODE: For Widget no Email with Ver.Code
        $translation = $this->getTranslations($lang);
        $token = $this->emailConfirmation->createToken($user);
 /*       $this->mailSender->send('hello@superinterface.com',
            $user->getEmail(),
            $translation['verify_email_address'],
            $this->templating->render('email/email-confirmation-numeric.html.twig', [
                'token' => $token->getToken(),
                'user' => $user,
                'translation' => $translation,
            ])
        );*/
    }

    /**
     * @param User $user
     * @param string $lang
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function createAndSendEmailSetPassword(User $user, string $lang = 'en')
    {
        $translation = $this->getTranslations($lang);

        $this->mailSender->send('hello@superinterface.com',
            $user->getEmail(),
            $translation['set_your_password'],
            $this->templating->render('email/set-password.html.twig', [
                'hash' => $this->generateHash($user),
            ])
        );
    }

    /**
     * @param User $user
     * @return string
     * @throws \Exception
     */
    private function generateHash(User $user) :string
    {
        $hash = bin2hex(random_bytes(32));

        $passwordReminder = new PasswordReminder();
        $passwordReminder->setUser($user);
        $passwordReminder->setHash($hash);

        $this->em->persist($passwordReminder);
        $this->em->flush();

        return $hash;
    }

    /**
     * @param User $user
     * @param string $lang
     * @param bool $setPasswordViaEmail
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function registerUser(User $user, string $lang = 'en', bool $setPasswordViaEmail = false)
    {
        $this->em->persist($user);

        if ($setPasswordViaEmail) {

           $this->createAndSendEmailSetPassword($user, $lang);
        } else {
            $this->createAndSendEmailConfirmationToken($user, $lang);
        }
    }

    /**
     * @return User
     * @throws \Exception
     */
    private function createNewUserInstance(): User
    {
        $role = $this->roleRepository->findOneBy(['name' => Role::ROLE_REFERRER]);

        $user = new User();
        $user->setTermsAcceptedAt(new \DateTime());
        $user->setRole($role);

        return $user;
    }

    /**
     * @param string $email
     * @param string|null $name
     * @param string $lang
     * @param bool $setPasswordViaEmail
     * @return User $user
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function createBasicAccount(string $email, string $name = null, string $lang = 'en', bool $setPasswordViaEmail = false)
    {
        $user = $this->createNewUserInstance();
        $user->setEmail($email);
        if ($name) {
            $nameArr = explode(' ', $name);
            if (count($nameArr) > 1) {
                $user->setName(ucfirst($nameArr[0]));
                $user->setSurname(ucfirst($nameArr[1]));
            } else {
                $user->setName(ucfirst($name));
            }
        }
        $user->setNickname($this->generateNickname());
        $user->setPassword($this->passwordEncoder->encodePassword($user, $this->generateRandomPassword()));

        $this->registerUser($user, $lang, $setPasswordViaEmail);

        return $user;
    }

    private function generateNickname(): string
    {
        return $this->numericTokenGenerator->generate(20);
    }

    /**
     * @param string $email
     * @param string $name
     * @param string $surname
     * @param string $password
     * @throws \Exception
     */
    public function createFinancialAccount(string $email, string $name, string $surname, string $password, string $lang = 'en')
    {
        $user = $this->createNewUserInstance();
        $user->setEmail($email);
        $user->setName(ucfirst($name));
        $user->setSurname(ucfirst($name));
        $user->setNickname($this->generateNickname());

        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user, $password
            )
        );

        $this->registerUser($user, $lang);
    }

    /**
     * @param User $user
     * @param string $token
     */
    public function activateUserAccountByToken(User $user, string $token)
    {
        $this->emailConfirmation->activateUserByToken($token, $user->getId());
    }
}