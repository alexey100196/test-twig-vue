<?php


namespace App\Service\Subscription;


use App\Adapter\MailSender;
use App\Entity\Shop;
use App\Entity\Subscription\PlanSubscription;
use App\Event\Subscription\SubscriptionExpired;
use App\Service\Shop\ShopStatusService;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ShopSubscriptionService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var ShopStatusService
     */
    private $shopStatusService;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var MailSender
     */
    private $mailSender;

    /**
     * @var \Twig_Environment
     */
    private $templating;

    public function __construct(
        EntityManagerInterface $em,
        SubscriptionService $subscriptionService,
        ShopStatusService $shopStatusService,
        EventDispatcherInterface $eventDispatcher,
        MailSender $mailSender,
        \Twig_Environment $templating

    )
    {
        $this->em = $em;
        $this->subscriptionService = $subscriptionService;
        $this->shopStatusService = $shopStatusService;
        $this->eventDispatcher = $eventDispatcher;
        $this->mailSender = $mailSender;
        $this->templating = $templating;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     */
    public function processAllEndedSubscriptions()
    {
        $subscriptions = $this->subscriptionService->getAllExpiredSubscriptions();

        foreach ($subscriptions as $subscription) {
            $this->processEndedSubscription($subscription);
        }
    }

    public function notifyUserAboutEndedTrial(PlanSubscription $planSubscription)
    {
        $this->mailSender->send('hello@superinterface.com',
            $planSubscription->getUser()->getEmail(),
            'Your trial plan expired.',
            $this->templating->render('email/shop/subscription-trial-expired.html.twig', [
                'subscription' => $planSubscription,
            ])
        );
    }

    /**
     * @param PlanSubscription $subscription
     * @throws \Doctrine\ORM\ORMException
     */
    protected function processEndedSubscription(PlanSubscription $subscription)
    {
        if (!$subscription->isEnded()) {
            throw new LogicException('Subscription ' . $subscription->getId() . ' is not ended.');
        }

        /** @var Shop $shop */
        $shop = $subscription->getUser()->getShop();

        if ($shop->isActive()) {
            $this->eventDispatcher->dispatch(SubscriptionExpired::NAME, new SubscriptionExpired($subscription));
            $this->shopStatusService->makeShopSuspended($shop);
        }
    }
}