<?php

namespace App\Listener\Event;

use App\Adapter\MailSender;
use App\Event\User\ReferrerRegistered;
use App\Event\User\UserRegistered;
use App\Repository\UserRepository;
use App\Service\Referrer\ReferrerNickname;
use App\Service\User\EmailConfirmation;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;


class ReferrerRegistrationListener
{
    /**
     * @var ReferrerNickname
     */
    private $referrerNickname;

    /**
     * UserRegistrationListener constructor.
     * @param ReferrerNickname $referrerNickname
     */
    public function __construct(ReferrerNickname $referrerNickname)
    {
        $this->referrerNickname = $referrerNickname;
    }

    /**
     * @param ReferrerRegistered $referrerRegistered
     */
    public function generateNickname(ReferrerRegistered $referrerRegistered)
    {
        $this->referrerNickname->updateNickname($referrerRegistered->getUser());
    }
}
