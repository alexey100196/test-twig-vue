<?php


namespace App\Controller\Ajax;


use App\Controller\AjaxAbstractController;
use App\Entity\Campaign;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\Widget\WidgetShopSetting;
use App\Form\Shop\CampaignType;
use App\Form\Shop\Widget\WidgetRewardSettingsType;
use App\Repository\CampaignRepository;
use App\Repository\Widget\WidgetShopSettingRepository;
use App\Service\CampaignService;
use App\Service\OpenGraph\OpenGraphMetaData;
use App\Service\Referrer\ReferrerLinkStoreCardService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class WidgetRewardsController
 * @Security("has_role('ROLE_SHOP')")
 */
class WidgetRewardsController extends AjaxAbstractController
{
    /** @var CampaignRepository */
    private $campaignRepository;

    /** @var CampaignService */
    private $campaignService;

    /** @var WidgetShopSettingRepository */
    private $widgetShopSettingRepository;

    public function __construct(
        CampaignRepository $campaignRepository,
        WidgetShopSettingRepository $widgetShopSettingRepository,
        CampaignService $campaignService
    )
    {
        $this->campaignRepository = $campaignRepository;
        $this->widgetShopSettingRepository = $widgetShopSettingRepository;
        $this->campaignService = $campaignService;
    }

    /**
     * @Route(
     *     "/ajax/widget/rewards",
     *     name="ajax_shop_widget_rewards_update",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createOrUpdate(Request $request)
    {
        $shop = $this->getUser()->getShop();
        $data = $request->request->all();

        $campaign_list[] = $shop->getCampaignsArray();

        $campaign = new Campaign();
        if (isset($data['id']) && (int)$data['id'] > 0) {
            $campaign = $this->campaignRepository->find($data['id']);

            if ($campaign->getShop()->getId() !== $shop->getId()) {
                return new JsonResponse([
                    'errors' => ['Invalid a shop id for the campaign']
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
        } else {
            $campaign->setShop($shop);
        }

        $form = $this->createForm(WidgetRewardSettingsType::class, $widgetSetting = $campaign->getWidgetShopSettingOrCreateNew());
        $form->submit($data['setting']);

        if (!$form->isValid()) {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $this->updateRewardsMode($shop, $campaign, $form->getData());

        $campaign->setWidgetShopSetting($form->getData());
        $campaignForm = $this->createForm(CampaignType::class, $campaign);
        $campaignForm->submit([
            '_token' => $data['_token'],
            'name' => $data['campaign_name']
        ]);

        if (!$campaignForm->isValid()) {
            return new JsonResponse($this->getFormErrorMessages($campaignForm), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

       if ( sizeof($campaign_list[0]) >= Campaign::MAX_CAMPAING_LIST) {
           return new JsonResponse([
               'errors' => ['Can not create new campaing. Campaign limit is: '.Campaign::MAX_CAMPAING_LIST]
           ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        try {
            $this->campaignService->save($campaignForm->getData());
        } catch (\Exception $e) {
            return new JsonResponse([
                'errors' => [$e->getMessage()]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->jsonResponse([
            'message' => 'Campaign has been '.((isset($data['id']) && (int)$data['id'] > 0) ? 'updated' : 'created').' successfully.',
            'campaigns' => $shop->getCampaignsArray(),
            'campaignsLimit' => Campaign::MAX_CAMPAING_LIST
        ]);
    }

    /**
     * @Route(
     *     "/ajax/widget/rewards/remove/{campaign}",
     *     name="ajax_shop_widget_rewards_remove",
     *     methods={"POST"}
     * )
     * @param Campaign $campaign
     * @return JsonResponse
     */
    public function remove(Campaign $campaign)
    {

        $shop = $this->getUser()->getShop();

        if ($campaign->getShop()->getId() !== $shop->getId()) {
            return new JsonResponse([
                'errors' => [
                    'You don\'t have permission to remove this campaign.'
                ]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $this->campaignService->remove($campaign);
        } catch (\Exception $e) {
            return new JsonResponse([
                'errors' => [$e->getMessage()]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->jsonResponse([
            'message' => 'Campaign has been removed successfully.',
        ]);
    }

    /**
     * @Route(
     *     "/ajax/widget/rewards/activate/{campaign}",
     *     name="ajax_shop_widget_rewards_activate",
     *     methods={"POST"}
     * )
     * @param Campaign $campaign
     * @return JsonResponse
     */
    public function activate(Campaign $campaign)
    {
        $shop = $this->getUser()->getShop();

        if ($campaign->getShop()->getId() !== $shop->getId()) {
            return new JsonResponse([
                'errors' => [
                    'You don\'t have permission to activate this campaign.'
                ]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $campaigns = $this->campaignRepository->findActiveByShop($shop);
            if (!empty($campaigns)) {
                foreach($campaigns as $cp) {
                    if ($cp->getId() === $campaign->getId()) continue;
                    $this->campaignService->deactivate($cp);
                }
            }
            $this->campaignService->activate($campaign);
        } catch (\Exception $e) {
            return new JsonResponse([
                'errors' => [$e->getMessage()]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->jsonResponse([
            'message' => 'Campaign has been activated successfully.',
            'campaigns' => $shop->getCampaignsArray(),
        ]);
    }

    /**
     * @Route(
     *     "/ajax/widget/rewards/deactivate",
     *     name="ajax_shop_widget_rewards_deactivate",
     *     methods={"POST"}
     * )
     * @return JsonResponse
     */
    public function deactivate()
    {
        $shop = $this->getUser()->getShop();

        try {
            $campaigns = $this->campaignRepository->findActiveByShop($shop);
            if (empty($campaigns)) {
                return new JsonResponse([
                    'errors' => ['Not found campaign to deactivate']
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            foreach($campaigns as $cp) {
                $to_deactivate = $this->campaignRepository->find($cp->getId());
            $this->campaignService->deactivate($to_deactivate);
        }
        } catch (\Exception $e) {
            return new JsonResponse([
                'errors' => [$e->getMessage()]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return $this->jsonResponse([
            'message' => 'Campaign has been deactivated successfully.',
            'campaigns' => $shop->getCampaignsArray()
        ]);
    }

    private function updateRewardsMode(Shop $shop, Campaign $campaign, WidgetShopSetting $widgetShopSetting)
    {
        if ($widgetShopSetting->getReferrerRewardCashEnabled() && $widgetShopSetting->getReferrerRewardCouponEnabled()) {
            $rewardsMode = Shop::REWARD_MODE_COUPON_CODES_AND_CASH;
        } else {
            $rewardsMode = $widgetShopSetting->getReferrerRewardCashEnabled() ? Shop::REWARD_MODE_CASH : Shop::REWARD_MODE_COUPON_CODES;
        }

        if ($rewardsMode === Shop::REWARD_MODE_COUPON_CODES && $shop->getUser()->hasAnyFunds('EUR')) {
            $rewardsMode = Shop::REWARD_MODE_COUPON_CODES_AND_CASH;
        }

        $shop->setRewardsMode($rewardsMode);
        $campaign->setRewardsMode($rewardsMode);
    }
}