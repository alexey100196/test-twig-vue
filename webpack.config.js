var Encore = require('@symfony/webpack-encore');

Encore
    .configureRuntimeEnvironment(`env`)
    .enableVueLoader()
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .cleanupOutputBeforeBuild()
    .autoProvidejQuery()
    .enableVersioning()
    .addEntry('js/app', './assets/js/app.js')
    .splitEntryChunks()
;

module.exports = Encore.getWebpackConfig();


