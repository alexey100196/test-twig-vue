<?php


namespace App\Service\Subscription\SubscriptionValidation;


use App\Entity\Subscription\Plan;
use App\Entity\User;
use App\Service\Subscription\SubscriptionValidation\Validators\CanSubscribeIfAlreadySubscribed;
use App\Service\Subscription\SubscriptionValidation\Validators\CanSubscribePlanWithType;
use App\Service\Subscription\SubscriptionValidation\Validators\CanSubscribeTheSamePlan;
use App\Service\Subscription\SubscriptionValidation\Validators\ValidatorInterface;

class SubscriptionValidator
{
    const SUBSCRIBE = 'subscribe';

    /**
     * @var array|ValidatorInterface[]
     */
    protected $validators = [
        self::SUBSCRIBE => [
            CanSubscribeTheSamePlan::class,
            CanSubscribePlanWithType::class,
            CanSubscribeIfAlreadySubscribed::class,
        ]
    ];

    /**
     * @param User $user
     * @param Plan $plan
     * @param string $validationStrategy
     * @throws \Exception
     */
    public function validate(User $user, Plan $plan, string $validationStrategy)
    {
        if (isset($this->validators[$validationStrategy]) === false) {
            throw $this->invalidStrategyException($validationStrategy);
        }

        foreach ($this->validators[$validationStrategy] as $validatorClass) {
            /** @var ValidatorInterface $validator */
            $validator = new $validatorClass();
            $validator->validate($user, $plan);
            unset($validator);
        }
    }

    /**
     * @param $validationStrategy
     * @return \Exception
     */
    private function invalidStrategyException($validationStrategy)
    {
        return new \Exception(
            'Invalid validation {' . $validationStrategy . '} strategy. Allowed strategies: ' . implode(', ', array_keys($this->validators))
        );
    }
}