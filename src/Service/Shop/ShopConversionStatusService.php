<?php


namespace App\Service\Shop;


use App\Entity\ShopConversion;
use App\Entity\ShopConversionStatus;
use App\Entity\ShopConversionStatusChange;
use App\Entity\User;
use App\Event\Conversion\ConversionTimeForAcceptanceExpired;
use App\Event\Conversion\ShopConversionAcceptedAfterRejection;
use App\Event\Conversion\ShopConversionRejected;
use App\Repository\ShopConversionRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ShopConversionStatusService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var ShopConversionRepository
     */
    private $shopConversionRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        EntityManagerInterface $em,
        EventDispatcherInterface $eventDispatcher,
        ShopConversionRepository $shopConversionRepository,
        LoggerInterface $logger
    )
    {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
        $this->shopConversionRepository = $shopConversionRepository;
        $this->logger = $logger;
    }

    /**
     * @param ShopConversion $shopConversion
     * @param int $status
     * @param User $user
     * @param string|null $reason
     * @throws \Doctrine\ORM\ORMException
     */
    public function changeStatus(ShopConversion $shopConversion, int $status, User $user, string $reason = null)
    {
        if (in_array($status, ShopConversionStatus::getAll())) {
            /** @var ShopConversionStatus $statusReference */
            $statusReference = $this->em->getReference(ShopConversionStatus::class, $status);
            $this->rememberChange($shopConversion, $shopConversion->getStatus(), $statusReference, $user, $reason);

            if ($status === ShopConversionStatus::ACCEPTED && $shopConversion->isRejected()) {
                $this->eventDispatcher->dispatch(ShopConversionAcceptedAfterRejection::NAME, new ShopConversionAcceptedAfterRejection($shopConversion));
            } elseif ($status === ShopConversionStatus::REJECTED) {
                $this->eventDispatcher->dispatch(ShopConversionRejected::NAME, new ShopConversionRejected($shopConversion));
            }

            $shopConversion->setStatus($statusReference);

            $this->em->flush();
        }
    }

    public function acceptConversionsDatePeriod(\DateTime $start, \DateTime $end)
    {
        /** @var ShopConversionStatus $unverifiedStatusReference */
        $unverifiedStatus = $this->em->getReference(ShopConversionStatus::class, ShopConversionStatus::UNVERIFIED);

        /** @var ShopConversionStatus $unverifiedStatusReference */
        $acceptedStatus = $this->em->getReference(ShopConversionStatus::class, ShopConversionStatus::ACCEPTED);

        $conversions = $this->shopConversionRepository->getByDatePeriod($start, $end, $unverifiedStatus);

        foreach ($conversions as $conversion) {
            $this->storeStatusChangeInLogs($conversion, $unverifiedStatus, $acceptedStatus);

            $conversion->setStatus($acceptedStatus);
        }

        $this->em->flush();

        $this->eventDispatcher->dispatch(ConversionTimeForAcceptanceExpired::NAME, new ConversionTimeForAcceptanceExpired($start, $end));
    }

    /**
     * @param ShopConversion $conversion
     * @param ShopConversionStatus $before
     * @param ShopConversionStatus $after $before
     */
    private function storeStatusChangeInLogs(ShopConversion $conversion, ShopConversionStatus $before, ShopConversionStatus $after)
    {
        $this->logger->info('Changing status for conversion #' . $conversion->getId(), [
            'before' => $before->getName(),
            'after' => $after->getName()
        ]);
    }

    /**
     * @param ShopConversion $shopConversion
     * @param ShopConversionStatus $statusBefore
     * @param ShopConversionStatus $statusAfter
     * @param User $user
     * @param string|null $reason
     */
    private function rememberChange(
        ShopConversion $shopConversion,
        ShopConversionStatus $statusBefore,
        ShopConversionStatus $statusAfter,
        User $user,
        string $reason = null
    )
    {
        $change = new ShopConversionStatusChange(
            $shopConversion,
            $statusBefore,
            $statusAfter,
            $user,
            $reason ? trim($reason) : null
        );

        $this->em->persist($change);
    }
}