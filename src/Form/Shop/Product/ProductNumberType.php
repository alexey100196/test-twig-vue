<?php

namespace App\Form\Shop\Product;

use App\Entity\Product;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class ProductNumberType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', TextType::class, [
                'attr' => ['placeholder' => 'Identifier'],
                'empty_data' => '',
                'constraints' => [
                    new Length(['min' => 1, 'max' => 32]),
                    new NotBlank(),
                    new NotNull(),
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
            'constraints' => [
                new UniqueEntity([
                    'fields' => ['shop', 'number'],
                    'repositoryMethod' => 'findBy',
                    'message' => 'Given number is taken.',
                ]),
            ],
        ]);
    }
}