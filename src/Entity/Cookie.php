<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\InheritanceType;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints\Collection;

/**
 * @ORM\InheritanceType("JOINED")
 * @ORM\Entity(repositoryClass="App\Repository\CookieRepository")
 */
class Cookie
{
    public const DATE_VALIDITY_FORMAT = 'Y-m-d\TH:i:s+';
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $key;

    /**
     * The internal primary identity key
     * @var UuidInterfac
     * @ORM\Column(type="uuid", unique=true)
     */
    private $value;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     */
    private $consumed;

    /**
     * @ORM\Column(type="boolean")
     */
    private $dropped;

    /**
     * @ORM\Column(type="string")
     */
    private $cookieType;

    /**
     * @ORM\Column(type="string")
     */
    private $validity;

//    /**
//     * @ORM\Column(type="string")
//     */
    private $validityType;

    public function __construct()
    {
       // $this->amount = 0;
       // $this->transactions = new ArrayCollection();
        try {
            $this->value = Uuid::uuid4();
        } catch (\Exception $e) {
            printf($e->getMessage());
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getConsumed(): ?int
    {
        return $this->consumed;
    }
    public function setConsumed($consumed): self
    {
        $this->consumed = $consumed;
        return $this;
    }
    public function getCookieType(): ?string
    {
        return $this->cookieType;
    }
    public function setCookieType($cookieType): self
    {
        if(in_array($cookieType, CookieType::Types())){
            $this->cookieType = $cookieType;
        }
        return $this;
    }
    public function getValidity(): ?string
    {
        return $this->validity;
    }
    public function setValidity($cookieValidity): self
    {
        if($this->validityType === CookieValidityType::DATE){
            //This will only be correct when $this->validityType has been set
            try {
                $date = new DateTime(self::DATE_VALIDITY_FORMAT, $cookieValidity);
            } catch (\Exception $e) {
                throwException(new Exception("Cookie Validity cannot be formatted as a DateTime using the format: ".self::DATE_VALIDITY_FORMAT));
            }
        }
        $this->validity = $cookieValidity;
        return $this;
    }
    public function getValidityType(): ?string
    {
        return $this->validityType;
    }
    public function setValidityType($cookieValidityType): self
    {
        if(in_array($cookieValidityType, CookieValidityType::Types())){
            $this->validityType = $cookieValidityType;
        }
        return $this;
    }

    public function __toString()
    {
        return $this->getAmount() . ' ' . $this->getCurrency()->getName();
    }

    public function setValue(Uuid $value): self
    {
        $this->value = $value;
       // return $this;
    }

    public function getValue(): Uuid
    {
        return $this->value;
    }

}
