<?php


namespace App\Command;


use App\Repository\ReferrerCouponRepository;
use App\Service\Referrer\RegenerateReferrerLinkCardContent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateCardsContentForExpiredReferrerCoupons extends ContainerAwareCommand
{
    const MINUTES_ARGUMENT = 'minutes';

    protected static $defaultName = 'app:generate-cards-content-for-expired-referrer-coupons';

    /**
     * @var ReferrerCouponRepository
     */
    private $referrerCouponRepository;

    /**
     * @var RegenerateReferrerLinkCardContent
     */
    private $regenerateReferrerLinkCardContent;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        string $name = null,
        ReferrerCouponRepository $referrerCouponRepository,
        RegenerateReferrerLinkCardContent $regenerateReferrerLinkCardContent,
        EntityManagerInterface $entityManager
    )
    {
        $this->referrerCouponRepository = $referrerCouponRepository;
        $this->regenerateReferrerLinkCardContent = $regenerateReferrerLinkCardContent;
        $this->entityManager = $entityManager;
        parent::__construct($name);
    }

    protected function configure()
    {
        $this
            ->addArgument(
                self::MINUTES_ARGUMENT,
                InputArgument::REQUIRED,
                'How old referrer coupons are regenerated'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $minutes = (int)$input->getArgument(self::MINUTES_ARGUMENT);

        if ($minutes <= 0) {
            $output->writeln(sprintf('Argument "%s" should be greater than 0.', self::MINUTES_ARGUMENT));
            return;
        }

        // Sub 2 * minutes to cover all fails.
        $start = (new \DateTime())->modify('-' . $minutes * 2 . ' minutes');

        $referrerCoupons = $this->referrerCouponRepository->findExpiredBetweenDates($start, new \DateTime());

        foreach ($referrerCoupons as $referrerCoupon) {
            foreach ($referrerCoupon->getReferrer()->getReferrerLinks() as $referrerLink) {
                if ($card = $referrerLink->getCard()) {
                    ($this->regenerateReferrerLinkCardContent)($card);
                }
            }

            // FLush per user.
            $this->entityManager->flush();
        }
    }
}