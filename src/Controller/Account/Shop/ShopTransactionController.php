<?php


namespace App\Controller\Account\Shop;

use App\Helper\DatePeriodGenerator;
use App\Repository\ShopConversionRepository;
use App\Traits\Controller\HasOrdering;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ShopIncomeController
 *
 * @Security("has_role('ROLE_SHOP')")
 */
class ShopTransactionController extends AbstractController
{
    use HasOrdering;

    private $shopConversionRepository;

    public function __construct(ShopConversionRepository $shopConversionRepository)
    {
        $this->shopConversionRepository = $shopConversionRepository;
    }

    /**
     * @Route("/account/company/transactions/{year}/{month}", name="account_shop_transactions")
     * @param int|null $year
     * @param int|null $month
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function index(int $year = null, int $month = null, Request $request)
    {
        $datePeriod = DatePeriodGenerator::generateFromYearAndMonth($year, $month);

        $orderBy = $this->getOrderByFromRequest($request, ['createdAt', 'purchaseValue', 'referrerProfit', 'revenue']);

        $conversions = $this->shopConversionRepository
            ->getAllByShopQueryBuilder(
                $this->getUser()->getShop(),
                $datePeriod->getStart(),
                $datePeriod->getEnd(),
                $orderBy
            )
            ->getQuery()
            ->getResult();

        return $this->render('account/shop/transaction/index.html.twig', [
            'conversions' => $conversions,
            'currentPeriod' => $datePeriod
        ]);
    }
}