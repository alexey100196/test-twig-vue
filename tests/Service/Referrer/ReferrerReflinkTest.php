<?php


namespace App\Tests\Service\Referrer;


use App\Entity\EventEntry;
use App\Entity\EventEntryType;
use App\Entity\Product;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\User;
use App\Repository\EventEntryTypeRepository;
use App\Repository\ReferrerLinkRepository;
use App\Service\Referrer\ReferrerReflink;
use App\Service\Referrer\ReferrerShop;
use App\Service\Referrer\ReferrerShopLink;
use App\Service\ShopEvent\EventService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class ReferrerReflinkTest extends TestCase
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    private $service;
    private $referrerLinkRepo;
    private $eventEntryTypeRepo;
    private $eventService;

    protected function setUp(): void
    {
        $this->em = $this->createMock(EntityManagerInterface::class);

        $this->referrerLinkRepo = $this->createMock(ReferrerLinkRepository::class);
        $this->eventEntryTypeRepo = $this->createMock(EventEntryTypeRepository::class);
        $this->eventService = $this->createMock(EventService::class);

        $this->service = new ReferrerReflink($this->referrerLinkRepo, $this->eventEntryTypeRepo, $this->eventService);
    }



    public function getReferrerLinkParams()
    {
        yield ['https://google.com', 'any-slug', 'https://google.com?siReferrer=any-slug'];
        yield ['https://google.com/', 'any-slug', 'https://google.com/?siReferrer=any-slug'];
        yield ['http://google.com/?page=1', 'any-slug', 'http://google.com/?page=1&siReferrer=any-slug'];
    }

    /**
     * @dataProvider getReferrerLinkParams
     */
    public function testGetRedirectUrlForReffererLinkGeneratingWithoutProduct($destinationUrl, $slug, $expectedUrl)
    {
        $referrerLink = new ReferrerLink();
        $referrerLink->setDestinationUrl($destinationUrl);
        $referrerLink->setSlug($slug);

        $this->assertEquals($expectedUrl, $this->service->getRedirectUrlForReffererLink($referrerLink));
    }

    public function testGetRedirectUrlForReffererLinkGeneratingWithProduct()
    {
        $referrerLink = new ReferrerLink();
        $referrerLink->setDestinationUrl('https://google.com');
        $referrerLink->setSlug('any-slug');

        $product = new Product();
        $product->setNumber('1234');

        $referrerLink->setProduct($product);

        $this->assertEquals(
            'https://google.com?siReferrer=any-slug&siProduct=1234',
            $this->service->getRedirectUrlForReffererLink($referrerLink)
        );
    }

    public function testRegisterViewForReferrerLink()
    {
        $eventEntryType = new EventEntryType();
        $eventEntryType->setName(EventEntryType::TYPE_VIEW);

        $this->eventEntryTypeRepo
            ->expects($this->once())
            ->method('findOneBy')
            ->with(['name' => EventEntryType::TYPE_VIEW])
            ->willReturn($eventEntryType);

        $user = new User();
        $referrerLink = new ReferrerLink();
        $referrerLink->setShop($shop = new Shop());
        $referrerLink->setReferrer($user);

        $this->eventService
            ->expects($this->once())
            ->method('process')
            ->with($this->callback(function ($eventEntry) use ($eventEntryType, $shop, $referrerLink) {
                return $eventEntry->getType() === $eventEntryType &&
                    $eventEntry->getShop() === $shop &&
                    $eventEntry->getReferrerLink() === $referrerLink;
            }));

        $this->service->registerViewForReferrerLink($referrerLink);
    }

}