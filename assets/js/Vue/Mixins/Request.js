export default {
    methods: {
        makeRequest(method, url, data = {}, headers) {
            method = method.toLowerCase();
            let obj = {method: method, url: url, headers: headers};

            if (obj.method === 'get') {
                obj.params = data;
            } else {
                obj.data = data;
            }

            return axios(obj);
        }
    }
}