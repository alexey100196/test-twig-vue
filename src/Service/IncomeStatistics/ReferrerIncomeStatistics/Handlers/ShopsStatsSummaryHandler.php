<?php


namespace App\Service\IncomeStatistics\ReferrerIncomeStatistics\Handlers;


use App\Entity\ShopInteraction;
use App\Service\IncomeStatistics\CommandInterface;
use App\Service\IncomeStatistics\ReferrerIncomeStatistics\Entities\ShopsStatsSummary;
use Doctrine\Common\Collections\ArrayCollection;

class ShopsStatsSummaryHandler extends AbstractHandler
{
    /**
     * @param CommandInterface $command
     * @return ArrayCollection|ShopsStatsSummary[]
     */
    public function handle(CommandInterface $command)
    {
        $filteredBy = method_exists($command, 'getFilterBy') ? $command->getFilterBy() : '';

        $rows = $this->doctrine
            ->getRepository(ShopInteraction::class)
            ->getReferrerShopsStats(
                $command->getShops(),
                $command->getReferrer(),
                $command->getStartDate(),
                $command->getEndDate(),
                $filteredBy
            );

        return new ArrayCollection(array_map([$this, 'createShopStatsSummaryFromArray'], $rows));
    }

    /**
     * @param array $item
     * @return ShopsStatsSummary
     */
    private function createShopStatsSummaryFromArray(array $item): ShopsStatsSummary
    {
        $shopStatsSummary = new ShopsStatsSummary();
        $shopStatsSummary->setTotalPurchaseValueGenerated((float)$item['purchaseTotal']);
        $shopStatsSummary->setTotalSignUpsValueGenerated((float)$item['signUpCommission']);
        $shopStatsSummary->setTotalNumberOfViews((int)$item['viewsCount']);
        $shopStatsSummary->setPurchaseReferrerProfit((float)$item['purchaseReferrerProfit']);
        $shopStatsSummary->setSignUpReferrerProfit((float)$item['signUpReferrerProfit']);
        $shopStatsSummary->setShop($item['shop']);
        $shopStatsSummary->setPurchaseCount((int)$item['purchaseCount']);
        $shopStatsSummary->setInviteFriendEmails((int)$item['inviteFriendEmails']);
        $shopStatsSummary->setUsedCouponsCount((int)$item['usedCouponsCount']);

        return $shopStatsSummary;
    }
}