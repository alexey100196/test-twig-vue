<?php


namespace App\Controller\Account\Referrer;


use App\Entity\ReferrerLink;
use App\Entity\ReferrerStoreNonReferableLink;
use App\Entity\Shop;
use App\Form\Referrer\ReferrerLinkType;
use App\Form\Referrer\ReferrerStoreNonReferableLinkType;
use App\Repository\ReferrerLinkRepository;
use App\Repository\ReferrerStoreNonReferableLinkRepository;
use App\Repository\UserShopRepository;
use App\Service\Referrer\ReferrerShop;
use App\Service\Referrer\ReferrerShopLink;
use App\Traits\Controller\FormResponseArray;
use App\Util\Consts;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerShopStoreController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerShopStoreController extends AbstractController
{
    /**
     * @var ReferrerShopLink
     */
    private $referrerShopLink;

    /**
     * @var ReferrerShop
     */
    private $referrerShop;

    /**
     * ReferrerShopController constructor.
     * @param ReferrerShopLink $referrerShopLink
     * @param ReferrerShop $referrerShop
     */
    public function __construct(
        ReferrerShopLink $referrerShopLink,
        ReferrerShop $referrerShop
    )
    {
        $this->referrerShopLink = $referrerShopLink;
        $this->referrerShop = $referrerShop;
    }

    /**
     * @Route("/account/referrer/company/{shop}/store", name="account_referrer_shop_store_show")
     * @param Shop $shop
     * @param ReferrerLinkRepository $referrerLinkRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(
        Shop $shop,
        ReferrerLinkRepository $referrerLinkRepository,
        Request $request
    )
    {
        $userShop = $this->referrerShop->findReferrerShopWithStats($this->getUser(), $shop);

        if (!$userShop) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        $forms = $this->createForms($request);

        foreach ($forms as $form) {
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($form->getData());
                $em->flush();

                $this->addFlash(Consts::SUCCESS, 'Non Refferable Link Saved');
            }
        }

        $myStoreReferrerLinks = $referrerLinkRepository->findBy(['referrer' => $this->getUser(), 'showInMyStore' => true], ['createdAt' => 'DESC']);

        return $this->render('account/referrer/shop/show-store.html.twig', [
            'item' => $userShop,
            'forms' => $forms->map(function ($form) {
                return $form->createView();
            }),
            'myStoreReferrerLinks' => $myStoreReferrerLinks,
        ]);
    }

    /**
     * @param Request $request
     * @return ArrayCollection
     */
    private function createForms(Request $request): ArrayCollection
    {
        $forms = new ArrayCollection();

        for ($i = 0; $i < $this->getParameter('my_store_links_count'); $i++) {
            // Prepare data.
            $item = $this->getUser()->getStoreNonReferableLinkByOrderColumn($i) ?? new ReferrerStoreNonReferableLink();
            $item->setOrderColumn($i);
            $item->setReferrer($this->getUser());
            // Prepare form.
            $form = $this->createNamedForm('store_' . $i, ReferrerStoreNonReferableLinkType::class, $item);
            $form->handleRequest($request);
            $forms->add($form);
        }

        return $forms;
    }

    /**
     * @param $name
     * @param $type
     * @param null $data
     * @param array $options
     * @return mixed
     */
    private function createNamedForm($name, $type, $data = null, array $options = array())
    {
        return $this->container->get('form.factory')->createNamed($name, $type, $data, $options);
    }
}