<?php


namespace App\Form\Referrer\Parts\CardSet;


use App\Entity\CardSet\CardSetCardBackground;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class CardSetCardBackgroundType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', FileType::class, [
            'constraints' => [
                new NotNull(['message' => 'Please specify file.'])
            ]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CardSetCardBackground::class,
            // @todo: fix it
//            'csrf_protection' => false,
        ]);
    }
}