<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Handlers;


use App\Entity\ShopInteraction;
use App\Service\IncomeStatistics\CommandInterface;
use App\Service\IncomeStatistics\Handler;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Entities\ShopStatistics;
use Doctrine\Common\Collections\ArrayCollection;

class ReferrersStatisticsHandler extends Handler
{
    /**
     * @param CommandInterface $command
     * @return ArrayCollection
     */
    public function handle(CommandInterface $command)
    {
        $result = $this->doctrine
            ->getRepository(ShopInteraction::class)
            ->getShopReferrerStats(
                $command->getShop(),
                $command->getStartDate(),
                $command->getEndDate(),
                $command->getProductsIds(),
                $command->getIncludeMainOffer(),
                $command->getFilterBy(),
                $command->getCampaigns()
            );

        return new ArrayCollection(array_map([$this, 'convertToEntity'], $result));
    }

    /**
     * @param array $item
     * @return ShopStatistics
     */
    private function convertToEntity(array $item): ShopStatistics
    {
        return (new ShopStatistics())
            ->setReferrer($item['referrer'])
            ->setLastPurchaseDate($item['lastPurchaseDate'] ? new \DateTime($item['lastPurchaseDate']) : null)
            ->setLastSignUpDate($item['lastSignUpDate'] ? new \DateTime($item['lastSignUpDate']) : null)
            ->setPurchaseCount((int)$item['purchaseCount'])
            ->setSignUpCount((int)$item['signUpCount'])
            ->setViewsCount((int)$item['viewsCount'])
            ->setInviteFriendEmails((int)$item['inviteFriendEmails'])
            ->setUsedCouponsCount((int)$item['usedCouponsCount'])
            ->setSignUpReferrerProfit((float)$item['signUpReferrerProfit'])
            ->setPurchaseReferrerProfit((float)$item['purchaseReferrerProfit'])
            ->setSystemCommission((float)$item['systemCommission'])
            ->setPurchaseTotal((float)$item['purchaseTotal']);
    }
}