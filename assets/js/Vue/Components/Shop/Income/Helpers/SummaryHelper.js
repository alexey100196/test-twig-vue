'use strict';

const moneyFactory = require('../../../../Objects/Money').moneyFactory;

function SummaryHelper(items) {
    this.items = items;
    this.purchase_referrer_profit = moneyFactory('EUR', this.sumScalar('purchase_referrer_profit'));
    this.purchase_count = this.sumScalar('purchase_count');
    this.sign_up_count = this.sumScalar('sign_up_count');
    this.views_count = this.sumScalar('views_count');
    this.used_coupons_count = this.sumScalar('used_coupons_count');
    this.invite_friend_emails_count = this.sumScalar('invite_friend_emails_count');
    this.total_referrer_income = this.sumMoney('total_referrer_income');
    this.sign_up_referrer_profit = this.sumMoney('sign_up_referrer_profit');
    this.system_commission = this.sumMoney('system_commission');
    this.purchase_total = this.sumMoney('purchase_total');
    this.revenue = _.clone(this.purchase_total)
        .sub(this.system_commission)
        .sub(this.total_referrer_income);
}

SummaryHelper.prototype.sumScalar = function (fieldName) {
    return _.reduce(this.items, function (memo, item) {
        return memo + item[fieldName];
    }, 0)
};

SummaryHelper.prototype.sumMoney = function (fieldName) {
    return _.reduce(this.items, function (memo, item) {
        return memo.add(item[fieldName]);
    }, moneyFactory('EUR', 0))
};

SummaryHelper.prototype.getAll = function () {
    return {
        purchase_count: this.purchase_count,
        sign_up_count: this.sign_up_count,
        views_count: this.views_count,
        used_coupons_count: this.used_coupons_count,
        invite_friend_emails_count: this.invite_friend_emails_count,
        total_referrer_income: this.total_referrer_income,
        sign_up_referrer_profit: this.sign_up_referrer_profit,
        system_commission: this.system_commission,
        purchase_total: this.purchase_total,
        revenue: this.revenue,
        purchase_referrer_profit: this.purchase_referrer_profit
    }
};

module.exports = SummaryHelper;