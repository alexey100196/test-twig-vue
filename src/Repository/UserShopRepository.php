<?php

namespace App\Repository;

use App\Entity\ProductPurchase;
use App\Entity\Shop;
use App\Entity\ShopConversion;
use App\Entity\ShopInteraction;
use App\Entity\ShopPurchase;
use App\Entity\ShopSignUp;
use App\Entity\ShopView;
use App\Entity\User;
use App\Entity\UserShop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserShop|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserShop|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserShop[]    findAll()
 * @method UserShop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserShopRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserShop::class);
    }

    /**
     * @param User $user
     * @return array
     */
    public function findUserProgramsWithStats(User $user)
    {
        return $this->createQueryBuilder('us')
            ->select('
                us as userShop,
                SUM(sp.referrerProfit) as shopPurchaseProfit,
                SUM(ssu.referrerProfit) as shopSignUpProfit,
                COUNT(sp.id) as shopPurchaseCount,
                COUNT(ssu.id) as shopSignUpCount,
                SUM(sp.amount * sp.price) as shopPurchaseTotal,
                COUNT(sv.id) as viewsCount
             ')
            ->leftJoin(ShopInteraction::class, 'si', Join::WITH, 'si.shop = us.shop')
            ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sp = si AND sp.referrer = :user')
            ->leftJoin(ShopSignUp::class, 'ssu', Join::WITH, 'ssu = si AND ssu.referrer = :user')
            ->leftJoin(ShopView::class, 'sv', Join::WITH, 'sv = si AND sv.referrer = :user')
            ->andWhere('us.user = :user')
            ->setParameter('user', $user)
            ->groupBy('us.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param User $user
     * @return array
     */
    public function findUserPrograms(User $user)
    {
        return $this->createQueryBuilder('us')
            ->select('
                s
             ')
            ->innerJoin(Shop::class, 's', Join::WITH, 's = us.shop')
            ->andWhere('us.user = :user')
            ->setParameter('user', $user)
            ->groupBy('s.id')
            ->getQuery()
            ->getResult();
    }

    public function getUserProgramsStats(User $user)
    {
        return $this->createQueryBuilder('us')
                ->select('
                SUM(sp.referrerProfit) as shopPurchaseProfit,
                SUM(ssu.referrerProfit) as shopSignUpProfit,
                COUNT(sp.id) as shopPurchaseCount,
                COUNT(ssu.id) as shopSignUpCount,
                SUM(sp.amount * sp.price) as shopPurchaseTotal,
                COUNT(sv.id) as viewsCount
             ')
                ->leftJoin(ShopInteraction::class, 'si', Join::WITH, 'si.shop = us.shop')
                ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sp = si AND sp.referrer = :user')
                ->leftJoin(ShopSignUp::class, 'ssu', Join::WITH, 'ssu = si AND ssu.referrer = :user')
                ->leftJoin(ShopView::class, 'sv', Join::WITH, 'sv = si AND sv.referrer = :user')
                ->andWhere('us.user = :user')
                ->setParameter('user', $user)
                ->getQuery()
                ->getArrayResult()[0] ?? [];
    }

    /**
     * @param Shop $shop
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByShop(Shop $shop): int
    {
        return (int)$this->createQueryBuilder('us')
            ->where('us.shop = :shop')
            ->setParameter('shop', $shop)
            ->select('COUNT(us.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param Shop $shop
     * @return array
     */
    public function findPartners(Shop $shop)
    {
        return $this->createQueryBuilder('us')
              //  ->leftJoin(User::class, 'u', Join::WITH, 'u = us.user')
              ->where('us.shop = :shop')
            ->setParameter('shop', $shop)
            ->select('us')
            ->getQuery()
            ->getResult() ?? [];
    }
}
