<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReferreMyStore")
 */
class ReferrerMyStore
{
    use TimestampableEntity;

    const DEFAULT_TEMPLATE = 'whitesmoke';
    const MAX_MYSTORES = 10;
    const MAX_CUSTOM_LINKS = 20;
    const MAX_AFFILIATE_LINKS = 20;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"referrer-my-store"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"referrer-my-store"})
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"referrer-my-store"})
     */
    private $subtitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"referrer-my-store"})
     */
    private $fb_messenger;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"referrer-my-store"})
     * @Assert\Email()
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"referrer-my-store"})
     */
    private $skype;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"referrer-my-store"})
     */
    private $whatsapp;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"referrer-my-store"})
     */
    private $whatsappTitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"referrer-my-store"})
     * @Assert\Url()
     */
    private $facebook;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"referrer-my-store"})
     * @Assert\Url()
     */
    private $instagram;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"referrer-my-store"})
     * @Assert\Url()
     */
    private $linkedin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"referrer-my-store"})
     * @Assert\Url()
     */
    private $twitter;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"referrer-my-store"})
     * @Assert\Url()
     */
    private $youtube;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Groups({"referrer-my-store"})
     */
    private $template;

    /**
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     * @Groups({"referrer-my-store"})
     * @Assert\Url()
     */
    private $link;

    /**
     * @ORM\Column(type="boolean")
     */
    private $facebookPixel = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $twitterPixel = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pinterestPixel = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $linkedinPixel = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $googlePixel = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $quoraPixel = 0;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"referrer-my-store"})
     */
    private $avatar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ReferrerStoreNonReferableLink", mappedBy="mystore")
     */
    private $nonReferableLinks;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="my_store")
     */
    private $referrer;

    /**
     * ReferrerMyStore constructor.
     */
    public function __construct()
    {
        $this->template = self::DEFAULT_TEMPLATE;
        $this->nonReferableLinks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(?string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFbMessenger()
    {
        return $this->fb_messenger;
    }

    /**
     * @param mixed $fb_messenger
     */
    public function setFbMessenger($fb_messenger): void
    {
        $this->fb_messenger = $fb_messenger;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     */
    public function setMail($mail): void
    {
        $this->mail = $mail;
    }

    /**
     * @return mixed
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * @param mixed $skype
     */
    public function setSkype($skype): void
    {
        $this->skype = $skype;
    }

    /**
     * @return mixed
     */
    public function getWhatsapp()
    {
        return $this->whatsapp;
    }

    /**
     * @param mixed $whatsapp
     */
    public function setWhatsapp($whatsapp): void
    {
        $this->whatsapp = $whatsapp;
    }

    /**
     * @return mixed
     */
    public function getWhatsappTitle()
    {
        return $this->whatsappTitle;
    }

    /**
     * @param mixed $whatsappTitle
     */
    public function setWhatsappTitle($whatsappTitle): void
    {
        $this->whatsappTitle = $whatsappTitle;
    }

    /**
     * @return mixed
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param mixed $facebook
     */
    public function setFacebook($facebook): void
    {
        $this->facebook = $facebook;
    }

    /**
     * @return mixed
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * @param mixed $instagram
     */
    public function setInstagram($instagram): void
    {
        $this->instagram = $instagram;
    }

    /**
     * @return mixed
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * @param mixed $linkedin
     */
    public function setLinkedin($linkedin): void
    {
        $this->linkedin = $linkedin;
    }

    /**
     * @return mixed
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * @param mixed $twitter
     */
    public function setTwitter($twitter): void
    {
        $this->twitter = $twitter;
    }

    /**
     * @return mixed
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * @param mixed $youtube
     */
    public function setYoutube($youtube): void
    {
        $this->youtube = $youtube;
    }

    /**
     * @return string|null
     */
    public function getTemplate(): ?string
    {
        return $this->template;
    }

    /**
     * @param string|null $template
     */
    public function setTemplate(?string $template): void
    {
        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link): void
    {
        $this->link = $link;
    }

    /**
     * @return int
     */
    public function getFacebookPixel(): int
    {
        return $this->facebookPixel;
    }

    /**
     * @param int $facebookPixel
     */
    public function setFacebookPixel(int $facebookPixel): void
    {
        $this->facebookPixel = $facebookPixel;
    }

    /**
     * @return int
     */
    public function getTwitterPixel(): int
    {
        return $this->twitterPixel;
    }

    /**
     * @param int $twitterPixel
     */
    public function setTwitterPixel(int $twitterPixel): void
    {
        $this->twitterPixel = $twitterPixel;
    }

    /**
     * @return int
     */
    public function getPinterestPixel(): int
    {
        return $this->pinterestPixel;
    }

    /**
     * @param int $pinterestPixel
     */
    public function setPinterestPixel(int $pinterestPixel): void
    {
        $this->pinterestPixel = $pinterestPixel;
    }

    /**
     * @return int
     */
    public function getLinkedinPixel(): int
    {
        return $this->linkedinPixel;
    }

    /**
     * @param int $linkedinPixel
     */
    public function setLinkedinPixel(int $linkedinPixel): void
    {
        $this->linkedinPixel = $linkedinPixel;
    }

    /**
     * @return int
     */
    public function getGooglePixel(): int
    {
        return $this->googlePixel;
    }

    /**
     * @param int $googlePixel
     */
    public function setGooglePixel(int $googlePixel): void
    {
        $this->googlePixel = $googlePixel;
    }

    /**
     * @return int
     */
    public function getQuoraPixel(): int
    {
        return $this->quoraPixel;
    }

    /**
     * @param int $quoraPixel
     */
    public function setQuoraPixel(int $quoraPixel): void
    {
        $this->quoraPixel = $quoraPixel;
    }

    public function getAvatar(): ?String
    {
        return $this->avatar;
    }

    public function setAvatar(?String $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return Collection|ReferrerStoreNonReferableLink[]
     */
    public function getNonReferableLinks()
    {
        return $this->nonReferableLinks;
    }

    /**
     * @param mixed $nonReferableLinks
     */
    public function setNonReferableLinks($nonReferableLinks): void
    {
        $this->nonReferableLinks = $nonReferableLinks;
    }

    /**
     * @return User|null
     */
    public function getReferrer()
    {
        if ($this->referrer->count() > 0)
            return $this->referrer->first();

        return null;
    }

    /**
     * @param mixed $referrer
     */
    public function setReferrer($referrer): void
    {
        $this->referrer = $referrer;
    }
}
