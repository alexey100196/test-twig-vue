<?php


namespace App\Form\ShopEvent;

use App\Entity\Coupon\CouponPackage;
use App\Entity\EventEntry;
use App\Entity\EventEntryType;
use App\Entity\Product;
use App\Entity\Shop;
use App\Entity\User;
use App\Form\ShopEvent\Parts\EventEntryHttpReferrerType;
use App\Form\ShopEvent\Parts\EventEntryTestType;
use App\Repository\Coupon\UserCouponRepository;
use App\Repository\ShopRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class PurchaseType extends AbstractEventEntryType
{
    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * @var UserCouponRepository
     */
    private $userCouponRepository;

    /**
     * @var array
     */
    private $rawData = [];

    /**
     * PurchaseType constructor.
     * @param ShopRepository $shopRepository
     * @param UserCouponRepository $userCouponRepository
     */
    public function __construct(ShopRepository $shopRepository, UserCouponRepository $userCouponRepository)
    {
        $this->shopRepository = $shopRepository;
        $this->userCouponRepository = $userCouponRepository;
    }

    /**
     * @param array $data
     */
    protected function rememberRawData(array $data)
    {
        $this->rawData = $data;
    }

    /**
     * @return array
     */
    protected function getRawData()
    {
        return $this->rawData;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('http_referer', EventEntryHttpReferrerType::class)
            ->add('payload', PurchasePayloadType::class, [
                'by_reference' => true,
            ])
            ->add('test', EventEntryTestType::class)
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                $data['type'] = EventEntryType::TYPE_PURCHASE;
                if (empty($data['referrer_link']) && !empty($data['payload']['coupon'])) {
                    $data['type'] = EventEntryType::TYPE_PURCHASE_COUPON;
                    $event->getForm()->remove('referrer_link');
                    unset($data['referrer_link']);
                }
                if ($data['test'] ?? false) {
                    $event->getForm()->remove('referrer_link');
                }

                $this->rememberRawData($data);
                $event->setData($data);
            })
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                $data->setPayload($this->getRawData()['payload'] ?? []);
                $event->setData($data);
            });
    }
}