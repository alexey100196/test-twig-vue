# ValueLink - Source Code - Core

ValueLink - Source Code - Core 
valuelink.io 
==========

- PHP 7.2
- Symfony 4.4
- MySQL

## CI

## Prerequisite

- bash
- docker >= 18.09
- docker-compose >= 1.23
- node.js v8.10.0


## Setup

- copy and edit .env file

    `cp .env.dist .env`

- copy and edit docker-compose.override.yml file

```sh
cp docker-compose.override.yml.dist docker-compose.override.yml
```

- add new host entry on your machine

    `127.0.0.1 marfil-app.local`
    
- ensure that locally there is no http server listening on the port 80. If there is, either remove it or modify the following files and environment variables:
    1. `docker-compose.override.yml` -> `ports` -> change `80:80` to map another host port (eg. `8042` -> `8042:80`)
    2. `.env` and `.env.dev` -> modify `APP_URL` to have the host port (eg. `http://marfil-app.local:8042`)
    
    
- run setup scripts

```sh
./bin/build-scss
./bin/build-vue
./bin/composer install
./bin/build-dev
```
# Setup Widget

In order to run Widget Product:
Read readme.md file in widgets folder

# Run

- start the containers - will be started in the background

```sh
docker-compose up -d
```

- perform the migrations 

```sh
./bin/marfil-php-console doctrine:migrations:migrate
```

- open the app
    1. navigate to `http://marfil-app.local` or, if in docker-compose.override.yml another port, other than  `80` is mapped to the container port `80`, use that, 
    2. for example, if the port entry is `8042:80`, than open `http://marfil-app.local:8042`

# DataBase Setup

In order to setup database with initial settings:

0.  Run: docker-compose exec php bash
1.  Drop all tables: ./bin/console doctrine:schema:drop --full-database --force
2.  Execute migrations from: valuelink-source-code-core/src/Migrations/
3.  Run migration (create db): ./bin/console doctrine:migrations:diff
4.  Run migration (insert db): ./bin/console doctrine:migrations:execute --up 20210117174555

# Generate updated migrations

In order to get the latest database changes from the Entities, execute the following command:
```sh
./bin/marfil-php-console doctrine:migrations:diff
```

# Custom DB Changes

In order to add custom "Migrations" you can generate an empty migration file with the following command:

```sh
./bin/marfil-php-console doctrine:migrations:generate
```

Follow the output for the newly created migration file.

# Test

- execute the following to run the tests

```sh
./bin/test tests/
```

Local DB Viewer
==============

- phpmyadmin 

    http://marfil-app.local:8043

Migrations Shortcuts
=================

Generate new migrations from diff

```sh
./bin/marfil-php-console doctrine:migrations:diff
```

Apply new migrations

```sh
./bin/marfil-php-console doctrine:migrations:migrate
```

Miscellaneous
============

Update `inotify` `max_user_watches` limit:

```shell script
echo 16384 | sudo tee /proc/sys/fs/inotify/max_user_watches
```

# Local developmet in IDE

In order to develop locally, using an IDE like PHPStorm, you should install the following php extensions:

- xdebug
- dom
- gd
- bcmath

In Ubuntu, to install `php-cli` and `php-xdebug`:

```shell script
sudo apt-get update
sudo apt-get install php-cli php-xdebug php-dom php-gd php-bcmath
```

# .ENV Nodes

For development purpose ensure .env exits && APP_ENV=dev


# .Initial Settings to Run valuelink.io

1. Create Admin User: Create Company profile (use name as Admin)
2. In table User set field role_id to ROLE_ADMIN 3
3. Create Your test company profile.
4. In Company Profile enter http://marfil-app.local/snippet/snippet.php as url.
5. Place snippet to Your page under resources/snippet/snippet.php
6. Set rewards, cash/coupon.
7. Test Connection.
