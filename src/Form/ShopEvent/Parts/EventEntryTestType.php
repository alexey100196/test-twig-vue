<?php

namespace App\Form\ShopEvent\Parts;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventEntryTestType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'required' => false,
            'empty_data' => false,
            'false_values' => ['0', 'false', 0, false],
        ]);
    }

    public function getParent()
    {
        return CheckboxType::class;
    }
}