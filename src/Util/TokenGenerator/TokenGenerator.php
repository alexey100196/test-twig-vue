<?php


namespace App\Util\TokenGenerator;


class TokenGenerator implements TokenGeneratorInterface
{
    /**
     * @param int $length
     * @return string
     * @throws \Exception
     */
    public function generate(int $length = 32): string
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}