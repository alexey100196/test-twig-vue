<?php

namespace App\Form\Referrer\Parts\ReferrerLinkCard;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;

class SloganSizeType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'constraints' => [
                new GreaterThanOrEqual(7),
                new LessThanOrEqual(70),
            ]
        ]);
    }

    public function getParent()
    {
        return IntegerType::class;
    }
}