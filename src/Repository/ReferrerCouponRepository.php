<?php

namespace App\Repository;

use App\Entity\ReferrerCoupon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ReferrerCoupon|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferrerCoupon|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferrerCoupon[]    findAll()
 * @method ReferrerCoupon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferrerCouponRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReferrerCoupon::class);
    }

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @return iterable|ReferrerCoupon[]
     */
    public function findExpiredBetweenDates(\DateTime $start, \DateTime $end): iterable
    {
        return $this->createQueryBuilder('r')
            ->where('r.expiresAt >= :start AND r.expiresAt <= :end')
            ->setParameters(['start' => $start, 'end' => $end])
            ->getQuery()
            ->getResult();
    }
}
