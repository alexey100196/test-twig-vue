<?php

namespace App\Controller;

use App\Traits\Controller\HasJsonResponse;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AjaxAbstractController extends AbstractController
{
    use HasJsonResponse;

    /**
     * Create (and submit) form from request.
     * @param Request $request
     * @param string $form
     * @param mixed $entity
     * @return Form
     */
    protected function createFormFromRequest(Request $request, string $form, $entity): Form
    {
        $form = $this->createForm($form, $entity);
        $form->submit($request->request->all());

        return $form;
    }

    /**
     * Return form errors as array.
     *
     * @param Form $form
     * @return array
     */
    protected function getFormErrorMessages(Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if ($child->isSubmitted() && !$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrorMessages($child);
            }
        }

        return $errors;
    }

    /**
     * Make validation response.
     * @param array $errors
     * @return JsonResponse
     */
    protected function validationErrorResponse(array $errors): JsonResponse
    {
        return new JsonResponse(['errors' => $errors], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
