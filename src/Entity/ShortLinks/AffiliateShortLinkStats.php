<?php

namespace App\Entity\ShortLinks;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShortLinks\AffiliateShortLinkStatsRepository")
 * @ORM\Table(name="affiliate_short_links_stats")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class AffiliateShortLinkStats
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShortLinks\AffiliateShortLink")
     * @ORM\JoinColumn(nullable=false)
     */
    private $affiliateShortLink;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $system;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $browser;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAffiliateShortLink()
    {
        return $this->affiliateShortLink;
    }

    /**
     * @param mixed $affiliateShortLink
     */
    public function setAffiliateShortLink($affiliateShortLink): void
    {
        $this->affiliateShortLink = $affiliateShortLink;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getSystem()
    {
        return $this->system;
    }

    /**
     * @param mixed $system
     */
    public function setSystem($system): void
    {
        $this->system = $system;
    }

    /**
     * @return mixed
     */
    public function getBrowser()
    {
        return $this->browser;
    }

    /**
     * @param mixed $browser
     */
    public function setBrowser($browser): void
    {
        $this->browser = $browser;
    }

}
