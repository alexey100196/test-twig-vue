<?php

namespace App\Service\ShopEvent\EventStrategies;

use App\Entity\Coupon\Coupon;
use App\Entity\Coupon\CouponPackage;
use App\Entity\Coupon\UserCoupon;
use App\Entity\Currency;
use App\Entity\Shop;
use App\Repository\Coupon\UserCouponRepository;
use App\Service\Exchange\CurrencyExchangeInterface;
use App\Service\ShopEvent\Interfaces\EventStrategyInterface;
use App\Service\Widget\InviteFriendByEmailService;

class ProductPurchasedCouponStrategy extends AbstractStrategy implements EventStrategyInterface
{
    /**
     * @var CurrencyExchangeInterface
     */
    private $currencyExchange;

    /**
     * @var InviteFriendByEmailService
     */
    private $inviteFriendByEmailService;

    /**
     * @param CurrencyExchangeInterface $currencyExchange
     */
    public function setCurrencyExchange(CurrencyExchangeInterface $currencyExchange)
    {
        $this->currencyExchange = $currencyExchange;
    }

    /**
     * @param InviteFriendByEmailService $inviteFriendByEmailService
     */
    public function setInviteFriendByEmailService(InviteFriendByEmailService $inviteFriendByEmailService): void
    {
        $this->inviteFriendByEmailService = $inviteFriendByEmailService;
    }

    public function process()
    {
        /** @var UserCouponRepository $userCouponRepository */
        $userCouponRepository = $this->doctrine->getRepository(UserCoupon::class);

        /** @var Shop $shop */
        $shop = $this->eventEntry->getShop();
        $shopSettings = $shop->getWidgetSetting();

        if (!$shopSettings) {
            return;
        }

        /** @var Coupon $friendCoupon */
        $friendCoupon = $userCouponRepository->findOneByShopAndCouponCodeAndAllocation(
            $shop,
            strval($this->eventEntry->getPayload('coupon')),
            CouponPackage::ALLOCATION_INVITEE
        );

        // If coupon does not exists or referrer already got coupon
        if (!$friendCoupon) {
            return;
        }

        $amount = $this->eventEntry->getPayload('amount');
        $price = $this->eventEntry->getPayload('price');
        $currency = $this->eventEntry->getPayload('currency');
        $totalPrice = $amount * $price;

        $totalEurPrice = $currency ? $this->calculatePriceDependingOnCurrency($totalPrice, $currency) : $totalPrice;

        $friendCoupon->increaseConversionValue($totalEurPrice);

        $em = $this->doctrine->getEntityManager();
        $em->persist($friendCoupon);

        $minPurchaseAmountConditionFulfilled = $shopSettings->getReferrerRewardCouponDiscountMinPurchase() <= $totalEurPrice;

        if ($shop->hasCouponRewards() && $minPurchaseAmountConditionFulfilled) {
            $this->inviteFriendByEmailService->sendCouponCodeToReferrer(
                $shop,
                $friendCoupon->getUser(),
                $friendCoupon
            );
        }

        $em->flush();
    }

    private function calculatePriceDependingOnCurrency(float $fromAmount, string $fromCurrency)
    {
        if ($fromCurrency === Currency::EUR) {
            return $fromAmount;
        }

        return $this->currencyExchange->exchange($fromAmount, $fromCurrency, Currency::EUR);
    }
}