<?php


namespace App\Service\Referrer;


use App\Entity\Shop;
use App\Entity\ShopSignUp;
use App\Entity\User;
use App\Entity\UserShop;
use App\Repository\UserShopRepository;
use Doctrine\ORM\EntityManagerInterface;

class ReferrerShop
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var UserShopRepository
     */
    protected $userShopRepository;

    /**
     * ReferrerShop constructor.
     * @param EntityManagerInterface $em
     * @param UserShopRepository $userShopRepository
     */
    public function __construct(EntityManagerInterface $em, UserShopRepository $userShopRepository)
    {
        $this->em = $em;
        $this->userShopRepository = $userShopRepository;
    }

    /**
     * @param User $referrer
     * @return UserShop[]
     */
    public function getReferrerShopsWithStats(User $referrer)
    {
        return $this->userShopRepository->findUserProgramsWithStats($referrer);
    }

    /**
     * @param User $referrer
     * @return UserShop[]
     */
    public function getReferrerShops(User $referrer)
    {
        return $this->userShopRepository->findUserPrograms($referrer);
    }

    /**
     * @param User $referrer
     * @param Shop $shop
     * @return UserShop|null
     */
    public function findReferrerShopWithStats(User $referrer, Shop $shop)
    {
        $results = $this->getReferrerShopsWithStats($referrer);

        foreach ($results as $result) {
            if (isset($result['userShop']) && $result['userShop']->getShop() == $shop) {
                return $result;
            }
        }

        return null;
    }

    public function getReferrerStats(User $referrer)
    {
        return $this->userShopRepository->getUserProgramsStats($referrer);
    }
}