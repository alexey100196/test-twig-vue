<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product extends Offerable
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"shop"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=32)
     * @Groups({"shop"})
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"shop"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"shop"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="products", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductBalance", mappedBy="product", orphanRemoval=true)
     */
    private $balances;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"shop"})
     */
    private $website;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"shop"})
     */
    private $cpsOffer;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductPurchase", mappedBy="product", orphanRemoval=true)
     */
    private $purchases;

    public function __construct()
    {
        parent::__construct();

        $this->offers = new ArrayCollection();
        $this->balances = new ArrayCollection();

        $this->generateNewNumber();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(? string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(? Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * @return ProductBalance
     */
    public function createBalance(): Balance
    {
        $balance = new ProductBalance();
        $balance->setUser($this);

        return $balance;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getCpsOffer(): ?Offer
    {
        return $this->cpsOffer;
    }

    public function setCpsOffer(?Offer $cpsOffer): self
    {
        $this->cpsOffer = $cpsOffer;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getFullUrl()
    {
        if ($this->shop && $this->shop->getWebsite()) {
            return rtrim($this->shop->getWebsite(), '/') . '/' . $this->getWebsite();
        }

        return null;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @throws \Exception
     */
    public function generateNewNumber()
    {
        $number = '';
        while (strlen($number) < 9) {
            $number .= (string)mt_rand(0, 9);
        }

        $this->number = $number;
    }
    public function __toString()
    {
        return (string) $this->name;
    }
}
