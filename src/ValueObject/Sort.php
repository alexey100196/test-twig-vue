<?php


namespace App\ValueObject;


class Sort
{
    /**
     * @var string
     */
    private $field;

    /**
     * @var string
     */
    private $direction;

    /**
     * @param string $field
     * @throws \Exception
     */
    public function __construct(string $field)
    {
        $this->field = preg_replace("/[^a-zA-Z]/", '', $field);
        $this->direction = 0 === mb_strpos($field, '-') ? 'DESC' : 'ASC';
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @return string
     */
    public function getDirection(): string
    {
        return $this->direction;
    }
}