<?php

namespace App\Repository\ShortLinks;

use App\Entity\ShortLinks\AffiliateShortLink;
use App\Entity\ShortLinks\AffiliateShortLinkStats;
use App\Entity\ShortLinks\AffiliateShortLinkTax;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AffiliateShortLinkStats|null find($id, $lockMode = null, $lockVersion = null)
 * @method AffiliateShortLinkStats|null findOneBy(array $criteria, array $orderBy = null)
 * @method AffiliateShortLinkStats[]    findAll()
 * @method AffiliateShortLinkStats[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AffiliateShortLinkStatsRepository extends ServiceEntityRepository
{
    /** @var EntityManagerInterface $em */
    private $em;

    public function __construct(RegistryInterface $registry, EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        parent::__construct($registry, AffiliateShortLinkStats::class);
    }

    /**
     * @param $user
     * @param string|null $propName
     * @param string|null $from
     * @param string|null $to
     * @param array|null $tags
     * @param array|null $folders
     * @param string|null $linkId
     * @return array
     */
    public function findGeneralByFilters($user, ?string $propName, ?string $from = null, ?string $to = null, ?array $tags = null, ?array $folders = null, ?string $linkId = null) {
        $this->em->getFilters()->disable('softdeleteable');
        $query = $this->createQueryBuilder('s')->select(
            $propName ? 's.'.$propName.', COUNT(s.id) as clicks' : 'COUNT(s.id) as clicks'
        )
            ->innerJoin(AffiliateShortLink::class, 'a', Join::WITH, 'a = s.affiliateShortLink')
            ->leftJoin(AffiliateShortLinkTax::class, 'tt', Join::WITH, 'a = tt.link AND tt.type = \'tag\'')
            ->leftJoin(AffiliateShortLinkTax::class, 'tf', Join::WITH, 'a = tf.link AND tf.type = \'folder\'')
            ->where('a.referrer = :user');

        if ($from) $query->andWhere('s.createdAt >= :from');
        if ($to) $query->andWhere('s.createdAt <= :to');
        if ($linkId) $query->andWhere('s.affiliateShortLink = :link');
        if ($tags) {
            $query->andWhere('tt.name IN (\''.str_replace(', ', '\', \'', strip_tags(
                    implode(', ', $tags)
                )).'\')');
        };
        if ($folders) {
            $query->andWhere('tf.name IN (\''.str_replace(', ', '\', \'', strip_tags(
                    implode(', ', $folders)
                )).'\')');
        };

        $query->setParameter('user', $user);

        if ($from) $query->setParameter('from', $from);
        if ($to) $query->setParameter('to', $to);
        if ($linkId) $query->setParameter('link', $linkId);

        if ($propName) $query->groupBy('s.'.$propName);
        $query = $query->getQuery()->getResult();

        $this->em->getFilters()->enable('softdeleteable');
        return (array) $query;
    }

    /**
     * @param $user
     * @param string|null $from
     * @param string|null $to
     * @param array|null $tags
     * @param array|null $folders
     * @param string|null $linkId
     * @return array
     */
    public function findDaysByFilters($user, ?string $from = null, ?string $to = null, ?array $tags = null, ?array $folders = null, ?string $linkId = null) {
        $this->em->getFilters()->disable('softdeleteable');
        $query = $this->createQueryBuilder('s')->select(
            'DATE_FORMAT(s.createdAt, \'%Y-%m-%d\') as day, COUNT(s.id) as clicks'
        )
            ->innerJoin(AffiliateShortLink::class, 'a', Join::WITH, 'a = s.affiliateShortLink')
            ->leftJoin(AffiliateShortLinkTax::class, 'tt', Join::WITH, 'a = tt.link AND tt.type = \'tag\'')
            ->leftJoin(AffiliateShortLinkTax::class, 'tf', Join::WITH, 'a = tf.link AND tf.type = \'folder\'')
            ->where('a.referrer = :user');

        if ($from) $query->andWhere('s.createdAt >= :from');
        if ($to) $query->andWhere('s.createdAt <= :to');
        if ($linkId) $query->andWhere('s.affiliateShortLink = :link');
        if ($tags) {
            $query->andWhere('tt.name IN (\''.str_replace(', ', '\', \'', strip_tags(
                    implode(', ', $tags)
                )).'\')');
        };
        if ($folders) {
            $query->andWhere('tf.name IN (\''.str_replace(', ', '\', \'', strip_tags(
                    implode(', ', $folders)
                )).'\')');
        };

        $query->setParameter('user', $user);

        if ($from) $query->setParameter('from', $from);
        if ($to) $query->setParameter('to', $to);
        if ($linkId) $query->setParameter('link', $linkId);

        $query = $query->groupBy('day')->getQuery()->getResult();

        $this->em->getFilters()->enable('softdeleteable');
        return (array) $query;
    }
}
