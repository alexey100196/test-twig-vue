<?php


namespace App\Controller\Api\V1;


use App\Controller\Api\ApiController;
use App\Entity\EventEntry;
use App\Entity\EventEntryType;
use App\Entity\Shop;
use App\Entity\User;
use App\Form\ShopEvent\PurchaseType;
use App\Form\ShopEvent\SignUpType;
use App\Form\ShopEvent\TestConnectionType;
use App\Form\ShopEvent\ViewType;
use App\Repository\EventEntryTypeRepository;
use App\Repository\ShopRepository;
use App\Service\Shop\ShopCookie;
use App\Service\ShopEvent\EventService;
use App\Util\Consts;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ShopCookieController extends ApiController
{
    /**
     * @var ShopRepository
     */
    protected $shopRepository;

    /**
     * @var ShopCookie
     */
    protected $shopCookie;

    /**
     * ShopCookieController constructor.
     * @param ShopRepository $shopRepository
     * @param ShopCookie $shopCookie
     */
    public function __construct(ShopRepository $shopRepository, ShopCookie $shopCookie)
    {
        $this->shopRepository = $shopRepository;
        $this->shopCookie = $shopCookie;
    }

    /**
     * @Route("/api/v1/company/{partnerNumber}/cookie", name="api_v1_shop_cookie_index")
     * @param $partnerNumber
     * @return JsonResponse
     */
    public function index($partnerNumber)
    {
        $shop = $this->shopRepository->findOneBy(['partnerNumber' => (string)$partnerNumber]);

        if (!$shop) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        return $this->makeResponse([
            'time' => $this->shopCookie->getShopCookieLifeTime($shop),
            'bannerTime' => $this->shopCookie->getShopBannerCookieLifeTime($shop),
        ], Response::HTTP_OK);
    }
}