<?php

namespace App\Entity\Glossary;

use App\Entity\BaseGlossary;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ShopType extends BaseGlossary
{
    const SAAS = 1;
    const ECOMMERCE = 2;

    const SAAS_NAME = 'saas';
    const ECOMMERCE_NAME = 'ecommerce';

    public static function idFromName(string $name)
    {
        $name = strtolower($name);

        if ($name === self::SAAS_NAME) {
            return self::SAAS;
        } elseif ($name === self::ECOMMERCE_NAME) {
            return self::ECOMMERCE;
        }

        return null;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
