<?php


namespace App\Service\Subscription\PlanTypeStrategy\Strategies;


use App\Entity\Subscription\PlanSubscription;

interface StrategyInterface
{
    public function process(PlanSubscription $planSubscription);
}