<?php


namespace App\Validator\Constraints;


use App\Entity\Product;
use App\Repository\OfferRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ShopOfferValidator extends ConstraintValidator
{
    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        $shopOffer = $value->getCpsOffer();

        foreach ($value->getProducts() as $product) {
            if ($productOffer = $product->getCpsOffer()) {
                if ($productOffer->getCommission() < $shopOffer->getCommission()) {
                    $this->context->buildViolation($constraint->message)->addViolation();

                    return;
                }
            }
        }

        unset($productOffer, $shopOffer);
    }
}