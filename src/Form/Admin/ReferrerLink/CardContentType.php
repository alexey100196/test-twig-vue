<?php


namespace App\Form\Admin\ReferrerLink;


use App\Entity\ReferrerLinkCard;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class CardContentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'constraints' => [new Length(['min' => 3, 'max' => 128])],
            ])
            ->add('subtitle', null, [
                'constraints' => [new Length(['min' => 3, 'max' => 128])],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReferrerLinkCard::class,
        ]);
    }
}