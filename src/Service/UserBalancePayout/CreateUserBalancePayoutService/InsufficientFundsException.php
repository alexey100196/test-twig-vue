<?php


namespace App\Service\UserBalancePayout\CreateUserBalancePayoutService;


use Throwable;

class InsufficientFundsException extends \LogicException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $message = 'Insufficient funds for currency ' . $message;

        parent::__construct($message, $code, $previous);
    }
}
