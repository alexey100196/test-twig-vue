<?php


namespace App\Service\Shop;


use App\Entity\Budget;
use App\Entity\Currency;
use App\Entity\Shop;
use App\Event\Shop\ShopDeclaredBudget;
use App\Repository\CurrencyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ShopBudget
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $eventDispatcher)
    {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Shop $shop
     * @param Currency $currency
     * @param float $amount
     * @param bool $declared
     * @return Budget|null
     */
    public function updateBudget(Shop $shop, Currency $currency, float $amount = 0, bool $declared = false)
    {
        if (!$budget = $this->getBudgetForCurrency($shop, $currency)) {
            $budget = new Budget();
            $budget->setCurrency($currency);
            $shop->addBudget($budget);
        }

        $budget->setAmount($amount);

        $this->em->persist($shop);
        $this->em->flush();

        $this->eventDispatcher->dispatch(ShopDeclaredBudget::NAME, new ShopDeclaredBudget($shop, $budget, $declared));

        return $budget;
    }

    /**
     * @param Shop $shop
     * @param Currency $currency
     * @return Budget
     */
    public function getOrCreateBudgetForCurrency(Shop $shop, Currency $currency): Budget
    {
        if (!$budget = $this->getBudgetForCurrency($shop, $currency)) {
            $budget = new Budget();
            $budget->setCurrency($currency);
        }

        return $budget;
    }

    /**
     * @param Shop $shop
     * @param Currency $currency
     * @return mixed|null
     */
    protected function getBudgetForCurrency(Shop $shop, Currency $currency)
    {
        foreach ($shop->getBudgets() as $budget) {
            if ($budget->getCurrency() === $currency) {
                return $budget;
            }
        }

        return null;
    }
}