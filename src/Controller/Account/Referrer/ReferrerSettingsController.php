<?php


namespace App\Controller\Account\Referrer;

use App\Service\Referrer\ReferrerNickname;
use App\Service\User\ChangePassword;
use App\Util\Consts;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\Account\EditAccountBillingDataType;
use App\Form\Account\EditAccountPasswordType;
use App\Form\Account\EditAccountType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerSettingsController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER')")
 */
class ReferrerSettingsController extends AbstractController
{
    /**
     * @var ChangePassword
     */
    private $changePassword;

    private $referrerNickname;

    /**
     * @param ChangePassword $changePassword
     * @param ReferrerNickname $referrerNickname
     */
    public function __construct(ChangePassword $changePassword, ReferrerNickname $referrerNickname)
    {
        $this->changePassword = $changePassword;
        $this->referrerNickname = $referrerNickname;
    }

    /**
     * Show settings.
     *
     * @Route("/account/referrer/settings", name="account_referrer_settings")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request)
    {
        $form = $this->createForm(EditAccountType::class, $user = $this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->referrerNickname->updateNickname($user);
            $this->addFlash(Consts::COMPANY_DETAILS_SUCCESS, 'Basic data saved.');
            return $this->redirectToRoute('account_referrer_settings');
        }

        return $this->render('account/referrer/edit.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/account/edit/password", name="account_referrer_settings_password")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editPassword(Request $request)
    {
        $form = $this->createForm(EditAccountPasswordType::class, $user = $this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->changePassword->changePassword($user, $form->getData()->getPlainPassword());
            $this->addFlash(Consts::SUCCESS, 'Password changed.');
            return $this->redirectToRoute('account_referrer_settings_password');
        }

        return $this->render('account/referrer/edit-password.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/account/edit/billing-data", name="account_referrer_settings_billing")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editBilling(Request $request)
    {
        $form = $this->createForm(EditAccountBillingDataType::class, $user = $this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(Consts::COMPANY_DETAILS_SUCCESS, 'Billing data saved.');
            return $this->redirectToRoute('account_referrer_settings_billing');
        }

        return $this->render('account/referrer/edit-billing.html.twig', ['form' => $form->createView()]);

    }
}