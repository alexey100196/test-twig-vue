<?php


namespace App\Service\IncomeStatistics\ReferrerIncomeStatistics\Commands;


use App\Entity\User;
use App\Service\IncomeStatistics\CommandInterface;
use Doctrine\Common\Collections\ArrayCollection;

class GenerateShopsSummaryStats implements CommandInterface
{
    /**
     * @var ArrayCollection
     */
    protected $shops;

    /**
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @var User
     */
    protected $referrer;

    /**
     * @var string
     */
    protected $filterBy;

    /**
     * GenerateStats constructor.
     * @param ArrayCollection $shops
     * @param User $referrer
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param string $filterBy
     */
    public function __construct(ArrayCollection $shops, User $referrer, \DateTime $startDate, \DateTime $endDate, string $filterBy = '')
    {
        $this->shops = $shops;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->referrer = $referrer;
        $this->filterBy = $filterBy;
    }

    /**
     * @return ArrayCollection
     */
    public function getShops(): ArrayCollection
    {
        return $this->shops;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    /**
     * @return User
     */
    public function getReferrer(): User
    {
        return $this->referrer;
    }

    /**
     * @return string
     */
    public function getFilterBy(): string
    {
        return $this->filterBy;
    }

}