<?php


namespace App\Event\Subscription;


use App\Entity\Subscription\PlanSubscription;
use App\Entity\Transaction;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\Event;

class SubscriptionExpired extends Event
{
    /**
     * @var string
     */
    const NAME = 'subscription.expired';

    /**
     * @var PlanSubscription
     */
    protected $subscription;

    public function __construct(PlanSubscription $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * @return PlanSubscription
     */
    public function getSubscription(): PlanSubscription
    {
        return $this->subscription;
    }
}