<?php


namespace App\Event\Shop;

use App\Entity\Shop;
use Symfony\Component\EventDispatcher\Event;

class ShopChangedOffer extends Event
{
    protected $shop;

    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }
}