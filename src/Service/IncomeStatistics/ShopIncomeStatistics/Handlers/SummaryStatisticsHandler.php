<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Handlers;


use App\Entity\ShopInteraction;
use App\Service\IncomeStatistics\CommandInterface;
use App\Service\IncomeStatistics\Handler;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Entities\SummaryStatistics;

class SummaryStatisticsHandler extends Handler
{
    public function handle(CommandInterface $command)
    {
        $stats = $this->doctrine
            ->getRepository(ShopInteraction::class)
            ->getShopSummaryStats($command->getShop());

        return new SummaryStatistics(
            (int)$stats['viewsCount'],
            (int)$stats['purchaseCount'],
            (int)$stats['signUpCount'],
            (float)$stats['purchaseTotal'],
            (float)$stats['systemCommission'],
            (float)$stats['referrerProfit'],
            (float)$stats['purchaseReferrerProfit'],
            (float)$stats['signUpReferrerProfit']
        );
    }
}