<?php


namespace App\Controller\Ajax;


use App\Controller\AjaxAbstractController;
use App\Entity\ReferrerLink;
use App\Entity\ReferrerMyStore;
use App\Entity\ReferrerStoreNonReferableLink;
use App\Entity\Shop;
use App\Form\Referrer\ReferrerStoreNonReferableLinkType;
use App\Repository\ReferrerStoreNonReferableLinkRepository;
use App\Service\OpenGraph\OpenGraphMetaData;
use App\Service\Referrer\ReferrerLinkStoreCard;
use App\Service\Referrer\ReferrerMyStoreService;
use App\Service\Referrer\ReferrerStoreNonReferableLinkService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Form;

/**
 * Class ReferrerShopCardController
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerNonReferableLinkController extends AjaxAbstractController
{
    /**
     * @var OpenGraphMetaData
     */
    private $openGraphMetaData;

    /**
     * @var ReferrerMyStoreService
     */
    private $referrerMyStoreService;

    /**
     * @var ReferrerStoreNonReferableLinkService
     */
    private $referrerStoreNonReferableLinkService;

    /**
     * @var ReferrerStoreNonReferableLinkRepository
     */
    private $referrerStoreNonReferableLinkRepository;

    /**
     * ReferrerNonReferableLinkController constructor.
     * @param OpenGraphMetaData $openGraphMetaData
     * @param ReferrerMyStoreService $referrerMyStoreService
     * @param ReferrerStoreNonReferableLinkService $referrerStoreNonReferableLinkService
     * @param ReferrerStoreNonReferableLinkRepository $referrerStoreNonReferableLinkRepository
     */
    public function __construct(
        OpenGraphMetaData $openGraphMetaData,
        ReferrerMyStoreService $referrerMyStoreService,
        ReferrerStoreNonReferableLinkService $referrerStoreNonReferableLinkService,
        ReferrerStoreNonReferableLinkRepository $referrerStoreNonReferableLinkRepository
    ) {
        $this->openGraphMetaData = $openGraphMetaData;
        $this->referrerMyStoreService = $referrerMyStoreService;
        $this->referrerStoreNonReferableLinkService = $referrerStoreNonReferableLinkService;
        $this->referrerStoreNonReferableLinkRepository = $referrerStoreNonReferableLinkRepository;
    }

    /**
     * @Route(
     *     "/ajax/non-referable-link",
     *     name="ajax_non_referable_link",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @return Response
     */
    public function saveLink(Request $request)
    {
        $data = $request->request->all();
        $formData = $data;
        unset($formData['id']);
        $user = $this->getUser();
        $myStore = $user->getReferrerMyStore();
        $ylForm = $this->createForm(ReferrerStoreNonReferableLinkType::class);
        $ylForm->submit($formData);
        unset($formData);

        if (!$myStore) {
            $myStore = new ReferrerMyStore();
            $this->referrerMyStoreService->generate($user, $myStore);
        }

        if (!$ylForm->isValid()) {
            $ylErrorMessages = $this->getFormErrorMessages($ylForm);
            return new JsonResponse(['errors' => $ylErrorMessages], Response::HTTP_UNPROCESSABLE_ENTITY);
        } else if ($myStore->getNonReferableLinks()->count() >= ReferrerMyStore::MAX_CUSTOM_LINKS) {
            return new JsonResponse(['errors' => [
                'Your links' => 'You can add only '.ReferrerMyStore::MAX_CUSTOM_LINKS.' custom links (Present count: '.$myStore->getNonReferableLinks()->count().').'
            ]], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if (!isset($data['id'])) {
            $nonReferableLink = new ReferrerStoreNonReferableLink();
            $nonReferableLink->setMyStore($myStore);
            $this->openGraphMetaData->fetch((string)$data['destinationUrl']);
            $nonReferableLink->setImage($this->openGraphMetaData->getImage() ?? null);
            $nonReferableLink->setTitle($this->openGraphMetaData->getTitle() ?? null);
        } else {
            $nonReferableLink = $this->referrerStoreNonReferableLinkRepository->find($data['id']) ?? new ReferrerStoreNonReferableLink();
            if ((int)$data['id'] <= 0) {
                $nonReferableLink->setImage($data['image'] ?? null);
                $nonReferableLink->setVideo($data['video'] ?? null);
                $nonReferableLink->setLayout($data['layout'] ?? null);
                $nonReferableLink->setCustomTitle($data['customTitle'] ?? null);
            }
        }
        $nonReferableLink->setDestinationUrl($data['destinationUrl']);

        $this->referrerStoreNonReferableLinkService->save($nonReferableLink);

        $customLinks = $this->referrerStoreNonReferableLinkRepository->findBy([
            'mystore' => $myStore
        ], ['orderColumn' => 'ASC', 'updatedAt' => 'DESC']);

        return new JsonResponse([
            'success' => true,
            'links' => $this->referrerStoreNonReferableLinkService->loadItemsToArray($customLinks),
        ]);
    }

    /**
     * @Route(
     *     "/ajax/non-referable-link/{id}",
     *     name="ajax_non_referable_link_remove",
     *     methods={"POST"}
     * )
     * @param int $id
     * @return Response
     */
    public function remove(int $id)
    {
        try {
            $user = $this->getUser();
            $myStore = $user->getReferrerMyStore();
            $referrerStoreNonReferableLink = $this->referrerStoreNonReferableLinkRepository->find($id);

            if (!$myStore) {
                $myStore = new ReferrerMyStore();
                $this->referrerMyStoreService->generate($user, $myStore);
            }

            if (!$referrerStoreNonReferableLink) {
                return new JsonResponse(['errors' => [
                    'link' => 'Not found link'
                ]], Response::HTTP_UNPROCESSABLE_ENTITY);
            } else if ($referrerStoreNonReferableLink->getMyStore() !== $myStore) {
                return new JsonResponse(['errors' => [
                    'link' => 'It\'s not your link'
                ]], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $this->referrerStoreNonReferableLinkService->remove($referrerStoreNonReferableLink);

            $customLinks = $this->referrerStoreNonReferableLinkRepository->findBy([
                'mystore' => $myStore
            ], ['orderColumn' => 'ASC', 'updatedAt' => 'DESC']);
        } catch (\Exception $e) {
            return new JsonResponse(['errors' => [
                'link' => $e->getMessage()
            ]], $e->getCode());
        }

        return new JsonResponse([
            'success' => true,
            'links' => $this->referrerStoreNonReferableLinkService->loadItemsToArray($customLinks),
        ]);
    }

    /**
     * Return form errors as array.
     *
     * @param Form $form
     * @return array
     */
    protected function getFormErrorMessages(Form $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $key => $error) {
            $params = $error->getMessageParameters();
            if ($params && isset($params['{{ value }}'])) {
                $errors[] = $error->getMessage() .' ('. $params['{{ value }}'].')';
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrorMessages($child);
            }
        }

        return $errors;
    }

}