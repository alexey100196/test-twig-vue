<?php

namespace App\Controller\Admin;

use App\Entity\Subscription\PlanSubscription;
use App\Service\Subscription\SubscriptionService;
use App\Traits\Controller\FormResponseArray;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SubscriptionController extends AbstractController
{
    use FormResponseArray;

    private $subscriptionService;

    public function __construct(SubscriptionService $subscriptionService)
    {
        $this->subscriptionService = $subscriptionService;
    }

    /**
     * @Route(
     *     "/admin/subscription/{planSubscription}/renew",
     *     name="admin_transaction_renew",
     *     methods={"POST"}
     * )
     * @param PlanSubscription $planSubscription
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(PlanSubscription $planSubscription)
    {
        $this->subscriptionService->renewPlan($planSubscription);

        $this->addFlash(Consts::SUCCESS, 'Subscription processed');

        return new JsonResponse([]);
    }
}
