<?php

namespace App\Helper\ShopCompletion\Resolvers;

use App\Helper\ShopCompletion\Resolvers\AbstractResolver;

class TestConnection extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        if ($this->shop->getSnippetConnectedAt()) {
            $this->addScore(100);
        }
    }
}
