<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation\Groups;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserShopRepository")
 */
class UserShop
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"shop"})
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userShops")
     */
    private $user;

    /**
     * @Groups({"shop"})
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="userShops")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, options={"default": 0, "unsigned": true})
     */
    private $totalIncome = 0;

    /**
     * @ORM\Column(type="integer", options={"default": 0, "unsigned": true})
     */
    private $signups = 0;

    /**
     * @ORM\Column(type="integer", options={"default": 0, "unsigned": true})
     */
    private $clicks = 0;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $activated = false;

    public function getId():  ? int
    {
        return $this->id;
    }

    public function getUser() :  ? User
    {
        return $this->user;
    }

    public function setUser( ? User $user) : self
    {
        $this->user = $user;

        return $this;
    }

    public function getShop() :  ? Shop
    {
        return $this->shop;
    }

    public function setShop( ? Shop $shop) : self
    {
        $this->shop = $shop;

        return $this;
    }

    public function getTotalIncome()
    {
        return $this->totalIncome;
    }

    public function setTotalIncome($totalIncome) : self
    {
        $this->totalIncome = $totalIncome;

        return $this;
    }

    public function getSignups():  ? int
    {
        return $this->signups;
    }

    public function setSignups(int $signups) : self
    {
        $this->signups = $signups;

        return $this;
    }

    public function getClicks():  ? int
    {
        return $this->clicks;
    }

    public function setClicks(int $clicks) : self
    {
        $this->clicks = $clicks;

        return $this;
    }

    public function getActivated():  ? bool
    {
        return $this->activated;
    }

    public function setActivated(bool $activated) : self
    {
        $this->activated = $activated;

        return $this;
    }
}
