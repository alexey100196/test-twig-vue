<?php

namespace App\Controller;

use App\Entity\BrandRequest;
use App\Entity\Glossary\ShopType;
use App\Entity\Shop;
use App\Entity\ShopStatus;
use App\Form\BrandRequestType;
use App\Repository\ShopRepository;
use App\Service\Shop\ShopCompletion;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

class ShopController extends AbstractController
{
    const PER_PAGE = 15;

    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * @var ShopCompletion
     */
    private $shopCompletion;

    /**
     * @param ShopRepository $shopRepository
     */
    public function __construct(ShopRepository $shopRepository, ShopCompletion $shopCompletion)
    {
        $this->shopRepository = $shopRepository;
        $this->shopCompletion = $shopCompletion;
    }

    private function processBrandRequestForm($brandRequestForm)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($brandRequestForm->getData());
        $em->flush();

        $this->addFlash(Consts::SUCCESS, 'Thank you, we will respond to your email as soon as possible.');

        return $this->redirectToRoute('shop_index');
    }

    /**
     * @Route("/marketplace", methods={"GET", "POST"}, name="shop_index")
     * @Route("/marketplace/{type}", methods={"GET", "POST"}, name="shop_index_typed")
     * @param string|null $type
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(string $type = null, Request $request, PaginatorInterface $paginator)
    {
        $brandRequestForm = $this->createForm(BrandRequestType::class, $brandRequest = new BrandRequest());
        $brandRequestForm->handleRequest($request);

        if ($brandRequestForm->isSubmitted() && $brandRequestForm->isValid()) {
            return $this->processBrandRequestForm($brandRequestForm);
        }

        $em = $this->getDoctrine()->getManager();

        $shopType = $type ? $em->getReference(ShopType::class, ShopType::idFromName($type)) : null;

        $shops = $paginator->paginate(
            $this->shopRepository->findAllWithStatusAndRewardModes([ShopStatus::ACTIVE_NAME],[Shop::REWARD_MODE_CASH, Shop::REWARD_MODE_COUPON_CODES_AND_CASH], $shopType)->getQuery(),
            $request->query->getInt('page', 1),
            self::PER_PAGE
        );

        return $this->render('shop/index.html.twig', [
            'shops' => $shops,
            'shopType' => $shopType,
            'brandRequestForm' => $brandRequestForm->createView(),
        ]);
    }

    /**
     * @Route("/company/{id}", name="shop_show")
     */
    public function show($id)
    {
        if (!$shop = $this->shopRepository->find($id)) {
            throw $this->createNotFoundException('Shop does not exists.');
        }

        return $this->render('shop/show.html.twig', compact('shop'));
    }
}
