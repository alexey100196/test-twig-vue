<?php

namespace App\Event\Shop;

use App\Entity\Currency;
use App\Entity\Shop;
use Symfony\Component\EventDispatcher\Event;

class ShopBalanceDecreased extends Event
{
    /** @var string */
    const NAME = 'shop.balance-decreased';

    /**
     * @var Shop
     */
    private $shop;

    /**
     * @var Currency
     */
    private $currency;

    public function __construct(Shop $shop, Currency $currency)
    {
        $this->shop = $shop;
        $this->currency = $currency;
    }

    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }
}