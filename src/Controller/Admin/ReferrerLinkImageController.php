<?php

namespace App\Controller\Admin;

use App\Entity\ReferrerLink;
use App\Entity\ShopStatus;
use App\Form\Admin\ReferrerLink\CardContentType;
use App\Form\Admin\ReferrerLink\StoreCardContentType;
use App\Form\Admin\Shop\EditShopType;
use App\Repository\ReferrerLinkRepository;
use App\Traits\Controller\FormResponseArray;
use App\Util\Consts;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReferrerLinkImageController extends AbstractController
{
    use FormResponseArray;

    const PER_PAGE = 25;

    /**
     * @var ReferrerLinkRepository
     */
    private $referrerLinkRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param ReferrerLinkRepository $referrerLinkRepository
     */
    public function __construct(ReferrerLinkRepository $referrerLinkRepository, EntityManagerInterface $em)
    {
        $this->referrerLinkRepository = $referrerLinkRepository;
        $this->em = $em;
    }

    /**
     * @Route("/admin/referrer-links/images", name="admin_referrer_link_images_index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $links = $paginator->paginate(
            $this->referrerLinkRepository->createQueryBuilder('l')->getQuery(),
            $request->query->getInt('page', 1),
            self::PER_PAGE
        );

        return $this->render('admin/link-images/index.html.twig', ['links' => $links]);
    }

    /**
     * @Route(
     *     "/admin/referrer-links/{referrerLink}/card-image",
     *     name="admin_referrer_link_card_image_delete",
     *     methods={"DELETE"}
     * )
     * @param ReferrerLink $referrerLink
     * @return JsonResponse
     */
    public function deleteCardImage(ReferrerLink $referrerLink)
    {
        if ($card = $referrerLink->getCard()) {
            $card->incrementAdminImageUpdates();
            $card->deleteBackground();
            $this->em->flush();
            $this->addFlash(Consts::SUCCESS, 'Card image deleted.');
        }

        return new JsonResponse([], Response::HTTP_OK);
    }

    /**
     * @Route(
     *     "/admin/referrer-links/{referrerLink}/store-image",
     *     name="admin_referrer_link_store_image_delete",
     *     methods={"DELETE"}
     * )
     * @param ReferrerLink $referrerLink
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteMyStoreImage(ReferrerLink $referrerLink)
    {
        if ($storeCard = $referrerLink->getStoreCard()) {
            $storeCard->incrementAdminImageUpdates();
            $storeCard->deleteImage();
            $this->em->flush();
            $this->addFlash(Consts::SUCCESS, 'MyStore image deleted.');
        }

        return new JsonResponse([], Response::HTTP_OK);
    }
}
