<?php


namespace App\Service\DocumentReferrer;


use App\Entity\DocumentReferrer\DocumentReferrer;
use App\Repository\DocumentReferrer\DocumentReferrerRepository;
use App\Service\DocumentReferrer\ValueObject\ReferrerLink;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class DocumentReferrerService
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var DocumentReferrerRepository */
    private $documentReferrerRepository;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(
        EntityManagerInterface $em,
        DocumentReferrerRepository $documentReferrerRepository,
        LoggerInterface $logger
    )
    {
        $this->em = $em;
        $this->documentReferrerRepository = $documentReferrerRepository;
        $this->logger = $logger;
    }

    /**
     * @param string $url
     * @return DocumentReferrer|null
     */
    public function create(string $url): ?DocumentReferrer
    {
        try {
            $link = new ReferrerLink($url);

            $mainReferrer = $this->findDocumentReferrer(['referrer' => $link->getBaseUrl()]);

            if (null === $mainReferrer) {
                $mainReferrer = new DocumentReferrer($link->getBaseUrl());
            }

            $this->em->persist($mainReferrer);
            $this->em->flush();

            return $mainReferrer;

        } catch (\Exception $e) {
            $this->logger->warning('Document referrer creation problem.', [
                'msg' => $e->getMessage(),
                'url' => $url,
            ]);

            return null;
        }
    }

    /**
     * @param array $search
     * @return DocumentReferrer|null
     */
    private function findDocumentReferrer(array $search): ?DocumentReferrer
    {
        return $this->documentReferrerRepository->findOneBy($search);
    }
}