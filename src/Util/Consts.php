<?php


namespace App\Util;


class Consts
{
    const SUCCESS = 'success';
    const ERROR = 'error';

    const COMPANY_DETAILS_SUCCESS = 'company_details_success';

    const SNIPPET_TEST_PARAM_NAME = 'siTest';
    const SNIPPET_REFERRER_PARAM_NAME = 'siReferrer';
    const SNIPPET_PRODUCT_PARAM_NAME = 'siProduct';

    const SNIPPET_DEFAULT_COOKIE_LIFETIME_DAYS = 30;
    const SNIPPET_DEFAULT_BANNER_COOKIE_LIFETIME_DAYS = 30;

    const GUEST_SUBSCRIPTION_PLAN_COOKIE_NAME = 'subscription-plan';

    const SNIPPET_COOKIE_NAME = 'SuperInterface3';
}