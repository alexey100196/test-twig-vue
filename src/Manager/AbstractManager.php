<?php
namespace App\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

abstract class AbstractManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var Registry
     */
    protected $doctrine;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @required
     */
    public function setEntityManager(ManagerRegistry $doctrine, LoggerInterface $logger): void
    {
        $this->doctrine = $doctrine;
        $this->entityManager = $this->doctrine->getManager();
        $this->logger = $logger;
    }
}
