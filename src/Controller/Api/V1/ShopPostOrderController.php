<?php

namespace App\Controller\Api\V1;

use App\Controller\Api\ApiController;
use App\Repository\ShopRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ShopPostOrderController extends ApiController
{
    /**
     * @Route("/api/v1/{number}/post-order", name="api_v1_post_order_info")
     * @param string $number
     * @param ShopRepository $shopRepository
     * @return JsonResponse
     */
    public function info(string $number, ShopRepository $shopRepository)
    {
        $shop = $shopRepository->findOneBy(['partnerNumber' => $number]);

        return new JsonResponse([
            'enabled' => $shop ? boolval($shop->getPostOrderPopupEnabled()) : false,
        ]);
    }
}