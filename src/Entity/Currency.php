<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyRepository")
 */
class Currency extends BaseGlossary
{
    const EUR_ID = 1;

    const EUR = 'EUR';
}