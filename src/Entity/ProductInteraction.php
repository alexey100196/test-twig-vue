<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\InheritanceType("JOINED")
 * @ORM\Entity(repositoryClass="App\Repository\ProductInteractionRepository")
 */
class ProductInteraction
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ReferrerLink", inversedBy="productPurchases")
     * @ORM\JoinColumn(nullable=false)
     */
    private $referrerLink;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $referrer;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $systemCommission;

    public function __construct()
    {
        $this->systemCommission = 0;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getReferrer(): ?User
    {
        return $this->referrer;
    }

    public function setReferrer(?User $referrer): self
    {
        $this->referrer = $referrer;

        return $this;
    }

    public function getReferrerLink(): ?ReferrerLink
    {
        return $this->referrerLink;
    }

    public function setReferrerLink(?ReferrerLink $referrerLink): self
    {
        $this->referrerLink = $referrerLink;
        $this->setReferrer($this->referrerLink->getReferrer());

        return $this;
    }

    public function getSystemCommission()
    {
        return $this->systemCommission;
    }

    public function setSystemCommission($systemCommission): self
    {
        $this->systemCommission = $systemCommission;

        return $this;
    }
}
