<?php


namespace App\Controller\Ajax;


use App\Controller\AjaxAbstractController;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Service\OpenGraph\OpenGraphMetaData;
use App\Service\Referrer\ReferrerLinkStoreCardService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerShopCardController
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerLinkController extends AjaxAbstractController
{
    /** @var OpenGraphMetaData */
    private $openGraphMetaData;

    public function __construct(OpenGraphMetaData $openGraphMetaData)
    {
        $this->openGraphMetaData = $openGraphMetaData;
    }

    /**
     * @Route(
     *     "/ajax/referrer-link/{referrerLink}/open-graph",
     *     name="ajax_referrer_link_og_image",
     *     methods={"GET"}
     * )
     * @param ReferrerLink $referrerLink
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getOpenGraph(ReferrerLink $referrerLink)
    {
        if (!$this->getUser()->hasReferrerLink($referrerLink)) {
            throw new NotFoundHttpException();
        }

        if ($url = $referrerLink->getDestinationUrl()) {
            $this->openGraphMetaData->fetch((string)$url);

            dump($this->openGraphMetaData);exit;
            //@TODO: Fix of load meta data from destination url

            return new JsonResponse([
                'image' => $this->openGraphMetaData->getImage(),
                'title' => $this->openGraphMetaData->getTitle(),
                'description' => $this->openGraphMetaData->getDescription(),
            ]);
        }

        return new JsonResponse([]);
    }
}