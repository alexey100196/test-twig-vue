<?php


namespace App\Controller\Account\Shop;


use App\Entity\Shop;
use App\Form\Shop\ShopUrlName;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ShopSubdomainController extends AbstractController
{
    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/edit/subdomain", name="account_shop_edit_subdomain", methods={"GET", "PATCH"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request)
    {
        $shop = $this->getUser()->getShop();

        $form = $this->createForm(ShopUrlName::class, $shop, ['method' => 'PATCH']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shop->setWizardStep(9);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash(Consts::COMPANY_DETAILS_SUCCESS, 'Configure your subdomain: Data saved.');

            return $this->redirectToRoute($shop->getProducts()->count() ? 'account_shop_edit_product_number' : 'account_shop_edit_finish');
        }

        return $this->render('account/shop/edit-subdomain.html.twig', [
            'shop' => $shop,
            'form' => $form->createView()
        ]);
    }
}