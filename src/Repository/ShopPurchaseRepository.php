<?php

namespace App\Repository;

use App\Entity\CommissionType;
use App\Entity\Product;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\ShopConversionStatus;
use App\Entity\ShopPurchase;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopPurchase|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopPurchase|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopPurchase[]    findAll()
 * @method ShopPurchase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopPurchaseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopPurchase::class);
    }

    public function sumShopTurnover(Shop $shop, \DateTime $start = null, \DateTime $end = null)
    {
        $qb = $this->createQueryBuilder('sp')
            ->select('SUM(sp.amount * sp.price)')
            ->andWhere('sp.shop = :shop')
            ->andWhere('sp.status = :status')
            ->setParameter('shop', $shop)
            ->setParameter('status', ShopConversionStatus::ACCEPTED);

        if ($start) {
            $qb = $qb->andWhere('sp.createdAt >= :start')->setParameter('start', $start);
        }

        if ($end) {
            $qb = $qb->andWhere('sp.createdAt <= :end')->setParameter('end', $end);
        }

        return (float)$qb->getQuery()->getSingleScalarResult();
    }
}
