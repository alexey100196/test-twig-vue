<?php

namespace App\Controller\Account\Referrer;

use App\Entity\Coupon\CouponPackage;
use App\Repository\Coupon\UserCouponRepository;
use App\Service\Referrer\ReferrerShop;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerCouponController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerCouponsController extends AbstractController
{
    const PER_PAGE = 15;

    /**
     * @var ReferrerShop $referrerShop
     */
    private $referrerShop;

    /**
     * @var UserCouponRepository
     */
    private $userCouponRepository;


    /**
     * ReferrerCouponsController constructor.
     * @param ReferrerShop $referrerShop
     */
    public function __construct(ReferrerShop $referrerShop, UserCouponRepository $userCouponRepository)
    {
        $this->referrerShop = $referrerShop;
        $this->userCouponRepository = $userCouponRepository;
    }

    /**
     * @Route("/account/referrer/coupons/{type}", name="account_referrer_coupons_index")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(string $type = null, PaginatorInterface $paginator, Request $request)
    {
        if (!\in_array($type, CouponPackage::ALLOCATION_NAMES)) {
            return $this->redirectToRoute('account_referrer_coupons_index', ['type' => CouponPackage::ALLOCATION_NAMES[CouponPackage::ALLOCATION_REFERRER]]);
        }

        $user = $this->getUser();
        $userShops = $this->referrerShop->getReferrerShopsWithStats($user);
        $globalStats = $this->referrerShop->getReferrerStats($user);

        $userCoupons = $paginator->paginate(
            $this->userCouponRepository->findAllByUserQuery($user, [array_search($type, CouponPackage::ALLOCATION_NAMES)]),
            $request->query->getInt('page', 1),
            self::PER_PAGE
        );

        return $this->render('account/referrer/coupons/index.html.twig', [
            'userShops' => $userShops,
            'globalStats' => $globalStats,
            'userCoupons' => $userCoupons,
            'type' => $type,
        ]);
    }
}