<?php


namespace App\Controller\Ajax;


use App\Controller\AjaxAbstractController;
use App\Entity\DocumentReferrer\DocumentReferrer;
use App\Entity\ReferrerLink;
use App\Entity\ShopConversion;
use App\Entity\User;
use App\Repository\ShopConversionRepository;
use App\Repository\UserRepository;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateReferrerLinkStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateReferrerReflinksStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateReferrersStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateSingleSourceTrafficStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateSourceTrafficStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateSummaryStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\ShopStatisticsService;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * Class ShopIncomeController
 *
 * @Security("has_role('ROLE_SHOP')")
 */
class ShopIncomeController extends AjaxAbstractController
{
    /**
     * @var ShopStatisticsService
     */
    protected $shopStatisticsService;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var ShopConversionRepository
     */
    protected $shopConversionRepository;

    /**
     * ShopIncomeController constructor.
     * @param ShopStatisticsService $shopStatisticsService
     * @param UserRepository $userRepository
     * @param ShopConversionRepository $shopConversionRepository
     */
    public function __construct(
        ShopStatisticsService $shopStatisticsService,
        UserRepository $userRepository,
        ShopConversionRepository $shopConversionRepository
    )
    {
        $this->shopStatisticsService = $shopStatisticsService;
        $this->userRepository = $userRepository;
        $this->shopConversionRepository = $shopConversionRepository;
    }

    /**
     * Get shop stats for referrers.
     *
     * @Route("/ajax/company/income/summary", name="ajax_shop_income_summary_index")
     * @throws \Exception
     */
    public function summary()
    {
        $command = new GenerateSummaryStatistics($this->getUser()->getShop());

        return $this->jsonResponse(
            $this->shopStatisticsService->generate($command)
        );
    }

    /**
     * Get shop stats for referrers.
     *
     * @Route("/ajax/company/income/referrers", name="ajax_shop_income_referrers_index")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function referrersIndex(Request $request)
    {
        $this->validateDateRange($request->query->get('start_at'), $request->query->get('end_at'));

        $daily = [];
        $start = new \DateTime($request->query->get('start_at'));
        $end = new \DateTime($request->query->get('end_at'));

        $command = new GenerateReferrersStatistics(
            $this->getUser()->getShop(),
            $start,
            $end,
            array_filter($request->query->get('products', []), 'ctype_digit'),
            $request->query->getBoolean('include_main_offer'),
            $request->query->get('filterBy', ''),
            array_filter($request->query->get('campaigns', []), 'ctype_digit')
        );

        $response = $this->collectionToArray($this->shopStatisticsService->generate($command));
        if ((bool) $request->query->getBoolean('withDaily')) {
            $newStart = clone $start;
            $newEnd = clone $start;
            $newEnd->modify('+1 day');
            do {
                $command = new GenerateReferrersStatistics(
                    $this->getUser()->getShop(),
                    $newStart,
                    $newEnd,
                    array_filter($request->query->get('products', []), 'ctype_digit'),
                    $request->query->getBoolean('include_main_offer'),
                    $request->query->get('filterBy', ''),
                    array_filter($request->query->get('campaigns', []), 'ctype_digit')
                );

                $res = $this->collectionToArray($this->shopStatisticsService->generate($command));
                if (!empty($res)) $daily[$newEnd->format('d')] = $res;
                $newStart->modify('+1 day');
                $newEnd->modify('+1 day');
            } while(
                ((int) $newStart->format('d')) <= ((int) $end->format('d')) &&
                ((int) $newStart->format('m')) === ((int) $end->format('m'))
            );

            $response['daily'] = $daily;
            return new JsonResponse($response);
        }

        return new JsonResponse($response);
    }

    /**
     * Get shop stats by traffic_source.
     *
     * @Route("/ajax/company/income/traffic-source", name="ajax_shop_income_traffic_source_index")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function trafficSourceIndex(Request $request)
    {
        $this->validateDateRange($request->query->get('start_at'), $request->query->get('end_at'));

        $start = new \DateTime($request->query->get('start_at'));
        $end = new \DateTime($request->query->get('end_at'));

        $command = new GenerateSourceTrafficStatistics(
            $this->getUser()->getShop(),
            $start,
            $end,
            array_filter($request->query->get('products', []), 'ctype_digit'),
            $request->query->getBoolean('include_main_offer')
        );

        return new JsonResponse($this->collectionToArray($this->shopStatisticsService->generate($command)));
    }

    /**
     * Get shop stats by single traffic_source.
     *
     * @Route("/ajax/company/income/traffic-source/{documentReferrer}", name="ajax_shop_income_traffic_source_show")
     * @param DocumentReferrer $documentReferrer
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function trafficSourceShow(DocumentReferrer $documentReferrer, Request $request)
    {
        $this->validateDateRange($request->query->get('start_at'), $request->query->get('end_at'));

        $command = new GenerateSingleSourceTrafficStatistics(
            $this->getUser()->getShop(),
            $documentReferrer,
            new \DateTime($request->query->get('start_at')),
            new \DateTime($request->query->get('end_at')),
            array_filter($request->query->get('products', []), 'ctype_digit'),
            $request->query->getBoolean('include_main_offer')
        );

        return new JsonResponse($this->collectionToArray($this->shopStatisticsService->generate($command)));
    }

    /**
     * Get shop stats for referrers.
     *
     * @Route("/ajax/company/income/referrer/{referrer}", name="ajax_shop_income_referrer_show")
     * @param User $referrer
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function referrerShow(User $referrer, Request $request)
    {
        $this->validateDateRange($request->query->get('start_at'), $request->query->get('end_at'));

        $command = new GenerateReferrerReflinksStatistics(
            $this->getUser()->getShop(),
            $referrer,
            new \DateTime($request->query->get('start_at')),
            new \DateTime($request->query->get('end_at')),
            array_filter($request->query->get('products', []), 'ctype_digit'),
            $request->query->getBoolean('include_main_offer'),
            $request->query->get('filterBy', '')
        );

        return new JsonResponse($this->collectionToArray($this->shopStatisticsService->generate($command)));
    }

    /**
     * Get shop stats for referrers.
     *
     * @Route("/ajax/company/income/referrer-link/{referrerLink}", name="ajax_shop_income_referrer_link_show")
     * @param ReferrerLink $referrerLink
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function referrerLinkShow(ReferrerLink $referrerLink, Request $request)
    {
        $this->validateDateRange($request->query->get('start_at'), $request->query->get('end_at'));

        $command = new GenerateReferrerLinkStatistics(
            $referrerLink,
            new \DateTime($request->query->get('start_at')),
            new \DateTime($request->query->get('end_at')),
            $request->query->get('filterBy', '')
        );

        return new JsonResponse($this->collectionToArray($this->shopStatisticsService->generate($command)));
    }


    /**
     * Get shop stats for referrers.
     *
     * @Route("/ajax/company/income/referrer-link/{referrerLink}/exchange-rates", name="ajax_shop_income_referrer_link_exchange_rates_show")
     * @param ReferrerLink $referrerLink
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function referrerLinkExchangeRatesShow(ReferrerLink $referrerLink, Request $request)
    {
        $this->validateDateRange($request->query->get('start_at'), $request->query->get('end_at'));

        $price = $request->query->get('purchase_price');

        $data = $this->shopConversionRepository->getConversionsRatesForReferrerLink(
            $referrerLink,
            new \DateTime($request->query->get('start_at')),
            new \DateTime($request->query->get('end_at')),
            $request->query->getAlpha('offer_type'),
            $request->query->getAlpha('commission_type'),
            (float)$request->query->get('commission'),
            $request->query->get('source_link'),
            $price ? (float) $price : null
        );

        return new JsonResponse($data);
    }

    /**
     * @param $start
     * @param $end
     * @throws NotFoundHttpException
     */
    private function validateDateRange($start, $end)
    {
        foreach ([$start, $end] as $date) {
            if ($start !== date('Y-m-d H:i:s', strtotime($start))) {
                throw new NotFoundHttpException('Invalid dates give.');
            }
        }
    }

    /**
     * @param ArrayCollection $collection
     * @return array
     */
    private function collectionToArray(ArrayCollection $collection)
    {
        return $collection->map(function ($item) {
            return $item->toArray();
        })->toArray();
    }
}
