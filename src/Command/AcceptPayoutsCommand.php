<?php

namespace App\Command;

use App\Repository\UserBalancePayoutRepository;
use App\Service\UserBalancePayout\UserPendingPayoutAmountUpdater;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AcceptPayoutsCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:accept-payouts';

    /**
     * @var UserBalancePayoutRepository
     */
    private $userBalancePayoutRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserPendingPayoutAmountUpdater
     */
    private $userPendingPayoutAmountUpdater;

    public function __construct(
        string $name = null,
        UserBalancePayoutRepository $userBalancePayoutRepository,
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        UserPendingPayoutAmountUpdater $userPendingPayoutAmountUpdater
    )
    {
        parent::__construct($name);

        $this->userBalancePayoutRepository = $userBalancePayoutRepository;
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->userPendingPayoutAmountUpdater = $userPendingPayoutAmountUpdater;
    }

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $payouts = $this->userBalancePayoutRepository->findAllNotAccepted();

        $users = [];

        $this->entityManager->beginTransaction();

        foreach ($payouts as $payout) {
            $payout->accept();
            $users[$payout->getBalance()->getUser()->getId()] = $payout->getBalance()->getUser();
        }

        $this->entityManager->flush();
        $this->entityManager->commit();

        $this->entityManager->clear();

        $this->entityManager->beginTransaction();
        foreach ($users as $user) {
            $this->userPendingPayoutAmountUpdater->update($user);
        }
        $this->entityManager->commit();
    }
}
