<?php

namespace App\Controller\Admin;

use App\Entity\ShopStatus;
use App\Form\Admin\Shop\EditShopType;
use App\Repository\ShopRepository;
use App\Util\Consts;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SubmissionController extends AbstractController
{
    const PER_PAGE = 25;

    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * @param ShopRepository $shopRepository
     */
    public function __construct(ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    /**
     * @Route("/admin/submissions", name="admin_submission_index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $status = $this->getValidStatusFromRequest($request) ?: [ShopStatus::INACTIVE_NAME];

        $shops = $paginator->paginate(
            $this->shopRepository->findAllWithStatusQueryBuilder($status)->getQuery(),
            $request->query->getInt('page', 1),
            self::PER_PAGE
        );

        return $this->render('admin/submission/index.html.twig', ['shops' => $shops]);
    }

    /**
     * @param Request $request
     * @return null|string
     */
    private function getValidStatusFromRequest(Request $request)
    {
        if ($status = $request->query->get('status')) {
            $status = explode('-', $status);
            return $status;
        }

        return null;
    }
}
