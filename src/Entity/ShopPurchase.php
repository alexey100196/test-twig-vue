<?php

namespace App\Entity;

use App\Helper\Math;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShopPurchaseRepository")
 */
class ShopPurchase extends ShopConversion
{
    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     */
    private $product;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $originalPrice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     */
    private $originalCurrency;

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return string
     */
    public function getTotal()
    {
        return Math::mul($this->getPrice(), $this->getAmount());
    }

    public function getRevenue()
    {
        return Math::sub($this->getTotal(), $this->getSystemCommission());
    }

    public function getOriginalPrice()
    {
        return $this->originalPrice;
    }

    public function setOriginalPrice($originalPrice): self
    {
        $this->originalPrice = $originalPrice;

        return $this;
    }

    public function getOriginalCurrency(): ?Currency
    {
        return $this->originalCurrency;
    }

    public function setOriginalCurrency(?Currency $originalCurrency): self
    {
        $this->originalCurrency = $originalCurrency;

        return $this;
    }

    /**
     * @param Offer $offer
     */
    public function applyOffer(Offer $offer)
    {
        if ($this->getPrice() === null || $this->getAmount() === null) {
            throw new \LogicException('Cannot apply offer to conversion without price and amount');
        }

        $this->setCommission($offer->getCommission());
        $this->setCommissionType($offer->getCommissionType());
        $this->setReferrerProfit($offer->calculateCommissionValue($this->getPrice(), $this->getAmount()));
        $this->setOfferType($offer->getType());
    }
}
