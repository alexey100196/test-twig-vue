<?php


namespace App\Service\ShopEvent;


use App\Entity\DocumentReferrer\DocumentReferrer;
use App\Entity\EventEntry;
use App\Entity\EventEntryType;
use App\Manager\EventEntryManager;
use App\Service\DocumentReferrer\DocumentReferrerService;
use App\Service\ShopEvent\EventStrategies\SignUpStrategy;
use App\Service\ShopEvent\EventStrategies\ProductPurchasedStrategy;
use App\Service\ShopEvent\EventStrategies\TestConnectionStrategy;
use App\Service\ShopEvent\EventStrategies\TestStrategy;
use App\Service\ShopEvent\EventStrategies\ViewStrategy;
use App\Service\ShopEvent\Exceptions\InvalidEventTypeException;
use App\Service\ShopEvent\Interfaces\EventStrategyInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class EventService
{
    const TEST_PREFIX = 'test.';

    /**
     * @var Registry
     */
    protected $doctrine;

    /**
     * @var EventEntryManager
     */
    protected $eventEntryManager;

    /**
     * @var EventStrategyInterface[]
     */
    protected $strategies;

    /**
     * @var DocumentReferrerService
     */
    protected $documentReferrerService;

    /**
     * EventService constructor.
     * @param RegistryInterface $doctrine
     * @param EventEntryManager $eventEntryManager
     * @param DocumentReferrerService $documentReferrerService
     */
    public function __construct(
        RegistryInterface $doctrine,
        EventEntryManager $eventEntryManager,
        DocumentReferrerService $documentReferrerService
    )
    {
        $this->doctrine = $doctrine;
        $this->eventEntryManager = $eventEntryManager;
        $this->documentReferrerService = $documentReferrerService;
    }

    /**
     * @param string $name
     * @param EventStrategyInterface $strategy
     */
    public function addStrategy(string $name, EventStrategyInterface $strategy)
    {
        $this->strategies[$name] = $strategy;
    }

    /**
     * @param EventEntry $eventEntry
     * @return mixed
     * @throws InvalidEventTypeException
     */
    public function process(EventEntry $eventEntry)
    {
        $eventType = $eventEntry->getType()->getName();

        if ($eventEntry->getTest()) {
            $eventType = self::TEST_PREFIX . $eventType;
        }

        if (!isset($this->strategies[$eventType])) {
            throw new InvalidEventTypeException('Invalid event type.');
        }

        $this->extractHttpReferrer($eventEntry);

        return $this->strategyProcess($this->strategies[$eventType], $eventEntry);
    }

    /**
     * @param EventEntry $eventEntry
     * @throws \Doctrine\ORM\ORMException
     */
    private function extractHttpReferrer(EventEntry $eventEntry)
    {
        $documentReferrer = null;

        if ($httpReferer = $eventEntry->getHttpReferer()) {
            $documentReferrer = $this->documentReferrerService->create($httpReferer);
        }

        if (null === $documentReferrer) {
            /** @var EntityManagerInterface $em */
            $em = $this->doctrine->getManager();
            /** @var DocumentReferrer $documentReferrer */
            $documentReferrer = $em->getReference(DocumentReferrer::class, DocumentReferrer::OTHERS_ID);
        }

        $eventEntry->setDocumentReferrer($documentReferrer);
    }

    /**
     * @param EventStrategyInterface $strategy
     * @param EventEntry $eventEntry
     * @return mixed
     */
    private function strategyProcess(EventStrategyInterface $strategy, EventEntry $eventEntry)
    {
        $strategy->setEventEntry($eventEntry);

        return $strategy->process();
    }
}