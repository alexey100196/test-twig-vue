<?php


namespace App\Service\IncomeStatistics\ReferrerIncomeStatistics\Commands;


use App\Entity\Shop;
use App\Entity\User;
use App\Service\IncomeStatistics\CommandInterface;

class GenerateShopStats implements CommandInterface
{
    /**
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @var User
     */
    protected $referrer;

    /**
     * @var Shop
     */
    protected $shop;

    /**
     * @var string
     */
    protected $filterBy;

    /**
     * GenerateStats constructor.
     * @param Shop $shop
     * @param User $referrer
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param string $filterBy
     */
    public function __construct(Shop $shop, User $referrer, \DateTime $startDate, \DateTime $endDate, string $filterBy = '')
    {
        $this->shop = $shop;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->referrer = $referrer;
        $this->filterBy = $filterBy;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    /**
     * @return User
     */
    public function getReferrer(): User
    {
        return $this->referrer;
    }

    /**
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @return string
     */
    public function getFilterBy()
    {
        return $this->filterBy;
    }
}