<?php

namespace App\Command;

use App\Service\Exchange\ExchangeService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateExchangeRatesCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:update-exchange-rates';

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get(ExchangeService::class)->updatePrices('EUR');
    }
}