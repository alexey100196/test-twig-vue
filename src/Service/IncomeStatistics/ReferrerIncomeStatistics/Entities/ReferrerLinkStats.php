<?php


namespace App\Service\IncomeStatistics\ReferrerIncomeStatistics\Entities;


use App\Entity\CommissionType;
use App\Entity\OfferType;
use App\Entity\Product;
use App\Entity\Shop;
use App\Helper\Math;

class ReferrerLinkStats
{
    /**
     * @var float
     */
    private $amount;

    /**
     * @var float
     */
    private $price;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var float
     */
    private $referrerProfit;

    /**
     * @var Shop
     */
    private $shop;

    /**
     * @var float
     */
    private $commission;

    /**
     * @var CommissionType
     */
    private $commissionType;

    /**
     * @var float
     */
    private $total;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $offerType;

    /**
     * @param Product $product
     */
    public function setProduct(Product $product = null): void
    {
        if ($this->product = $product) {
            $this->link = $product->getFullUrl();
        }
    }

    /**
     * @param Shop $shop
     */
    public function setShop(Shop $shop): void
    {
        $this->shop = $shop;

        if (!$this->link) {
            $this->link = $shop->getWebsite();
        }
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @param float $referrerProfit
     */
    public function setReferrerProfit(float $referrerProfit): void
    {
        $this->referrerProfit = $referrerProfit;
    }

    /**
     * @param float $commission
     */
    public function setCommission(float $commission): void
    {
        $this->commission = $commission;
    }

    /**
     * @param CommissionType $commissionType
     */
    public function setCommissionType(CommissionType $commissionType): void
    {
        $this->commissionType = $commissionType;
    }

    /**
     * @param float $total
     */
    public function setTotal(float $total): void
    {
        $this->total = $total;
    }

    /**
     * @param string $offerType
     */
    public function setOfferType(string $offerType): void
    {
        $this->offerType = $offerType;
    }

    /**
     * @return array|null
     */
    private function productToArray()
    {
        if ($this->product) {
            return [
                'name' => $this->product->getName(),
                'number' => $this->product->getNumber(),
            ];
        }

        return null;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'product' => $this->productToArray(),
            'amount' => $this->amount?: 1,
            'price' => $this->price,
            'total' => $this->total, // amount*price
            'commission' => $this->commission,
            'commission_type' => $this->commissionType ? $this->commissionType->getName() : null,
            'link' => $this->link,
            'referrer_profit' => $this->referrerProfit,
            'offer_type' => $this->offerType,
        ];
    }
}