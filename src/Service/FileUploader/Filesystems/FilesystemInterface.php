<?php

namespace App\Service\FileUploader\Filesystems;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 *
 */
interface FilesystemInterface
{
    public function getPath(): string;
    public function setPath(string $path);
    public function store(UploadedFile $file, string $name): File;
}
