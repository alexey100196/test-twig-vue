<?php


namespace App\Service\SystemCommission\ShopCommission\Strategies;


use App\Entity\Shop;

interface StrategyInterface
{
    public function apply(Shop $shop, $amount, $fee);
}