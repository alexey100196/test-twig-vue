<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExchangeRateRepository")
 */
class ExchangeRate
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=7)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $baseCurrency;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $quotedCurrency;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=8)
     */
    private $rate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    public function __construct(string $baseCurrency, string $quotedCurrency, string $rate)
    {
        $this->baseCurrency = $baseCurrency;
        $this->quotedCurrency = $quotedCurrency;
        $this->rate = $rate;
        $this->updatedAt = new \DateTime();
        $this->generateId();
    }

    private function generateId()
    {
        $this->id = sprintf('%s-%s', $this->baseCurrency, $this->quotedCurrency);
    }

    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getBaseCurrency(): ?string
    {
        return $this->baseCurrency;
    }

    public function setBaseCurrency(string $baseCurrency): self
    {
        $this->baseCurrency = $baseCurrency;

        return $this;
    }

    public function getQuotedCurrency(): ?string
    {
        return $this->quotedCurrency;
    }

    public function setQuotedCurrency(string $quotedCurrency): self
    {
        $this->quotedCurrency = $quotedCurrency;

        return $this;
    }

    public function getRate()
    {
        return $this->rate;
    }

    public function setRate($rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
