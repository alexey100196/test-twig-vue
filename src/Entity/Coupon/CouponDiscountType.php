<?php

namespace App\Entity\Coupon;

use App\Entity\BaseGlossary;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Coupon\CouponDiscountTypeRepository")
 */
class CouponDiscountType extends BaseGlossary
{
    const PERCENTAGE = 'percent';
    const FIXED = 'fixed';

}
