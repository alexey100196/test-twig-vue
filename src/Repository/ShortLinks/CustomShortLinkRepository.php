<?php

namespace App\Repository\ShortLinks;

use App\Entity\CustomDomain;
use App\Entity\ShortLinks\AffiliateShortLink;
use App\Entity\ShortLinks\CustomShortLink;
use App\Entity\ShortLinks\CustomShortLinkStats;
use App\Entity\ShortLinks\CustomShortLinkTax;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method CustomShortLink|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomShortLink|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomShortLink[]    findAll()
 * @method CustomShortLink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomShortLinkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CustomShortLink::class);
    }

    /**
     * @param $user
     * @param string|null $from
     * @param string|null $to
     * @param string|null $tags
     * @param string|null $folders
     * @param string|null $page
     * @param string|null $pageSize
     * @param object|null $sort
     * @return array
     */
    public function findByFilters($user, ?string $from = null, ?string $to = null, ?string $tags = null, ?string $folders = null, ?string $page = null, ?string $pageSize = null, ?object $sort = null) {
        $query = $this->createQueryBuilder('c')->select(
            'c.id as id, c.short as short, c.original as original, c.description as description, COUNT(s.id) as clicks, c.createdAt as createdAt, c.updatedAt as updatedAt,
            cd.id as customDomainId, cd.name as customDomainName,
            GROUP_CONCAT(DISTINCT tt.name SEPARATOR :separator) as tags, GROUP_CONCAT(DISTINCT tf.name SEPARATOR :separator) as folders, 
            GROUP_CONCAT(DISTINCT us.name SEPARATOR :separator) as utm_source, GROUP_CONCAT(DISTINCT um.name SEPARATOR :separator) as utm_medium, GROUP_CONCAT(DISTINCT uca.name SEPARATOR :separator) as utm_campaign, GROUP_CONCAT(DISTINCT uc.name SEPARATOR :separator) as utm_content, GROUP_CONCAT(DISTINCT uk.name SEPARATOR :separator) as utm_keyword, 
            c.facebookPixel as facebookPixel, c.twitterPixel as twitterPixel, c.pinterestPixel as pinterestPixel, c.linkedinPixel as linkedinPixel, c.googlePixel as googlePixel, c.quoraPixel as quoraPixel,
            c.metaTitle as metaTitle, c.metaDesc as metaDesc, c.metaImage as metaImage, c.metaIframe as metaIframe,
            SUM(c.facebookPixel + c.twitterPixel + c.pinterestPixel + c.linkedinPixel + c.googlePixel + c.quoraPixel) as pixelsCount'
        )
            ->leftJoin(CustomDomain::class, 'cd', Join::WITH, 'c.customDomain = cd')
            ->leftJoin(CustomShortLinkStats::class, 's', Join::WITH, 'c = s.customShortLink')
            ->leftJoin(CustomShortLinkTax::class, 'tt', Join::WITH, 'c = tt.link AND tt.type = \'tag\'')
            ->leftJoin(CustomShortLinkTax::class, 'tf', Join::WITH, 'c = tf.link AND tf.type = \'folder\'')
            ->leftJoin(CustomShortLinkTax::class, 'us', Join::WITH, 'c = us.link AND us.type = \'utm_source\'')
            ->leftJoin(CustomShortLinkTax::class, 'um', Join::WITH, 'c = um.link AND um.type = \'utm_medium\'')
            ->leftJoin(CustomShortLinkTax::class, 'uca', Join::WITH, 'c = uca.link AND uca.type = \'utm_campaign\'')
            ->leftJoin(CustomShortLinkTax::class, 'uc', Join::WITH, 'c = uc.link AND uc.type = \'utm_content\'')
            ->leftJoin(CustomShortLinkTax::class, 'uk', Join::WITH, 'c = uk.link AND uk.type = \'utm_keyword\'')
            ->where('c.referrer = :user');

        if ($from) $query->andWhere('c.createdAt >= STR_TO_DATE(:from, \'%Y-%m-%d %H:%i:%s\')');
        if ($to) $query->andWhere('c.createdAt <= STR_TO_DATE(:to, \'%Y-%m-%d %H:%i:%s\')');
        if ($tags) $query->andWhere('tt.name IN (\''.str_replace(', ', '\', \'', strip_tags($tags)).'\')');
        if ($folders) $query->andWhere('tf.name IN (\''.str_replace(', ', '\', \'', strip_tags($folders)).'\')');

        $query->setParameter('user', $user);
        $query->setParameter('separator', ', ');

        if ($from) $query->setParameter('from', $from);
        if ($to) $query->setParameter('to', $to);

        if ($sort && isset($sort->order)) {
            $mSort = ($sort->order === 'ascending') ? 'ASC' : 'DESC';
            if (!isset($sort->prop) || empty($sort->prop)) {
                $query->orderBy('pixelsCount', $mSort);
            } else {
                $query->orderBy(
                    (($sort->prop === 'clicks') || substr($sort->prop, 0, 3) === 'utm') ? $sort->prop : 'c.'.$sort->prop,
                    $mSort
                );
            }
        } else {
            $query->orderBy('c.updatedAt', 'DESC');
        }

        $mPageSize = $pageSize ? ((int) $pageSize) : 20;
        return (array) $query->groupBy('c')
            ->setFirstResult($page ? ($page-1)*$mPageSize : 0)
            ->setMaxResults($page ? $page*$mPageSize : $mPageSize)
            ->getQuery()
            ->getResult();
    }

    public function getUserTotal($user) {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->where('c.referrer = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findDuplicate(object $link) {
        $query = $this->createQueryBuilder('c')
            ->where('c.short = :short');

        if(get_class($link) === 'App\Entity\ShortLinks\CustomShortLink' && $link->getId()) {
            $query->andWhere('c.id != :id');
        }

        $query->setParameter('short', $link->getShort());

        if(get_class($link) === 'App\Entity\ShortLinks\CustomShortLink' && $link->getId()) {
            $query->setParameter('id', $link->getId());
        }

        return $query->getQuery()->getResult();
    }
}
