<?php

namespace App\Form\Shop\Widget;

use App\Entity\Widget\WidgetButton;
use App\Entity\Widget\WidgetButtonIcon;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Widget\WidgetButton as WidgetButtonEntity;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class WidgetButtonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('blinkEffect', CheckboxType::class, [
                'required' => false,
            ])
            ->add('showNumberOfReferrals', CheckboxType::class, [
                'required' => false,
            ])
            ->add('textColor', TextType::class, [
                'constraints' => [
                    new Length(['max' => 10])
                ]
            ])
            ->add('backgroundColor', TextType::class, [
                'constraints' => [
                    new Length(['max' => 10])
                ]
            ])
            ->add('icon', EntityType::class, [
                'class' => WidgetButtonIcon::class,
                'choice_label' => 'image',
                'expanded' => true,
                'constraints' => [
                    new NotBlank(['message' => 'Please specify widget icon'])
                ]
            ])
            ->add('position', ChoiceType::class, [
                'choices' => [
                    'Top Left Corner' => WidgetButtonEntity::POSITION_TOP_LEFT,
                    'Top Right Corner' => WidgetButtonEntity::POSITION_TOP_RIGHT,
                    'Bottom Left Corner' => WidgetButtonEntity::POSITION_BOTTOM_LEFT,
                    'Bottom Right Corner' => WidgetButtonEntity::POSITION_BOTTOM_RIGHT,
                ],
                'label' => 'Choose Your Widget Position',
                'expanded' => true,
            ])
            ->add('pTop', IntegerType::class, [
                'label' => 'Icon position top (Desktop)',
                'attr' => [
                    'min' => 0
                ],
                'constraints' => [
                    new NotBlank(['allowNull' => true])
                ],
                'required' => false
            ])
            ->add('pRight', IntegerType::class, [
                'label' => 'Icon position right (Desktop)',
                'attr' => [
                    'min' => 0
                ],
                'constraints' => [
                    new NotBlank(['allowNull' => true])
                ],
                'required' => false
            ])
            ->add('pBottom', IntegerType::class, [
                'label' => 'Icon position bottom (Desktop)',
                'attr' => [
                    'min' => 0
                ],
                'constraints' => [
                    new NotBlank(['allowNull' => true])
                ],
                'required' => false
            ])
            ->add('pLeft', IntegerType::class, [
                'label' => 'Icon position left (Desktop)',
                'attr' => [
                    'min' => 0
                ],
                'constraints' => [
                    new NotBlank(['allowNull' => true])
                ],
                'required' => false
            ])
            ->add('mpTop', IntegerType::class, [
                'label' => 'Icon position top (Mobile)',
                'attr' => [
                    'min' => 0
                ],
                'constraints' => [
                    new NotBlank(['allowNull' => true])
                ],
                'required' => false
            ])
            ->add('mpRight', IntegerType::class, [
                'label' => 'Icon position right (Mobile)',
                'attr' => [
                    'min' => 0
                ],
                'constraints' => [
                    new NotBlank(['allowNull' => true])
                ],
                'required' => false
            ])
            ->add('mpBottom', IntegerType::class, [
                'label' => 'Icon position bottom (Mobile)',
                'attr' => [
                    'min' => 0
                ],
                'constraints' => [
                    new NotBlank(['allowNull' => true])
                ],
                'required' => false
            ])
            ->add('mpLeft', IntegerType::class, [
                'label' => 'Icon position left (Mobile)',
                'attr' => [
                    'min' => 0
                ],
                'constraints' => [
                    new NotBlank(['allowNull' => true])
                ],
                'required' => false
            ])
            ->add('iconSize', RangeType::class, [
                'attr' => [
                    'min' => 20,
                    'max' => 120
                ]
            ])
            ->add('backgroundSize', RangeType::class, [
                'attr' => [
                    'min' => 13,
                    'max' => 140
                ]
            ])
            ->add('hideBackgroundColor', CheckboxType::class)
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WidgetButton::class
        ]);
    }
}