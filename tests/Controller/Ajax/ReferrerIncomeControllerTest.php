<?php

namespace App\Tests\Controller\Ajax;

use App\Entity\Currency;
use App\Entity\EventEntry;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\TransactionType;
use App\Entity\User;
use App\Entity\UserShop;
use App\Entity\Widget\WidgetReferenceLink;
use App\Event\Shop\ShopStatusChangedByAdmin;
use App\Service\Balance\UserBalanceService;
use App\Service\Shop\ShopStatusService;
use App\Service\Widget\ReferenceLinkService;
use GuzzleHttp\Client;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ReferrerIncomeControllerTest extends WebTestCase
{
    private $referrerSlug = 'anyslug';
    private $filters = ['', 'affiliate', 'referral'];

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var Shop
     */
    protected $shop;

    /**
     * @var User
     */
    protected $referrer;

    /**
     * @var ReferrerLink
     */
    protected $referrerLink;

    /**
     * @var WidgetReferenceLink
     */
    protected $widgetReferenceLink;

    /**
     * @testdox Check if user hasn't referrer rule
     */
    public function testIsNotReferrer() {
        $client = $this->getStatsReferrerClient();
        $client->request('GET', '/ajax/referrer/income/companies', [
            'start_at' => date('Y-m-d', strtotime('-1 week')),
            'end_at' => date('Y-m-d', strtotime('+1 week')),
            'shops' => []
        ]);

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertEquals("[]", $client->getResponse()->getContent());
    }


    /**
     * Helper functions...
     * @param string|null $name
     * @return \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private function getStatsReferrerClient(?string $name = null) {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('nickname', $name));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);

        return $client;
    }

    /**
     * @param array $args
     */
    private function checkShop(array $args) {
        $client = $this->getStatsReferrerClient('john');

        $client->request('GET', '/ajax/referrer/income/companies', $args);

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $result = json_decode(html_entity_decode($client->getResponse()->getContent()));

        if (empty($result)) {
            $this->assertEmpty($result);
        } else {
            switch($args['filterBy']) {
                case 'referral':
                    $this->assertEquals(30, $result[0]->total_purchase_value_generated);
                    break;
                case 'affiliate':
                    $this->assertEquals(300, $result[0]->total_purchase_value_generated);
                    break;
                default:
                    $this->assertEquals(330, $result[0]->total_purchase_value_generated);
            }
        }
    }

    /**
     * @param array $args
     */
    private function checkSingleShop(array $args) {
       $client = $this->getStatsReferrerClient('john');

        $client->request('GET', '/ajax/referrer/income/company/'.$this->shop->getId(), $args);

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $result = json_decode(html_entity_decode($client->getResponse()->getContent()));

        foreach($result as $rfObj) {
            $referrerLink = $this->em->getRepository(ReferrerLink::class)->find($rfObj->referrer_link->id);
            if ($referrerLink->getSlug() === $this->referrerSlug) {
                $this->assertFalse($referrerLink->getIsGenerated());
            } else {
                $this->assertTrue($referrerLink->getIsGenerated());
            }
        }
    }

    /**
     * @param string $filter
     * @param $value
     * @return Cookie
     */
    private function logIn(string $filter, $value)
    {
        $session = self::$container->get('session');
        $em = self::$container->get('doctrine.orm.entity_manager');

        $user = $em->getRepository(User::class)->findOneBy([$filter => $value]);

        $firewallName = 'main';

        $token = new UsernamePasswordToken($user, null, $firewallName, $user->getRoles());
        $session->set('_security_'.$firewallName, serialize($token));
        $session->save();

        return new Cookie($session->getName(), $session->getId());
    }

    /**
     * @throws \Exception
     */
    private function addBudgetShop() {
        /** @var Uuid $uuid */
        $uuid = Uuid::uuid4();

        /** @var TransactionType $depositType */
        $depositType = $this->em->getRepository(TransactionType::class)->find(TransactionType::SHOP_DEPOSIT);

        $currency = $this->em->getRepository(Currency::class)->find(1);

        $balanceService = $this->client->getKernel()->getContainer()->get(UserBalanceService::class);
        $this->client->getKernel()->getContainer()->get(UserBalanceService::class)->addTransaction(
            $uuid,
            $this->shop->getUser(),
            $currency,
            (string) 200,
            $depositType
        );

        if ($this->shop && $balanceService->hasFunds($this->shop->getUser(), $currency)) {
            $dispatcher = new EventDispatcher();
            $shopStatusService = $this->client->getKernel()->getContainer()->get(ShopStatusService::class);
            $shopStatusService->makeShopActiveIfSubscriptionActive($this->shop);
            $dispatcher->dispatch(ShopStatusChangedByAdmin::NAME, new ShopStatusChangedByAdmin($this->shop));
        }
    }

    private function loadPurchases() {
        $client = static::createClient();
        $eventEntriesCount = count($this->em->getRepository(EventEntry::class)->findAll());

        if ($eventEntriesCount === 0) {
            // Not generated link purchases
            $client->xmlHttpRequest('POST', 'http://marfil-app.local/api/v1/event/purchase', [
                'referrer_link' => $this->referrerLink->getSlug(),
                'shop' => $this->shop->getPartnerNumber(),
                'test' => false,
                'payload' => [
                    'amount' => '1',
                    'coupon' => '',
                    'currency' => 'EUR',
                    'price' => '200',
                    'product' => ''
                ]
            ]);
            $client->xmlHttpRequest('POST', 'http://marfil-app.local/api/v1/event/purchase', [
                'referrer_link' => $this->referrerLink->getSlug(),
                'shop' => $this->shop->getPartnerNumber(),
                'test' => false,
                'payload' => [
                    'amount' => '1',
                    'coupon' => '',
                    'currency' => 'EUR',
                    'price' => '100',
                    'product' => ''
                ]
            ]);

            // Generated link purchases
            $client->xmlHttpRequest('POST', 'http://marfil-app.local/api/v1/event/purchase', [
                'referrer_link' => $this->widgetReferenceLink->getReferrerLink()->getSlug(),
                'shop' => $this->shop->getPartnerNumber(),
                'test' => false,
                'payload' => [
                    'amount' => '1',
                    'coupon' => '',
                    'currency' => 'EUR',
                    'price' => '20',
                    'product' => ''
                ]
            ]);
            $client->xmlHttpRequest('POST', 'http://marfil-app.local/api/v1/event/purchase', [
                'referrer_link' => $this->widgetReferenceLink->getReferrerLink()->getSlug(),
                'shop' => $this->shop->getPartnerNumber(),
                'test' => false,
                'payload' => [
                    'amount' => '1',
                    'coupon' => '',
                    'currency' => 'EUR',
                    'price' => '10',
                    'product' => ''
                ]
            ]);
        }
    }
}
