<?php


namespace App\Service\IncomeStatistics\ReferrerIncomeStatistics\Handlers;


use App\Entity\ShopInteraction;
use App\Service\IncomeStatistics\CommandInterface;
use App\Service\IncomeStatistics\ReferrerIncomeStatistics\Entities\ShopStats;
use Doctrine\Common\Collections\ArrayCollection;

class ShopStatsHandler extends AbstractHandler
{
    /**
     * @param CommandInterface $command
     * @return ArrayCollection|ShopStats[]
     */
    public function handle(CommandInterface $command)
    {
        $filteredBy = method_exists($command, 'getFilterBy') ? $command->getFilterBy() : '';

        $rows = $this->doctrine
            ->getRepository(ShopInteraction::class)
            ->getReferrerShopProductStats(
                $command->getShop(),
                $command->getReferrer(),
                $command->getStartDate(),
                $command->getEndDate(),
                $filteredBy
            );

        return new ArrayCollection(array_map([$this, 'createShopStatsSummaryFromArray'], $rows));
    }

    /**
     * @param array $item
     * @return ShopStats
     */
    private function createShopStatsSummaryFromArray(array $item): ShopStats
    {
        return new ShopStats(
            $item['referrerLink'],
            $item['lastPurchaseDate'] ? new \DateTime($item['lastPurchaseDate']) : null,
            $item['lastSignUpDate'] ? new \DateTime($item['lastSignUpDate']) : null,
            $item['lastViewDate'] ? new \DateTime($item['lastViewDate']) : null,
            (int)$item['purchaseCount'],
            (int)$item['signUpCount'],
            (int)$item['viewsCount'],
            (float)$item['purchaseReferrerProfit'],
            (float)$item['signUpReferrerProfit'],
            (float)$item['purchaseTotal'],
            (float)$item['signUpCommission']
        );
    }
}