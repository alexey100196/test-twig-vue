<?php

namespace App\Repository\ShortLinks;

use App\Entity\ShortLinks\CustomShortLink;
use App\Entity\ShortLinks\CustomShortLinkStats;
use App\Entity\ShortLinks\CustomShortLinkTax;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method CustomShortLinkTax|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomShortLinkTax|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomShortLinkTax[]    findAll()
 * @method CustomShortLinkTax[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomShortLinkTaxRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CustomShortLinkTax::class);
    }

    /**
     * @param $user
     * @param string $type
     * @return array
     */
    public function getUniqueTaxes($user, $type = 'tag') {
        $query = $this->createQueryBuilder('t')->select('DISTINCT t.name as name')
            ->leftJoin(CustomShortLink::class, 'c', Join::WITH, 'c = t.link')
            ->where('c.referrer = :user')
            ->andWhere('t.type = :type');

        $query->setParameter('user', $user);
        $query->setParameter('type', $type);

        return (array) $query
            ->orderBy('name', 'ASC')
            ->groupBy('name')
            ->getQuery()
            ->getResult();
    }
}
