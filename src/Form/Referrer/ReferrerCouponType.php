<?php

namespace App\Form\Referrer;

use App\Entity\CommissionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Range;

class ReferrerCouponType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('value', NumberType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'Enter coupon value']),
                    new GreaterThan([
                        'value'=> 0,
                        'message' => 'Coupon value should be greater than {{ compared_value }}'
                    ]),
                    new LessThanOrEqual([
                        'value'=> 100,
                        'message' => 'Coupon value should be less or equal to {{ compared_value }}'
                    ]),
                ],
                'invalid_message' => 'Coupon value is not valid',
            ])
            ->add('valueType', EntityType::class, [
                'class' => CommissionType::class,
                'constraints' => [
                    new NotBlank(['message' => 'Enter coupon value type: €/%']),
                ],
            ])
            ->add('code', null, [
                'constraints' => [
                    new NotBlank(['message' => 'Enter coupon code']),
                    new Length([
                        'min' => 3,
                        'max' => 15,
                        'minMessage' => 'Coupon code is too short. It should have {{ limit }} characters or more',
                        'maxMessage' => 'Coupon code is too long. It should have {{ limit }} characters or less',
                    ])
                ],
            ])
            ->add('linksEnabled', CheckboxType::class, [
                'label' => 'Widget Links'
            ])
            ->add('setOfCardsEnabled', CheckboxType::class, [
                'label' => 'Card&SetOfCards'
            ])
            ->add('myStoreEnabled', CheckboxType::class, [
                'label' => 'MyStore'
            ])
            ->add('expiresAt', DateTimeType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'constraints' => [
                    new NotBlank(['message' => 'Please specify expiration date']),
                    new Range([
                        'min' => 'now',
                        'max' => '+10 years',
                        'minMessage' => 'Coupon date should be {{ limit }} or more',
                        'maxMessage' => 'Coupon date should be {{ limit }} or less',
                    ])
                ]
            ]);
    }
}
