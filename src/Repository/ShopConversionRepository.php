<?php

namespace App\Repository;

use App\Entity\CommissionType;
use App\Entity\Currency;
use App\Entity\OfferType;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\ShopConversion;
use App\Entity\ShopConversionStatus;
use App\Entity\ShopPurchase;
use App\Entity\User;
use App\ValueObject\Sort;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopConversion|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopConversion|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopConversion[]    findAll()
 * @method ShopConversion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopConversionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopConversion::class);
    }

    /**
     * @param ReferrerLink $referrerLink
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param string $offerType
     * @param string $commissionType
     * @param float $commission
     * @param string $sourceLink
     * @param float $price|null
     * @return array
     */
    public function getConversionsRatesForReferrerLink(
        ReferrerLink $referrerLink,
        \DateTime $startDate,
        \DateTime $endDate,
        string $offerType,
        string $commissionType,
        float $commission,
        string $sourceLink,
        float $price = null
    )
    {
        $query = $this->createQueryBuilder('sc')
            ->select(' 
                sc.id as idd,
                ot.name as off,
                sp.price as purchasesPrice,
                c.name as originalCurrency,
                (sp.amount * sp.originalPrice) as originalTotal, 
                sp.originalPrice as originalPrice,
                (sp.price / sp.originalPrice) as exchangeRate,
                sc.createdAt
            ')
            ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sc = sp')
            ->leftJoin(Currency::class, 'c', Join::WITH, 'c = sp.originalCurrency')
            ->leftJoin(OfferType::class, 'ot', Join::WITH, 'ot = sc.offerType')
            ->leftJoin(CommissionType::class, 'ct', Join::WITH, 'ct = sc.commissionType')
            ->andWhere('sc.createdAt >= :startDate')
            ->andWhere('sc.createdAt <= :endDate')
            ->andWhere('sc.referrerLink = :referrerLink')
            ->andWhere('sc.status <> :status')
            ->andWhere('ot.name = :offerType')
            ->andWhere('sc.commission = :commission')
            ->andWhere('sc.sourceLink = :sourceLink')
            ->andWhere('ct.name = :commissionType');


        $query->setParameters(compact(
            'referrerLink',
            'startDate',
            'endDate',
            'offerType',
            'commissionType',
            'commission',
            'sourceLink'
        ));

        if (null !== $price) {
            $query->andWhere('sp.price = :price')->setParameter('price', $price);
        }

        return $query->setParameter('status', ShopConversionStatus::REJECTED)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param ReferrerLink $referrerLink
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return array
     */
    public function getReferrerLinkStats(
        ReferrerLink $referrerLink,
        \DateTime $startDate,
        \DateTime $endDate
    )
    {
        $query = $this->createQueryBuilder('sc')
            ->select('
                ot.name as offerType,
                ct.name as commissionType,
                sc.commission as commission,
                COUNT(sc) as transactionsCount,
                SUM(sp.amount) as purchasesAmount,
                sp.price as purchasesPrice,
                SUM(sc.referrerProfit) as referrerProfit,
                sc.sourceLink as sourceLink,
                SUM(sp.originalPrice * sp.amount) as originalTotal,
                SUM(sp.price * sp.amount) as purchasesTotal
            ')
            ->leftJoin(OfferType::class, 'ot', Join::WITH, 'sc.offerType = ot')
            ->leftJoin(CommissionType::class, 'ct', Join::WITH, 'sc.commissionType = ct')
            ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sc = sp')
            ->andWhere('sc.createdAt >= :startDate')
            ->andWhere('sc.createdAt <= :endDate')
            ->andWhere('sc.referrerLink = :referrerLink')
            ->andWhere('sc.status <> :status');

        return $query->setParameters(compact('referrerLink', 'startDate', 'endDate'))
            ->setParameter('status', ShopConversionStatus::REJECTED)
            ->groupBy('offerType, sc.commission, commissionType, sp.price, sourceLink')
            ->getQuery()
            ->getResult();
    }



    /**
     * @param Shop $shop
     * @param \DateTime|null $start
     * @param \DateTime|null $end
     * @param Sort|null $sort
     * @return mixed
     */
    public function getAllByShopQueryBuilder(
        Shop $shop,
        \DateTime $start = null,
        \DateTime $end = null,
        Sort $sort = null
    )
    {
        $qb = $this->createQueryBuilder('sc')
            ->andWhere('sc.shop = :shop')
            ->setParameter('shop', $shop);

        if (null !== $start) {
            $qb->andWhere('sc.createdAt >= :start')->setParameter('start', $start);
        }

        if (null !== $end) {
            $qb->andWhere('sc.createdAt <= :end')->setParameter('end', $end);
        }

        if ($sort !== null) {
            if ($sort->getField() === 'purchaseValue') {
                $qb = $this->applyPurchaseValueOrder($qb, $sort);
            } elseif ($sort->getField() === 'revenue') {
                $qb = $this->applyRevenueOrder($qb, $sort);
            } else {
                $qb->orderBy('sc.' . $sort->getField(), $sort->getDirection());
            }
        }

        return $qb;
    }

    /**
     * @param Shop $shop
     * @param \DateTime $start
     * @param \DateTime $end
     * @return mixed
     */
    public function getShopReferrersPayouts(Shop $shop, \DateTime $start, \DateTime $end)
    {
        return $this->createQueryBuilder('sc')
            ->select('
                u.name as referrerName,
                u.surname as referrerSurname,
                u.email as referrerEmail,
                SUM(sc.referrerProfit) as amount,
                c.name as currency
            ')
            ->leftJoin(User::class, 'u', Join::WITH, 'sc.referrer = u')
            ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sc = sp')
            ->leftJoin(Currency::class, 'c', Join::WITH, 'c = sp.originalCurrency')
            ->andWhere('sc.status = :status')
            ->andWhere('sc.createdAt >= :start')
            ->andWhere('sc.createdAt <= :end')
            ->andWhere('sc.shop = :shop')
            ->setParameters([
                'status' => ShopConversionStatus::ACCEPTED,
                'start' => $start,
                'shop' => $shop,
                'end' => $end,
            ])
            ->groupBy('referrerEmail')
            ->addGroupBy('c.name')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @return mixed
     */
    public function getReferrersPayouts(\DateTime $start, \DateTime $end)
    {
        return $this->createQueryBuilder('sc')
            ->select('
                u.id as referrerId,
                s.name as shopName,
                u.name as referrerName,
                u.surname as referrerSurname,
                u.email as referrerEmail,
                SUM(sc.referrerProfit) as amount
            ')
            ->leftJoin(User::class, 'u', Join::WITH, 'sc.referrer = u')
            ->leftJoin(Shop::class, 's', Join::WITH, 'sc.shop = s')
            ->andWhere('sc.status = :status')
            ->andWhere('sc.createdAt >= :start')
            ->andWhere('sc.createdAt <= :end')
            ->setParameters([
                'status' => ShopConversionStatus::ACCEPTED,
                'start' => $start,
                'end' => $end,
            ])
            ->groupBy('referrerEmail', 'shopName')
            ->getQuery()
            ->getResult();
    }

    public function getShopsTotalReferrerPayouts(\DateTime $start, \DateTime $end)
    {
        return $this->createQueryBuilder('sc')
            ->select('
                s as shop,
                SUM(sc.referrerProfit) as amount
            ')
            ->leftJoin(Shop::class, 's', Join::WITH, 'sc.shop = s')
            ->andWhere('sc.status = :status')
            ->andWhere('sc.createdAt >= :start')
            ->andWhere('sc.createdAt <= :end')
            ->setParameters([
                'status' => ShopConversionStatus::ACCEPTED,
                'start' => $start,
                'end' => $end,
            ])
            ->groupBy('shop')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @param ShopConversionStatus|null $status
     * @param Sort|null $sort
     * @return mixed
     */
    public function getByDatePeriod(
        \DateTime $start,
        \DateTime $end,
        ShopConversionStatus $status = null,
        Sort $sort = null
    )
    {
        $qb = $this->createQueryBuilder('sc')
            ->andWhere('sc.createdAt >= :start')
            ->andWhere('sc.createdAt <= :end')
            ->setParameter('start', $start)
            ->setParameter('end', $end);

        if ($status !== null) {
            $qb->andWhere('sc.status = :status')->setParameter('status', $status);
        }

        if ($sort !== null) {
            if ($sort->getField() === 'purchaseValue') {
                $qb = $this->applyPurchaseValueOrder($qb, $sort);
            } else {
                $qb->orderBy('sc.' . $sort->getField(), $sort->getDirection());
            }
        }

        return $qb
            ->getQuery()
            ->getResult();
    }

    private function applyPurchaseValueOrder(QueryBuilder $qb, Sort $sort): QueryBuilder
    {
        return $qb
            ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sc.id = sp.id')
            ->orderBy('sp.amount * sp.price', $sort->getDirection());
    }

    private function applyRevenueOrder(QueryBuilder $qb, Sort $sort): QueryBuilder
    {
        return $qb
            ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sc.id = sp.id')
            ->orderBy('sp.amount * sp.price - sc.systemCommission', $sort->getDirection());
    }
}
