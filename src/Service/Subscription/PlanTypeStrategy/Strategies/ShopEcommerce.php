<?php


namespace App\Service\Subscription\PlanTypeStrategy\Strategies;


use App\Entity\Glossary\ShopType;
use App\Entity\Shop;
use App\Entity\Subscription\PlanSubscription;
use Doctrine\ORM\EntityManagerInterface;

class ShopEcommerce implements StrategyInterface
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function process(PlanSubscription $planSubscription)
    {
        /** @var Shop $shop */
        $shop = $planSubscription->getUser()->getShop();

        if ($shop->getType() === null) {
            /** @var ShopType $shopType */
            $shopType = $this->em->getReference(ShopType::class, ShopType::ECOMMERCE);
            $shop->setType($shopType);
        }
    }
}