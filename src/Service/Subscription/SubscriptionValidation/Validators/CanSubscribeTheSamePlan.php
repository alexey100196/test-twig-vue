<?php


namespace App\Service\Subscription\SubscriptionValidation\Validators;


use App\Entity\Subscription\Plan;
use App\Entity\User;

class CanSubscribeTheSamePlan implements ValidatorInterface
{
    /**
     * {@inheritdoc}
     */
    public function validate(User $user, Plan $plan): void
    {
        if ($user->subscribesPlan($plan)) {
            throw new SubscriptionValidationException('You are already subscriber of chosen plan.');
        }
    }
}