<?php

namespace App\Repository;

use App\Entity\Coupon\Coupon;
use App\Entity\Coupon\CouponPackage;
use App\Entity\DocumentReferrer\DocumentReferrer;
use App\Entity\EventEntry;
use App\Entity\InviteFriendByEmail;
use App\Entity\Product;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\ShopConversion;
use App\Entity\ShopConversionStatus;
use App\Entity\ShopInteraction;
use App\Entity\ShopPurchase;
use App\Entity\ShopSignUp;
use App\Entity\ShopView;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopInteraction|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopInteraction|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopInteraction[]    findAll()
 * @method ShopInteraction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopInteractionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopInteraction::class);
    }

    /**
     * @param Shop $shop
     * @param User $referrer
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param string $filterBy
     * @return mixed
     */
    public function getReferrerShopProductStats(Shop $shop, User $referrer, \DateTime $startDate, \DateTime $endDate, string $filterBy)
    {
        $query = $this->createQueryBuilder('si')
            ->select('
                rl as referrerLink,
                MAX(sp.createdAt) as lastPurchaseDate,
                MAX(ssu.createdAt) as lastSignUpDate,
                MAX(sv.createdAt) as lastViewDate,
                COUNT(sp.id) as purchaseCount,
                COUNT(ssu.id) as signUpCount,
                COUNT(sv.id) as viewsCount,
                SUM(sp.referrerProfit) as purchaseReferrerProfit,
                SUM(ssu.referrerProfit) as signUpReferrerProfit,
                SUM(sp.amount * sp.price) as purchaseTotal,
                SUM(ssu.commission) as signUpCommission
             ')
            ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sp = si AND sp.status != :status')
            ->leftJoin(ShopSignUp::class, 'ssu', Join::WITH, 'ssu = si AND ssu.status != :status')
            ->leftJoin(ShopView::class, 'sv', Join::WITH, 'sv = si')
            ->innerJoin(ReferrerLink::class, 'rl', Join::WITH, 'rl = si.referrerLink')
            ->andWhere('si.shop = :shop')
            ->andWhere('si.createdAt >= :startDate')
            ->andWhere('si.createdAt <= :endDate')
            ->andWhere('rl.referrer = :referrer');

        if ($filterBy) {
            $query->andWhere('rl.isGenerated = :isGenerated');
        }

        $query->setParameters(compact('referrer', 'shop', 'startDate', 'endDate'))
            ->setParameter('status', ShopConversionStatus::REJECTED)
            ->groupBy('si.referrerLink');

        if ($filterBy) {
            $query->setParameter('isGenerated', (int) ($filterBy === 'referral'));
        }

        return $query->getQuery()
            ->getResult();
    }

    /**
     * @param ArrayCollection $shops
     * @param User $referrer
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param string $filterBy
     * @return mixed
     */
    public function getReferrerShopsStats(
        ArrayCollection $shops,
        User $referrer,
        \DateTime $startDate,
        \DateTime $endDate,
        string $filterBy
    )
    {
        $emailQuery = $this->createQueryBuilder('sie')
            ->select('COUNT(ie.id)')
            ->innerJoin(InviteFriendByEmail::class, 'ie', Join::WITH, 'ie = sie')
            ->where('sie.shop = s')
            ->andWhere('sie.referrer = :referrer')
            ->andWhere('sie.createdAt >= :startDate')
            ->andWhere('sie.createdAt <= :endDate');

        $query = $this->createQueryBuilder('si')
            ->select('
                s as shop,
                SUM(sp.referrerProfit) as purchaseReferrerProfit,
                SUM(ssu.referrerProfit) as signUpReferrerProfit,
                COUNT(sp.id) as purchaseCount,
                COUNT(ssu.id) as signUpCount,
                SUM(sp.amount * sp.price) as purchaseTotal,
                COUNT(sv.id) as viewsCount,
                SUM(ssu.commission) as signUpCommission,
                COUNT(c.id) as usedCouponsCount,
                ('.$emailQuery->getDQL().') as inviteFriendEmails
             ')
            ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sp = si AND sp.referrer = :referrer AND sp.status != :status')
            ->leftJoin(ShopSignUp::class, 'ssu', Join::WITH, 'ssu = si AND ssu.referrer = :referrer AND ssu.status != :status')
            ->leftJoin(ShopView::class, 'sv', Join::WITH, 'sv = si AND sv.referrer = :referrer')
            ->leftJoin(Shop::class, 's', Join::WITH, 'si.shop = s')
            ->leftJoin(CouponPackage::class, 'cp', Join::WITH, 'si.shop = cp.shop')
            ->leftJoin(Coupon::class, 'c', Join::WITH, 'c.package = cp')
            ->leftJoin(ReferrerLink::class, 'rl', Join::WITH,
                'si.referrerLink = rl'. (($filterBy === 'referral') ? ' AND rl.isGenerated = 1' : '')
            )
            ->andWhere('si.shop IN (:shops)')
            ->andWhere('si.createdAt >= :startDate')
            ->andWhere('si.createdAt <= :endDate')
            ->andWhere('c.used >= :usedCount');

        $query->setParameters(compact('startDate', 'endDate', 'referrer', 'shops'))
            ->setParameter('status', ShopConversionStatus::REJECTED)
            ->setParameter('usedCount', 1);

        $query->groupBy('s');

        return $query->getQuery()
            ->getResult();
    }

    /**
     * Get statistics about referrers for given shop.
     * It means that data will be grouped by every referrer.
     *
     * @param Shop $shop
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param array $productsIds
     * @param bool $includeMainOffer
     * @param string $filterBy
     * @param array|null $campaigns
     * @return array
     */
    public function getShopReferrerStats(
        Shop $shop,
        \DateTime $startDate,
        \DateTime $endDate,
        array $productsIds = [],
        bool $includeMainOffer = false,
        string $filterBy = '',
        ?array $campaigns = null
    )
    {
        $emailQuery = $this->createQueryBuilder('sie')
            ->select('COUNT(ie.id)')
            ->innerJoin(InviteFriendByEmail::class, 'ie', Join::WITH, 'ie = sie')
            ->leftJoin(ReferrerLink::class, 'rls', Join::WITH,
                'sie.referrerLink = rls'. (($filterBy === 'referral') ? ' AND rls.isGenerated = 1' : '')
            )
            ->where('sie.shop = si.shop')
            ->andWhere('sie.referrer = r')
            ->andWhere('sie.createdAt >= :startDate')
            ->andWhere('sie.createdAt <= :endDate');

        if ($campaigns !== null && ($filterBy === 'referral')) {
            $emailQuery->andWhere('rl.campaign in (:campaigns)');
        }

        $query = $this->createQueryBuilder('si')
            ->select('
                r as referrer,
                MAX(sp.createdAt) as lastPurchaseDate,
                MAX(ssu.createdAt) as lastSignUpDate,
                COUNT(sp.id) as purchaseCount,
                COUNT(ssu.id) as signUpCount,
                COUNT(sv.id) as viewsCount,
                COUNT(c.id) as usedCouponsCount,
                SUM(ssu.referrerProfit) as signUpReferrerProfit,
                SUM(sp.referrerProfit) as purchaseReferrerProfit,
                SUM(sc.systemCommission) as systemCommission,
                SUM(sp.amount * sp.price) as purchaseTotal,
                ('.$emailQuery->getDQL().') as inviteFriendEmails
            ')
            ->leftJoin(ShopView::class, 'sv', Join::WITH, 'sv = si')
            ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sp = si')
            ->leftJoin(ShopSignUp::class, 'ssu', Join::WITH, 'ssu = si')
            ->leftJoin(User::class, 'r', Join::WITH, 'r = si.referrer')
            ->leftJoin(ShopConversion::class, 'sc', Join::WITH, 'sc = si')
            ->leftJoin(CouponPackage::class, 'cp', Join::WITH, 'si.shop = cp.shop')
            ->leftJoin(Coupon::class, 'c', Join::WITH, 'c.package = cp')
            ->leftJoin(ReferrerLink::class, 'rl', Join::WITH,
                'si.referrerLink = rl'. (($filterBy === 'referral') ? ' AND rl.isGenerated = 1' : '')
            )
            ->andWhere('si.createdAt >= :startDate')
            ->andWhere('si.createdAt <= :endDate')
            ->andWhere('si.shop = :shop')
            ->andWhere('sc.status != :status OR sc.status IS NULL')
            ->andWhere('c.used >= :usedCount');

        $query->setParameters(compact('startDate', 'endDate', 'shop'))
            ->setParameter('status', ShopConversionStatus::REJECTED)
            ->setParameter('usedCount', 1);

        if ($campaigns !== null && ($filterBy === 'referral')) {
            $query->andWhere('rl.campaign in (:campaigns)')
                ->setParameter('campaigns', $campaigns);
        }

        $query = $this->resolveOfferFilteringQuery($query, $includeMainOffer, $productsIds);

        return $query
            ->groupBy('referrer, rl.campaign')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Shop $shop
     * @param User $referrer
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param array $productsIds
     * @param bool $includeMainOffer
     * @param string $filterBy
     * @return mixed
     */
    public function getShopReferrerReflinksStats(
        Shop $shop,
        User $referrer,
        \DateTime $startDate,
        \DateTime $endDate,
        array $productsIds = [],
        bool $includeMainOffer = false,
        string $filterBy = ''
    )
    {
        $query = $this->createQueryBuilder('si')
            ->select('
                rl as reflink,
                COUNT(sv.id) as viewsCount,
                COUNT(sp.id) as purchaseCount,
                COUNT(ssu.id) as signUpCount,
                SUM(ssu.referrerProfit) as signUpReferrerProfit,
                SUM(sp.referrerProfit) as purchaseReferrerProfit,
                SUM(sp.amount * sp.price) as purchaseTotal
            ')
            ->leftJoin(ShopView::class, 'sv', Join::WITH, 'sv = si')
            ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sp = si AND sp.status != :status')
            ->leftJoin(ShopSignUp::class, 'ssu', Join::WITH, 'ssu = si AND ssu.status != :status')
            ->leftJoin(User::class, 'r', Join::WITH, 'r = si.referrer')
            ->innerJoin(ReferrerLink::class, 'rl', Join::WITH, 'si.referrerLink = rl')
            ->andWhere('si.createdAt >= :startDate')
            ->andWhere('si.createdAt <= :endDate')
            ->andWhere('si.shop = :shop')
            ->andWhere('si.referrer = :referrer');

        if ($filterBy) {
            $query->andWhere('rl.isGenerated = :isGenerated');
        }

        $query->setParameters(compact('startDate', 'endDate', 'shop', 'referrer'))
            ->setParameter('status', ShopConversionStatus::REJECTED);

        if ($filterBy) {
            $query->setParameter('isGenerated', (int) ($filterBy === 'referral'));
        }

        $query = $this->resolveOfferFilteringQuery($query, $includeMainOffer, $productsIds);

        return $query
            ->groupBy('reflink')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Shop $shop
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getShopSummaryStats(Shop $shop)
    {
        return $this->createQueryBuilder('si')
            ->select('
                COUNT(sv.id) as viewsCount,
                COUNT(sp.id) as purchaseCount,
                COUNT(ssu.id) as signUpCount,
                SUM(sp.amount * sp.price) as purchaseTotal,
                SUM(sc.systemCommission) as systemCommission,
                SUM(sc.referrerProfit) as referrerProfit,
                SUM(sp.referrerProfit) as purchaseReferrerProfit,
                SUM(ssu.referrerProfit) as signUpReferrerProfit
            ')
            ->leftJoin(ShopView::class, 'sv', Join::WITH, 'sv = si')
            ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sp = si')
            ->leftJoin(ShopSignUp::class, 'ssu', Join::WITH, 'ssu = si')
            ->leftJoin(ShopConversion::class, 'sc', Join::WITH, 'sc = si')
            ->andWhere('si.shop = :shop')
            ->andWhere('sc.status != :status OR sc.status IS NULL')
            ->setParameter('shop', $shop)
            ->setParameter('status', ShopConversionStatus::REJECTED)
            ->getQuery()
            ->getSingleResult();
    }

    public function getShopDocumentReferrerStats(
        Shop $shop,
        \DateTime $startDate,
        \DateTime $endDate,
        array $productsIds = [],
        bool $includeMainOffer = false
    )
    {
        $query = $this->createQueryBuilder('si')
            ->select('
                dr as documentReferrer,
                MAX(sp.createdAt) as lastPurchaseDate,
                MAX(ssu.createdAt) as lastSignUpDate,
                COUNT(sp.id) as purchaseCount,
                COUNT(ssu.id) as signUpCount,
                COUNT(sv.id) as viewsCount,
                SUM(ssu.referrerProfit) as signUpReferrerProfit,
                SUM(sp.referrerProfit) as purchaseReferrerProfit,
                SUM(sc.systemCommission) as systemCommission,
                SUM(sp.amount * sp.price) as purchaseTotal
            ')
            ->leftJoin(ShopView::class, 'sv', Join::WITH, 'sv = si')
            ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sp = si')
            ->leftJoin(ShopSignUp::class, 'ssu', Join::WITH, 'ssu = si')
            ->leftJoin(User::class, 'r', Join::WITH, 'r = si.referrer')
            ->leftJoin(ShopConversion::class, 'sc', Join::WITH, 'sc = si')
            ->innerJoin(EventEntry::class, 'e', Join::WITH, 'si.eventEntry = e')
            ->innerJoin(DocumentReferrer::class, 'dr', Join::WITH, 'e.documentReferrer = dr')
            ->andWhere('si.createdAt >= :startDate')
            ->andWhere('si.createdAt <= :endDate')
            ->andWhere('si.shop = :shop')
            ->andWhere('sc.status != :status OR sc.status IS NULL')
            ->setParameters(compact('startDate', 'endDate', 'shop'))
            ->setParameter('status', ShopConversionStatus::REJECTED);

        $query = $this->resolveOfferFilteringQuery($query, $includeMainOffer, $productsIds);

        return $query
            ->groupBy('dr.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * This is helper method to help with offer filtering.
     * Offer can be filtered by main offer (if want to include) and detailed offer (products).
     *
     * @param QueryBuilder $query
     * @param bool $includeMainOffer
     * @param array $products
     * @return QueryBuilder
     */
    private function resolveOfferFilteringQuery(
        QueryBuilder $query,
        bool $includeMainOffer,
        array $products
    ): QueryBuilder
    {
        if ($includeMainOffer) {
            $query->andWhere('sp.product is NULL OR sp.product in (:productsIds)');
        } else {
            $query->andWhere('sp.product in (:productsIds)');
        }

        $query->setParameter('productsIds', $products);

        return $query;
    }

    /**
     * @param Shop $shop
     * @param DocumentReferrer $documentReferrer
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param array $productsIds
     * @param bool $includeMainOffer
     * @return mixed
     */
    public function getShopDocumentReferrerReflinksStats(
        Shop $shop,
        DocumentReferrer $documentReferrer,
        \DateTime $startDate,
        \DateTime $endDate,
        array $productsIds = [],
        bool $includeMainOffer = false
    )
    {
        $query = $this->createQueryBuilder('si')
            ->select('
                rl as reflink,
                COUNT(sv.id) as viewsCount,
                COUNT(sp.id) as purchaseCount,
                COUNT(ssu.id) as signUpCount,
                SUM(ssu.referrerProfit) as signUpReferrerProfit,
                SUM(sp.referrerProfit) as purchaseReferrerProfit,
                SUM(sp.amount * sp.price) as purchaseTotal
            ')
            ->leftJoin(ShopView::class, 'sv', Join::WITH, 'sv = si')
            ->leftJoin(ShopPurchase::class, 'sp', Join::WITH, 'sp = si AND sp.status != :status')
            ->leftJoin(ShopSignUp::class, 'ssu', Join::WITH, 'ssu = si AND ssu.status != :status')
            ->leftJoin(User::class, 'r', Join::WITH, 'r = si.referrer')
            ->leftJoin(ReferrerLink::class, 'rl', Join::WITH, 'si.referrerLink = rl')
            ->innerJoin(EventEntry::class, 'e', Join::WITH, 'si.eventEntry = e')
            ->innerJoin(DocumentReferrer::class, 'dr', Join::WITH, 'e.documentReferrer = dr')
            ->setParameters(compact('startDate', 'endDate', 'shop', 'documentReferrer'))
            ->andWhere('si.createdAt >= :startDate')
            ->andWhere('si.createdAt <= :endDate')
            ->andWhere('si.shop = :shop')
            ->andWhere('e.documentReferrer = :documentReferrer')
            ->setParameter('status', ShopConversionStatus::REJECTED);

        $query = $this->resolveOfferFilteringQuery($query, $includeMainOffer, $productsIds);

        return $query
            ->groupBy('reflink')
            ->getQuery()
            ->getResult();
    }

}
