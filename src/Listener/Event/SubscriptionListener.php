<?php


namespace App\Listener\Event;


use App\Adapter\MailSender;
use App\Entity\Subscription\PlanSubscription;
use App\Entity\User;
use App\Event\Subscription\SubscriptionExpired;
use App\Event\User\UserRegistered;

class SubscriptionListener
{
    /**
     * @var MailSender
     */
    private $mailSender;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var string
     */
    private $adminEmail;

    /**
     * UserRegistrationListener constructor.
     * @param \Twig_Environment $twig
     * @param MailSender $mailSender
     */
    public function __construct(\Twig_Environment $twig, MailSender $mailSender)
    {
        $this->twig = $twig;
        $this->mailSender = $mailSender;
    }

    /**
     * @param string $adminEmail
     */
    public function setAdminEmail(string $adminEmail)
    {
        $this->adminEmail = $adminEmail;
    }

    public function onSubscriptionExpired(SubscriptionExpired $event)
    {
        $this->sendEmailToAdmin($event->getSubscription());
        $this->sendEmailToSubscriber($event->getSubscription());
    }

    private function sendEmailToAdmin(PlanSubscription $subscription)
    {
        $this->mailSender->send(
            'hello@superinterface.com',
            $this->adminEmail,
            'Welcome to SuperInterface!',
            $this->twig->render('email/admin/subscription-expired.html.twig', ['subscription' => $subscription])
        );
    }

    private function sendEmailToSubscriber(PlanSubscription $subscription)
    {
        $this->mailSender->send(
            'hello@superinterface.com',
            $subscription->getUser()->getEmail(),
            'Welcome to SuperInterface!',
            $this->twig->render('email/shop/subscription-expired.html.twig', ['subscription' => $subscription])
        );
    }
}