<?php

namespace App\Repository;

use App\Entity\ConfirmationEmailToken;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\Expr\Join;


/**
 * @method ConfirmationEmailToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConfirmationEmailToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConfirmationEmailToken[]    findAll()
 * @method ConfirmationEmailToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConfirmationEmailTokenRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ConfirmationEmailToken::class);
    }

    /**
     * @param string $token
     * @param int $userId
     * @return mixed|null
     */
    public function findNotExpiredTokenByTokenAndUserId(string $token, int $userId)
    {
        try {
            return $this->createQueryBuilder('c')
                ->join(User::class, 'u', Join::WITH, 'c.user = u')
                ->andWhere('c.token = :token')
                ->andWhere('c.expiresAt > :now')
                ->andWhere('u.id = :userId')
                ->setParameters([
                    'token' => $token,
                    'now' => (new \DateTime())->format('Y-m-d H:i:s'),
                    'userId' => $userId,
                ])
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException | \Exception $exception) {
            return null;
        }
    }
}
