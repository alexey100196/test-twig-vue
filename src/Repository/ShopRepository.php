<?php

namespace App\Repository;

use App\Entity\Glossary\ShopType;
use App\Entity\Product;
use App\Entity\ProductPurchase;
use App\Entity\Shop;
use App\Entity\ShopConversion;
use App\Entity\ShopConversionStatus;
use App\Entity\ShopPurchase;
use App\Entity\ShopSignUp;
use App\Entity\ShopStatus;
use App\Entity\ShopView;
use App\Entity\User;
use App\Entity\UserTransaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Shop|null find($id, $lockMode = null, $lockVersion = null)
 * @method Shop|null findOneBy(array $criteria, array $orderBy = null)
 * @method Shop[]    findAll()
 * @method Shop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Shop::class);
    }

    /**
     * @param ArrayCollection $shops
     * @param User $referrer
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return mixed
     */
    public function getShopsPurchaseStatsForReferrer(
        ArrayCollection $shops,
        User $referrer,
        \DateTime $startDate,
        \DateTime $endDate
    )
    {
        return $this->createQueryBuilder('s')
            ->select('s as shop, sum(sp.price * sp.amount) as totalValue, sum(sp.referrerProfit) as referrerProfit')
            ->join(ShopPurchase::class, 'sp', Join::WITH, 'sp.shop = s')
            ->andWhere('sp.createdAt >= :startDate')
            ->andWhere('sp.createdAt <= :endDate')
            ->andWhere('sp.referrer = :referrer')
            ->andWhere('s IN (:shops)')
            ->setParameters(compact('startDate', 'endDate', 'referrer', 'shops'))
            ->groupBy('s.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param ArrayCollection $shops
     * @param User $referrer
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return mixed
     */
    public function getShopsSignUpStatsForReferrer(
        ArrayCollection $shops,
        User $referrer,
        \DateTime $startDate,
        \DateTime $endDate
    )
    {
        return $this->createQueryBuilder('s')
            ->select('s as shop, sum(ssu.commission) as totalCommission, sum(ssu.referrerProfit) as referrerProfit')
            ->join(ShopSignUp::class, 'ssu', Join::WITH, 'ssu.shop = s')
            ->andWhere('ssu.createdAt >= :startDate')
            ->andWhere('ssu.createdAt <= :endDate')
            ->andWhere('ssu.referrer = :referrer')
            ->andWhere('s IN (:shops)')
            ->setParameters(compact('startDate', 'endDate', 'referrer', 'shops'))
            ->groupBy('s.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param ArrayCollection $shops
     * @param User $referrer
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return mixed
     */
    public function countShopsViewsForReferrer(
        ArrayCollection $shops,
        User $referrer,
        \DateTime $startDate,
        \DateTime $endDate
    )
    {
        return $this->createQueryBuilder('s')
            ->select('s as shop, count(sv.id) as viewsCount')
            ->join(ShopView::class, 'sv', Join::WITH, 'sv.shop = s')
            ->andWhere('sv.createdAt >= :startDate')
            ->andWhere('sv.createdAt <= :endDate')
            ->andWhere('sv.referrer = :referrer')
            ->andWhere('s IN (:shops)')
            ->setParameters(compact('startDate', 'endDate', 'referrer', 'shops'))
            ->groupBy('s.id')
            ->getQuery()
            ->getResult();
    }

    public function getShopsPurchasesGroupedByDate(User $user, \DateTime $startDate, \DateTime $endDate)
    {
        return $this->createQueryBuilder('s')
            ->select('
                p as product,
                sum(pp.price * pp.amount) as total_purchase,
                DATE_FORMAT(pp.createdAt, \'%Y-%m-%d\') as created_date
            ')
            ->join(
                Product::class,
                'p',
                Join::WITH,
                'p.shop = s'
            )
            ->join(
                ProductPurchase::class,
                'pp',
                Join::WITH,
                'pp.product = p'
            )
            ->groupBy('created_date, p.id')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param array|string $status
     * @param ShopType|null $type
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAllWithStatusQueryBuilder($status, ShopType $type = null): QueryBuilder
    {
        $qb = $this->createQueryBuilder('s')
            ->leftJoin(ShopStatus::class, 'ss', Join::WITH, 's.status = ss')
            ->andWhere('ss.name IN (:statuses)')
            ->setParameter('statuses', (array)$status);

        if ($type !== null) {
            $qb->andWhere('s.type = :type')
                ->setParameter('type',$type);
        }

        return $qb;
    }

    /**
     * @param $status
     * @param \DateTime $start
     * @param \DateTime $end
     * @return mixed
     */
    public function findAllWithStatusBetweenDates($status, \DateTime $start = null, \DateTime $end = null)
    {
        $qb = $this->findAllWithStatusQueryBuilder($status)
            ->leftJoin(User::class, 'u', Join::WITH, 's.user = u');

        if ($start) {
            $qb->andWhere('u.createdAt >= :start')->setParameter('start', $start);
        }

        if ($end) {
            $qb->andWhere('u.createdAt <= :end')->setParameter('end', $end);
        }

        return $qb->getQuery()->getResult();
    }

    public function getShopsWithAcceptedConversionsBetweenDates(\DateTime $start, \DateTime $end)
    {
        return $this->createQueryBuilder('s')
            ->innerJoin(ShopConversion::class, 'sc', Join::WITH, 'sc.shop = s')
            ->andWhere('sc.status = :status')
            ->andWhere('sc.createdAt >= :start')
            ->andWhere('sc.createdAt <= :end')

            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setParameter('status', ShopConversionStatus::ACCEPTED)

            ->getQuery()
            ->getResult();
    }

    public function findAllWithStatusAndRewardModes($status, array $rewardsMod, ShopType $type = null): QueryBuilder
    {
        $qb = $this->findAllWithStatusQueryBuilder($status, $type);

        $qb->andWhere('s.rewardsMode IN (:rewardsMode)')
            ->setParameter('rewardsMode', $rewardsMod);

        return $qb;
    }
}
