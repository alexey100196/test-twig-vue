<?php


namespace App\Security\Voter;


use App\Entity\Shop;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;


class ShopVoter extends Voter
{
    const JOIN_SHOP_PROGRAM = 'JOIN_SHOP_PROGRAM';
    const SHARE_SHOP = 'SHARE_SHOP';

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::JOIN_SHOP_PROGRAM))) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof Shop) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param $shop
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $shop, TokenInterface $token)
    {
        $user = $token->getUser();

        // Check if user is logged and is referrer.
        if (!$user instanceof User) {
            return false;
        }

        if ($attribute === self::JOIN_SHOP_PROGRAM) {
            return $this->canJoinShopProgram($shop, $user);
        } elseif($attribute === self::SHARE_SHOP) {
            return $this->canShareShop($shop, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * Check if referrer profile is complete.
     *
     * @param Shop $shop
     * @param User $user
     * @return bool
     */
    private function canJoinShopProgram(Shop $shop, User $user)
    {
        return $user->isReferrer() && $shop->hasJoinedUser($user) === false;
    }

    /**
     * Check if shop can share.
     *
     * @param Shop $shop
     * @return bool
     */
    private function canShareShop(Shop $shop)
    {
        return $shop->isActive();
    }
}