<?php


namespace App\Form\ShopEvent;

use App\Entity\EventEntry;
use App\Entity\EventEntryType;
use App\Entity\Shop;
use App\Entity\User;
use App\Form\ShopEvent\Parts\EventEntryHttpReferrerType;
use App\Form\ShopEvent\Parts\EventEntryTestType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class SignUpType extends AbstractEventEntryType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('http_referer', EventEntryHttpReferrerType::class)
            ->add('test', EventEntryTestType::class)
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                if ($data['test'] ?? false) {
                    $event->getForm()->remove('referrer_link');
                }
                $data['type'] = EventEntryType::TYPE_SIGN_UP;
                $event->setData($data);
            });
    }
}