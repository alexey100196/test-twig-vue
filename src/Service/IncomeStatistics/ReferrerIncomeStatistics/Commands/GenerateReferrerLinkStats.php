<?php


namespace App\Service\IncomeStatistics\ReferrerIncomeStatistics\Commands;


use App\Entity\Product;
use App\Entity\ReferrerLink;
use App\Entity\User;
use App\Service\IncomeStatistics\CommandInterface;

class GenerateReferrerLinkStats implements CommandInterface
{
    /**
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @var ReferrerLink
     */
    protected $referrerLink;

    /**
     * GenerateStats constructor.
     * @param ReferrerLink $referrerLink
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     */
    public function __construct(ReferrerLink $referrerLink, \DateTime $startDate, \DateTime $endDate)
    {
        $this->referrerLink = $referrerLink;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    /**
     * @return ReferrerLink
     */
    public function getReferrerLink(): ReferrerLink
    {
        return $this->referrerLink;
    }
}