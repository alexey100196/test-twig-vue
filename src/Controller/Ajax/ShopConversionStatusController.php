<?php


namespace App\Controller\Ajax;


use App\Controller\AjaxAbstractController;
use App\Entity\ShopConversion;
use App\Repository\ShopConversionRepository;
use App\Service\Shop\ShopConversionStatusService;
use App\Util\Consts;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ShopConversionStatusController extends AjaxAbstractController
{
    /**
     * @var ShopConversionStatusService
     */
    private $shopConversionStatusService;

    /**
     * ShopConversionStatusController constructor.
     * @param ShopConversionStatusService $shopConversionStatusService
     */
    public function __construct(ShopConversionStatusService $shopConversionStatusService)
    {
        $this->shopConversionStatusService = $shopConversionStatusService;
    }

    /**
     * @Security("is_granted('CHANGE_CONVERSION_STATUS', shopConversion)")
     * @Route(
     *     "/ajax/company/conversion/{shopConversion}/status",
     *     name="ajax_shop_conversion_status_update",
     *     methods={"POST"}
     * )
     * @param ShopConversion $shopConversion
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function update(ShopConversion $shopConversion, Request $request)
    {
        // @todo: csrf


        $this->shopConversionStatusService->changeStatus(
            $shopConversion,
            $request->request->get('status'),
            $this->getUser(),
            (string)$request->request->get('reason')
        );

        $this->addFlash(Consts::SUCCESS, 'Transaction has been rejected.');

        return new JsonResponse([], Response::HTTP_OK);
    }
}