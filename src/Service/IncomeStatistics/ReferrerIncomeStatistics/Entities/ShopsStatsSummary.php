<?php


namespace App\Service\IncomeStatistics\ReferrerIncomeStatistics\Entities;


use App\Entity\Shop;

class ShopsStatsSummary
{
    /**
     * @var Shop
     */
    private $shop;

    /**
     * @var float
     */
    private $totalPurchaseValueGenerated;

    /**
     * @var float
     */
    private $totalSignUpsValueGenerated;

    /**
     * @var int
     */
    private $totalNumberOfViews;

    /**
     * @var int
     */
    private $purchaseCount;

    /**
     * @var int
     */
    private $usedCouponsCount;

    /**
     * @var int
     */
    private $inviteFriendEmails;

    /**
     * @var float
     */
    private $purchaseReferrerProfit;

    /**
     * @var float
     */
    private $signUpReferrerProfit;

    /**
     * @param Shop $shop
     */
    public function setShop(Shop $shop): void
    {
        $this->shop = $shop;
    }

    /**
     * @param float $totalPurchaseValueGenerated
     */
    public function setTotalPurchaseValueGenerated(float $totalPurchaseValueGenerated): void
    {
        $this->totalPurchaseValueGenerated = $totalPurchaseValueGenerated;
    }

    /**
     * @param float $totalSignUpsValueGenerated
     */
    public function setTotalSignUpsValueGenerated(float $totalSignUpsValueGenerated): void
    {
        $this->totalSignUpsValueGenerated = $totalSignUpsValueGenerated;
    }

    /**
     * @param int $totalNumberOfViews
     */
    public function setTotalNumberOfViews(int $totalNumberOfViews): void
    {
        $this->totalNumberOfViews = $totalNumberOfViews;
    }

    /**
     * @param float $purchaseReferrerProfit
     */
    public function setPurchaseReferrerProfit(float $purchaseReferrerProfit): void
    {
        $this->purchaseReferrerProfit = $purchaseReferrerProfit;
    }

    /**
     * @param float $signUpReferrerProfit
     */
    public function setSignUpReferrerProfit(float $signUpReferrerProfit): void
    {
        $this->signUpReferrerProfit = $signUpReferrerProfit;
    }

    /**
     * @param int $usedCouponsCount
     * @return ShopsStatsSummary
     */
    public function setUsedCouponsCount(int $usedCouponsCount): ShopsStatsSummary
    {
        $this->usedCouponsCount = $usedCouponsCount;

        return $this;
    }

    /**
     * @param int $inviteFriendEmails
     * @return ShopsStatsSummary
     */
    public function setInviteFriendEmails(int $inviteFriendEmails): ShopsStatsSummary
    {
        $this->inviteFriendEmails = $inviteFriendEmails;

        return $this;
    }

    /**
     * @param int $purchaseCount
     * @return ShopsStatsSummary
     */
    public function setPurchaseCount(int $purchaseCount): ShopsStatsSummary
    {
        $this->purchaseCount = $purchaseCount;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'shop' => [
                'id' => $this->shop->getId(),
                'name' => $this->shop->getName(),
            ],
            'total_purchase_value_generated' => $this->totalPurchaseValueGenerated,
            'total_sign_ups_value_generated' => $this->totalSignUpsValueGenerated,
            'total_number_of_views' => $this->totalNumberOfViews,
            'purchase_referrer_profit' => $this->purchaseReferrerProfit,
            'sign_up_referrer_profit' => $this->signUpReferrerProfit,
            'purchase_count' => $this->purchaseCount,
            'invite_friend_emails_count' => $this->inviteFriendEmails,
            'used_coupons_count' => $this->usedCouponsCount,
        ];
    }
}