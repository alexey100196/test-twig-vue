<?php

namespace App\Helper\WidgetCompletion\Resolvers;

use App\Helper\WidgetCompletion\Resolvers\AbstractResolver;

class CouponsData extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        $inv = $this->widgetShopSetting->getShop()->getInviteeCouponPackages();
        $ref = $this->widgetShopSetting->getShop()->getCouponPackages();
        if ($inv->count() > 0 && $ref->count() > 0) {
            $this->addScore(100);
        }
    }
}