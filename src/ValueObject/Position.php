<?php


namespace App\ValueObject;


use JsonSerializable;

class Position implements JsonSerializable
{
    /**
     * @var int|float
     */
    private $x;

    /**
     * @var int|float
     */
    private $y;

    /**
     * Position constructor.
     * @param $x
     * @param $y
     */
    public function __construct($x = null, $y = null)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function setX($x)
    {
        $this->x = $x;
    }

    public function setY($y)
    {
        $this->y = $y;
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

    public function jsonSerialize()
    {
        return json_encode(get_object_vars($this));
    }


}