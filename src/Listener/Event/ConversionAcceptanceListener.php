<?php


namespace App\Listener\Event;


use App\Adapter\MailSender;
use App\Entity\Shop;
use App\Event\Conversion\ConversionTimeForAcceptanceExpired;
use App\Repository\ShopRepository;
use App\Service\Admin\Reports\ReferrerPayoutsCollection;
use App\Service\Admin\Reports\ReferrerPayoutsService;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConversionAcceptanceListener
{
    /**
     * @var MailSender
     */
    private $mailSender;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * @var ReferrerPayoutsService
     */
    private $referrerPayoutsService;

    /**
     * @var string
     */
    private $adminEmail;

    public function __construct(
        \Twig_Environment $twig,
        MailSender $mailSender,
        LoggerInterface $logger,
        ShopRepository $shopRepository,
        ReferrerPayoutsService $referrerPayoutsService,
        ContainerInterface $container
    )
    {
        $this->twig = $twig;
        $this->mailSender = $mailSender;
        $this->logger = $logger;
        $this->shopRepository = $shopRepository;
        $this->referrerPayoutsService = $referrerPayoutsService;
        $this->adminEmail = $container->getParameter('admin_email');
    }

    public function onTimeForAcceptanceExpired(ConversionTimeForAcceptanceExpired $event)
    {
        $start = $event->getStart();
        $end = $event->getEnd();

        $shops = $this->shopRepository->getShopsWithAcceptedConversionsBetweenDates($start, $end);

        foreach ($shops as $shop) {
            $payouts = $this->referrerPayoutsService->getShopReferrersPayouts($shop, $start, $end);
            $this->sendPayoutsToShopOwnerByEmail($shop, $payouts, $start);
        }
    }

    private function sendPayoutsToShopOwnerByEmail(Shop $shop, ReferrerPayoutsCollection $payouts, \DateTime $start)
    {
        $mailSubjsct = sprintf(
            'SuperInterface payment request – %s – %s - %s EUR',
            $start->format('F Y'),
            $shop->getName(),
            $payouts->getTotal()
        );

        foreach ([$this->adminEmail, $shop->getUser()->getEmail()] as $email) {
            $this->mailSender->send(
                'hello@superinterface.com',
                $email,
                $mailSubjsct,
                $this->twig->render('email/shop/payouts.html.twig', ['payouts' => $payouts, 'shop' => $shop])
            );
        }
    }
}