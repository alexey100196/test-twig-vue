<?php

namespace App\Helper\ShopCompletion\Resolvers;

use App\Helper\ShopCompletion\Resolvers\AbstractResolver;

class CookieLifespan extends AbstractResolver
{
    public function resolve()
    {
        if ($this->shop->hasOnlyCouponRewards()) {
            $this->addScore(100);
            return;
        }

        if ($this->shop->getCookieLifetime()) {
            $this->addScore(100);
        }
    }
}
