<?php


namespace App\Controller;


use App\Entity\ShortLinks\AffiliateShortLinkStats;
use App\Entity\ShortLinks\CustomShortLinkStats;
use App\Repository\ShortLinks\AffiliateShortLinkRepository;
use App\Repository\ShortLinks\CustomShortLinkRepository;
use App\Service\ShortLinks\AffiliateShortLinkService;
use App\Service\ShortLinks\AffiliateShortLinkStatsService;
use App\Service\ShortLinks\CustomShortLinkStatsService;
use GeoIp2\Exception\AddressNotFoundException;
use MaxMind\Db\Reader\InvalidDatabaseException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use GeoIp2\Database\Reader;
use Browser;

class RedirectController extends AbstractController
{
    /** @var LoggerInterface */
    private $logger;

    /** @var Reader */
    private $reader;

    /** @var CustomShortLinkStatsService */
    private $customShortLinkStatsService;

    /** @var AffiliateShortLinkStatsService */
    private $affiliateShortLinkStatsService;

    /** @var CustomShortLinkRepository */
    private $customShortLinkRepository;

    /** @var AffiliateShortLinkRepository */
    private $affiliateShortLinkRepository;

    /**
     * ReferrerController constructor.
     * @param LoggerInterface $logger
     * @param CustomShortLinkStatsService $customShortLinkStatsService
     * @param AffiliateShortLinkService $affiliateShortLinkStatsService
     * @param CustomShortLinkRepository $customShortLinkRepository
     * @param AffiliateShortLinkRepository $affiliateShortLinkRepository
     */
    public function __construct(
        LoggerInterface $logger,
        CustomShortLinkStatsService $customShortLinkStatsService,
        AffiliateShortLinkService $affiliateShortLinkStatsService,
        CustomShortLinkRepository $customShortLinkRepository,
        AffiliateShortLinkRepository $affiliateShortLinkRepository
    ) {
        $this->logger = $logger;
        $this->reader = new Reader(__DIR__ . '/../../GeoLite2-Country.mmdb');
        $this->customShortLinkStatsService = $customShortLinkStatsService;
        $this->affiliateShortLinkStatsService = $affiliateShortLinkStatsService;
        $this->customShortLinkRepository = $customShortLinkRepository;
        $this->affiliateShortLinkRepository = $affiliateShortLinkRepository;
    }

    public function shortRedirect(string $slug)
    {
        if (!($customLink = $this->customShortLinkRepository->findOneBy(['short' => $slug]))) {
            $customLink = $this->affiliateShortLinkRepository->findOneBy(['short' => $slug]);
        }

        if (!$customLink) throw new NotFoundHttpException();

        $destinationUrl = $this->getDestinationUrl($customLink);

        if (!($referrer = $customLink->getReferrer())) {
            throw new NotFoundHttpException('Not found owner of this link.');
        }

        try {
            /** @var \GeoIp2\Model\Country $ipResp */
            $ipResp = $this->reader->country($this->getUserIp());
            $country = isset($ipResp->country->isoCode) ? $ipResp->country->isoCode : 'Unknown';
            $browser = new Browser();
            $this->saveLinkStats($customLink, $country, $browser->getBrowser(), $browser->getPlatform());
            // @TODO: Sprawdzanie /{slug} musi byc ostatnim routem! Nie widzi teraz np. /store
        } catch (AddressNotFoundException $e) {
            $this->logger->error($e->getMessage());
        } catch (InvalidDatabaseException $e) {
            $this->logger->error($e->getMessage());
        }

        $email = $referrer->getEmail();
        $pixels = [
            'facebook' => $customLink->getFacebookPixel() ? $referrer->getFacebookPixel() : null,
            'twitter' => $customLink->getTwitterPixel() ? $referrer->getTwitterPixel() : null,
            'pinterest' => $customLink->getPinterestPixel() ? $referrer->getPinterestPixel() : null,
            'linkedin' => $customLink->getLinkedinPixel() ? $referrer->getLinkedinPixel() : null,
            'google' => $customLink->getGooglePixel() ? $referrer->getGooglePixel() : null,
            'quora' => $customLink->getQuoraPixel() ? $referrer->getQuoraPixel() : null,
            'referrerEmail' => [
                'text' => $email,
                'hash' => $email ? hash('sha256', $email) : ''
            ]
        ];

        return $this->render('redirect.html.twig', compact('destinationUrl', 'pixels'));
    }

    private function getDestinationUrl(object $customLink) {
        $destinationUrl = null;

        if ($customLink && (get_class($customLink) === 'App\Entity\ShortLinks\CustomShortLink')) {
            $destinationUrl = $customLink->getOriginal();
        } elseif (!($referrerLink = $customLink->getReferrerLink())) {
            throw new NotFoundHttpException('Not found affiliate short link for this slug.');
        } else {
            $destinationUrl = $referrerLink->getDestinationUrl();
        }

        return $destinationUrl;
    }

    private function getUserIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    /**
     * @param object $shortLink
     * @param string $country
     * @param string $browser
     * @param string $os
     * @return AffiliateShortLinkStats|CustomShortLinkStats
     */
    private function saveLinkStats(object $shortLink, string $country, string $browser, string $os) {
        if ($shortLink && (get_class($shortLink) === 'App\Entity\ShortLinks\CustomShortLink')) {
            $stats = new CustomShortLinkStats();
            $stats->setCustomShortLink($shortLink);
            $stats->setCountry($country);
            $stats->setBrowser($browser);
            $stats->setSystem($os);
            $stats = $this->customShortLinkStatsService->save($stats);
        } else {
            $stats = new AffiliateShortLinkStats();
            $stats->setAffiliateShortLink($shortLink);
            $stats->setCountry($country);
            $stats->setBrowser($browser);
            $stats->setSystem($os);
            $stats = $this->affiliateShortLinkStatsService->save($stats);
        }

        return $stats;
    }
}