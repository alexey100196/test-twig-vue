<?php


namespace App\Controller\Account\Referrer;


use App\Entity\CustomDomain;
use App\Entity\User;
use App\Form\CustomDomainType;
use App\Repository\CustomDomainRepository;
use App\Service\Referrer\CustomDomainService;
use App\Traits\Controller\FormResponseArray;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Exception\InvalidParameterException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\ForbiddenOverwriteException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ReferrerShopLinkController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class CustomDomainController extends AbstractController
{
    use FormResponseArray;

    /** @var CustomDomainRepository */
    private $customDomainRepository;

    /** @var CustomDomainService */
    private $customDomainService;

    /**
     * ReferrerShopController constructor.
     *
     * @param CustomDomainRepository $customDomainRepository
     * @param CustomDomainService $customDomainService
     */
    public function __construct(
        CustomDomainRepository $customDomainRepository,
        CustomDomainService $customDomainService
    )
    {
        $this->customDomainRepository = $customDomainRepository;
        $this->customDomainService = $customDomainService;
    }

    /**
     * @Route("/account/custom-domains", name="account_custom_domains_save", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function save(Request $request)
    {
        try {
            $customDomain = null;
            $user = $this->getUser();
            $data = $request->request->all();
            unset($data['id']);

            if ($request->request->get('id')) {
                $customDomain = $this->customDomainRepository->find($request->request->get('id'));
            }

            if (!$customDomain) {
                $customDomain = new CustomDomain();
                $customDomain->setUser($user);
            } else if($user->getId() !== $customDomain->getUser()->getId()) {
                throw new \Exception('You don\'t have permission to edit this custom domain', Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $form = $this->createForm(CustomDomainType::class, $customDomain);
            $form->submit($data);

            if ($form->isSubmitted() && $form->isValid()) {
                /** @var CustomDomain $existsDomain */
                $existsDomain = $this->customDomainRepository->findOneBy([
                    'name' => $data['name']
                ]);

                if ($existsDomain && (
                    ($existsDomain->getId() !== $customDomain->getId()) ||
                    ($existsDomain->getUser()->getId() !== $customDomain->getUser()->getId())
                )) {
                    throw new \Exception('This domain name is already used.', Response::HTTP_UNPROCESSABLE_ENTITY);
                }
                unset($existsDomain);

                $this->customDomainService->save($form->getData());

                $customDomains = $user->getCustomDomains()->map(function (CustomDomain $item) {
                    return [
                        'id' => $item->getId(),
                        'name' => $item->getName()
                    ];
                });

                return new JsonResponse([
                    'message' => 'Custom domain has been saved successfully.',
                    'customDomains' => (array) $customDomains->toArray()
                ], Response::HTTP_OK);
            }

            return new JsonResponse($form->getErrors()[0]->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return new JsonResponse([$e->getMessage()], ($e->getCode()) ? $e->getCode() : 500);
        }
    }

    /**
     * @Route("/account/custom-domains/{customDomain}/delete", name="account_custom_domains_delete", methods={"POST"})
     * @param CustomDomain $customDomain
     * @return JsonResponse
     */
    public function delete(CustomDomain $customDomain)
    {
        try {
            $user = $this->getUser();

            if (!$customDomain) {
                throw new NotFoundHttpException('Custom domain not found.');
            } else if ($customDomain->getUser()->getId() !== $user->getId()) {
                throw new NotFoundHttpException('Custom domain not found.');
            }

            $this->customDomainService->remove($user, $customDomain);

            $customDomains = $user->getCustomDomains()->map(function (CustomDomain $item) {
                return [
                    'id' => $item->getId(),
                    'name' => $item->getName()
                ];
            });

            return new JsonResponse([
                'message' => 'Custom domain has been removed successfully.',
                'customDomains' => (array) $customDomains->toArray()
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonResponse([$e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}