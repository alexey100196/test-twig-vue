<?php

namespace App\Entity\Subscription;

use App\Entity\User;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JsonSerializable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Subscription\PlanSubscriptionRepository")
 */
class PlanSubscription implements JsonSerializable
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="subscriptions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subscription\Plan")
     * @ORM\JoinColumn(nullable=false)
     */
    private $plan;

    /**
     * @ORM\Column(type="datetime")
     */
    private $trialEndsAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startsAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endsAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $cancelsAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $canceledAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPlan(): ?Plan
    {
        return $this->plan;
    }

    public function setPlan(?Plan $plan): self
    {
        $this->plan = $plan;

        return $this;
    }

    public function getTrialEndsAt(): ?\DateTimeInterface
    {
        return $this->trialEndsAt;
    }

    public function setTrialEndsAt(\DateTimeInterface $trialEndsAt): self
    {
        $this->trialEndsAt = $trialEndsAt;

        return $this;
    }

    public function getStartsAt(): ?\DateTimeInterface
    {
        return $this->startsAt;
    }

    public function setStartsAt(?\DateTimeInterface $startsAt): self
    {
        $this->startsAt = $startsAt;

        return $this;
    }

    public function getEndsAt(): ?\DateTimeInterface
    {
        return $this->endsAt;
    }

    public function setEndsAt(?\DateTimeInterface $endsAt): self
    {
        $this->endsAt = $endsAt;

        return $this;
    }

    public function getCancelsAt(): ?\DateTimeInterface
    {
        return $this->cancelsAt;
    }

    public function setCancelsAt(?\DateTimeInterface $cancelsAt): self
    {
        $this->cancelsAt = $cancelsAt;

        return $this;
    }

    public function getCanceledAt(): ?\DateTimeInterface
    {
        return $this->canceledAt;
    }

    public function setCanceledAt(?\DateTimeInterface $canceledAt): self
    {
        $this->canceledAt = $canceledAt;

        return $this;
    }

    public function cancel(bool $immediately = false)
    {
        $this->canceledAt = new \DateTime();

        if ($immediately) {
            $this->setEndsAt($this->canceledAt);
        }
    }

    public function isEnded(): bool
    {
        if ($this->endsAt) {
            return $this->endsAt <= new \DateTime();
        }

        return false;
    }

    public function isOnTrial(): bool
    {
        if ($this->trialEndsAt) {
            return $this->getTrialEndsAt() == $this->getEndsAt();
        }

        return false;
    }

    public function isOnExpiredTrial(): bool
    {
        return $this->getTrialEndsAt() == $this->getEndsAt() && $this->isEnded();
    }

    public function isCanceled(): bool
    {
        if ($this->canceledAt) {
            return $this->canceledAt <= new \DateTime();
        }

        return false;
    }

    public function isActive()
    {
        return !$this->isEnded() || $this->isOnTrial();
    }

    public function willContinueAfterTrial()
    {
        return $this->getStartsAt() < $this->getEndsAt() && $this->getEndsAt() > new \DateTime();
    }

    public function isActiveButNotOnTrial()
    {
        return !$this->isEnded() && !$this->isOnTrial();
    }

    public function getExpiryDate()
    {
        return (clone $this->getStartsAt())->modify('+' . $this->getPlan()->getTotalPeriodInDays() . ' days');
    }

    public function getSummaryName()
    {
        if ($this->isOnTrial()) {
            return 'Trial';
        } elseif ($this->isEnded() || $this->isOnExpiredTrial()) {
            return 'Expired';
        }

        return $this->getPlan()->getName();
    }

    public function wasCanceledOnTrial()
    {
        return $this->getCanceledAt() && $this->getCanceledAt() <= $this->getTrialEndsAt();
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'plan_name' => $this->getPlan()->getName(),
        ];
    }
}
