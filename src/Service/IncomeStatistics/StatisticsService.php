<?php


namespace App\Service\IncomeStatistics;


use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

abstract class StatisticsService
{
    /**
     * @var array|CommandInterface[]
     */
    protected $handlers = [];

    /**
     * @var RegistryInterface
     */
    protected $doctrine;

    /**
     * @var ParameterBagInterface
     */
    protected $parameterBag;

    /**
     * IncomeStatsService constructor.
     * @param RegistryInterface $doctrine
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(RegistryInterface $doctrine, ParameterBagInterface $parameterBag)
    {
        $this->doctrine = $doctrine;
        $this->parameterBag = $parameterBag;

        foreach ($this->handlers as $command => $handler) {
            $this->handlers[$command] = new $handler($doctrine, $parameterBag);
        }
    }

    /**
     * @param CommandInterface $command
     * @return void
     * @throws \Exception
     */
    public function generate(CommandInterface $command)
    {
        if (!isset($this->handlers[get_class($command)])) {
            throw new \Exception(get_class($command) . ' is not a valid income stats handler command.');
        }

        return $this->handlers[get_class($command)]->handle($command);
    }

}