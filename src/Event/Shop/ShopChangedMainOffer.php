<?php


namespace App\Event\Shop;

class ShopChangedMainOffer extends ShopChangedOffer
{
    /** @var string */
    const NAME = 'shop.changed-main-offer';
}