<?php

namespace App\Controller\Auth;

use App\Adapter\MailSender;
use App\Entity\PasswordReminder;
use App\Form\PasswordReminderType;
use App\Form\PasswordResetType;
use App\Repository\PasswordReminderRepository;
use App\Repository\UserRepository;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordReminderController extends AbstractController
{
    /**
     * @var MailSender
     */
    private $mailSender;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var PasswordReminderRepository
     */
    private $passwordReminderRepository;

    public function __construct(
        MailSender $mailSender,
        UserRepository $userRepository,
        PasswordReminderRepository $passwordReminderRepository
    )
    {
        $this->mailSender = $mailSender;
        $this->userRepository = $userRepository;
        $this->passwordReminderRepository = $passwordReminderRepository;
    }

    /**
     * @todo: check if hash exists
     * @param Request $request
     * @Route("/password/remind", name="auth_password_reminder")
     */
    public function remind(Request $request)
    {
        $form = $this->createForm(PasswordReminderType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // Show message to prevent checking if account exists.
            if (!$user = $this->userRepository->findOneBy($form->getData())) {
                return $this->passwordResetEmailSent();
            }

            $hash = bin2hex(random_bytes(32));

            $passwordReminder = new PasswordReminder();
            $passwordReminder->setUser($user);
            $passwordReminder->setHash($hash);

            $em = $this->getDoctrine()->getManager();
            $em->persist($passwordReminder);
            $em->flush();

            $this->sendReminderEmail($passwordReminder);

            return $this->passwordResetEmailSent();
        }

        return $this->render('auth/password/remind.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function passwordResetEmailSent()
    {
        $this->addFlash(Consts::SUCCESS, 'Reset link has been sent. Please check, if the e-mail is in your spam filter.');

        return $this->redirectToRoute('auth_password_reminder');
    }

    /**
     * @param UserPasswordEncoderInterface $encoder
     * @param Request $request
     * @param string $hash
     * @Route("/password/set/{hash}", name="set_auth_password_hash")
     */
    public function set(UserPasswordEncoderInterface $encoder, Request $request, string $hash)
    {
        $passwordReminder = $this->passwordReminderRepository->findByHash($hash);

        if (!$passwordReminder) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(PasswordResetType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $plainPassword = $data['plainPassword'];
            $user = $passwordReminder->getUser();
            $user->setPassword($encoder->encodePassword($user, $plainPassword));
            $user->setEmailConfirmedAt(new \DateTime());

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($user);
            $manager->remove($passwordReminder);
            $manager->flush();

            $this->addFlash(Consts::SUCCESS, 'Password has been saved.');

            return $this->redirectToRoute('login');
        }

        return $this->render('auth/password/set.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param UserPasswordEncoderInterface $encoder
     * @param Request $request
     * @param string $hash
     * @Route("/password/reset/{hash}", name="auth_password_hash")
     */
    public function reset(UserPasswordEncoderInterface $encoder, Request $request, string $hash)
    {
        $passwordReminder = $this->passwordReminderRepository->findByHash($hash);

        if (!$passwordReminder) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(PasswordResetType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $plainPassword = $data['plainPassword'];
            $user = $passwordReminder->getUser();
            $user->setPassword($encoder->encodePassword($user, $plainPassword));

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($user);
            $manager->remove($passwordReminder);
            $manager->flush();

            $this->addFlash(Consts::SUCCESS, 'Password has been changed.');

            return $this->redirectToRoute('login');
        }

        return $this->render('auth/password/reset.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Send email with link to reset password.
     * @param PasswordReminder $reminder
     * @return void
     */
    protected function sendReminderEmail(PasswordReminder $reminder): void
    {
        $this->mailSender->send(
            'hello@superinterface.com',
            $reminder->getUser()->getEmail(),
            'Password reminder.',
            $this->renderView('email/password-remind.html.twig', ['hash' => $reminder->getHash()])
        );
    }

}
