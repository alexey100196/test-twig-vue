<?php

namespace App\Service\Coupon;

use App\Entity\Coupon\Coupon;
use App\Entity\Coupon\UserCoupon;
use App\Entity\User;
use App\Event\ContactRequest\ContactRequestCreated;
use App\Event\Coupon\CouponUsed;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UseCoupon
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(EntityManagerInterface $em, EventDispatcherInterface $eventDispatcher)
    {
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Coupon $coupon
     * @param User $user
     * @param string|null $inviteeEmail
     * @return UserCoupon
     */
    public function __invoke(Coupon $coupon, User $user, string $inviteeEmail = null): UserCoupon
    {
        $this->em->beginTransaction();

        $coupon->incrementUsed();
        $userCoupon = $this->addCouponToUser($coupon, $user);
        $userCoupon->setInviteeEmail($inviteeEmail);

        $this->em->persist($userCoupon);
        $this->em->flush();
        $this->em->commit();

        $this->eventDispatcher->dispatch(CouponUsed::NAME, new CouponUsed($coupon));

        return $userCoupon;
    }

    /**
     * @param Coupon $coupon
     * @param User $user
     * @return UserCoupon
     */
    private function addCouponToUser(Coupon $coupon, User $user)
    {
        $userCoupon = new UserCoupon();
        $userCoupon->setUser($user);
        $userCoupon->setCoupon($coupon);

        return $userCoupon;
    }
}