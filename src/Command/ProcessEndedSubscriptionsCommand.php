<?php


namespace App\Command;

use App\Service\Subscription\ShopSubscriptionService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProcessEndedSubscriptionsCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:process-ended-subscriptions';

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ShopSubscriptionService $service */
        $service = $this->getContainer()->get(ShopSubscriptionService::class);
        $service->processAllEndedSubscriptions();
    }
}