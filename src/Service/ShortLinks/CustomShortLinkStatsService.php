<?php


namespace App\Service\ShortLinks;


use App\Entity\ShortLinks\CustomShortLinkStats;
use Doctrine\ORM\EntityManagerInterface;

class CustomShortLinkStatsService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param CustomShortLinkStats $customShortLinkStats
     * @return CustomShortLinkStats
     */
    public function save(CustomShortLinkStats $customShortLinkStats)
    {
        $this->em->persist($customShortLinkStats);
        $this->em->flush();

        return $customShortLinkStats;
    }

    /**
     * @param CustomShortLinkStats $customShortLinkStats
     */
    public function remove(CustomShortLinkStats $customShortLinkStats)
    {
        $this->em->remove($customShortLinkStats);
        $this->em->flush();
    }

}