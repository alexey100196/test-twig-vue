<?php

namespace App\Repository;

use App\Entity\ShopWidgetButton;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopWidgetButton|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopWidgetButton|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopWidgetButton[]    findAll()
 * @method ShopWidgetButton[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopWidgetButtonRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopWidgetButton::class);
    }

    // /**
    //  * @return ShopWidgetButton[] Returns an array of ShopWidgetButton objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ShopWidgetButton
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
