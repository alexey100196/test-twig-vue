<?php


namespace App\Service\ShopEvent\EventStrategies;


use App\Entity\Product;
use App\Entity\ProductPurchase;
use App\Entity\ShopPurchase;
use App\Manager\ProductManager;
use App\Service\ShopEvent\Interfaces\EventStrategyInterface;
use App\Service\SystemCommission\ApplierInterface;
use App\Service\SystemCommission\ShopCommission\Applier;

class TestStrategy extends AbstractStrategy implements EventStrategyInterface
{
    public function process()
    {
        $this->eventEntryManager->save($this->eventEntry);
    }
}