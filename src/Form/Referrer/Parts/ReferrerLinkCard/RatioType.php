<?php


namespace App\Form\Referrer\Parts\ReferrerLinkCard;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RatioType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => [
                '1:1' => '1:1',
                '5:4' => '5:4',
                '4:3' => '4:3',
                '16:9' => '16:9',
            ],
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}