<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class ShopOffer extends Constraint
{
    public $message = 'Special Offer should be greater than main offer.';

    public function validatedBy()
    {
        return ShopOfferValidator::class;
    }
}