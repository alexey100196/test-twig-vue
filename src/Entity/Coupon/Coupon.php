<?php

namespace App\Entity\Coupon;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Coupon\CouponRepository")
 * @UniqueEntity("code")
 */
class Coupon
{
    const CODE_MAX_LENGTH = 15;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $code;

    /**
     * @ORM\Column(type="integer")
     */
    private $used = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Coupon\CouponPackage", inversedBy="coupons")
     * @ORM\JoinColumn(nullable=false)
     */
    private $package;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", precision=20, scale=2, nullable=true)
     */
    private $conversionValue;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getUsed(): ?int
    {
        return $this->used;
    }

    public function setUsed(int $used): self
    {
        $this->used = $used;

        return $this;
    }

    public function getPackage(): ?CouponPackage
    {
        return $this->package;
    }

    public function setPackage(?CouponPackage $package): self
    {
        $this->package = $package;

        return $this;
    }

    public function incrementUsed()
    {
        $this->used++;
    }

    /**
     * @return float|null
     */
    public function getConversionValue(): ?float
    {
        return $this->conversionValue;
    }

    /**
     * @param float|null $conversionValue
     */
    public function setConversionValue(?float $conversionValue): void
    {
        $this->conversionValue = $conversionValue;
    }

    public function increaseConversionValue(float $by)
    {
        $this->conversionValue += $by;
    }
}
