<?php

namespace App\Tests\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ReferrerControllerTest extends WebTestCase
{

    /**
     * @testdox Redirect to login if user isn't referrer
     */
    public function testRedirect()
    {
        $client = static::createClient();
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $url = $client->getKernel()->getContainer()->getParameter('url');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('GET', '/store/me');

        $this->assertResponseRedirects($url.'/login', Response::HTTP_FOUND);
    }

    /**
     * @testdox Check if user hasn't referrer rule
     */
    public function testIsNotReferrer() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', null));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('GET', '/store/me');

        $this->assertResponseRedirects('/logged', Response::HTTP_FOUND);
    }

    /**
     * @param string $filter
     * @param $value
     * @return Cookie
     */
    private function logIn(string $filter, $value)
    {
        $session = self::$container->get('session');
        $em = self::$container->get('doctrine.orm.entity_manager');

        $user = $em->getRepository(User::class)->findOneBy([$filter => $value]);

        $firewallName = 'main';

        $token = new UsernamePasswordToken($user, null, $firewallName, $user->getRoles());
        $session->set('_security_'.$firewallName, serialize($token));
        $session->save();

        return new Cookie($session->getName(), $session->getId());
    }
}
