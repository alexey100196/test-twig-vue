<?php

namespace App\Form\Shop\Widget;

use App\Entity\Language;
use App\Entity\Widget\WidgetShopSetting;
use App\Form\Type\Shop\LogoType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class WidgetHeaderType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
         /*   Disable Widget Translations
         ->add('language', EntityType::class, [
                'class' => Language::class,
                'choice_value' => 'name',
            ])*/
            ->add('customText', TextareaType::class, [
                'constraints' => [
                    new NotNull(['message' => 'This field cannot be empty']),
                    new Length(['max' => 255])
                ]
            ])
            ->add('customBarText', TextareaType::class, [
                'constraints' => [
                    new NotNull(['message' => 'This field cannot be empty']),
                    new Length(['max' => 255])
                ]
            ])
            ->add('welcomePicture', LogoType::class, [
                'required' => false
            ])
            ->add('welcomePictureWidth', HiddenType::class, [
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual(100),
                    new LessThanOrEqual(2000),
                ]
            ])
            ->add('welcomePictureHeight', HiddenType::class, [
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual(100),
                    new LessThanOrEqual(2000),
                ]
            ])
            ->add('welcomePicturePosX', HiddenType::class, [
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual(-10000),
                    new LessThanOrEqual(10000),
                ]
            ])
            ->add('welcomePicturePosY', HiddenType::class, [
                'constraints' => [
                    new NotBlank(),
                    new GreaterThanOrEqual(-10000),
                    new LessThanOrEqual(10000),
                ]
            ])
            ->add('welcomePictureRatio', HiddenType::class, [
                'constraints' => [
                    new LessThanOrEqual(2000),
                    new GreaterThanOrEqual(0),
                ]
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            //Disable Widget Translations
            if (0){
                /** @var WidgetShopSetting $data */
                $data = $event->getData();

                if (!$data->getLanguage()) {
                    /** @var Language $language */
                    $language = $this->entityManager->getReference(Language::class, Language::LANGUAGE_EN_ID);
                    $data->setLanguage($language);
                }
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'data_class' => WidgetShopSetting::class
        ]);
    }
}