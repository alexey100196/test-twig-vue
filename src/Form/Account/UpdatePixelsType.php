<?php

namespace App\Form\Account;

use App\Entity\User;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class UpdatePixelsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('facebook_pixel', TextType::class, [
                'required' => false,
                'constraints' => [
                    new Length(['max' => 255])
                ]
            ])
            ->add('twitter_pixel', TextType::class, [
                'required' => false,
                'constraints' => [
                    new Length(['max' => 255])
                ]
            ])
            ->add('pinterest_pixel', TextType::class, [
                'required' => false,
                'constraints' => [
                    new Length(['max' => 255])
                ]
            ])
            ->add('linkedin_pixel', TextType::class, [
                'required' => false,
                'constraints' => [
                    new Length(['max' => 255])
                ]
            ])
            ->add('google_pixel', TextType::class, [
                'required' => false,
                'constraints' => [
                    new Length(['max' => 255])
                ]
            ])
            ->add('quora_pixel', TextType::class, [
                'required' => false,
                'constraints' => [
                    new Length(['max' => 255])
                ]
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
        ]);
    }

}
