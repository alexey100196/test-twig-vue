<?php

namespace App\Entity\Widget;

use App\Entity\Shop;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Widget\WidgetButtonRepository")
 */
class WidgetButton
{
    const POSITION_TOP_LEFT = 1;
    const POSITION_TOP_RIGHT = 2;
    const POSITION_BOTTOM_LEFT = 3;
    const POSITION_BOTTOM_RIGHT = 4;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $blinkEffect = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $hideBackgroundColor = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $showNumberOfReferrals = true;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $textColor = '#000000';

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $backgroundColor = '#ffffff';

    /**
     * @ORM\Column(type="integer")
     */
    private $iconSize = 50;

    /**
     * @ORM\Column(type="integer")
     */
    private $backgroundSize = 75;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Widget\WidgetButtonIcon")
     * @ORM\JoinColumn(nullable=false)
     */
    private $icon;

    /**
     * @ORM\Column(type="smallint")
     */
    private $position = self::POSITION_BOTTOM_LEFT;

    /**
     * @ORM\Column(name="ptop", type="smallint", nullable=true)
     */
    private $pTop;
    /**
     * @ORM\Column(name="pright", type="smallint", nullable=true)
     */
    private $pRight;
    /**
     * @ORM\Column(name="pbottom", type="smallint", nullable=true)
     */
    private $pBottom;
    /**
     * @ORM\Column(name="pleft", type="smallint", nullable=true)
     */
    private $pLeft;
    /**
     * @ORM\Column(name="mptop", type="smallint", nullable=true)
     */
    private $mpTop;
    /**
     * @ORM\Column(name="mpright", type="smallint", nullable=true)
     */
    private $mpRight;
    /**
     * @ORM\Column(name="mpbottom", type="smallint", nullable=true)
     */
    private $mpBottom;
    /**
     * @ORM\Column(name="mpleft", type="smallint", nullable=true)
     */
    private $mpLeft;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBlinkEffect(): ?bool
    {
        return $this->blinkEffect;
    }

    public function setBlinkEffect(bool $blinkEffect): self
    {
        $this->blinkEffect = $blinkEffect;

        return $this;
    }

    public function getShowNumberOfReferrals(): ?bool
    {
        return $this->showNumberOfReferrals;
    }

    public function setShowNumberOfReferrals(bool $showNumberOfReferrals): self
    {
        $this->showNumberOfReferrals = $showNumberOfReferrals;

        return $this;
    }

    public function getTextColor(): ?string
    {
        return $this->textColor;
    }

    public function setTextColor(string $textColor): self
    {
        $this->textColor = $textColor;

        return $this;
    }

    public function getBackgroundColor(): ?string
    {
        return $this->backgroundColor;
    }

    public function setBackgroundColor(string $backgroundColor): self
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    public function getIconSize(): ?int
    {
        return $this->iconSize;
    }

    public function setIconSize(int $iconSize): self
    {
        $this->iconSize = $iconSize;

        return $this;
    }

    public function getBackgroundSize(): ?int
    {
        return $this->backgroundSize;
    }

    public function setBackgroundSize(int $backgroundSize): self
    {
        $this->backgroundSize = $backgroundSize;

        return $this;
    }

    public function getIcon(): ?WidgetButtonIcon
    {
        return $this->icon;
    }

    public function setIcon(?WidgetButtonIcon $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getPtop(): ?int
    {
        return $this->pTop;
    }

    public function setPtop(?int $pTop): self
    {
        $this->pTop = $pTop;

        return $this;
    }

    public function getPright(): ?int
    {
        return $this->pRight;
    }

    public function setPright(?int $pRight): self
    {
        $this->pRight = $pRight;

        return $this;
    }

    public function getPbottom(): ?int
    {
        return $this->pBottom;
    }

    public function setPbottom(?int $pBottom): self
    {
        $this->pBottom = $pBottom;

        return $this;
    }

    public function getPleft(): ?int
    {
        return $this->pLeft;
    }

    public function setPleft(?int $pLeft): self
    {
        $this->pLeft = $pLeft;

        return $this;
    }

    public function getMptop(): ?int
    {
        return $this->mpTop;
    }

    public function setMptop(?int $mpTop): self
    {
        $this->mpTop = $mpTop;

        return $this;
    }

    public function getMpright(): ?int
    {
        return $this->mpRight;
    }

    public function setMpright(?int $mpRight): self
    {
        $this->mpRight = $mpRight;

        return $this;
    }

    public function getMpbottom(): ?int
    {
        return $this->mpBottom;
    }

    public function setMpbottom(?int $mpBottom): self
    {
        $this->mpBottom = $mpBottom;

        return $this;
    }

    public function getMpleft(): ?int
    {
        return $this->mpLeft;
    }

    public function setMpleft(?int $mpLeft): self
    {
        $this->mpLeft = $mpLeft;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * @return bool
     */
    public function isHideBackgroundColor(): bool
    {
        return $this->hideBackgroundColor;
    }

    /**
     * @param bool $hideBackgroundColor
     */
    public function setHideBackgroundColor(bool $hideBackgroundColor): void
    {
        $this->hideBackgroundColor = $hideBackgroundColor;
    }

    public function getCoordsArray() {
        return [
            'desktop' => [
                'top' => ($this->getPtop()) ? $this->getPtop().'px' : 'auto',
                'right' => ($this->getPright()) ? $this->getPright().'px' : 'auto',
                'bottom' => ($this->getPbottom() && !$this->getPtop()) ? $this->getPbottom().'px' : 'auto',
                'left' => ($this->getPleft() && !$this->getPright()) ? $this->getPleft().'px' : 'auto'
            ],
            'mobile' => [
                'top' => ($this->getMptop()) ? $this->getMptop().'px' : 'auto',
                'right' => ($this->getMpright()) ? $this->getMpright().'px' : 'auto',
                'bottom' => ($this->getMpbottom() && !$this->getMptop()) ? $this->getMpbottom().'px' : 'auto',
                'left' => ($this->getMpleft() && !$this->getMpright()) ? $this->getMpleft().'px' : 'auto'
            ]
        ];
    }
}
