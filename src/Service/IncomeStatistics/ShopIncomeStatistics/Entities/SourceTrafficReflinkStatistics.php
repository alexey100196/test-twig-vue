<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Entities;


use App\Entity\ReferrerLink;

class SourceTrafficReflinkStatistics
{
    /** @var ReferrerLink */
    public $referrerLink;

    /** @var int */
    public $viewsCount;

    /** @var int */
    public $purchaseCount;

    /** @var int */
    public $signUpCount;

    /** @var float */
    public $signUpReferrerProfit;

    /** @var float */
    public $purchaseReferrerProfit;

    /** @var float */
    public $purchaseTotal;

    public function __construct(
        ReferrerLink $referrerLink,
        int $viewsCount,
        int $purchaseCount,
        int $signUpCount,
        float $signUpReferrerProfit,
        float $purchaseReferrerProfit,
        float $purchaseTotal
    )
    {
        $this->referrerLink = $referrerLink;
        $this->viewsCount = $viewsCount;
        $this->purchaseCount = $purchaseCount;
        $this->signUpCount = $signUpCount;
        $this->signUpReferrerProfit = $signUpReferrerProfit;
        $this->purchaseReferrerProfit = $purchaseReferrerProfit;
        $this->purchaseTotal = $purchaseTotal;
    }

    public function toArray()
    {
        return [
            'reflink' => [
                'id' => $this->referrerLink->getId(),
                'slug' => $this->referrerLink->getSlug(),
                'nickname' => $this->referrerLink->getNickname(),
                'shop_url_name' => $this->referrerLink->getShopUrlName(),
            ],
            'views_count' => $this->viewsCount,
            'purchase_count' => $this->purchaseCount,
            'sign_up_count' => $this->signUpCount,
            'sign_up_referrer_profit' => $this->signUpReferrerProfit,
            'purchase_referrer_profit' => $this->purchaseReferrerProfit,
            'purchase_total' => $this->purchaseTotal,
        ];
    }

}