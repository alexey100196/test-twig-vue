<?php


namespace App\Controller\Account\Referrer;


use App\Entity\CardSet\CardSet;
use App\Helper\DatePeriodGenerator;
use App\Repository\CardSet\CardSetRepository;
use App\Service\Referrer\ReferrerShop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerCardsSetController extends AbstractController
{
    /**
     * @Route("/account/referrer/cards-set/create", name="account_referrer_cards_set_create")
     * @param ReferrerShop $referrerShop
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ReferrerShop $referrerShop)
    {
        $globalStats = $referrerShop->getReferrerStats($this->getUser());

        return $this->render('account/referrer/card-set/create.html.twig', [
            'globalStats' => $globalStats
        ]);
    }

    /**
     * @Route("/account/referrer/cards-set/{cardSet}", name="account_referrer_cards_set_show")
     * @param CardSet $cardSet
     * @param ReferrerShop $referrerShop
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(CardSet $cardSet, ReferrerShop $referrerShop)
    {
        if ($cardSet->getUser() !== $this->getUser()) {
            throw new HttpException(Response::HTTP_FORBIDDEN);
        }

        $globalStats = $referrerShop->getReferrerStats($this->getUser());

        return $this->render('account/referrer/card-set/show.html.twig', [
            'cardSet' => $cardSet,
            'globalStats' => $globalStats,
        ]);
    }

    /**
     * @Route("/account/referrer/cards-set/{year}/{month}", name="account_referrer_cards_set_index")
     * @param CardSetRepository $repository
     * @param int|null $year
     * @param int|null $month
     * @param Request $request
     * @param ReferrerShop $referrerShop
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(
        CardSetRepository $repository,
        int $year = null,
        int $month = null,
        Request $request,
        ReferrerShop $referrerShop
    )
    {
        $datePeriod = DatePeriodGenerator::generateFromYearAndMonth($year, $month);

        $cards = $repository->findAllWithStatusBetweenDates(
            $this->getUser(),
            $datePeriod->getStart(),
            $datePeriod->getEnd(),
            $request->query->get('name')
        );

        $globalStats = $referrerShop->getReferrerStats($this->getUser());

        return $this->render('account/referrer/card-set/index.html.twig', [
            'cardSets' => $cards,
            'currentPeriod' => $datePeriod,
            'globalStats' => $globalStats,
        ]);
    }
}