<?php


namespace App\Controller\Account\Referrer;


use App\Entity\Shop;
use App\Service\Referrer\ReferrerShop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerShopIncomeController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerShopIncomeController extends AbstractController
{
    /**
     * @var ReferrerShop $referrerShop
     */
    private $referrerShop;

    /**
     * ReferrerShopController constructor.
     * @param ReferrerShop $referrerShop
     */
    public function __construct(ReferrerShop $referrerShop)
    {
        $this->referrerShop = $referrerShop;
    }

    /**
     * @Route("/account/referrer/company/{shop}/income", name="account_referrer_shop_income_show")
     * @param Shop $shop
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(Shop $shop)
    {
        $item = $this->referrerShop->findReferrerShopWithStats($this->getUser(), $shop);

        if (!$item) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        return $this->render('account/referrer/shop/show-income.html.twig', [
            'item' => $item,
        ]);
    }

}