<?php


namespace App\Traits\Controller;

use App\Util\Consts;
use Symfony\Component\Form\Form;

trait FormResponseArray
{
    /**
     * Return form errors as array.
     *
     * @param Form $form
     * @return array
     */
    protected function getFormErrorMessages(Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrorMessages($child);
            }
        }

        return $errors;
    }

    /**
     * @param Form $form
     */
    protected function createFlashMessagesFromFormErrors(Form $form)
    {
        $this->addFlashErrorFromArray($this->getFormErrorMessages($form));
    }

    /**
     * @param array $messages
     */
    private function addFlashErrorFromArray(array $messages)
    {
        foreach ($messages as $message) {
            if (is_array($message)) {
                $this->addFlashErrorFromArray($message);
            } else {
                $this->addFlash(Consts::ERROR, $message);
            }
        }
    }

}