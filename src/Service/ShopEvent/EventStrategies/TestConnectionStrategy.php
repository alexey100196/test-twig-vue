<?php


namespace App\Service\ShopEvent\EventStrategies;


use App\Entity\EventEntry;
use App\Manager\EventEntryManager;
use App\Service\ShopEvent\Auth;
use App\Service\ShopEvent\Interfaces\EventStrategyInterface;

class TestConnectionStrategy extends AbstractStrategy implements EventStrategyInterface
{
    public function process()
    {
        $this->eventEntry->getShop()->setSnippetConnectedAt(new \DateTime());
        $this->eventEntryManager->save($this->eventEntry);
    }
}