<?php

namespace App\Repository\Coupon;

use App\Entity\Coupon\CouponDiscountType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CouponDiscountType|null find($id, $lockMode = null, $lockVersion = null)
 * @method CouponDiscountType|null findOneBy(array $criteria, array $orderBy = null)
 * @method CouponDiscountType[]    findAll()
 * @method CouponDiscountType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CouponDiscountTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CouponDiscountType::class);
    }
}
