<?php

namespace App\Form\Account;

use App\Entity\User;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Bic;
use Symfony\Component\Validator\Constraints\Iban;
use Symfony\Component\Validator\Constraints\Length;

class EditAccountBillingDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ibanNumber', TextType::class, ['constraints' => [
                new Iban()
            ]])
            ->add('swiftNumber', TextType::class, ['constraints' => [
                new Bic()
            ]])
            ->add('paypalAccount', EmailType::class, ['constraints' => [
                new Length(['max' => 64])
            ]]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'constraints' => [
                new UniqueEntity([
                    'fields' => 'ibanNumber',
                    'repositoryMethod' => 'findBy',
                    'message' => 'User with that iban number already exists.',
                ]),
                new UniqueEntity([
                    'fields' => 'swiftNumber',
                    'repositoryMethod' => 'findBy',
                    'message' => 'User with that swift number already exists.',
                ]),
                new UniqueEntity([
                    'fields' => 'paypalAccount',
                    'repositoryMethod' => 'findBy',
                    'message' => 'User with that email already exists.',
                ]),
            ],
        ]);
    }

}
