<?php


namespace App\Event\Shop;

use App\Entity\Shop;
use Symfony\Component\EventDispatcher\Event;

class ShopStatusChangedByAdmin extends Event
{
    /** @var string */
    const NAME = 'shop.status-changed-by-admin';

    /** @var Shop */
    protected $shop;

    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
    }

    public function getShop(): Shop
    {
        return $this->shop;
    }
}