<?php

namespace App\Repository\Widget;

use App\Entity\Widget\WidgetReferenceLinkBrowserFingerprint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method WidgetReferenceLinkBrowserFingerprint|null find($id, $lockMode = null, $lockVersion = null)
 * @method WidgetReferenceLinkBrowserFingerprint|null findOneBy(array $criteria, array $orderBy = null)
 * @method WidgetReferenceLinkBrowserFingerprint[]    findAll()
 * @method WidgetReferenceLinkBrowserFingerprint[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WidgetReferenceLinkBrowserFingerprintRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WidgetReferenceLinkBrowserFingerprint::class);
    }

    // /**
    //  * @return WidgetReferenceLinkBrowserFingerprint[] Returns an array of WidgetReferenceLinkBrowserFingerprint objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WidgetReferenceLinkBrowserFingerprint
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
