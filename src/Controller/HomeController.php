<?php

namespace App\Controller;

use App\Entity\ReferrerCoupon;
use App\Entity\Shop;
use App\Entity\Subscription\PlanType;
use App\Entity\User;
use App\Repository\CampaignRepository;
use App\Repository\ShopRepository;
use App\Repository\Subscription\PlanRepository;
use App\Repository\Subscription\PlanSubscriptionRepository;
use App\Service\DocumentReferrer\DocumentReferrerService;
use App\Service\DocumentReferrer\ValueObject\ReferrerLink;
use App\Service\Referrer\ReferrerReflink;
use App\Service\Referrer\ValidateReferrerLink;
use App\Service\ShopEvent\EventService;
use App\Service\Subscription\SubscriptionService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;

class HomeController extends AbstractController
{
    /**
     * @var ReferrerReflink
     */
    private $referrerReflink;
    /**
     * @var ShopRepository
     */
    private $shopRepository;
    /**
     * @var CampaignRepository
     */
    private $campaignRepository;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @param ReferrerReflink $referrerReflink
     * @param ShopRepository $shopRepository
     * @param CampaignRepository $campaignRepository
     * @param SessionInterface $session
     */
    public function __construct(
        ReferrerReflink $referrerReflink,
        ShopRepository $shopRepository,
        CampaignRepository $campaignRepository,
        SessionInterface $session
    )
    {
        $this->referrerReflink = $referrerReflink;
        $this->shopRepository = $shopRepository;
        $this->campaignRepository = $campaignRepository;
        $this->session = $session;
    }

    /**
     * This route is handled by config/routes.yaml.
     *
     * @param $subdomain
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showShop($subdomain)
    {
        if (!$shop = $this->shopRepository->findOneBy(['urlName' => $subdomain])) {
            throw $this->createNotFoundException('Shop does not exists.');
        }

        if ($shop->isInactive() || $shop->isRejected()) {
            return $this->redirectToRoute('shop_index');
        }

        return $this->render('shop/show.html.twig', [
            'shop' => $shop,
            'showShortenedFooter' => true,
            'disableSnippet' => $shop->getPartnerNumber() != $this->getParameter('internal_shop_number'),
        ]);
    }

    /**
     * This route is handled by config/routes.yaml.
     *
     * @param $subdomain
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showWidget($subdomain)
    {
        if (!$shop = $this->shopRepository->findOneBy(['urlName' => $subdomain])) {
            throw $this->createNotFoundException('Shop does not exists.');
        }

        if ($shop->isInactive() || $shop->isRejected()) {
            return $this->redirectToRoute('shop_index');
        }

        $hasActiveCampaign = !empty($this->campaignRepository->findActiveByShop($shop));

        return $this->render('shop/widget.html.twig', [
            'shop' => $shop,
            'hasActiveCampaign' => $hasActiveCampaign,
            'company_id' => $shop->getPartnerNumber()
        ]);
    }

    /**
     * @Route("/", name="home_index")
     * @param PlanRepository $planRepository
     * @return Response
     */
    public function indexAction(PlanRepository $planRepository)
    {
        if ($this->shouldRedirectToDashboard()) {
            return $this->redirectToRoute('logged');
        }

        return $this->render('home/index.html.twig', [
            'saasPlans' => $planRepository->findBy(['type' => PlanType::SHOP_SAAS]),
            'ecommercePlans' => $planRepository->findBy(['type' => PlanType::SHOP_ECOMMERCE]),
        ]);
    }

    /**
     * @Route("/pricing", name="home_pricing")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pricingAction()
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user && $shop = $user->getShop()) {
            if ($shop->isEcommerce()) {
                return $this->redirectToEcommercePricing();
            }
        }

        return $this->redirectToSaasPricing();
    }

    /**
     * @Route("/pricing/saas", name="home_pricing_saas")
     *
     * @param PlanRepository $planRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pricingSaasAction(PlanRepository $planRepository)
    {
        return $this->render('home/pricing-saas.html.twig', [
            'plans' => $planRepository->findBy(['type' => PlanType::SHOP_SAAS]),
        ]);
    }

    /**
     * @Route("/pricing/ecommerce", name="home_pricing_ecommerce")
     *
     * @param PlanRepository $planRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pricingEcommerceAction(PlanRepository $planRepository)
    {
        return $this->render('home/pricing-ecommerce.html.twig', [
            'plans' => $planRepository->findBy(['type' => PlanType::SHOP_ECOMMERCE]),
        ]);
    }

    /**
     * @Route("/reflink/card/{shopName}/{nickname}/{reflinkSlug}", name="home_reflink_card")
     *
     * @param string $shopName
     * @param string $nickname
     * @param string $reflinkSlug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cardReflink(string $shopName, string $nickname, string $reflinkSlug)
    {
        $referrerLink = $this->referrerReflink->getReferrerLink($shopName, $nickname, $reflinkSlug);
        $reflinkUrl = $this->generateUrl('home_reflink', compact('shopName', 'nickname', 'reflinkSlug'));

        if (!$referrerLink || !$reflinkUrl) {
            return $this->redirectToRoute('shop_index');
        }

        $this->session->set('redirectedFrom', ReferrerCoupon::TYPE_SET_OF_CARDS);

        return $this->redirect($reflinkUrl);
    }

    /**
     * @Route("/reflink/mystore/{shopName}/{nickname}/{reflinkSlug}", name="home_reflink_mystore")
     *
     * @param string $shopName
     * @param string $nickname
     * @param string $reflinkSlug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function myStoreReflink(string $shopName, string $nickname, string $reflinkSlug)
    {
        $referrerLink = $this->referrerReflink->getReferrerLink($shopName, $nickname, $reflinkSlug);
        $reflinkUrl = $this->generateUrl('home_reflink', compact('shopName', 'nickname', 'reflinkSlug'));

        if (!$referrerLink || !$reflinkUrl) {
            return $this->redirectToRoute('shop_index');
        }

        $this->session->set('redirectedFrom', ReferrerCoupon::TYPE_MY_STORE);

        return $this->redirect($reflinkUrl);
    }

    private function renderShareReferrerCoupon(\App\Entity\ReferrerLink $referrerLink, string $reflinkUrl)
    {
        $shop = $referrerLink->getShop();
        $lang = $shop->getWidgetSetting() && $shop->getWidgetSetting()->getLanguage() ? $shop->getWidgetSetting()->getLanguage()->getCode() : 'en';
        $translations = $this->getTranslations($lang);

        return $this->render('widget/share-referrer.html.twig', [
            'url' => $reflinkUrl,
            'user' => $referrerLink->getReferrer(),
            'widgetReferenceLink' => null,
            'shop' => $referrerLink->getShop(),
            'referrerCoupon' => $referrerLink->getReferrer()->getReferrerCouponByShop($referrerLink->getShop()),
            'translations' => $translations
        ]);
    }

    private function getTranslations(string $lang)
    {
        return Yaml::parseFile(__DIR__ . '/../../translations/widget.' . $lang . '.yaml');
    }

    /**
     * @param string $shopName
     * @param string $nickname
     * @param string $reflinkSlug
     * @param ValidateReferrerLink $validateReferrerLink
     * @param LoggerInterface $logger
     * @param SessionInterface $session
     * @return Response
     */
    public function reflink(
        string $shopName,
        string $nickname,
        string $reflinkSlug,
        ValidateReferrerLink $validateReferrerLink,
        LoggerInterface $logger,
        SessionInterface $session
    )
    {
        $referrerLink = $this->referrerReflink->getReferrerLink($shopName, $nickname, $reflinkSlug);
        if (!$referrerLink) {
            throw new NotFoundHttpException('Invalid url');
        }
/*        if(
            !($activeCampaign = $this->campaignRepository->findActiveByShop($referrerLink->getShop())) || empty($activeCampaign) ||
            !($refCampaign = $referrerLink->getCampaign()) ||
            !($activeCampaign[0]->getId() === $refCampaign->getId())
        ) {
            return $this->redirectToRoute('home_index');
        }*/

        $this->referrerReflink->registerViewForReferrerLink($referrerLink);

        $redirectFrom = $session->get('redirectedFrom');
        $targetUrl = $this->referrerReflink->getRedirectUrlForReffererLink($referrerLink);

/*        if ($referrerLink->getWidgetReferenceLink()) {
            return $this->redirectToRoute('widget_share', [
                'slug' => $referrerLink->getWidgetReferenceLink()->getSlug()
            ]);
        } elseif (in_array($redirectFrom, [ReferrerCoupon::TYPE_MY_STORE, ReferrerCoupon::TYPE_SET_OF_CARDS])) {
            if ($referrerLink->getReferrer()->hasActiveReferrerCoupon($referrerLink->getShop(), $redirectFrom)) {
                return $this->renderShareReferrerCoupon($referrerLink, $targetUrl);
            }
        }

        try {
            $validateReferrerLink($referrerLink);
        } catch (\LogicException $e) {
            $logger->warning('Invalid referrer link', [$shopName, $nickname, $reflinkSlug, $e->getMessage()]);
            return $this->redirectToRoute('home_index');
        }*/

        try {
            $validateReferrerLink($referrerLink);
        } catch (\LogicException $e) {
            $logger->warning('Invalid referrer link', [$shopName, $nickname, $reflinkSlug, $e->getMessage()]);
            return $this->redirectToRoute('home_index');
        }
        return new RedirectResponse($targetUrl);
    }

    private function redirectToSaasPricing()
    {
        return $this->redirectToRoute('home_pricing_saas', [], Response::HTTP_SEE_OTHER);
    }

    private function redirectToEcommercePricing()
    {
        return $this->redirectToRoute('home_pricing_ecommerce', [], Response::HTTP_SEE_OTHER);
    }

    private function shouldRedirectToDashboard(): bool
    {
        return null !== $this->getUser();
    }
}
