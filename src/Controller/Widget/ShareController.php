<?php

namespace App\Controller\Widget;

use App\Controller\AjaxAbstractController;
use App\Entity\ReferrerCoupon;
use App\Entity\Widget\WidgetReferenceLink;
use App\Repository\CampaignRepository;
use App\Repository\Coupon\CouponRepository;
use App\Repository\Coupon\UserCouponRepository;
use App\Repository\ReferrerCouponRepository;
use App\Service\Coupon\UseCoupon;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use App\Service\Referrer\ReferrerReflink;

/**
 * @Route("/share")
 */
class ShareController extends AjaxAbstractController
{
    /**
     * @Route("/{slug}", name="widget_share", methods={"GET"})
     *
     * @param Request $request
     * @param WidgetReferenceLink $widgetReferenceLink
     * @param ReferrerReflink $referrerReflink
     * @param SessionInterface $session
     * @param CampaignRepository $campaignRepository
     * @param ReferrerCouponRepository $referrerCouponRepository
     * @param CouponRepository $couponRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function share(
        Request $request,
        WidgetReferenceLink $widgetReferenceLink,
        ReferrerReflink $referrerReflink,
        SessionInterface $session,
        CampaignRepository $campaignRepository,
        ReferrerCouponRepository $referrerCouponRepository,
        CouponRepository $couponRepository
    )
    {
        $skipPhoneCode = false;
        if (null === $settings = $widgetReferenceLink->getShop()->getWidgetSetting(true)) {
            return $this->redirectToRoute('home_index');
        }


        if ($widgetReferenceLink->getCampaignId() != $settings->getCampaign()->getID()){
            return $this->redirectToRoute('home_index');
        }

        $lang = $settings->getLanguage() ? $settings->getLanguage()->getCode() : 'en';
        $translations = $this->getTranslations($lang);
        $referrerLink = $widgetReferenceLink->getReferrerLink();

        $url = $this->generateUrl('widget_redirect', ['slug' => $widgetReferenceLink->getSlug()]);
        if ($referrerLink) {
            $url = $referrerReflink->getRedirectUrlForReffererLink($referrerLink);
        }

        $shop = $widgetReferenceLink->getShop();
        if (empty($campaignRepository->findActiveByShop($shop))) {
            return $this->redirectToRoute('home_index');
        }

        $activeCampaign = $campaignRepository->findActiveByShop($shop);
        $activeCampaign = ($activeCampaign) ? $activeCampaign[0] : $activeCampaign;

        $isReferrerCoupon = ($widgetReferenceLink->getUser()->hasActiveReferrerCoupon($widgetReferenceLink->getShop(), $session->get('redirectedFrom', ReferrerCoupon::TYPE_LINKS)));
        $cookieName = $this->getCouponCookieNameForReferenceLink($widgetReferenceLink);
        $cookieCouponId = $request->cookies->getInt($cookieName);
        if ($cookieCouponId) {
            $coupon = ($isReferrerCoupon) ? $referrerCouponRepository->find($cookieCouponId) : $couponRepository->find($cookieCouponId);
            if ($coupon) {
                $skipPhoneCode = true;
            }
        }

        if ($widgetReferenceLink->getUser()->hasActiveReferrerCoupon($widgetReferenceLink->getShop(), $session->get('redirectedFrom', ReferrerCoupon::TYPE_LINKS))) {
            return $this->render('widget/share-referrer.html.twig', [
                'skipPhoneCode' => $skipPhoneCode,
                'url' => $url,
                'user' => $widgetReferenceLink->getUser(),
                'widgetReferenceLink' => $widgetReferenceLink,
                'shop' => $widgetReferenceLink->getShop(),
                'translations' => $translations,
                'referrerCoupon' => $widgetReferenceLink->getUser()->getReferrerCouponByShop($widgetReferenceLink->getShop())
            ]);
        }

/*      if (!$activeCampaign || !$referrerLink || ($referrerLink->getCampaign()->getId() !== $activeCampaign->getId())) {
            return $this->redirectToRoute('home_index');
        }

        if (!$activeCampaign || !$referrerLink || ($referrerLink->getCampaign()->getId() !== $activeCampaign->getId())) {
            return $this->redirectToRoute('home_index');
        }*/

        if ($widgetReferenceLink->getUser()->hasActiveReferrerCoupon($widgetReferenceLink->getShop(), $session->get('redirectedFrom', ReferrerCoupon::TYPE_LINKS))) {
            return $this->render('widget/share.html.twig', [
                'skipPhoneCode' => $skipPhoneCode,
                'shop' => $shop,
                'campaign' => $activeCampaign,
                'user' => $widgetReferenceLink->getUser(),
                'fingerprints' => $widgetReferenceLink->getBrowserFingerprints(),
                'widgetReferenceLink' => $widgetReferenceLink,
                'referrerCoupon' => $widgetReferenceLink->getUser()->getReferrerCouponByShop($shop),
                'selfReferring' => $request->cookies->has('widgetReferrerLink' . $widgetReferenceLink->getId()),
                'selfReferringKey' => 'widgetReferrerLink' . $widgetReferenceLink->getId(),
                'disableSnippet' => true,
                'url' => $url,
                'translations' => $translations,
            ]);
        }

        if (null === $couponPackage = $activeCampaign->getWidgetShopSetting()->getInviteeRewardCouponPackage()) {
            return $this->redirectToRoute('home_index');
        }

        if ($settings->linkExpired($widgetReferenceLink)) {
            return $this->redirectToRoute('home_index');
        }

        return $this->render('widget/share.html.twig', [
            'skipPhoneCode' => $skipPhoneCode,
            'shop' => $widgetReferenceLink->getShop(),
            'campaign' => $activeCampaign,
            'user' => $widgetReferenceLink->getUser(),
            'fingerprints' => $widgetReferenceLink->getBrowserFingerprints(),
            'widgetReferenceLink' => $widgetReferenceLink,
            'couponPackage' => $couponPackage,
            'selfReferring' => $request->cookies->has('widgetReferrerLink' . $widgetReferenceLink->getId()),
            'selfReferringKey' => 'widgetReferrerLink' . $widgetReferenceLink->getId(),
            'disableSnippet' => true,
            'linkExpiresAt' => $settings->getLinkExpiresAt($widgetReferenceLink),
            'url' => $url,
            'translations' => $translations,
        ]);
    }

    private function getTranslations(string $lang)
    {
        return Yaml::parseFile(__DIR__ . '/../../../translations/widget.' . $lang . '.yaml');
    }

    /**
     * @Route("/{slug}/coupon", name="widget_share_coupon", methods={"POST"})

     * @param WidgetReferenceLink $widgetReferenceLink
     * @param ReferrerCouponRepository $referrerCouponRepository
     * @param CouponRepository $couponRepository
     * @param UseCoupon $useCoupon
     * @param SessionInterface $session
     * @return JsonResponse
     */
    public function getCoupon(
        WidgetReferenceLink $widgetReferenceLink,
        ReferrerCouponRepository $referrerCouponRepository,
        CouponRepository $couponRepository,
        UseCoupon $useCoupon,
        SessionInterface $session,
        Request $request
    )
    {
        if ($request->cookies->has('widgetReferrerLink' . $widgetReferenceLink->getId())) {
            return $this->couponNotFoundJsonResponse('Self referring error');
        }

        $isReferrerCoupon = ($widgetReferenceLink->getUser()->hasActiveReferrerCoupon($widgetReferenceLink->getShop(), $session->get('redirectedFrom', ReferrerCoupon::TYPE_LINKS)));
        $cookieName = $this->getCouponCookieNameForReferenceLink($widgetReferenceLink);
        $cookieCouponId = $request->cookies->getInt($cookieName);
        if ($cookieCouponId) {
            $coupon = ($isReferrerCoupon) ? $referrerCouponRepository->find($cookieCouponId) : $couponRepository->find($cookieCouponId);
            if ($coupon) {
                return new JsonResponse(['coupon' => $coupon->getCode()]);
            }
        }
        $activeCampaign = $widgetReferenceLink->getShop()->getCampaigns();
        $activeCampaign = ($activeCampaign) ? $activeCampaign[0] : $activeCampaign;

        if (null === $settings = $activeCampaign->getWidgetShopSetting()->getInviteeRewardCouponPackage()) {
            return $this->couponNotFoundJsonResponse();
        }

        /*if (null === $settings = $widgetReferenceLink->getReferrerLink()->getCampaign()->getWidgetShopSetting()) {
            return $this->couponNotFoundJsonResponse();
        }*/

        /*if ($isReferrerCoupon) {
            $coupon = $widgetReferenceLink->getUser()->getReferrerCouponByShop($widgetReferenceLink->getShop());
        } else {
            $couponPackage = $settings->getInviteeRewardCouponPackage();
            $coupon = $couponRepository->findFreeCouponInCouponPackage($couponPackage);
        }*/

        if ($isReferrerCoupon) {
            $coupon = $widgetReferenceLink->getUser()->getReferrerCouponByShop($widgetReferenceLink->getShop());
        } else {
            $couponPackage = $activeCampaign->getWidgetShopSetting()->getInviteeRewardCouponPackage();
            $coupon = $couponRepository->findFreeCouponInCouponPackage($couponPackage);
        }

        if (null === $coupon) {
            return $this->couponNotFoundJsonResponse();
        }

        if(get_class($coupon) === 'App\Entity\ReferrerCoupon') {
            $response = new JsonResponse(['coupon' => $coupon->getCode()]);

            $response->headers->setCookie(
                new Cookie($cookieName, $coupon->getId(), time() + (86400 * $this->getParameter('coupons_lifetime_days')))
            );

            return $response;
        }

        try {
            $useCoupon($coupon, $widgetReferenceLink->getUser());
        } catch (\LogicException $exception) {
            return $this->couponNotFoundJsonResponse($exception->getMessage());
        }

        $response = new JsonResponse(['coupon' => $coupon->getCode()]);

        $response->headers->setCookie(
            new Cookie($cookieName, $coupon->getId(), time() + (86400 * $this->getParameter('coupons_lifetime_days')))
        );

        return $response;
    }

    private function couponNotFoundJsonResponse(string $message = 'Coupons temporary not available.'): JsonResponse
    {
        return new JsonResponse(['message' => $message], Response::HTTP_NOT_FOUND);
    }

    private function getCouponCookieNameForReferenceLink(WidgetReferenceLink $widgetReferenceLink)
    {
        return sprintf('shareCouponValue_%s', $widgetReferenceLink->getId());
    }
}