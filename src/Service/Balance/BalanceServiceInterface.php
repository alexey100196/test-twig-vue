<?php


namespace App\Service\Balance;


use App\Entity\Currency;
use App\Entity\TransactionType;
use App\Service\Balance\Entity\HasBalanceEntity;
use Ramsey\Uuid\UuidInterface;

interface BalanceServiceInterface
{
    public function addTransaction(
        UuidInterface $uuid,
        HasBalanceEntity $user,
        Currency $currency,
        string $amount,
        TransactionType $type
    );

    public function findTransactionByUuid(UuidInterface $uuid);
}