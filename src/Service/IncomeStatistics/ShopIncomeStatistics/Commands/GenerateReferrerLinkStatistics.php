<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Commands;


use App\Entity\ReferrerLink;
use App\Service\IncomeStatistics\CommandInterface;

class GenerateReferrerLinkStatistics implements CommandInterface
{
    /**
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @var ReferrerLink
     */
    protected $referrerLink;


    public function __construct(ReferrerLink $referrerLink, \DateTime $startDate, \DateTime $endDate)
    {
        $this->referrerLink = $referrerLink;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    /**
     * @return ReferrerLink
     */
    public function getReferrerLink(): ReferrerLink
    {
        return $this->referrerLink;
    }
}