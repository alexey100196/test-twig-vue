<?php

namespace App\Service\DocumentReferrer\ValueObject;

use App\Helper\UrlHelpers;

class ReferrerLink
{
    /** @var string */
    private $url;

    /** @var array|mixed */
    private $urlParts = [];

    public function __construct(string $url)
    {
        // Fixing schema.
        if (UrlHelpers::hasScheme($url) === false) {
            $url = 'http://' . ltrim($url, '/');
        }

        // Validate.
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            throw new \Exception('Invalid url: ' . $url);
        }

        // Build.
        $this->url = $url;
        $this->urlParts = parse_url($url);
    }

    public function getBaseUrl()
    {
        return $this->getScheme() . '://' . $this->getHost();
    }

    public function getScheme(): string
    {
        return $this->urlParts['scheme'];
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getHost(): ?string
    {
        if (null !== $this->urlParts['host']) {
            return rtrim($this->urlParts['host'], '/');
        }

        return null;
    }

}