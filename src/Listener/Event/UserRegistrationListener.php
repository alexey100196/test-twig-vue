<?php

namespace App\Listener\Event;

use App\Adapter\MailSender;
use App\Event\User\UserRegistered;
use App\Repository\UserRepository;
use App\Service\User\EmailConfirmation;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;


class UserRegistrationListener
{
    /**
     * @var MailSender
     */
    private $mailSender;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var EmailConfirmation
     */
    private $emailConfirmation;

    /**
     * UserRegistrationListener constructor.
     * @param \Twig_Environment $twig
     * @param MailSender $mailSender
     * @param EmailConfirmation $emailConfirmation
     */
    public function __construct(\Twig_Environment $twig, MailSender $mailSender, EmailConfirmation $emailConfirmation)
    {
        $this->twig = $twig;
        $this->mailSender = $mailSender;
    }

    /**
     * Send welcome message to user.
     *
     * @param UserRegistered $userRegistered
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendWelcomeEmailToRegisteredUser(UserRegistered $userRegistered)
    {
        $this->mailSender->send(
            'hello@superinterface.com',
            $userRegistered->getUser()->getEmail(),
            'Welcome to SuperInterface!',
            $this->twig->render('email/welcome.html.twig')
        );
    }
}
