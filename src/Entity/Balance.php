<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\InheritanceType;
use Symfony\Component\Validator\Constraints\Collection;

/**
 * @ORM\InheritanceType("JOINED")
 * @ORM\Entity(repositoryClass="App\Repository\BalanceRepository")
 */
class Balance
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $currency;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    protected $amount;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transaction", mappedBy="balance")
     */
    protected $transactions;

    public function __construct()
    {
        $this->amount = 0;
        $this->transactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * @return ArrayCollection
     */
    public function getNegativeTransactions()
    {
        return $this->getTransactions()->filter(
            function ($transaction) {
                return $transaction->getAmount() < 0;
            }
        );
    }

    public function getNegativeTransactionsSum()
    {
        $sum = 0;
        foreach ($this->getNegativeTransactions() as $transaction) {
            $sum += $transaction->getAmount();
        }
        return abs($sum);
    }

    // To Review Balance Update !
    public function updateBalance()
    {
        $sum = 0;
        foreach ($this->transactions as $transaction) {
            $sum += $transaction->getAmount();
        }
        $this->setAmount($sum);
    }

   public function __toString()
    {
        return $this->getAmount() . ' ' . $this->getCurrency()->getName();
    }
}
