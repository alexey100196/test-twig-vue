<?php


namespace App\Service\Referrer;


use App\Entity\Media;
use App\Entity\ReferrerLink;
use App\Entity\ReferrerLinkCard;
use App\Repository\ReferrerLinkCardRepository;
use Doctrine\ORM\EntityManagerInterface;

class ReferrerLinkCardService
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var ReferrerLinkCardRepository */
    private $referrerLinkCardRepository;

    public function __construct(
        EntityManagerInterface $em,
        ReferrerLinkCardRepository $referrerLinkCardRepository
    )
    {
        $this->em = $em;
        $this->referrerLinkCardRepository = $referrerLinkCardRepository;

    }

    /**
     * @param ReferrerLinkCard $card
     * @throws \Exception
     */
    public function save(ReferrerLinkCard $card)
    {
        $this->em->persist($card);
        $this->em->flush();
    }

    /**
     * @param ReferrerLinkCard|null $referrerLinkCard
     * @return array
     */
    public function convertObjectToArray(?ReferrerLinkCard $referrerLinkCard) {
        return $referrerLinkCard ? [
            'width' => $referrerLinkCard->getWidth(),
            'socialMedia' => $referrerLinkCard->getSocialMedia(),
            'name' => $referrerLinkCard->getName(),
            'url' => $referrerLinkCard->getUrl(),
            'logo' => $referrerLinkCard->getLogo(),
            'title' => $referrerLinkCard->getTitle(),
            'subtitle' => $referrerLinkCard->getSubtitle(),
            'description' => $referrerLinkCard->getDescription(),
            'image' => $referrerLinkCard->getImage(),
            'iframe' => $referrerLinkCard->getIframe(),
            'layout' => $referrerLinkCard->getLayout(),
            'showShopLogo' => $referrerLinkCard->getShowShopLogo(),
            'showCompanyName' => $referrerLinkCard->getShowCompanyName(),
            'show3DEffect' => $referrerLinkCard->getShow3DEffect(),
            'showVideo' => $referrerLinkCard->getShowVideo()
        ] : [];
    }
}