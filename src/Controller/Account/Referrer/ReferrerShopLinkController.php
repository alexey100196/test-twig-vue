<?php


namespace App\Controller\Account\Referrer;


use App\Entity\ReferrerCoupon;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\User;
use App\Form\Referrer\ReferrerCouponType;
use App\Form\Referrer\ReferrerLinkType;
use App\Repository\ReferrerLinkCardRepository;
use App\Repository\UserShopRepository;
use App\Service\Referrer\ReferrerShop;
use App\Service\Referrer\ReferrerShopLink;
use App\Service\Referrer\RegenerateReferrerLinkCardContent;
use App\Traits\Controller\FormResponseArray;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class ReferrerShopLinkController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerShopLinkController extends AbstractController
{
    use FormResponseArray;

    /**
     * @var UserShopRepository
     */
    private $userShopRepository;

    /**
     * @var ReferrerShopLink
     */
    private $referrerShopLink;

    /**
     * @var ReferrerShop
     */
    private $referrerShop;

    /**
     * ReferrerShopController constructor.
     * @param ReferrerShopLink $referrerShopLink
     * @param UserShopRepository $userShopRepository
     * @param ReferrerShop $referrerShop
     */
    public function __construct(
        ReferrerShopLink $referrerShopLink,
        UserShopRepository $userShopRepository,
        ReferrerShop $referrerShop
    )
    {
        $this->userShopRepository = $userShopRepository;
        $this->referrerShopLink = $referrerShopLink;
        $this->referrerShop = $referrerShop;
    }

    /**
     * @Route("/account/referrer/company/{shop}", name="account_referrer_shop_link_home")
     * @param Shop $shop
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function home(Shop $shop, Request $request)
    {
        $userShop = $this->referrerShop->findReferrerShopWithStats($this->getUser(), $shop);
        $referrerCouponForm = $this->createForm(ReferrerCouponType::class, $this->getUser()->getOrCreateReferrerCoupon($shop), [
            'action' => $this->generateUrl('account_referrer_shop_link_referrer_coupon_save', ['shop' => $shop->getId()])
        ]);

        $referrerCouponForm->handleRequest($request);

        if (!$userShop) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        return $this->render('account/referrer/shop/home-link.html.twig', [
            'item' => $userShop,
            'referrerCouponForm' => $referrerCouponForm,
        ]);
    }

    /**
     * @Route("/account/referrer/company/{shop}/link", name="account_referrer_shop_link_show")
     * @param Shop $shop
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(Shop $shop, Request $request)
    {
        $referrerCouponForm = $this->createForm(ReferrerCouponType::class, $this->getUser()->getOrCreateReferrerCoupon($shop), [
            'action' => $this->generateUrl('account_referrer_shop_link_referrer_coupon_save', ['shop' => $shop->getId()])
        ]);

        $referrerCouponForm->handleRequest($request);

        return $this->renderLinkPage($shop, $referrerCouponForm, $request, $referrerCouponForm->isSubmitted());
    }

    private function renderLinkPage(Shop $shop, $referrerCouponForm, Request $request, bool $createCouponFormSubmitted = false)
    {
        $userShop = $this->referrerShop->findReferrerShopWithStats($this->getUser(), $shop);

        if (!$userShop) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        $referrerLinks = $this->referrerShopLink->findReferrerShopLinks($shop, $this->getUser(), false);
        $createForm = $this->getCreateReferrerLinkForm($shop, $request);

        return $this->render('account/referrer/shop/show-link.html.twig', [
            'item' => $userShop,
            'createForm' => $createForm->createView(),
            'referrerLinks' => $referrerLinks,
            'referrerCouponForm' => $referrerCouponForm->createView(),
            'createCouponFormSubmitted' => $createCouponFormSubmitted,
        ]);
    }

    /**
     * @Route("/account/referrer/company/{shop}/link/referrer-coupon", name="account_referrer_shop_link_referrer_coupon_save")
     * @param Shop $shop
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param RegenerateReferrerLinkCardContent $regenerateReferrerLinkCardContent
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function saveReferrerCoupon(
        Shop $shop,
        Request $request,
        EntityManagerInterface $entityManager,
        RegenerateReferrerLinkCardContent $regenerateReferrerLinkCardContent
    )
    {
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createForm(ReferrerCouponType::class, $user->getOrCreateReferrerCoupon($shop));
        $form->handleRequest($request);

        if (($isSubmitted = $form->isSubmitted()) && $form->isValid()) {
            $entityManager->persist($form->getData());
            $entityManager->flush();
        }
        return $this->renderLinkPage($shop, $form, $request, $isSubmitted);
    }

    /**
     * @Route("/account/referrer/company/{shop}/referrer-link", name="account_referrer_shop_referrer_link_store")
     * @param Shop $shop
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function storeReferrerLink(Shop $shop, Request $request)
    {
        $form = $this->getCreateReferrerLinkForm($shop, $request);

        if ($form->isSubmitted() && $form->isValid() && $this->referrerShopLink->canUserCreateShopLink($this->getUser(), $shop)) {
            $this->referrerShopLink->create($form->getData());
            $this->addFlash('success', 'Link created.');
        } else {
            $this->createFlashMessagesFromFormErrors($form);
        }

        return $this->redirectToRoute('account_referrer_shop_link_show', ['shop' => $shop->getId()]);
    }

    /**
     * @Route("/account/referrer/company/{shop}/referrer-link/{referrerLink}", name="account_referrer_shop_referrer_link_update", methods={"PUT"})
     * @param Shop $shop
     * @param ReferrerLink $referrerLink
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateReferrerLink(Shop $shop, ReferrerLink $referrerLink, Request $request)
    {
        if (!$this->getUser()->hasReferrerLink($referrerLink)) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(ReferrerLinkType::class, $referrerLink, [
            'method' => 'PUT',
            'shop' => $shop,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->referrerShopLink->save($form->getData());
            $this->addFlash('success', 'Link updated.');
        } else {
            $this->createFlashMessagesFromFormErrors($form);
        }

        return $this->redirectToRoute('account_referrer_shop_link_show', ['shop' => $shop->getId()]);
    }

    /**
     * @Route("/account/referrer/company/{shop}/referrer-link/{referrerLink}", name="account_referrer_shop_referrer_link_delete", methods={"DELETE"})
     * @param Shop $shop
     * @param ReferrerLink $referrerLink
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteReferrerLink(Shop $shop, ReferrerLink $referrerLink)
    {
        // @todo: check if can remove - if have campaigns etc.

        if (!$this->referrerShopLink->remove($this->getUser(), $referrerLink)) {
            throw new NotFoundHttpException();
        }

        return $this->redirectToRoute('account_referrer_shop_link_show', ['shop' => $shop->getId()]);
    }

    /**
     * @param Shop $shop
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getCreateReferrerLinkForm(Shop $shop, Request $request)
    {
        $createForm = $this->createForm(ReferrerLinkType::class, $this->getUser()->getNewReferrerLink($shop), [
            'shop' => $shop
        ]);
        $createForm->handleRequest($request);

        return $createForm;
    }
}