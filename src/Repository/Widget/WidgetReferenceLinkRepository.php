<?php

namespace App\Repository\Widget;

use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\User;
use App\Entity\Widget\WidgetReferenceLink;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WidgetReferenceLink|null find($id, $lockMode = null, $lockVersion = null)
 * @method WidgetReferenceLink|null findOneBy(array $criteria, array $orderBy = null)
 * @method WidgetReferenceLink[]    findAll()
 * @method WidgetReferenceLink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WidgetReferenceLinkRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WidgetReferenceLink::class);
    }

    /**
     * @param Shop $shop
     * @param User $user
     * @param int $type
     * @param string $destinationUrl
     * @return mixed|null
     */
    public function findOneForShopAndUserAndDestinationUrl(Shop $shop, User $user, int $type, string $destinationUrl):?WidgetReferenceLink
    {
        try {
            return $this->createQueryBuilder('w')
                ->join(ReferrerLink::class, 'r', Join::WITH, 'w.referrerLink = r')
                ->andWhere('w.shop = :shop')
                ->andWhere('w.user = :user')
                ->andWhere('w.type = :type')
                ->andWhere('r.destinationUrl = :destinationUrl')

                ->setParameter('shop', $shop)
                ->setParameter('user', $user)
                ->setParameter('type', $type)
                ->setParameter('destinationUrl', $destinationUrl)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
