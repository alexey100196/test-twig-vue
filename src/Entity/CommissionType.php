<?php

namespace App\Entity;

use App\Entity\BaseGlossary;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommissionTypeRepository")
 */
class CommissionType extends BaseGlossary
{
    const PERCENT = 'percent';
    const FIXED = 'fixed';

    const PERCENT_ID = 1;
    const FIXED_ID = 2;

    /**
     * @ORM\Column(type="string", length=128, unique=true)
     * @Groups({"shop"})

     */
    protected $name;
}
