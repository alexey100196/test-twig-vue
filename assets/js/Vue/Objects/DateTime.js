var moment = require('moment');

export function dateTimeFactory (dt = undefined) {

    function DateTime(dt) {
        this.handler = moment(dt);
    }

    DateTime.prototype.endOfMonth = function () {
        this.handler.endOf('month');
        return this;
    };

    DateTime.prototype.startOfMonth = function () {
        this.handler.startOf('month');
        return this;
    };

    DateTime.prototype.toDateTimeString = function () {
        return this.handler.format('YYYY-MM-DD HH:mm:ss')
    };

    DateTime.prototype.addMonths = function (months) {
        this.handler.add(months, 'M');
        return this;
    };

    DateTime.prototype.subMonths = function (months) {
        this.handler.subtract(months, 'M');
        return this;
    };

    DateTime.prototype.addDays = function (days) {
        this.handler.add(days, 'days');
        return this;
    };

    DateTime.prototype.subDays = function (days) {
        this.handler.subtract(days, 'days');
        return this;
    };

    DateTime.prototype.diff = function (dt, unit) {
        return this.handler.diff(dt, unit);
    };

    DateTime.prototype.format = function (format) {
        return this.handler.format(format);
    };

    DateTime.prototype.year = function () {
        return this.handler.format('YYYY');
    };

    DateTime.prototype.month = function () {
        return this.handler.format('MM');
    };

    DateTime.prototype.monthName = function () {
        return this.handler.format('MMMM');
    };

    return new DateTime(dt);
}