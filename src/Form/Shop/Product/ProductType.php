<?php

namespace App\Form\Shop\Product;

use App\Entity\Offer;
use App\Entity\Product;
use App\Form\Shop\CplOfferType;
use App\Validator\Constraints\ProductOffer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['attr' => ['placeholder' => 'Soccer Jerseys']])
            ->add('description', TextareaType::class)
            ->add('cpsOffer', CplOfferType::class, [
                'data_class' => Offer::class,
            ])
            ->add('website', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
            'constraints' => [
                new ProductOffer(),
                new UniqueEntity([
                    'fields' => ['shop', 'number'],
                    'repositoryMethod' => 'findBy',
                    'message' => 'Given number is taken.',
                ]),
            ],
        ]);
    }
}