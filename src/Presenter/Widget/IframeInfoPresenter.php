<?php

namespace App\Presenter\Widget;

use App\Entity\Coupon\CouponPackage;
use App\Entity\Shop;
use App\Service\Coupon\GetValidCouponCode;

class IframeInfoPresenter
{
    /**
     * @var GetValidCouponCode
     */
    private $getValidCouponCode;

    /**
     * @var Shop
     */
    private $shop;

    public function __construct(Shop $shop, GetValidCouponCode $getValidCouponCode)
    {
        $this->shop = $shop;
        $this->getValidCouponCode = $getValidCouponCode;
        if (
                       null === $this->shop->getWidgetSetting(true) ||
                       null === $this->shop->getLogo() ||
                       null === $this->shop->getWidgetSetting(true)->getInviteeRewardCouponPackage()
        ) {
            throw new \InvalidArgumentException('Shop is not ready.');
        }
    }

    public function toArray()
    {
        $widgetSettings = $this->shop->getWidgetSetting(true);
        try {
            $inviteeCoupon = ($this->getValidCouponCode)($this->shop, CouponPackage::ALLOCATION_INVITEE);
        } catch (\LogicException $e) {
            $inviteeCoupon = null;
        }

        try {
            $referrerCoupon = ($this->getValidCouponCode)($this->shop, CouponPackage::ALLOCATION_REFERRER);
        } catch (\LogicException $e) {
            $referrerCoupon = null;
        }

        $result = [
            'company_name' => $this->shop->getName(),
            'logo_url' => $this->shop->getLogo(),
            'lead' => $this->shop->getDescription(),
            'customer_text' => $widgetSettings->getCustomText(),
            'customer_bar_text' => $widgetSettings->getCustomBarText(),
            'welcome_image' => [
                'url' => $widgetSettings->getWelcomePicture() ? $widgetSettings->getWelcomePicture()->getPublicUrl() : 'http://marfil-app.local/images/welcomeWidgetHeader.png',
                'width' => $widgetSettings->getWelcomePictureWidth(),
                'height' => $widgetSettings->getWelcomePictureHeight(),
                'pos_x' => $widgetSettings->getWelcomePicturePosX(),
                'pos_y' => $widgetSettings->getWelcomePicturePosY(),
                'ration' => $widgetSettings->getWelcomePictureRatio()
            ],
            'referrer_cash_reward' => [
                'enabled' => $widgetSettings->getReferrerRewardCashEnabled(),
                'reward_type' => (string)$this->shop->getCpsOffer()->getCommissionType(),
                'reward_value' => (string)$this->shop->getCpsOffer()->getCommission(),
            ],
            'referrer_coupon_reward' => [],
            'invitee_reward' => [],

            'coupon_reward_enabled' => null !== $inviteeCoupon && null !== $referrerCoupon && $widgetSettings->getReferrerRewardCouponEnabled(),
            'cash_reward_enabled' => null !== $inviteeCoupon && $widgetSettings->getReferrerRewardCashEnabled() && $this->shop->hasOwnerBalance(),
/*            'cash_reward_enabled' => true,*/
            'language' => $widgetSettings->getLanguage() ? $widgetSettings->getLanguage()->getCode() : 'en'
        ];
//        dump($widgetSettings->getReferrerRewardCouponPackage());exit;
        if ($widgetSettings->getReferrerRewardCouponPackage()) {
            $result['referrer_coupon_reward'] = [
                'enabled' => $widgetSettings->getReferrerRewardCouponEnabled(),
                'reward_type' => (string)$widgetSettings->getReferrerRewardCouponPackage()->getDiscountType(),
                'reward_value' => (string)$widgetSettings->getReferrerRewardCouponPackage()->getDiscount(),
            ];
        }
        if ($widgetSettings->getInviteeRewardCouponPackage()) {
            $result['invitee_reward'] = [
                'reward_type' => (string)$widgetSettings->getInviteeRewardCouponPackage()->getDiscountType(),
                'reward_value' => (string)$widgetSettings->getInviteeRewardCouponPackage()->getDiscount(),
            ];
        }

        return $result;
    }
}