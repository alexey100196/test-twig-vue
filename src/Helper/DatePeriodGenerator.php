<?php


namespace App\Helper;


use App\ValueObject\DatePeriod;
use Carbon\Carbon;

class DatePeriodGenerator
{
    /**
     * @param int|null $year
     * @param int|null $month
     * @return DatePeriod
     */
    public static function generateFromYearAndMonth(int $year = null, int $month = null): DatePeriod
    {
        $year = $year ? intval($year) : date('Y');
        $month = $month ? intval($month) : date('m');

        return new DatePeriod(
            Carbon::create($year, $month)->startOfMonth(),
            Carbon::create($year, $month)->endOfMonth()
        );
    }
}