<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

abstract class BaseGlossary
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=128, unique=true)
     */
    protected $name;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId():  ? int
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param int $id
     * @return BaseGlossary
     */
    public function setId(int $id) : BaseGlossary
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BaseGlossary
     */
    public function setName(string $name): BaseGlossary
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param $type
     * @return bool
     */
    public function is($type): bool
    {
        return is_string($type) ? $this->getName() == $type : $this->getId() == $type;
    }

}
