<?php


namespace App\Service;


use App\Entity\WhatsappCampaignVerify;
use App\Repository\WhatsappCampaignVerifyRepository;
use Doctrine\ORM\EntityManagerInterface;

class WhatsappCampaignVerifyService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var WhatsappCampaignVerifyRepository
     */
    private $whatsappCampaignVerifyRepository;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     * @param WhatsappCampaignVerifyRepository $whatsappCampaignVerifyRepository
     */
    public function __construct(EntityManagerInterface $em, WhatsappCampaignVerifyRepository $whatsappCampaignVerifyRepository)
    {
        $this->em = $em;
        $this->whatsappCampaignVerifyRepository = $whatsappCampaignVerifyRepository;
    }

    /**
     * @param WhatsappCampaignVerify $whatsappCampaignVerify
     * @return WhatsappCampaignVerify
     */
    public function save(WhatsappCampaignVerify $whatsappCampaignVerify)
    {
        $this->em->persist($whatsappCampaignVerify);
        $this->em->flush();

        return $whatsappCampaignVerify;
    }

    /**
     * @param WhatsappCampaignVerify $whatsappCampaignVerify
     * @return WhatsappCampaignVerify
     */
    public function generate(WhatsappCampaignVerify $whatsappCampaignVerify)
    {
        do {
            $randNum = mt_rand(100000,999999);
        } while ($this->whatsappCampaignVerifyRepository->findOneBy([
            'code' => $randNum
        ]) !== null);
        $whatsappCampaignVerify->setCode($randNum);
        $whatsappCampaignVerify->setVerifiedAt(null);
        $this->save($whatsappCampaignVerify);

        return $whatsappCampaignVerify;
    }

    /**
     * @param WhatsappCampaignVerify $whatsappCampaignVerify
     * @return WhatsappCampaignVerify
     */
    public function regenerate(WhatsappCampaignVerify $whatsappCampaignVerify)
    {
        do {
            $randNum = mt_rand(100000,999999);
        } while ($this->whatsappCampaignVerifyRepository->findOneBy([
            'code' => $randNum
        ]) !== null);
        $whatsappCampaignVerify->setCode($randNum);
        $whatsappCampaignVerify->setVerifiedAt(null);
        $this->save($whatsappCampaignVerify);

        return $whatsappCampaignVerify;
    }

    /**
     * @param WhatsappCampaignVerify $whatsappCampaignVerify
     */
    public function remove(WhatsappCampaignVerify $whatsappCampaignVerify)
    {
        $this->em->remove($whatsappCampaignVerify);
        $this->em->flush();
    }

    /**
     * @param WhatsappCampaignVerify $whatsappCampaignVerify
     * @return WhatsappCampaignVerify
     */
    public function verify(WhatsappCampaignVerify $whatsappCampaignVerify)
    {
        if (!$whatsappCampaignVerify->getVerifiedAt()) {
            $whatsappCampaignVerify->setVerifiedAt(new \DateTime());
            $whatsappCampaignVerify->setCode(null);
            $this->em->persist($whatsappCampaignVerify);
            $this->em->flush();
        }

        return $whatsappCampaignVerify;
    }

    /**
     * @param WhatsappCampaignVerify $whatsappCampaignVerify
     * @return bool|null
     */
    public function isNotExpired(WhatsappCampaignVerify $whatsappCampaignVerify)
    {
        if ($whatsappCampaignVerify->getVerifiedAt() || !$whatsappCampaignVerify->getCode()) {
            return null;
        }

        $now = new \DateTime();
        $updatedAt = $whatsappCampaignVerify->getUpdatedAt();

        if ($updatedAt && ( ($now->getTimestamp() - $updatedAt->getTimestamp()) <= 3600)) {
            return true;
        }

        return false;
    }

    /**
     * @param WhatsappCampaignVerify $whatsappCampaignVerify
     */
    public function unverify(WhatsappCampaignVerify $whatsappCampaignVerify)
    {
        if ($whatsappCampaignVerify->getVerifiedAt()) {
            $whatsappCampaignVerify->setVerifiedAt(null);
            $this->em->persist($whatsappCampaignVerify);
            $this->em->flush();
        }
    }
}