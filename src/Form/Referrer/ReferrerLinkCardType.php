<?php


namespace App\Form\Referrer;


use App\Entity\ReferrerLink;
use App\Entity\ReferrerLinkCard;
use App\Form\Referrer\Parts\ReferrerLinkCard\CardDimensionType;
use App\Form\Referrer\Parts\ReferrerLinkCard\FontType;
use App\Form\Referrer\Parts\ReferrerLinkCard\RatioType;
use App\Form\Referrer\Parts\ReferrerLinkCard\SloganSizeType;
use App\Form\Referrer\Parts\ReferrerLinkCard\SloganType;
use App\Form\Type\PositionType;
use App\ValueObject\Font;
use PHPUnit\Framework\Constraint\GreaterThan;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class ReferrerLinkCardType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('width', CardDimensionType::class)
            ->add('socialMedia', ChoiceType::class, [
                'choices' => ['Yes' => 1, 'No' => 0],
                'expanded' => true,
            ])
            ->add('showShopLogo', ChoiceType::class, [
                'choices' => ['Yes' => 1, 'No' => 0],
                'expanded' => true,
            ])
            ->add('showCompanyName', ChoiceType::class, [
                'choices' => ['Yes' => 1, 'No' => 0],
                'expanded' => true,
            ])
            ->add('show3DEffect', ChoiceType::class, [
                'choices' => ['Yes' => 1, 'No' => 0],
                'expanded' => true,
            ])
            ->add('showVideo', ChoiceType::class, [
                'choices' => ['Yes' => 1, 'No' => 0],
                'expanded' => true,
            ])
            ->add('name', SloganType::class, [
                'label' => 'Company name',
            ])
            ->add('url', SloganType::class, [
                'label' => 'Company url',
            ])
            ->add('logo', SloganType::class, [
                'label' => 'Company logo',
            ])
            ->add('title', SloganType::class, [
                'label' => 'Title',
            ])
            ->add('description', SloganType::class, [
                'label' => 'Description',
            ])
            ->add('image', SloganType::class, [])
            ->add('iframe', SloganType::class, [])
            ->add('layout', ChoiceType::class, [
                'choices' => ['Column' => 'col', 'Line' => 'line'],
                'expanded' => true,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReferrerLinkCard::class,
            'intention' => 'referrer_link_card',
        ]);
    }
}
