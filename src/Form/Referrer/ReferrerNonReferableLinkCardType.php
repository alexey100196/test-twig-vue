<?php


namespace App\Form\Referrer;


use App\Entity\ReferrerNonReferableLinkCard;
use App\Form\Referrer\Parts\ReferrerLinkCard\CardDimensionType;
use App\Form\Referrer\Parts\ReferrerLinkCard\SloganType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class ReferrerNonReferableLinkCardType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('width', CardDimensionType::class)
            ->add('socialMedia', ChoiceType::class, [
                'choices' => ['Yes' => 1, 'No' => 0],
                'expanded' => true,
            ])
            ->add('showShopLogo', ChoiceType::class, [
                'choices' => ['Yes' => 1, 'No' => 0],
                'expanded' => true,
            ])
            ->add('showCompanyName', ChoiceType::class, [
                'choices' => ['Yes' => 1, 'No' => 0],
                'expanded' => true,
            ])
            ->add('show3DEffect', ChoiceType::class, [
                'choices' => ['Yes' => 1, 'No' => 0],
                'expanded' => true,
            ])
            ->add('showVideo', ChoiceType::class, [
                'choices' => ['Yes' => 1, 'No' => 0],
                'expanded' => true,
            ])
            ->add('name', SloganType::class, [
                'label' => 'Company name',
            ])
            ->add('url', SloganType::class, [
                'label' => 'Company url',
            ])
            ->add('logo', TextType::class, [
                'label' => 'Company logo',
            ])
            ->add('title', SloganType::class, [
                'label' => 'Title',
            ])
            ->add('description', SloganType::class, [
                'label' => 'Description',
            ])
            ->add('image', SloganType::class, [])
            ->add('iframe', SloganType::class, [])
            ->add('layout', ChoiceType::class, [
                'choices' => ['Column' => 'col', 'Line' => 'line'],
                'expanded' => true,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReferrerNonReferableLinkCard::class,
            'intention' => 'referrer_non_referable_link_card',
        ]);
    }
}
