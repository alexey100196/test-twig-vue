<?php

namespace App\Repository;

use App\Entity\InviteFriendByEmail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InviteFriendByEmail|null find($id, $lockMode = null, $lockVersion = null)
 * @method InviteFriendByEmail|null findOneBy(array $criteria, array $orderBy = null)
 * @method InviteFriendByEmail[]    findAll()
 * @method InviteFriendByEmail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InviteFriendByEmailRepository extends ServiceEntityRepository
{
    /**
     * InviteFriendByEmailRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InviteFriendByEmail::class);
    }
}
