<?php

namespace App\Listener\Event;

use App\Adapter\MailSender;
use App\Event\Coupon\CouponUsed;

class CouponUsedListener
{
    /**
     * @var MailSender
     */
    private $mailSender;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @param MailSender $mailSender
     * @param \Twig_Environment $twig
     */
    public function __construct(MailSender $mailSender, \Twig_Environment $twig)
    {
        $this->mailSender = $mailSender;
        $this->twig = $twig;
    }

    public function notifyCouponProvider(CouponUsed $event)
    {
        $coupon = $event->getCoupon();
        $shop = $coupon->getPackage()->getShop();

        if (!$coupon->getPackage()->hasAvailableCoupons()) {
            $this->mailSender->send(
                'hello@superinterface.com',
                $shop->getUser()->getEmail(),
                'Coupons are over for ' . $coupon->getPackage()->getAllocationName(),
                $this->twig->render('email/shop/widget-coupons-over.html.twig', ['coupon' => $coupon])
            );
        }
    }
}