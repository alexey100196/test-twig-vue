<?php

namespace App\Repository;

use App\Entity\Campaign;
use App\Entity\Shop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Campaign|null find($id, $lockMode = null, $lockVersion = null)
 * @method Campaign|null findOneBy(array $criteria, array $orderBy = null)
 * @method Campaign[]    findAll()
 * @method Campaign[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CampaignRepository extends ServiceEntityRepository
{
    /** @var EntityManagerInterface $em */
    private $em;

    public function __construct(RegistryInterface $registry, EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        parent::__construct($registry, Campaign::class);
    }


    /**
     * @param Shop $shop
     * @return Campaign[] Returns an array of Campaign objects
     */
    public function findActiveByShop(Shop $shop)
    {
        return $this->createQueryBuilder('c')
            ->where('c.activatedAt IS NOT NULL')
            ->andWhere('c.shop = :shop')
            ->setParameter('shop', $shop)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Shop $shop
     * @return Campaign[] Returns an array of Campaign objects
     */
    public function getAllByShop(Shop $shop)
    {
        $this->em->getFilters()->disable('softdeleteable');
        $query = $this->createQueryBuilder('c')
            ->where('c.deletedAt IS NOT NULL OR c.deletedAt IS NULL')
            ->andWhere('c.shop = :shop')
            ->setParameter('shop', $shop)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult();

        $this->em->getFilters()->enable('softdeleteable');
        return (array) $query;
    }

    /**
     * @param Shop $shop
     * @return array|array[]
     */
    public function getAllByShopArray(Shop $shop)
    {
        $result = $this->getAllByShop($shop);

        return array_map(function(Campaign $item) {
            return $item->toArray();
        }, $result);
    }

    /**
     * @param Shop $shop
     * @return Campaign[] Returns an array of Campaign objects
     */
    public function getAllByShopEntity(Shop $shop)
    {
        return $result = $this->getAllByShop($shop);
    }
}
