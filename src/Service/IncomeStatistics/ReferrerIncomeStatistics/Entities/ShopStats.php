<?php


namespace App\Service\IncomeStatistics\ReferrerIncomeStatistics\Entities;


use App\Entity\ReferrerLink;
use App\Helper\Math;

class ShopStats
{
    /**
     * @var ReferrerLink
     */
    private $referrerLink;

    /**
     * @var \DateTime
     */
    private $lastPurchaseDate;

    /**
     * @var \DateTime
     */
    private $lastSignUpDate;

    /**
     * @var \DateTime
     */
    private $lastViewDate;

    /**
     * @var int
     */
    private $purchaseCount;

    /**
     * @var int
     */
    private $signUpCount;

    /**
     * @var int
     */
    private $viewsCount;

    /**
     * @var float
     */
    private $purchaseReferrerProfit;

    /**
     * @var float
     */
    private $signUpReferrerProfit;

    /**
     * @var float
     */
    private $purchaseTotal;

    /**
     * @var float
     */
    private $signUpCommission;

    public function __construct(
        $referrerLink,
        $lastPurchaseDate,
        $lastSignUpDate,
        $lastViewDate,
        int $purchaseCount,
        int $signUpCount,
        int $viewsCount,
        float $purchaseReferrerProfit,
        float $signUpReferrerProfit,
        float $purchaseTotal,
        float $signUpCommission
    )
    {
        $this->referrerLink = $referrerLink;
        $this->lastPurchaseDate = $lastPurchaseDate;
        $this->lastSignUpDate = $lastSignUpDate;
        $this->lastViewDate = $lastViewDate;
        $this->purchaseCount = $purchaseCount;
        $this->signUpCount = $signUpCount;
        $this->viewsCount = $viewsCount;
        $this->purchaseReferrerProfit = $purchaseReferrerProfit;
        $this->signUpReferrerProfit = $signUpReferrerProfit;
        $this->purchaseTotal = $purchaseTotal;
        $this->signUpCommission = $signUpCommission;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [

            'referrer_link' => [
                'id' => $this->referrerLink->getId(),
                'is_deleted' => $this->referrerLink->getDeletedAt() ? true : false,
                'shop_url_name' => $this->referrerLink->getShopUrlName(),
                'slug' => $this->referrerLink->getSlug(),
                'nickname' => $this->referrerLink->getNickname(),
                'destination_url' => (string)$this->referrerLink->getDestinationUrl(),
                'product_name' => $this->referrerLink->getProduct() ? $this->referrerLink->getProduct()->getName() : null,
                'product_number' => $this->referrerLink->getProduct() ? $this->referrerLink->getProduct()->getNumber() : null,
            ],

            'last_purchase_date' => $this->lastPurchaseDate ? $this->lastPurchaseDate->format('Y-m-d H:i:s') : null,
            'last_sign_up_date' => $this->lastSignUpDate ? $this->lastSignUpDate->format('Y-m-d H:i:s') : null,
            'last_view_date' => $this->lastViewDate ? $this->lastViewDate->format('Y-m-d H:i:s') : null,

            'purchase_count' => $this->purchaseCount,
            'sign_up_count' => $this->signUpCount,
            'views_count' => $this->viewsCount,
            'purchase_referrer_profit' => $this->purchaseReferrerProfit,
            'sign_up_referrer_profit' => $this->signUpReferrerProfit,
            'purchase_total' => $this->purchaseTotal,
            'sign_up_commission' => $this->signUpCommission,
            'total_referrer_profit' => (float)Math::add($this->purchaseReferrerProfit, $this->signUpReferrerProfit),
        ];
    }
}