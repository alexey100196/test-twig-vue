<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Handlers;


use App\Entity\DocumentReferrer\DocumentReferrer;
use App\Entity\ShopInteraction;
use App\Service\IncomeStatistics\CommandInterface;
use App\Service\IncomeStatistics\Handler;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateSourceTrafficStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Entities\ShopDocumentReferrerStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Entities\ShopStatistics;
use Doctrine\Common\Collections\ArrayCollection;

class SourceTrafficStatisticsHandler extends Handler
{
    private $default = [
        'https://facebook.com',
        'https://twitter.com',
    ];

    /**
     * @param string $link
     * @return ShopDocumentReferrerStatistics
     */
    private function createEmptyElementForLink(string $link): ShopDocumentReferrerStatistics
    {
        return $this->convertToEntity([
            'documentReferrer' => new DocumentReferrer($link),
            'lastPurchaseDate' => null,
            'lastSignUpDate' => null,
            'purchaseCount' => 0,
            'signUpCount' => 0,
            'viewsCount' => 0,
            'signUpReferrerProfit' => 0,
            'purchaseReferrerProfit' => 0,
            'systemCommission' => 0,
            'purchaseTotal' => 0,
        ]);
    }

    private function collectionContainsDefaultLink(ArrayCollection $data, string $link)
    {
        $filtered = $data->filter(function ($item) use ($link) {
            return $item->documentReferrer->getReferrer() === $link;
        });

        return $filtered->count() > 0;
    }

    /**
     * @param GenerateSourceTrafficStatistics $command
     * @return ArrayCollection
     */
    public function handle(GenerateSourceTrafficStatistics $command)
    {
        $result = $this->doctrine
            ->getRepository(ShopInteraction::class)
            ->getShopDocumentReferrerStats(
                $command->shop,
                $command->startDate,
                $command->endDate,
                $command->productsIds,
                $command->includeMainOffer
            );

        $this->default[] = $this->parameterBag->get('url');
        $this->default[] = 'Others';

        $collection = new ArrayCollection(array_map([$this, 'convertToEntity'], $result));

        foreach ($this->default as $default) {
            if (!$this->collectionContainsDefaultLink($collection, $default)) {
                $collection->add($this->createEmptyElementForLink($default));
            }
        }

        return $collection;
    }

    /**
     * @param array $item
     * @return ShopDocumentReferrerStatistics
     */
    private function convertToEntity(array $item): ShopDocumentReferrerStatistics
    {
        return new ShopDocumentReferrerStatistics(
            $item['documentReferrer'],
            $item['lastPurchaseDate'] ? new \DateTime($item['lastPurchaseDate']) : null,
            $item['lastSignUpDate'] ? new \DateTime($item['lastSignUpDate']) : null,
            (int)$item['purchaseCount'],
            (int)$item['signUpCount'],
            (int)$item['viewsCount'],
            (float)$item['signUpReferrerProfit'],
            (float)$item['purchaseReferrerProfit'],
            (float)$item['systemCommission'],
            (float)$item['purchaseTotal']
        );
    }
}