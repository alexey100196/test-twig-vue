<?php

namespace App\DataFixtures;

use App\DataFixtures\BaseGlossaryFixtures;
use App\Entity\OfferType;

class OfferTypeFixtures extends BaseGlossaryFixtures
{
    public function getData(): array
    {
        return [
            1 => OfferType::CPS,
            2 => OfferType::CPL,
        ];
    }

    public function getEntity()
    {
        return new OfferType();
    }
}
