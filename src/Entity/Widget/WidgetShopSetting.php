<?php

namespace App\Entity\Widget;

use App\Entity\Coupon\CouponDiscountType;
use App\Entity\Coupon\CouponPackage;
use App\Entity\Currency;
use App\Entity\Language;
use App\Entity\Media;
use App\Entity\Shop;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use App\Helper\WidgetCompletion\WidgetCompletionResolver;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Widget\WidgetShopSettingRepository")
 */
class WidgetShopSetting
{
    const REFERRER_GET_COUPON_REWARD_FLAG_ALWAYS = 0;
    const REFERRER_GET_COUPON_REWARD_FLAG_REDIRECT = 1;
    const REFERRER_GET_COUPON_REWARD_FLAG_ORDER = 2;

    const REFERRER_LINK_VALIDITY_MODE_ALWAYS_VALID = 1;
    //Disable CAMPAIGN VALIDITY: Valid Number of Days
/*    const REFERRER_LINK_VALIDITY_MODE_N_DAYS_AFTER_REGISTRATION = 0;*/
    const REFERRER_LINK_VALIDITY_MODE_TILL_DATE = 2;

    const REFERRER_LINK_CASH_TYPE_FLAG_ALWAYS = 1;
    const REFERRER_LINK_CASH_TYPE_FLAG_MIN_VALUE = 2;

    const REFERRER_LINK_PURCHASES_VALID_FLAG_ALWAYS = 1;
    const REFERRER_LINK_PURCHASES_VALID_FLAG_MAX_ORDERS = 2;

    const REFERRER_LINK_VALIDITY_MIN_DAYS = 1;
    const REFERRER_LINK_VALIDITY_MAX_DAYS = 30;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $referrerGetCouponRewardFlag = self::REFERRER_GET_COUPON_REWARD_FLAG_ALWAYS;

    /**
     * @ORM\Column(type="smallint")
     */
    private $referrerLinkValidityMode = self::REFERRER_LINK_VALIDITY_MODE_ALWAYS_VALID;

    /**
     * @ORM\Column(type="smallint")
     */
    private $cashType = 1;

    /**
     * @ORM\Column(type="smallint")
     */
    private $cashRewards = 1;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $referrerLinkValidityDate;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    /*
    private $referrerLinkValidityDays;
    */

    /**
     * @ORM\Column(type="boolean")
     */
    private $referrerRewardCouponEnabled = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $referrerRewardCashEnabled = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Coupon\CouponDiscountType")
     */
    private $referrerRewardCouponDiscountType;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=2, nullable=true)
     */
    private $referrerRewardCouponDiscountMinPurchase;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=2, nullable=true)
     */
    private $minCashPurchase;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cashRewardsMaxOrders;

    /**
     * @ORM\Column(type="boolean")
     */
    private $inviteeRewardCouponEnabled = true;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Coupon\CouponDiscountType")
     */
    private $inviteeRewardCouponDiscountType;

    /**
     * @var CouponPackage|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Coupon\CouponPackage", inversedBy="referrerWidgetShopSettings", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $referrerRewardCouponPackage;

    /**
     * @var CouponPackage|null
     * @ORM\ManyToOne(targetEntity="App\Entity\Coupon\CouponPackage", inversedBy="inviteeWidgetShopSettings", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $inviteeRewardCouponPackage;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="widgetSetting", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $shop;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language")
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customBarText = 'Let Your friends to hear about us';

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customText = 'Enter Your Text e.g. Recommend ValueLink to Your friends and You both will be rewarded!';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", inversedBy="widgetShopSetting",cascade={"persist"}, fetch="EAGER")
     */
    private $welcomePicture;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Campaign", mappedBy="widgetShopSetting", cascade={"persist", "remove"})
     */
    private $campaign;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $welcomePictureWidth = 400;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $welcomePictureHeight = 230;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $welcomePicturePosX = 20;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $welcomePicturePosY = 20;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $welcomePictureRatio = 1.77;

    /**
     * @ORM\Column(type="smallint", options={"default" : 0})
     */
    private $wizardStep = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReferrerGetCouponRewardFlag(): ?int
    {
        return $this->referrerGetCouponRewardFlag;
    }

    public function setReferrerGetCouponRewardFlag(int $referrerGetCouponRewardFlag): self
    {
        $this->referrerGetCouponRewardFlag = $referrerGetCouponRewardFlag;

        return $this;
    }

    public function getReferrerLinkValidityMode(): ?int
    {
        return $this->referrerLinkValidityMode;
    }

    public function setReferrerLinkValidityMode(int $referrerLinkValidityMode): self
    {
        $this->referrerLinkValidityMode = $referrerLinkValidityMode;

        return $this;
    }

    public function getCashType(): ?int
    {
        return $this->cashType;
    }

    public function setCashType(int $cashType): self
    {
        $this->cashType = $cashType;

        return $this;
    }

    public function getCashRewards(): ?int
    {
        return $this->cashRewards;
    }

    public function setCashRewards(int $cashRewards): self
    {
        $this->cashRewards = $cashRewards;

        return $this;
    }

    public function getReferrerLinkValidityDate(): ?\DateTimeInterface
    {
        return $this->referrerLinkValidityDate;
    }

    public function setReferrerLinkValidityDate(?\DateTimeInterface $referrerLinkValidityDate): self
    {
        $this->referrerLinkValidityDate = $referrerLinkValidityDate;

        return $this;
    }

 /*   public function getReferrerLinkValidityDays(): ?int
    {
        return $this->referrerLinkValidityDays;
    }

    public function setReferrerLinkValidityDays(?int $referrerLinkValidityDays): self
    {
        $this->referrerLinkValidityDays = $referrerLinkValidityDays;

        return $this;
    }*/

    public function getReferrerRewardCouponEnabled(): ?bool
    {
        return $this->referrerRewardCouponEnabled;
    }

    public function setReferrerRewardCouponEnabled(bool $referrerRewardCouponEnabled): self
    {
        $this->referrerRewardCouponEnabled = $referrerRewardCouponEnabled;

        return $this;
    }

    public function getReferrerRewardCouponDiscountType(): ?CouponDiscountType
    {
        return $this->referrerRewardCouponDiscountType;
    }

    public function setReferrerRewardCouponDiscountType(?CouponDiscountType $referrerRewardCouponDiscountType): self
    {
        $this->referrerRewardCouponDiscountType = $referrerRewardCouponDiscountType;

        return $this;
    }

    public function getReferrerRewardCouponDiscountMinPurchase()
    {
        return $this->referrerRewardCouponDiscountMinPurchase;
    }

    public function setReferrerRewardCouponDiscountMinPurchase($referrerRewardCouponDiscountMinPurchase): self
    {
        $this->referrerRewardCouponDiscountMinPurchase = $referrerRewardCouponDiscountMinPurchase;

        return $this;
    }

    public function getMinCashPurchase()
    {
        return $this->minCashPurchase;
    }

    public function setMinCashPurchase($minCashPurchase): self
    {
        $this->minCashPurchase = $minCashPurchase;

        return $this;
    }

    public function getCashRewardsMaxOrders()
    {
        return $this->cashRewardsMaxOrders;
    }

    public function setCashRewardsMaxOrders($cashRewardsMaxOrders): self
    {
        $this->cashRewardsMaxOrders = $cashRewardsMaxOrders;

        return $this;
    }

    public function getInviteeRewardCouponEnabled(): ?bool
    {
        return $this->inviteeRewardCouponEnabled;
    }

    public function setInviteeRewardCouponEnabled(bool $inviteeRewardCouponEnabled): self
    {
        $this->inviteeRewardCouponEnabled = $inviteeRewardCouponEnabled;

        return $this;
    }

    public function getInviteeRewardCouponDiscountType(): ?CouponDiscountType
    {
        return $this->inviteeRewardCouponDiscountType;
    }

    public function setInviteeRewardCouponDiscountType(?CouponDiscountType $inviteeRewardCouponDiscountType): self
    {
        $this->inviteeRewardCouponDiscountType = $inviteeRewardCouponDiscountType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReferrerRewardCashEnabled()
    {
        return $this->referrerRewardCashEnabled;
    }

    /**
     * @param mixed $referrerRewardCashEnabled
     */
    public function setReferrerRewardCashEnabled($referrerRewardCashEnabled): void
    {
        $this->referrerRewardCashEnabled = $referrerRewardCashEnabled;
    }

    public function getReferrerRewardCouponPackage(): ?CouponPackage
    {
        return $this->referrerRewardCouponPackage;
    }

    public function setReferrerRewardCouponPackage(?CouponPackage $referrerRewardCouponPackage): self
    {
        $this->referrerRewardCouponPackage = $referrerRewardCouponPackage;

        return $this;
    }

    public function getInviteeRewardCouponPackage(): ?CouponPackage
    {
        return $this->inviteeRewardCouponPackage;
    }

    public function hasInviteeRewardCouponsAvailable(): bool
    {
        return $this->inviteeRewardCouponPackage && $this->inviteeRewardCouponPackage->hasAvailableCoupons();
    }

    public function hasReferrerRewardCouponsAvailable(): bool
    {
        return $this->referrerRewardCouponPackage && $this->referrerRewardCouponPackage->hasAvailableCoupons();
    }

    public function setInviteeRewardCouponPackage(?CouponPackage $inviteeRewardCouponPackage): self
    {
        $this->inviteeRewardCouponPackage = $inviteeRewardCouponPackage;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(Language $language): void
    {
        $this->language = $language;
    }

    public function getCustomText(): ?string
    {
        return $this->customText;
    }

    public function setCustomText(?string $customText): void
    {
        $this->customText = $customText;
    }

    public function getCustomBarText(): ?string
    {
        return $this->customBarText;
    }

    public function setCustomBarText(?string $customBarText): void
    {
        $this->customBarText = $customBarText;
    }

    /**
     * @return mixed
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * @param mixed $campaign
     */
    public function setCampaign($campaign): void
    {
        $this->campaign = $campaign;
    }

    public function getWelcomePicture(): ?Media
    {
        return $this->welcomePicture;
    }

    public function setWelcomePicture(?Media $welcomePicture)
    {
        $this->welcomePicture = $welcomePicture;
    }

    public function getWelcomePictureWidth(): ?float
    {
        return $this->welcomePictureWidth;
    }

    public function setWelcomePictureWidth(?float $welcomePictureWidth): self
    {
        $this->welcomePictureWidth = $welcomePictureWidth;

        return $this;
    }

    public function getWelcomePictureHeight(): ?float
    {
        return $this->welcomePictureHeight;
    }

    public function setWelcomePictureHeight(?float $welcomePictureHeight): self
    {
        $this->welcomePictureHeight = $welcomePictureHeight;

        return $this;
    }

    public function getWelcomePicturePosX(): ?int
    {
        return $this->welcomePicturePosX;
    }

    public function setWelcomePicturePosX(?int $welcomePicturePosX): self
    {
        $this->welcomePicturePosX = $welcomePicturePosX;

        return $this;
    }

    public function getWelcomePicturePosY(): ?int
    {
        return $this->welcomePicturePosY;
    }

    public function setWelcomePicturePosY(?int $welcomePicturePosY): self
    {
        $this->welcomePicturePosY = $welcomePicturePosY;

        return $this;
    }

    public function getWelcomePictureRatio(): ?float
    {
        return $this->welcomePictureRatio;
    }

    public function setWelcomePictureRatio(?float $welcomePictureRatio): self
    {
        $this->welcomePictureRatio = $welcomePictureRatio;

        return $this;
    }

    public function getWizardStep(): ?int
    {
        return $this->wizardStep;
    }

    /**
     * @param int $wizardStep (1 - email preview, 2 - activate widget view opened, 3 - preview widget view opened, 4 - last step opened [completed])
     * @param bool $force
     * @return bool (true if changed)
     */
    public function setWizardStep(int $wizardStep, bool $force = false): bool
    {
        if ($wizardStep > $this->wizardStep || $force) {
            $this->wizardStep = $wizardStep;

            return true;
        }

        return false;
    }

    public function linkExpired(WidgetReferenceLink $referenceLink): bool
    {
        if ($this->referrerLinkValidityMode == self::REFERRER_LINK_VALIDITY_MODE_ALWAYS_VALID) {
            return false;
        }
        //Disable CAMPAIGN VALIDITY: Valid Number of Days-->
        /*if ($this->referrerLinkValidityMode == self::REFERRER_LINK_VALIDITY_MODE_N_DAYS_AFTER_REGISTRATION) {
            return Carbon::instance($referenceLink->getCreatedAt())->diffInDays(Carbon::now()) > $this->referrerLinkValidityDays;
        }*/
        if ($this->referrerLinkValidityMode == self::REFERRER_LINK_VALIDITY_MODE_TILL_DATE) {
            return Carbon::now() >= Carbon::instance($this->referrerLinkValidityDate);
        }
    }

    public function getLinkExpiresAt(WidgetReferenceLink $referenceLink): ?\DateTimeInterface
    {
        //Disable CAMPAIGN VALIDITY: Valid Number of Days-->
       /* if ($this->referrerLinkValidityMode == self::REFERRER_LINK_VALIDITY_MODE_N_DAYS_AFTER_REGISTRATION) {
            return Carbon::instance($referenceLink->getCreatedAt())->addDays($this->referrerLinkValidityDays);
        }*/
        if ($this->referrerLinkValidityMode == self::REFERRER_LINK_VALIDITY_MODE_TILL_DATE) {
            return Carbon::instance($this->referrerLinkValidityDate);
        }

        return null;
    }

    private function canEnableWidget(): bool
    {
        $enabled = $this->hasInviteeRewardCouponsAvailable() &&
            $this->getCampaign() && $this->getCampaign()->getShop()->hasOwnerActiveSubscription();

        if ($this->referrerLinkValidityMode == self::REFERRER_LINK_VALIDITY_MODE_TILL_DATE) {
            $enabled = $enabled && Carbon::now() < Carbon::instance($this->referrerLinkValidityDate);
        }

        return $enabled;
    }

    public function couponRewardsEnabled(): bool
    {
        return $this->canEnableWidget() &&
            $this->hasReferrerRewardCouponsAvailable() &&
            $this->getReferrerRewardCouponEnabled();
    }

    public function cashRewardsEnabled(): bool
    {
        return $this->canEnableWidget() &&
            $this->getReferrerRewardCashEnabled() &&
            $this->getCampaign()->getShop()->hasOwnerBalance();
    }

    public function hasAnyCoupons(): bool
    {
        return $this->hasInviteeRewardCouponsAvailable() || $this->hasReferrerRewardCouponsAvailable();
    }

    /**
     * @return WidgetCompletionResolver
     */
    public function getCompletion()
    {
        $widgetCompletionResolver = new WidgetCompletionResolver($this);
        $widgetCompletionResolver->resolve();

        return $widgetCompletionResolver;
    }

    public function toArray() {
        return [
            'id' => $this->id,
            'referrerGetCouponRewardFlag' => $this->referrerGetCouponRewardFlag,
            'referrerLinkValidityMode' => $this->referrerLinkValidityMode,
            'cashType' => $this->cashType,
            'cashRewards' => $this->cashRewards,
            'cashRewardsMaxOrders' => $this->cashRewardsMaxOrders,
            'referrerLinkValidityDate' => $this->referrerLinkValidityDate ? $this->referrerLinkValidityDate->format('Y-m-d 23:59:59') : null,
/*            'referrerLinkValidityDays' => $this->referrerLinkValidityDays,*/
            'referrerRewardCouponEnabled' => $this->referrerRewardCouponEnabled,
            'referrerRewardCashEnabled' => $this->referrerRewardCashEnabled,
            'referrerRewardCouponDiscountType' => $this->referrerRewardCouponDiscountType ? $this->referrerRewardCouponDiscountType->getName() : null,
            'referrerRewardCouponDiscountMinPurchase' => $this->referrerRewardCouponDiscountMinPurchase,
            'minCashPurchase' => $this->minCashPurchase,
            'inviteeRewardCouponEnabled' => $this->inviteeRewardCouponEnabled,
            'inviteeRewardCouponDiscountType' => $this->inviteeRewardCouponDiscountType ? $this->inviteeRewardCouponDiscountType->getName() : null,
            'referrerRewardCouponPackage' => $this->referrerRewardCouponPackage ? $this->referrerRewardCouponPackage->getId() : null,
            'inviteeRewardCouponPackage' => $this->inviteeRewardCouponPackage ? $this->inviteeRewardCouponPackage->getId() : null,
            'language' => $this->language,
            'customBarText' => $this->customBarText,
            'customText' => $this->customText,
            'welcomePicture' => $this->welcomePicture,
            'welcomePictureWidth' => $this->welcomePictureWidth,
            'welcomePictureHeight' => $this->welcomePictureHeight,
            'welcomePicturePosX' => $this->welcomePicturePosX,
            'welcomePicturePosY' => $this->welcomePicturePosY,
            'welcomePictureRatio' => $this->welcomePictureRatio,
            'wizardStep' => $this->wizardStep
        ];
    }
}
