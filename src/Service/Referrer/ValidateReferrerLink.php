<?php


namespace App\Service\Referrer;


use App\Entity\Currency;
use App\Entity\ReferrerLink;
use App\Repository\CurrencyRepository;
use App\Service\Balance\UserBalanceService;

class ValidateReferrerLink
{
    /**
     * @var UserBalanceService
     */
    private $balanceService;

    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    public function __construct(UserBalanceService $balanceService, CurrencyRepository $currencyRepository)
    {
        $this->balanceService = $balanceService;
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @param ReferrerLink $referrerLink
     */
    public function __invoke(ReferrerLink $referrerLink): void
    {
        if ($referrerLink->isDeleted()) {
            throw new \LogicException('Link has been deleted.');
        }

        $shop = $referrerLink->getShop();

        if ($shop->isSuspended()) {
            $eurCurrency = $this->currencyRepository->findOneByName(Currency::EUR);
            if (!$this->balanceService->hasFunds($shop->getUser(), $eurCurrency)) {
                throw new \LogicException('Suspended shop with no balance is not allowed to promote.');
            }
        }

        if (!$shop->isActive()) {
            throw new \LogicException('Only active shop can promote.');
        }
    }
}