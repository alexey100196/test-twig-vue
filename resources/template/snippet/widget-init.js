"use strict";

function _classCallCheck(instance, Constructor) {if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function getPosVal(first, second) {
  return (!second || second === 'auto') ? first : 'auto';
}
function calcPosition(data) {
  if (data.coords) {
    if (window.matchMedia( '(max-width: 768px)' ).matches) {
      // mobile
      if(getPosVal(data.coords.mobile.bottom, data.coords.mobile.top) === 'auto') {
        // top
        if (getPosVal(data.coords.mobile.left, data.coords.mobile.right) === 'auto') {
          // right
          return 2;
        } else {
          // left
          return 1;
        }
      } else {
        // bottom
        if (getPosVal(data.coords.mobile.left, data.coords.mobile.right) === 'auto') {
          // right
          return 4;
        } else {
          // left
          return 3;
        }
      }
    } else {
      // desktop
      if(getPosVal(data.coords.desktop.bottom, data.coords.desktop.top) === 'auto') {
        // bottom
        if (getPosVal(data.coords.desktop.left, data.coords.desktop.right) === 'auto') {
          // right
          return 2;
        } else {
          // left
          return 1;
        }
      } else {
        // bottom
        if (getPosVal(data.coords.desktop.left, data.coords.desktop.right) === 'auto') {
          // right
          return 4;
        } else {
          // left
          return 3;
        }
      }
    }
  }

  return 4;
}

function addButtonStyles() {
  var btnStyles = document.createElement('STYLE')
  btnStyles.type = 'text/css'
  btnStyles.innerHTML = '.bell-button{position:absolute;z-index:2;background:0 0;border:0;outline:0;border-radius:90px;padding:3px;-ms-touch-action:auto;touch-action:auto;pointer-events:visible}.bell-button img{width:40px;height:auto}.sonar-emitter{border-radius:9999px;background-color:inherit}.sonar-emitter .sonar-wave{-webkit-box-sizing:content-box;box-sizing:content-box;position:absolute;top:50%;left:50%;-webkit-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);transform:translate(-50%,-50%);width:102%;height:102%;border-radius:9999px;background-color:transparent;border:0 solid;z-index:-1;pointer-events:none;-webkit-animation:sonarWave 2s linear infinite;animation:sonarWave 2s linear infinite}@-webkit-keyframes sonarWave{from{opacity:1}to{border-width:32px;opacity:0}}@keyframes sonarWave{from{opacity:1}to{border-width:32px;opacity:0}}'
  document.head.appendChild(btnStyles)
}

//editable
var widgetIframe =
/*#__PURE__*/
function () {
  function widgetIframe() {
    _classCallCheck(this, widgetIframe);
    this.id = ValueLinkID;
    // this.url = document.getElementsByName('app_url')[0].content + "/";
    this.url = '//marfil-app.local/';
    // this.url = 'http://marfil.junior.dev.dige.com.pl/';
    this.iframe = document.createElement('iframe');
    // this.iframe.setAttribute("sandbox", "allow-scripts allow-same-origin allow-popups");
    //allow-downloads-without-user-activation
    //allow-storage-access-by-user-activation
    this.iframeWrapper = document.createElement('div');
    this.iframeWrapper.id = 'iframeWrapper';
    this.size = {
      width: "400px",
      widthWithButton: "440px",
      height: "540px"
    };
    this.iframe.onload = () => {
      this.iframe.contentWindow.postMessage({
        description: document.head.querySelector('meta[property*="description"], meta[name*="description"]') && document.head.querySelector('meta[property*="description"], meta[name*="description"]').content,
        image: document.head.querySelector('meta[property*="image"], meta[name*="image"]') && document.head.querySelector('meta[property*="image"], meta[name*="image"]').content
      }, 'http://marfil-app.local')
    }
  }

  _createClass(widgetIframe, [{
    key: "getOptions",
    value: function getOptions() {
      var req = new XMLHttpRequest();
      req.open('GET', this.url + 'widget/button-info/' + this.id, false);
      req.send(null);

      if (req.status === 200) {
        var parsedResponse = JSON.parse(req.response);

        if (parsedResponse.enabled === true) {
          return parsedResponse;
        } else {
          return null;
        }
      }
    }
  }, {
    key: "buildIframe",
    value: function buildIframe(data) {
      var _this = this;

      //iframe style
      this.iframe.style.width = this.size.width;
      this.iframe.style.maxWidth = '100%';
      this.iframe.style.height = 'calc(100vh - 40px)';
      this.iframe.style.backgroundColor = '#fff';
      this.iframe.style.marginBottom = '-1px';
      this.iframe.src = this.url + 'widget/iframe/' + this.id;
      var iframeClose = document.createElement('div');
      iframeClose.innerHTML = "<div style=\"background: white;border: 2px solid grey; border-bottom: 0; border-top: 0; padding: 8px 20px 12px;display: flex;justify-content: space-between;align-items: center;line-height: 1.2\" class=\"closeWrapper\"><span style=\"color: grey; font-weight: bold; font-size: 14px\">".concat(data.heading_text, "</span><span style=\"cursor: pointer; color: grey; font-weight: bold; font-size: 18px\">x</span></div>");
      var iFrameCloseWrapper = iframeClose.querySelector('.closeWrapper');
      var position = 4;

      iframeClose.onclick = function () {
        position = calcPosition(data);
        if (window.innerWidth < 768.1) {
          var scrollY = document.body.style.top;
          document.body.style.position = '';
          document.body.style.top = '';
          window.scrollTo(0, parseInt(scrollY || '0') * -1);
        }

        switch (position) {
          case 1:
            //top left
            _this.iframeWrapper.style.left = "-" + _this.size.widthWithButton;
            _this.iframeWrapper.style.right = window.innerWidth * 5 + "px";
            break;

          case 3:
            //bottom left
            _this.iframeWrapper.style.left = "-" + _this.size.widthWithButton;
            _this.iframeWrapper.style.right = window.innerWidth * 5 + "px";
            break;

          case 2:
            //top right
            _this.iframeWrapper.style.left = window.innerWidth * 5 + "px";
            _this.iframeWrapper.style.right = "-" + _this.size.widthWithButton;
            break;

          case 4:
            //bottom right
            _this.iframeWrapper.style.left = window.innerWidth * 5 + "px";
            _this.iframeWrapper.style.right = "-" + _this.size.widthWithButton;
            break;
        }
      };

      position = calcPosition(data);
      switch (position) {
        case 1:
          //top left
          this.iframeWrapper.style.left = "-" + this.size.widthWithButton;
          this.iframeWrapper.style.right = window.innerWidth * 5 + "px";
          this.iframeWrapper.style.top = '0';
          iFrameCloseWrapper.style.borderLeft = '0';

          if (window.innerWidth < 1113) {
            iFrameCloseWrapper.style.borderLeft = '2px solid grey';
            iFrameCloseWrapper.style.borderRight = '2px solid grey';
          }

          break;

        case 2:
          //top right
          this.iframeWrapper.style.right = "-" + this.size.widthWithButton;
          this.iframeWrapper.style.left = window.innerWidth * 5 + "px";
          this.iframeWrapper.style.top = '0';
          iFrameCloseWrapper.style.borderRight = '0';

          if (window.innerWidth < 1113) {
            iFrameCloseWrapper.style.borderLeft = '2px solid grey';
            iFrameCloseWrapper.style.borderRight = '2px solid grey';
          }

          break;

        case 3:
          //bottom left
          this.iframeWrapper.style.left = "-" + this.size.widthWithButton;
          this.iframeWrapper.style.right = window.innerWidth * 5 + "px";
          this.iframeWrapper.style.top = '0';
          this.iframeWrapper.style.bottom = '0';
          iFrameCloseWrapper.style.borderLeft = '0';

          if (window.innerWidth < 1113) {
            iFrameCloseWrapper.style.borderLeft = '2px solid grey';
            iFrameCloseWrapper.style.borderRight = '2px solid grey';
          }

          break;

        case 4:
          //bottom right
          this.iframeWrapper.style.right = "-" + this.size.widthWithButton;
          this.iframeWrapper.style.left = window.innerWidth * 5 + "px";
          this.iframeWrapper.style.top = '0';
          this.iframeWrapper.style.bottom = '0';
          iFrameCloseWrapper.style.borderRight = '0';

          if (window.innerWidth < 1113) {
            iFrameCloseWrapper.style.borderLeft = '2px solid grey';
            iFrameCloseWrapper.style.borderRight = '2px solid grey';
          }

          break;

        default:
          this.iframeWrapper.style.right = "-" + this.size.widthWithButton;
          this.iframeWrapper.style.left = window.innerWidth * 5 + "px";
          this.iframeWrapper.style.top = '0';
          this.iframeWrapper.style.bottom = '0';
          iFrameCloseWrapper.style.borderRight = '0';

          if (window.innerWidth < 1113) {
            iFrameCloseWrapper.style.borderLeft = '2px solid grey';
            iFrameCloseWrapper.style.borderRight = '2px solid grey';
          }

          break;
      } //iframe wrapper style


      this.iframeWrapper.style.position = 'fixed';
      this.iframeWrapper.style.transition = ".5s all ease-in-out";
      this.iframeWrapper.style.zIndex = 9999999;
      this.iframeWrapper.style.margin = "0";
      this.iframeWrapper.style.minWidth = '320px';
      this.iframeWrapper.appendChild(iframeClose);
      this.iframeWrapper.appendChild(this.iframe);
      document.body.appendChild(this.iframeWrapper);
    }
  }, {
    key: "buildButton",
    value: function buildButton(data) {
      addButtonStyles()
      var _this2 = this;

      var position = 4;
      var button = document.createElement('button');
      button.id = 'iframeButton';
      button.className = 'bell-button sonar-emitter';
      button.style.position = 'fixed';
      button.style.zIndex = 1000;
      button.style.padding = ((data.background_size - data.icon_size) || 43) + 'px';
      button.style.background = (!data.is_hide_background) ? (data.background_color || 'transparent') : 'transparent';
      button.style.border = (data.is_hide_background) ? `4px solid ${(data.background_color || 'transparent')}` : '0';

      if (data.coords) {
        position = calcPosition(data);
        setIconPosition();
        window.addEventListener('resize', () => {
          setIconPosition();
        });

        function setIconPosition() {
          if (window.matchMedia( '(max-width: 768px)' ).matches) {
            button.style.top = data.coords.mobile.top;
            button.style.right = data.coords.mobile.right;
            button.style.bottom = getPosVal(data.coords.mobile.bottom, data.coords.mobile.top);
            button.style.left = getPosVal(data.coords.mobile.left, data.coords.mobile.right);
          } else {
            button.style.top = data.coords.desktop.top;
            button.style.right = data.coords.desktop.right;
            button.style.bottom = getPosVal(data.coords.desktop.bottom, data.coords.desktop.top);
            button.style.left = getPosVal(data.coords.desktop.left, data.coords.desktop.right);
          }
        }
      } else {
        // Old solution of position
        switch (data.position) {
          case 1:
            //top left
            button.style.left = '30px';

            if (window.innerWidth < 1199) {
              button.style.bottom = '30px';
            } else {
              button.style.top = '220px';
            }

            break;

          case 2:
            //top right
            button.style.right = '30px';

            if (window.innerWidth < 1199) {
              button.style.bottom = '30px';
            } else {
              button.style.top = '220px';
            }

            break;

          case 3:
            //bottom left
            button.style.left = '30px';
            button.style.bottom = '30px';
            break;

          case 4:
            //bottom right
            button.style.right = '30px';
            button.style.bottom = '30px';
            break;

          default:
            button.style.right = '30px';
            button.style.bottom = '30px';
            break;
        }
      }

      button.onclick = function () {
        position = calcPosition(data);
        if (window.innerWidth < 768.1) {
          document.body.style.top = "-".concat(window.scrollY, "px");
          document.body.style.position = 'fixed';
          window.addEventListener('resize', function () {
            if (_this2.iframeWrapper.style.right === "0px" && _this2.iframeWrapper.style.left === "0px") {
              document.querySelector('.closeWrapper').click();
              button.click();
            }
          });
        }

        switch (position) {
          case 1:
            //top left
            _this2.iframe.style.border = 'none';
            _this2.iframe.style.borderRight = '2px solid grey';
            _this2.iframeWrapper.style.left = "0px";
            _this2.iframeWrapper.style.margin = "0 0";
            _this2.iframeWrapper.style.right = "unset";
            _this2.iframeWrapper.border = 'none';
            _this2.iframeWrapper.top = 'unset';
            _this2.iframeWrapper.overflowY = 'auto';
            _this2.iframeWrapper.transform = 'unset';

            if (window.innerWidth < 1199) {
              _this2.iframe.style.height = "100%";
              _this2.iframe.style.maxHeight = "100vh";
              _this2.iframe.style.border = '2px solid grey';
              _this2.iframe.style.borderTop = '0';
              _this2.iframe.style.width = "400px";
            }

            if (window.innerWidth < 590) {
              _this2.iframeWrapper.style.margin = "0 0";
              _this2.iframe.style.width = "calc(100% - 0px)";
              _this2.iframe.style.boxSizing = "border-box";
              _this2.iframe.style.height = "calc(100vh - 41px)";
              _this2.iframe.style.border = '2px solid grey';
              _this2.iframe.style.borderTop = '0';
              _this2.iframeWrapper.style.left = "0px";
              _this2.iframeWrapper.style.right = "0px";
            }

            if (window.innerWidth < 480) {
              _this2.iframeWrapper.style.right = "0px";
              _this2.iframeWrapper.style.left = "0px";
            }

            break;

          case 3:
            //bottom left
            _this2.iframe.style.border = 'none';
            _this2.iframeWrapper.style.left = "0px";
            _this2.iframeWrapper.style.margin = "0 0";
            _this2.iframe.style.borderRight = '2px solid grey';
            _this2.iframeWrapper.style.right = "unset";
            _this2.iframeWrapper.border = 'none';
            _this2.iframeWrapper.top = 'unset';
            _this2.iframeWrapper.overflowY = 'auto';
            _this2.iframeWrapper.transform = 'unset';

            if (window.innerWidth < 1199) {
              _this2.iframeWrapper.style.top = "0px";
              _this2.iframe.style.height = "100%";
              _this2.iframe.style.maxHeight = "100vh";
              _this2.iframe.style.border = '2px solid grey';
              _this2.iframe.style.borderTop = '0';
              _this2.iframe.style.width = "400px";
              _this2.iframe.style.paddingBottom = "30px";
            }

            if (window.innerWidth < 590) {
              _this2.iframeWrapper.style.margin = "0 0";
              _this2.iframeWrapper.style.left = "0px";
              _this2.iframeWrapper.style.right = "0px";
              _this2.iframeWrapper.style.top = "0px";
              _this2.iframe.style.width = "calc(100% - 0px)";
              _this2.iframe.style.boxSizing = "border-box"; // this.iframe.style.height = "calc(100vh - 41px)";

              _this2.iframe.style.border = '2px solid grey';
              _this2.iframe.style.borderTop = '0';
              _this2.iframe.style.paddingBottom = "0";
              _this2.iframe.style.width = "calc(100% - 0px)";
              _this2.iframe.style.boxSizing = "border-box";
              _this2.iframe.style.height = "calc(100vh - 41px)";
            }

            if (window.innerWidth < 480) {
              _this2.iframeWrapper.style.right = "0px";
              _this2.iframeWrapper.style.left = "0px";
            }

            break;

          case 2:
            //top right
            _this2.iframe.style.border = 'none';
            _this2.iframe.style.borderLeft = '2px solid grey';
            _this2.iframeWrapper.style.right = "0px";
            _this2.iframeWrapper.style.margin = "0 0";
            _this2.iframeWrapper.style.left = "unset";
            _this2.iframeWrapper.border = 'none';
            _this2.iframeWrapper.top = 'unset';
            _this2.iframeWrapper.overflowY = 'auto';
            _this2.iframeWrapper.transform = 'unset';

            if (window.innerWidth < 1199) {
              _this2.iframe.style.height = "100%";
              _this2.iframe.style.maxHeight = "100vh";
              _this2.iframe.style.width = "400px";
              _this2.iframe.style.paddingBottom = "30px";
              _this2.iframe.style.border = '2px solid grey';
              _this2.iframe.style.borderTop = '0';
            }

            if (window.innerWidth < 590) {
              _this2.iframeWrapper.style.left = "0px";
              _this2.iframeWrapper.style.right = "0px";
              _this2.iframeWrapper.style.margin = "0 0";
              _this2.iframe.style.width = "calc(100% - 0px)";
              _this2.iframe.style.boxSizing = "border-box";
              _this2.iframe.style.height = "calc(100vh - 41px)";
              _this2.iframe.style.paddingBottom = "0";
              _this2.iframe.style.border = '2px solid grey';
              _this2.iframe.style.borderTop = '0';
            }

            if (window.innerWidth < 480) {
              _this2.iframeWrapper.style.right = "0px";
              _this2.iframeWrapper.style.left = "0px";
            }

            break;

          case 4:
            //bottom right
            _this2.iframe.style.border = 'none';
            _this2.iframe.style.borderLeft = '2px solid grey';
            _this2.iframeWrapper.style.right = "0px";
            _this2.iframeWrapper.style.left = "unset";
            _this2.iframeWrapper.style.margin = "0 0";
            _this2.iframeWrapper.border = 'none';
            _this2.iframeWrapper.top = 'unset';
            _this2.iframeWrapper.overflowY = 'auto';
            _this2.iframeWrapper.transform = 'unset';

            if (window.innerWidth < 1199) {
              _this2.iframe.style.height = "100%";
              _this2.iframe.style.maxHeight = "100vh";
              _this2.iframe.style.width = "400px";
              _this2.iframe.style.paddingBottom = "30px";
              _this2.iframe.style.border = '2px solid grey';
              _this2.iframe.style.borderTop = '0';
            }

            if (window.innerWidth < 590) {
              _this2.iframe.style.width = "calc(100% - 0px)";
              _this2.iframe.style.boxSizing = "border-box";
              _this2.iframe.style.height = "calc(100vh - 41px)";
              _this2.iframe.style.paddingBottom = "0";
              _this2.iframeWrapper.style.left = "0px";
              _this2.iframeWrapper.style.right = "0px";
              _this2.iframe.style.border = '2px solid grey';
              _this2.iframe.style.borderTop = '0';
            }

            if (window.innerWidth < 480) {
              _this2.iframeWrapper.style.right = "0px";
              _this2.iframeWrapper.style.left = "0px";
            }

            break;
        }

        if (window.innerWidth < 1113) {
          _this2.iframe.style.border = '2px solid grey';
          _this2.iframe.style.borderTop = '0';
        }

        if (window.innerWidth < 996) {
          document.querySelector('.closeWrapper').style.borderLeft = '2px solid grey';
          document.querySelector('.closeWrapper').style.borderRight = '2px solid grey';
        }
      };

      button.innerHTML = "\n" +
          "<div class=\"sonar-wave\" style='border-color: "+(data.background_color || 'transparent')+"; background: "+(!data.is_hide_background && data.background_color ? data.background_color : 'transparent')+"'></div>" +
          "<img src=\"".concat(this.url).concat(data.image, "\" alt=\"icon\" style=\"width: "+(data.icon_size || 40)+"px;\" />\n");
      document.body.appendChild(button);
    }
  }, {
    key: "build",
    value: function build(data) {
      if (data) {
        this.buildButton(data);
        this.buildIframe(data);
      }
    }
  }, {
    key: "init",
    value: function init() {
      var data = this.getOptions();
      this.build(data);
    }
  }]);

  return widgetIframe;
}();

var widget = new widgetIframe();
widget.init();