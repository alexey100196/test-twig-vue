<?php

namespace App\Repository\CardSet;

use App\Entity\CardSet\CardSetCard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardSetCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardSetCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardSetCard[]    findAll()
 * @method CardSetCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardSetCardRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardSetCard::class);
    }

    // /**
    //  * @return CardSetCard[] Returns an array of CardSetCard objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardSetCard
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
