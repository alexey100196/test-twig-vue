<?php

namespace App\Form\Type;

use App\Entity\CommissionType;
use App\Entity\Shop;
use App\Validator\Constraints\ShopOffer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class OfferType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $commissionConstraints = [];

        if ($options['validate_min_amount']) {
            $commissionConstraints[] = new GreaterThan(['value' => 0]);
        }

        $builder
            ->add('commission_type', EntityType::class, [
                'class' => CommissionType::class,
                'choice_label' => 'name',
                'required' => true,
            ])
            ->add('commission', NumberType::class, [
                'required' => true,
                'constraints' => array_merge([new NotNull()], $commissionConstraints)
            ])
            ->add('fixed_commission', NumberType::class, [
                'required' => false,
                'mapped' => false,
                'constraints' => $commissionConstraints,
            ])
            ->add('percent_commission', NumberType::class, [
                'required' => false,
                'mapped' => false,
                'constraints' => $commissionConstraints,
            ]);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($options) {
            $data = $event->getData();

            $data['commission'] = $options['validate_min_amount'] ? null : 0;
            $data['commission_type'] = CommissionType::FIXED_ID;

            if ($data['fixed_commission'] ?? false) {
                $data['commission'] = (float)$data['fixed_commission'];
                $data['commission_type'] = CommissionType::FIXED_ID;
            } elseif ($data['percent_commission'] ?? false) {
                $data['commission'] = (float)$data['percent_commission'];
                $data['commission_type'] = CommissionType::PERCENT_ID;
            }

            unset($data['percent_commission'], $data['fixed_commission']);

            $event->setData($data);
        });
    }


    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'validate_min_amount' => true,
        ]);
    }
}
