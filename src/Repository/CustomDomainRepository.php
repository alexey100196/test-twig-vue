<?php

namespace App\Repository;

use App\Entity\CustomDomain;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CustomDomain|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomDomain|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomDomain[]    findAll()
 * @method CustomDomain[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomDomainRepository extends ServiceEntityRepository
{
    /**
     * ReferrerMyStoreRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CustomDomain::class);
    }
}
