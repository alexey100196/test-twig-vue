<?php


namespace App\Controller\Account\Referrer;


use App\Entity\Shop;
use App\Service\Referrer\ReferrerShop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerShopSettingsController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerShopSettingsController extends AbstractController
{
    /**
     * @var ReferrerShop
     */
    private $referrerShop;

    /**
     * ReferrerShopController constructor.
     * @param ReferrerShop $referrerShop
     */
    public function __construct(ReferrerShop $referrerShop)
    {
        $this->referrerShop = $referrerShop;
    }

    /**
     * @Route("/account/referrer/company/{shop}/settings", name="account_referrer_shop_settings_show")
     * @param Shop $shop
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(Shop $shop, Request $request)
    {
        $userShop = $this->referrerShop->findReferrerShopWithStats($this->getUser(), $shop);

        if (!$userShop) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        return $this->render('account/referrer/shop/show-settings.html.twig', [
            'item' => $userShop,
        ]);
    }
}