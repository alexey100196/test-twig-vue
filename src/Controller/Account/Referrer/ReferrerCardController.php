<?php

namespace App\Controller\Account\Referrer;

use App\Entity\ReferrerLink;
use App\Entity\ReferrerMyStore;
use App\Entity\ReferrerStoreNonReferableLink;
use App\Form\Referrer\ReferrerNonReferableLinkCardType;
use App\Repository\ReferrerLinkRepository;
use App\Repository\ReferrerStoreNonReferableLinkRepository;
use App\Service\OpenGraph\OpenGraphMetaData;
use App\Service\Referrer\ReferrerMyStoreService;
use App\Service\Referrer\ReferrerNonReferableLinkCardService;
use App\Service\Referrer\ReferrerReflink;
use App\Service\Referrer\ReferrerShop;
use App\Service\Referrer\ReferrerStoreNonReferableLinkService;
use App\ValueObject\DatePeriod;
use Carbon\Carbon;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerIncomeController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerCardController extends AbstractController
{
    /**
     * @var OpenGraphMetaData
     */
    private $graphMetaData;

    /**
     * @var ReferrerShop
     */
    private $referrerShop;

    /**
     * @var ReferrerReflink
     */
    private $referrerReflink;

    /**
     * @var ReferrerMyStoreService
     */
    private $referrerMyStoreService;

    /**
     * @var ReferrerStoreNonReferableLinkService
     */
    private $referrerStoreNonReferableLinkService;

    /**
     * @var ReferrerNonReferableLinkCardService
     */
    private $referrerNonReferableLinkCardService;

    /**
     * @var ReferrerStoreNonReferableLinkRepository
     */
    private $referrerStoreNonReferableLinkRepository;


    /**
     * ReferrerShopController constructor.
     * @param OpenGraphMetaData $graphMetaData
     * @param ReferrerShop $referrerShop
     * @param ReferrerReflink $referrerReflink
     * @param ReferrerMyStoreService $referrerMyStoreService
     * @param ReferrerStoreNonReferableLinkService $referrerStoreNonReferableLinkService
     * @param ReferrerNonReferableLinkCardService $referrerNonReferableLinkCardService
     * @param ReferrerStoreNonReferableLinkRepository $referrerStoreNonReferableLinkRepository
     */
    public function __construct(
        OpenGraphMetaData $graphMetaData,
        ReferrerShop $referrerShop,
        ReferrerReflink $referrerReflink,
        ReferrerMyStoreService $referrerMyStoreService,
        ReferrerStoreNonReferableLinkService $referrerStoreNonReferableLinkService,
        ReferrerNonReferableLinkCardService $referrerNonReferableLinkCardService,
        ReferrerStoreNonReferableLinkRepository $referrerStoreNonReferableLinkRepository
    ) {
        $this->graphMetaData = $graphMetaData;
        $this->referrerShop = $referrerShop;
        $this->referrerReflink = $referrerReflink;
        $this->referrerMyStoreService = $referrerMyStoreService;
        $this->referrerStoreNonReferableLinkService = $referrerStoreNonReferableLinkService;
        $this->referrerNonReferableLinkCardService = $referrerNonReferableLinkCardService;
        $this->referrerStoreNonReferableLinkRepository = $referrerStoreNonReferableLinkRepository;
    }

    /**
     * @Route(
     *     "/account/referrer/card/{year}/{month}",
     *     name="account_referrer_card_index"
     * )
     * @param null|int $year
     * @param null|int $month
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(int $year = null, int $month = null)
    {
        $user = $this->getUser();
        $myStore = $user->getReferrerMyStore();
        $datePeriod = $this->buildDatePeriodFromYearAndMonth($year, $month);
        $globalStats = $this->referrerShop->getReferrerStats($user);

        $referrerLinks = $this->referrerReflink
            ->getByCardCreationDateRangeForReferrer(
                $user,
                $datePeriod->getStart(),
                $datePeriod->getEnd()
            );

        if (!$myStore) {
            $myStore = new ReferrerMyStore();
            $this->referrerMyStoreService->generate($user, $myStore);
        }

        $customLinks = $this->referrerStoreNonReferableLinkRepository->findBy([
            'mystore' => $myStore
        ], ['orderColumn' => 'ASC', 'updatedAt' => 'DESC']);

        $referrerLinks = array_filter($referrerLinks, function (ReferrerLink $link) {
            return $link->getCard();
        });

        return $this->render('account/referrer/card/index.html.twig', [
            'referrerLinks' => $referrerLinks,
            'customLinks' => $this->referrerStoreNonReferableLinkService->loadItemsToArray($customLinks),
            'globalStats' => $globalStats,
            'currentPeriod' => $datePeriod,
        ]);
    }

    /**
     * @Route("/account/referrer/custom-card/{referrerStoreNonReferableLink}/edit", name="account_referrer_custom_card_edit")
     * @param ReferrerStoreNonReferableLink $referrerStoreNonReferableLink
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editCustomLink(ReferrerStoreNonReferableLink $referrerStoreNonReferableLink, Request $request)
    {
        $user = $this->getUser();
        $myStore = $user->getReferrerMyStore();

        if (!$referrerStoreNonReferableLink) {
            throw new NotFoundHttpException('Link not found.');
        }

        if (!$myStore || $myStore !== $referrerStoreNonReferableLink->getMyStore()) {
            throw new NotFoundHttpException('Invalid my store.');
        }
        $card = $referrerStoreNonReferableLink->getCardOrCreate();
        if ($card->getId() == 0) {
            $card->setTitle('');
            $this->referrerNonReferableLinkCardService->save($card);
        }
        $form = $this->createForm(ReferrerNonReferableLinkCardType::class, $card);
        /** @var OpenGraphMetaData $og */
        $og = $this->graphMetaData->fetch((string) $referrerStoreNonReferableLink->getDestinationUrl());

        if (!$card->getTitle() && $card->isNew() && $ogTitle = $og->getTitle()) {
            $card->setTitle($ogTitle);
        }
        $ogImage = $og->getImage();

        return $this->render('account/referrer/show-custom-card.html.twig', [
            'referrerLink' => $referrerStoreNonReferableLink,
            'referrerLinkCard' => $this->referrerNonReferableLinkCardService->convertObjectToArray($card),
            'ogImage' => $ogImage,
            'form' => $form->createView()
        ]);
    }

    /**
     * @param null $year
     * @param null $month
     * @return DatePeriod
     */
    private function buildDatePeriodFromYearAndMonth($year = null, $month = null): DatePeriod
    {
        $year = $year ? intval($year) : date('Y');
        $month = $month ? intval($month) : date('m');

        return new DatePeriod(
            Carbon::create($year, $month)->startOfMonth(),
            Carbon::create($year, $month)->endOfMonth()
        );
    }
}