<?php

namespace App\Form\Type\Shop;

use App\Entity\Glossary\ShopType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopTypeType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => ShopType::class
        ]);
    }

    public function getParent()
    {
        return EntityType::class;
    }
}