<?php


namespace App\Service\SystemCommission\ValueObjects;

use App\Helper\Number;

class TaxedAmount
{
    /**
     * @var
     */
    private $amount;

    /**
     * @var string
     */
    private $tax;

    public function __construct($amount, $tax)
    {
        $this->amount = Number::toString($amount);
        $this->tax = Number::toString($tax);;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getTax(): string
    {
        return $this->tax;
    }
}