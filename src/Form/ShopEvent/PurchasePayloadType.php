<?php


namespace App\Form\ShopEvent;

use App\Entity\Currency;
use App\Entity\EventEntry;
use App\Entity\EventEntryType;
use App\Entity\Product;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class PurchasePayloadType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('product', EntityType::class, [
                'class' => Product::class,
                'choice_value' => 'number',
                'mapped' => false,
            ])
            ->add('price', NumberType::class, [
                'mapped' => false,
                'constraints' => [
                    new NotBlank(),
                    new GreaterThan(['value' => 0]),
                ]
            ])
            ->add('amount', NumberType::class, [
                'mapped' => false,
                'constraints' => [
                    new NotBlank(),
                    new GreaterThan(['value' => 0]),
                ]
            ])
            ->add('currency', EntityType::class, [
                'class' => Currency::class,
                'choice_value' => 'name',
                'mapped' => false,
                'required' => false,
                'empty_data' => null
            ])
            ->add('coupon', null, [
                'mapped' => false,
                'required' => false,
                'empty_data' => null
            ]);
    }
}