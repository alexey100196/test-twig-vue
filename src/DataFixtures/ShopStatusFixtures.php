<?php

namespace App\DataFixtures;

use App\DataFixtures\BaseGlossaryFixtures;
use App\Entity\Shop;
use App\Entity\ShopStatus;

class ShopStatusFixtures extends BaseGlossaryFixtures
{
    public function getData(): array
    {
        return [
            ShopStatus::INACTIVE => ShopStatus::INACTIVE_NAME,
            ShopStatus::ACTIVE => ShopStatus::ACTIVE_NAME,
            ShopStatus::REJECTED => ShopStatus::REJECTED_NAME,
            ShopStatus::SUSPENDED => ShopStatus::SUSPENDED_NAME,
            ShopStatus::ACCEPTED => ShopStatus::ACCEPTED_NAME,
        ];
    }

    public function getEntity()
    {
        return new ShopStatus();
    }
}
