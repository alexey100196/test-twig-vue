<?php


namespace App\Service\IncomeStatistics;


use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

abstract class Handler
{
    /**
     * @var RegistryInterface
     */
    protected $doctrine;

    /**
     * @var ParameterBagInterface
     */
    protected $parameterBag;

    /**
     * AbstractHandler constructor.
     * @param RegistryInterface $doctrine
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(RegistryInterface $doctrine, ParameterBagInterface $parameterBag)
    {
        $this->doctrine = $doctrine;
        $this->parameterBag = $parameterBag;
    }
}