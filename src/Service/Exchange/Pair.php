<?php

namespace App\Service\Exchange;

class Pair
{
    /** @var string */
    private $baseCurrency;

    /** @var string */
    private $quotedCurrency;

    /** @var string */
    private $rate;

    public function __construct(string $baseCurrency, string $quotedCurrency, string $rate)
    {
        $this->baseCurrency = $baseCurrency;
        $this->quotedCurrency = $quotedCurrency;
        $this->rate = $rate;
    }

    /**
     * @return string
     */
    public function getSymbol(): string
    {
        return $this->baseCurrency . '-' . $this->quotedCurrency;
    }

    /**
     * @return string
     */
    public function getBaseCurrency(): string
    {
        return $this->baseCurrency;
    }

    /**
     * @return string
     */
    public function getQuotedCurrency(): string
    {
        return $this->quotedCurrency;
    }

    /**
     * @return string
     */
    public function getRate(): string
    {
        return $this->rate;
    }
}