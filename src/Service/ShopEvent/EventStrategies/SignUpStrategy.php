<?php


namespace App\Service\ShopEvent\EventStrategies;


use App\Entity\Product;
use App\Entity\ProductSignUp;
use App\Entity\Shop;
use App\Entity\ShopConversionStatus;
use App\Entity\ShopPurchase;
use App\Entity\ShopSignUp;
use App\Event\Conversion\ShopSignUpCreated;
use App\Service\ShopEvent\Auth;
use App\Service\ShopEvent\Interfaces\EventStrategyInterface;
use App\Service\SystemCommission\ApplierInterface;
use App\Service\SystemCommission\ValueObjects\TaxedAmount;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SignUpStrategy extends AbstractStrategy implements EventStrategyInterface
{
    /**
     * @var Applier
     */
    private $feeApplier;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param ApplierInterface $applier
     */
    public function setFeeApplier(ApplierInterface $applier)
    {
        $this->feeApplier = $applier;
    }

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function process()
    {
        $em = $this->doctrine->getEntityManager();

        /** @var Shop $shop */
        $shop = $this->eventEntry->getShop();

        $shopSignUp = new ShopSignUp();
        $shopSignUp->setShop($shop);
        $shopSignUp->setReferrerLink($this->eventEntry->getReferrerLink());

        $shopSignUp->setCommission($shop->getCplOffer()->getCommission());
        $shopSignUp->setCommissionType($shop->getCplOffer()->getCommissionType());
        $shopSignUp->setReferrerProfit($shop->getCplOffer()->getCommission());
        $shopSignUp->setOfferType($shop->getCplOffer()->getType());
        $shopSignUp->setStatus($em->getReference(ShopConversionStatus::class, ShopConversionStatus::UNVERIFIED));

        /** @var TaxedAmount $taxedAmount */
        $taxedAmount = $this->feeApplier->apply($shop, $shopSignUp->getCommission());
        $shopSignUp->setSystemCommission($taxedAmount->getAmount());
        $shopSignUp->setSourceLink($shop->getWebsite());

        $shopSignUp->setEventEntry($this->eventEntry);

        $em->persist($shopSignUp);
        $em->flush();

        $this->eventDispatcher->dispatch(ShopSignUpCreated::NAME, new ShopSignUpCreated($shopSignUp));

    }
}