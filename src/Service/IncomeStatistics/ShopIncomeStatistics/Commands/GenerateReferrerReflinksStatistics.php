<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Commands;


use App\Entity\Shop;
use App\Entity\User;
use App\Service\IncomeStatistics\CommandInterface;

class GenerateReferrerReflinksStatistics implements CommandInterface
{
    /**
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @var Shop
     */
    protected $shop;

    /**
     * @var User
     */
    protected $referrer;

    /**
     * @var array
     */
    protected $productsIds;

    /**
     * @var bool
     */
    protected $includeMainOffer;

    /**
     * @var string
     */
    protected $filterBy;

    /**
     * GenerateStats constructor.
     * @param Shop $shop
     * @param User $referrer
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param array $productsIds
     * @param bool $includeMainOffer
     * @param string $filterBy
     */
    public function __construct(Shop $shop, User $referrer, \DateTime $startDate, \DateTime $endDate, array $productsIds = [], bool $includeMainOffer = false, string $filterBy = '')
    {
        $this->shop = $shop;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->referrer = $referrer;
        $this->productsIds = $productsIds;
        $this->includeMainOffer = $includeMainOffer;
        $this->filterBy = $filterBy;
    }

    /**
     * @return \DateTime
     */
    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @return User
     */
    public function getReferrer(): User
    {
        return $this->referrer;
    }

    /**
     * @return array
     */
    public function getProductsIds(): array
    {
        return $this->productsIds;
    }

    /**
     * @return bool
     */
    public function getIncludeMainOffer(): bool
    {
        return $this->includeMainOffer;
    }

    /**
     * @return string
     */
    public function getFilterBy(): string
    {
        return $this->filterBy;
    }
}