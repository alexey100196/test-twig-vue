<?php

namespace App\Repository;

use App\Entity\WhatsappCampaignVerify;
use App\Entity\Campaign;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WhatsappCampaignVerify|null find($id, $lockMode = null, $lockVersion = null)
 * @method WhatsappCampaignVerify|null findOneBy(array $criteria, array $orderBy = null)
 * @method WhatsappCampaignVerify[]    findAll()
 * @method WhatsappCampaignVerify[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WhatsappCampaignVerifyRepository extends ServiceEntityRepository
{
    /** @var EntityManagerInterface $em */
    private $em;

    public function __construct(RegistryInterface $registry, EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        parent::__construct($registry, WhatsappCampaignVerify::class);
    }


    /**
     * @param String $number
     * @param Campaign $campaign
     * @return Campaign[]  Returns an array of Campaign objects
     */
    public function findVerifiedByCampaign(Campaign $campaign, String $number)
    {
        return $this->createQueryBuilder('v')
            ->where('v.verifiedAt IS NOT NULL')
            ->andWhere('v.campaign = :campaign')
            ->andWhere('v.number = :number')
            ->setParameter('campaign', $campaign)
            ->setParameter('number', $number)
            ->orderBy('v.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param Campaign $campaign
     * @return Campaign[] Returns an array of Campaign objects
     */
    public function getAllByCampaign(Campaign $campaign)
    {
        $query = $this->createQueryBuilder('v')
            ->andWhere('v.campaign = :campaign')
            ->setParameter('campaign', $campaign)
            ->orderBy('v.id', 'ASC')
            ->getQuery()
            ->getResult();

        return (array) $query;
    }

    /**
     * @param Campaign $campaign
     * @return array|array[]
     */
    public function getAllByCampaignArray(Campaign $campaign, $withCampaign = false)
    {
        $result = $this->getAllByCampaign($campaign);

        return array_map(function(Campaign $item) use ($withCampaign) {
            return $item->toArray($withCampaign);
        }, $result);
    }
}
