<?php


namespace App\Controller\Account\Shop;

use App\Helper\DatePeriodGenerator;
use App\Repository\ShopConversionRepository;
use App\Repository\UserShopRepository;
use App\Traits\Controller\HasOrdering;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ShopIncomeController
 *
 * @Security("has_role('ROLE_SHOP')")
 */
class ShopPartnerController extends AbstractController
{
    use HasOrdering;

    /**
     * @var UserShopRepository
     */
    private $userShopRepository;

    public function __construct(UserShopRepository $userShopRepository)
    {
        $this->userShopRepository = $userShopRepository;
    }

    /**
     * @Route("/account/company/partner", name="account_shop_partners")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function index(Request $request)
    {
        $partners = $this->userShopRepository
            ->findPartners(
                $this->getUser()->getShop()
            );
        return $this->render('account/shop/partners/index.html.twig', [
            'partners' => $partners
        ]);
    }
}