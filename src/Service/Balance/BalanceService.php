<?php

namespace App\Service\Balance;

use App\Entity\Currency;
use App\Entity\Transaction;
use App\Entity\TransactionType;
use App\Repository\TransactionRepository;
use App\Service\Balance\Entity\HasBalanceEntity;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\UuidInterface;

abstract class BalanceService
{
    protected $em;

    protected $transactionRepository;

    public function __construct(
        EntityManagerInterface $em,
        TransactionRepository $transactionRepository
    )
    {
        $this->em = $em;
        $this->transactionRepository = $transactionRepository;
    }

    protected function process(Transaction $transaction)
    {
        $this->em->persist($transaction);
        $this->em->flush();
    }

    /**
     * @param HasBalanceEntity $target
     * @param Currency $currency
     * @param float|null $amount (null if want to check if has ANY funds)
     * @return bool
     */
    public function hasFunds(HasBalanceEntity $target, Currency $currency, float $amount = null): bool
    {
        foreach ($target->getBalances() as $balance) {
            if ($balance->getCurrency() === $currency) {
                return $amount === null ? $balance->getAmount() > 0 : $balance->getAmount() >= $amount;
            }
        }

        return false;
    }

    /**
     * @param UuidInterface $uuid
     * @return Transaction|null
     */
    public function findTransactionByUuid(UuidInterface $uuid)
    {
        return $this->transactionRepository->findOneBy(['uuid' => (string)$uuid]);
    }
}