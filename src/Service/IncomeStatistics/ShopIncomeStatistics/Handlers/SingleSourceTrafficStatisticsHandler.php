<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Handlers;


use App\Entity\ShopInteraction;
use App\Repository\ShopConversionRepository;
use App\Repository\ShopInteractionRepository;
use App\Service\IncomeStatistics\Handler;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateSingleSourceTrafficStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Entities\SourceTrafficReflinkStatistics;
use Doctrine\Common\Collections\ArrayCollection;

class SingleSourceTrafficStatisticsHandler extends Handler
{
    public function handle(GenerateSingleSourceTrafficStatistics $command)
    {
        /** @var ShopInteractionRepository $repository */
        $repository = $this->doctrine->getRepository(ShopInteraction::class);

        /** @var array $rows */
        $rows = $repository->getShopDocumentReferrerReflinksStats(
            $command->shop,
            $command->documentReferrer,
            $command->startDate,
            $command->endDate,
            $command->productsIds,
            $command->includeMainOffer
        );

        return new ArrayCollection(array_map([$this, 'prepareStatsItem'], $rows));
    }

    /**
     * @param $item
     * @return SourceTrafficReflinkStatistics
     */
    private function prepareStatsItem($item): SourceTrafficReflinkStatistics
    {
        return new SourceTrafficReflinkStatistics(
            $item['reflink'],
            (int)$item['viewsCount'],
            (int)$item['purchaseCount'],
            (int)$item['signUpCount'],
            (float)$item['signUpReferrerProfit'],
            (float)$item['purchaseReferrerProfit'],
            (float)$item['purchaseTotal']
        );
    }
}