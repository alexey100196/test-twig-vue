<?php


namespace App\Service\Shop;


use App\Entity\Shop;
use App\Entity\ShopStatus;
use App\Entity\Subscription\PlanSubscription;
use Doctrine\ORM\EntityManagerInterface;

class ShopStatusService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Shop $shop
     * @param int $statusId
     * @throws \Doctrine\ORM\ORMException
     */
    private function changeShopStatus(Shop $shop, int $statusId): void
    {
        /** @var ShopStatus $status */
        $status = $this->em->getReference(ShopStatus::class, $statusId);
        $shop->setStatus($status);

        $this->em->flush();
    }

    /**
     * @param Shop $shop
     * @throws \Doctrine\ORM\ORMException
     */
    public function makeShopSuspended(Shop $shop): void
    {
        $this->changeShopStatus($shop, ShopStatus::SUSPENDED);

        // Here you can emit any event.
    }

    /**
     * @param Shop $shop
     * @throws \Doctrine\ORM\ORMException
     */
    public function makeShopActive(Shop $shop): void
    {
        $this->changeShopStatus($shop, ShopStatus::ACTIVE);

        // Here you can emit any event.
    }

    public function makeShopActiveIfSubscriptionActive(Shop $shop)
    {
        $user = $shop->getUser();

        if (!$user->getCurrentPlan()) {
            return false;
        }

        if ($plan = $user->getCurrentPlan()) {
            if ($plan->isActive()) {
                $this->makeShopActive($shop);
            }
        }
    }
}