<?php

namespace App\Controller\Account\Shop;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ShopIncomeController
 *
 * @Security("has_role('ROLE_SHOP')")
 */
class ShopIncomeController extends AbstractController
{
    /**
     * @Route("/account/company/income", name="account_shop_income")
     */
    public function index()
    {
        return $this->render('account/shop/income/index.html.twig');
    }
}