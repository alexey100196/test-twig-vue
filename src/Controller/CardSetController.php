<?php


namespace App\Controller;


use App\Entity\CardSet\CardSet;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CardSetController extends AbstractController
{
    /**
     * @Route("/card-set/{cardSet}", name="card_set_show")
     * @param CardSet $cardSet
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(CardSet $cardSet)
    {
        return $this->render('card-set/show.html.twig', ['cardSet' => $cardSet]);
    }
}
