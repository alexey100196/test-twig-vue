<?php


namespace App\Tests\Helper;


use App\Helper\Math;
use PHPUnit\Framework\TestCase;

class MathTest extends TestCase
{
    public function getMulParamsWithResults()
    {
        yield [10, 20, 200];
        yield [-1, 2, -2];
        yield [0, 0, 0];
    }

    /**
     * @dataProvider getMulParamsWithResults
     */
    public function testMul($leftNumber, $rightNumber, $expectedResult)
    {
        $result = Math::mul($leftNumber, $rightNumber);

        $this->assertEquals($result, $expectedResult);
    }

    public function getAddParamsWithResults()
    {
        yield [2, 2, 4];
        yield [-10, 10, 0];
        yield [0, 0, 0];
    }

    /**
     * @dataProvider getAddParamsWithResults
     */
    public function testAdd($leftNumber, $rightNumber, $expectedResult)
    {
        $result = Math::add($leftNumber, $rightNumber);

        $this->assertEquals($result, $expectedResult);
    }
    
    public function getSubParamsWithResults()
    {
        yield [2, 2, 0];
        yield [-10, 10, -20];
        yield [0, 0, 0];
    }

    /**
     * @dataProvider getSubParamsWithResults
     */
    public function testSub($leftNumber, $rightNumber, $expectedResult)
    {
        $result = Math::sub($leftNumber, $rightNumber);

        $this->assertEquals($result, $expectedResult);
    }

    public function getDivParamsWithResults()
    {
        yield [2, 2, 1];
        yield [100, 10, 10];
        yield [-2, 1, -2];
    }

    /**
     * @dataProvider getDivParamsWithResults
     */
    public function testDiv($leftNumber, $rightNumber, $expectedResult)
    {
        $result = Math::div($leftNumber, $rightNumber);

        $this->assertEquals($result, $expectedResult);
    }

    public function getCompParamsWithResults()
    {
        yield [1, 2, -1];
        yield [2, -1, 1];
        yield [0, 0, 0];
    }

    /**
     * @dataProvider getCompParamsWithResults
     */
    public function testComp($leftNumber, $rightNumber, $expectedResult)
    {
        $result = Math::comp($leftNumber, $rightNumber);

        $this->assertEquals($result, $expectedResult);
    }
}