<?php

namespace App\Repository\Widget;

use App\Entity\Widget\WidgetButtonIcon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WidgetButtonIcon|null find($id, $lockMode = null, $lockVersion = null)
 * @method WidgetButtonIcon|null findOneBy(array $criteria, array $orderBy = null)
 * @method WidgetButtonIcon[]    findAll()
 * @method WidgetButtonIcon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WidgetButtonIconRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WidgetButtonIcon::class);
    }
}
