<?php

namespace App\Controller\Account\Shop;

use App\Entity\Product;
use App\Entity\Shop;
use App\Form\Shop\Product\ProductNumberType;
use App\Service\Shop\ShopProduct;
use App\Traits\Controller\FormResponseArray;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ShopProductNumberController extends AbstractController
{
    use FormResponseArray;

    /**
     * @var ShopProduct
     */
    private $shopProduct;

    /**
     * ShopProductNumberController constructor.
     * @param ShopProduct $shopProduct
     */
    public function __construct(ShopProduct $shopProduct)
    {
        $this->shopProduct = $shopProduct;
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/product/{product}/number", name="account_shop_product_number_update", methods={"PATCH"})
     * @param Request $request
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function update(Request $request, Product $product)
    {
        $shop = $this->getUser()->getShop();

        $form = $this->createForm(ProductNumberType::class, $product, ['method' => 'PATCH']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $this->shopProduct->canShopEditProduct($shop, $product)) {
            $this->shopProduct->save($form->getData());
            $this->addFlash(Consts::SUCCESS, 'Offer saved.');
        } else {
            $this->createFlashMessagesFromFormErrors($form);
        }

        return $this->redirectToRoute('account_shop_edit_product_number');
    }

    /**
     * @param array $messages
     */
    private function addFlashErrorFromArray(array $messages)
    {
        foreach ($messages as $message) {
            if (is_array($message)) {
                $this->addFlashErrorFromArray($message);
            } else {
                $this->addFlash(Consts::ERROR, $message);
            }
        }
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/edit/product-number", name="account_shop_edit_product_number", methods={"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit()
    {
        $shop = $this->getUser()->getShop();

        if ($shop->setWizardStep(10)) {
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->render('account/shop/edit-products-numer.html.twig', [
            'shop' => $shop,
        ]);
    }
}