'use strict';

function Sort() {
    // this.items = items;
    this.field = undefined;
    this.dir = 'ASC';
}

Sort.prototype.sort = function (data, field) {
    data = _.sortBy(data, function (item) {
        return field.split('.').reduce(function (obj, i) {
            return obj[i];
        }, item);
    });

    if (this.field === field) {
        if (this.dir === 'ASC') {
            this.dir = 'DESC';
            data = data.reverse();
        } else {
            this.dir = 'ASC';
        }
    } else {
        this.dir = 'ASC';
    }

    this.field = field;

    return data;
};

module.exports = Sort;