<?php

namespace App\Service\Balance\Entity;

use App\Entity\Balance;
use App\Entity\Currency;
use App\Entity\UserBalance;
use Doctrine\Common\Collections\Collection;

trait HasBalance
{
    /**
     * @return Collection|Balance[]
     */
    public function getBalances(): Collection
    {
        return $this->balances;
    }

    public function addBalance(Balance $balance): self
    {
        if (!$this->balances->contains($balance)) {
            $this->balances[] = $balance;
            $balance->setUser($this);
        }

        return $this;
    }

    public function removeBalance(Balance $balance): self
    {
        if ($this->balances->contains($balance)) {
            $this->balances->removeElement($balance);
            // set the owning side to null (unless already changed)
            if ($balance->getUser() === $this) {
                $balance->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @param string $currency
     * @return UserBalance|null
     */
    public function getCurrencyBalance(string $currency)
    {
        foreach ($this->getBalances() as $balance) {
            if ($balance->getCurrency()->getName() === $currency) {
                return $balance;
            }
        }

        return null;
    }

    /**
     * @param Currency $currency
     * @return Balance
     */
    public function findOrCreateBalance(Currency $currency): Balance
    {
        if (!$balance = $this->getCurrencyBalance($currency->getName())) {
            $balance = $this->createBalance();
            $balance->setCurrency($currency);
        }

        return $balance;
    }
}
