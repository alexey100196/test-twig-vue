<?php

namespace App\Tests\Controller;

use App\Entity\ReferrerMyStore;
use App\Entity\User;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ReferrerMyStoreControllerTest extends WebTestCase
{
    /** @var string */
    private $slug;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    protected function setUp(): void
    {
        $this->slug = bin2hex(random_bytes(8));
        $this->client = static::createClient();
        $this->em = $this->client->getKernel()->getContainer()->get('doctrine')
            ->getManager();

        $user = $this->em->getRepository(User::class)->findOneBy(['name' => 'john']);
        $referrerMyStore = new ReferrerMyStore();
        $referrerMyStore->setTitle('-');
        $referrerMyStore->setLink('http://marfil-app.local/store/'.$this->slug);

        $user->setReferrerMyStore($referrerMyStore);
        $this->em->persist($user);
        $this->em->persist($referrerMyStore);
        $this->em->flush();
    }

    /**
     * @testdox Check not found my store
     */
    public function testNotFound() {
        $client = static::createClient();
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('GET', '/store/asdasd');

        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }



}
