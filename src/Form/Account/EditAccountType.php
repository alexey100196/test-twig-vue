<?php

namespace App\Form\Account;

use App\Entity\User;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class EditAccountType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'constraints' => [
                    new Length(['max' => 24])
                ]
            ])
            ->add('surname', TextType::class, [
                'constraints' => [
                    new Length(['max' => 32])
                ]
            ])
            ->add('phone', TextType::class, [
                'required' => false,
                'constraints' => [
                    new Length(['max' => 30])
                ]
            ])
            ->add('youtube', TextType::class, [
                'required' => false,
                'constraints' => [
                    new Length(['max' => 30])
                ]
            ]);

//            ->add('ytc', TextType::class, [
//                'required' => false,
//                'constraints' => [
//                    new Length(['max' => 30])
//                ]
//            ])

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'constraints' => [
                new UniqueEntity([
                    'fields' => 'email',
                    'repositoryMethod' => 'findBy',
                    'message' => 'User with that email already exists.',
                ]),
                new UniqueEntity([
                    'fields' => 'nickname',
                    'repositoryMethod' => 'findBy',
                    'message' => 'User with that nickname already exists.',
                ]),
            ],
        ]);
    }

}
