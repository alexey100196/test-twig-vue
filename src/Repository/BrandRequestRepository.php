<?php

namespace App\Repository;

use App\Entity\BrandRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BrandRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method BrandRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method BrandRequest[]    findAll()
 * @method BrandRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BrandRequestRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BrandRequest::class);
    }

    // /**
    //  * @return BrandRequest[] Returns an array of BrandRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BrandRequest
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
