(function () {

    ValueLink = [];
    // BASE_URL = document.getElementsByName('app_url')[0].content + "/";
    BASE_URL = '//marfil-app.local/';

    var cookieName = 'ValueLink_' + ValueLinkID;
    var url = new URL(window.location.href);
    var searchParams = new URLSearchParams(url.search);
    var newReferrerNumber = searchParams.get('siReferrer');
    var getReferrer = newReferrerNumber ? newReferrerNumber : SICookie.get(cookieName);

    var listener = function (arr, callback) {
        arr.push = function (e) {
            Array.prototype.push.call(arr, e);
            callback(arr);
        };
    };

    listener(ValueLink, function (SItoPush) {
        sendSI(SItoPush);
    });

    var checkPopup = function() {
        let req = new XMLHttpRequest();
        req.open('GET', BASE_URL + 'api/v1/' + ValueLinkID + '/post-order', false);
        req.send(null);
        if (req.status === 200) {
            let parsedResponse = JSON.parse(req.response);
            if (parsedResponse.enabled === true) {
                setTimeout( function() {
                    if (document.body.offsetWidth < 401) {
                        document.querySelector('#iframeButton').click()
                    } else {
                        let iframeWrapper = document.querySelector('#iframeWrapper')
                        document.body.style.overflowY = 'hidden'
                        iframeWrapper.style.border = '1px solid black'
                        iframeWrapper.querySelector('.closeWrapper').style.border = 'none'
                        iframeWrapper.style.transition = 'none'
                        iframeWrapper.style.right = '50%'
                        iframeWrapper.style.left = 'unset'
                        iframeWrapper.style.bottom = '30px'
                        iframeWrapper.style.top = '30px'
                        iframeWrapper.style.overflowY = 'hidden'
                        iframeWrapper.style.height = 'calc(100% - 60px)'
                        iframeWrapper.style.width = '410px'
                        iframeWrapper.style.transform = 'translateX(50%)'
                        iframeWrapper.querySelector('iframe').style.borderLeft = 'none'
                        iframeWrapper.querySelector('iframe').style.width = '100%'
                        iframeWrapper.querySelector('iframe').style.height = 'calc(100% - 31px)'

                        let background = document.createElement('div')
                        background.id = 'popupBackground'
                        background.style.width = '100%'
                        background.style.height = '100%'
                        background.style.top = '0'
                        background.style.left = '0'
                        background.style.position = 'fixed'
                        background.style.zIndex = '999999'
                        background.style.backgroundColor = 'rgba(0,0,0,0.6)'
                        iframeWrapper.querySelector('.closeWrapper').addEventListener('click', () => {
                            background.style.display = 'none'
                            iframeWrapper.style.height = '100%'
                            iframeWrapper.style.top = '0'
                            iframeWrapper.style.bottom = '0'
                            iframeWrapper.style.width = '400px'
                            iframeWrapper.style.transform = 'translateX(0%)'
                            iframeWrapper.style.transition = 'all 0.3s ease'
                            document.body.style.overflowY = 'scroll'
                        })
                        background.addEventListener('click', () => {
                            iframeWrapper.querySelector('.closeWrapper').click()
                            background.style.display = 'none'
                            iframeWrapper.style.height = '100%'
                            iframeWrapper.style.top = '0'
                            iframeWrapper.style.bottom = '0'
                            iframeWrapper.style.width = '400px'
                            iframeWrapper.style.transform = 'translateX(0%)'
                            iframeWrapper.style.transition = 'all 0.3s ease'
                            document.body.style.overflowY = 'scroll'
                        })
                        document.body.appendChild(background)
                    }
                }, 1000)
            } else {
                return null
            }
        }
    }

    var sendSI = function (SItoPush, forceEvent) {
        var eventToSend = SItoPush[SItoPush.length - 1];

        if (typeof forceEvent !== "undefined") {
            eventToSend.event = forceEvent;
        }

        var sendValueLink = new XMLHttpRequest();
        sendValueLink.open('POST', BASE_URL + 'api/v1/event/' + eventToSend.event);
        sendValueLink.setRequestHeader('Content-Type', 'application/json');
        if (eventToSend.event === 'purchase') {
            if (typeof eventToSend.data == 'undefined') {
                eventToSend.data = {};
            }
            if (typeof eventToSend.data.product != 'undefined') {} else {
                eventToSend.data["product"] = siProduct;
            }
            // Uncomment to Post Order PostOrder PopUp Popover
            // checkPopup()
        }
        sendValueLink.onload = function () {
            if (sendValueLink.status === 200 || sendValueLink.status === 201) {
                // Ok
            }
            if (sendValueLink.status !== 200) {
                // Failed
            }
        };

        if (eventToSend.event == 'test-connection') {
            sendValueLink.send(
                JSON.stringify({
                    shop: ValueLinkID
                })
            );
        } else {
            if (typeof eventToSend.test !== "undefined") {
                var testCase = eventToSend.test;
            };
            if (typeof eventToSend.http_referer !== "undefined") {
                var httpReferer = eventToSend.http_referer;
            };
            sendValueLink.send(
                JSON.stringify({
                    shop: ValueLinkID,
                    referrer_link: testCase ? undefined : getReferrer,
                    payload: eventToSend.data,
                    test: testCase,
                    http_referer: httpReferer
                })
            );
        }
    };

    var getTest = searchParams.get('siTest');
    if ((getTest !== null) && (getTest !== "")) {
        ValueLink.push({
            "event": "test-connection"
        });
    }

    var getTest = searchParams.get('isCard');
    var cardCookieLifetime = "";
    if ((getTest !== null) && (getTest !== "")) {
        sendSI(ValueLink, "endpoint?isCard");
        cardCookieLifetime = "?cardCookie";
    }

    var getsiProduct = searchParams.get('siProduct');
    if ((getsiProduct !== null) && (getsiProduct !== "")) {
        var siProduct = getsiProduct;
    }

    if (newReferrerNumber) {
        var getCookieLifetime = new XMLHttpRequest();
        getCookieLifetime.open('GET', BASE_URL + 'api/v1/company/' + ValueLinkID + '/cookie' + cardCookieLifetime);
        getCookieLifetime.onload = function () {
            if (getCookieLifetime.status === 200) {
                var cookieLifetime = JSON.parse(getCookieLifetime.responseText).time;
                if (typeof getReferrer != "undefined") {
                    SICookie.set(cookieName, getReferrer, cookieLifetime);
                }
            }
        };
        getCookieLifetime.send();
    };

    SIParams = ['siReferrer', 'siTest', 'isCard', 'siProduct'];
    SIParams.forEach(element => {
        searchParams.delete(element);
        history.replaceState(null, '', '?' + searchParams + location.hash)
    });

    var cleanURL = window.location.href.split('?');
    if (!cleanURL[1]) {
        window.history.replaceState(null, null, window.location.pathname)
    }

    if (typeof ValueLinkOnLoad !== 'undefined' && ValueLinkOnLoad.length > 0) {
        ValueLinkOnLoad.forEach((el) => {
            ValueLink.push(el)
        })
    }

})();


