<?php


namespace App\Tests\Service\Referrer;


use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\User;
use App\Entity\UserShop;
use App\Manager\UserManager;
use App\Service\Referrer\ReferrerShopLink;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class ReferrerShopLinkTest extends TestCase
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var ReferrerShopLink
     */
    protected $referrerShopLink;

    protected function setUp(): void
    {
        $this->em = $this->createMock(EntityManagerInterface::class);
        $this->referrerShopLink = new ReferrerShopLink($this->em);
    }



    public function testValidRemove()
    {
        $user = $this->createMock(User::class);

        $referrerLink = new ReferrerLink();
        $referrerLink->setReferrer($user);

        $this->assertTrue($this->referrerShopLink->remove($user, $referrerLink));
    }

    public function testInvalidRemove()
    {
        $referrer = $this->createMock(User::class);
        $anyOtherUser = $this->createMock(User::class);

        $referrerLink = new ReferrerLink();
        $referrerLink->setReferrer($referrer);

        $this->assertFalse($this->referrerShopLink->remove($anyOtherUser, $referrerLink));
    }

    public function testValidCanUserCreateShopLink()
    {
        $user = new User;

        $userShop = new UserShop();
        $userShop->setUser($user);

        $shop = new Shop();
        $shop->addUserShop($userShop);

        $this->assertTrue($this->referrerShopLink->canUserCreateShopLink($user, $shop));
    }


    public function testInvalidCanUserCreateShopLink()
    {
        $user = new User;

        $userShop = new UserShop();
        $userShop->setUser($user);

        $this->assertFalse($this->referrerShopLink->canUserCreateShopLink($user, new Shop()));
    }

}