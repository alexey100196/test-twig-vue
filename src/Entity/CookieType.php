<?php


namespace App\Entity;


class CookieType
{
    private const prefix = "COOKIE_TYPE_";
    const REFERRAL = CookieType::prefix . "REFERRAL";
    const AFFILIATE = CookieType::prefix . "AFFILIATE";

    public static function Types()
    {
        $types = [];
        array_push($types, self::REFERRAL, self::AFFILIATE);
        return $types;
    }
}