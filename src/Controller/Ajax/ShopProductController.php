<?php

namespace App\Controller\Ajax;

use App\Controller\AjaxAbstractController;
use App\Util\Context;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ShopIncomeController
 *
 * @Security("has_role('ROLE_SHOP')")
 */
class ShopProductController extends AjaxAbstractController
{
    /**
     * @Route("/ajax/company/products", name="ajax_shop_products_index")
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function index()
    {
        return $this->jsonResponse($this->getUser()->getShop()->getProducts(), [Context::SHOP]);
    }
}