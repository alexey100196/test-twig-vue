<?php

namespace App\Entity;

use App\Entity\BaseGlossary;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShopStatusRepository")
 */
class ShopStatus extends BaseGlossary
{
    const INACTIVE = 1;
    const ACTIVE = 2;
    const REJECTED = 3;
    const SUSPENDED = 4;
    const ACCEPTED = 5;

    const INACTIVE_NAME = 'inactive';
    const ACTIVE_NAME = 'active';
    const REJECTED_NAME = 'rejected';
    const SUSPENDED_NAME = 'suspended';
    const ACCEPTED_NAME = 'accepted';
}
