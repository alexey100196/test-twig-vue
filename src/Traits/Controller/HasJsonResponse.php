<?php


namespace App\Traits\Controller;


use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;

trait HasJsonResponse
{
    /**
     * @param mixed $data
     * @param array $contextGroups
     * @return JsonResponse
     */
    protected function jsonResponse($data, array $contextGroups = []): JsonResponse
    {
        $serializer = SerializerBuilder::create()->build();
        $context = $contextGroups ? SerializationContext::create()->setGroups($contextGroups) : null;

        return new JsonResponse(json_decode($serializer->serialize($data, 'json', $context)));
    }
}