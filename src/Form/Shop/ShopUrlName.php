<?php


namespace App\Form\Shop;


use App\Entity\Shop;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Regex;

class ShopUrlName extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('urlName', TextType::class, [
                'label' => 'Company Superinterface Name',
                'attr' => [
                    'placeholder' => 'Company or Brand visible in url'
                ],
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => 3, 'max' => 64]),
                    new Regex('/^[a-zA-Z0-9\-\_]+$/')
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Shop::class,
            'constraints' => [
                new UniqueEntity([
                    'fields' => ['urlName'],
                    'repositoryMethod' => 'findBy',
                    'message' => 'Given subdomain is taken.',
                ]),
            ],
        ]);
    }
}
