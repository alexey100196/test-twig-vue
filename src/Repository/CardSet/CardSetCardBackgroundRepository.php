<?php

namespace App\Repository\CardSet;

use App\Entity\CardSet\CardSetCardBackground;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CardSetCardBackground|null find($id, $lockMode = null, $lockVersion = null)
 * @method CardSetCardBackground|null findOneBy(array $criteria, array $orderBy = null)
 * @method CardSetCardBackground[]    findAll()
 * @method CardSetCardBackground[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CardSetCardBackgroundRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CardSetCardBackground::class);
    }

    // /**
    //  * @return CardSetCardBackground[] Returns an array of CardSetCardBackground objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CardSetCardBackground
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
