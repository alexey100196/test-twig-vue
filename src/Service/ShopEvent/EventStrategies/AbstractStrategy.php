<?php


namespace App\Service\ShopEvent\EventStrategies;


use App\Entity\EventEntry;
use App\Manager\EventEntryManager;
use Symfony\Bridge\Doctrine\RegistryInterface;

abstract class AbstractStrategy
{
    /**
     * @var EventEntry
     */
    protected $eventEntry;

    /**
     * @var Registry
     */
    protected $doctrine;

    /**
     * @var EventEntryManager
     */
    protected $eventEntryManager;

    /**
     * AbstractStrategy constructor.
     * @param RegistryInterface $doctrine
     * @param EventEntryManager $eventEntryManager
     */
    public function __construct(RegistryInterface $doctrine, EventEntryManager $eventEntryManager)
    {
        $this->doctrine = $doctrine;
        $this->eventEntryManager = $eventEntryManager;
    }

    /**
     * @param EventEntry $eventEntry
     */
    public function setEventEntry(EventEntry $eventEntry)
    {
        $this->eventEntry = $eventEntry;
    }
}