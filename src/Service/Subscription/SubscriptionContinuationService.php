<?php


namespace App\Service\Subscription;


use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class SubscriptionContinuationService
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function userWantContinue(User $user)
    {
        $this->changeDecision($user, true);
    }

    public function userDoesNotWantContinue(User $user)
    {
        $this->changeDecision($user, false);
    }

    private function changeDecision(User $user, bool $wantContinueSubscription)
    {
        $user->setWantContinueSubscription($wantContinueSubscription);

        $this->em->flush();
    }
}