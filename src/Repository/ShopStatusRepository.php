<?php

namespace App\Repository;

use App\Entity\ShopStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopStatus[]    findAll()
 * @method ShopStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopStatusRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopStatus::class);
    }

//    /**
//     * @return ShopStatus[] Returns an array of ShopStatus objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ShopStatus
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
