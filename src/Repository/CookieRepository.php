<?php

namespace App\Repository;

use App\Entity\Cookie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


class CookieRepository extends ServiceEntityRepository
{
    /**
     * @var ManagerRegistry
     */
    private $registry;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cookie::class);
        $this->registry = $registry;
    }
}
