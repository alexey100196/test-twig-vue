import axios from 'axios'
import {config} from '../configWidget'
import Fingerprint2 from 'fingerprintjs2'

export default class FetchApi {
  constructor () {
    axios.defaults.withCredentials = true
    this.request = axios.create({
      baseURL: process.env.NODE_ENV === 'development' ? config.apiURL.dev : config.apiURL.production,
      withCredentials: true
    })

    this.request.interceptors.response.use((response) => {
      return response
    }, function (error) {
      return Promise.reject(error)
    })

    this.env = process.env.NODE_ENV
    this.compnayID = this.env === 'development' ? 'E1E4DECD9699E14667EB' : window.companyId
    this.widgetLanguage = 'pl'
  }

  formDataCreator (obj) {
    let formData = new FormData()

    for (let key in obj) {
      formData.append(key, obj[key])
    }

    return formData
  }

  checkEmail ({ email }) {
    const reqBody = {
      email,
      companyId: window.companyId
    }
    const formData = this.formDataCreator(reqBody)

    return new Promise((resolve, reject) => {
      this.request.post('/api/v1/check/email', formData, {
        headers: {
          'widget-language': this.widgetLanguage,
          'Access-Control-Allow-Credentials': true
        }
      })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error.response)
        })
    })
  }

  register ({name, surname, email, password}) {
    let requestbody = {
      name: name,
      surname: surname,
      email: email,
      'plainPassword[first]': password,
      'plainPassword[second]': password
    }

    let formData = this.formDataCreator(requestbody)

    return new Promise((resolve, reject) => {
      this.request.post('/widget/referrer/register/financial', formData, {headers: {'widget-language': this.widgetLanguage}})
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error.response)
        })
    })
  }

  login ({email, password}) {
    let requestbody = {username: email, password}

    return new Promise((resolve, reject) => {
      this.request.post('/login/ajax', JSON.stringify(requestbody), {
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then((response) => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }

  registerVerify ({user, token}) {
    let requestbody = {
      user: user,
      token: token
    }

    let formData = this.formDataCreator(requestbody)

    return new Promise((resolve, reject) => {
      this.request.post('/widget/register/verify-email', formData)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error.response)
        })
    })
  }

  registerLight ({name, email}) {
    let requestbody = {}

    if (name) {
      requestbody = {
        name: name,
        email: email
      }
    } else {
      requestbody = {
        email: email
      }
    }

    let formData = this.formDataCreator(requestbody)

    return new Promise((resolve, reject) => {
      this.request.post('/widget/referrer/register/basic', formData, {headers: {'widget-language': this.widgetLanguage}})
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error.response)
        })
    })
  }

  getWidgetConfig () {
    return new Promise((resolve, reject) => {
      this.request.get('/widget/iframe-info/' + this.compnayID)
        .then((response) => {
          this.widgetLanguage = response.data.language
          resolve(response)
        })
        .catch((error) => {
          reject(error.response)
        })
    })
  }

  generateLink (chosenReward, url = null, registerEmail = null, parentMeta = null) {
    let data = {
      type: chosenReward
    }
    if (url) {
      data.url = url
    }
    if (registerEmail) {
      data.referrer_email = registerEmail
    }

    if (parentMeta) {
      data.image = parentMeta.image || null
      data.description = parentMeta.description || null
    }

    let that = this
    return new Promise((resolve, reject) => {
      Fingerprint2.getPromise().then(function (components) {
        const values = components.map(function (component) {
          return component.value
        })
        const murmur = Fingerprint2.x64hash128(values.join(''), 31)
        data.fingerprint = murmur

        that.request.post('/widget/referring/' + that.compnayID + '/generate-link', {...data})
          .then((response) => {
            resolve(response)
          })
          .catch((error) => {
            reject(error.response)
          })
      })
    })
  }

  sendEmail (email, link, refereremail = null) {
    let data = {
      email: email,
      url: link
    }
    if (refereremail) {
      data.referrer_email = refereremail
    }
    return new Promise((resolve, reject) => {
      this.request.post('/widget/referring/' + this.compnayID + '/invite-friend/email', {...data})
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error.response)
        })
    })
  }

  getMeta (link) {
    const reqBody = {
      link,
      companyId: window.companyId
    }
    const formData = this.formDataCreator(reqBody)

    return new Promise((resolve, reject) => {
      this.request.post('/api/v1/meta', formData, {
        headers: {
          'Content-Type': 'text/html; charset=UTF-8',
          'Access-Control-Allow-Origin': '*'
        }
      })
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error.response)
        })
    })
  }

  getTranslation () {
    return new Promise((resolve, reject) => {
      this.request.get('/widget/content/' + this.compnayID)
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error.response)
        })
    })
  }

  getSpecialOffer () {
    return new Promise((resolve, reject) => {
      this.request.get('/widget/shop/' + this.compnayID + '/products')
        .then((response) => {
          resolve(response)
        })
        .catch((error) => {
          reject(error.response)
        })
    })
  }
}
