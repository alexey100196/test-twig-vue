<?php

namespace App\Repository;

use App\Entity\Currency;
use App\Entity\User;
use App\Entity\UserBalance;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserBalance|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserBalance|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserBalance[]    findAll()
 * @method UserBalance[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserBalanceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserBalance::class);
    }

    public function findOneByUserAndCurrency(User $user, Currency $currency): ?UserBalance
    {
        return $this->findOneBy([
            'user' => $user,
            'currency' => $currency,
        ]);
    }
}
