<?php


namespace App\DataFixtures;


use App\Entity\TransactionType;

class TransactionTypeFixtures extends BaseGlossaryFixtures
{
    public function getData(): array
    {
        return [
            1 => 'balance_correction',
            2 => 'shop_budget',
            3 => 'shop_deposit',
            4 => 'shop_conversion_referrer_profit',
            5 => 'shop_conversion_system_commission',
            6 => 'shop_conversion_referrer_profit_return',
            7 => 'shop_conversion_system_commission_return',
            8 => 'user_subscription',
            9 => 'user_payout',
        ];
    }

    public function getEntity()
    {
        return new TransactionType();
    }
}