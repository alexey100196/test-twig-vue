<?php

namespace App\Controller\Account\Shop;

use App\Entity\Shop;
use App\Repository\EventEntryRepository;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ShopTestConnectionController extends AbstractController
{
    /**
     * @var EventEntryRepository
     */
    protected $eventEntryRepository;

    /**
     * ShopTestConnectionController constructor.
     * @param EventEntryRepository $eventEntryRepository
     */
    public function __construct(EventEntryRepository $eventEntryRepository)
    {
        $this->eventEntryRepository = $eventEntryRepository;
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/edit/test-connection", name="account_shop_edit_test_connection", methods={"GET", "POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request)
    {
        /** @var Shop $shop */
        $shop = $this->getUser()->getShop();

        if ($shop->setWizardStep(8)) {
            $this->getDoctrine()->getManager()->flush();
        }

        if ($request->isMethod('post')) {
            if ($shop->getSnippetConnectedAt()) {
                if ($shop->hasOnlyCouponRewards()) {
                    return $this->redirectToRoute('account_shop_edit_finish');
                }

                return $this->redirectToRoute('account_shop_edit_subdomain');
            } else {
                $this->addFlash(Consts::ERROR, 'You have to finish test connection before.');
                return $this->redirectToRoute('account_shop_edit_test_connection');
            }
        }

        $testEventEntries = $this->eventEntryRepository->getLastShopEntries($shop, true);

        return $this->render('account/shop/edit-test-connection.html.twig', compact('shop', 'testEventEntries'));
    }
}