<?php


namespace App\Controller\Ajax;


use App\Controller\AjaxAbstractController;
use App\Entity\EventEntryType;
use App\Entity\Shop;
use App\Helper\UrlHelpers;
use App\Manager\ShopManager;
use App\Repository\EventEntryRepository;
use App\Repository\ShopRepository;
use App\Util\Consts;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;

class ShopEventController extends AjaxAbstractController
{
    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * @var ShopManager
     */
    private $shopManager;

    /**
     * ShopEventController constructor.
     * @param ShopManager $shopManager
     * @param ShopRepository $shopRepository
     */
    public function __construct(
        ShopManager $shopManager,
        ShopRepository $shopRepository
    )
    {
        $this->shopManager = $shopManager;
        $this->shopRepository = $shopRepository;
    }

    /**
     * @Route("/ajax/company/{shop_id}/event/test-connection/start", name="ajax_shop_event_test_connection_start")
     */
    public function startShopEventTestConnection(Request $request)
    {
        $shop = $this->shopRepository->find($request->get('shop_id'));

        if (!$shop->getWebsite()) {
            return new JsonResponse(['message' => 'Complete website url before test.'], Response::HTTP_NOT_FOUND);
        }

        $shop->setSnippetConnectedAt(null);
        $this->shopManager->save($shop);

        return new JsonResponse([
            'shop_url' => UrlHelpers::addGetParam($shop->getWebsite(), Consts::SNIPPET_TEST_PARAM_NAME, 'test-connection'),
            'check_url' => $this->generateUrl('ajax_shop_event_test_connection_check', ['shop_id' => $shop->getId()]),
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/ajax/company/{shop_id}/event/test-connection/check", name="ajax_shop_event_test_connection_check")
     */
    public function checkShopEventTestConnection(Request $request)
    {
        $shop = $this->shopRepository->find($request->get('shop_id'));

        if ($shop->getSnippetConnectedAt()) {
            return new JsonResponse(['message' => 'Thank You. Your snippet has been successfully verified.'], Response::HTTP_OK);
        }

        return new JsonResponse([], Response::HTTP_ACCEPTED);
    }
}