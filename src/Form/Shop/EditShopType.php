<?php

namespace App\Form\Shop;

use App\Entity\Media;
use App\Entity\Shop;
use App\Entity\User;
use App\Form\Type\Shop\ShopTypeType;
use App\Form\Type\Shop\LogoType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Validator\Constraints as Assert;

class EditShopType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Company Name',
                'attr' => [
                    'placeholder' => 'Company or Brand'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Company description',
                'attr' => [
                    'placeholder' => '200 characters max. Short description of your business. Will be displayed on referral page.',
                    'maxlength' => '200'
                ]
            ])
            ->add('website', UrlType::class, [
                'label' => 'Company Website URL',
                'attr' => [
                    'placeholder' => 'e.g. www.companyname.com'
                ]
            ])
            ->add('service', TextType::class, [
                'label' => 'Product or Service categories',
                'attr' => [
                    'placeholder' => 'Sport Wear Clothes, Sport Equipment',
                    'maxlength' => '100'
                ]
            ])
            ->add('color', TextType::class, [
                'required' => false,
                'constraints' => [
                    new NotNull(['message' => 'Please specify Your color']),
                ]
            ])
            ->add(
                $builder
                    ->create('user', FormType::class, ['by_reference' => User::class, 'data_class' => User::class])
                    ->add('phone', TextType::class, [
                        'label' => 'Phone number',
                        'constraints' => [
                            new Length(['min' => 6, 'max' => 30]),
                        ]
                    ])
            )
            ->add('logoFile', FileType::class, [
                'constraints' => [
                    new NotNull([
                        'message' => 'Please add your logo',
                        'groups' => array('LOGO_IS_NULL_RULES'),
                    ]),
                    new Assert\Image([
                        'maxSize' => '1M',
                        'maxSizeMessage' => 'Max logo size is 1MB',
                    ])
                ],
                'invalid_message' => 'Invalid uploaded logo',
                'mapped' => false
            ])
            ->add('logo', TextType::class, [
                'mapped' => false
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Shop::class,
            'validation_groups' => function (FormInterface $form) {
                $groups = ['Default'];
                $data = $form->getData();

                if (!$data->getLogo()) { // check shop has saved logo
                    $groups[] = 'LOGO_IS_NULL_RULES';
                }

                return $groups;
            }
        ]);
    }
}
