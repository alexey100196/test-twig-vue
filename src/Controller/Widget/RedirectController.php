<?php

namespace App\Controller\Widget;

use App\Controller\AjaxAbstractController;
use App\Entity\ReferrerCoupon;
use App\Entity\Widget\WidgetReferenceLink;
use App\Entity\Widget\WidgetShopSetting;
use App\Repository\Coupon\CouponRepository;
use App\Repository\Coupon\UserCouponRepository;
use App\Service\Coupon\UseCoupon;
use App\Service\Widget\InviteFriendByEmailService;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use App\Service\Referrer\ReferrerReflink;

/**
 * @Route("/redirect")
 */
class RedirectController extends AjaxAbstractController
{
    /**
     * @Route("/{slug}", name="widget_redirect", methods={"GET"})
     *
     * @param Request $request
     * @param WidgetReferenceLink $widgetReferenceLink
     * @param ReferrerReflink $referrerReflink
     * @param SessionInterface $session
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function redirectRoute(
        Request $request,
        WidgetReferenceLink $widgetReferenceLink,
        ReferrerReflink $referrerReflink,
        InviteFriendByEmailService $inviteFriendByEmailService,
        SessionInterface $session
    )
    {
        $shop = $widgetReferenceLink->getShop();
        if (!$shop || null === $settings = $shop->getWidgetSetting()) {
            return $this->redirectToRoute('home_index');
        }

        $url = $widgetReferenceLink->getShareableDestinationUrl();
        if (
            ($widgetReferenceLink->getType() === WidgetReferenceLink::TYPE_COUPON_REWARD) &&
            ($settings->getReferrerGetCouponRewardFlag() === WidgetShopSetting::REFERRER_GET_COUPON_REWARD_FLAG_REDIRECT)
        ) {
            $cookieName = WidgetReferenceLink::COUPON_REWARD_COOKIE_PREFIX.$widgetReferenceLink->getSlug();
            $cookieVal = (isset($_COOKIE[$cookieName]) && (int) $_COOKIE[$cookieName] > 0) ? (int) $_COOKIE[$cookieName] : 0;
            if ($cookieVal <= 0) {
                $inviteFriendByEmailService->sendCouponCodeToReferrer($shop, $widgetReferenceLink->getUser());
            }
            setcookie($cookieName, ($cookieVal + 1));
        }

        return $this->redirect($url);
    }
}