<?php

namespace App\Form\Shop;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;

class EditShopCookieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cookie_lifetime', IntegerType::class, [
                'constraints' => [
                    new GreaterThan(0),
                    new LessThanOrEqual(90),
                ]
            ])
            ->add('banner_cookie_lifetime', IntegerType::class, [
                'constraints' => [
                    new GreaterThan(0),
                    new LessThanOrEqual(90),
                ]
            ])
            ->add('custom_cookie_lifetime', TextType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new GreaterThan(0),
                    new LessThanOrEqual(90),
                ]
            ])
            ->add('custom_banner_cookie_lifetime', TextType::class, [
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new GreaterThan(0),
                    new LessThanOrEqual(90),
                ]
            ]);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();

            if ($data['custom_cookie_lifetime'] ?? false) {
                $data['cookie_lifetime'] = $data['custom_cookie_lifetime'];
                unset($data['custom_cookie_lifetime']);
            }

            if ($data['custom_banner_cookie_lifetime'] ?? false) {
                $data['banner_cookie_lifetime'] = $data['custom_banner_cookie_lifetime'];
                unset($data['custom_banner_cookie_lifetime']);
            }

            $event->setData($data);
        });
    }

}
