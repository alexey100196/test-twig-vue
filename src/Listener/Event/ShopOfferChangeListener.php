<?php


namespace App\Listener\Event;


use App\Adapter\MailSender;
use App\Entity\Shop;
use App\Entity\User;
use App\Event\Shop\ShopChangedOffer;

class ShopOfferChangeListener
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var MailSender
     */
    private $mailSender;

    public function __construct(\Twig_Environment $twig, MailSender $mailSender)
    {
        $this->twig = $twig;
        $this->mailSender = $mailSender;
    }

    public function onOfferChange(ShopChangedOffer $event)
    {
        $shop = $event->getShop();
        foreach ($shop->getUserShops() as $userShop) {
            $this->sendEmailToReferrer($shop, $userShop->getUser());
        }
    }

    private function sendEmailToReferrer(Shop $shop, User $user)
    {
        $this->mailSender->send(
            'hello@superinterface.com',
            $user->getEmail(),
            sprintf('Shop %s changed his offer.', $shop->getName()),
            $this->twig->render('email/shop/changed-offer.html.twig', compact('shop', 'user'))
        );
    }
}