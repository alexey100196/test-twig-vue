<?php


namespace App\Form\Referrer\Parts\ReferrerLinkCard;

use App\ValueObject\Font;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FontType extends AbstractType
{
    /**
     * @return array|Font[}
     */
    private function getFonts(): array
    {
        return array_map(function ($font) {
            return new Font($font);
        }, Font::FONTS);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => $this->getFonts(),
            'choice_label' => 'name',
            'choice_value' => 'name',
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}