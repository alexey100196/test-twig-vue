<?php

namespace App\Repository;

use App\Entity\CommissionType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CommissionType|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommissionType|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommissionType[]    findAll()
 * @method CommissionType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommissionTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CommissionType::class);
    }

//    /**
    //     * @return CommissionType[] Returns an array of CommissionType objects
    //     */
    /*
    public function findByExampleField($value)
    {
    return $this->createQueryBuilder('c')
    ->andWhere('c.exampleField = :val')
    ->setParameter('val', $value)
    ->orderBy('c.id', 'ASC')
    ->setMaxResults(10)
    ->getQuery()
    ->getResult()
    ;
    }
     */

    /*
public function findOneBySomeField($value): ?CommissionType
{
return $this->createQueryBuilder('c')
->andWhere('c.exampleField = :val')
->setParameter('val', $value)
->getQuery()
->getOneOrNullResult()
;
}
 */
}
