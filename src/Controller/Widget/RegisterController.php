<?php


namespace App\Controller\Widget;

use App\Controller\AjaxAbstractController;
use App\Entity\User;
use App\Form\Widget\FormData\ReferrerRegisterEmailVerification;
use App\Form\Widget\ReferrerRegisterBasicType;
use App\Form\Widget\ReferrerRegisterEmailVerificationType;
use App\Form\Widget\ReferrerRegisterFinancialType;
use App\Repository\UserRepository;
use App\Service\Widget\RegisterReferrerService;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/widget")
 */
class RegisterController extends AjaxAbstractController
{
    private $registerReferrerService;

    public function __construct(
        RegisterReferrerService $registerReferrerService
    )
    {
        $this->registerReferrerService = $registerReferrerService;
    }

    /**
     * @Route("/referrer/register/basic", name="widget_referrer_register_basic", methods={"POST"})
     * @param Request $request
     * @param UserRepository $userRepository
     * @return JsonResponse
     * @throws \Exception
     */
    public function registerReferrerBasic(Request $request, UserRepository $userRepository)
    {
        $headers = ['Access-Control-Allow-Credentials' => 'true'];
        $form = $this->createForm(ReferrerRegisterBasicType::class);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse($this->getFormErrorMessages($form), 400);
        }

        /** @var User $data */
        $data = $form->getData();
        $user = $userRepository->findOneBy(['email' => $data->getEmail()]);
        if (null === $user) {
            $this->registerReferrerService->createBasicAccount(
                $data->getEmail(),
                $data->getName(),
                $request->headers->get('widget-language', 'en')
            );
            return new JsonResponse(null, Response::HTTP_CREATED);
        }
        return new JsonResponse( Response::HTTP_OK, $headers);
    }

    /**
     * @Route("/referrer/register/financial", name="widget_referrer_register_financial", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function registerReferrerFinancial(Request $request)
    {
        $form = $this->createForm(ReferrerRegisterFinancialType::class);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse($this->getFormErrorMessages($form), 400);
        }

        /** @var User $data */
        $data = $form->getData();

        $this->registerReferrerService->createFinancialAccount(
            $data->getEmail(),
            $data->getName(),
            $data->getSurname(),
            $data->getPlainPassword(),
            $request->headers->get('widget-language', 'en')
        );

        return new JsonResponse();
    }

    /**
     * @Route("/register/verify-email", name="widget_register_verify_email", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function confirmEmail(Request $request)
    {
        $form = $this->createForm(ReferrerRegisterEmailVerificationType::class);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse($this->getFormErrorMessages($form), 400);
        }

        try {
            $this->registerReferrerService->activateUserAccountByToken(
                $form->getData()->getUser(),
                $form->getData()->getToken()
            );
        } catch (\LogicException $e) {
            return new JsonResponse(['message' => $e->getMessage()], 400);
        }

        return new JsonResponse(['message' => 'Thank you for registration. Please login to your account.']);
    }

}