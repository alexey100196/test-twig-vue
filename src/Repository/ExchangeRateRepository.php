<?php

namespace App\Repository;

use App\Entity\ExchangeRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ExchangeRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExchangeRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExchangeRate[]    findAll()
 * @method ExchangeRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExchangeRateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ExchangeRate::class);
    }

    /**
     * @param string $firstCurrency
     * @param string $secondCurrency
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findForCurrencies(string $firstCurrency, string $secondCurrency)
    {
        return $this->createQueryBuilder('e')
            ->where('e.quotedCurrency = :first AND e.baseCurrency = :second')
            ->orWhere('e.quotedCurrency = :second AND e.baseCurrency = :first')
            ->setParameters(['first' => $firstCurrency, 'second' => $secondCurrency])
            ->getQuery()
            ->getOneOrNullResult();
    }
}
