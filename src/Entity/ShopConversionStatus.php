<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShopConversionStatusRepository")
 */
class ShopConversionStatus extends BaseGlossary
{
    const UNVERIFIED = 1;
    const ACCEPTED = 2;
    const REJECTED = 3;

    public static function getAll()
    {
        return [
            self::UNVERIFIED,
            self::ACCEPTED,
            self::REJECTED,
        ];
    }
}
