<?php


namespace App\ValueObject;


use App\Entity\Currency;

class Money
{
    /**
     * @var float|int
     */
    private $amount;

    /**
     * @var Currency
     */
    private $currency;

    public function __construct(Currency $currency, $amount = 0)
    {
        $this->currency = $currency;
        $this->amount = $amount;
    }

    /**
     * @return Currency
     */
    public  function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return float|int
     */
    public function getAmount()
    {
        return $this->amount;
    }

}