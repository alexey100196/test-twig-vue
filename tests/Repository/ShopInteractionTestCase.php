<?php


namespace App\Tests\Repository;


use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\ShopConversion;
use App\Entity\ShopInteraction;
use App\Entity\ShopPurchase;
use App\Entity\ShopSignUp;
use App\Entity\ShopView;
use App\Entity\User;
use App\Entity\UserShop;
use App\Service\SystemCommission\ShopCommission\Applier;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ShopInteractionTestCase extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var Shop
     */
    protected $shop;

    /**
     * @var User
     */
    protected $referrer;

    /**
     * @var User
     */
    protected $anotherReferrer;

    /**
     * @var ReferrerLink
     */
    protected $referrerLink;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $schemaTool = new SchemaTool($this->em);

        $classes = [
            $this->em->getClassMetadata(UserShop::class),
            $this->em->getClassMetadata(ShopPurchase::class),
            $this->em->getClassMetadata(ShopSignUp::class),
            $this->em->getClassMetadata(ShopView::class),
            $this->em->getClassMetadata(ShopConversion::class),
            $this->em->getClassMetadata(ShopInteraction::class),
        ];

        try {
            $schemaTool->dropSchema($classes);
            $schemaTool->createSchema($classes);
        } catch (\Exception $e) {

        }

        $this->shop = $this->em->getRepository(Shop::class)->findOneBy(['name' => 'Demo']);
        $this->referrer = $this->em->getRepository(User::class)->findOneBy(['nickname' => 'john']);
        $this->anotherReferrer = $this->em->getRepository(User::class)->findOneBy(['nickname' => 'kate']);

        $this->em->persist((new UserShop())->setShop($this->shop)->setUser($this->referrer));
        $this->em->persist((new UserShop())->setShop($this->shop)->setUser($this->anotherReferrer));

        $this->referrerLink = (new ReferrerLink())
            ->setReferrer($this->referrer)
            ->setShop($this->shop)
            ->setShopUrlName($this->shop->getUrlName())
            ->setSlug('anyslug')
            ->setDestinationUrl('https://google.com/');

        $this->em->persist($this->referrerLink);

        $this->em->flush();
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null;
    }

    protected function createPurchase($user, $price, $amount, $commission)
    {
        // Prepare shop purchase
        $shopPurchase = (new ShopPurchase())
            ->setShop($this->shop)
            ->setPrice($price)
            ->setAmount($amount)
            ->setReferrerLink($this->referrerLink)
            ->setCommission($commission)
            ->setCommissionType($this->shop->getCpsOffer()->getCommissionType())
            ->setOfferType($this->shop->getCpsOffer()->getType())
            ->setSourceLink($this->shop->getWebsite());

        $shopPurchase->setReferrerProfit(
            $this->shop
                ->getCpsOffer()
                ->calculateCommissionValue($shopPurchase->getPrice(), $shopPurchase->getAmount())
        );

        $shopPurchase->setReferrer($user);

        // Apply system commission. It is common for each shop.
        $feeApplier = new Applier(1);
        $taxedAmount = $feeApplier->apply($shopPurchase->getShop(), $shopPurchase->getTotal());
        $shopPurchase->setSystemCommission($taxedAmount->getTax());

        $this->em->persist($shopPurchase);
    }

    protected function createSignUp($user)
    {
        $shopSignUp = new ShopSignUp();
        $shopSignUp->setShop($this->shop);
        $shopSignUp->setReferrerLink($this->referrerLink);

        $shopSignUp->setCommission($this->shop->getCplOffer()->getCommission());
        $shopSignUp->setCommissionType($this->shop->getCplOffer()->getCommissionType());
        $shopSignUp->setReferrerProfit($this->shop->getCplOffer()->getCommission());
        $shopSignUp->setOfferType($this->shop->getCplOffer()->getType());
        $shopSignUp->setReferrer($user);
        $shopSignUp->setSourceLink($this->shop->getWebsite());

        // Apply system commission. It is common for each shop.
        $feeApplier = new Applier(1);
        $taxedAmount = $feeApplier->apply($shopSignUp->getCommission(), $shopSignUp->getShop()->getSystemCommission());
        $shopSignUp->setSystemCommission($taxedAmount->getTax());

        $this->em->persist($shopSignUp);
    }

    protected function createView($user)
    {
        $shopView = new ShopView();
        $shopView->setShop($this->shop);
        $shopView->setReferrerLink($this->referrerLink);
        $shopView->setReferrer($user);

        $this->em->persist($shopView);
    }
}
