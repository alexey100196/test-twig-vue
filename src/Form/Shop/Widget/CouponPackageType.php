<?php

namespace App\Form\Shop\Widget;

use App\Entity\Coupon\CouponDiscountType;
use App\Entity\Role;
use App\Form\Shop\Widget\CouponPackageType\CsvUploadType;
use App\Form\Shop\Widget\FormData\CouponPackage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\File;

class CouponPackageType extends AbstractType
{
    const ONE_TIME_OPTION = 'one_time';
    const MULTIPLE_OPTION = 'multiple';

    const COUPON_TYPES = [
        'Upload One Time Coupons' => self::ONE_TIME_OPTION,
        'Upload Multiple Usage Coupon' => self::MULTIPLE_OPTION,
    ];

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sameCouponsForInvitee', CheckboxType::class, ['label' => 'Same Coupons for Invitee'])
            ->add('referrerCouponType', ChoiceType::class, ['choices' => self::COUPON_TYPES, 'expanded' => true, 'data' => self::ONE_TIME_OPTION])
            ->add('inviteeCouponType', ChoiceType::class, ['choices' => self::COUPON_TYPES, 'expanded' => true, 'data' => self::ONE_TIME_OPTION])
            ->add('referrerCouponDiscountType', EntityType::class, [
                'class' => CouponDiscountType::class,
                'choice_value' => 'name',
            ])
            ->add('referrerCouponDiscount', NumberType::class)
            ->add('inviteeCouponDiscountType', EntityType::class, ['class' => CouponDiscountType::class, 'choice_value' => 'name'])
            ->add('inviteeCouponDiscount', NumberType::class);

        $this->addReferrerMultipleUsageCouponCode($builder);
        $this->addInviteeMultipleUsageCouponCode($builder);
        $this->addReferrerOneTimeCoupons($builder);
        $this->addInviteeOneTimeCoupons($builder);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            if (isset($data['sameCouponsForInvitee']) && $data['sameCouponsForInvitee']) {
                $data['inviteeCouponDiscountType'] = $data['referrerCouponDiscountType'] ?? '';
                $data['inviteeCouponDiscount'] = $data['referrerCouponDiscount'] ?? '';
                $data['inviteeMultipleUsageCouponCode'] = $data['referrerMultipleUsageCouponCode'] ?? '';
                $data['inviteeOneTimeCoupons'] = $data['referrerOneTimeCoupons'] ?? [];
                $data['inviteeCouponType'] = $data['referrerCouponType'] ?? [];
            }
            $event->setData($data);
        });

        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();
            $options = $form->getConfig()->getOptions();

            if (isset($data['referrerCouponDiscount'])) {
                $this->setValidationForReferralCoupon($data, $form);
            } elseif (
                ((isset($data['referrerMultipleUsageCouponCode']) || isset($data['referrerOneTimeCoupons']['csv'])) && (!$options['referer_coupons_provided'] || $options['referer_coupons_provided']) ||
                ((!isset($data['referrerMultipleUsageCouponCode']) && !isset($data['referrerOneTimeCoupons']['csv'])) && !$options['referer_coupons_provided']))
            ) {
              $form->get('referrerCouponDiscount')->addError(new FormError('Coupon value is required'));
                $this->setValidationForReferralCoupon($data, $form);
            } else {
                $this->addReferrerMultipleUsageCouponCode($form, false);
                $this->addReferrerOneTimeCoupons($form, false);

                unset($data['referrerMultipleUsageCouponCode']);
                unset($data['referrerOneTimeCoupons']);
            }

            if (!isset($data['sameCouponsForInvitee']) || !$data['sameCouponsForInvitee']) {
                if (isset($data['inviteeCouponDiscount'])) {
                    $this->setValidationForInviteeCoupon($data, $form);
                } elseif (
                    ((isset($data['inviteeMultipleUsageCouponCode']) || isset($data['inviteeOneTimeCoupons']['csv'])) && (!$options['invitee_coupons_provided'] || $options['invitee_coupons_provided']) ||
                    ((!isset($data['inviteeMultipleUsageCouponCode']) && !isset($data['inviteeOneTimeCoupons']['csv'])) && !$options['invitee_coupons_provided']))
                ) {
                   $form->get('inviteeCouponDiscount')->addError(new FormError('Coupon value is required'));
                    $this->setValidationForInviteeCoupon($data, $form);
                } else {
                    $form->remove('inviteeMultipleUsageCouponCode');
                    $this->addInviteeMultipleUsageCouponCode($form, false);
                    $this->addInviteeOneTimeCoupons($form, false);

                    unset($data['inviteeMultipleUsageCouponCode']);
                    unset($data['inviteeOneTimeCoupons']);
                }
            } else {
                $this->addInviteeMultipleUsageCouponCode($form, false);
                $this->addInviteeOneTimeCoupons($form, false);
            }
            $event->setData($data);
        });
    }

    private function addReferrerMultipleUsageCouponCode($builder, bool $required = true)
    {
        $builder->remove('referrerMultipleUsageCouponCode');
        $builder->add('referrerMultipleUsageCouponCode', null, [
            'required' => $required,
            'constraints' => $required ? [new NotBlank()] : []
        ]);
    }

    private function addInviteeMultipleUsageCouponCode($builder, bool $required = true)
    {
        $builder->remove('inviteeMultipleUsageCouponCode');
        $builder->add('inviteeMultipleUsageCouponCode', null, [
            'required' => $required,
            'constraints' => $required ? [new NotBlank()] : []
        ]);
    }

    private function addReferrerOneTimeCoupons($builder, bool $required = true)
    {
        $builder->remove('referrerOneTimeCoupons');
        $builder->add('referrerOneTimeCoupons', CsvUploadType::class, ['required' => $required]);
    }

    private function addInviteeOneTimeCoupons($builder, bool $required = true)
    {
        $builder->remove('inviteeOneTimeCoupons');
        $builder->add('inviteeOneTimeCoupons', CsvUploadType::class, ['required' => $required]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
//            'data_class' => CouponPackage::
            'invitee_coupons_provided' => false,
            'referer_coupons_provided' => false
        ]);
    }

    private function setValidationForReferralCoupon(array $data, Form $form): void
    {
        if ($data['referrerCouponType'] === self::ONE_TIME_OPTION) {
            $this->addReferrerMultipleUsageCouponCode($form, false);
            unset($data['referrerMultipleUsageCouponCode']);
        } elseif ($data['referrerCouponType'] === self::MULTIPLE_OPTION) {
            $this->addReferrerOneTimeCoupons($form, false);
            unset($data['referrerOneTimeCoupons']);
        }
    }

    private function setValidationForInviteeCoupon(array $data, Form $form): void
    {
        if ($data['inviteeCouponType'] === self::ONE_TIME_OPTION) {
            $form->remove('inviteeMultipleUsageCouponCode');
            $this->addInviteeMultipleUsageCouponCode($form, false);
            unset($data['inviteeMultipleUsageCouponCode']);
        } elseif ($data['inviteeCouponType'] === self::MULTIPLE_OPTION) {
            $this->addInviteeOneTimeCoupons($form, false);
            unset($data['inviteeOneTimeCoupons']);
        }
    }

}