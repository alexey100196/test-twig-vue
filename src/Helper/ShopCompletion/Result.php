<?php
/**
 * Created by PhpStorm.
 * User: wuer
 * Date: 17.10.2018
 * Time: 16:26
 */

namespace App\Helper\ShopCompletion;


class Result
{
    /**
     * Maximum score.
     * @var int
     */
    const MAX_SCORE = 100;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $score;


    /**
     * @return mixed
     */
    public function getScore():int
    {
        return $this->score;
    }

    /**
     * @param mixed $score
     */
    public function setScore(int $score): void
    {
        $this->score = $score;
    }

    /**
     * @return mixed
     */
    public function getName():string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isComplete():bool
    {
        return $this->getScore() == static::MAX_SCORE;
    }
}