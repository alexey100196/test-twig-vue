<?php


namespace App\Service\Referrer;


use App\Entity\ReferrerMyStore;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\UrlHelper;

class ReferrerMyStoreService
{
    /**
     * @var UrlHelper
     */
    private $urlHelper;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     * @param UrlHelper $urlHelper
     */
    public function __construct(EntityManagerInterface $em, UrlHelper $urlHelper)
    {
        $this->em = $em;
        $this->urlHelper = $urlHelper;
    }

    /**
     * @param ReferrerMyStore $referrerMyStore
     * @return ReferrerMyStore
     */
    public function save(ReferrerMyStore $referrerMyStore)
    {
        $this->em->persist($referrerMyStore);
        $this->em->flush();

        return $referrerMyStore;
    }

    /**
     * @param User $referrer
     * @param ReferrerMyStore $referrerMyStore
     * @return bool
     */
    public function remove(User $referrer, ReferrerMyStore $referrerMyStore)
    {
        if ($referrerMyStore->getId() === $referrer->getReferrerMyStore()->getId()) {
            $this->em->remove($referrerMyStore);
            $this->em->flush();

            return true;
        }

        return false;
    }

    public function generate(User $user, ReferrerMyStore $referrerMyStore) {
        $url = $this->urlHelper->getAbsoluteUrl('/store/'.bin2hex(random_bytes(8)));

        $user->setReferrerMyStore($referrerMyStore);
        $referrerMyStore->setLink($url);
        $this->em->persist($user);
        $this->em->persist($referrerMyStore);
        $this->em->flush();

        return $referrerMyStore;
    }

    /**
     * Load data from array to ReferrerMyStore object.
     * @param User $referrer
     * @param array $data
     * @param string $sAvatar
     * @return ReferrerMyStore
     */
    public function saveOrCreateFromArray(User $referrer, Form $form, $sAvatar = null) {
        $isFirst = false;
        $referrerMyStore = $referrer->getReferrerMyStore();
        if (!$referrerMyStore || !$this->em->contains($referrerMyStore)) {
            $isFirst = true;
            $referrerMyStore = new ReferrerMyStore();
        }

        if ($avatar = $form->get('avatar')->getData()) {
            if (is_string($avatar)) {
                $referrerMyStore->setAvatar($avatar);
            } else {
                $base64 = base64_encode(file_get_contents($avatar));
                $mimeType = $avatar->getMimeType();
                $referrerMyStore->setAvatar('data:'.$mimeType.';base64,'.$base64);
            }
        } elseif ($sAvatar) {
            $referrerMyStore->setAvatar($sAvatar);
        } else {
            $referrerMyStore->setAvatar(null);
        }

        $referrerMyStore->setTitle($form->get('title')->getData());
        $referrerMyStore->setSubtitle($form->get('subtitle')->getData());

        $contactUrls = $form->get('contactUrls')->getData();
        $referrerMyStore->setFbMessenger($contactUrls['fb_messenger']);
        $referrerMyStore->setMail($contactUrls['mail']);
        $referrerMyStore->setSkype($contactUrls['skype']);
        $referrerMyStore->setWhatsapp($contactUrls['whatsapp']);
        $referrerMyStore->setWhatsappTitle($contactUrls['whatsapp_title']);
        unset($contactUrls);

        $socials = $form->get('socials')->getData();
        $referrerMyStore->setFacebook($socials['facebook']);
        $referrerMyStore->setInstagram($socials['instagram']);
        $referrerMyStore->setLinkedin($socials['linkedin']);
        $referrerMyStore->setTwitter($socials['twitter']);
        $referrerMyStore->setYoutube($socials['youtube']);
        unset($socials);

        $config = $form->get('config')->getData();
        $referrerMyStore->setLink($config['link']);
        $referrerMyStore->setTemplate($config['template']);

        $pixels = $config['pixels'];
        unset($config);
        $referrerMyStore->setFacebookPixel((int) $pixels['facebook']);
        $referrerMyStore->setTwitterPixel((int) $pixels['twitter']);
        $referrerMyStore->setPinterestPixel((int) $pixels['pinterest']);
        $referrerMyStore->setLinkedinPixel((int) $pixels['linkedin']);
        $referrerMyStore->setGooglePixel((int) $pixels['google']);
        $referrerMyStore->setQuoraPixel((int) $pixels['quora']);
        unset($pixels);

        if ($isFirst) {
            $referrer->setReferrerMyStore($referrerMyStore);
            $this->em->persist($referrer);
        }
        $this->em->persist($referrerMyStore);
        $this->em->flush();

        return $referrerMyStore;
    }

    /**
     * @param ReferrerMyStore|null $referrerMyStore
     * @return array
     */
    public function objectToArray(?ReferrerMyStore $referrerMyStore = null) {
        if (!$referrerMyStore) return [
            'title' => '',
            'subtitle' => '',
            'avatar' => '',
            'config' => [
                'link' => '',
                'template' => '',
                'pixels' => [
                    'facebook' => '',
                    'twitter' => '',
                    'pinterest' => '',
                    'linkedin' => '',
                    'google' => '',
                    'quora' => '',
                ]
            ],
            'socials' => [
                'facebook' => '',
                'instagram' => '',
                'linkedin' => '',
                'twitter' => '',
                'youtube' => '',
            ],
            'contactUrls' => [
                'fb_messenger' => '',
                'mail' => '',
                'skype' => '',
                'whatsapp' => '',
                'whatsapp_title' => ''
            ],
        ];

        return [
            'title' => $referrerMyStore->getTitle(),
            'subtitle' => $referrerMyStore->getSubtitle(),
            'avatar' => $referrerMyStore->getAvatar(),
            'config' => [
                'link' => $referrerMyStore->getLink(),
                'template' => $referrerMyStore->getTemplate(),
                'pixels' => [
                    'facebook' => $referrerMyStore->getFacebookPixel(),
                    'twitter' => $referrerMyStore->getTwitterPixel(),
                    'pinterest' => $referrerMyStore->getPinterestPixel(),
                    'linkedin' => $referrerMyStore->getLinkedinPixel(),
                    'google' => $referrerMyStore->getGooglePixel(),
                    'youtube' => $referrerMyStore->getQuoraPixel(),
                ]
            ],
            'socials' => [
                'facebook' => $referrerMyStore->getFacebook(),
                'instagram' => $referrerMyStore->getInstagram(),
                'linkedin' => $referrerMyStore->getLinkedin(),
                'twitter' => $referrerMyStore->getTwitter(),
                'youtube' => $referrerMyStore->getYoutube(),
            ],
            'contactUrls' => [
                'fb_messenger' => $referrerMyStore->getFbMessenger(),
                'mail' => $referrerMyStore->getMail(),
                'skype' => $referrerMyStore->getSkype(),
                'whatsapp' => $referrerMyStore->getWhatsapp(),
                'whatsapp_title' => $referrerMyStore->getWhatsappTitle()
            ],
        ];
    }
}