<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReferrerLinkCardRepository")
 */
class ReferrerLinkCard
{
    use TimestampableEntity;

    const DEFAULT_WIDTH = 800;
    const DEFAULT_LAYOUT = 'col';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"card-sets"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $width;

    /**
     * @ORM\Column(type="boolean")
     */
    private $socialMedia;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"card-sets"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"card-sets"})
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"card-sets"})
     */
    private $logo;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"card-sets"})
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"card-sets"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"card-sets"})
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"card-sets"})
     */
    private $iframe;

    /**
     * @ORM\Column(type="string", length=12)
     * @Groups({"card-sets"})
     */
    private $layout;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ReferrerLink", mappedBy="card", cascade={"persist", "remove"})
     */
    private $referrerLink;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $showShopLogo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $showCompanyName;

    /**
     * @ORM\Column(type="boolean", nullable=true, name="show_3d_effect")
     */
    private $show3DEffect;

    /**
     * @ORM\Column(type="boolean", nullable=true, name="show_video")
     */
    private $showVideo;

    /**
     * @ORM\Column(type="integer")
     */
    private $adminContentUpdates;

    /**
     * @ORM\Column(type="integer")
     */
    private $adminImageUpdates;

    /**
     * ReferrerLinkCard constructor.
     */
    public function __construct()
    {
        $this->width = self::DEFAULT_WIDTH;
        $this->layout = self::DEFAULT_LAYOUT;
        $this->socialMedia = true;
        $this->showShopLogo = true;
        $this->showCompanyName = true;
        $this->show3DEffect = false;
        $this->showVideo = false;
        $this->adminContentUpdates = 0;
        $this->adminImageUpdates = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getSocialMedia(): ?bool
    {
        return $this->socialMedia;
    }

    public function setSocialMedia(bool $socialMedia): self
    {
        $this->socialMedia = $socialMedia;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSubtitle(): ?string
    {
        $referrer = $this->getReferrerLink()->getReferrer();

        if ($coupon = $referrer->getReferrerCouponByShop($this->getReferrerLink()->getShop())) {
            if ($coupon->isEnabled(ReferrerCoupon::TYPE_SET_OF_CARDS)) {
                return $coupon->getValueWithType() . ' OFF';
            }
        }

        return null;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getIframe(): ?string
    {
        return $this->iframe;
    }

    public function setIframe(?string $iframe): self
    {
        $this->iframe = $iframe;

        return $this;
    }

    public function getLayout(): ?string
    {
        return $this->layout;
    }

    public function setLayout(?string $layout): self
    {
        $this->layout = $layout;

        return $this;
    }

    public function getReferrerLink(): ?ReferrerLink
    {
        return $this->referrerLink;
    }

    public function setReferrerLink(?ReferrerLink $referrerLink): self
    {
        $this->referrerLink = $referrerLink;

        // set (or unset) the owning side of the relation if necessary
        $newCard = $referrerLink === null ? null : $this;
        if ($newCard !== $referrerLink->getCard()) {
            $referrerLink->setCard($newCard);
        }

        return $this;
    }

    public function getShowShopLogo(): ?bool
    {
        return $this->showShopLogo;
    }

    public function setShowShopLogo(?bool $showShopLogo): self
    {
        $this->showShopLogo = $showShopLogo;

        return $this;
    }

    public function getShowCompanyName(): ?bool
    {
        return $this->showCompanyName;
    }

    public function setShowCompanyName(?bool $showCompanyName): self
    {
        $this->showCompanyName = $showCompanyName;

        return $this;
    }

    public function getShow3DEffect(): ?bool
    {
        return $this->show3DEffect;
    }

    public function setShow3DEffect(?bool $show3DEffect): self
    {
        $this->show3DEffect = $show3DEffect;

        return $this;
    }

    public function getShowVideo(): ?bool
    {
        return $this->showVideo;
    }

    public function setShowVideo(?bool $showVideo): self
    {
        $this->showVideo = $showVideo;

        return $this;
    }

    public function getAdminContentUpdates(): ?int
    {
        return $this->adminContentUpdates;
    }

    public function setAdminContentUpdates(int $adminContentUpdates): self
    {
        $this->adminContentUpdates = $adminContentUpdates;

        return $this;
    }

    public function incrementAdminContentUpdates()
    {
        $this->adminContentUpdates++;
    }

    public function getAdminImageUpdates(): ?int
    {
        return $this->adminImageUpdates;
    }

    public function setAdminImageUpdates(int $adminImageUpdates): self
    {
        $this->adminImageUpdates = $adminImageUpdates;

        return $this;
    }

    public function incrementAdminImageUpdates()
    {
        $this->adminImageUpdates++;
    }

    public function isNew(): bool
    {
        return $this->getId() === null;
    }
}
