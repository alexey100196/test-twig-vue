<?php

namespace App\Helper\WidgetCompletion\Resolvers;

use App\Entity\Widget\WidgetShopSetting;
use App\Helper\WidgetCompletion\Result;
use App\Helper\WidgetCompletion\WidgetCompletionResolver;

abstract class AbstractResolver
{
    /**
     * @var WidgetShopSetting
     */
    protected $widgetShopSetting;

    /**
     * @var integer
     */
    protected $score = 0;

    /**
     * @var WidgetCompletionResolver
     */
    protected $resolver;

    /**
     * @return mixed
     */
    abstract public function resolve();

    /**
     * @param WidgetShopSetting $widgetShopSetting
     * @return AbstractResolver
     */
    public static function getInstance(WidgetShopSetting $widgetShopSetting)
    {
        $obj = new static;
        $obj->setWidgetShopSetting($widgetShopSetting);

        return $obj;
    }

    /**
     * @param WidgetShopSetting $widgetShopSetting
     */
    public function setWidgetShopSetting(WidgetShopSetting $widgetShopSetting)
    {
        $this->widgetShopSetting = $widgetShopSetting;
    }

    /**
     * @return Result
     */
    public function getResult(): Result
    {
        $result = new Result();
        $result->setName($this->getName());
        $result->setScore($this->getScore());

        return $result;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        $name = explode('\\', static::class);

        return array_pop($name);
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @param int $score
     */
    public function addScore(int $score)
    {
        $this->setScore($this->getScore() + $score);

        if ($this->score > 100) {
            throw new \Exception('Invalid score. Max is 100.');
        }
    }

    /**
     * @param int $score
     */
    public function setScore(int $score)
    {
        $this->score = $score;
    }

    /**
     * @param WidgetCompletionResolver $resolver
     */
    public function setResolver(WidgetCompletionResolver $resolver)
    {
        $this->resolver = $resolver;
    }

    /**
     * @return WidgetCompletionResolver
     */
    public function getResolver()
    {
        return $this->resolver;
    }
}
