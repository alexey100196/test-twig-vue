<?php

namespace App\Service\SystemCommission\ShopCommission;

use App\Entity\Shop;
use App\Service\SystemCommission\ApplierInterface;
use App\Service\SystemCommission\ShopCommission\Strategies\EcommerceStrategy;
use App\Service\SystemCommission\ShopCommission\Strategies\SaasStrategy;
use App\Service\SystemCommission\ShopCommission\Strategies\StrategyInterface;
use App\Service\SystemCommission\ValueObjects\TaxedAmount;

class Applier implements ApplierInterface
{
    /**
     * @var mixed
     */
    private $defaultFee;

    /**
     * @var array|StrategyInterface[]
     */
    private $strategies = [];

    /**
     * Applier constructor.
     * @param $defaultFee
     */
    public function __construct($defaultFee)
    {
        $this->defaultFee = $defaultFee;
    }

    /**
     * @param StrategyInterface $strategy
     */
    public function addStrategy(StrategyInterface $strategy)
    {
        $this->strategies[get_class($strategy)] = $strategy;
    }

    /**
     * @param Shop $shop
     * @param $amount
     * @return TaxedAmount
     */
    public function apply(Shop $shop, $amount): TaxedAmount
    {
        if ($shop->getType() && $shop->isEcommerce()) {
            return $this->getStrategy(EcommerceStrategy::class)->apply($shop, $amount, $this->defaultFee);
        }

        return $this->getStrategy(SaasStrategy::class)->apply($shop, $amount, $this->defaultFee);
    }

    /**
     * @param string $class
     * @return StrategyInterface
     */
    private function getStrategy(string $class): StrategyInterface
    {
        return $this->strategies[$class];
    }
}
