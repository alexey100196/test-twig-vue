<?php

namespace App\Controller\Admin;

use App\Entity\Transaction;
use App\Entity\TransactionType;
use App\Entity\User;
use App\Form\Balance\TransactionAmountType;
use App\Form\Balance\UserTransactionType;
use App\Repository\TransactionRepository;
use App\Service\Balance\UserBalanceService;
use App\Traits\Controller\FormResponseArray;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TransactionController extends AbstractController
{
    use FormResponseArray;

    private $balanceService;

    public function __construct(UserBalanceService $balanceService)
    {
        $this->balanceService = $balanceService;
    }

    /**
     * @Route("/admin/transaction/{uuid}", name="admin_transaction_update", methods={"PUT"})
     * @param User $user
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function update(Transaction $transaction, Request $request)
    {
        $form = $this->createForm(TransactionAmountType::class);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_BAD_REQUEST);
        }

        $data = $form->getData();
        $this->balanceService->updateTransaction($transaction->getUuid(), (float)$data['amount']);

        return new JsonResponse(['amount' => $transaction->getAmount()]);
    }
}
