<?php

namespace App\DataFixtures;

use App\DataFixtures\BaseGlossaryFixtures;
use App\Entity\CommissionType;
use App\Entity\ShopConversionStatus;

class ShopConversionStatusFixtures extends BaseGlossaryFixtures
{
    public function getData(): array
    {
        return [
            ShopConversionStatus::UNVERIFIED => 'unverified',
            ShopConversionStatus::ACCEPTED => 'accepted',
            ShopConversionStatus::REJECTED => 'rejected',
        ];
    }

    public function getEntity()
    {
        return new ShopConversionStatus();
    }
}
