<?php


namespace App\Controller\Account\Referrer;


use App\Entity\CustomDomain;
use App\Entity\ReferrerCoupon;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\ShortLinks\AffiliateShortLink;
use App\Entity\ShortLinks\CustomShortLink;
use App\Entity\User;
use App\Form\Referrer\AffiliateShortLinkType;
use App\Form\Referrer\CustomShortLinkType;
use App\Form\Referrer\ReferrerCouponType;
use App\Form\Referrer\ReferrerLinkType;
use App\Repository\CustomDomainRepository;
use App\Repository\ReferrerLinkCardRepository;
use App\Repository\ReferrerLinkRepository;
use App\Repository\ShortLinks\AffiliateShortLinkRepository;
use App\Repository\ShortLinks\AffiliateShortLinkStatsRepository;
use App\Repository\ShortLinks\AffiliateShortLinkTaxRepository;
use App\Repository\ShortLinks\CustomShortLinkRepository;
use App\Repository\ShortLinks\CustomShortLinkStatsRepository;
use App\Repository\ShortLinks\CustomShortLinkTaxRepository;
use App\Repository\UserShopRepository;
use App\Service\Referrer\ReferrerShop;
use App\Service\Referrer\ReferrerShopLink;
use App\Service\Referrer\RegenerateReferrerLinkCardContent;
use App\Service\ShortLinks\AffiliateShortLinkService;
use App\Service\ShortLinks\CustomShortLinkService;
use App\Traits\Controller\FormResponseArray;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Exception\InvalidParameterException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\ForbiddenOverwriteException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ReferrerShopLinkController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerShopShortLinkController extends AbstractController
{
    use FormResponseArray;

    /** @var CustomShortLinkRepository */
    private $customShortLinkRepository;

    /** @var AffiliateShortLinkRepository */
    private $affiliateShortLinkRepository;

    /** @var CustomShortLinkStatsRepository */
    private $customShortLinkStatsRepository;

    /** @var AffiliateShortLinkStatsRepository */
    private $affiliateShortLinkStatsRepository;

    /** @var CustomShortLinkTaxRepository */
    private $customShortLinkTaxRepository;

    /** @var AffiliateShortLinkTaxRepository */
    private $affiliateShortLinkTaxRepository;

    /** @var ReferrerLinkRepository */
    private $referrerLinkRepository;

    /** @var CustomDomainRepository */
    private $customDomainRepository;

    /** @var CustomShortLinkService */
    private $customShortLinkService;

    /** @var AffiliateShortLinkService */
    private $affiliateShortLinkService;

    /**
     * ReferrerShopController constructor.
     *
     * @param CustomShortLinkRepository $customShortLinkRepository
     * @param AffiliateShortLinkRepository $affiliateShortLinkRepository
     * @param CustomShortLinkStatsRepository $customShortLinkStatsRepository
     * @param AffiliateShortLinkStatsRepository $affiliateShortLinkStatsRepository
     * @param CustomShortLinkTaxRepository $customShortLinkTaxRepository
     * @param AffiliateShortLinkTaxRepository $affiliateShortLinkTaxRepository
     * @param ReferrerLinkRepository $referrerLinkRepository
     * @param CustomDomainRepository $customDomainRepository
     * @param CustomShortLinkService $customShortLinkService
     * @param AffiliateShortLinkService $affiliateShortLinkService
     */
    public function __construct(
        CustomShortLinkRepository $customShortLinkRepository,
        AffiliateShortLinkRepository $affiliateShortLinkRepository,
        CustomShortLinkStatsRepository $customShortLinkStatsRepository,
        AffiliateShortLinkStatsRepository $affiliateShortLinkStatsRepository,
        CustomShortLinkTaxRepository $customShortLinkTaxRepository,
        AffiliateShortLinkTaxRepository $affiliateShortLinkTaxRepository,
        ReferrerLinkRepository $referrerLinkRepository,
        CustomDomainRepository $customDomainRepository,
        CustomShortLinkService $customShortLinkService,
        AffiliateShortLinkService $affiliateShortLinkService
    )
    {
        $this->customShortLinkRepository = $customShortLinkRepository;
        $this->affiliateShortLinkRepository = $affiliateShortLinkRepository;
        $this->customShortLinkStatsRepository = $customShortLinkStatsRepository;
        $this->affiliateShortLinkStatsRepository = $affiliateShortLinkStatsRepository;
        $this->customShortLinkTaxRepository = $customShortLinkTaxRepository;
        $this->affiliateShortLinkTaxRepository = $affiliateShortLinkTaxRepository;
        $this->referrerLinkRepository = $referrerLinkRepository;
        $this->customDomainRepository = $customDomainRepository;
        $this->customShortLinkService = $customShortLinkService;
        $this->affiliateShortLinkService = $affiliateShortLinkService;
    }

    /**
     * @Route("/account/referrer/short-links", name="account_referrer_short_links_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        return $this->render('account/referrer/short-links/index.html.twig', [
            'pixels' => $this->getUser()->getPixels()
        ]);
    }

    /**
     * @Route("/account/referrer/short-links/custom", name="account_referrer_short_links_custom", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function getShortCustomLinks(Request $request)
    {
        $user = $this->getUser();
        $from = $request->query->get('from', null);
        $to = $request->query->get('to', null);
        $tags = $request->query->get('tags', null);
        $folders = $request->query->get('folders', null);

        $page = $request->query->get('page', null);
        $pageSize = $request->query->get('pageSize', null);
        $sort = $request->query->get('sort', null);
        if ($sort) {
            $sort = json_decode($sort);
        }

        if (!isset($from) || empty($from)) {
            $customDomains = $user->getCustomDomains()->map(function (CustomDomain $item) {
                return [
                    'id' => $item->getId(),
                    'name' => $item->getName()
                ];
            });

            return $this->render('account/referrer/short-links/custom.html.twig', [
                'customDomains' => $customDomains->toArray(),
                'pixels' => $user->getPixels()
            ]);
        }

        if ($err = $this->checkDateRange($from, $to)) {
            return new JsonResponse($err, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return new JsonResponse([
            'items' => $this->customShortLinkRepository->findByFilters($user, $from, $to, $tags, $folders, $page, $pageSize, $sort),
            'total' => $this->customShortLinkRepository->getUserTotal($user)
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/account/referrer/short-links/affiliate", name="account_referrer_short_links_affiliate", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getShortAffiliateLinks(Request $request)
    {
        $user = $this->getUser();
        $from = $request->query->get('from', null);
        $to = $request->query->get('to', null);
        $tags = $request->query->get('tags', null);
        $folders = $request->query->get('folders', null);
        $companies = $request->query->get('companies', null);

        $page = $request->query->get('page', null);
        $pageSize = $request->query->get('pageSize', null);
        $sort = $request->query->get('sort', null);
        if ($sort) {
            $sort = json_decode($sort);
        }

        $shopArr = $user->getJoinedShops()->map(function(Shop $item) {
            return [
                'id' => $item->getId(),
                'name' => $item->getName()
            ];
        });

        if (!isset($from) || empty($from)) {
            $customDomains = $user->getCustomDomains()->map(function (CustomDomain $item) {
                return [
                    'id' => $item->getId(),
                    'name' => $item->getName()
                ];
            });

            return $this->render('account/referrer/short-links/partner.html.twig', [
                'customDomains' => $customDomains->toArray(),
                'pixels' => $user->getPixels(),
                'shops' => $shopArr->toArray()
            ]);
        }

        if ($err = $this->checkDateRange($from, $to)) {
            return new JsonResponse($err, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return new JsonResponse([
            'items' => ($companies) ? $this->affiliateShortLinkRepository->findByFilters($user, $from, $to, $tags, $folders, $companies, $page, $pageSize, $sort) : [],
            'total' => $this->affiliateShortLinkRepository->getUserTotal($user)
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/account/referrer/short-links/custom/stats", name="account_referrer_short_links_custom_stats", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getShortCustomLinksStats(Request $request)
    {
        $user = $this->getUser();
        $linkId = $request->query->get('id', null);
        $from = $request->query->get('from', null);
        $to = $request->query->get('to', null);
        $tags = $request->query->get('tags', null);
        $folders = $request->query->get('folders', null);

        if ($err = $this->checkDateRange($from, $to)) {
            return new JsonResponse($err, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $generalStats = $this->customShortLinkStatsRepository->findGeneralByFilters($user, null, $from, $to, $tags, $folders, $linkId);

        return new JsonResponse([
            'country' => $this->customShortLinkStatsRepository->findGeneralByFilters($user, 'country', $from, $to, $tags, $folders, $linkId),
            'system' => $this->customShortLinkStatsRepository->findGeneralByFilters($user, 'system', $from, $to, $tags, $folders, $linkId),
            'browser' => $this->customShortLinkStatsRepository->findGeneralByFilters($user, 'browser', $from, $to, $tags, $folders, $linkId),
            'daily' => $this->customShortLinkStatsRepository->findDaysByFilters($user, $from, $to, $tags, $folders, $linkId),
            'total' => !empty($generalStats) ? $generalStats[0]['clicks'] : \stdClass::class
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/account/referrer/short-links/affiliate/stats", name="account_referrer_short_links_affiliate_stats", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getShortAffiliateLinksStats(Request $request)
    {
        $user = $this->getUser();
        $linkId = $request->query->get('id', null);
        $from = $request->query->get('from', null);
        $to = $request->query->get('to', null);
        $tags = $request->query->get('tags', null);
        $folders = $request->query->get('folders', null);

        if ($err = $this->checkDateRange($from, $to)) {
            return new JsonResponse($err, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $generalStats = $this->affiliateShortLinkStatsRepository->findGeneralByFilters($user, null, $from, $to, $tags, $folders, $linkId);

        return new JsonResponse([
            'country' => $this->affiliateShortLinkStatsRepository->findGeneralByFilters($user, 'country', $from, $to, $tags, $folders, $linkId),
            'system' => $this->affiliateShortLinkStatsRepository->findGeneralByFilters($user, 'system', $from, $to, $tags, $folders, $linkId),
            'browser' => $this->affiliateShortLinkStatsRepository->findGeneralByFilters($user, 'browser', $from, $to, $tags, $folders, $linkId),
            'daily' => $this->affiliateShortLinkStatsRepository->findDaysByFilters($user, $from, $to, $tags, $folders, $linkId),
            'total' => !empty($generalStats) ? $generalStats[0]['clicks'] : \stdClass::class
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/account/referrer/short-links/custom/tax", name="account_referrer_short_links_custom_tax")
     * @return JsonResponse
     */
    public function getShortCustomLinksTax()
    {
        $user = $this->getUser();

        return new JsonResponse([
            'tags' => $this->customShortLinkTaxRepository->getUniqueTaxes($user, 'tag'),
            'folders' => $this->customShortLinkTaxRepository->getUniqueTaxes($user, 'folder'),
            'utm_source' => $this->customShortLinkTaxRepository->getUniqueTaxes($user, 'utm_source'),
            'utm_medium' => $this->customShortLinkTaxRepository->getUniqueTaxes($user, 'utm_medium'),
            'utm_campaign' => $this->customShortLinkTaxRepository->getUniqueTaxes($user, 'utm_campaign'),
            'utm_content' => $this->customShortLinkTaxRepository->getUniqueTaxes($user, 'utm_content'),
            'utm_keyword' => $this->customShortLinkTaxRepository->getUniqueTaxes($user, 'utm_keyword')
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/account/referrer/short-links/affiliate/tax", name="account_referrer_short_links_affiliate_tax")
     * @return JsonResponse
     */
    public function getShortAffiliateLinksTax()
    {
        $user = $this->getUser();

        return new JsonResponse([
            'tags' => $this->affiliateShortLinkTaxRepository->getUniqueTaxes($user, 'tag'),
            'folders' => $this->affiliateShortLinkTaxRepository->getUniqueTaxes($user, 'folder'),
            'utm_source' => $this->affiliateShortLinkTaxRepository->getUniqueTaxes($user, 'utm_source'),
            'utm_medium' => $this->affiliateShortLinkTaxRepository->getUniqueTaxes($user, 'utm_medium'),
            'utm_campaign' => $this->affiliateShortLinkTaxRepository->getUniqueTaxes($user, 'utm_campaign'),
            'utm_content' => $this->affiliateShortLinkTaxRepository->getUniqueTaxes($user, 'utm_content'),
            'utm_keyword' => $this->affiliateShortLinkTaxRepository->getUniqueTaxes($user, 'utm_keyword')
        ], Response::HTTP_OK);
    }

    /**
     * @Route("/account/referrer/short-links/custom", name="account_referrer_short_links_custom_save", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function saveShortCustomLink(Request $request)
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            $customShortLink = null;
            $data = $request->request->all();
            $data['referrer'] = $user->getId();
            $tag = $request->request->get('tag');
            $folder = $request->request->get('folder');
            $utmSource = $request->request->get('utm_source');
            $utmMedium = $request->request->get('utm_medium');
            $utmCampaign = $request->request->get('utm_campaign');
            $utmContent = $request->request->get('utm_content');
            $utmKeyword = $request->request->get('utm_keyword');
            $customDomainId = $request->request->get('customDomainId', null);
            unset($data['id']);
            unset($data['tag']);
            unset($data['folder']);
            unset($data['utm_source']);
            unset($data['utm_medium']);
            unset($data['utm_campaign']);
            unset($data['utm_content']);
            unset($data['utm_keyword']);
            unset($data['customDomainId']);
            unset($data['referrerLinkId']);

            if ($request->request->get('id')) {
                $customShortLink = $this->customShortLinkRepository->find($request->request->get('id'));
            }

            if (!$customShortLink) {
                $customShortLink = new CustomShortLink();
                $customShortLink->setReferrer($user);
            }

            if ($customDomainId) {
                $customDomain = $this->customDomainRepository->find($customDomainId);
                if (!$user->getCustomDomains()->contains($customDomain)) {
                    throw new UnprocessableEntityHttpException('You can\'t use this custom domain.');
                }

                $customShortLink->setCustomDomain($customDomain);
            } else {
                $customShortLink->setCustomDomain(null);
            }

            $form = $this->createForm(CustomShortLinkType::class, $customShortLink);
            $form->submit($data);
            if ($form->isSubmitted() && $form->isValid()) {
                if (!$this->checkShortIsUnique($customShortLink)) {
                    throw new UnprocessableEntityHttpException('This short link is already taken.');
                }

                $this->customShortLinkService->save($form->getData(), $tag, $folder, $utmSource, $utmMedium, $utmCampaign, $utmContent, $utmKeyword);

                return new JsonResponse('Short link has been saved successfully.', Response::HTTP_OK);
            }

            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return new JsonResponse([$e->getMessage()], ($e->getCode()) ? $e->getCode() : 500);
        }
    }

    /**
     * @Route("/account/referrer/short-links/affiliate", name="account_referrer_short_links_affiliate_save", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function saveShortAffiliateLink(Request $request)
    {
        try {
            /** @var User $user */
            $user = $this->getUser();
            $affiliateShortLink = null;
            $data = $request->request->all();
            $data['referrer'] = $user->getId();
            $tag = $request->request->get('tag');
            $folder = $request->request->get('folder');
            $utmSource = $request->request->get('utm_source');
            $utmMedium = $request->request->get('utm_medium');
            $utmCampaign = $request->request->get('utm_campaign');
            $utmContent = $request->request->get('utm_content');
            $utmKeyword = $request->request->get('utm_keyword');
            $customDomainId = $request->request->get('customDomainId', null);
            unset($data['id']);
            unset($data['original']);
            unset($data['tag']);
            unset($data['folder']);
            unset($data['utm_source']);
            unset($data['utm_medium']);
            unset($data['utm_campaign']);
            unset($data['utm_content']);
            unset($data['utm_keyword']);
            unset($data['customDomainId']);
            unset($data['referrerLinkId']);

            if ($request->request->get('id')) {
                $affiliateShortLink = $this->affiliateShortLinkRepository->find($request->request->get('id'));
            }

            if (!$affiliateShortLink) {
                $affiliateShortLink = new AffiliateShortLink();
                $affiliateShortLink->setReferrer($user);
                if ($request->request->get('referrerLinkId')) {
                    $referrerLink = $this->referrerLinkRepository->find($request->request->get('referrerLinkId'));
                    if (!$referrerLink || !$user->hasReferrerLink($referrerLink)) {
                        throw new \Exception('Not found the referrer link.', Response::HTTP_UNPROCESSABLE_ENTITY);
                    }
                    $affiliateShortLink->setReferrerLink($referrerLink);
                } else {
                    throw new \Exception('Referrer link id is required', Response::HTTP_UNPROCESSABLE_ENTITY);
                }
            }

            if ($customDomainId) {
                $customDomain = $this->customDomainRepository->find($customDomainId);
                if (!$user->getCustomDomains()->contains($customDomain)) {
                    throw new UnprocessableEntityHttpException('You can\'t use this custom domain.');
                }

                $affiliateShortLink->setCustomDomain($customDomain);
            } else {
                $affiliateShortLink->setCustomDomain(null);
            }

            $form = $this->createForm(AffiliateShortLinkType::class, $affiliateShortLink);
            $form->submit($data);

            if ($form->isSubmitted() && $form->isValid()) {
                if (!$this->checkShortIsUnique($affiliateShortLink)) {
                    throw new UnprocessableEntityHttpException('This short link already taken.');
                }
                $this->affiliateShortLinkService->save($form->getData(), $tag, $folder, $utmSource, $utmMedium, $utmCampaign, $utmContent, $utmKeyword);

                return new JsonResponse('Short link has been saved successfully.', Response::HTTP_OK);
            }

            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_UNPROCESSABLE_ENTITY);
        } catch (\Exception $e) {
            return new JsonResponse([$e->getMessage()], ($e->getCode()) ? $e->getCode() : 500);
        }
    }

    /**
     * @Route("/account/referrer/short-links/custom/{customShortLink}/delete", name="account_referrer_short_links_custom_delete", methods={"POST"})
     * @param CustomShortLink $customShortLink
     * @return JsonResponse
     */
    public function deleteShortCustomLink(CustomShortLink $customShortLink)
    {
        try {
            $user = $this->getUser();
            if (!$customShortLink) {
                throw new NotFoundHttpException('Short link not found.');
            } else if ($customShortLink->getReferrer()->getId() !== $user->getId()) {
                throw new NotFoundHttpException('Short link not found.');
            }

            $this->customShortLinkService->remove($customShortLink);

            return new JsonResponse('Short link has been removed successfully.', Response::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonResponse([$e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Route("/account/referrer/short-links/affiliate/{affiliateShortLink}/delete", name="account_referrer_short_links_affiliate_delete", methods={"POST"})
     * @param AffiliateShortLink $affiliateShortLink
     * @return JsonResponse
     */
    public function deleteShortAffiliateLink(AffiliateShortLink $affiliateShortLink)
    {
        try {
            $user = $this->getUser();
            if (!$affiliateShortLink) {
                throw new NotFoundHttpException('Short link not found.');
            } else if ($affiliateShortLink->getReferrer()->getId() !== $user->getId()) {
                throw new NotFoundHttpException('Short link not found.');
            }

            $this->affiliateShortLinkService->remove($affiliateShortLink);

            return new JsonResponse('Short link has been removed successfully.', Response::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonResponse([$e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param string|null $from
     * @param string|null $to
     * @return string|null
     * @throws \Exception
     */
    private function checkDateRange(?string $from, ?string $to) {
        $days = 90;
        $rangeTimestamp = $days * 24 * 60 * 60; // 90 days
        $error = 'You can get stats for a range of up to '.$days.' days.';

        if ($from) {
            $fromDate = new \DateTime($from);
            if ($to) {
                $toDate = new \DateTime($to);
                if ($rangeTimestamp < ($toDate->getTimestamp() - $fromDate->getTimestamp())) {
                    return $error;
                }
                return null;
            } else if ($rangeTimestamp < (time() - $fromDate->getTimestamp())) {
                return $error;
            }
            return null;
        } else if ($to) {
            return 'Parameter \'to\' detected, but without parameter \'from\'';
        }

        return 'You must enter at least the \'from\' parameter';
    }

    /**
     * @param string $short
     * @param CustomShortLink|AffiliateShortLink $link
     * @return bool
     */
    private function checkShortIsUnique($link) {
        return (
            empty($this->customShortLinkRepository->findDuplicate($link)) &&
            empty($this->affiliateShortLinkRepository->findDuplicate($link))
        );
    }
}