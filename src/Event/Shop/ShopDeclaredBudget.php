<?php

namespace App\Event\Shop;

use App\Entity\Budget;
use App\Entity\Shop;
use Symfony\Component\EventDispatcher\Event;

class ShopDeclaredBudget extends Event
{
    /** @var string */
    const NAME = 'shop.declared-budget';

    /**
     * @var Shop
     */
    private $shop;

    /**
     * @var Budget
     */
    private $budget;

    /**
     * @var bool
     */
    private $declared;

    public function __construct(Shop $shop, Budget $budget, bool $declared = false)
    {
        $this->shop = $shop;
        $this->budget = $budget;
        $this->declared = $declared;
    }

    public function getShop(): Shop
    {
        return $this->shop;
    }

    public function getBudget(): Budget
    {
        return $this->budget;
    }

    public function getDeclared(): bool
    {
        return $this->declared;
    }
}