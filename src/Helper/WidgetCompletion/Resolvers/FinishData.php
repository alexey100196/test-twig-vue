<?php

namespace App\Helper\WidgetCompletion\Resolvers;

use App\Helper\WidgetCompletion\Resolvers\AbstractResolver;

class FinishData extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        $inv = $this->widgetShopSetting->getShop()->getInviteeCouponPackages();
        $ref = $this->widgetShopSetting->getShop()->getCouponPackages();
        if ($this->widgetShopSetting->getWelcomePicture() && $this->widgetShopSetting->getWelcomePictureWidth() > 0 &&
            $this->widgetShopSetting->getWelcomePictureHeight() > 0 && $this->widgetShopSetting->getWelcomePicturePosX() != null &&
            $this->widgetShopSetting->getWelcomePicturePosY() != null && $this->widgetShopSetting->getWelcomePictureRatio() )
            if ($inv->count() > 0 && $ref->count() > 0) {
                $campaigns = $this->widgetShopSetting->getShop()->getCampaigns();
                foreach($campaigns as $campaign){
                    if  ($campaign->getId() > 0 ) {
                         $this->addScore(100);
                         return;
                    }
                }
            }
    }
}




