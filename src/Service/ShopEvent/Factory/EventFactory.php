<?php


namespace App\Service\ShopEvent\Factory;


class EventFactory
{
    public function getInstance(array $eventArray)
    {
        $class = '\App\Service\ShopEvent\Events\\'.$eventArray['event'];

        // @todo: validate
        // @todo: improve it

        $event = new $class();
        return $event->createFromArray($eventArray);
    }

    protected function validateEvent(array $eventArray)
    {
        throw new \Exception('invalid event');
    }
}