<?php


namespace App\Controller\Ajax;


use App\Controller\AjaxAbstractController;
use App\Entity\Media;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\User;
use App\Form\Referrer\ReferrerLinkCardType;
use App\Service\MediaUploader;
use App\Service\Referrer\ReferrerLinkCardService;
use App\Service\UrlFileUploader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerShopCardController
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerLinkCardController extends AjaxAbstractController
{
    /** @var ReferrerLinkCardService */
    private $referrerLinkCardService;

    public function __construct(ReferrerLinkCardService $referrerLinkCardService)
    {
        $this->referrerLinkCardService = $referrerLinkCardService;
    }

    /**
     * @Route(
     *     "/ajax/referrer-link/{referrerLink}/card",
     *     name="ajax_referrer_link_card_update",
     *     methods={"POST"}
     * )
     * @param ReferrerLink $referrerLink
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function update(ReferrerLink $referrerLink, Request $request)
    {
        if (!$this->getUser()->hasReferrerLink($referrerLink)) {
            throw new NotFoundHttpException();
        }
        $data = $request->request->all();
        $data['width'] = isset($data['width']) ? (int) $data['width'] : 0;
        $data['socialMedia'] = isset($data['socialMedia']) ? (int) $data['socialMedia'] : 0;
        $data['showShopLogo'] = isset($data['showShopLogo']) ? (int) $data['showShopLogo'] : 0;
        $data['showCompanyName'] = isset($data['showCompanyName']) ? (int) $data['showCompanyName'] : 0;

        $form = $this->createForm(ReferrerLinkCardType::class, $card = $referrerLink->getCardOrCreate());
        $form->submit($data);

        if ($form->isValid()) {
            $this->referrerLinkCardService->save($card);
        } else {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return new JsonResponse(['message' => 'Card saved', 'success' => true]);
    }
}