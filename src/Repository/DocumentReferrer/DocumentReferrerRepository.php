<?php

namespace App\Repository\DocumentReferrer;

use App\Entity\DocumentReferrer\DocumentReferrer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DocumentReferrer|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentReferrer|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentReferrer[]    findAll()
 * @method DocumentReferrer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentReferrerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DocumentReferrer::class);
    }

    // /**
    //  * @return DocumentReferrer[] Returns an array of DocumentReferrer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DocumentReferrer
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
