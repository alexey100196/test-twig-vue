<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Entities;


class SummaryStatistics
{
    /**
     * @var int
     */
    private $viewsCount;

    /**
     * @var int
     */
    private $purchaseCount;

    /**
     * @var int
     */
    private $signUpCount;

    /**
     * @var float
     */
    private $purchaseTotal;

    /**
     * @var float
     */
    private $systemCommission;

    /**
     * @var float
     */
    private $referrerProfit;

    /**
     * @var float
     */
    private $purchaseReferrerProfit;

    /**
     * @var float
     */
    private $signUpReferrerProfit;

    public function __construct(
        int $viewsCount,
        int $purchaseCount,
        int $signUpCount,
        float $purchaseTotal,
        float $systemCommission,
        float $referrerProfit,
        float $purchaseReferrerProfit,
        float $signUpReferrerProfit
    )
    {
        $this->viewsCount = $viewsCount;
        $this->purchaseCount = $purchaseCount;
        $this->signUpCount = $signUpCount;
        $this->purchaseTotal = $purchaseTotal;
        $this->systemCommission = $systemCommission;
        $this->referrerProfit = $referrerProfit;
        $this->purchaseReferrerProfit = $purchaseReferrerProfit;
        $this->signUpReferrerProfit = $signUpReferrerProfit;
    }
}