<?php


namespace App\Form\Referrer;


use App\Entity\CardSet\CardSet;
use App\Entity\CardSet\CardSetCard;
use App\Entity\ReferrerLink;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

class CardSetCardType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'constraints' => [
                    new NotNull(),
                    new Length(['min' => 2, 'max' => 128])
                ]
            ])
            ->add('description', null, [
                'constraints' => [
                    new NotNull(),
                    new Length(['min' => 2, 'max' => 10000])
                ]
            ])
            // @todo: restrict below only to current referrer reflink
            ->add('referrerLink', EntityType::class, [
                'class' => ReferrerLink::class,
                'choice_label' => 'id',
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('background', CardSetCardBackgroundType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('articleUrl', UrlType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('articleUrlName', TextType::class, [
                'constraints' => [
                    new NotNull(),
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CardSetCard::class,
            // @todo: fix it
            'csrf_protection' => false,
        ]);
    }
}