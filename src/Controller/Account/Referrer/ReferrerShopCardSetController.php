<?php


namespace App\Controller\Account\Referrer;


use App\Entity\CardSet\CardSet;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Form\Referrer\ReferrerLinkType;
use App\Helper\DatePeriodGenerator;
use App\Repository\CardSet\CardSetRepository;
use App\Repository\UserShopRepository;
use App\Service\Referrer\ReferrerShop;
use App\Service\Referrer\ReferrerShopLink;
use App\Traits\Controller\FormResponseArray;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerShopCardSetController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerShopCardSetController extends AbstractController
{
    /**
     * @var ReferrerShopLink
     */
    private $referrerShopLink;

    /**
     * @var ReferrerShop
     */
    private $referrerShop;

    /**
     * ReferrerShopController constructor.
     * @param ReferrerShopLink $referrerShopLink
     * @param ReferrerShop $referrerShop
     */
    public function __construct(
        ReferrerShopLink $referrerShopLink,
        ReferrerShop $referrerShop
    )
    {
        $this->referrerShopLink = $referrerShopLink;
        $this->referrerShop = $referrerShop;
    }

    /**
     * @Route("/account/referrer/company/{shop}/card-set/{year}/{month}", name="account_referrer_shop_card_set_show")
     *
     * @param Shop $shop
     * @param Request $request
     * @param CardSetRepository $cardSetRepository
     * @param int|null $year
     * @param int|null $month
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(
        Shop $shop,
        Request $request,
        CardSetRepository $cardSetRepository,
        int $year = null,
        int $month = null
    )
    {
        $userShop = $this->referrerShop->findReferrerShopWithStats($this->getUser(), $shop);

        if (!$userShop) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        $datePeriod = DatePeriodGenerator::generateFromYearAndMonth($year, $month);

        $cards = $cardSetRepository->findAllWithStatusBetweenDates(
            $this->getUser(),
            $datePeriod->getStart(),
            $datePeriod->getEnd(),
            $request->query->get('name')
        );

        return $this->render('account/referrer/shop/show-card-set.html.twig', [
            'shop' => $shop,
            'item' => $userShop,
            'cardSets' => $cards,
            'currentPeriod' => $datePeriod,
        ]);
    }

    /**
     * @Route("/account/referrer/company/{shop}/card-set-create", name="account_referrer_shop_card_set_create_show")
     * @param Shop $shop
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Shop $shop, Request $request)
    {
        $userShop = $this->referrerShop->findReferrerShopWithStats($this->getUser(), $shop);

        if (!$userShop) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        return $this->render('account/referrer/shop/show-card-set-create.html.twig', [
            'item' => $userShop,
            'shop' => $shop,
        ]);
    }


    /**
     * @Route("/account/referrer/company/{shop}/card-set-edit/{cardSet}", name="account_referrer_shop_card_set_edit_show")
     * @param Shop $shop
     * @param CardSet $cardSet
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Shop $shop, CardSet $cardSet)
    {
        $userShop = $this->referrerShop->findReferrerShopWithStats($this->getUser(), $shop);

        if (!$userShop) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        if ($cardSet->getUser() !== $this->getUser()) {
            throw new HttpException(Response::HTTP_FORBIDDEN);
        }

        return $this->render('account/referrer/shop/show-card-set-edit.html.twig', [
            'cardSet' => $cardSet,
            'item' => $userShop,
            'shop' => $shop,
        ]);
    }
}