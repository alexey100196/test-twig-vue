<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class ImageFileOrUrl extends Constraint
{
    public $message = 'Please provide an image.';

    public function validatedBy()
    {
        return ImageFileOrUrlRequiredValidator::class;
    }
}