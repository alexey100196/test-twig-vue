<?php

namespace App\Form\Shop\Widget;

use App\Entity\Coupon\Coupon;
use App\Entity\Coupon\CouponDiscountType;
use App\Entity\Coupon\CouponPackage;
use App\Entity\Widget\WidgetShopSetting;
use App\Repository\Coupon\CouponPackageRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class WidgetRewardSettingsType extends AbstractType
{
    const MODIFY_STRUCTURE_OPTION = 'modify_structure';

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('referrerLinkValidityMode', ChoiceType::class, [
                'choices' => [
                    'always' => WidgetShopSetting::REFERRER_LINK_VALIDITY_MODE_ALWAYS_VALID,
/*                    'n_days_after_registration' => WidgetShopSetting::REFERRER_LINK_VALIDITY_MODE_N_DAYS_AFTER_REGISTRATION,*/
                    'till_date' => WidgetShopSetting::REFERRER_LINK_VALIDITY_MODE_TILL_DATE,
                ],
                'expanded' => true,
                'choice_value' => function ($val) {
                    return $val ? (string)$val : null;
                },
            ])
            ->add('cashType', ChoiceType::class, [
                'choices' => [
                    'always' => WidgetShopSetting::REFERRER_LINK_CASH_TYPE_FLAG_ALWAYS,
                    'min_value' => WidgetShopSetting::REFERRER_LINK_CASH_TYPE_FLAG_MIN_VALUE,
                ],
                'expanded' => true,
                'choice_value' => function ($val) {
                    return $val ? (string)$val : null;
                },
            ])
            ->add('cashRewards', ChoiceType::class, [
                'choices' => [
                    'always' => WidgetShopSetting::REFERRER_LINK_PURCHASES_VALID_FLAG_ALWAYS,
                    'max_orders' => WidgetShopSetting::REFERRER_LINK_PURCHASES_VALID_FLAG_MAX_ORDERS,
                ],
                'expanded' => true,
                'choice_value' => function ($val) {
                    return $val ? (string)$val : null;
                },
            ])
 /*           ->add('referrerLinkValidityDays', IntegerType::class, [
                'required' => false,
                'attr' => [
                    'min' => WidgetShopSetting::REFERRER_LINK_VALIDITY_MIN_DAYS,
                    'max' => WidgetShopSetting::REFERRER_LINK_VALIDITY_MAX_DAYS
                ],
                'constraints' => [
                    new GreaterThanOrEqual(WidgetShopSetting::REFERRER_LINK_VALIDITY_MIN_DAYS),
                    new LessThanOrEqual(WidgetShopSetting::REFERRER_LINK_VALIDITY_MAX_DAYS),
                ]
            ])*/
            ->add('referrerLinkValidityDate', DateTimeType::class, [
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'required' => false,
                'constraints' => [
                    new NotBlank(['message' => 'Please specify referrer link validity date']),
                    new Range([
                        'min' => 'now',
                        'max' => '+10 years',
                        'minMessage' => 'Referrer link validity date should be {{ limit }} or more',
                        'maxMessage' => 'Referrer link validity date should be {{ limit }} or less',
                    ])
                ]
            ])
            ->add('referrerRewardCouponEnabled', CheckboxType::class)
            ->add('referrerRewardCashEnabled', CheckboxType::class)
            ->add('referrerRewardCouponDiscountType', EntityType::class, [
                'class' => CouponDiscountType::class,
                'choice_value' => 'name',
                'expanded' => true,
            ])
            ->add('referrerRewardCouponDiscountMinPurchase', NumberType::class, [
                'required' => false,
                'constraints' => [
                    new GreaterThan(0),
                ]
            ])
            ->add('minCashPurchase', NumberType::class, [
                'required' => false,
                'constraints' => [
                    new GreaterThan(0),
                ]
            ])
            ->add('cashRewardsMaxOrders', NumberType::class, [
                'required' => false,
                'constraints' => [
                    new GreaterThan(0),
                ]
            ])
            ->add('referrerGetCouponRewardFlag', ChoiceType::class, [
                'choices' => [
                    'always' => WidgetShopSetting::REFERRER_GET_COUPON_REWARD_FLAG_ALWAYS,
                    'redirect' => WidgetShopSetting::REFERRER_GET_COUPON_REWARD_FLAG_REDIRECT,
                    'order' => WidgetShopSetting::REFERRER_GET_COUPON_REWARD_FLAG_ORDER,
                ],
                'empty_data' => (string)WidgetShopSetting::REFERRER_GET_COUPON_REWARD_FLAG_ALWAYS,
                'expanded' => true,
                'choice_value' => function ($val) {
                    return $val ? (string)$val : null;
                },
            ])
            ->add('inviteeRewardCouponDiscountType', EntityType::class, [
                'class' => CouponDiscountType::class,
                'expanded' => true
            ]);


        if ($this->tokenStorage->getToken() && $user = $this->tokenStorage->getToken()->getUser()) {
            $builder->add('referrerRewardCouponPackage', EntityType::class, [
                'class' => CouponPackage::class,
                'choice_label' => 'shortDiscountLabelWithType',
                'query_builder' => function (CouponPackageRepository $er) use ($user) {
                    return $er->createQueryBuilder('c')
                        ->innerJoin(Coupon::class, 'co', Join::WITH, 'co.package = c')
                        ->andWhere('c.allocation = :allocation')
                        ->andWhere('c.shop = :shop')
                        ->setParameter('allocation', CouponPackage::ALLOCATION_REFERRER)
                        ->setParameter('shop', $user->getShop());
                },
            ]);

            $builder->add('inviteeRewardCouponPackage', EntityType::class, [
                'class' => CouponPackage::class,
                'choice_label' => 'shortDiscountLabelWithType',
                'query_builder' => function (CouponPackageRepository $er) use ($user) {
                    return $er->createQueryBuilder('c')
                        ->innerJoin(Coupon::class, 'co', Join::WITH, 'co.package = c')
                        ->andWhere('c.shop = :shop')
                        ->andWhere('c.allocation = :allocation')
                        ->setParameter('allocation', CouponPackage::ALLOCATION_INVITEE)
                        ->setParameter('shop', $user->getShop());
                },
            ]);
        }
        
        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'handleReferrerLinkValidityMode']);
        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'handleReferrerRewardCouponDiscountMinPurchase']);
        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'handleMinCashPurchase']);
        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'handleCashRewardsMaxOrders']);
    }

    /**
     * @param FormEvent $event
     */
    public function handleReferrerLinkValidityMode(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (empty($data['referrerLinkValidityMode'])) {
            return;
        }

        switch ($data['referrerLinkValidityMode']) {
            case WidgetShopSetting::REFERRER_LINK_VALIDITY_MODE_ALWAYS_VALID:
                $this->removeFieldFromFormIfPossible($form, 'referrerLinkValidityDate');
                $this->removeFieldFromFormIfPossible($form, 'referrerLinkValidityDays');
                unset($data['referrerLinkValidityDate']);
                unset($data['referrerLinkValidityDays']);
                break;
            /*case WidgetShopSetting::REFERRER_LINK_VALIDITY_MODE_N_DAYS_AFTER_REGISTRATION:
                unset($data['referrerLinkValidityDate']);
                $this->removeFieldFromFormIfPossible($form, 'referrerLinkValidityDate');
                break;*/
            case WidgetShopSetting::REFERRER_LINK_VALIDITY_MODE_TILL_DATE:
                unset($data['referrerLinkValidityDays']);
                $this->removeFieldFromFormIfPossible($form, 'referrerLinkValidityDays');
                break;
        }

        $event->setData($data);
    }

    public function handleReferrerRewardCouponDiscountMinPurchase(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (empty($data['referrerRewardCouponDiscountValidateMinPurchase'])) {
            return;
        }

//        @todo: add required to referrerRewardCouponDiscountMinPurchase

        if ($data['referrerRewardCouponDiscountValidateMinPurchase'] == 0) {
            $this->removeFieldFromFormIfPossible($form, 'referrerRewardCouponDiscountMinPurchase');
            unset($data['referrerRewardCouponDiscountMinPurchase']);
        }

        $event->setData($data);
    }

    public function handleMinCashPurchase(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (empty($data['minCashPurchase'])) {
            return;
        }

//        @todo: add required to minCashPurchase

        if ($data['minCashPurchase'] == 0) {
            $this->removeFieldFromFormIfPossible($form, 'minCashPurchase');
            unset($data['minCashPurchase']);
        }

        $event->setData($data);
    }

    public function handleCashRewardsMaxOrders(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (empty($data['cashRewardsMaxOrders'])) {
            return;
        }

//        @todo: add required to cashRewardsMaxOrders

        if ($data['cashRewardsMaxOrders'] == 0) {
            $this->removeFieldFromFormIfPossible($form, 'cashRewardsMaxOrders');
            unset($data['cashRewardsMaxOrders']);
        }

        $event->setData($data);
    }

    private function removeFieldFromFormIfPossible(FormInterface $form, string $fieldName)
    {
        if ($form->getConfig()->getOption(self::MODIFY_STRUCTURE_OPTION)) {
            $form->remove($fieldName);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // @todo: enable csrf
            'csrf_protection' => false,
            'data_class' => WidgetShopSetting::class,
            self::MODIFY_STRUCTURE_OPTION => true,
        ]);
    }
}