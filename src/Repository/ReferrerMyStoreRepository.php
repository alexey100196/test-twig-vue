<?php

namespace App\Repository;

use App\Entity\ReferrerMyStore;
use App\Entity\Shop;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReferrerMyStore|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferrerMyStore|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferrerMyStore[]    findAll()
 * @method ReferrerMyStore[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferrerMyStoreRepository extends ServiceEntityRepository
{
    /**
     * ReferrerMyStoreRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReferrerMyStore::class);
    }
}
