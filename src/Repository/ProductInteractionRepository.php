<?php

namespace App\Repository;

use App\Entity\ProductInteraction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductInteraction|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductInteraction|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductInteraction[]    findAll()
 * @method ProductInteraction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductInteractionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductInteraction::class);
    }

//    /**
//     * @return ProductInteraction[] Returns an array of ProductInteraction objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductInteraction
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
