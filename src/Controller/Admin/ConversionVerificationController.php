<?php


namespace App\Controller\Admin;


use App\Helper\DatePeriodGenerator;
use App\Repository\ShopConversionRepository;
use App\Traits\Controller\HasOrdering;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ConversionVerificationController extends AbstractController
{
    use HasOrdering;

    private $shopConversionRepository;

    public function __construct(ShopConversionRepository $shopConversionRepository)
    {
        $this->shopConversionRepository = $shopConversionRepository;
    }

    /**
     * @Route("/admin/conversion-verification/{year}/{month}", name="admin_conversion_verification_index")
     * @param int|null $year
     * @param int|null $month
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function index(int $year = null, int $month = null, Request $request)
    {
        $datePeriod = DatePeriodGenerator::generateFromYearAndMonth($year, $month);

        $orderBy = $this->getOrderByFromRequest($request, ['createdAt', 'purchaseValue', 'referrerProfit']);

        $conversions = $this->shopConversionRepository->getByDatePeriod(
            $datePeriod->getStart(),
            $datePeriod->getEnd(),
            null,
            $orderBy
        );

        return $this->render('admin/conversion-verification/index.html.twig', [
            'conversions' => $conversions,
            'currentPeriod' => $datePeriod,
        ]);
    }
}