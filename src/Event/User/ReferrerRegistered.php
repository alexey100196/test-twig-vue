<?php

namespace App\Event\User;

use App\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class ReferrerRegistered extends Event
{
    /**
     * Event name.
     * @var string
     */
    const NAME = 'referrer.registered';

    /**
     * Registered user.
     * @var User
     */
    protected $user;

    /**
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
