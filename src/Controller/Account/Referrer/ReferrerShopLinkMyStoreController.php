<?php


namespace App\Controller\Account\Referrer;


use App\Entity\ReferrerLink;
use App\Entity\ReferrerMyStore;
use App\Entity\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Csrf\Exception\TokenNotFoundException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ReferrerShopLinkMyStoreController extends AbstractController
{

    /**
     * @Route(
     *     "/account/referrer/company/{shop}/referrer-link/{referrerLink}/my-store",
     *     name="account_referrer_shop_referrer_link_my_store_update",
     *     methods={"POST"}
     * )
     * @param Shop $shop
     * @param ReferrerLink $referrerLink
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Shop $shop, ReferrerLink $referrerLink, Request $request)
    {
        if (!$this->isCsrfTokenValid('referrer_link_my_store', $request->request->get('_token'))) {
            throw new TokenNotFoundException('Invalid CSRF.');
        }

        if (!$this->getUser()->hasReferrerLink($referrerLink)) {
            throw new NotFoundHttpException();
        }

        if (($showInMyStore = $request->request->get('showInMyStore')) !== null) {
            if ($showInMyStore && $this->getUser()->getMyStoreReferrerLinks()->count() >= ReferrerMyStore::MAX_AFFILIATE_LINKS) {
                return new JsonResponse([
                    'error' => 'You can add only '.ReferrerMyStore::MAX_AFFILIATE_LINKS.' links to store (Present count: '.$this->getUser()->getMyStoreReferrerLinks()->count().').'
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            $referrerLink->setShowInMyStore($showInMyStore);
        }

        if ($request->request->get('showInMyStoreAsCard') !== null) {
            $referrerLink->setShowInMyStoreAsCard($request->request->getBoolean('showInMyStoreAsCard'));
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        return new JsonResponse();
    }
}