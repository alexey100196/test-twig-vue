<?php

namespace App\Entity;

use App\Entity\BaseGlossary;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfferTypeRepository")
 */
class OfferType extends BaseGlossary
{
    const CPL = 'CPL';
    const CPS = 'CPS';
}
