<?php


namespace App\Service\Referrer;


use App\Entity\EventEntry;
use App\Entity\EventEntryType;
use App\Entity\ReferrerLink;
use App\Entity\User;
use App\Helper\UrlHelpers;
use App\Repository\EventEntryTypeRepository;
use App\Repository\ReferrerLinkRepository;
use App\Service\ShopEvent\EventService;
use App\Util\Consts;

class ReferrerReflink
{
    private $referrerLinkRepository;
    private $eventEntryTypeRepository;
    private $eventService;

    /**
     * ReferrerReflink constructor.
     * @param ReferrerLinkRepository $referrerLinkRepository
     * @param EventEntryTypeRepository $eventEntryTypeRepository
     * @param EventService $eventService
     */
    public function __construct(
        ReferrerLinkRepository $referrerLinkRepository,
        EventEntryTypeRepository $eventEntryTypeRepository,
        EventService $eventService
    )
    {
        $this->referrerLinkRepository = $referrerLinkRepository;
        $this->eventEntryTypeRepository = $eventEntryTypeRepository;
        $this->eventService = $eventService;
    }

    public function getReferrerLink(string $shopName, string $nickname, string $reflinkSlug)
    {
        return $this->referrerLinkRepository->findOneBy([
            'shopUrlName' => $shopName,
            'nickname' => $nickname,
            'slug' => $reflinkSlug,
        ]);
    }

    /**
     * @param ReferrerLink $referrerLink
     * @return string
     */
    public function getRedirectUrlForReffererLink(ReferrerLink $referrerLink): string
    {
        $url = (string)$referrerLink->getDestinationUrl();
        $url = UrlHelpers::addGetParam($url, Consts::SNIPPET_REFERRER_PARAM_NAME, $referrerLink->getSlug());

        if ($referrerLink->getProduct()) {
            $url = UrlHelpers::addGetParam($url, Consts::SNIPPET_PRODUCT_PARAM_NAME, $referrerLink->getProduct()->getNumber());
        }

        return $url;
    }

    /**
     * @param ReferrerLink $referrerLink
     */
    public function registerViewForReferrerLink(ReferrerLink $referrerLink)
    {
        $eventEntryType = $this->eventEntryTypeRepository->findOneBy(['name' => EventEntryType::TYPE_VIEW]);

        $eventEntry = new EventEntry();
        $eventEntry->setType($eventEntryType);
        $eventEntry->setShop($referrerLink->getShop());
        $eventEntry->setReferrerLink($referrerLink);

        if ($_SERVER['HTTP_REFERER'] ?? null) {
            $eventEntry->setHttpReferer($_SERVER['HTTP_REFERER']);
        }

        $this->eventService->process($eventEntry);
    }

    /**
     * @param User $referrer
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @return mixed
     */
    public function getByCardCreationDateRangeForReferrer(User $referrer, \DateTime $startDate, \DateTime $endDate)
    {
        return $this->referrerLinkRepository
            ->findByReferrerAndCardCreationDateRange($referrer, $startDate, $endDate);
    }
}