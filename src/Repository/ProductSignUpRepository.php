<?php

namespace App\Repository;

use App\Entity\ProductSignUp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductSignUp|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductSignUp|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductSignUp[]    findAll()
 * @method ProductSignUp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductSignUpRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductSignUp::class);
    }

//    /**
//     * @return ProductSignUp[] Returns an array of ProductSignUp objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProductSignUp
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
