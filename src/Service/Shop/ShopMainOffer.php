<?php


namespace App\Service\Shop;


use App\Entity\OfferType;
use App\Entity\Shop;
use App\Event\Shop\ShopChangedMainOffer;
use App\Repository\OfferTypeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ShopMainOffer
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var OfferTypeRepository
     */
    protected $offerTypeRepository;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    public function __construct(
        EntityManagerInterface $em,
        OfferTypeRepository $offerTypeRepository,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->em = $em;
        $this->offerTypeRepository = $offerTypeRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Shop $shop
     * @return Shop
     */
    public function saveMainOffer(Shop $shop)
    {
        if ($shop->getCpsOffer() && !$shop->getCpsOffer()->getType()) {
            $shop->getCpsOffer()->setType($this->getOfferTypeByName(OfferType::CPS));
        }

        if ($shop->getCplOffer()) {
            if ($shop->getCplOffer()->getCommission() && !$shop->getCplOffer()->getType()) {
                $shop->getCplOffer()->setType($this->getOfferTypeByName(OfferType::CPL));
            }

            if ($shop->getCplOffer()->getCommission() === null) {
                $this->em->remove($shop->getCplOffer());
                $shop->setCplOffer(null);
            }
        }

        $this->em->persist($shop);

        if ($this->shopOfferChanged($shop)) {
            $this->eventDispatcher->dispatch(ShopChangedMainOffer::NAME, new ShopChangedMainOffer($shop));
        }

        $this->em->flush();

        return $shop;
    }

    private function shopOfferChanged(Shop $shop)
    {
        $uow = $this->em->getUnitOfWork();
        $uow->computeChangeSets();

        $cpsChanged = $cplChanged = false;

        if ($shop->getCpsOffer()) {
            $cpsChanges = $uow->getEntityChangeSet($shop->getCpsOffer());
            if (isset($cpsChanges['commission']) && count($cpsChanges['commission']) == 2) {
                $cpsChanged = $cpsChanges['commission'][0] != $cpsChanges['commission'][1];
            }
        }

        if ($shop->getCplOffer()) {
            $cplChanges = $uow->getEntityChangeSet($shop->getCplOffer());
            if (isset($cplChanges['commission']) && count($cplChanges['commission']) == 2) {
                $cplChanged = $cplChanges['commission'][0] != $cplChanges['commission'][1];
            }
        }

        return $cpsChanged || $cplChanged;
    }

    /**
     * @param string $name
     * @return OfferType
     */
    private function getOfferTypeByName(string $name): OfferType
    {
        return $this->offerTypeRepository->findOneBy(compact('name'));
    }
}