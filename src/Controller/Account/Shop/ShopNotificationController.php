<?php


namespace App\Controller\Account\Shop;


use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ShopNotificationController extends AbstractController
{
    /**
     * @Route("/account/company/notification", name="account_shop_notification")
     * @Security("has_role('ROLE_SHOP')")
     */
    public function index(Request $request)
    {
        $rewardsMode = $request->query->getInt('rewards_mode');

        /** @var User $user */
        $user = $this->getUser();
        $shop = $user->getShop();

        if ($rewardsMode) {
            $shop->setRewardsMode($rewardsMode);
        }

        return $this->render('layouts/shop-notification.html.twig', [
            'shop' => $shop
        ]);
    }
}