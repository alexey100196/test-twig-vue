<?php


namespace App\Service\ShopEvent\Interfaces;


use App\Service\ShopEvent\Auth;

interface EventStrategyInterface
{
    /**
     * @return mixed
     */
    public function process();
}