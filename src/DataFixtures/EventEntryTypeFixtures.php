<?php


namespace App\DataFixtures;


use App\Entity\EventEntryType;

class EventEntryTypeFixtures extends BaseGlossaryFixtures
{
    public function getData(): array
    {
        return [
            1 => EventEntryType::TYPE_SIGN_UP,
            2 => EventEntryType::TYPE_PURCHASE,
            3 => EventEntryType::TYPE_TEST_CONNECTION,
            4 => EventEntryType::TYPE_VIEW,
        ];
    }

    public function getEntity()
    {
        return new EventEntryType();
    }
}