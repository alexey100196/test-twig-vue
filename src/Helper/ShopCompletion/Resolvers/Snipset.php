<?php

namespace App\Helper\ShopCompletion\Resolvers;

use App\Helper\ShopCompletion\Resolvers\AbstractResolver;

class Snipset extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        if ($this->getResolver()->getResult('CookieLifespan')->getScore() == 100) {
            $this->addScore(100);
        }
    }
}
