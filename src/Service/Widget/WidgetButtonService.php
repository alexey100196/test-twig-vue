<?php


namespace App\Service\Widget;


use App\Entity\Shop;
use App\Entity\Widget\WidgetButton;
use App\Repository\Widget\WidgetButtonIconRepository;
use Doctrine\ORM\EntityManagerInterface;

class WidgetButtonService
{
    private $widgetButtonIconRepository;

    public function __construct(
        WidgetButtonIconRepository $widgetButtonIconRepository
    )
    {
        $this->widgetButtonIconRepository = $widgetButtonIconRepository;
    }

    /**
     * @param Shop $shop
     * @return WidgetButton
     */
    public function createWidgetButtonForShop(Shop $shop): WidgetButton
    {
        $icon = $this->widgetButtonIconRepository->findOneBy([]);

        $widgetButton = new WidgetButton();
        $widgetButton->setShop($shop);
        $widgetButton->setIcon($icon);

        return $widgetButton;
    }
}