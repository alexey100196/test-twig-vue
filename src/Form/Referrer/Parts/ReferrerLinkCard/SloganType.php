<?php

namespace App\Form\Referrer\Parts\ReferrerLinkCard;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class SloganType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'constraints' => [
                new Length(['min' => 1, 'max' => 255])
            ],
            'attr' => ['placeholder' => 'Type your text...'],
        ]);
    }

    public function getParent()
    {
        return TextType::class;
    }
}