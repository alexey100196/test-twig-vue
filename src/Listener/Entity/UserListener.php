<?php

namespace App\Listener\Entity;

use App\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;

class UserListener
{
    public function prePersist(User $user, LifecycleEventArgs $event)
    {
    }

    public function preRemove(User $user, LifecycleEventArgs $event)
    {
    }

}
