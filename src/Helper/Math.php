<?php


namespace App\Helper;


class Math
{
    /**
     * @param $left
     * @param $right
     * @param int $scale
     * @return string
     */
    public static function mul($left, $right, int $scale = 2)
    {
        return bcmul(Number::toString($left), Number::toString($right), $scale);
    }


    /**
     * @param $left
     * @param $right
     * @param int $scale
     * @return string
     */
    public static function add($left, $right, int $scale = 2)
    {
        return bcadd(Number::toString($left), Number::toString($right), $scale);
    }

    /**
     * @param $left
     * @param $right
     * @param int $scale
     * @return string
     */
    public static function sub($left, $right, int $scale = 2)
    {
        return bcsub(Number::toString($left), Number::toString($right), $scale);
    }

    /**
     * @param $left
     * @param $right
     * @param int $scale
     * @return string
     */
    public static function div($left, $right, int $scale = 2)
    {
        return bcdiv(Number::toString($left), Number::toString($right), $scale);
    }

    /**
     * @param $left
     * @param $right
     * @param int $scale
     * @return int
     */
    public static function comp($left, $right, int $scale = 2)
    {
        return bccomp(Number::toString($left), Number::toString($right), $scale);
    }
}