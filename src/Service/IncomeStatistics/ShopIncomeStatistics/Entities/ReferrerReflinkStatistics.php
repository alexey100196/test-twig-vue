<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Entities;

use App\Entity\ReferrerLink;
use App\Entity\User;

class ReferrerReflinkStatistics
{
    /**
     * @var ReferrerLink
     */
    private $reflink;

    /**
     * @var int
     */
    private $viewsCount;

    /**
     * @var int
     */
    private $purchaseCount;

    /**
     * @var float
     */
    private $signUpReferrerProfit;

    /**
     * @var float
     */
    private $purchaseTotal;

    /**
     * @var int
     */
    private $signUpCount;

    /**
     * @var float
     */
    private $purchaseReferrerProfit;

    /**
     * @param ReferrerLink $reflink
     * @return ReferrerReflinkStatistics
     */
    public function setReflink(ReferrerLink $reflink): ReferrerReflinkStatistics
    {
        $this->reflink = $reflink;
        return $this;
    }

    /**
     * @param int $viewsCount
     * @return ReferrerReflinkStatistics
     */
    public function setViewsCount(int $viewsCount): ReferrerReflinkStatistics
    {
        $this->viewsCount = $viewsCount;
        return $this;
    }

    /**
     * @param int $purchaseCount
     * @return ReferrerReflinkStatistics
     */
    public function setPurchaseCount(int $purchaseCount): ReferrerReflinkStatistics
    {
        $this->purchaseCount = $purchaseCount;
        return $this;
    }

    /**
     * @param float $signUpReferrerProfit
     * @return ReferrerReflinkStatistics
     */
    public function setSignUpReferrerProfit(float $signUpReferrerProfit): ReferrerReflinkStatistics
    {
        $this->signUpReferrerProfit = $signUpReferrerProfit;
        return $this;
    }

    /**
     * @param float $purchaseTotal
     * @return ReferrerReflinkStatistics
     */
    public function setPurchaseTotal(float $purchaseTotal): ReferrerReflinkStatistics
    {
        $this->purchaseTotal = $purchaseTotal;
        return $this;
    }

    /**
     * @param int $signUpCount
     * @return ReferrerReflinkStatistics
     */
    public function setSignUpCount(int $signUpCount): ReferrerReflinkStatistics
    {
        $this->signUpCount = $signUpCount;
        return $this;
    }

    /**
     * @param float $purchaseReferrerProfit
     * @return ReferrerReflinkStatistics
     */
    public function setPurchaseReferrerProfit(float $purchaseReferrerProfit): ReferrerReflinkStatistics
    {
        $this->purchaseReferrerProfit = $purchaseReferrerProfit;
        return $this;
    }


    public function toArray()
    {
        return [
            'reflink' => [
                'id' => $this->reflink->getId(),
                'slug' => $this->reflink->getSlug(),
                'nickname' => $this->reflink->getNickname(),
                'shop_url_name' => $this->reflink->getShopUrlName(),
            ],
            'views_count' => $this->viewsCount,
            'purchase_count' => $this->purchaseCount,
            'sign_up_referrer_profit' => $this->signUpReferrerProfit,
            'purchase_referrer_profit' => $this->purchaseReferrerProfit,
            'purchase_total' => $this->purchaseTotal,
            'sign_up_count' => $this->signUpCount,
        ];
    }
}