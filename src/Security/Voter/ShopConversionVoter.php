<?php


namespace App\Security\Voter;


use App\Entity\Shop;
use App\Entity\ShopConversion;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;


class ShopConversionVoter extends Voter
{
    const CHANGE_CONVERSION_STATUS = 'CHANGE_CONVERSION_STATUS';

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::CHANGE_CONVERSION_STATUS))) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof ShopConversion) {
            return false;
        }

        return true;
    }

    /**
     * @param string $attribute
     * @param $shopConversion
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $shopConversion, TokenInterface $token)
    {
        $user = $token->getUser();

        // Check if user is logged and is referrer.
        if (!$user instanceof User) {
            return false;
        }

        if ($attribute === self::CHANGE_CONVERSION_STATUS) {
            return $this->canChangeConversionStatus($shopConversion, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param ShopConversion $shopConversion
     * @param User $user
     * @return bool
     */
    private function canChangeConversionStatus(ShopConversion $shopConversion, User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }

        if ($user->getShop() && $shopConversion->getShop() === $user->getShop()) {
            return true;
        }

        return false;
    }
}