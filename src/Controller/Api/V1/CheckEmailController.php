<?php


namespace App\Controller\Api\V1;


use App\Controller\Api\ApiController;
use App\Repository\ShopRepository;
use App\Repository\UserRepository;
use App\Service\PaypalService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CheckEmailController extends ApiController
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * @var PaypalService
     */
    private $paypalService;

    /**
     * CheckEmailController constructor.
     * @param UserRepository $userRepository
     * @param ShopRepository $shopRepository
     * @param PaypalService $paypalService
     */
    public function __construct(UserRepository $userRepository, ShopRepository $shopRepository, PaypalService $paypalService)
    {
        $this->userRepository = $userRepository;
        $this->shopRepository = $shopRepository;
        $this->paypalService = $paypalService;
    }

    /**
     * @Route("/api/v1/check/email", name="api_v1_check_email_exists")
     * @param Request $request
     * @return JsonResponse
     */
    public function checkEmail(Request $request)
    {
        @header("Access-Control-Allow-Credentials: true");
        $email = $request->get('email', null);
        $companyId = $request->get('companyId', null);
        $headers = [];

        $validator = $this->container->get('validator');
        $constraints = array(
            new \Symfony\Component\Validator\Constraints\Email(),
            new \Symfony\Component\Validator\Constraints\NotBlank()
        );

        if (!$this->shopRepository->findOneBy([
            'partnerNumber' => $companyId
        ])) {
            return $this->makeResponse([
                'error' => 'Shop not found'
            ], Response::HTTP_NOT_FOUND);
        }

        $violations = $validator->validate($email, $constraints);

        if ($violations->count() > 0) {
            return $this->validationErrorResponse([
                'email' => $violations->get(0)->getMessage()
            ]);
        }

        $user = $this->userRepository->findOneBy([
            'email' => $email
        ]);

        if ($user) {
            return $this->makeResponse([
                'accountType' => 'valuelink'
            ], Response::HTTP_OK, $headers);
        }

        if ($paypalUrl = $this->paypalService->getAuthUrl()) {
            return $this->makeResponse([
                'accountType' => 'paypal',
                'paypalUrl' => $paypalUrl
            ], Response::HTTP_OK, $headers);
        }

        return $this->makeResponse([
            'accountType' => 'notfound'
        ], Response::HTTP_OK, $headers);
    }
}