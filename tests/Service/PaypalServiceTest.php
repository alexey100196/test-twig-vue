<?php


namespace App\Tests\Service;

use App\Service\PaypalService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PaypalServiceTest extends KernelTestCase
{
    /**
     * @var PaypalService
     */
    private $paypalService;

    /**
     * @var ParameterBagInterface
     */
    private $appContainer;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->appContainer = $kernel->getContainer();
        $this->paypalService = $this->appContainer->get(PaypalService::class);
    }

    public function testPaypalServiceGetAuthUrl_isOpenIdSessionUrl()
    {
        $authUrl = $this->paypalService->getAuthUrl();
        $this->assertStringStartsWith("http", $authUrl, "begins with http");
        $this->assertStringContainsString("sandbox.paypal.com", $authUrl, "configured to use the sandbox endpoint");
        $this->assertStringContainsString("authorize", $authUrl, "has 'authorize' in url");
        $this->assertStringContainsString("scope=openid+profile+email", $authUrl, "has oidc scopes");
    }
}