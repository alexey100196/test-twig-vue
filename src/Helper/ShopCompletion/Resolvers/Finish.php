<?php

namespace App\Helper\ShopCompletion\Resolvers;

use App\Helper\ShopCompletion\Resolvers\AbstractResolver;

class Finish extends AbstractResolver
{
    public function resolve()
    {
        if ($this->shop->getWizardStep() >= 11) {
            $this->addScore(100);
        }
    }
}
