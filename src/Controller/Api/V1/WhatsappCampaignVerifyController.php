<?php


namespace App\Controller\Api\V1;

use App\Entity\WhatsappCampaignVerify;
use App\Repository\CampaignRepository;
use App\Repository\WhatsappCampaignVerifyRepository;
use App\Service\WhatsappCampaignVerifyService;
use Embed\Embed;
use App\Controller\Api\ApiController;
use App\Repository\ShopRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Routing\Annotation\Route;

class WhatsappCampaignVerifyController extends ApiController
{
    /**
     * @var CampaignRepository
     */
    private $campaignRepository;

    /**
     * @var WhatsappCampaignVerifyRepository
     */
    private $whatsappCampaignVerifyRepository;

    /**
     * @var WhatsappCampaignVerifyService
     */
    private $whatsappCampaignVerifyService;

    /**
     * CheckEmailController constructor.
     * @param WhatsappCampaignVerifyRepository $whatsappCampaignVerifyRepository
     * @param CampaignRepository $campaignRepository
     * @param WhatsappCampaignVerifyService $whatsappCampaignVerifyService
     */
    public function __construct(WhatsappCampaignVerifyRepository $whatsappCampaignVerifyRepository, CampaignRepository $campaignRepository, WhatsappCampaignVerifyService $whatsappCampaignVerifyService)
    {
        $this->whatsappCampaignVerifyRepository = $whatsappCampaignVerifyRepository;
        $this->campaignRepository = $campaignRepository;
        $this->whatsappCampaignVerifyService = $whatsappCampaignVerifyService;
    }

    /**
     * @Route("/api/v1/verify-number", name="api_v1_verify_number")
     * @param Request $request
     * @return JsonResponse
     */
    public function verifyNumber(Request $request)
    {
        $number = $request->get('number', null);
        $code = $request->get('code', null);
        $headers = [];

        $campaignId = $request->get('campaign', null);

        if (!$campaignId) throw new NotFoundHttpException('Not found campaign.');

        $campaign = $this->campaignRepository->find($campaignId);
        if (!$campaign) throw new NotFoundHttpException('Not found campaign.');

        $validator = $this->container->get('validator');
        $constraints = array(new \Symfony\Component\Validator\Constraints\NotBlank());
        $violations = $validator->validate($number, $constraints);

        if ($violations->count() > 0) {
            return $this->validationErrorResponse([$violations->get(0)->getMessage()]);
        }

        if ($code) return $this->checkCode($number, $code, $campaign);

        try {
            if ($this->whatsappCampaignVerifyRepository->findVerifiedByCampaign($campaign, $number)) {
                throw new UnprocessableEntityHttpException('This phone number has already been used in current campaign.');
            }

            // @TODO: Verify number by SIGNAL App.
            if (!$this->whatsappCampaignVerifyRepository->findOneBy(['number' => $number])){
                $verifyItem = new WhatsappCampaignVerify();
                $verifyItem->setCampaign($campaign);
                $verifyItem->setNumber($number);
                $this->whatsappCampaignVerifyService->generate($verifyItem);
            }

        } catch (\Exception $e) {
            return $this->validationErrorResponse([is_string($e) ? $e : $e->getMessage()]);
        }

        return $this->makeResponse([
            'success' => true,
            'message' => 'Verification code has been sent successfully.'
        ], Response::HTTP_OK, $headers);
    }

    private function checkCode($number, $code, $campaign) {
        try {
            if (!($campaignVerObj = $this->whatsappCampaignVerifyRepository->findOneBy([
                'number' => $number,
                'code' => $code,
                'campaign' => $campaign
            ]))) {
                throw new UnprocessableEntityHttpException('Unfortunately, this verification code isn\'t valid.');
            }

            if ($this->whatsappCampaignVerifyService->isNotExpired($campaignVerObj) !== true) {
                $this->resendCode($campaignVerObj);
                throw new UnprocessableEntityHttpException('Code has expired. Check SIGNAL App. for new code');
            }

            $this->whatsappCampaignVerifyService->verify($campaignVerObj);
        } catch (\Exception $e) {
            return $this->validationErrorResponse([is_string($e) ? $e : $e->getMessage()]);
        }

        return $this->makeResponse([
            'success' => true,
            'message' => 'Your phone number has been verified successfully.'
        ], Response::HTTP_OK, []);
    }

    /**
     * @param $campaignVer
     * @return WhatsappCampaignVerify
     */
    private function resendCode($campaignVer) {
        // @TODO: Send the code again SIGNAL App.
        return $this->whatsappCampaignVerifyService->regenerate($campaignVer);
    }
}
