<?php


namespace App\Form\Referrer;


use App\Entity\CardSet\CardSet;
use App\Entity\CardSet\CardSetCard;
use App\Entity\ReferrerLink;
use App\Entity\ShortLinks\CustomShortLink;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;

class CustomShortLinkType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('referrer', EntityType::class, [
                'class' => User::class,
                'choice_label' => 'id',
                'constraints' => [
                    new NotNull(),
                ]
            ])
            ->add('short', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new Length(['min' => 4, 'max' => 255])
                ]
            ])
            ->add('original', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new Length(['min' => 4, 'max' => 65535])
                ]
            ])
            ->add('description', TextType::class, [
                'constraints' => [
                    new Length(['min' => 0])
                ]
            ])
            ->add('facebook_pixel', ChoiceType::class, [
                'choices' => [
                    'Yes' => true,
                    'No' => false
                ]
            ])
            ->add('twitter_pixel', ChoiceType::class, [
                'choices' => [
                    'Yes' => true,
                    'No' => false
                ]
            ])
            ->add('pinterest_pixel', ChoiceType::class, [
                'choices' => [
                    'Yes' => true,
                    'No' => false
                ]
            ])
            ->add('linkedin_pixel', ChoiceType::class, [
                'choices' => [
                    'Yes' => true,
                    'No' => false
                ]
            ])
            ->add('google_pixel', ChoiceType::class, [
                'choices' => [
                    'Yes' => true,
                    'No' => false
                ]
            ])
            ->add('quora_pixel', ChoiceType::class, [
                'choices' => [
                    'Yes' => true,
                    'No' => false
                ]
            ])
            ->add('meta_title', TextType::class, [
                'constraints' => [
                    new NotNull(),
                    new Length(['min' => 4, 'max' => 65535])
                ]
            ])
            ->add('meta_desc', TextType::class, [
                'constraints' => [
                    new Length(['min' => 4, 'max' => 65535])
                ]
            ])
            ->add('meta_image', TextType::class, [
                'constraints' => [
                    new Length(['min' => 4, 'max' => 65535])
                ]
            ])
            ->add('meta_iframe', TextType::class, [
                'constraints' => [
                    new Length(['min' => 4, 'max' => 65535])
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CustomShortLink::class,
            'csrf_protection' => false
        ]);
    }
}