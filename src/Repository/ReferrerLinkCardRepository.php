<?php

namespace App\Repository;

use App\Entity\ReferrerLinkCard;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReferrerLinkCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferrerLinkCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferrerLinkCard[]    findAll()
 * @method ReferrerLinkCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferrerLinkCardRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReferrerLinkCard::class);
    }
}
