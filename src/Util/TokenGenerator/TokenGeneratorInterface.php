<?php


namespace App\Util\TokenGenerator;


interface TokenGeneratorInterface
{
    /**
     * @param int $length
     * @return string
     */
    public function generate(int $length = 32): string;
}