<?php


namespace App\Form\Type\Shop;

use App\Entity\Media;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class LogoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function (FormEvent $event) use ($builder, $options) {
                $form = $event->getForm();

                if ($form->getData() && $form->getData()->getId() !== null || isset($options['required']) && !$options['required']) {
                    $form->add('file', FileType::class);

                } else {

                    $form->add('file', FileType::class, [
                        'constraints' => [
                            new NotNull(['message' => 'Please specify Your logo'])
                        ]
                    ]);

                }
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'by_reference' => Media::class,
            'data_class' => Media::class,
            'required' => true
        ]);
    }
}