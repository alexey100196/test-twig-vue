<?php


namespace App\Tests\Service\OpenGraph;


use App\Service\OpenGraph\OpenGraphMetaData;
use PHPUnit\Framework\TestCase;

class OpenGraphMetaDataTest extends TestCase
{
    public function testFetch()
    {
        $service = new OpenGraphMetaData();
        $service->fetch('https://ogp.me/');

        $this->assertInstanceOf(OpenGraphMetaData::class, $service);
    }

    public function testGetImage()
    {
        $service = new OpenGraphMetaData();
        $service->fetch('https://ogp.me/');

        $this->assertEquals('https://ogp.me/logo.png', $service->getImage());
    }
}