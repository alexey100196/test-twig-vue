<?php

namespace App\Controller\CR_Referrer;


use App\Entity\Shop;
use App\Entity\User;
use App\Form\RegisterReferrerType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

class SignUpController extends AbstractController
{

    /**
     *
     * @Route("/signup", name="signup")
     * @return Response
     */
    public function showShop(CsrfTokenManagerInterface $tokenManager, Security $security): Response
    {
        $userId = $security->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $shop = $em->getRepository(Shop::class)->findOneBy(['id'=>$userId]);
        $logo = $shop->getLogo();
        $token = $tokenManager->getToken('register_referrer')->getValue();
        return $this->render('CR_Referrer/signup.html.twig', [
            'token'=>$token,
            'logo'=>$logo
        ]);
    }

}
