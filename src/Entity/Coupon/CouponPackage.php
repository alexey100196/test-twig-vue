<?php

namespace App\Entity\Coupon;

use App\Entity\Campaign;
use App\Entity\Shop;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Coupon\CouponPackageRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class CouponPackage
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    const MAX_SINGLE_COUPON_PACKAGE = 10;
    const MAX_MULTIPLE_COUPON_PACKAGE = 5;

    const ALLOCATION_REFERRER = 1;
    const ALLOCATION_INVITEE = 2;

    const ALLOCATION_NAMES = [
        self::ALLOCATION_REFERRER => 'referrer',
        self::ALLOCATION_INVITEE => 'invitee',
    ];

    private $cpr;
    private $cpi;
    private $ws;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $discount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Coupon\CouponDiscountType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $discountType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Coupon\Coupon", mappedBy="package", orphanRemoval=true, cascade={"persist"})
     */
    private $coupons;

    /**
     * @ORM\Column(type="boolean")
     */
    private $multipleUsage = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="couponPackages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    /**
     * @ORM\Column(type="integer")
     */
    private $allocation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Widget\WidgetShopSetting", mappedBy="referrerRewardCouponPackage", orphanRemoval=true)
     */
    private $referrerWidgetShopSettings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Widget\WidgetShopSetting", mappedBy="inviteeRewardCouponPackage", orphanRemoval=true)
     */
    private $inviteeWidgetShopSettings;

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function __construct()
    {
        $this->coupons = new ArrayCollection();
        $this->referrerWidgetShopSettings = new ArrayCollection();
        $this->inviteeWidgetShopSettings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getDiscountType(): ?CouponDiscountType
    {
        return $this->discountType;
    }

    public function setDiscountType(?CouponDiscountType $discountType): self
    {
        $this->discountType = $discountType;

        return $this;
    }

    public function isPercentage()
    {
        return $this->getDiscountType()->getName() === CouponDiscountType::PERCENTAGE;
    }

    /**
     * @return Collection|Coupon[]
     */
    public function getCoupons(): Collection
    {
        return $this->coupons;
    }

    public function addCoupon(Coupon $coupon): self
    {
        if (!$this->coupons->contains($coupon)) {
            $this->coupons[] = $coupon;
            $coupon->setPackage($this);
        }
        return $this;
    }

    public function removeCoupon(Coupon $coupon): self
    {
        if ($this->coupons->contains($coupon)) {
            $this->coupons->removeElement($coupon);
            // set the owning side to null (unless already changed)
            if ($coupon->getPackage() === $this) {
                $coupon->setPackage(null);
            }
        }
        return $this;
    }

    public function getMultipleUsage(): ?bool
    {
        return $this->multipleUsage;
    }

    public function setMultipleUsage(bool $multipleUsage): self
    {
        $this->multipleUsage = $multipleUsage;

        return $this;
    }

    public function addCoupons(iterable $coupons)
    {
        foreach ($coupons as $coupon) {
            $this->addCoupon($coupon);
        }
    }

    public function getAllocation(): ?int
    {
        return $this->allocation;
    }

    public function setAllocation(int $allocation): self
    {
        $this->allocation = $allocation;

        return $this;
    }

    public function getCouponsCount()
    {
        return count($this->coupons);
    }

    public function getDiscountLabel()
    {
        return $this->getDiscount() . ($this->isPercentage() ? '%' : ' EUR');
    }

    public function getShortDiscountLabelWithType()
    {
        $type = $this->getMultipleUsage() ? 'Multiple Usage Coupon' : 'One Time Coupon';

        return sprintf('%s (%s)', $this->getShortDiscountLabel(), $type);
    }

    public function getShortDiscountLabel()
    {
        $discountValue = (string)$this->getDiscount();
        $discountValue = str_replace('.00', '', $discountValue);
        if ($this->isPercentage()) {
            return $discountValue . '%' . ' OFF';
        } else {
            return $discountValue . '€ ' .' OFF';
        }
    }

    public function getAllocationName()
    {
        return $this->getAllocation() ? self::ALLOCATION_NAMES[$this->getAllocation()] : null;
    }

    public function hasCoupons(): bool
    {
        return $this->coupons->count() > 0;
    }

    public function hasAvailableCoupons(): bool
    {
        if ($this->coupons->count() === 0) {
            return false;
        }

        if ($this->multipleUsage) {
            return true;
        }

        return $this->coupons
                ->filter(function (Coupon $coupon) {
                    return $coupon->getUsed() === 0;
                })
                ->count() > 0;
    }

    public function isActive(?int $allocation = null): bool
    {
/*        if ($allocation) {
            return $allocation === self::ALLOCATION_REFERRER ? !$this->referrerWidgetShopSettings->isEmpty() : !$this->inviteeWidgetShopSettings->isEmpty();
        }*/

      $this->shop = $this->getShop();
       if ($this->shop->getCampaignsArray()){
           $this->ws   = $this->shop->getWidgetSetting($this->shop->getCampaignsArray());
           if ($this->ws <> null ) {
               $this->cpi  = $this->ws->getInviteeRewardCouponPackage();
               $this->cpr  = $this->ws->getReferrerRewardCouponPackage();
               if ($this->id === $this->cpr->id || $this->id === $this->cpi->id) {
                   return true;
               } else {
                   return false;
               }
           }
       }
        return false;
    }

    public function isUsed(?int $allocation = null): bool
    {
        if ($allocation) {
            return $allocation === self::ALLOCATION_REFERRER ? !$this->referrerWidgetShopSettings->isEmpty() : !$this->inviteeWidgetShopSettings->isEmpty();
        }

        return !($this->referrerWidgetShopSettings->isEmpty() && $this->inviteeWidgetShopSettings->isEmpty());
    }
}
