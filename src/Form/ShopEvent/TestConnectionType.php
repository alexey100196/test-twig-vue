<?php


namespace App\Form\ShopEvent;

use App\Entity\EventEntry;
use App\Entity\EventEntryType;
use App\Entity\Shop;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class TestConnectionType extends AbstractEventEntryType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('shop', EntityType::class, [
                'class' => Shop::class,
                'choice_value' => 'partner_number',
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('type', EntityType::class, [
                'class' => EventEntryType::class,
                'choice_value' => 'name',
                'constraints' => [
                    new NotNull()
                ]
            ]);

        $builder
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                $data['type'] = EventEntryType::TYPE_TEST_CONNECTION;
                $event->setData($data);
            });
    }
}