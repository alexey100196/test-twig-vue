<?php


namespace App\Controller\Ajax;


use App\Controller\AjaxAbstractController;
use App\Entity\Media;
use App\Entity\ReferrerMyStore;
use App\Entity\ReferrerStoreNonReferableLink;
use App\Entity\Shop;
use App\Entity\User;
use App\Form\Referrer\ReferrerNonReferableLinkCardType;
use App\Service\MediaUploader;
use App\Service\Referrer\ReferrerMyStoreService;
use App\Service\Referrer\ReferrerNonReferableLinkCardService;
use App\Service\Referrer\ReferrerStoreNonReferableLinkService;
use App\Service\UrlFileUploader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerShopCardController
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerNonReferableLinkCardController extends AjaxAbstractController
{
    /** @var ReferrerMyStoreService */
    private $referrerMyStoreService;

    /** @var ReferrerNonReferableLinkCardService */
    private $referrerNonReferableLinkCardService;

    /** @var ReferrerStoreNonReferableLinkService */
    private $referrerStoreNonReferableLinkService;

    public function __construct(
        ReferrerMyStoreService $referrerMyStoreService,
        ReferrerNonReferableLinkCardService $referrerNonReferableLinkCardService,
        ReferrerStoreNonReferableLinkService $referrerStoreNonReferableLinkService
    )
    {
        $this->referrerMyStoreService = $referrerMyStoreService;
        $this->referrerNonReferableLinkCardService = $referrerNonReferableLinkCardService;
        $this->referrerStoreNonReferableLinkService = $referrerStoreNonReferableLinkService;
    }

    /**
     * @Route(
     *     "/ajax/referrer-custom-link/card/create",
     *     name="ajax_referrer_non_referable_link_card_create",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $user = $this->getUser();
        $myStore = $user->getReferrerMyStore();
        if (!$myStore) {
            $myStore = new ReferrerMyStore();
            $this->referrerMyStoreService->generate($user, $myStore);
        }

        $referrerStoreNonReferableLink = new ReferrerStoreNonReferableLink();

        $data = $request->request->all();
        $data['width'] = isset($data['width']) ? (int) $data['width'] : 0;
        $data['socialMedia'] = isset($data['socialMedia']) ? (int) $data['socialMedia'] : 0;
        $data['showShopLogo'] = isset($data['showShopLogo']) ? (int) $data['showShopLogo'] : 0;
        $data['showCompanyName'] = isset($data['showCompanyName']) ? (int) $data['showCompanyName'] : 0;

        $form = $this->createForm(ReferrerNonReferableLinkCardType::class, $card = $referrerStoreNonReferableLink->getCardOrCreate());
        $form->submit($data);

        if ($form->isValid()) {
            $referrerStoreNonReferableLink->setDestinationUrl($data['url']);
            $referrerStoreNonReferableLink->setMyStore($myStore);
            $this->referrerNonReferableLinkCardService->save($card);
        } else {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return new JsonResponse(['message' => 'Card created', 'success' => true]);
    }

    /**
     * @Route(
     *     "/ajax/referrer-custom-link/{referrerStoreNonReferableLink}/card",
     *     name="ajax_referrer_non_referable_link_card_update",
     *     methods={"POST"}
     * )
     * @param ReferrerStoreNonReferableLink $referrerStoreNonReferableLink
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function update(ReferrerStoreNonReferableLink $referrerStoreNonReferableLink, Request $request)
    {
        $user = $this->getUser();
        $myStore = $user->getReferrerMyStore();

        if (!$referrerStoreNonReferableLink) {
            throw new NotFoundHttpException('Link not found.');
        }

        if (!$myStore || $myStore !== $referrerStoreNonReferableLink->getMyStore()) {
            throw new NotFoundHttpException('Invalid my store.');
        }

        $data = $request->request->all();
        $data['width'] = isset($data['width']) ? (int) $data['width'] : 0;
        $data['socialMedia'] = isset($data['socialMedia']) ? (int) $data['socialMedia'] : 0;
        $data['showShopLogo'] = isset($data['showShopLogo']) ? (int) $data['showShopLogo'] : 0;
        $data['showCompanyName'] = isset($data['showCompanyName']) ? (int) $data['showCompanyName'] : 0;

        $form = $this->createForm(ReferrerNonReferableLinkCardType::class, $card = $referrerStoreNonReferableLink->getCardOrCreate());
        $form->submit($data);

        if ($form->isValid()) {
            $this->referrerNonReferableLinkCardService->save($card);
        } else {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return new JsonResponse(['message' => 'Card saved', 'success' => true]);
    }
}