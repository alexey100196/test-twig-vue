<?php


namespace App\Service;


class UrlUpload
{
    private $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }
}