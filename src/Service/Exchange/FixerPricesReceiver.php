<?php


namespace App\Service\Exchange;


use GuzzleHttp\Client;

class FixerPricesReceiver implements PricesReceiver
{
    /**
     * @var string
     */
    private $url = 'http://data.fixer.io/api/latest';

    /**
     * @var string
     */
    private $token;

    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return Client
     */
    private function getHttpClient()
    {
        return new Client();
    }

    /**
     * @param string $quotedCurrency
     * @return array|Pair[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $quotedCurrency)
    {
        $response = $this->getHttpClient()->request('GET', $this->url, [
            'connect_timeout' => 10,
            'query' => ['access_key' => $this->token, 'base' => $quotedCurrency]
        ]);

        $result = json_decode($response->getBody(), 1);

        $pairs = [];
        foreach ($result['rates'] ?? [] as $baseCurrency => $rate) {
            $pairs[] = new Pair($baseCurrency, $quotedCurrency, (string)$rate);
        }

        return $pairs;
    }
}