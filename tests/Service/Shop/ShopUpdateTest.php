<?php


namespace App\Tests\Service\Shop;


use App\Entity\Shop;
use App\Form\Shop\EditShopType;
use App\Service\Shop\ShopUpdate;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ShopUpdateTest extends KernelTestCase
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var FormFactory
     */
    private $factory;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->factory = $kernel->getContainer()->get('form.factory');
    }

    public function testSave() {
        $formData = [
            'name' => 'Demo',
            'description' => 'Apple Computers',
            'website' => 'https://google.com/',
            'service' => 'Macbook, iPhone',
            'logoFile' => new UploadedFile(dirname(__FILE__).'/../../../public/images/about.jpg', 'logo.jpg')
        ];

        $shopUpdate = new ShopUpdate($this->em);
        $this->assertEquals(true, $shopUpdate instanceof ShopUpdate);

        /** @var Shop $shop */
        $shop = $this->em->getRepository(Shop::class)->findOneBy(['name' => 'Demo']);
        $this->assertEquals(true, $shop instanceof Shop);

        $expected = clone $shop;
        $expected->setWizardStep(1);
        $this->assertEquals(true, $shop instanceof Shop);

        $form = $this->factory->create(EditShopType::class, $shop);
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());

        $result = $shopUpdate->save($shop, $form);
        $this->assertEquals($expected, $result);
    }
}