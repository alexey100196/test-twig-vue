<?php

namespace App\Helper\WidgetCompletion;

use App\Entity\Widget\WidgetShopSetting;
use App\Helper\WidgetCompletion\Resolvers\LayoutData;

class WidgetCompletionResolver
{
    /**
     * @var WidgetShopSetting
     */
    protected $widgetShopSetting;

    /**
     * @var \AbstractResolver[]
     */
    protected $resolvers = [
        \App\Helper\WidgetCompletion\Resolvers\LayoutData::class,
        \App\Helper\WidgetCompletion\Resolvers\CouponsData::class,
        \App\Helper\WidgetCompletion\Resolvers\RewardsData::class,
        \App\Helper\WidgetCompletion\Resolvers\EmailData::class,
        \App\Helper\WidgetCompletion\Resolvers\IconData::class,
        \App\Helper\WidgetCompletion\Resolvers\ActivateRefferalData::class,
        \App\Helper\WidgetCompletion\Resolvers\FinishData::class,
    ];

    /**
     * @var array
     */
    protected $results = [];

    /**
     * @var int
     */
    protected $completedStepsCount = 0;

    /**
     * @var int
     */
    protected $completionPercent = 0;

    /**
     * @param WidgetShopSetting $widgetShopSetting
     */
    public function __construct(WidgetShopSetting $widgetShopSetting)
    {
        $this->widgetShopSetting = $widgetShopSetting;
    }

    /**
     * Run resolvers
     * @return void
     */
    public function resolve(): void
    {
        foreach ($this->resolvers as $resolver) {
            $instance = $resolver::getInstance($this->widgetShopSetting);
            $instance->setResolver($this);
            $instance->resolve();
            $result = $instance->getResult();
            $this->results[$result->getName()] = $result;
        }

        $this->resolved();
    }

    /**
     * Get all results.
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * Get single result.
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function getResult(string $name)
    {
        $this->validateResolver($name);

        return $this->results[$name];
    }

    public function allBeforeCompleted(string $name): bool
    {
        $this->validateResolver($name);

        foreach ($this->getResults() as $result) {
            if ($name === $result->getName()) {
                break;
            }

            if (!$result->isComplete()) {
                return false;
            }
        }

        return true;
    }


    /**
     * @return int
     */
    public function getStepsCount()
    {
        return count($this->resolvers);
    }

    /**
     * @return mixed
     */
    public function getCompletedStepsCount()
    {
        return $this->completedStepsCount;
    }

    /**
     * @return int|float
     */
    public function getCompletionPercent()
    {
        return $this->completionPercent;
    }

    /**
     * @return void
     */
    protected function setStepsCount()
    {
        $this->stepsCount = count($this->resolvers);
    }

    /**
     * @return void
     */
    public function setCompletedStepsCount()
    {
        $this->completedStepsCount = array_reduce($this->getResults(), function ($sum, $result) {
            return $result->isComplete() ? $sum + 1 : $sum;
        }, 0);
    }

    /**
     * @return void
     */
    public function setCompletionPercent()
    {
        $this->completionPercent = $this->getCompletedStepsCount() * 100 / $this->getStepsCount();
    }

    /**
     * What to do when resolved.
     */
    private function resolved(): void
    {
        $this->setCompletedStepsCount();
        $this->setCompletionPercent();
    }

    private function validateResolver(string $name)
    {
        if (!isset($this->results[$name])) {
            throw new \Exception('Invalid result type given ' . $name . '. Valid types: ' . implode(', ', array_keys($this->results)));
        }
    }
}
