<?php

namespace App\Entity;

use App\Helper\Math;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\Timestampable;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductPurchaseRepository")
 */
class ProductPurchase extends ProductInteraction
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CommissionType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $commissionType;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $commission;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $referrerProfit;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ReferrerLink", inversedBy="productPurchases")
     * @ORM\JoinColumn(nullable=false)
     */
    private $referrerLink;

    public function __construct()
    {
        $this->amount = 1;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return string
     */
    public function getTotal()
    {
        return Math::mul($this->getPrice(), $this->getAmount());
    }

    public function getCommissionType(): ?CommissionType
    {
        return $this->commissionType;
    }

    public function setCommissionType(?CommissionType $commissionType): self
    {
        $this->commissionType = $commissionType;

        return $this;
    }

    public function getCommission()
    {
        return $this->commission;
    }

    public function setCommission($commission): self
    {
        $this->commission = $commission;

        return $this;
    }

    public function getReferrerProfit()
    {
        return $this->referrerProfit;
    }

    public function setReferrerProfit($referrerProfit): self
    {
        $this->referrerProfit = $referrerProfit;

        return $this;
    }

}
