<?php


namespace App\Tests\Service\IncomeStatistics\ShopIncomeStatistics\Entities;


use App\Entity\User;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Entities\ShopStatistics;
use PHPUnit\Framework\TestCase;

class ShopStatisticsTest extends TestCase
{
    public function testToArray()
    {
        $referrer = (new User)
            ->setName('John')
            ->setSurname('Doe')
            ->setEmail('john.doe@example.com');

        $lastPurchaseDate = new \DateTime();
        $lastSignUpDate = new \DateTime();

        $entity = new ShopStatistics();
        $entity->setReferrer($referrer);
        $entity->setLastPurchaseDate($lastPurchaseDate);
        $entity->setLastSignUpDate($lastSignUpDate);
        $entity->setPurchaseCount(1);
        $entity->setSignUpCount(2);
        $entity->setViewsCount(100);
        $entity->setSignUpReferrerProfit(12);
        $entity->setPurchaseReferrerProfit(13);
        $entity->setSystemCommission(2);
        $entity->setPurchaseTotal(1230);

        $this->assertEquals([
            'referrer' => [
                'id' => null,
                'name' => 'John',
                'surname' => 'Doe',
                'email' => 'john.doe@example.com',
            ],
            'last_purchase_date' => $lastSignUpDate->format('Y-m-d H:i:s'),
            'last_sign_up_date' => $lastSignUpDate->format('Y-m-d H:i:s'),
            'purchase_count' => 1,
            'sign_up_count' => 2,
            'views_count' => 100,
            'sign_up_referrer_profit' => 12.0,
            'purchase_referrer_profit' => 13.0,
            'system_commission' => 2.0,
            'purchase_total' => 1230.0,
            'invite_friend_emails_count' => null,
            'used_coupons_count' => null

        ], $entity->toArray());
    }
}