<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReferrerLinkStoreCardRepository")
 */
class ReferrerLinkStoreCard
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Groups({"referrer"})
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Groups({"referrer"})
     */
    private $description;

    /**
     * @ORM\Column(type="smallint")
     * @JMS\Groups({"referrer"})
     */
    private $orderColumn;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ReferrerLink", mappedBy="storeCard", cascade={"persist", "remove"})
     */
    private $referrerLink;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @JMS\Groups({"referrer"})
     */
    private $image;

    /**
     * @ORM\Column(type="integer")
     */
    private $adminContentUpdates;

    /**
     * @ORM\Column(type="integer")
     */
    private $adminImageUpdates;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\Groups({"referrer"})
     */
    private $imageWidth;

    public function __construct()
    {
        $this->title = '';
        $this->orderColumn = 0;
        $this->adminContentUpdates = 0;
        $this->adminImageUpdates = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOrderColumn(): ?int
    {
        return $this->orderColumn;
    }

    public function setOrderColumn(int $orderColumn): self
    {
        $this->orderColumn = $orderColumn;

        return $this;
    }

    public function getReferrerLink(): ?ReferrerLink
    {
        return $this->referrerLink;
    }

    public function setReferrerLink(?ReferrerLink $referrerLink): self
    {
        $this->referrerLink = $referrerLink;

        // set (or unset) the owning side of the relation if necessary
        $newStoreCard = $referrerLink === null ? null : $this;
        if ($newStoreCard !== $referrerLink->getStoreCard()) {
            $referrerLink->setStoreCard($newStoreCard);
        }

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getAdminContentUpdates(): ?int
    {
        return $this->adminContentUpdates;
    }

    public function setAdminContentUpdates(int $adminContentUpdates): self
    {
        $this->adminContentUpdates = $adminContentUpdates;

        return $this;
    }

    public function incrementAdminContentUpdates()
    {
        $this->adminContentUpdates++;
    }

    public function getAdminImageUpdates(): ?int
    {
        return $this->adminImageUpdates;
    }

    public function setAdminImageUpdates(int $adminImageUpdates): self
    {
        $this->adminImageUpdates = $adminImageUpdates;

        return $this;
    }

    public function deleteImage()
    {
        $this->setImage(null);
    }

    public function incrementAdminImageUpdates()
    {
        $this->adminImageUpdates++;
    }

    public function getImageWidth(): ?int
    {
        return $this->imageWidth;
    }

    public function setImageWidth(?int $imageWidth): self
    {
        $this->imageWidth = $imageWidth;

        return $this;
    }

}
