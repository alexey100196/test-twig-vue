<?php


namespace App\Service\Referrer;


use App\Entity\Product;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\User;
use App\Repository\ReferrerLinkRepository;
use Doctrine\ORM\EntityManagerInterface;

class ReferrerShopLink
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ReferrerLink $referrerLink
     * @return ReferrerLink
     */
    public function save(ReferrerLink $referrerLink)
    {
        $referrerLink->setShopUrlName($referrerLink->getShop()->getUrlName());

        if ($product = $this->guessProductForReferrerLink($referrerLink)) {
            $referrerLink->setProduct($product);
        }

        $this->em->persist($referrerLink);
        $this->em->flush();

        return $referrerLink;
    }

    /**
     * @param ReferrerLink $referrerLink
     * @return ReferrerLink
     */
    public function create(ReferrerLink $referrerLink)
    {
        $referrerLink->setNickname($referrerLink->getReferrer()->getNickname());

        return $this->save($referrerLink);
    }

    /**
     * @param User $referrer
     * @param ReferrerLink $referrerLink
     * @return bool
     */
    public function remove(User $referrer, ReferrerLink $referrerLink)
    {
        if ($referrerLink->getReferrer() === $referrer) {
            $referrerLink->delete();
            $this->em->flush();

            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @param Shop $shop
     * @return bool
     */
    public function canUserCreateShopLink(User $user, Shop $shop): bool
    {
        return $shop->hasJoinedUser($user);
    }

    public function findReferrerShopLinks(Shop $shop, User $referrer, $isGenerated = null)
    {
        $args = [
            'shop' => $shop,
            'referrer' => $referrer,
            'deletedAt' => null,
        ];
        if ($isGenerated !== null) {
            $args['isGenerated'] = (bool) $isGenerated;
        }
        return $this->em->getRepository(ReferrerLink::class)
            ->findBy($args, ['id' => 'DESC']);
    }

    /**
     * @param ReferrerLink $referrerLink
     * @return Product|null
     */
    protected function guessProductForReferrerLink(ReferrerLink $referrerLink): ?Product
    {
        $destinationLink = (string)$referrerLink->getDestinationUrl();
        $websiteLink = $referrerLink->getShop()->getWebsite();

        if ($destinationLink === null || $websiteLink === null) {
            return null;
        }

        $productLink = trim(str_replace($websiteLink, '', $destinationLink), '/');

        return $this->em
            ->getRepository(Product::class)
            ->findShopProductByWebsite($referrerLink->getShop(), $productLink, true);
    }
}