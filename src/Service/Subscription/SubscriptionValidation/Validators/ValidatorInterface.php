<?php


namespace App\Service\Subscription\SubscriptionValidation\Validators;


use App\Entity\Subscription\Plan;
use App\Entity\User;

interface ValidatorInterface
{
    /**
     * @param User $user
     * @param Plan $plan
     *
     * @throws SubscriptionValidationException
     */
    public function validate(User $user, Plan $plan): void;
}