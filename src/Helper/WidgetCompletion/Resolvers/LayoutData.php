<?php

namespace App\Helper\WidgetCompletion\Resolvers;

use App\Helper\WidgetCompletion\Resolvers\AbstractResolver;

class LayoutData extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
/*      if ($this->widgetShopSetting->getLanguage()) {
            $this->addScore(25);
        }*/

        if ($this->widgetShopSetting->getWelcomePicture()) {
            $this->addScore(10);
        }

        if ($this->widgetShopSetting->getWelcomePictureWidth() > 0) {
            $this->addScore(18);
        }

        if ($this->widgetShopSetting->getWelcomePictureHeight() > 0) {
            $this->addScore(18);
        }

        if ($this->widgetShopSetting->getWelcomePicturePosX() != null) {
            $this->addScore(18);
        }

        if ($this->widgetShopSetting->getWelcomePicturePosY() != null) {
            $this->addScore(18);
        }

        if ($this->widgetShopSetting->getWelcomePictureRatio()) {
            $this->addScore(18);
        }
    }
}