<?php

namespace App\Entity;

use App\Entity\Coupon\Coupon;
use App\Entity\Coupon\CouponPackage;
use App\Entity\Glossary\ShopType;
use App\Entity\Widget\WidgetButton;
use App\Entity\Widget\WidgetShopSetting;
use App\Helper\ShopCompletion\ShopCompletionResolver;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShopRepository")
 * @UniqueEntity(
 *   fields="website",
 *   message="This website is already in use."
 * )
 */
class Shop
{
    const REWARD_MODE_COUPON_CODES = 1;
    const REWARD_MODE_CASH = 2;
    const REWARD_MODE_COUPON_CODES_AND_CASH = 3;

    const REWARD_MODE_NAMES = [
        self::REWARD_MODE_COUPON_CODES => 'coupon codes only',
        self::REWARD_MODE_CASH => 'cash rewards only',
        self::REWARD_MODE_COUPON_CODES_AND_CASH => 'cash rewards with coupon codes',
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"card-sets"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"card-sets"})
     */
    private $name;

    /**
     *
     * @Assert\Url
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="shop", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="shop", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $products;

    /**
     * Cookie lifetime in minutes.
     * @ORM\Column(type="integer", nullable=true)
     */
    private $cookieLifetime;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $logo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShopStatus")
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserShop", mappedBy="shop", orphanRemoval=true)
     */
    private $userShops;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $service;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer", cascade={"persist", "remove"})
     */
    private $cpsOffer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Offer", cascade={"persist", "remove"})
     */
    private $cplOffer;

    /**
     * @ORM\Column(type="string", length=7, nullable=true)
     */
    private $color;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $partnerNumber;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EventEntry", mappedBy="shop", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $eventEntries;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $snippetConnectedAt;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=3)
     */
    private $systemCommission;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Budget", cascade={"persist", "remove"})
     */
    private $budgets;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $urlName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $bannerCookieLifetime;

    /**
     * @ORM\Column(type="smallint", options={"default" : 0})
     */
    private $wizardStep = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Glossary\ShopType")
     */
    private $type;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=2)
     */
    private $lastMonthPayout = 0;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ReferrerLink", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $referredBy;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Coupon\CouponPackage", mappedBy="shop", orphanRemoval=true)
     */
    private $couponPackages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Campaign", mappedBy="shop")
     * @ORM\OrderBy({"updatedAt" = "DESC", "createdAt" = "DESC"})
     */
    private $campaigns;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Widget\WidgetShopSetting", mappedBy="shop", cascade={"persist", "remove"})
     */
    private $widgetSetting;

    /**
     * @var WidgetButton|null
     * @ORM\OneToOne(targetEntity="App\Entity\Widget\WidgetButton", mappedBy="shop", cascade={"persist", "remove"})
     */
    private $widgetButton;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $rewardsMode;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $postOrderPopupEnabled;

    public function __construct()
    {
        $this->generateNewPartnerNumber();

        $this->products = new ArrayCollection();
        $this->userShops = new ArrayCollection();
        $this->eventEntries = new ArrayCollection();
        $this->budgets = new ArrayCollection();
        $this->couponPackages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getReferredBy(): ?ReferrerLink
    {
        return $this->referredBy;
    }

    public function setReferredBy(ReferrerLink $referrerLink): self
    {
        $this->referredBy = $referrerLink;

        return $this;
    }


    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setShop($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getShop() === $this) {
                $product->setShop(null);
            }
        }

        return $this;
    }

    /**
     * @param int $id
     * @return mixed|null
     */
    public function getProductById(int $id)
    {
        foreach ($this->products as $product) {
            if ($product->getId() == $id) {
                return $product;
            }
        }

        return null;
    }

    public function getCookieLifetime(): ?int
    {
        return $this->cookieLifetime;
    }

    public function setCookieLifetime(?int $cookieLifetime): self
    {
        $this->cookieLifetime = $cookieLifetime;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLogo(): ?String
    {
        return $this->logo;
    }

    public function setLogo(?String $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getStatus(): ?ShopStatus
    {
        return $this->status;
    }

    public function setStatus(?ShopStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|UserShop[]
     */
    public function getUserShops(): Collection
    {
        return $this->userShops;
    }

    /**
     * @param UserShop $userShop
     * @return Shop
     */
    public function addUserShop(UserShop $userShop): self
    {
        $userShop->setShop($this);

        $this->userShops[] = $userShop;

        return $this;
    }

    /**
     * Check if given user is joined to the shop.
     * @param User $user
     * @return boolean
     */
    public function hasJoinedUser(User $user): bool
    {
        foreach ($this->userShops as $userShop) {
            if ($userShop->getUser()->getId() == $user->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return ShopCompletionResolver
     */
    public function getCompletion()
    {
        $shopCompletionResolver = new ShopCompletionResolver($this);
        $shopCompletionResolver->resolve();
        return $shopCompletionResolver;
    }

    public function getService(): ?string
    {
        return $this->service;
    }

    public function setService(?string $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getCpsOffer(): ?Offer
    {
        return $this->cpsOffer;
    }

    public function setCpsOffer(?Offer $cpsOffer): self
    {
        $this->cpsOffer = $cpsOffer;

        return $this;
    }

    public function hasMainOffer(): bool
    {
        return $this->getCpsOffer() !== null;
    }

    public function hasPositiveCpsOffer(): bool
    {
        if (!$this->getCpsOffer()) {
            return false;
        }

        return $this->getCpsOffer()->getCommission() > 0;
    }

    public function getCplOffer(): ?Offer
    {
        return $this->cplOffer;
    }

    public function setCplOffer(?Offer $cplOffer): self
    {
        $this->cplOffer = $cplOffer;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getPartnerNumber(): ?string
    {
        return $this->partnerNumber;
    }

    public function setPartnerNumber(string $partnerNumber): self
    {
        $this->partnerNumber = $partnerNumber;

        return $this;
    }

    /**
     * @throws \Exception
     */
    public function generateNewPartnerNumber()
    {
        $this->partnerNumber = strtoupper(bin2hex(random_bytes(10)));
    }

    /**
     * @return Collection|EventEntry[]
     */
    public function getEventEntries(): Collection
    {
        return $this->eventEntries;
    }

    public function addEventEntry(EventEntry $eventEntry): self
    {
        if (!$this->eventEntries->contains($eventEntry)) {
            $this->eventEntries[] = $eventEntry;
            $eventEntry->setShop($this);
        }

        return $this;
    }

    public function removeEventEntry(EventEntry $eventEntry): self
    {
        if ($this->eventEntries->contains($eventEntry)) {
            $this->eventEntries->removeElement($eventEntry);
            // set the owning side to null (unless already changed)
            if ($eventEntry->getShop() === $this) {
                $eventEntry->setShop(null);
            }
        }

        return $this;
    }

    public function getSnippetConnectedAt(): ?\DateTimeInterface
    {
        return $this->snippetConnectedAt;
    }

    public function setSnippetConnectedAt(?\DateTimeInterface $snippetConnectedAt): self
    {
        $this->snippetConnectedAt = $snippetConnectedAt;

        return $this;
    }

    public function getSystemCommission()
    {
        return $this->systemCommission;
    }

    public function setSystemCommission($systemCommission): self
    {
        $this->systemCommission = $systemCommission;

        return $this;
    }

    /**
     * @return Collection|Budget[]
     */
    public function getBudgets(): Collection
    {
        return $this->budgets;
    }

    /**
     * @param Currency $currency
     * @return Budget|null
     */
    public function getCurrencyBudget(Currency $currency)
    {
        return $this->getCurrencyNameBudget($currency->getName());
    }

    public function getCurrencyNameBudget(string $currency): ?Budget
    {
        foreach ($this->getBudgets() as $budget) {
            if ($budget->getCurrency()->getName() === $currency) {
                return $budget;
            }
        }

        return null;
    }

    public function hasPositiveBudget(string $currency): bool
    {
        $budget = $this->getCurrencyNameBudget($currency);

        if (!$budget) {
            return false;
        }

        return $budget->getAmount() > 0;
    }

    /**
     * @param Budget $budget
     * @return Shop
     */
    public function addBudget(Budget $budget): self
    {
        $this->budgets[] = $budget;

        return $this;
    }

    /**
     * @param Budget $budget
     * @return Shop
     */
    public function removeBudget(Budget $budget): self
    {
        if ($this->budgets->contains($budget)) {
            $this->budgets->removeElement($budget);
        }

        return $this;
    }

    public function getUrlName(): ?string
    {
        return $this->urlName;
    }

    public function setUrlName(?string $urlName): self
    {
        $this->urlName = $urlName;

        return $this;
    }

    public function getBannerCookieLifetime(): ?int
    {
        return $this->bannerCookieLifetime;
    }

    public function setBannerCookieLifetime(?int $bannerCookieLifetime): self
    {
        $this->bannerCookieLifetime = $bannerCookieLifetime;

        return $this;
    }

    /**
     * @return Product
     */
    public function createNewProduct(): Product
    {
        $product = new Product();
        $product->setShop($this);

        return $product;
    }

    /**
     * @return bool
     */
    public function hasProducts(): bool
    {
        return $this->products->count() > 0;
    }

    public function getWizardStep(): ?int
    {
        return $this->wizardStep;
    }

    /**
     * @param int $wizardStep
     * @param bool $force
     * @return bool (true if changed)
     */
    public function setWizardStep(int $wizardStep, bool $force = false): bool
    {
        if ($wizardStep > $this->wizardStep || $force) {
            $this->wizardStep = $wizardStep;

            return true;
        }

        return false;
    }

    public function isActive(): bool
    {
        return $this->getStatus()->getId() === ShopStatus::ACTIVE;
    }

    public function isRejected(): bool
    {
        return $this->getStatus()->getId() === ShopStatus::REJECTED;
    }

    public function isInactive(): bool
    {
        return $this->getStatus()->getId() === ShopStatus::INACTIVE;
    }

    public function isSuspended(): bool
    {
        return $this->getStatus()->getId() === ShopStatus::SUSPENDED;
    }

    public function getType(): ?ShopType
    {
        return $this->type;
    }

    public function setType(?ShopType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function isSaas(): bool
    {
        return $this->isShopTypeById(ShopType::SAAS);
    }

    public function isEcommerce(): bool
    {
        return $this->isShopTypeById(ShopType::ECOMMERCE);
    }

    private function isShopTypeById(int $typeId): bool
    {
        if ($type = $this->getType()) {
            return $type = $this->getType() && $type->getId() === $typeId;
        }

        return false;
    }

    public function getLastMonthPayout()
    {
        return $this->lastMonthPayout;
    }

    public function setLastMonthPayout(float $lastMonthPayout): self
    {
        $this->lastMonthPayout = $lastMonthPayout;

        return $this;
    }

    /**
     * @return Collection|CouponPackage[]
     */
    public function getCouponPackages(): Collection
    {
        return $this->couponPackages;
    }

    /**
     * @return Collection|CouponPackage[]
     */
    public function getCouponPackagesWithCoupons(): Collection
    {
        return $this->couponPackages->filter(function (CouponPackage $package) {
            return $package->hasCoupons();
        });
    }

    public function hasInviteeCouponPackages(): bool
    {
        return $this->getInviteeCouponPackages()
                ->filter(function (CouponPackage $package) {
                    return !$package->isDeleted();
                })->count() > 0;
    }

    public function hasReferrerCouponPackages(): bool
    {
        return $this->getReferrerCouponPackages()
                ->filter(function (CouponPackage $package) {
                    return !$package->isDeleted();
                })->count() > 0;
    }

    public function getInviteeCouponPackages(): Collection
    {
        return $this->getCouponPackages()->filter(function (CouponPackage $package) {
            return $package->getAllocation() === CouponPackage::ALLOCATION_INVITEE;
        });
    }

    public function getReferrerCouponPackages(): Collection
    {
        return $this->getCouponPackages()->filter(function (CouponPackage $package) {
            return $package->getAllocation() === CouponPackage::ALLOCATION_REFERRER;
        });
    }

    public function addCouponPackage(CouponPackage $couponPackage): self
    {
        if (!$this->couponPackages->contains($couponPackage)) {
            $this->couponPackages[] = $couponPackage;
            $couponPackage->setShop($this);
        }

        return $this;
    }

    public function removeCouponPackage(CouponPackage $couponPackage): self
    {
        if ($this->couponPackages->contains($couponPackage)) {
            $this->couponPackages->removeElement($couponPackage);
            // set the owning side to null (unless already changed)
            if ($couponPackage->getShop() === $this) {
                $couponPackage->setShop(null);
            }
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    /**
     * @return mixed
     */
    public function getCampaignsArray()
    {
        $res = [];
        if (isset($this->campaigns) && $this->campaigns->count() > 0) {
            foreach($this->campaigns->getValues() as $campaign) {
                $res[] = $campaign->toArray();
            }
        }
        return $res;
    }

    /**
     * @param mixed $campaigns
     */
    public function setCampaigns($campaigns): void
    {
        $this->campaigns = $campaigns;
    }

    public function getWidgetSetting($fromCampaign = false): ?WidgetShopSetting
    {
        if ($fromCampaign) {
            $activeCampaign = null;
            foreach($this->campaigns as $campaign) {
                if ($campaign && $campaign->getActivatedAt()) {
                    $activeCampaign = $campaign;
                }
            }

            return ($activeCampaign) ? $activeCampaign->getWidgetShopSetting() : null;
        }

            foreach($this->widgetSetting as $wss) {
               if ( $wss->getWelcomePicture() ) {
                   return $wss;
            }
        }
        return $this->widgetSetting[0];
    }

    public function getWidgetSettingOrCreateNew($fromCampaign = false) : ?WidgetShopSetting
    {
        if ($fromCampaign) {
            $activeCampaign = null;
            foreach($this->campaigns as $campaign) {
                if ($campaign && $campaign->getActivatedAt()) {
                    $activeCampaign = $campaign;
                }
            }
            return ($activeCampaign) ? $activeCampaign->getWidgetShopSettingOrCreateNew() : new WidgetShopSetting();
        }

        if (!$this->widgetSetting[0]) {
            $this->widgetSetting->add(new WidgetShopSetting());
            $this->widgetSetting[0]->setShop($this);
        }

        return $this->widgetSetting[0];

        /*return ($this->widgetSetting) ? $this->widgetSetting : new WidgetShopSetting(); */
    }

    public function setWidgetSetting(WidgetShopSetting $widgetSetting): self
    {
        $this->widgetSetting = $widgetSetting;


        if ($this !== $widgetSetting->getShop()) {
            $this->widgetSetting->add(new WidgetShopSetting());
            $this->widgetSetting->setShop($this);
        }
      /*  if ($this !== $widgetSetting->getShop()) {
            $this->widgetSetting->setShop($this);
        }*/
        if ($widgetSetting->getCampaign()) {
            $this->widgetSetting->setCampaign(null);
        }
        return $this;
    }


    public function setRewardsMode(int $rewardsMode)
    {
        $this->rewardsMode = $rewardsMode;
    }

    public function getRewardsMode(): ?int
    {
        return $this->rewardsMode;
    }

    public function getRewardsModeName(): ?string
    {
        if (!$this->getRewardsMode()) {
            return null;
        }

        return self::REWARD_MODE_NAMES[$this->getRewardsMode()];
    }

    /**
     * @return WidgetButton|null
     */
    public function getWidgetButton(): ?WidgetButton
    {
        return $this->widgetButton;
    }

    public function setWidgetButton(WidgetButton $widgetButton): self
    {
        $this->widgetButton = $widgetButton;

        // set the owning side of the relation if necessary
        if ($this !== $widgetButton->getShop()) {
            $widgetButton->setShop($this);
        }

        return $this;
    }

    public function hasCouponRewards(): bool
    {
        return \in_array($this->rewardsMode, [
            self::REWARD_MODE_COUPON_CODES_AND_CASH,
            self::REWARD_MODE_COUPON_CODES,
        ]);
    }

    public function hasCashRewards(): bool
    {
        return \in_array($this->rewardsMode, [
            self::REWARD_MODE_COUPON_CODES_AND_CASH,
            self::REWARD_MODE_CASH,
        ]);
    }

    public function hasOnlyCouponRewards(): bool
    {
        return \in_array($this->rewardsMode, [
            self::REWARD_MODE_COUPON_CODES,
        ]);
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function canRewardImmediatelyAfterReferringToAFriend(): bool
    {
        $widgetSetting = $this->getWidgetSetting();

        if (!$widgetSetting) {
            return false;
        }

        return $widgetSetting->getReferrerRewardCouponDiscountMinPurchase() <= 0;
    }

    public function hasOwnerBalance()
    {
        return $this->getUser()->hasAnyFunds(Currency::EUR);
    }

    public function hasOwnerActiveSubscription()
    {
        return $this->getUser()->getCurrentPlan()->isActive();
    }

    public function getPostOrderPopupEnabled(): ?bool
    {
        return $this->postOrderPopupEnabled;
    }

    public function setPostOrderPopupEnabled(?bool $postOrderPopupEnabled): self
    {
        $this->postOrderPopupEnabled = $postOrderPopupEnabled;

        return $this;
    }
}
