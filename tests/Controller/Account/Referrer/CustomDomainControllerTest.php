<?php

namespace App\Tests\Controller\Account\Referrer;


use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class CustomDomainControllerTest extends WebTestCase
{
    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        //
    }

    /**
     * @testdox Redirect to login if user isn't referrer
     */
    public function testRedirect()
    {
        $client = static::createClient();
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $url = $client->getKernel()->getContainer()->getParameter('url');
        $client->setServerParameter('HTTP_HOST', $domain);

        $client->request('POST', '/account/custom-domains', []);
        $this->assertResponseRedirects($url.'/login', Response::HTTP_FOUND);

        $client->request('POST', '/account/custom-domains/1/delete', []);
        $this->assertResponseRedirects($url.'/login', Response::HTTP_FOUND);
    }

    /**
     * @testdox Check if user hasn't referrer rule
     */
    public function testIsNotReferrer() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', null));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);

        $client->request('POST', '/account/custom-domains', []);
        $this->assertResponseRedirects('/logged', Response::HTTP_FOUND);

        $client->request('POST', '/account/custom-domains/1/delete', []);
        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    /**
     * @testdox Check if user hasn't referrer rule
     */
    public function testIsReferrer() {
        $kernel = self::bootKernel();
        $em = $kernel->getContainer()->get('doctrine')->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['name' => 'john']);

        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', 'john'));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);

        $client->request('POST', '/account/custom-domains', [
            'name' => 'test_first.pl',
        ]);

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('Custom domain has been saved successfully.', $content['message']);
        $this->assertTrue(count($content['customDomains']) > 0);

        $client->request('POST', '/account/custom-domains/'.$content['customDomains'][0]['id'].'/delete', []);

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $content = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('Custom domain has been removed successfully.', $content['message']);
        $this->assertTrue(count($content['customDomains']) === 0);
    }

    /**
     * @param string $filter
     * @param $value
     * @return Cookie
     */
    private function logIn(string $filter, $value)
    {
        $session = self::$container->get('session');
        $em = self::$container->get('doctrine.orm.entity_manager');

        $user = $em->getRepository(User::class)->findOneBy([$filter => $value]);

        $firewallName = 'main';

        $token = new UsernamePasswordToken($user, null, $firewallName, $user->getRoles());
        $session->set('_security_'.$firewallName, serialize($token));
        $session->save();

        return new Cookie($session->getName(), $session->getId());
    }
}
