<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Handlers;


use App\Entity\ShopInteraction;
use App\Service\IncomeStatistics\CommandInterface;
use App\Service\IncomeStatistics\Handler;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Entities\ReferrerReflinkStatistics;
use Doctrine\Common\Collections\ArrayCollection;

class ReferrerReflinksStatisticsHandler extends Handler
{
    public function handle(CommandInterface $command)
    {
        $result = $this->doctrine
            ->getRepository(ShopInteraction::class)
            ->getShopReferrerReflinksStats(
                $command->getShop(),
                $command->getReferrer(),
                $command->getStartDate(),
                $command->getEndDate(),
                $command->getProductsIds(),
                $command->getIncludeMainOffer(),
                $command->getFilterBy()
            );

        return new ArrayCollection(array_map([$this, 'convertToEntity'], $result));
    }


    /**
     * @param array $item
     * @return ReferrerReflinkStatistics
     */
    private function convertToEntity(array $item): ReferrerReflinkStatistics
    {
        return (new ReferrerReflinkStatistics())
            ->setReflink($item['reflink'])
            ->setViewsCount((int)$item['viewsCount'])
            ->setSignUpCount((int)$item['signUpCount'])
            ->setPurchaseCount((int)$item['purchaseCount'])
            ->setSignUpReferrerProfit((float)$item['signUpReferrerProfit'])
            ->setPurchaseReferrerProfit((float)$item['purchaseReferrerProfit'])
            ->setPurchaseTotal((float)$item['purchaseTotal']);
    }
}