<?php

namespace App\Repository;

use App\Entity\ShopConversionStatusChange;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopConversionStatusChange|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopConversionStatusChange|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopConversionStatusChange[]    findAll()
 * @method ShopConversionStatusChange[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopConversionStatusChangeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopConversionStatusChange::class);
    }

    // /**
    //  * @return ShopConversionStatusChange[] Returns an array of ShopConversionStatusChange objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ShopConversionStatusChange
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
