<?php


namespace App\Service\Subscription\ValueObject;


use App\Entity\Subscription\PlanInterval;
use Carbon\Carbon;

class Period
{
    /**
     * @var PlanInterval
     */
    private $interval;

    /**
     * @var Carbon
     */
    private $start;

    /**
     * @var Carbon
     */
    private $end;

    /**
     * @var int
     */
    private $period;

    public function __construct(PlanInterval $interval, int $period, \DateTime $start = null)
    {
        $this->interval = $interval;

        if (null === $start) {
            $this->start = new Carbon();
        } elseif (!$start instanceof Carbon) {
            $this->start = Carbon::instance($start);
        } else {
            $this->start = $start;
        }

        if ($period <= 0) {
            throw new \LogicException('Subscription period has to been greater than 0');
        }

        $this->period = $period;

        $start = clone $this->start;

        $method = 'add' . ucfirst($this->interval) . 's';
        $this->end = $start->{$method}($this->period);
    }

    /**
     * @return PlanInterval
     */
    public function getInterval(): PlanInterval
    {
        return $this->interval;
    }

    /**
     * @return Carbon
     */
    public function getStart(): Carbon
    {
        return $this->start;
    }

    /**
     * @return Carbon
     */
    public function getEnd(): Carbon
    {
        return $this->end;
    }

    /**
     * @return int
     */
    public function getPeriod(): int
    {
        return $this->period;
    }

    /**
     * @param \DateTimeInterface $start
     */
    public function setStart(\DateTimeInterface $start): void
    {
        $this->start = $start;
    }

    /**
     * @param \DateTimeInterface $end
     */
    public function setEnd(\DateTimeInterface $end): void
    {
        $this->end = $end;
    }
}