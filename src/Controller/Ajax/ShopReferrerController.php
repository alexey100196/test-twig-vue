<?php


namespace App\Controller\Ajax;


use App\Controller\AjaxAbstractController;
use App\Entity\Shop;
use App\Repository\UserRepository;
use App\Util\Context;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_SHOP')")
 */
class ShopReferrerController extends AjaxAbstractController
{
    /**
     * Get shop referrers.
     *
     * @Route("/ajax/company/referrers", name="ajax_shop_referrers_index")
     * @return JsonResponse
     */
    public function index()
    {
        return $this->jsonResponse(
            $this->getUser()->getShop()->getUserShops(),
            [Context::SHOP]
        );
    }
}