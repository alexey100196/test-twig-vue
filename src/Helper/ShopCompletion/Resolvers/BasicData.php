<?php

namespace App\Helper\ShopCompletion\Resolvers;

use App\Helper\ShopCompletion\Resolvers\AbstractResolver;

class BasicData extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        if ($this->shop->getName()) {
            $this->addScore(25);
        }

        if ($this->shop->getDescription()) {
            $this->addScore(25);
        }

        if ($this->shop->getWebsite()) {
            $this->addScore(25);
        }

        if ($this->shop->getUser()->getPhone()) {
            $this->addScore(25);
        }
    }
}