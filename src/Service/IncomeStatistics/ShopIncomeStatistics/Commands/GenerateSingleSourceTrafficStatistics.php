<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Commands;


use App\Entity\DocumentReferrer\DocumentReferrer;
use App\Entity\Shop;
use App\Service\IncomeStatistics\CommandInterface;

class GenerateSingleSourceTrafficStatistics implements CommandInterface
{
    /** @var Shop */
    public $shop;

    /** @var DocumentReferrer */
    public $documentReferrer;

    /** @var \DateTime */
    public $startDate;

    /** @var \DateTime */
    public $endDate;

    /** @var array */
    public $productsIds;

    /** @var bool */
    public $includeMainOffer;

    public function __construct(
        Shop $shop,
        DocumentReferrer $documentReferrer,
        \DateTime $startDate,
        \DateTime $endDate,
        array $productsIds = [],
        bool $includeMainOffer = false
    )
    {
        $this->shop = $shop;
        $this->documentReferrer = $documentReferrer;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->productsIds = $productsIds;
        $this->includeMainOffer = $includeMainOffer;
    }
}