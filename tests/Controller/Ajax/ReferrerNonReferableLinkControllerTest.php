<?php

namespace App\Tests\Controller\Ajax;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ReferrerNonReferableLinkControllerTest extends WebTestCase
{
    /**
     * @testdox Redirect to login if user isn't referrer
     */
    public function testRedirect()
    {
        $client = static::createClient();
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $url = $client->getKernel()->getContainer()->getParameter('url');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/ajax/non-referable-link');

        $this->assertResponseRedirects($url.'/login', Response::HTTP_FOUND);
    }

    /**
     * @testdox Check if user hasn't referrer rule
     */
    public function testIsNotReferrer() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', null));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/ajax/non-referable-link');

        $this->assertResponseRedirects('/logged', Response::HTTP_FOUND);
    }

    /**
     * @testdox Check request without data
     */
    public function testWithoutData() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', 'john'));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/ajax/non-referable-link', []);
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
        $this->assertEquals(
            '{"errors":{"destinationUrl":["This value should not be blank. (null)"]}}',
            $client->getResponse()->getContent()
        );
    }

    /**
     * @testdox Check request with data
     */
    public function testWithData() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', 'john'));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/ajax/non-referable-link', [
            'destinationUrl' => 'http://google.pl',
            'title' => 'http://google.pl',
            'layout' => 'text'
        ]);

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent(), true);
//        echo $response['links'][0]['id']; // id to remove
        $this->assertEquals(true, $response['success']);
    }

    /**
     * @testdox Check request to remove with invalid data
     */
    public function testRemoveInvalidData() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', 'john'));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/ajax/non-referable-link/0', []);

        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
        $this->assertEquals('{"errors":{"link":"Not found link"}}', $client->getResponse()->getContent());
    }

    /**
     * @testdox Check request to remove with data
     */
    public function testRemoveData() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', 'john'));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/ajax/non-referable-link/8', []);

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals(true, $response['success']);
    }

    /**
     * @param string $filter
     * @param $value
     * @return Cookie
     */
    private function logIn(string $filter, $value)
    {
        $session = self::$container->get('session');
        $em = self::$container->get('doctrine.orm.entity_manager');

        $user = $em->getRepository(User::class)->findOneBy([$filter => $value]);

        $firewallName = 'main';

        $token = new UsernamePasswordToken($user, null, $firewallName, $user->getRoles());
        $session->set('_security_'.$firewallName, serialize($token));
        $session->save();

        return new Cookie($session->getName(), $session->getId());
    }
}
