<?php

namespace App\Service\Balance;

use App\Entity\Balance;
use App\Entity\Currency;
use App\Entity\TransactionType;
use App\Entity\UserTransaction;
use App\Event\Shop\ShopBalanceDecreased;
use App\Repository\TransactionRepository;
use App\Repository\UserBalanceRepository;
use App\Service\Balance\Entity\HasBalanceEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use PhpParser\Node\Scalar\String_;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserBalanceService extends BalanceService implements BalanceServiceInterface
{
    /**
     * @var UserBalanceRepository
     */
    private $userBalanceRepository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        EntityManagerInterface $em,
        TransactionRepository $transactionRepository,
        UserBalanceRepository $userBalanceRepository,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->em = $em;
        $this->transactionRepository = $transactionRepository;
        $this->userBalanceRepository = $userBalanceRepository;
        $this->eventDispatcher = $eventDispatcher;

        parent::__construct($em, $transactionRepository);
    }

    public function addTransaction(UuidInterface $uuid, HasBalanceEntity $user, Currency $currency, string $amount, TransactionType $type)
    {
        $balance = $user->findOrCreateBalance($currency);
        $transaction = new UserTransaction();
        $transaction->setUuid($uuid);
        $transaction->setUser($user);
        $transaction->setType($type);
        $transaction->setAmount($amount);
        $transaction->setBalance($balance);
        $this->process($transaction);

        // To Review Balance Update !
        $this->updateBalance($balance);

        if ($user->getShop() && $amount < 0) {
            $this->eventDispatcher->dispatch(ShopBalanceDecreased::NAME, new ShopBalanceDecreased($user->getShop(), $currency));
        }
    }

    public function updateTransaction(UuidInterface $uuid, float $amount)
    {

        $transaction = $this->transactionRepository->findOneByUuid($uuid);

        if (null === $transaction) {
            throw new EntityNotFoundException('Transaction doesn\'t exists');
        }

        $newTransaction = clone $transaction;
        $newTransaction->setAmount($amount);

        $this->em->beginTransaction();

        $this->em->remove($transaction);

        $this->em->flush();

        $this->em->persist($newTransaction);

        $this->em->flush();

        $this->em->commit();

        // To Review Balance Update !
        $balance = $transaction->getBalance();
        $this->updateBalance($balance);

        if ($transaction->getUser()->getShop() && $transaction->getAmount() > $amount) {
            $this->eventDispatcher->dispatch( ShopBalanceDecreased::NAME,
                new ShopBalanceDecreased($transaction->getUser()->getShop() , $transaction->getBalance()->getCurrency())
            );
        }
    }

    // To Review Balance Update !
    public function updateBalance(Balance $balance)
    {
        $balance->updateBalance();
        $this->em->persist($balance);
        $this->em->flush();
    }

    /**
     * @param HasBalanceEntity $target
     * @param Currency $currency
     * @param float|null $amount (null if want to check if has ANY funds)
     * @return bool
     */
    public function hasFunds(HasBalanceEntity $target, Currency $currency, float $amount = null): bool
    {
        $balance = $this->userBalanceRepository->findOneByUserAndCurrency($target, $currency);

        $this->em->refresh($balance);

        if (!$balance) {
            return false;
        }

        return $amount === null ? $balance->getAmount() > 0 : $balance->getAmount() >= $amount;
    }
}
