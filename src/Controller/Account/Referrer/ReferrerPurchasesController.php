<?php

namespace App\Controller\Account\Referrer;

use App\Entity\Coupon\CouponPackage;
use App\Entity\User;
use App\Repository\Coupon\UserCouponRepository;
use App\Repository\CurrencyRepository;
use App\Repository\UserBalancePayoutRepository;
use App\Service\Referrer\ReferrerShop;
use App\Service\UserBalancePayout\CreateUserBalancePayoutService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerPurchasesController
 *
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerPurchasesController extends AbstractController
{
    /**
     * @var UserCouponRepository
     */
    private $userCouponRepository;

    /**
     * ReferrerPurchasesController constructor.
     * @param UserCouponRepository $userCouponRepository
     */
    public function __construct(
        UserCouponRepository $userCouponRepository
    )
    {
        $this->userCouponRepository = $userCouponRepository;
    }

    /**
     * @Route("/account/referrer/purchases", name="account_referrer_purchases_index")
     */
    public function index()
    {
        /** @var User $user */
        $user = $this->getUser();

        $inviteCoupons = $this->userCouponRepository->findAllByUserQuery($user, [CouponPackage::ALLOCATION_INVITEE]);
        $referrerCoupons = $this->userCouponRepository->findAllByUserQuery($user, [CouponPackage::ALLOCATION_REFERRER]);

        return $this->render('account/referrer/purchases/index.html.twig', [
            'inviteCoupons' => count($inviteCoupons->getResult()),
            'referrerCoupons' => count($referrerCoupons->getResult())
        ]);
    }
}