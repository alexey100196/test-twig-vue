<?php


namespace App\Service\ShortLinks;


use App\Entity\ShortLinks\CustomShortLink;
use App\Entity\ShortLinks\CustomShortLinkTax;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class CustomShortLinkService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param CustomShortLink $customShortLink
     * @param array|null $tag
     * @param array|null $folder
     * @param array|null $utmSource
     * @param array|null $utmMedium
     * @param array|null $utmCampaign
     * @param array|null $utmContent
     * @param array|null $utmKeyword
     * @return CustomShortLink
     */
    public function save(CustomShortLink $customShortLink, ?array $tag, ?array $folder, ?array $utmSource, ?array $utmMedium, ?array $utmCampaign, ?array $utmContent, ?array $utmKeyword)
    {
        $tagArr = $this->loadNewArray('tag', $customShortLink, $tag);
        $folderArr = $this->loadNewArray('folder', $customShortLink, $folder);
        $utmSourceArr = $this->loadNewArray('utm_source', $customShortLink, $utmSource);
        $utmMediumArr = $this->loadNewArray('utm_medium', $customShortLink, $utmMedium);
        $utmCampaignArr = $this->loadNewArray('utm_campaign', $customShortLink, $utmCampaign);
        $utmContentArr = $this->loadNewArray('utm_content', $customShortLink, $utmContent);
        $utmKeywordArr = $this->loadNewArray('utm_keyword', $customShortLink, $utmKeyword);
        $customShortLink->setTaxes(array_merge($tagArr, $folderArr, $utmSourceArr, $utmMediumArr, $utmCampaignArr, $utmContentArr, $utmKeywordArr));
        $this->em->persist($customShortLink);
        $this->em->flush();

        return $customShortLink;
    }

    /**
     * @param CustomShortLink $customShortLink
     */
    public function remove(CustomShortLink $customShortLink)
    {
        $this->em->remove($customShortLink);
        $this->em->flush();
    }

    /**
     * @param string $type
     * @param CustomShortLink $link
     * @param array|null $newArr
     * @return array
     */
    private function loadNewArray(string $type, CustomShortLink $link, ?array $newArr) {
        $currentNames = [];
        $oldArr = $link->getTaxesByType($type);
        $res = [];
        if ($oldArr) {
            $nOldArr = $oldArr->toArray();
            $i = 0;
            foreach($nOldArr as $tag) {
                if ($newArr && isset($newArr[$i])) {
                    $newItem = $tag;
                    $newItem->setName($newArr[$i]);
                    if (!in_array(strtolower($newArr[$i]), $currentNames)) {
                        $currentNames[] = strtolower($newArr[$i]);
                        $res[] = $newItem;
                    }
                } else {
                    $link->removeTag($tag);
                    $this->em->remove($tag);
                }
                $i++;
            }

            if ($newArr && count($res) < count($newArr)) {
                for($i=count($res); $i<count($newArr); $i++) {
                    $newItem = new CustomShortLinkTax();
                    $newItem->setLink($link);
                    $newItem->setType($type);
                    $newItem->setName($newArr[$i]);
                    if (!in_array(strtolower($newArr[$i]), $currentNames)) {
                        $currentNames[] = strtolower($newArr[$i]);
                        $res[] = $newItem;
                    }
                }
            }
        } else if($newArr) {
            foreach($newArr as $tag) {
                $newItem = new CustomShortLinkTax();
                $newItem->setLink($link);
                $newItem->setType($type);
                $newItem->setName($tag);
                if (!in_array(strtolower($tag), $currentNames)) {
                    $currentNames[] = strtolower($tag);
                    $res[] = $newItem;
                }
            }
        }

        return $res;
    }
}