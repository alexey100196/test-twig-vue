<?php

namespace App\Repository;

use App\Entity\EventEntry;
use App\Entity\EventEntryType;
use App\Entity\Shop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EventEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventEntry[]    findAll()
 * @method EventEntry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventEntryRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EventEntry::class);
    }

    /**
     * @param Shop $shop
     * @param bool|null $test
     * @return mixed
     */
    public function getLastShopEntries(Shop $shop, bool $test = null)
    {
        $query = $this->createQueryBuilder('ee')
            ->andWhere('ee.shop = :shop');

        if ($test !== null) {
            $query = $query->andWhere('ee.test = :test')->setParameter('test', $test);
        }

        return $query->orderBy('ee.id', 'DESC')
            ->setParameter('shop', $shop)
            ->getQuery()
            ->getResult();
    }

}
