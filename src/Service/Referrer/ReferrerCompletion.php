<?php


namespace App\Service\Referrer;


use App\Entity\User;

class ReferrerCompletion
{
    /**
     * @param User $user
     * @return bool
     */
    public function isReferrerFullyCompleted(User $user): bool
    {
        return !empty($user->getNickname());
    }
}