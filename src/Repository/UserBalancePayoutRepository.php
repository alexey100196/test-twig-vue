<?php

namespace App\Repository;

use App\Entity\Currency;
use App\Entity\Shop;
use App\Entity\User;
use App\Entity\UserBalance;
use App\Entity\UserBalancePayout;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\Expr;

/**
 * @method UserBalancePayout|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserBalancePayout|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserBalancePayout[]    findAll()
 * @method UserBalancePayout[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserBalancePayoutRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserBalancePayout::class);
    }

    /**
     * @param User $user
     * @return iterable
     */
    public function getPayoutsForUser(User $user): iterable
    {
        return $this->createQueryBuilder('ubp')
            ->leftJoin(UserBalance::class, 'ub', Join::WITH, 'ubp.balance = ub')
            ->andWhere('ub.user = :user')
            ->setParameter('user', $user)
            ->orderBy('ubp.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return iterable|UserBalancePayout[]
     */
    public function findAllNotAccepted(): iterable
    {
        return $this->createQueryBuilder('ubp')
            ->where('ubp.acceptedAt IS NULL')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return iterable|UserBalancePayout[]
     */
    public function findAllAcceptedForDatePeriod(\DateTime $startDate, \DateTime $endDate): iterable
    {
        return $this->createQueryBuilder('ubp')
            ->andWhere('ubp.createdAt >= :startDate')
            ->andWhere('ubp.createdAt <= :endDate')
            ->setParameters(compact('startDate', 'endDate'))
            ->getQuery()
            ->getResult();
    }

    public function sumAmountForUser(User $user, bool $pendingOnly = false)
    {
        $qb = $this->createQueryBuilder('ubp')
            ->select('SUM(ubp.amount) as amount, c.name as currency')
            ->join(UserBalance::class, 'b')
            ->join(Currency::class, 'c', Expr\Join::WITH, 'b.currency = c')
            ->andWhere('b.user = :user')
            ->setParameter('user', $user);


        if ($pendingOnly) {
            $qb->andWhere('ubp.acceptedAt IS NULL');
        }

        $rows = $qb->groupBy('c')
            ->getQuery()
            ->getResult();

        $results = [];

        // Map to format: [EUR => 100]
        foreach ($rows as $row) {
            $results[$row['currency']] = $row['amount'];
        }

        return $results;
    }
}
