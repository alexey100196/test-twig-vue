export function userFactory (user) {

    function User(user) {
        _.assign(this, user);

        this.full_name = this.name + ' ' + this.surname;
    }

    return new User(user);
}