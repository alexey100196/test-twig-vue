<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReferrerCouponRepository")
 */
class ReferrerCoupon
{
    const TYPE_LINKS = 'linksEnabled';
    const TYPE_SET_OF_CARDS = 'setOfCardsEnabled';
    const TYPE_MY_STORE = 'myStoreEnabled';

    const TYPES = [
        self::TYPE_LINKS,
        self::TYPE_SET_OF_CARDS,
        self::TYPE_MY_STORE
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $code = 'Enter coupon code';

    /**
     * @ORM\Column(type="decimal", precision=10, scale=0)
     */
    private $value = 25;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CommissionType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $valueType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="referrerCoupons")
     * @ORM\JoinColumn(nullable=false)
     */
    private $referrer;

    /**
     * @ORM\Column(type="boolean")
     */
    private $linksEnabled;

    /**
     * @ORM\Column(type="boolean")
     */
    private $setOfCardsEnabled;

    /**
     * @ORM\Column(type="boolean")
     */
    private $myStoreEnabled;

    /**
     * @ORM\Column(type="datetime")
     */
    private $expiresAt;

    /**
     * @var Shop
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getValueType(): ?CommissionType
    {
        return $this->valueType;
    }

    public function setValueType(?CommissionType $valueType): self
    {
        $this->valueType = $valueType;

        return $this;
    }

    public function getReferrer(): ?User
    {
        return $this->referrer;
    }

    public function setReferrer(User $referrer): self
    {
        $this->referrer = $referrer;

        return $this;
    }

    public function getLinksEnabled(): ?bool
    {
        return $this->linksEnabled;
    }

    public function setLinksEnabled(bool $linksEnabled): self
    {
        $this->linksEnabled = $linksEnabled;

        return $this;
    }

    public function getSetOfCardsEnabled(): ?bool
    {
        return $this->setOfCardsEnabled;
    }
    public function getSetOfCardsEnabledAndNotExpired(): ?bool
    {
        return $this->setOfCardsEnabled && $this->getExpiresAt() >= new \DateTime();
    }

    public function setSetOfCardsEnabled(bool $setOfCardsEnabled): self
    {
        $this->setOfCardsEnabled = $setOfCardsEnabled;

        return $this;
    }

    public function getMyStoreEnabled(): ?bool
    {
        return $this->myStoreEnabled;
    }

    public function getMyStoreEnabledAndNotExpired(): ?bool
    {
        return $this->myStoreEnabled && $this->getExpiresAt() >= new \DateTime();
    }

    public function setMyStoreEnabled(bool $myStoreEnabled): self
    {
        $this->myStoreEnabled = $myStoreEnabled;

        return $this;
    }

    public function getExpiresAt(): ?\DateTimeInterface
    {
        return $this->expiresAt;
    }

    public function setExpiresAt(?\DateTimeInterface $expiresAt = null): self
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    /**
     * @param string $type @see(\App\Entity\ReferrerCoupon::TYPES)
     * @return bool
     */
    public function isEnabled(string $type): bool
    {
        return in_array($type, self::TYPES) && $this->$type && $this->getExpiresAt() >= new \DateTime();
    }

    public function getValueWithType()
    {
        if (!$this->getValue() || !$this->getValueType()) {
            return '';
        }

        return $this->getValue() . ($this->getValueType()->getId() === CommissionType::PERCENT_ID ? '%' : '€');
    }

    /**
     * @return Shop
     */
    public function getShop(): Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     */
    public function setShop(Shop $shop): void
    {
        $this->shop = $shop;
    }
}
