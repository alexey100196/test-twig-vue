<?php


namespace App\Service\Shop;


use App\Entity\Shop;
use App\Repository\ShopRepository;
use App\Util\Consts;
use Doctrine\ORM\EntityManagerInterface;

class ShopCookie
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var ShopRepository
     */
    protected $shopRepository;

    public function __construct(EntityManagerInterface $em, ShopRepository $shopRepository)
    {
        $this->em = $em;
        $this->shopRepository = $shopRepository;
    }

    /**
     * @param Shop $shop
     * @param mixed $cookieLifeTime
     * @param mixed $bannerCookieLifeTime
     * @return Shop
     */
    public function updateCookieLifeTime(Shop $shop, $cookieLifeTime = null, $bannerCookieLifeTime = null)
    {
        $shop->setCookieLifetime($cookieLifeTime === null ? null : intval($cookieLifeTime));
        $shop->setBannerCookieLifetime($bannerCookieLifeTime === null ? null : intval($bannerCookieLifeTime));

        $this->em->persist($shop);
        $this->em->flush();

        return $shop;
    }

    /**
     * @param Shop $shop
     * @return int
     */
    public function getShopCookieLifeTime(Shop $shop): int
    {
        return $shop->getCookieLifetime() ?? Consts::SNIPPET_DEFAULT_COOKIE_LIFETIME_DAYS;
    }

    /**
     * @param Shop $shop
     * @return int
     */
    public function getShopBannerCookieLifeTime(Shop $shop): int
    {
        return $shop->getBannerCookieLifetime() ?? Consts::SNIPPET_DEFAULT_BANNER_COOKIE_LIFETIME_DAYS;
    }
}