<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EventEntryTypeRepository")
 */
class EventEntryType extends BaseGlossary
{
    const TYPE_SIGN_UP = 'sign-up';
    const TYPE_PURCHASE = 'purchase';
    const TYPE_PURCHASE_COUPON = 'purchase-coupon';
    const TYPE_TEST_CONNECTION = 'test-connection';
    const TYPE_VIEW = 'view';
}
