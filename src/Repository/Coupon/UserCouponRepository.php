<?php

namespace App\Repository\Coupon;

use App\Entity\Coupon\Coupon;
use App\Entity\Coupon\CouponPackage;
use App\Entity\Coupon\UserCoupon;
use App\Entity\Shop;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserCoupon|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserCoupon|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserCoupon[]    findAll()
 * @method UserCoupon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserCouponRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserCoupon::class);
    }

    public function getLastNotUsedCouponForUser(User $user)
    {
        $qb = $this->getFindByUserAndStatusQueryBuilder($user);

        return $qb
                ->setMaxResults(1)
                ->getQuery()
                ->getResult()[0] ?? null;
    }


    public function findAllByUserQuery(User $user, array $allocation = [])
    {
        return $this->getFindByUserAndStatusQueryBuilder($user, $allocation)->getQuery();
    }

    private function getFindByUserAndStatusQueryBuilder(User $user, array $allocation = [])
    {
        $qb = $this->createQueryBuilder('u')
            ->andWhere('u.user = :user')
            ->setParameter('user', $user);


        if (!empty($allocation)) {
            $qb->join(Coupon::class, 'c', Join::WITH, 'c = u.coupon');
            $qb->join(CouponPackage::class, 'p', Join::WITH, 'p = c.package');
            $qb->andWhere('p.allocation IN (:allocation)')->setParameter('allocation', $allocation);
        }

        $qb->orderBy('u.id', 'DESC');

        return $qb;
    }

    public function getFindByShopQuery(Shop $shop): Query
    {
        $qb = $this->createQueryBuilder('u');
        $qb->join(Coupon::class, 'c', Join::WITH, 'c = u.coupon');
        $qb->join(CouponPackage::class, 'p', Join::WITH, 'p = c.package');
        $qb->join(User::class, 'us', Join::WITH, 'us = u.user');
        $qb->andWhere('p.shop = :shop')->setParameter('shop', $shop);

        $qb->orderBy('u.id', 'DESC');

        return $qb->getQuery();
    }

    public function findOneByShopAndCouponCodeAndAllocation(Shop $shop, string $code, int $allocation): ?UserCoupon
    {
        $qb = $this->createQueryBuilder('u');
        $qb->join(Coupon::class, 'c', Join::WITH, 'c = u.coupon');
        $qb->join(CouponPackage::class, 'p', Join::WITH, 'p = c.package');
        $qb->andWhere('p.shop = :shop')->setParameter('shop', $shop);
        $qb->andWhere('c.code = :code')->setParameter('code', $code);
        $qb->andWhere('p.allocation = :allocation')->setParameter('allocation', $allocation);

        return $qb->getQuery()->setMaxResults(1)->getOneOrNullResult();
    }

    public function findUserCouponByShopUserAndFriendEmail(Shop $shop, User $user, string $inviteeEmail, int $allocation): ?UserCoupon
    {
        $qb = $this->createQueryBuilder('u');
        $qb->join(Coupon::class, 'c', Join::WITH, 'c = u.coupon');
        $qb->join(CouponPackage::class, 'p', Join::WITH, 'p = c.package');
        $qb->andWhere('p.shop = :shop')->setParameter('shop', $shop);
        $qb->andWhere('p.allocation = :allocation')->setParameter('allocation', $allocation);
        $qb->andWhere('u.user = :user');
        $qb->andWhere('u.inviteeEmail = :inviteeEmail');

        $qb->setParameters([
            'shop' => $shop,
            'user' => $user,
            'inviteeEmail' => $inviteeEmail,
            'allocation' => $allocation,
        ]);

        return $qb->getQuery()->setMaxResults(1)->getOneOrNullResult();
    }
}
