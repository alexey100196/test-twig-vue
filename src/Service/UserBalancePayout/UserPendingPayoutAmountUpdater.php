<?php


namespace App\Service\UserBalancePayout;


use App\Entity\Currency;
use App\Entity\User;
use App\Repository\UserBalancePayoutRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserPendingPayoutAmountUpdater
{
    /**
     * @var UserBalancePayoutRepository
     */
    private $userBalancePayoutRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param UserBalancePayoutRepository $userBalancePayoutRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(UserBalancePayoutRepository $userBalancePayoutRepository, EntityManagerInterface $em)
    {
        $this->userBalancePayoutRepository = $userBalancePayoutRepository;
        $this->em = $em;
    }

    public function update(User $user)
    {
        $summary = $this->userBalancePayoutRepository->sumAmountForUser($user, true);

        if (!isset($summary[Currency::EUR])) {
            $summary[Currency::EUR] = 0;
        }

        $user->setPendingPayoutAmount((float)$summary[Currency::EUR]);

        $this->em->persist($user);
        $this->em->flush();
    }
}