<?php


namespace App\Manager;


use App\Entity\Currency;
use App\Entity\Transaction;
use App\Entity\TransactionType;
use App\Interfaces\Entity\HasBalanceInterface;
use App\Repository\BalanceRepository;
use App\ValueObject\Money;

abstract class TransactionManager extends AbstractManager
{
    /**
     * @param HasBalanceInterface $target
     * @return mixed
     */
    abstract protected function newTransaction(HasBalanceInterface $target);

    /**
     * Store new transaction.
     * @param HasBalanceInterface $target
     * @param TransactionType $type
     * @param Money $money
     * @return Transaction
     */
    public function addTransaction(HasBalanceInterface $target, TransactionType $type, Money $money): Transaction
    {
        // Get balance for transaction
        $balance = $target->findOrCreateBalance($money->getCurrency());

        // Prepare transaction
        $transaction = $this->newTransaction($target);
        $transaction->setBalance($balance);
        $transaction->setAmount($money->getAmount());
        $transaction->setType($type);

        // Store transaction
        $this->entityManager->persist($transaction);
        $this->entityManager->flush();

        return $transaction;
    }
}