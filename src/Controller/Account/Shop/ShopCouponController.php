<?php

namespace App\Controller\Account\Shop;

use App\Repository\Coupon\UserCouponRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ShopIncomeController
 *
 * @Security("has_role('ROLE_SHOP')")
 */
class ShopCouponController extends AbstractController
{
    /**
     * @Route("/account/company/coupon", name="account_shop_coupon")
     */
    public function index(UserCouponRepository $userCouponRepository, Request $request, PaginatorInterface $paginator)
    {
        $userCoupons = $paginator->paginate(
            $userCouponRepository->getFindByShopQuery($this->getUser()->getShop()),
            $request->query->getInt('page', 1)
        );

        return $this->render('account/shop/coupon/index.html.twig', ['userCoupons' => $userCoupons]);
    }
}