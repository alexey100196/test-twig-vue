<?php

namespace App\Listener\Event;

use App\Adapter\MailSender;
use App\Event\ContactRequest\ContactRequestCreated;
use App\Event\User\UserRegistered;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;


class ContactRequestListener
{
    /**
     * @var MailSender
     */
    private $mailSender;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var string
     */
    private $developerEmail;

    /**
     * UserRegistrationListener constructor.
     * @param \Twig_Environment $twig
     * @param MailSender $mailSender
     */
    public function __construct(\Twig_Environment $twig, MailSender $mailSender)
    {
        $this->twig = $twig;
        $this->mailSender = $mailSender;
    }

    /**
     * @param ContactRequestCreated $contactRequestCreated
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendEmailToDeveloper(ContactRequestCreated $contactRequestCreated)
    {
        $this->mailSender->send(
            'hello@superinterface.com',
            $this->developerEmail,
            'SuperInterface contact request.',
            $this->twig->render('email/contact-request.html.twig', ['contactRequest' => $contactRequestCreated->getContactRequest()])
        );
    }

    /**
     * @param string $developerEmail
     */
    public function setDeveloperEmail(string $developerEmail)
    {
        $this->developerEmail = $developerEmail;
    }

}
