<?php


namespace App\Controller\Account\Referrer;

use App\Entity\ReferrerStoreNonReferableLink;
use App\Form\Referrer\ReferrerStoreNonReferableLinkType;
use App\Repository\ReferrerLinkRepository;
use App\Util\Consts;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerStoreController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerStoreController extends AbstractController
{
    /**
     * @Route("/account/referrer/store", name="account_referrer_store_index", methods={"GET", "POST"})
     * @param Request $request
     * @param ReferrerLinkRepository $referrerLinkRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, ReferrerLinkRepository $referrerLinkRepository)
    {
        $forms = $this->createForms($request);

        foreach ($forms as $form) {
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($form->getData());
                $em->flush();

                $this->addFlash(Consts::SUCCESS, 'Non Refferable Link Saved');
            }
        }

        return $this->render('account/referrer/store/index.html.twig', [
            'forms' => $forms->map(function ($form) {
                return $form->createView();
            }),
            'myStoreReferrerLinks' => $referrerLinkRepository->findBy(['referrer' => $this->getUser(), 'showInMyStore' => true], ['createdAt' => 'DESC'])
        ]);
    }

    /**
     * @Route("/account/referrer/store/{referrerStoreNonReferableLink}", name="account_referrer_store_destroy", methods={"DELETE"})
     * @Security("user === referrerStoreNonReferableLink.getReferrer()")
     *
     * @param ReferrerStoreNonReferableLink $referrerStoreNonReferableLink
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(ReferrerStoreNonReferableLink $referrerStoreNonReferableLink)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($referrerStoreNonReferableLink);
        $em->flush();

        $this->addFlash(Consts::SUCCESS, 'Non Refferable Link Deleted');

        return new JsonResponse([]);
    }

    /**
     * @param Request $request
     * @return ArrayCollection
     */
    private function createForms(Request $request): ArrayCollection
    {
        $forms = new ArrayCollection();

        for ($i = 0; $i < $this->getParameter('my_store_links_count'); $i++) {
            // Prepare data.
            $item = $this->getUser()->getStoreNonReferableLinkByOrderColumn($i) ?? new ReferrerStoreNonReferableLink();
            $item->setOrderColumn($i);
            $item->setReferrer($this->getUser());
            // Prepare form.
            $form = $this->createNamedForm('store_' . $i, ReferrerStoreNonReferableLinkType::class, $item);
            $form->handleRequest($request);
            $forms->add($form);
        }

        return $forms;
    }

    /**
     * @param $name
     * @param $type
     * @param null $data
     * @param array $options
     * @return mixed
     */
    private function createNamedForm($name, $type, $data = null, array $options = array())
    {
        return $this->container->get('form.factory')->createNamed($name, $type, $data, $options);
    }
}