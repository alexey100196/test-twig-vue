<?php

namespace App\Helper\ShopCompletion\Resolvers;

use App\Helper\ShopCompletion\Resolvers\AbstractResolver;

class Preview extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        foreach (['BasicData', 'MainOffer', 'OptionalOffer'] as $step) {
            if ($this->getResolver()->getResult($step)->getScore() < 100) {
                return;
            }
        }

        if ($this->shop->hasOnlyCouponRewards()) {
            $this->addScore(100);
            return;
        }

        $this->addScore(100);
    }
}
