<?php

namespace App\Repository\Subscription;

use App\Entity\Subscription\PlanSubscription;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PlanSubscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlanSubscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlanSubscription[]    findAll()
 * @method PlanSubscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanSubscriptionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PlanSubscription::class);
    }

    public function findActiveSubscriptionsWithEndedPeriod()
    {
        $now = new \DateTime();

        $qb = $this->createQueryBuilder('p');
        $qb->setParameter('now', $now);

        $qb->where('p.endsAt <= :now');
        $qb->andWhere('p.cancelsAt > :now OR p.cancelsAt IS NULL');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param \DateTime $endedAtStart
     * @param \DateTime $endedAtEnds
     * @return PlanSubscription[]
     */
    public function findEndedTrialSubscriptions(\DateTime $endedAtStart, \DateTime $endedAtEnds)
    {
        $qb = $this->createQueryBuilder('p');

        $qb->where('p.trialEndsAt = p.endsAt');
        $qb->andWhere('p.trialEndsAt >= :start AND p.trialEndsAt <= :end');
        $qb->andWhere('p.cancelsAt IS NULL');
        $qb->setParameters(['start' => $endedAtStart, 'end' => $endedAtEnds]);

        return $qb->getQuery()->getResult();
    }
}
