<?php


namespace App\Controller\Account\Referrer;


use App\Entity\User;
use App\Repository\CurrencyRepository;
use App\Repository\UserBalancePayoutRepository;
use App\Service\Referrer\ReferrerShop;
use App\Service\UserBalancePayout\CreateUserBalancePayoutService;
use App\Service\UserBalancePayout\CreateUserBalancePayoutService\InsufficientFundsException;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerProgramController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerPayoutController extends AbstractController
{
    /**
     * @var ReferrerShop
     */
    private $referrerShop;

    /**
     * @var UserBalancePayoutRepository
     */
    private $userBalancePayoutRepository;

    /**
     * @var CreateUserBalancePayoutService
     */
    private $createUserBalancePayoutService;

    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    /**
     * ReferrerShopController constructor.
     * @param ReferrerShop $referrerShop
     * @param UserBalancePayoutRepository $userBalancePayoutRepository
     * @param CreateUserBalancePayoutService $createUserBalancePayoutService
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(
        ReferrerShop $referrerShop,
        UserBalancePayoutRepository $userBalancePayoutRepository,
        CreateUserBalancePayoutService $createUserBalancePayoutService,
        CurrencyRepository $currencyRepository
    )
    {
        $this->referrerShop = $referrerShop;
        $this->userBalancePayoutRepository = $userBalancePayoutRepository;
        $this->createUserBalancePayoutService = $createUserBalancePayoutService;
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @Route("/account/referrer/payouts", name="account_referrer_payouts_index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $globalStats = $this->referrerShop->getReferrerStats($this->getUser());
        $payouts = $this->userBalancePayoutRepository->getPayoutsForUser($this->getUser());

        return $this->render('account/referrer/payout/index.html.twig', [
            'globalStats' => $globalStats,
            'payouts' => $payouts,
        ]);
    }

    /**
     * @Route("/account/referrer/request-payout", name="account_referrer_request_payout", methods={"POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function requestPayout()
    {
        /** @var User $user */
        $user = $this->getUser();

        $eurCurrency = $this->currencyRepository->findOneByName('EUR');
        $userBalance = $user->findOrCreateBalance($eurCurrency);

        try {
            ($this->createUserBalancePayoutService)($this->getUser(), $eurCurrency, $userBalance->getAmount());
            $this->addFlash(Consts::SUCCESS, 'Payout requested.');
        } catch (InsufficientFundsException | \InvalidArgumentException $e) {
            $this->addFlash(Consts::ERROR, $e->getMessage());
        }

        return $this->redirectToRoute('account_referrer_payouts_index');
    }
}
