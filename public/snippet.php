<?php ?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>SNIPPET</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
        <script src="snippet/SuperInterface.js"></script>
        <script>
            const SuperInterfaceID = "D209FA47C71964E14522";
        </script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div id="buy">BUY</div>
        <div id="register">REGISTER</div>

        
        <script>
            document.getElementById('buy').onclick = function(e) {
                SuperInterface.push({
                    "event" : "purchase",
                    "data": {
                        "product": 2,
                        "price": 10,
                        "amount": 10
                    }
                });
            };

            document.getElementById('register').onclick = function(e) {
                SuperInterface.push({
                    "event" : "signup"
                });
            };
        </script>
    </body>
</html>