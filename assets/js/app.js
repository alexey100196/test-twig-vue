
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

import Vue from 'vue';
import "regenerator-runtime/runtime.js";

Vue.use(require('vue-moment'));

import PageTitle from './Vue/Components/Account/PageTitle'
import ProgressBar from './Vue/Components/Account/ProgressBar'
import AccountHeaderBanner from './Vue/Components/Account/AccountHeaderBanner'
import CompanySidebarMenu from './Vue/Components/Account/Company/CompanySidebarMenu'
import ShopSidebarMenu from './Vue/Components/Account/Shop/ShopSidebarMenu'
import SectionTitle from './Vue/Components/Account/SectionTitle'
import CustomSwitch from './Vue/Components/Account/Input/CustomSwitch'
import AlertBag from './Vue/Components/Account/AlertBag'
import IconEditor from './Vue/Components/Account/Shop/IconEditor/IconEditor'
import CustomInput from './Vue/Components/Account/Input/Custom'
import CustomRadio from './Vue/Components/Account/Input/CustomRadio'
import ChooseInput from './Vue/Components/Account/Input/Choose'
import AmountInput from './Vue/Components/Account/Input/Amount'
import CopyInput from './Vue/Components/Account/Input/Copy'
import ChooseRadio from './Vue/Components/Account/Input/ChooseRadio'
import SubmitButton from './Vue/Components/Account/SubmitButton'
import ContactBox from './Vue/Components/Account/ContactBox'
import CustomModal from './Vue/Components/Account/CustomModal'
import WidgetTopBar from './Vue/Components/Account/Widget/TopBar'
import WidgetContent from './Vue/Components/Account/Widget/Content'
import WidgetCoupon from './Vue/Components/Account/Widget/Coupon'
import WidgetEmailTitle from './Vue/Components/Account/Widget/EmailTitle'
import WidgetEmail from './Vue/Components/Account/Widget/Email'
import ShortLinksHome from './Vue/Components/Referrer/ShortLinks'
import CustomShortLinks from './Vue/Components/Referrer/ShortLinks/Custom'
import PartnerShortLinks from './Vue/Components/Referrer/ShortLinks/Partner'
import ReferrerIncome from './Vue/Components/Referrer/Income'
import MyStoreReferrer from './Vue/Components/Referrer/MyStore'
import MyStoreReferrerCreate from './Vue/Components/Referrer/MyStore/Create'
import MyStoreReferrerModal from './Vue/Components/Referrer/MyStore/Modal'
import ShopIncome from './Vue/Components/Shop/Income'
import ShopPurchase from './Vue/Components/Shop/Purchase'
import ShopPayout from './Vue/Components/Shop/Payout'
import ShopStatsSummary from './Vue/Components/Shop/StatsSummary'
import AddLinkButton from './Vue/Components/Referrer/MyStore/AddLinkButton'
import MyStoreReferrableLink from './Vue/Components/Referrer/MyStore/MyStoreReferrableLink'
import ShopTransactionsReject from './Vue/Components/Shop/Transactions/Reject'
import ShopTransactionsAccept from './Vue/Components/Shop/Transactions/Accept'
import AdminShopChangeStatus from './Vue/Components/Admin/Shop/ChangeStatus';
import AdminCommonsDescriptionModal from './Vue/Components/Admin/Commons/DescriptionModal';
import AdminReferrerLinkContentCard from './Vue/Components/Admin/ReferrerLink/Content/Card.vue';
import AdminReferrerLinkContentStoreCard from './Vue/Components/Admin/ReferrerLink/Content/StoreCard.vue';
import AdminShopAddTransaction from './Vue/Components/Admin/Shop/AddTransaction.vue';
import AdminSubscriptionRenew from './Vue/Components/Admin/Subscription/Renew.vue';
import SubscriptionCancel from './Vue/Components/Subscription/Cancel.vue';
import SubscriptionContinuationDecision from './Vue/Components/Subscription/ContinuationDecision.vue';
import SubscriptionSubscribeButton from './Vue/Components/Subscription/SubscribeButton.vue';
import WidgetHeaderText from './Vue/Components/Shop/Widgets/WidgetHeaderText.vue';
import AmountValue from './Vue/Components/Admin/CompanyBudget';
import SetCards from './Vue/Components/Referrer/SetCards/index.vue';
import RewardStatusSwitcher from './Vue/Components/Shop/Widgets/RewardStatusSwitcher';
import WidgetHeader from './Vue/Components/Account/Widget/Header'
import CouponModal from "./Vue/Components/Coupons/CouponsModal";
import Sidebar from "./Vue/Components/Sidebar";
import CompanySidebar from "./Vue/Components/Shop/CompanySidebar";
import ReferrerSidebar from "./Vue/Components/Referrer/ReferrerSidebar";
import MyStoreSidebar from "./Vue/Components/Referrer/MyStore/MyStoreSidebar";
import ReferrerShop from "./Vue/Components/Account/Referrer/Shop";
import ReferrerEditCard from "./Vue/Components/Account/Referrer/Card/Edit";
import ReferrerCreateCustomCard from "./Vue/Components/Account/Referrer/Card/CreateCustom";
import ReferrerEditCustomCard from "./Vue/Components/Account/Referrer/Card/EditCustom";
import ReferrerPreviewCard from "./Vue/Components/Account/Referrer/Card/Preview";
import WidgetRewards from "./Vue/Components/Account/Shop/Widget/Rewards";
import SidebarButton from "./Vue/Components/SidebarButton";
import SignUp from './Vue/CR_Referrer/SignUp';
import VTooltip from 'v-tooltip'
import TextareaAutosize from 'vue-textarea-autosize'
import Toasted from 'vue-toasted'
import { DataTables, DataTablesServer } from 'vue-data-tables'
import ElementUI from 'element-ui'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'

locale.use(lang)

import VModal from 'vue-js-modal';
import Clipboard from 'v-clipboard';
import vSelect from 'vue-select';
import VueConfirmDialog from 'vue-confirm-dialog';

Vue.use(VModal, { dialog: true })
Vue.use(Clipboard)
Vue.use(VTooltip)
Vue.use(TextareaAutosize)
Vue.use(Toasted, {
    position: 'bottom-right',
    duration: 5000,
    singleton: true
})
Vue.use(ElementUI)
Vue.use(DataTables)
Vue.use(DataTablesServer)
Vue.use(VueConfirmDialog)
Vue.component('v-select', vSelect)
Vue.component('vue-confirm-dialog', VueConfirmDialog.default)
Vue.component('sign-up', SignUp)


const app = new Vue({
    el: '#app',
    components: {
        PageTitle,
        ProgressBar,
        AccountHeaderBanner,
        CompanySidebarMenu,
        ShopSidebarMenu,
        SectionTitle,
        CustomSwitch,
        AlertBag,
        IconEditor,
        CustomInput,
        CustomRadio,
        ChooseInput,
        AmountInput,
        CopyInput,
        SubmitButton,
        ChooseRadio,
        ContactBox,
        CustomModal,
        WidgetTopBar,
        WidgetContent,
        WidgetCoupon,
        WidgetEmailTitle,
        WidgetEmail,
        ShortLinksHome,
        CustomShortLinks,
        PartnerShortLinks,
        ReferrerIncome,
        MyStoreReferrer,
        MyStoreReferrerCreate,
        MyStoreReferrerModal,
        ShopIncome,
        ShopPurchase,
        ShopPayout,
        ShopStatsSummary,
        AddLinkButton,
        MyStoreReferrableLink,
        ShopTransactionsReject,
        ShopTransactionsAccept,
        AdminShopChangeStatus,
        AdminCommonsDescriptionModal,
        AdminReferrerLinkContentCard,
        AdminReferrerLinkContentStoreCard,
        AdminShopAddTransaction,
        AdminSubscriptionRenew,
        SubscriptionCancel,
        SubscriptionContinuationDecision,
        SubscriptionSubscribeButton,
        WidgetHeaderText,
        AmountValue,
        SetCards,
        RewardStatusSwitcher,
        WidgetHeader,
        CouponModal,
        Sidebar,
        CompanySidebar,
        ReferrerSidebar,
        MyStoreSidebar,
        SidebarButton,
        ReferrerShop,
        ReferrerEditCard,
        ReferrerCreateCustomCard,
        ReferrerEditCustomCard,
        ReferrerPreviewCard,
        WidgetRewards,
        SignUp
    }
});
