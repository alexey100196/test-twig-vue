<?php


namespace App\Manager;

use App\Entity\UserTransaction;
use App\Interfaces\Entity\HasBalanceInterface;

class UserTransactionManager extends TransactionManager
{
    /**
     * {@inheritdoc}
     */
    protected function newTransaction(HasBalanceInterface $target)
    {
        $userTransaction = new UserTransaction();
        $userTransaction->setUser($target);

        return $userTransaction;
    }
}