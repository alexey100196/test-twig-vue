<?php


namespace App\Controller\Widget;

use App\Controller\AjaxAbstractController;
use App\Entity\Shop;
use App\Entity\User;
use App\Form\Widget\FormData\ReferrerRegisterEmailVerification;
use App\Form\Widget\ReferrerRegisterBasicType;
use App\Form\Widget\ReferrerRegisterEmailVerificationType;
use App\Form\Widget\ReferrerRegisterFinancialType;
use App\Presenter\Widget\ButtonInfoPresenter;
use App\Presenter\Widget\IframeInfoPresenter;
use App\Repository\CampaignRepository;
use App\Repository\MediaRepository;
use App\Repository\ShopRepository;
use App\Repository\UserRepository;
use App\Service\Coupon\GetValidCouponCode;
use App\Service\PaypalService;
use App\Service\Widget\RegisterReferrerService;
use App\Util\Consts;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\VarDumper\Cloner\Data;
use Symfony\Component\Yaml\Yaml;



/**
 * @Route("/widget")
 */
class WidgetController extends AjaxAbstractController
{
    /**
     * @var string
     */
    private $iframeCookieName = 'valuelinkIframePartnerN';

    /**
     * @var CampaignRepository
     */
    private $campaignRepository;

    public function __construct(
        RegisterReferrerService $registerReferrerService,
        CampaignRepository $campaignRepository
    )
    {
        $this->registerReferrerService = $registerReferrerService;
        $this->campaignRepository = $campaignRepository;
    }

    /**
     * @Route("/iframe/paypal", name="widget_iframe_paypal", methods={"GET"})
     *
     * @param Request $request
     * @param ShopRepository $shopRepository
     * @param PaypalService $paypalService
     * @param ValidatorInterface $validator
     * @param RegisterReferrerService $registerReferrerService
     * @param UserRepository $userRepository
     * @return Response
     */
    public function paypal(Request $request, ShopRepository $shopRepository, PaypalService $paypalService, ValidatorInterface $validator, RegisterReferrerService $registerReferrerService, UserRepository $userRepository)
    {
        if (!isset($_COOKIE[$this->iframeCookieName])) {
            throw new NotFoundHttpException('Partner number not found. This request is outdated or you have disabled cookies in the browser.');
        }

        $number = $_COOKIE[$this->iframeCookieName];
        $shop = $shopRepository->findOneBy(['partnerNumber' => $number]);
        if (!$shop) {
            throw new NotFoundHttpException('Shop not found.');
        }

        $code = $request->get('code', null);
        $scope = $request->get('scope', null);
        $constraints = array(
            new \Symfony\Component\Validator\Constraints\NotBlank()
        );

        // Check code
        $error = $validator->validate($code, $constraints);
        if (count($error) > 0) {
            throw new HttpException('Invalid "paypal code" parameter');
        }

        // Check scope
        $error = $validator->validate($scope, $constraints);
        if (count($error) > 0) {
            throw new HttpException('Invalid "scope" parameter');
        }

        $tokens = $paypalService->getTokens($code);
        $userInfo = $paypalService->getUserInfo($tokens->getRefreshToken());
        $form = $this->createForm(ReferrerRegisterBasicType::class);
        $form->submit([
            'name' => $userInfo->getName(),
            'email' => $userInfo->getEmail()
        ]);

        if (!$form->isValid()) {
            throw new HttpException('Invalid paypal data: name or email');
        }

        /** @var User $data */
        $data = $form->getData();

        $user = $userRepository->findOneBy(['email' => $data->getEmail()]);

        if (null === $user) {
            $user = $registerReferrerService->createBasicAccount(
                $data->getEmail(),
                $data->getName(),
                $request->getLocale(),
                true
            );
        }

        $hasActiveCampaign = !empty($this->campaignRepository->findActiveByShop($shop));

        setcookie($this->iframeCookieName, '', time() + (86400 * 1), '/');

        return $this->render('widget-page/index.html.twig', [
            'company_id' => $shop->getPartnerNumber(),
            'paypalEmail' => $user->getEmail(),
            'hasActiveCampaign' => $hasActiveCampaign
        ]);
    }

    /**
     * @Route("/iframe/{number}", name="widget_iframe", methods={"GET"})
     *
     * @param string $number
     * @param Request $request
     * @param ShopRepository $shopRepository
     *
     * @return Response
     */
    public function iframe(string $number, Request $request, ShopRepository $shopRepository)
    {
        $shop = $shopRepository->findOneBy(['partnerNumber' => $number]);

        if (!$shop) {
            throw new NotFoundHttpException('Shop not found.');
        }

        $hasActiveCampaign = !empty($this->campaignRepository->findActiveByShop($shop));

        setcookie($this->iframeCookieName, $number, time() + (86400 * 1)); // 86400 = 1 day

        return $this->render('widget-page/index.html.twig', [
            'company_id' => $shop->getPartnerNumber(),
            'hasActiveCampaign' => $hasActiveCampaign
        ]);
    }

    /**
     * @Route("/content/{number}", name="widget_content", methods={"GET"})
     *
     * @param string $number
     * @param LoggerInterface $logger
     *
     * @return JsonResponse
     */
    public function content(string $number, LoggerInterface $logger, ShopRepository $shopRepository)
    {
        $shop = $shopRepository->findOneBy(['partnerNumber' => $number]);

        if (!$shop) {
            return new JsonResponse([], 400);
        }

        if (empty($this->campaignRepository->findActiveByShop($shop))) {
            throw new NotFoundHttpException('Not found active campaign for this shop.');
        }

        $lang = $shop->getWidgetSetting() && $shop->getWidgetSetting()->getLanguage() ? $shop->getWidgetSetting()->getLanguage()->getCode() : 'en';

        try {
            $file = Yaml::parseFile(__DIR__ . '/../../../translations/widget.' . $lang . '.yaml');
            return new JsonResponse($file, 200, [
                'Access-Control-Allow-Credentials' => 'true'
            ]);
        } catch (\Exception $e) {
            $logger->critical('Cant resolve language translation file for: ' . $lang);
            return new JsonResponse([], 400);
        }
    }

    /**
     * @Route("/iframe-info/{number}", name="widget_iframe_info", methods={"GET"})
     *
     * @param string $number
     * @param Request $request
     * @param ShopRepository $shopRepository
     * @param GetValidCouponCode $getValidCouponCode
     * @return JsonResponse
     */
    public function iframeInfo(string $number, Request $request, ShopRepository $shopRepository, GetValidCouponCode $getValidCouponCode, LoggerInterface $logger)
    {
        try {
            $shop = $shopRepository->findOneBy(['partnerNumber' => $number]);

            if (!$shop) {
                throw new NotFoundHttpException('Shop not found.');
            }

            if (empty($this->campaignRepository->findActiveByShop($shop))) {
                throw new NotFoundHttpException('Not found active campaign for this shop.');
            }

            $this->validateShopHeaders($request, $shop);

            try {
                $presenter = new IframeInfoPresenter($shop, $getValidCouponCode);
            } catch (\InvalidArgumentException $exception) {
                $logger->error($exception);
                throw new NotFoundHttpException('Invalid shop.');
            }

            $response = new JsonResponse($presenter->toArray(), 200, [
                'Access-Control-Allow-Credentials' => 'true'
            ]);
            $response->headers->setCookie(new Cookie('superInterfaceTest', $shop->getId()));

            return $response;
        } catch (\Exception $e) {
            $logger->error($e);
            return new JsonResponse($e->getMessage(), Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * @Route("/button-info/{number}", name="widget_button_info", methods={"GET"})
     *
     * @param string $number
     * @param Request $request
     * @param ShopRepository $shopRepository
     *
     * @param GetValidCouponCode $getValidCouponCode
     * @return JsonResponse
     */
    public function buttonInfo(string $number, Request $request, ShopRepository $shopRepository, GetValidCouponCode $getValidCouponCode)
    {
        $shop = $shopRepository->findOneBy(['partnerNumber' => $number]);

        if (!$shop || !$shop->getWidgetButton()) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        $campaignsArr = $this->campaignRepository->findActiveByShop($shop);
        if (empty($campaignsArr)) {
            throw new NotFoundHttpException('Not found active campaign for this shop.');
        }

        $this->validateShopHeaders($request, $shop);
        $presenter = new ButtonInfoPresenter($shop, $this->getUser(), $campaignsArr[0]);

        return new JsonResponse($presenter->toArray());
    }

    /**
     * @Route("/shop/{number}/products", name="widget_shop_products", methods={"GET"})
     *
     * @param string $number
     * @param Request $request
     * @param ShopRepository $shopRepository
     * @return void
     */
    public function shopProducts(string $number, Request $request, ShopRepository $shopRepository)
    {
        $shop = $shopRepository->findOneBy(['partnerNumber' => $number]);

        if (!$shop) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        $this->validateShopHeaders($request, $shop);

        $products = [];

        foreach ($shop->getProducts() as $product) {
            $products[] = [
                'name' => $product->getName(),
                'description' => $product->getDescription(),
                'offer' => $product->getCpsOffer() ? $product->getCpsOffer()->getShortLabel() : null,
                'website' => $product->getWebsite()
            ];
        }
        return new JsonResponse($products);
    }

    private function validateShopHeaders(Request $request, Shop $shop)
    {
        $referer = $request->headers->get('referer');

        if (!$referer) {
            throw new NotFoundHttpException('Invalid shop.');
        }

        $shopWebsite = parse_url($shop->getWebsite());

        if (!isset($shopWebsite['host']) || strpos($referer, $shopWebsite['host']) === -1) {
            throw new NotFoundHttpException('Invalid shop.');
        }
    }




}