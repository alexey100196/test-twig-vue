<?php

namespace App\Entity\ShortLinks;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShortLinks\AffiliateShortLinkRepository")
 * @ORM\Table(name="affiliate_short_links")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class AffiliateShortLink
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $referrer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CustomDomain")
     * @ORM\JoinColumn(nullable=true)
     */
    private $customDomain;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $short;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ReferrerLink")
     * @ORM\JoinColumn(nullable=false)
     */
    private $referrerLink;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean")
     */
    private $facebookPixel = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $twitterPixel = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pinterestPixel = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $linkedinPixel = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $googlePixel = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $quoraPixel = 0;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $metaTitle;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaDesc;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaImage;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaIframe;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShortLinks\AffiliateShortLinkTax", mappedBy="link", cascade={"persist", "remove"})
     */
    private $taxes;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getReferrer()
    {
        return $this->referrer;
    }

    /**
     * @param mixed $referrer
     */
    public function setReferrer($referrer): void
    {
        $this->referrer = $referrer;
    }

    /**
     * @return mixed
     */
    public function getCustomDomain()
    {
        return $this->customDomain;
    }

    /**
     * @param mixed $customDomain
     */
    public function setCustomDomain($customDomain): void
    {
        $this->customDomain = $customDomain;
    }

    /**
     * @return mixed
     */
    public function getShort()
    {
        return $this->short;
    }

    /**
     * @param mixed $short
     */
    public function setShort($short): void
    {
        $this->short = $short;
    }

    /**
     * @return mixed
     */
    public function getReferrerLink()
    {
        return $this->referrerLink;
    }

    /**
     * @param mixed $referrerLink
     */
    public function setReferrerLink($referrerLink): void
    {
        $this->referrerLink = $referrerLink;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getFacebookPixel(): int
    {
        return $this->facebookPixel;
    }

    /**
     * @param int $facebookPixel
     */
    public function setFacebookPixel(int $facebookPixel): void
    {
        $this->facebookPixel = $facebookPixel;
    }

    /**
     * @return int
     */
    public function getTwitterPixel(): int
    {
        return $this->twitterPixel;
    }

    /**
     * @param int $twitterPixel
     */
    public function setTwitterPixel(int $twitterPixel): void
    {
        $this->twitterPixel = $twitterPixel;
    }

    /**
     * @return int
     */
    public function getPinterestPixel(): int
    {
        return $this->pinterestPixel;
    }

    /**
     * @param int $pinterestPixel
     */
    public function setPinterestPixel(int $pinterestPixel): void
    {
        $this->pinterestPixel = $pinterestPixel;
    }

    /**
     * @return int
     */
    public function getLinkedinPixel(): int
    {
        return $this->linkedinPixel;
    }

    /**
     * @param int $linkedinPixel
     */
    public function setLinkedinPixel(int $linkedinPixel): void
    {
        $this->linkedinPixel = $linkedinPixel;
    }

    /**
     * @return int
     */
    public function getGooglePixel(): int
    {
        return $this->googlePixel;
    }

    /**
     * @param int $googlePixel
     */
    public function setGooglePixel(int $googlePixel): void
    {
        $this->googlePixel = $googlePixel;
    }

    /**
     * @return int
     */
    public function getQuoraPixel(): int
    {
        return $this->quoraPixel;
    }

    /**
     * @param int $quoraPixel
     */
    public function setQuoraPixel(int $quoraPixel): void
    {
        $this->quoraPixel = $quoraPixel;
    }

    /**
     * @return mixed
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * @param mixed $metaTitle
     */
    public function setMetaTitle($metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return mixed
     */
    public function getMetaDesc()
    {
        return $this->metaDesc;
    }

    /**
     * @param mixed $metaDesc
     */
    public function setMetaDesc($metaDesc): void
    {
        $this->metaDesc = $metaDesc;
    }

    /**
     * @return mixed
     */
    public function getMetaImage()
    {
        return $this->metaImage;
    }

    /**
     * @param mixed $metaImage
     */
    public function setMetaImage($metaImage): void
    {
        $this->metaImage = $metaImage;
    }

    /**
     * @return mixed
     */
    public function getMetaIframe()
    {
        return $this->metaIframe;
    }

    /**
     * @param mixed $metaIframe
     */
    public function setMetaIframe($metaIframe): void
    {
        $this->metaIframe = $metaIframe;
    }

    /**
     * @return mixed
     */
    public function getTaxes()
    {
        return $this->taxes;
    }

    /**
     * @param string|null $type
     * @return mixed
     */
    public function getTaxesByType(?string $type)
    {
        if (!$this->taxes || empty($this->taxes)) return [];
        if (!$type) return $this->taxes;
        return $this->taxes->filter(
            function($tax) use ($type) {
                return $tax->getType() === $type;
            }
        );
    }

    /**
     * @param mixed $taxes
     */
    public function setTaxes($taxes): void
    {
        $this->taxes = $taxes;
    }

    public function removeTag(AffiliateShortLinkTax $tax) {
        if ($this->taxes->contains($tax)) {
            $this->taxes->removeElement($tax);
        }
    }

    private function getTaxesArray() {
        $result = [
            'tag' => [],
            'folder' => [],
            'utm_source' => [],
            'utm_medium' => [],
            'utm_campaign' => [],
            'utm_content' => [],
            'utm_keyword' => []
        ];
        if ($taxes = $this->getTaxes()) {
            /** @var AffiliateShortLinkTax $tax */
            foreach($taxes as $tax) {
                $type = $tax->getType();
                if (!isset($result[$tax->getType()])) {
                    $result[$tax->getType()] = [];
                }
                $result[$type][] = $tax->getName();
            }
        }

        return $result;
    }

    public function toArray() {
        return array_merge([
            'id' => $this->id,
            'short' => $this->short,
            'referrerLink' => [
                'id' => $this->referrerLink->getId(),
                'destinationUrl' => $this->referrerLink->getDestinationUrl()
            ],
            'customDomain' => [
                'id' => $this->customDomain->getId(),
                'name' => $this->customDomain->getName()
            ],
            'description' => $this->description,
            'facebookPixel' => $this->facebookPixel,
            'twitterPixel' => $this->twitterPixel,
            'pinterestPixel' => $this->pinterestPixel,
            'linkedinPixel' => $this->linkedinPixel,
            'googlePixel' => $this->googlePixel,
            'quoraPixel' => $this->quoraPixel,
            'createdAt' => $this->createdAt ? $this->createdAt->format('Y-m-d H:i:s') : null,
            'updatedAt' => $this->updatedAt ? $this->updatedAt->format('Y-m-d H:i:s') : null
        ], $this->getTaxesArray());
    }

}
