<?php


namespace App\Tests\Service;

use App\Entity\Cookie;
use App\Entity\CookieValidityType;
use App\Repository\CookieRepository;
use App\Service\CookieService;
use App\Exception\CookieValueIsNullException;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;


class CookieServiceTest extends TestCase
{
    private $cookieService;
    private $cookieRepositoryMock;

    public function setUp(): void
    {
        $this->cookieRepositoryMock = $this->getMockBuilder(CookieRepository::class)
            ->setMethods(['findOneByValue'])
            ->disableOriginalConstructor()
            ->getMock();
        $this->cookieService = new CookieService( $this->cookieRepositoryMock);
    }
/*
    Case IsDate
      Given :
        - a $cookie instance, and
        - a $cookieService instance
      When:
        - $cookie->getValidityType() is CookieValidityType::DATE
      Expect:
        - $cookieService->isDateValidityType($cookie) to be true


    Case IsNull
      Given :
        - a $cookie instance, and
        - a $cookieService instance
      When:
        - $cookie->getValidityType() is null
      Expect:
        - $cookieService->isDateValidityType($cookie) to be false


    Case IsNotDate
      Given :
        - a $cookie instance, and
        - a $cookieService instance
      When:
        - $cookie->getValidityType() is not null, and
        - $cookie->getValidityType() is not CookieValidityType::DATE
      Expect:
        - $cookieService->isDateValidityType($cookie) to be false
*/
    public function testIsDateValidity_IsDate( ): void
    {
        $cookie = new Cookie();
        $cookie->setValidityType(CookieValidityType::DATE);
        $result = $this->cookieService->isDateValidity($cookie);
        $this->assertTrue($result, "Cookie Validity Type is DATE.");
    }

    public function testIsDateValidity_IsNull( ): void
    {
        $cookie = new Cookie();
        $result = $this->cookieService->isDateValidity($cookie);
        $this->assertNull($cookie->getValidityType(), "unset validityType is null");
        $this->assertFalse($result, "Cookie Validity Type is NOT DATE.");
    }

    public function testIsDateValidity_IsNotDate( ): void
    {
        $cookie = new Cookie();
        $cookie->setValidityType(CookieValidityType::USAGE);
        $result = $this->cookieService->isDateValidity($cookie);
        $this->assertNotNull($cookie->getValidityType(), "unset validityType is null");
        $this->assertFalse($result, "Cookie Validity Type is NOT DATE.");
    }

    public function testFetch_ValueIsNull( ): void
    {
        $this->expectException(\TypeError::class);
        $this->cookieService->fetch(null);
    }

    public function testFetch_ValueIsUUID(): void
    {
        $uuid = Uuid::uuid4();
        $fetchedCookie = new Cookie();
        $this->cookieRepositoryMock->expects($this->exactly(1))
            ->method("findOneByValue")
            ->with($uuid)
            ->will($this->returnValue($fetchedCookie));

        $result = $this->cookieService->fetch($uuid);
        $this->assertEquals($fetchedCookie, $result);
    }

    public function testFetch_ValueIsNotUUID():void
    {
        $this->expectException(\TypeError::class);
        $result = $this->cookieService->fetch('a');
        $this->assertIsNotNull($result);
    }
}