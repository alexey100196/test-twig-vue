<?php

namespace App\Helper\ShopCompletion\Resolvers;

use App\Entity\OfferType;
use App\Helper\ShopCompletion\Resolvers\AbstractResolver;

class MainOffer extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        if ($this->shop->hasCashRewards() && $this->shop->getCpsOffer() && $this->shop->getCpsOffer()->getCommission() > 0) {
            $this->setScore(100);
        }

        if ($this->shop->hasOnlyCouponRewards()) {
            $this->setScore(100);
        }
    }
}
