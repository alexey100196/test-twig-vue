<?php

namespace App\Form\Widget;

use App\Entity\Shop;
use App\Entity\Widget\WidgetReferenceLink;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReferrerGenerateLinkType extends AbstractType
{
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => [
                    array_keys(WidgetReferenceLink::TYPE_NAMES)
                ]
            ])
            ->add('url', UrlType::class, [
                'required' => false
            ])
            ->add('referrer_email', EmailType::class, [
                'required' => false
            ])
            ->add('image', TextType::class, [
                'required' => false
            ])
            ->add('description', TextType::class, [
                'required' => false
            ])
            ->add('fingerprint', null);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($options) {
            /** @var Shop $shop */
            $shop = $options['shop'];

            $data = $event->getData();
            $form = $event->getForm();

            if (!isset($data['url']) || !$data['url']) {
                return;
            }

            $shopWebsite = parse_url($shop->getWebsite());
            $urlWebsite = parse_url($data['url']);

            if (
                isset($shopWebsite['host']) &&
                isset($urlWebsite['host']) &&
                $shopWebsite['host'] == $urlWebsite['host']
            ) {
                return;
            }

            $form->get('url')->addError(
                new FormError(
                    sprintf('Invalid URL address. Address should be under \'%s\' domain.', $shop->getWebsite())
                )
            );
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'shop' => null,
            'csrf_protection' => false,
        ]);
    }
}