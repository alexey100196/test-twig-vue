<?php

namespace App\Tests\Controller\Account\Referrer;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ReferrerControllerTest extends WebTestCase
{
    private $userPixels = [
        'facebook_pixel' => 'facebook_pixel',
        'twitter_pixel' => 'twitter_pixel',
        'pinterest_pixel' => 'pinterest_pixel',
        'linkedin_pixel' => 'linkedin_pixel',
        'google_pixel' => 'google_pixel',
        'quora_pixel' => 'quora_pixel'
    ];

    /**
     * @testdox Redirect to login if user isn't referrer
     */
    public function testRedirect()
    {
        $client = static::createClient();
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $url = $client->getKernel()->getContainer()->getParameter('url');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('GET', '/account/referrer');

        $this->assertResponseRedirects($url.'/login', Response::HTTP_FOUND);
    }

    /**
     * @testdox Redirect to login if user isn't referrer
     */
    public function testUpdatePixelsRedirect()
    {
        $client = static::createClient();
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $url = $client->getKernel()->getContainer()->getParameter('url');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/account/referrer/update-pixels', $this->userPixels);

        $this->assertResponseRedirects($url.'/login', Response::HTTP_FOUND);
    }

    /**
     * @testdox Check if user hasn't referrer rule
     */
    public function testIsNotReferrer() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', null));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('GET', '/account/referrer');

        $this->assertResponseRedirects('/logged', Response::HTTP_FOUND);
    }

    /**
     * @testdox Check if user hasn't referrer rule
     */
    public function testUpdatePixelsIsNotReferrer() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', null));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/account/referrer/update-pixels', $this->userPixels);

        $this->assertResponseRedirects('/logged', Response::HTTP_FOUND);
    }



    /**
     * @testdox Check if user hasn't referrer rule
     */
    public function testUpdatePixelsIsReferrer() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn('name', 'john'));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('POST', '/account/referrer/update-pixels', $this->userPixels);

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertEquals('"Pixels have been saved successfully."', $client->getResponse()->getContent());
    }

    /**
     * @param string $filter
     * @param $value
     * @return Cookie
     */
    private function logIn(string $filter, $value)
    {
        $session = self::$container->get('session');
        $em = self::$container->get('doctrine.orm.entity_manager');

        $user = $em->getRepository(User::class)->findOneBy([$filter => $value]);

        $firewallName = 'main';

        $token = new UsernamePasswordToken($user, null, $firewallName, $user->getRoles());
        $session->set('_security_'.$firewallName, serialize($token));
        $session->save();

        return new Cookie($session->getName(), $session->getId());
    }
}
