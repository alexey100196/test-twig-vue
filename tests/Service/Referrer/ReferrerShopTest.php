<?php


namespace App\Tests\Service\Referrer;


use App\Entity\Shop;
use App\Entity\User;
use App\Entity\UserShop;
use App\Repository\UserShopRepository;
use App\Service\Referrer\ReferrerShop;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class ReferrerShopTest extends TestCase
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    protected function setUp(): void
    {
        $this->em = $this->createMock(EntityManagerInterface::class);
    }

    public function testFindReferrerShopWithStats()
    {
        $user = new User;
        $shop = new Shop;

        $userShop = new UserShop();
        $userShop->setUser($user);
        $userShop->setShop($shop);

        $mockData = [
            'userShop' => $shop,
            'shopPurchaseProfit' => 1,
            'shopSignUpProfit' => 2,
            'shopPurchaseCount' => 3,
            'shopSignUpCount' => 2,
            'shopPurchaseTotal' => 3,
        ];

        $userShopRepository = $this
            ->getMockBuilder(UserShopRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $userShopRepository
            ->expects($this->once())
            ->method('findUserProgramsWithStats')
            ->with($user)
            ->willReturn([$mockData]);

        $referrerShop = new ReferrerShop($this->em, $userShopRepository);

        $results = $referrerShop->getReferrerShopsWithStats($user);

        $this->assertEquals($results, [$mockData]);
    }
}