function Form ($form) {
    this.$form = $form;
    this.memo = $form.serialize();
}

Form.prototype.getTarget = function () {
    return this.$form;
};

Form.prototype.addValidationError = function(fieldName, error) {
    const $label = this.$form.find('[data-target="'+fieldName+'"]');
    $label.parent().addClass('has-error');
    $label.show().text(error);
};

Form.prototype.hideValidationErrors = function () {
    this.$form.find('.validation-error').text('').hide();
    this.$form.find('.has-error').removeClass('has-error');
};

Form.prototype.changed = function () {
    return this.memo !== this.$form.serialize();
};

Form.prototype.showValidationErrors = function (errors) {
    const self = this;
    Object.keys(errors).map(function(key) {
        if (errors[key][0] !== undefined) {
            self.addValidationError(key, errors[key][0])
        } else if (typeof errors[key] === 'object') {
            Object.keys(errors[key]).map(function(subKey) {
                self.addValidationError(key + '.' + subKey, errors[key][subKey][0])
            })
        }
    });
};

Form.prototype.doSubmit = function () {
    this.hideValidationErrors();
    const params = new FormData();
    $.each(this.$form.serializeArray(), function (i, input) { params.append(input.name, input.value); });
    axios.post(this.$form.attr('action'), params)
    .then(response => {
        if (response.headers.location) window.location.href = response.headers.location;
        else location.reload()
    }).catch(error => {
        if (error.response.status == 422) {
            this.showValidationErrors(error.response.data);
        }
    });
};
