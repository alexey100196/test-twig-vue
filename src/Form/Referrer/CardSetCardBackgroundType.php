<?php


namespace App\Form\Referrer;


use App\Entity\CardSet\CardSet;
use App\Entity\CardSet\CardSetCard;
use App\Entity\CardSet\CardSetCardBackground;
use App\Entity\ReferrerLink;
use App\Form\Type\MediaType;
use PhpParser\Node\Expr\BinaryOp\GreaterOrEqual;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\NotNull;

class CardSetCardBackgroundType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('width', IntegerType::class, [
                'constraints' => [
                    new NotNull(),
                    new GreaterThan(0),
                    new LessThanOrEqual(1500),
                ]
            ])
            ->add('positionLeft', IntegerType::class, [
                'constraints' => [
                    new NotNull(),
                    new GreaterThanOrEqual(-1500),
                    new LessThanOrEqual(1500),
                ]
            ])
            ->add('positionTop', IntegerType::class, [
                'constraints' => [
                    new NotNull(),
                    new GreaterThanOrEqual(-1500),
                    new LessThanOrEqual(1500),
                ]
            ])
            ->add('media', MediaType::class, [
                'required' => false
            ])
            ->add('url');


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CardSetCardBackground::class,
            // @todo: fix it
            'csrf_protection' => false,
        ]);
    }
}