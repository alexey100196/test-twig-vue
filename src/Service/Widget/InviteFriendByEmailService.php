<?php


namespace App\Service\Widget;


use App\Adapter\MailSender;
use App\Entity\SendInviteEmail;
use App\Entity\Coupon\CouponPackage;
use App\Entity\Coupon\UserCoupon;
use App\Entity\InviteFriendByEmail;
use App\Entity\Shop;
use App\Entity\User;
use App\Entity\Widget\WidgetReferenceLink;
use App\Repository\Coupon\UserCouponRepository;
use App\Repository\RoleRepository;
use App\Repository\ShopRepository;
use App\Repository\Widget\WidgetReferenceLinkRepository;
use App\Service\Coupon\UseCoupon;
use App\Service\Coupon\GetValidCouponCode;
use App\Service\User\EmailConfirmation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Yaml\Yaml;

class InviteFriendByEmailService
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var MailSender
     */
    private $mailSender;

    /**
     * @var \Twig_Environment
     */
    private $templating;

    /**
     * @var GetValidCouponCode
     */
    private $getValidCouponCode;

    /**
     * @var UseCoupon
     */
    private $useCoupon;

    /**
     * @var ReferenceLinkService
     */
    private $generateReferenceLinkService;

    /**
     * @var WidgetReferenceLinkRepository
     */
    private $widgetReferenceLinkRepository;
    /**
     * @var UserCouponRepository
     */
    private $userCouponRepository;

    public function __construct(
        EntityManagerInterface $em,
        MailSender $mailSender,
        \Twig_Environment $templating,
        GetValidCouponCode $getValidCouponCode,
        UseCoupon $useCoupon,
        ReferenceLinkService $generateReferenceLinkService,
        WidgetReferenceLinkRepository $widgetReferenceLinkRepository,
        UserCouponRepository $userCouponRepository
    )
    {
        $this->em = $em;
        $this->mailSender = $mailSender;
        $this->templating = $templating;
        $this->getValidCouponCode = $getValidCouponCode;
        $this->useCoupon = $useCoupon;
        $this->generateReferenceLinkService = $generateReferenceLinkService;
        $this->widgetReferenceLinkRepository = $widgetReferenceLinkRepository;
        $this->userCouponRepository = $userCouponRepository;
    }

    public function __invoke(Shop $shop, User $user, string $friendEmail, string $reflinkUrl)
    {
        $this->sendEmailToFriend($friendEmail, $shop, $user, $reflinkUrl);
        if ($this->shouldSendInfoWithReward($shop, $reflinkUrl)) {
        // SEND EMAIL: Send Rewards after redirect to company page
        /* $this->sendCouponCodeToReferrer($shop, $user);*/
        } else {
            $this->sendInfoAboutRewardToReferrer($shop, $user);
        }
        $this->save($user, $shop);
    }

    /**
     * @param User $user
     * @param Shop $shop
     */
    private function save(User $user, Shop $shop) {
        $inviteFriendByEmail = new InviteFriendByEmail();
        $inviteFriendByEmail->setReferrer($user);
        $inviteFriendByEmail->setShop($shop);

        $this->em->persist($inviteFriendByEmail);
        $this->em->flush();
    }

    private function shouldSendInfoWithReward(Shop $shop, string $reflinkUrl): bool
    {
        return $shop->hasCouponRewards() &&
            $shop->getWidgetSetting()->getReferrerRewardCouponDiscountMinPurchase() <= 0 &&
            strpos($reflinkUrl, '/share') !== false;
    }

    private function sendEmailToFriend(string $friendEmail, Shop $shop, User $referrer, string $reflinkUrl)
    {
        $coupon = ($this->getValidCouponCode)($shop, CouponPackage::ALLOCATION_INVITEE);

        if (null === $coupon) {
            throw new \LogicException('No coupons available');
        }

        $this->checkIfCanInviteeFriend($shop, $referrer, $friendEmail);


        $lang = $shop->getWidgetSetting() && $shop->getWidgetSetting()->getLanguage() ? $shop->getWidgetSetting()->getLanguage()->getCode() : 'en';

        $translation = $this->getTranslations($lang);

        $subject = $referrer->getName() ? $translation['from'] . '"' . $referrer->getName() . '": ' : '';
        $subject .= $translation['check_offer'] . ' "' . $shop->getName() . ' ". ' . $translation['i_send_you_discount'];

        $this->mailSender->send($referrer->getEmail(),
            $friendEmail,
            $subject,
            $this->templating->render('email/widget/invite-friend.html.twig', [
                'shop' => $shop,
                'referrer' => $referrer,
                'link' => $reflinkUrl,
                'translation' => $translation
            ])
        );
    }

    private function checkIfCanInviteeFriend(Shop $shop, User $referrer, string $friendEmail)
    {
        if ($referrer->getEmail() === $friendEmail) {
            throw new \LogicException('You cannot invite yourself.');
        }
    }

    public function sendCouponCodeToReferrer(Shop $shop, User $referrer)
    {
        $coupon = ($this->getValidCouponCode)($shop, CouponPackage::ALLOCATION_REFERRER);
        ($this->useCoupon)($coupon, $referrer);

        $this->em->flush();

        $lang = $shop->getWidgetSetting() && $shop->getWidgetSetting()->getLanguage() ? $shop->getWidgetSetting()->getLanguage()->getCode() : 'en';
        $translation = $this->getTranslations($lang);

        $this->mailSender->send($shop->getUser()->getEmail(),
            $referrer->getEmail(),
            $this->getReferrerEmailSubject($referrer, $translation),
            $this->templating->render('email/widget/referrer-coupon-code.html.twig', [
                'coupon' => $coupon,
                'shop' => $shop,
                'referrer' => $referrer,
                'translation' => $translation
            ])
        );
    }

    private function sendInfoAboutRewardToReferrer(Shop $shop, User $referrer)
    {
        $lang = $shop->getWidgetSetting() && $shop->getWidgetSetting()->getLanguage() ? $shop->getWidgetSetting()->getLanguage()->getCode() : 'en';
        $translation = $this->getTranslations($lang);

        $this->mailSender->send($shop->getUser()->getEmail(),
            $referrer->getEmail(),
            $this->getReferrerEmailSubject($referrer, $translation),
            $this->templating->render('email/widget/referrer-reward-info.html.twig', [
                'shop' => $shop,
                'referrer' => $referrer,
                'translation' => $translation,
            ])
        );
    }

    private function getReferrerEmailSubject(User $user, array $translations): string
    {
        return sprintf('%s %s', $translations['thank_you_for_recommendation'], $user->getName());
    }

    private function getTranslations(string $lang)
    {
        return Yaml::parseFile(__DIR__ . '/../../../translations/widget.' . $lang . '.yaml');
    }
}