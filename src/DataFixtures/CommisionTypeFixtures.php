<?php

namespace App\DataFixtures;

use App\DataFixtures\BaseGlossaryFixtures;
use App\Entity\CommissionType;

class CommisionTypeFixtures extends BaseGlossaryFixtures
{
    public function getData(): array
    {
        return [
            1 => CommissionType::PERCENT,
            2 => CommissionType::FIXED,
        ];
    }

    public function getEntity()
    {
        return new CommissionType();
    }
}
