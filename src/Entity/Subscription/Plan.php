<?php

namespace App\Entity\Subscription;

use App\Entity\Currency;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Subscription\PlanRepository")
 */
class Plan
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="decimal", precision=20, scale=2)
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(nullable=false)
     */
    private $currency;

    /**
     * @ORM\Column(type="smallint")
     */
    private $trialPeriod;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subscription\PlanInterval")
     * @ORM\JoinColumn(nullable=false)
     */
    private $trialInterval;

    /**
     * @ORM\Column(type="smallint")
     */
    private $planPeriod;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subscription\PlanInterval")
     * @ORM\JoinColumn(nullable=false)
     */
    private $planInterval;

    /**
     * @ORM\Column(type="smallint")
     */
    private $sortOrder;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Subscription\PlanType", inversedBy="plans")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getTrialPeriod(): ?int
    {
        return $this->trialPeriod;
    }

    public function setTrialPeriod(int $trialPeriod): self
    {
        $this->trialPeriod = $trialPeriod;

        return $this;
    }

    public function getTrialInterval(): ?PlanInterval
    {
        return $this->trialInterval;
    }

    public function setTrialInterval(?PlanInterval $trialInterval): self
    {
        $this->trialInterval = $trialInterval;

        return $this;
    }

    public function getPlanPeriod(): ?int
    {
        return $this->planPeriod;
    }

    public function setPlanPeriod(int $planPeriod): self
    {
        $this->planPeriod = $planPeriod;

        return $this;
    }

    public function getPlanInterval(): ?PlanInterval
    {
        return $this->planInterval;
    }

    public function setPlanInterval(?PlanInterval $planInterval): self
    {
        $this->planInterval = $planInterval;

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    public function getTotalPeriodInDays(): int
    {
        return $this->getPlanPeriod() * $this->getPlanInterval()->getDaysCount();
    }

    public function getMonthlyPrice()
    {
        $days = $this->getTotalPeriodInDays();
        $months = (int) ($days / 30);

        return round($this->getPrice() / $months, 2);
    }

    public function getType(): ?PlanType
    {
        return $this->type;
    }

    public function setType(?PlanType $type): self
    {
        $this->type = $type;

        return $this;
    }
}
