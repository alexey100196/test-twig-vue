<?php

namespace App\Manager;

use App\Entity\Product;
use App\Entity\Shop;
use App\Manager\AbstractManager;

class ShopManager extends AbstractManager
{
    /**
     * Add product to shop.
     * @param Shop $shop
     * @return Shop
     */
    public function save(Shop $shop): Shop
    {
        $this->entityManager->persist($shop);
        $this->entityManager->flush();

        return $shop;
    }
}
