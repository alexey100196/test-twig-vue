<?php

declare(strict_types=1);

namespace App\Security\Voter;

use App\Entity\User;
use App\Service\Referrer\ReferrerCompletion;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ReferrerVoter extends Voter
{
    const FULL_PROFILE = 'FULL_PROFILE';

    /**
     * @var ReferrerCompletion
     */
    protected $referrerCompletion;

    /**
     * ReferrerVoter constructor.
     * @param ReferrerCompletion $referrerCompletion
     */
    public function __construct(ReferrerCompletion $referrerCompletion)
    {
        $this->referrerCompletion = $referrerCompletion;
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @return bool
     */
    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [self::FULL_PROFILE]);
    }

    /**
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // Check if user is logged and is referrer.
        if (!$user instanceof User && $user->isReferrer()) {
            return false;
        }

        if ($attribute === self::FULL_PROFILE) {
            return $this->hasFullProfile($user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * Check if referrer profile is complete.
     *
     * @param User $user
     * @return bool
     */
    private function hasFullProfile(User $user)
    {
        return $this->referrerCompletion->isReferrerFullyCompleted($user);
    }
}