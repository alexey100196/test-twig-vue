<?php


namespace App\Controller\Admin\Report;

use App\Entity\Shop;
use App\Helper\DatePeriodGenerator;
use App\Service\Admin\Reports\ReferrerPayoutsService;
use App\Traits\Controller\HasCsvResponse;
use App\ValueObject\DatePeriod;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 */
class ReferrerPayoutController extends AbstractController
{
    use HasCsvResponse;

    /**
     * @var ReferrerPayoutsService
     */
    private $referrerPayoutsService;

    /**
     * ReferrerPayoutController constructor.
     * @param ReferrerPayoutsService $referrerPayoutsService
     */
    public function __construct(ReferrerPayoutsService $referrerPayoutsService)
    {
        $this->referrerPayoutsService = $referrerPayoutsService;
    }

    /**
     * @Route("/admin/report/{shop}/referrer-payout/last-month", name="admin_report_referrer_payout_last_month_index")
     * @param int $shop
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function lastMonthIndex(int $shop)
    {
        $lastMonth = new Carbon('first day of last month');

        return $this->redirectToRoute('admin_report_referrer_payout_index', [
            'shop' => $shop,
            'year' => $lastMonth->year,
            'month' => $lastMonth->month,
        ]);
    }

    /**
     * @Route("/admin/report/referrer-payout/last-month", name="admin_report_referrer_payout_last_month_index_all")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function lastMonthIndexAll()
    {
        $lastMonth = new Carbon('first day of last month');

        return $this->redirectToRoute('admin_report_referrer_payout_index_all', [
            'year' => $lastMonth->year,
            'month' => $lastMonth->month,
        ]);
    }

    /**
     * @Route("/admin/report/referrer-payout/{year}/{month}", name="admin_report_referrer_payout_index_all")
     * @param int $year
     * @param int $month
     * @return Response
     */
    public function indexAll(int $year, int $month)
    {
        $datePeriod = DatePeriodGenerator::generateFromYearAndMonth($year, $month);
        $report = $this->referrerPayoutsService->getReferrersPayouts($datePeriod->getStart(), $datePeriod->getEnd());

        $fileName = sprintf('payout-%s-%s.csv', $year, $month);

        return $this->csvResponse($this->formatShopPayoutReportsToPaymentRails($report), [], $fileName);
    }

    /**
     * @Route("/admin/report/{shop}/referrer-payout/{year}/{month}", name="admin_report_referrer_payout_index")
     * @param Shop $shop
     * @param int $year
     * @param int $month
     * @return Response
     */
    public function index(Shop $shop, int $year, int $month)
    {
        $datePeriod = DatePeriodGenerator::generateFromYearAndMonth($year, $month);
        $report = $this->referrerPayoutsService->getShopReferrersPayouts($shop, $datePeriod->getStart(), $datePeriod->getEnd());
        $fileName = sprintf('payout-%s-%s-%s.csv', $shop->getId(), $year, $month);

        return $this->csvResponse($this->formatShopPayoutReportsToPaymentRails($report), [], $fileName);
    }

    /**
     * @param ArrayCollection $report
     * @return array
     */
    private function formatShopPayoutReportsToPaymentRails(ArrayCollection $report)
    {
        // @todo: format to PR
        $data = [];

        foreach ($report as $item) {
            $data[] = [
                'Email' => $item->getReferrerEmail(),
                'Send Amount' => $item->getAmount(),
                'Send Currency' => $item->getCurrency()
            ];
        }

        return $data;
    }
}
