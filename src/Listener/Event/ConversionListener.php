<?php


namespace App\Listener\Event;


use App\Entity\Currency;
use App\Entity\ShopConversion;
use App\Entity\TransactionType;
use App\Event\Conversion\ConversionEventInterface;
use App\Event\Shop\ShopBalanceDecreased;
use App\Service\Balance\BalanceServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @todo: DRY
 */
class ConversionListener
{
    private $balanceService;
    private $em;
    private $eventDispatcher;

    public function __construct(
        BalanceServiceInterface $balanceService,
        EntityManagerInterface $em,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->balanceService = $balanceService;
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function onConversionCreated(ConversionEventInterface $event)
    {
        /** @var ShopConversion $conversion */
        $conversion = $event->getConversion();

        $referrer = $conversion->getReferrer();
        $shopOwner = $conversion->getShop()->getUser();

        // This is referrer profit. This amount should be:
        // 1. Subtracted from shop balance.
        // 2. Added to referrer balance.
        $referrerProfit = $conversion->getReferrerProfit();

        // This is system profit. This amount should be:
        // 1. Subtracted from shop balance.
        $systemCommission = $conversion->getSystemCommission();

        /** @var Currency $eurCurrency */
        $eurCurrency = $this->em->getReference(Currency::class, Currency::EUR_ID);

        /** @var TransactionType $referrerProfitType */
        $referrerProfitType = $this->em->getReference(TransactionType::class, TransactionType::SHOP_CONVERSION_REFERRER_PROFIT);

        /** @var TransactionType $referrerProfitType */
        $systemCommissionType = $this->em->getReference(TransactionType::class, TransactionType::SHOP_CONVERSION_SYSTEM_COMMISSION);

        $transactions = [
            [Uuid::uuid4(), $shopOwner, $eurCurrency, -1 * $referrerProfit, $referrerProfitType],
            [Uuid::uuid4(), $shopOwner, $eurCurrency, -1 * $systemCommission, $systemCommissionType],
            [Uuid::uuid4(), $referrer, $eurCurrency, $referrerProfit, $referrerProfitType]
        ];

        // @todo: transaction

        foreach ($transactions as $transaction) {
            $this->balanceService->addTransaction(...$transaction);
        }

        $this->eventDispatcher->dispatch(
            ShopBalanceDecreased::NAME,
            new ShopBalanceDecreased($conversion->getShop(), $eurCurrency)
        );
    }

    public function onConversionRejected(ConversionEventInterface $event)
    {
        /** @var ShopConversion $conversion */
        $conversion = $event->getConversion();

        $referrer = $conversion->getReferrer();
        $shopOwner = $conversion->getShop()->getUser();

        // Referrer profit. This amount should be:
        // 1. Subtracted from referrer profit.
        // 2. Returned to shop.
        $referrerProfit = $conversion->getReferrerProfit();

        // This is system profit. This amount should be:
        // 1. Returned to shop balance.
        $systemCommission = $conversion->getSystemCommission();

        /** @var Currency $eurCurrency */
        $eurCurrency = $this->em->getReference(Currency::class, Currency::EUR_ID);

        /** @var TransactionType $referrerProfitType */
        $referrerProfitType = $this->em->getReference(TransactionType::class, TransactionType::SHOP_CONVERSION_REFERRER_PROFIT_RETURN);

        /** @var TransactionType $referrerProfitType */
        $systemCommissionType = $this->em->getReference(TransactionType::class, TransactionType::SHOP_CONVERSION_SYSTEM_COMMISSION_RETURN);

        $transactions = [
            [Uuid::uuid4(), $shopOwner, $eurCurrency, $referrerProfit, $referrerProfitType],
            [Uuid::uuid4(), $shopOwner, $eurCurrency, $systemCommission, $systemCommissionType],
            [Uuid::uuid4(), $referrer, $eurCurrency, -1 * $referrerProfit, $referrerProfitType]
        ];

        // @todo: transaction

        foreach ($transactions as $transaction) {
            $this->balanceService->addTransaction(...$transaction);
        }
    }

    public function onShopConversionAcceptedAfterRejection(ConversionEventInterface $event)
    {
        // We need to run again the same transaction as with conversion created.
        $this->onConversionCreated($event);
    }
}
