<?php

namespace App\Tests\Controller\Account\Referrer;

use App\Entity\Shop;
use App\Entity\ShopConversion;
use App\Entity\ShopInteraction;
use App\Entity\ShopPurchase;
use App\Entity\ShopSignUp;
use App\Entity\ShopView;
use App\Entity\User;
use App\Entity\UserShop;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ReferrerShopLinkControllerTest extends WebTestCase
{
    /**
     * @var string
     */
    private $shopName = 'Demo';

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var Shop
     */
    protected $shop;

    /**
     * @var User
     */
    protected $referrer;

    /**
     * @var User
     */
    protected $notReferrer;

    /**
     * @var User
     */
    protected $anotherReferrer;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $schemaTool = new SchemaTool($this->em);

        $classes = [
            $this->em->getClassMetadata(UserShop::class),
            $this->em->getClassMetadata(ShopPurchase::class),
            $this->em->getClassMetadata(ShopSignUp::class),
            $this->em->getClassMetadata(ShopView::class),
            $this->em->getClassMetadata(ShopConversion::class),
            $this->em->getClassMetadata(ShopInteraction::class),
        ];

        try {
            $schemaTool->dropSchema($classes);
            $schemaTool->createSchema($classes);
        } catch (\Exception $e) {

        }

        $this->shop = $this->em->getRepository(Shop::class)->findOneBy(['name' => $this->shopName]);
        $this->notReferrer = $this->em->getRepository(User::class)->findOneBy(['name' => null]);
        $this->referrer = $this->em->getRepository(User::class)->findOneBy(['nickname' => 'john']);
        $this->anotherReferrer = $this->em->getRepository(User::class)->findOneBy(['nickname' => 'kate']);

        $this->em->persist((new UserShop())->setShop($this->shop)->setUser($this->referrer));

        $this->em->flush();
    }

    /**
     * @testdox Redirect to login if user isn't logged in
     */
    public function testHomeNotLoggedUser()
    {
        $client = static::createClient();
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $url = $client->getKernel()->getContainer()->getParameter('url');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('GET', '/account/referrer/company/'.$this->shop->getId());

        $this->assertResponseRedirects($url.'/login', Response::HTTP_FOUND);
    }

    /**
     * @testdox Check if user hasn't referrer rule
     */
    public function testIsNotReferrer() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn($this->notReferrer));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('GET', '/account/referrer/company/'.$this->shop->getId());

        $this->assertResponseRedirects('/logged', Response::HTTP_FOUND);
    }

    /**
     * @testdox Check if user hasn't relationship with current shop
     */
    public function testIsAnotherReferrer() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn($this->anotherReferrer));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $client->request('GET', '/account/referrer/company/'.$this->shop->getId());

        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    /**
     * @testdox Check if user is referrer and has relationship with current shop
     */
    public function testIsReferrer() {
        $client = static::createClient();
        $client->getCookieJar()->set($this->logIn($this->referrer));
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);
        $crawler = $client->request('GET', '/account/referrer/company/'.$this->shop->getId());

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertSame($this->shopName, $crawler->filter('.page-title h3')->text());
    }

    /**
     * @param User $user
     * @param $value
     * @return Cookie
     */
    private function logIn(User $user)
    {
        $session = self::$container->get('session');

        $firewallName = 'main';

        $token = new UsernamePasswordToken($user, null, $firewallName, $user->getRoles());
        $session->set('_security_'.$firewallName, serialize($token));
        $session->save();

        return new Cookie($session->getName(), $session->getId());
    }
}
