<?php


namespace App\Controller\Ajax;


use App\Controller\AjaxAbstractController;
use App\Entity\Media;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\User;
use App\Form\Referrer\ReferrerLinkStoreCardType;
use App\Service\MediaUploader;
use App\Service\Referrer\ReferrerLinkStoreCardService;
use App\Service\UrlFileUploader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerShopCardController
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerLinkStoreCardController extends AjaxAbstractController
{
    /** @var ReferrerLinkStoreCardService */
    private $referrerLinkStoreCard;

    public function __construct(ReferrerLinkStoreCardService $referrerLinkStoreCard)
    {
        $this->referrerLinkStoreCard = $referrerLinkStoreCard;
    }

    /**
     * @Route(
     *     "/ajax/referrer-link/{referrerLink}/store-card",
     *     name="ajax_referrer_link_store_card_update",
     *     methods={"POST"}
     * )
     * @param ReferrerLink $referrerLink
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function update(ReferrerLink $referrerLink, Request $request)
    {
        if (!$this->getUser()->hasReferrerLink($referrerLink)) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(ReferrerLinkStoreCardType::class, $card = $referrerLink->getStoreCardOrCreate());
        $form->submit(array_merge($request->request->all(), $request->files->all()));

        if ($form->isValid()) {
            $this->referrerLinkStoreCard->save($card);
        } else {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(['message' => 'Card saved']);
    }

    /**
     * @Route(
     *     "/ajax/referrer-link/{referrerLink}/store-card",
     *     name="ajax_referrer_link_store_card_show",
     *     methods={"GET"}
     * )
     * @param ReferrerLink $referrerLink
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(ReferrerLink $referrerLink, Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$this->getUser()->hasReferrerLink($referrerLink)) {
            throw new NotFoundHttpException();
        }

        $card = $referrerLink->getStoreCardOrCreate();

        $coupon = [];

        $referrerCoupon = $user->getReferrerCouponByShop($referrerLink->getShop());
        if ($referrerCoupon && $referrerCoupon->getMyStoreEnabledAndNotExpired()) {
            $coupon = [
                'code' => $referrerCoupon->getCode(),
                'discount' => $referrerCoupon->getValue(),
                'discount_type' => (string)$referrerCoupon->getValueType()
            ];
        }

        return $this->jsonResponse([
            'image_position' => $card->getImagePosition()->toArray(),
            'title' => $card->getTitle(),
            'description' => $card->getDescription(),
            'image' => ['file_name' => $card->getImage() ? $card->getImage()->getFileName() : null],
            'image_width' => $card->getImageWidth(),
            'image_height' => $card->getImageHeight(),
            'destination_url' => (string)$referrerLink->getDestinationUrl(),
            'coupon_code' => $referrerCoupon && $referrerCoupon->getMyStoreEnabledAndNotExpired() ? $referrerCoupon->getCode() : null,
            'coupon' => $coupon,
        ]);
    }
}