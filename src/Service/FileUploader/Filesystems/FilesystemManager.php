<?php

namespace App\Service\FileUploader\Filesystems;

use App\Service\FileUploader\Filesystems\FilesystemInterface;

/**
 *
 */
class FilesystemManager
{
    /**
     * All filesystems.
     * @var array
     */
    private $filesystems = [];

    /**
     * @param array $filesystems filesystems configuration
     */
    public function __construct($filesystems)
    {
        foreach ($filesystems as $name => $config) {
            $handler = new $config['handler'];
            $handler->setPath($config['directory']);
            $this->filesystems[$name] = $handler;
        }
    }

    public function get(string $name): FilesystemInterface
    {
        return $this->filesystems[$name];
    }
}
