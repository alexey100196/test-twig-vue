<?php


namespace App\Service\ShortLinks;


use App\Entity\ShortLinks\AffiliateShortLink;
use App\Entity\ShortLinks\AffiliateShortLinkTax;
use Doctrine\ORM\EntityManagerInterface;

class AffiliateShortLinkService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param AffiliateShortLink $affiliateShortLink
     * @param array|null $tag
     * @param array|null $folder
     * @param array|null $utmSource
     * @param array|null $utmMedium
     * @param array|null $utmCampaign
     * @param array|null $utmContent
     * @param array|null $utmKeyword
     * @return AffiliateShortLink
     */
    public function save(AffiliateShortLink $affiliateShortLink, ?array $tag, ?array $folder, ?array $utmSource, ?array $utmMedium, ?array $utmCampaign, ?array $utmContent, ?array $utmKeyword)
    {
        $tagArr = $this->loadNewArray('tag', $affiliateShortLink, $tag);
        $folderArr = $this->loadNewArray('folder', $affiliateShortLink, $folder);
        $utmSourceArr = $this->loadNewArray('utm_source', $affiliateShortLink, $utmSource);
        $utmMediumArr = $this->loadNewArray('utm_medium', $affiliateShortLink, $utmMedium);
        $utmCampaignArr = $this->loadNewArray('utm_campaign', $affiliateShortLink, $utmCampaign);
        $utmContentArr = $this->loadNewArray('utm_content', $affiliateShortLink, $utmContent);
        $utmKeywordArr = $this->loadNewArray('utm_keyword', $affiliateShortLink, $utmKeyword);
        $affiliateShortLink->setTaxes(array_merge($tagArr, $folderArr, $utmSourceArr, $utmMediumArr, $utmCampaignArr, $utmContentArr, $utmKeywordArr));
        $this->em->persist($affiliateShortLink);
        $this->em->flush();

        return $affiliateShortLink;
    }

    /**
     * @param AffiliateShortLink $affiliateShortLink
     */
    public function remove(AffiliateShortLink $affiliateShortLink)
    {
        $this->em->remove($affiliateShortLink);
        $this->em->flush();
    }

    /**
     * @param string $type
     * @param AffiliateShortLink $link
     * @param array|null $newArr
     * @return array
     */
    private function loadNewArray(string $type, AffiliateShortLink $link, ?array $newArr) {
        $currentNames = [];
        $oldArr = $link->getTaxesByType($type);
        $res = [];
        if ($oldArr) {
            $nOldArr = $oldArr->toArray();
            $i = 0;
            foreach($nOldArr as $tag) {
                if ($newArr && isset($newArr[$i])) {
                    $newItem = $tag;
                    $newItem->setName($newArr[$i]);
                    if (!in_array(strtolower($newArr[$i]), $currentNames)) {
                        $currentNames[] = strtolower($newArr[$i]);
                        $res[] = $newItem;
                    }
                } else {
                    $link->removeTag($tag);
                    $this->em->remove($tag);
                }
                $i++;
            }

            if ($newArr && count($res) < count($newArr)) {
                for($i=count($res); $i<count($newArr); $i++) {
                    $newItem = new AffiliateShortLinkTax();
                    $newItem->setLink($link);
                    $newItem->setType($type);
                    $newItem->setName($newArr[$i]);
                    if (!in_array(strtolower($newArr[$i]), $currentNames)) {
                        $currentNames[] = strtolower($newArr[$i]);
                        $res[] = $newItem;
                    }
                }
            }
        } else if($newArr) {
            foreach($newArr as $tag) {
                $newItem = new AffiliateShortLinkTax();
                $newItem->setLink($link);
                $newItem->setType($type);
                $newItem->setName($tag);
                if (!in_array(strtolower($tag), $currentNames)) {
                    $currentNames[] = strtolower($tag);
                    $res[] = $newItem;
                }
            }
        }

        return $res;
    }
}