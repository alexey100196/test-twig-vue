<?php


namespace App\Form\Shop\Widget\FormData;


use App\Entity\Coupon\CouponDiscountType;
use App\Entity\Shop;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CouponPackage
{
    /**
     * @var bool|null
     */
    private $multipleUsage;

    /**
     * @var CouponDiscountType|null
     */
    private $discountType;

    /**
     * @var float|null
     */
    private $discount;

    /**
     * @var UploadedFile|null
     */
    private $csv;

    /**
     * @var Shop|null
     */
    private $shop;

    /**
     * @var int|null
     */
    private $allocation;

    /**
     * @return bool|null
     */
    public function getMultipleUsage(): ?bool
    {
        return $this->multipleUsage;
    }

    /**
     * @param bool|null $multipleUsage
     */
    public function setMultipleUsage(?bool $multipleUsage): void
    {
        $this->multipleUsage = $multipleUsage;
    }

    /**
     * @return CouponDiscountType|null
     */
    public function getDiscountType(): ?CouponDiscountType
    {
        return $this->discountType;
    }

    /**
     * @param CouponDiscountType|null $discountType
     */
    public function setDiscountType(?CouponDiscountType $discountType): void
    {
        $this->discountType = $discountType;
    }

    /**
     * @return float|null
     */
    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    /**
     * @param float|null $discount
     */
    public function setDiscount(?float $discount): void
    {
        $this->discount = $discount;
    }

    /**
     * @return UploadedFile|null
     */
    public function getCsv(): ?UploadedFile
    {
        return $this->csv;
    }

    /**
     * @param UploadedFile|null $csv
     */
    public function setCsv(?UploadedFile $csv): void
    {
        $this->csv = $csv;
    }

    /**
     * @return Shop|null
     */
    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     */
    public function setShop(?Shop $shop): void
    {
        $this->shop = $shop;
    }

    /**
     * @return int|null
     */
    public function getAllocation(): ?int
    {
        return $this->allocation;
    }

    /**
     * @param int|null $allocation
     */
    public function setAllocation(?int $allocation): void
    {
        $this->allocation = $allocation;
    }
}