<?php


namespace App\Tests\Service\Referrer;


use App\Entity\User;
use App\Service\Referrer\ReferrerCompletion;
use PHPUnit\Framework\TestCase;

class ReferrerCompletionTest extends TestCase
{
    public function setUp(): void
    {
        $this->service = new ReferrerCompletion();
    }

    public function testIfUserWithNicknameIsCompleted()
    {
        $user = new User();
        $user->setNickname('anyNickName');

        $result = $this->service->isReferrerFullyCompleted($user);

        $this->assertTrue($result);
    }

    public function testIfUserWithoutNicknameIsCompleted()
    {
        $result = $this->service->isReferrerFullyCompleted(new User());

        $this->assertFalse($result);
    }
}