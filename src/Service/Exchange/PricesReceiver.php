<?php


namespace App\Service\Exchange;


interface PricesReceiver
{
    public function get(string $currency);
}