<?php


namespace App\Controller\Api\V1;


use App\Controller\Api\ApiController;
use App\Entity\EventEntry;
use App\Entity\EventEntryType;
use App\Entity\Shop;
use App\Entity\User;
use App\Form\ShopEvent\PurchaseType;
use App\Form\ShopEvent\SignUpType;
use App\Form\ShopEvent\TestConnectionType;
use App\Form\ShopEvent\ViewType;
use App\Interfaces\Controller\OriginAuthenticatedController;
use App\Repository\EventEntryTypeRepository;
use App\Service\ShopEvent\EventService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EventController extends ApiController implements OriginAuthenticatedController
{
    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @var EventEntryTypeRepository
     */
    private $eventEntryTypeRepository;

    /**
     * EventController constructor.
     * @param EventService $eventService
     * @param EventEntryTypeRepository $eventEntryTypeRepository
     */
    public function __construct(EventService $eventService, EventEntryTypeRepository $eventEntryTypeRepository)
    {
        $this->eventService = $eventService;
        $this->eventEntryTypeRepository = $eventEntryTypeRepository;
    }

    /**
     * @Route("/api/v1/event/view", name="api_v1_event_store_view")
     * @param Request $request
     * @return JsonResponse
     */
    public function storeView(Request $request)
    {
        return $this->processEventEntryForm($request, ViewType::class);
    }

    /**
     * @Route("/api/v1/event/test-connection", name="api_v1_event_store_test_connection")
     * @param Request $request
     * @return JsonResponse
     */
    public function storeTestConnection(Request $request)
    {
        return $this->processEventEntryForm($request, TestConnectionType::class);
    }

    /**
     * @Route("/api/v1/event/signup", name="api_v1_event_store_sign_up")
     * @param Request $request
     * @return JsonResponse
     */
    public function storeSignUp(Request $request)
    {
        return $this->processEventEntryForm($request, SignUpType::class);
    }

    /**
     * @Route("/api/v1/event/purchase", name="api_v1_event_store_purchase")
     * @param Request $request
     * @return JsonResponse
     */
    public function storePurchase(Request $request)
    {
        return $this->processEventEntryForm($request, PurchaseType::class);
    }

    /**
     * @param Request $request
     * @param string $formClass
     * @return JsonResponse
     */
    private function processEventEntryForm(Request $request, string $formClass)
    {
        $form = $this->createForm($formClass, $eventEntry = new EventEntry());
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            $errorMessages = $this->getFormErrorMessages($form);

            // If data not valid and and test mode enabled then process.
            if ($eventEntry->getShop() && $eventEntry->getTest() === true) {
                $eventEntry->setErrors($errorMessages);
                $this->eventService->process($eventEntry);
            }

            return $this->validationErrorResponse($errorMessages);
        }

        $this->eventService->process($eventEntry);

        return $this->makeResponse(['message' => 'OK'], Response::HTTP_CREATED);
    }
}