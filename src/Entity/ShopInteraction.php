<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\InheritanceType("JOINED")
 * @ORM\Entity(repositoryClass="App\Repository\ShopInteractionRepository")
 */
abstract class ShopInteraction
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ReferrerLink")
     */
    protected $referrerLink;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $referrer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EventEntry", cascade={"persist"})
     */
    private $eventEntry;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Shop|null
     */
    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    /**
     * @param Shop|null $shop
     * @return ShopInteraction
     */
    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * @return ReferrerLink|null
     */
    public function getReferrerLink(): ?ReferrerLink
    {
        return $this->referrerLink;
    }

    /**
     * @param ReferrerLink|null $referrerLink
     * @return ShopInteraction
     */
    public function setReferrerLink(?ReferrerLink $referrerLink)
    {
        $this->referrerLink = $referrerLink;

        return $this;
    }

    public function getReferrer(): ?User
    {
        return $this->referrer;
    }

    public function setReferrer(?User $referrer): self
    {
        $this->referrer = $referrer;

        return $this;
    }

    public function getEventEntry(): ?EventEntry
    {
        return $this->eventEntry;
    }

    public function setEventEntry(?EventEntry $eventEntry): self
    {
        $this->eventEntry = $eventEntry;

        return $this;
    }
}
