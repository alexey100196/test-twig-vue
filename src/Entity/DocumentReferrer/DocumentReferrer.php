<?php

namespace App\Entity\DocumentReferrer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DocumentReferrer\DocumentReferrerRepository")
 */
class DocumentReferrer
{
    const OTHERS_ID = 1;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $referrer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DocumentReferrer\DocumentReferrer", inversedBy="children", cascade={"persist"})
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DocumentReferrer\DocumentReferrer", mappedBy="parent", cascade={"persist"})
     */
    private $children;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function __construct(string $referrer)
    {
        $this->children = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->referrer = $referrer;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReferrer(): ?string
    {
        return $this->referrer;
    }

    public function setReferrer(string $referrer): self
    {
        $this->referrer = $referrer;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function createChildFromLink(string $link): ?DocumentReferrer
    {
        if ($this->getReferrer() !== $link) {
            $this->addChild($child = new static($link));

            return $child;
        }

        return null;
    }
}
