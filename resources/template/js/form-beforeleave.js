function FormBeforeLeave(form, targetSelector) {
    this.form = form;
    this.$target = $(targetSelector);
    this.leavingUrl = '';
    this.leavingElements = [];
};

FormBeforeLeave.prototype.attachTo = function(target) {
    this.leavingElements.push(target)
};

FormBeforeLeave.prototype.rememberLeavingUrl = function (url) {
    this.leavingUrl = url;
};

FormBeforeLeave.prototype.init = function () {
    const self = this;
    this.leavingElements.forEach(function (leaver) {
        leaver.click(function (e) {
            if (self.form.changed()) {
                e.preventDefault();
                self.rememberLeavingUrl(this.getAttribute('href'));
                self.showAlert();
            }
        })
    })
};

FormBeforeLeave.prototype.showAlert = function () {
    const template = `
    <div class="alert alert-warning beforeleave">
        <strong>Warning!</strong> Your form has been changed. Are you sure you want to leave without saving?
        <div class="row">
            <div class="col-md-12" style="margin-top: 10px;">
                <button class="btn btn-primary save-changes">Save</button>
                <button class="btn btn-secondary leave-changes">Leave without saving</button>
            </div>
        </div>
    </div>`;

    if (this.$target.has('.beforeleave').length === 0) {
        this.$target.prepend(template);
        $('.save-changes').click(this.onSaveButtonClick.bind(this));
        $('.leave-changes').click(this.onLeaveButtonClick.bind(this));
    }
};

FormBeforeLeave.prototype.onSaveButtonClick = function () {
    this.form.getTarget().submit();
};

FormBeforeLeave.prototype.onLeaveButtonClick = function () {
    if (this.leavingUrl) {
        window.location.href = this.leavingUrl;
    }
};