function ReferrerCampaignIncome() {
    this.totalCps = 0;
    this.totalCpl = 0;
    this.totalReferrerProfit = 0;
    this.totalSIProfit = 0;
    this.shops = [];
}

function getFilterByFromLS() {
    let filterBy = '';
    if (window.localStorage.getItem('referrerIncomeFilter')) {
        filterBy = '&filterBy='+window.localStorage.getItem('referrerIncomeFilter');
    }

    return filterBy;
}

var proto = ReferrerCampaignIncome.prototype;

proto.resetGlobalStats = function () {
    this.totalCps = 0;
    this.totalCpl = 0;
    this.totalReferrerProfit = 0;
    this.totalSIProfit = 0;
}

proto.addShop = function (shopId) {
    this.shops.push(shopId);
};

proto.renderShopsList = function (startDate, endDate) {
    const shopGetParam = _.reduce(this.shops, function(memo, shop){
        return memo + '&shops[]=' + shop;
     }, '');

    this.resetGlobalStats();

    axios.get('/ajax/referrer/income/companies?start_at=' + startDate + '&end_at=' + endDate + getFilterByFromLS() + shopGetParam , {})
        .then(response => {

            var $tbody = $('#stats-table').find('.rTableBody');

            $tbody.html('');
            $tbody.parent().parent().find('.alert-info').remove();
            $tbody.parent().show();

            if (response.data.length) {
                $tbody.find('.loading-row').hide();
            } else {
                $tbody.parent().after('<div role="alert" class="alert alert-info">\n' +
                    '                    There are no statistics for this period.\n' +
                    '                </div>');
                $tbody.parent().hide();
            }

            _.map(response.data, (function (item) {
                item.profit = parseFloat(item.purchase_referrer_profit) + parseFloat(item.sign_up_referrer_profit);

                this.totalCps += parseFloat(item.total_purchase_value_generated);
                this.totalCpl += parseFloat(item.total_sign_ups_value_generated);
                this.totalReferrerProfit += parseFloat(item.profit);

                let actionBtn = '<i class="fa fa-times-circle"></i>';
                if (item.total_purchase_value_generated || item.total_sign_ups_value_generated || item.total_number_of_views) {
                    actionBtn = `<div class="table-link actions btn btn-third" data-closed_text="Check<br/><span></span>details" onclick="proto.renderSingleShopStats(event, ${item.shop.id}, '${startDate}', '${endDate}')">Check<br/><span></span>details</div>`;
                }

                const tmpl = `
                    <div class="rTableRow" data-id="shop-${item.shop.id}">
                        <div class="rTableCell company_name width-24"><span class="mobile_table_header hide">Company Name: </span>${item.shop.name}</div>
                        <div class="rTableCell"></div>
                        <div class="rTableCell"></div>
                        <div class="rTableCell"><span class="mobile_table_header hide">Total Purchase value: </span>€${item.total_purchase_value_generated}</div>
                        <div class="rTableCell"><span class="mobile_table_header hide">Total Click Count: </span>${item.total_number_of_views}</div>
                        <div class="rTableCell"><span class="mobile_table_header hide">Total Income: </span>€${item.profit}</div>
                        <div class="rTableCell text-right">${actionBtn}</div>
                    </div>
                `;

                $tbody.append(tmpl);
            }).bind(this));

            $('.total-cps').html(this.totalCps);
            $('.total-cpl').html(this.totalCpl);
            $('.total-referrer-profit').html(this.totalReferrerProfit);
            $('.total-si-profit').html(this.totalSIProfit);

        }).catch(error => {

    });
};

proto.renderSingleShopStats = function (event, shopId, startDate, endDate) {

    var $actionButton = $(event.currentTarget);
    var $childItems = $('[data-shop=' + shopId + ']');

    $('div.exchange-rates').remove();

    if ($actionButton.hasClass('active')) {
        $actionButton.toggleClass('active').toggleClass('btn-third').toggleClass('btn-danger').html('Show<br/><span></span>details');
        $childItems.remove();
        return
    }

    $(event.target).parents('.rTableRow').siblings('.table-lvl1-content').remove();
    $(event.target).parents('.rTableRow').siblings('.rTableRow').each(function(){
        var $this = $(this).find('.table-link.active');

        if($this.length){
            $this.removeClass('active btn-danger').addClass('btn-third');
            $this.html($this.attr('data-closed_text'));
        }
    });

    axios.get('/ajax/referrer/income/company/' + shopId + '?start_at=' + startDate + '&end_at=' + endDate + getFilterByFromLS(), {})
        .then(response => {

            $('[data-shop=' + shopId + ']').remove();
            $actionButton.toggleClass('active').html('Close<br/><span></span>details').removeClass('btn-third').addClass('btn-danger');

            if (response.data.length) {
                const tmpl = `
                    <div class="rTableHeading table-lvl1-content" data-shop="${shopId}" data-id="shop-${shopId}-0">
                        <div class="rTableRow">
                            <div class="rTableHead width-24">Affiliate<br/>Link</div>
                            <div class="rTableHead"></div>
                            <div class="rTableHead">Last Purchase Date</div>
                            <div class="rTableHead">Purchase Value</div>
                            <div class="rTableHead">Click<br/>Count</div>
                            <div class="rTableHead">My<br/>PayOut</div>
                            <div class="rTableHead text-right">Actions</div>
                        </div>
                    </div>
                `;

                $('[data-id=shop-' + shopId + ']').after(tmpl);
            }

            _.map(response.data, function (item, i) {
                let actionBtn = '<i class="fa fa-times-circle"></i>';
                if (item.sign_up_count || item.purchase_count) {
                    actionBtn = `<div class="table-link actions btn btn-third" data-closed_text="Payout<br/><span></span>details" onClick="proto.renderSingleReferrerLinkStats(event, ${shopId}, ${item.referrer_link.id}, '${startDate}', '${endDate}')">Payout<br/><span></span>details</div>`;
                }

                var lastPurchaseDate = null ;
                var lastPurchaseContent = null;

                const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                ];

                if(item.last_purchase_date){
                    lastPurchaseDate = new Date(item.last_purchase_date.replace(/ /g,"T"));

                    if(lastPurchaseDate != 'Invalid Date'){
                        var currMonth = monthNames[lastPurchaseDate.getMonth()];

                        lastPurchaseContent =  lastPurchaseDate.getDate() + ' ' + currMonth + ' ' + lastPurchaseDate.getFullYear();
                        lastPurchaseContent += '<br/>';
                        lastPurchaseContent += (lastPurchaseDate.getHours().toString().length == 1 ? '0' + lastPurchaseDate.getHours() : lastPurchaseDate.getHours()) + ':' + (lastPurchaseDate.getMinutes().toString().length == 1 ? '0' + lastPurchaseDate.getMinutes() : lastPurchaseDate.getMinutes()) + ':' + (lastPurchaseDate.getSeconds().toString().length == 1 ? '0' + lastPurchaseDate.getSeconds() : lastPurchaseDate.getSeconds());
                    } else {
                        lastPurchaseContent = item.last_purchase_date;
                    }
                }

                const tmpl = `
                    <div class="rTableRow table-lvl1-content" data-shop="${shopId}"  data-id="shop-${shopId}-${i + 1}" data-referrer-link="${item.referrer_link.id}">
                        <div class="rTableCell width-24"><input readonly value="${window.location.protocol + '//' + item.referrer_link.shop_url_name + '.' + window.location.host + '/' + item.referrer_link.nickname + '/' + item.referrer_link.slug}"/></div>
                        <div class="rTableCell"><span class="mobile_table_header hide"></div>
                        <div class="rTableCell"><span class="mobile_table_header hide">Last Purchase Date: </span>${lastPurchaseContent ? lastPurchaseContent : '<i class=\"fa fa-times-circle\"></i>'}</div>
                        <div class="rTableCell"><span class="mobile_table_header hide">Purchase Value: </span>${item.purchase_total}</div>
                        <div class="rTableCell"><span3 class="mobile_table_header hide">Click Count: </span3>${item.views_count}</div>
                        <div class="rTableCell"><span class="mobile_table_header hide">My PayOut: </span>€${item.total_referrer_profit}</div>
                        <div class="rTableCell text-right">${actionBtn}</div>
                    </div>
                `;
                $('[data-id=shop-' + shopId + '-' + i + ']').after(tmpl)
            })

    });
};

proto.renderSingleReferrerLinkStats = function (event, shopId, referrerLinkId, startDate, endDate) {

    $('div.exchange-rates').remove();

    $(event.target).parent().parent().siblings('.table-lvl1-content').each(function(){

        var $this = $(this);

        if($this.children('.rTableCell').children('.table-link.actions.active').length){

            $('div[data-reflink="'+$(this).data('referrer-link')+'"]').remove();
            $this.children('.rTableCell').children('.table-link.actions.active').html($this.children('.rTableCell').children('.table-link.actions.active').data('closed_text'));
            $this.children('.rTableCell').children('.table-link.actions.active').removeClass('active btn-danger').addClass('btn-third');

        }

    });

    $('[data-reflink='+referrerLinkId+']').remove();

    const $actionButton = $(event.currentTarget);

    if ($actionButton.hasClass('active')) {
        $actionButton.toggleClass('active').html('Payout<br/><span></span>details').removeClass('btn-danger').addClass('btn-third');
        return
    }

    axios.get('/ajax/referrer/income/referrer-link/' + referrerLinkId + '?start_at=' + startDate + '&end_at=' + endDate, {})
        .then(response => {

            $actionButton.toggleClass('active').toggleClass('btn-third').toggleClass('btn-danger').html('Close<br/><span></span>details');

            $('[data-reflink=' + referrerLinkId + ']').remove();

            if (response.data.length) {
                const tmpl = `
                   <div class="rTableHeading table-lvl2-content" data-shop="${shopId}" data-reflink="${referrerLinkId}" data-id="product-${referrerLinkId}-0">
                        <div class="rTableRow">
                            <div class="rTableHead width-24">Source<br/>Links</div>
                            <div class="rTableHead"></div>
                            <div class="rTableHead">Income<br/>Generated</div>
                            <div class="rTableHead">Reward<br/>Value</div>
                            <div class="rTableHead">Purchase<br/>Value</div>
                            <div class="rTableHead">Purchase</div>
                            <div class="rTableHead text-right">Actions</div>
                        </div>
                   </div>
                `;
                $('[data-referrer-link=' + referrerLinkId + ']').after(tmpl)
            }

            _.map(response.data, function (item, i) {

                let profit_tooltip = '';
                if(item.original_currency_referrer_profit && item.original_currency != 'EUR'){
                    profit_tooltip = '<span class="shop-table-tooltip-elem" data-tippy-content="'+item.original_currency_referrer_profit+' '+item.original_currency+'"><i class="fa fa-question-circle"></i></span>';
                }

                let rowId = `product-${referrerLinkId}-${i + 1}`;
                let onClickFn = `proto.renderExchangeRatesStats(event, '${rowId}', '${referrerLinkId}', '${startDate}', '${endDate}', '${item.offer_type}', '${item.commission_type}', '${item.commission}', '${item.source_link}', '${item.purchases_price}')`;

                let actionBtn = '<i class=\"fa fa-times-circle\"></i>';
                if (item.has_exchanges) {
                    actionBtn = `<div class="table-link actions btn btn-third" data-closed_text="Payout<br/><span></span>details" onClick="${onClickFn}">eX.rate<br/><span></span>details</div>`;
                }

                const tmpl = `
                    <div class="rTableRow table-lvl2-content" data-shop="${shopId}" data-reflink="${referrerLinkId}" data-id="${rowId}">
                        <div class="rTableCell width-24"><span class="mobile_table_header hide">Source Links: </span>${item.source_link ? '<input value="'+item.source_link+'" readonly>' : '<i class=\"fa fa-times-circle\"></i>'}</div>
                        <div class="rTableCell"></div>
                        <div class="rTableCell">
                            <span class="mobile_table_header hide">Income: </span>€${item.referrer_profit}${profit_tooltip}
                        </div>
                        <div class="rTableCell"><span class="mobile_table_header hide">Reward Value: </span>${item.offer_type} ${item.commission}${item.commission_type == 'percent' ? '%' : '€'}</div>
                        <div class="rTableCell"><span class="mobile_table_header hide">Purchase Value : </span>${item.purchases_price ? '€' + item.purchases_price : '<i class=\"fa fa-times-circle\"></i>'}</div>
                        <div class="rTableCell"><span class="mobile_table_header hide">Number of Purchase: </span>${item.offer_type == 'CPS' ? item.purchases_amount : item.transactions_count}</div>
                        <div class="rTableCell text-right">${actionBtn}</div>
                    </div>
                `;
                $('[data-id=product-' + referrerLinkId + '-' + i + ']').after(tmpl)

                tippy('span');
            })

        }).catch(error => {

    })
};

proto.renderExchangeRatesStats = function (
    event,
    targetId,
    referrerLinkId,
    startDate,
    endDate,
    offerType,
    commissionType,
    commission,
    sourceLink,
    price
) {

    $(event.target).parent().parent().siblings('.rTableRow.table-lvl2-content').each(function(){

        var $this = $(this);

        if($this.children('.rTableCell').children('.table-link.actions.active').length){

            $('div.'+$(this).data('id')+'-exchange-rates').remove();
            $this.children('.rTableCell').children('.table-link.actions.active').html($this.children('.rTableCell').children('.table-link.actions.active').data('closed_text'));
            $this.children('.rTableCell').children('.table-link.actions.active').removeClass('active btn-danger').addClass('btn-third');

        }

    });

    const $actionButton = $(event.currentTarget);

    $('.' + targetId + '-exchange-rates').remove();

    if ($actionButton.hasClass('active')) {
        $actionButton.toggleClass('active').html('eX.rate<br/><span></span>details').removeClass('btn-danger').addClass('btn-third');
        return
    }


    let url = `/ajax/referrer/income/referrer-link/${referrerLinkId}/exchange-rates?start_at=${startDate}&end_at=${endDate}&offer_type=${offerType}&commission_type=${commissionType}&commission=${commission}&source_link=${sourceLink}&purchase_price=${price}`;

    axios.get(url, {})
        .then(response => {

            $actionButton.toggleClass('active').toggleClass('btn-third').toggleClass('btn-danger').html('Close');

            if (response.data.length) {
                const tmpl = `
                    <div class="rTableHeading table-lvl2-content ${targetId}-exchange-rates exchange-rates" data-id="${targetId}-exchange-rates">
                        <div class="rTableRow">
                            <div class="rTableHead">Transaction ID</div>
                            <div class="rTableHead">Source<br>Currency</div>
                            <div class="rTableHead">Exchange<br>Rate</div>
                            <div class="rTableHead">Transaction<br>Date &amp; Time</div>
                            <div class="rTableHead">Value in<br>Euro</div>
                        </div>
                    </div>
                `;
                $('[data-id=' + targetId + ']').after(tmpl)
            }

            _.map(response.data, function (item, i) {

                const tmpl = `
                    <div class="rTableRow table-lvl2-content ${targetId}-exchange-rates exchange-rates" style="background: #e2e2e2">
                        <div class="rTableCell">
                            <span class="mobile_table_header hide" tabindex="0">Number: </span>${response.data.length - i}
                        </div>
                        <div class="rTableCell"><span class="mobile_table_header hide" tabindex="0">Source Currency: </span>${item.originalPrice ? item.originalPrice + ' ' + item.originalCurrency : '<i class="fa fa-times-circle"></i>'}</div>
                        <div class="rTableCell"><span class="mobile_table_header hide" tabindex="0">Exchange Rate : </span>${item.exchangeRate ? item.exchangeRate + ' ' + item.originalCurrency : '<i class="fa fa-times-circle"></i>'}</div>
                        <div class="rTableCell"><span class="mobile_table_header hide" tabindex="0">Transaction Date & Time: </span>${String(item.createdAt.date).slice(0, -7)}</div>
                        <div class="rTableCell"><span class="mobile_table_header hide" tabindex="0">Value in Euro: </span>${item.purchasesPrice ? item.purchasesPrice + ' EUR' : '<i class="fa fa-times-circle"></i>'}</div>
                    </div>
                `;

                $('[data-id=' + targetId + '-exchange-rates]').after(tmpl)
            });

        }).catch(error => {

        })
};
