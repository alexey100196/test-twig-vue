<?php


namespace App\Service\Admin\Reports;


use App\Helper\Math;

class ReferrerPayout
{
    /** @var string */
    private $referrerEmail;

    /** @var string */
    private $amount;

    /** @var string|null */
    private $referrerName;

    /** @var string|null */
    private $referrerSurname;

    /** @var string|null */
    private $currency;

    public function __construct(
        string $referrerEmail,
        string $amount,
        string $referrerName = null,
        string $referrerSurname = null,
        string $currency = null
    )
    {
        $this->referrerEmail = $referrerEmail;
        $this->amount = $amount;
        $this->referrerName = $referrerName;
        $this->referrerSurname = $referrerSurname;
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getReferrerEmail(): string
    {
        return $this->referrerEmail;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @return null|string
     */
    public function getReferrerName(): ?string
    {
        return $this->referrerName;
    }

    /**
     * @return null|string
     */
    public function getReferrerSurname(): ?string
    {
        return $this->referrerSurname;
    }

    public function addAmount($amount)
    {
        $this->amount = Math::add($this->amount, $amount);
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }
}
