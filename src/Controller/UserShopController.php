<?php

namespace App\Controller;

use App\Entity\UserShop;
use App\Manager\UserManager;
use App\Repository\ShopRepository;
use App\Repository\UserShopRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class UserShopController extends AbstractController
{
    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * @var UserShopRepository
     */
    private $userShopRepository;

    private $userManager;

    /**
     * @param UserShopRepository $userShopRepository
     */
    public function __construct(
        ShopRepository $shopRepository,
        UserShopRepository $userShopRepository,
        UserManager $userManager
    )
    {
        $this->userShopRepository = $userShopRepository;
        $this->shopRepository = $shopRepository;
        $this->userManager = $userManager;
    }

    /**
     * Create new UserShop entity (join to shop program)
     * @Route("/company/{shop_id}/user", name="shop_user_store", methods={"POST"})
     *
     * @Security("has_role('ROLE_USER')")
     * @param Request $request
     * @param AuthorizationCheckerInterface $authChecker
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function store(Request $request, AuthorizationCheckerInterface $authChecker)
    {
        $shop = $this->shopRepository->find($request->get('shop_id'));

        if (!$authChecker->isGranted('JOIN_SHOP_PROGRAM', $shop)) {
            $this->addFlash('error', 'You have already joined the program.');
            return $this->redirectToRoute('subdomain_shop_show', ['subdomain' => $shop->getUrlName()]);
        }

        $this->userManager->joinToShopProgram($this->getUser(), $shop);

        $this->addFlash('success', 'You joined the program.');

        return $this->redirectToRoute('account_referrer_programs_index');
    }

}
