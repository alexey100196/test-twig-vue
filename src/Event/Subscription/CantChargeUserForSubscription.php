<?php


namespace App\Event\Subscription;

use App\Entity\Subscription\PlanSubscription;
use Symfony\Component\EventDispatcher\Event;

class CantChargeUserForSubscription extends Event
{
    /**
     * @var string
     */
    const NAME = 'subscription.cant-charge-user';

    /**
     * @var PlanSubscription
     */
    protected $subscription;

    public function __construct(PlanSubscription $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * @return PlanSubscription
     */
    public function getSubscription(): PlanSubscription
    {
        return $this->subscription;
    }
}