<?php


namespace App\Service\Shop;

use App\Entity\Shop;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;

class ShopUpdate
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * ShopProduct constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
    }

    /**
     * @param Shop $shop
     * @param FormInterface $form
     * @return Shop
     */
    public function save(Shop $shop, FormInterface $form)
    {
        $logoFile = $form->get('logoFile')->getData();
        if ($logoFile) {
            $base64 = base64_encode(file_get_contents($logoFile));
            $logoMimeType = $logoFile->getMimeType();
            $shop->setLogo('data:'.$logoMimeType.';base64,'.$base64);
        }

        $shop->setWizardStep(1);

        $this->em->persist($shop);
        $this->em->flush();

        return $shop;
    }

}