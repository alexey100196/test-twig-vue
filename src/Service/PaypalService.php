<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use \PayPal\Rest\ApiContext;
use \PayPal\Auth\OAuthTokenCredential;


class PaypalService{

    /**
     * @var string
     */
    private $paypalLink = '';

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var \PayPal\Rest\ApiContext
     */
    private $apiContext;

    /**
     * PaypalService constructor.
     * @param UrlGeneratorInterface $router
     * @param ParameterBagInterface $params
     */
    public function __construct(UrlGeneratorInterface $router, ParameterBagInterface $params)
    {
        $this->router = $router;
        $this->paypalLink = $params->get("paypal.url");
        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                $params->get('paypal.client_id'),
                $params->get('paypal.client_secret')
            )
        );
    }

    /**
     * Return paypal auth url.
     * @param null|string $companyId
     * @return string
     */
    public function getAuthUrl(?string $companyId = null): string
    {
        $url = $this->router->generate('widget_iframe_paypal', [], UrlGeneratorInterface::ABSOLUTE_URL);

        return $this->paypalLink.\PayPal\Auth\Openid\PPOpenIdSession::getAuthorizationUrl(
            $url,
            array('openid', 'profile', 'email'),
            null,
            null,
            null,
            $this->apiContext
        );
    }

    /**
     * @param string $authCode
     * @return object
     */
    public function getTokens(string $authCode = '') : object
    {
        try {
            $accessToken = \PayPal\Auth\Openid\PPOpenIdTokeninfo::createFromAuthorizationCode(
                array('code' => $authCode),
                null,
                null,
                $this->apiContext
            );
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'PP001: Get paypal access token: Request has been rejected');
        }

        return $accessToken;
    }

    public function getUserInfo (string $refreshToken = '')
    {
        try {
            $tokenInfo = new \PayPal\Auth\Openid\PPOpenIdTokeninfo();
            $tokenInfo = $tokenInfo->createFromRefreshToken(array('refresh_token' => $refreshToken), $this->apiContext);
            $params = array('access_token' => $tokenInfo->getAccessToken());
            $userInfo = \PayPal\Auth\Openid\PPOpenIdUserinfo::getUserinfo($params, $this->apiContext);
        } catch (\Exception $ex) {
            throw new HttpException(Response::HTTP_FORBIDDEN, 'PP002: Get paypal user info: Request has been rejected');
        }

        return $userInfo;
    }
}
