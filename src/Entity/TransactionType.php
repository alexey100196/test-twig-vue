<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TransactionTypeRepository")
 */
class TransactionType extends BaseGlossary
{
    const BALANCE_CORRECTION = 1;
    const SHOP_BUDGET = 2;
    const SHOP_DEPOSIT = 3;

    const SHOP_CONVERSION_REFERRER_PROFIT = 4;
    const SHOP_CONVERSION_SYSTEM_COMMISSION = 5;

    const SHOP_CONVERSION_REFERRER_PROFIT_RETURN = 6;
    const SHOP_CONVERSION_SYSTEM_COMMISSION_RETURN = 7;

    const USER_SUBSCRIPTION = 8;
    const USER_PAYOUT = 9;
}
