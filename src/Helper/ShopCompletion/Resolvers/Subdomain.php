<?php

namespace App\Helper\ShopCompletion\Resolvers;

use App\Helper\ShopCompletion\Resolvers\AbstractResolver;

class Subdomain extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {

        if ($this->shop->getUrlName()) {
            $this->addScore(100);
        }
    }
}
