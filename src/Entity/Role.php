<?php

namespace App\Entity;

use App\Entity\BaseGlossary;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 */
class Role extends BaseGlossary
{
    const ROLE_USER = 'ROLE_USER';
    const ROLE_SHOP = 'ROLE_SHOP';
    const ROLE_REFERRER = 'ROLE_REFERRER';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_FULL_PROFILE = 'FULL_PROFILE';

    const ROLE_SHOP_ID = 1;
    const ROLE_REFERRER_ID = 2;
    const ROLE_ADMIN_ID = 3;
    const ROLE_FULL_PROFILE_ID = 4;

    /**
     * @return boolean
     */
    public function isShop(): bool
    {
        return $this->getName() == self::ROLE_SHOP;
    }
}
