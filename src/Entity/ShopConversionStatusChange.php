<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShopConversionStatusChangeRepository")
 */
class ShopConversionStatusChange
{
    public function __construct(
        ShopConversion $shopConversion,
        ShopConversionStatus $statusBefore,
        ShopConversionStatus $statusAfter,
        User $user,
        string $reason = null
    )
    {
        $this->shopConversion = $shopConversion;
        $this->statusBefore = $statusBefore;
        $this->statusAfter = $statusAfter;
        $this->user = $user;
        $this->reason = $reason;
        $this->changedAt = new \DateTime();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShopConversionStatus")
     * @ORM\JoinColumn(nullable=false)
     */
    private $statusBefore;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShopConversionStatus")
     * @ORM\JoinColumn(nullable=false)
     */
    private $statusAfter;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShopConversion")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shopConversion;

    /**
     * @ORM\Column(type="datetime")
     */
    private $changedAt;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $reason;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatusBefore(): ?ShopConversionStatus
    {
        return $this->statusBefore;
    }

    public function setStatusBefore(?ShopConversionStatus $statusBefore): self
    {
        $this->statusBefore = $statusBefore;

        return $this;
    }

    public function getStatusAfter(): ?ShopConversionStatus
    {
        return $this->statusAfter;
    }

    public function setStatusAfter(?ShopConversionStatus $statusAfter): self
    {
        $this->statusAfter = $statusAfter;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getShopConversion(): ?ShopConversion
    {
        return $this->shopConversion;
    }

    public function setShopConversion(?ShopConversion $shopConversion): self
    {
        $this->shopConversion = $shopConversion;

        return $this;
    }

    public function getChangedAt(): ?\DateTimeInterface
    {
        return $this->changedAt;
    }

    public function setChangedAt(\DateTimeInterface $changedAt): self
    {
        $this->changedAt = $changedAt;

        return $this;
    }

    public function getReason(): ?string
    {
        return $this->reason;
    }

    public function setReason(?string $reason): self
    {
        $this->reason = $reason;

        return $this;
    }
}
