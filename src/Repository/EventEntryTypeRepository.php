<?php

namespace App\Repository;

use App\Entity\EventEntryType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EventEntryType|null find($id, $lockMode = null, $lockVersion = null)
 * @method EventEntryType|null findOneBy(array $criteria, array $orderBy = null)
 * @method EventEntryType[]    findAll()
 * @method EventEntryType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventEntryTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EventEntryType::class);
    }
}
