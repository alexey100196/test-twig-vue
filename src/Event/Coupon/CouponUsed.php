<?php

namespace App\Event\Coupon;

use App\Entity\Coupon\Coupon;
use Symfony\Component\EventDispatcher\Event;

class CouponUsed extends Event
{
    /**
     * Event name.
     * @var string
     */
    const NAME = 'coupon.used';

    /**
     * @var Coupon
     */
    private $coupon;

    /**
     * @param Coupon $coupon
     */
    public function __construct(Coupon $coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * @return Coupon
     */
    public function getCoupon(): Coupon
    {
        return $this->coupon;
    }
}
