<?php

namespace App\Controller\Admin;

use App\Entity\Currency;
use App\Entity\ShopStatus;
use App\Entity\UserBalance;
use App\Form\Admin\Shop\EditShopType;
use App\Form\Balance\UserTransactionType;
use App\Repository\ShopRepository;
use App\Service\Balance\UserBalanceService;
use App\Util\Consts;
use Knp\Component\Pager\PaginatorInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ShopBudgetController extends AbstractController
{
    const PER_PAGE = 25;

    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * @param ShopRepository $shopRepository
     */
    public function __construct(ShopRepository $shopRepository)
    {
        $this->shopRepository = $shopRepository;
    }

    /**
     * @Route("/admin/company-budget", name="admin_shop_budget_index")
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $shops = $paginator->paginate(
            $this->shopRepository->findAllWithStatusQueryBuilder([ShopStatus::ACCEPTED_NAME, ShopStatus::ACTIVE_NAME, ShopStatus::SUSPENDED_NAME])->getQuery(),
            $request->query->getInt('page', 1),
            self::PER_PAGE
        );

        return $this->render('admin/shop-budget/index.html.twig', ['shops' => $shops]);
    }
}
