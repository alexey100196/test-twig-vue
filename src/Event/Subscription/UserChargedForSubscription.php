<?php


namespace App\Event\Subscription;


use App\Entity\Subscription\PlanSubscription;
use App\Entity\Transaction;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\EventDispatcher\Event;

class UserChargedForSubscription extends Event
{
    /**
     * @var string
     */
    const NAME = 'subscription.user-charged';

    /**
     * @var PlanSubscription
     */
    protected $subscription;

    /**
     * @var Transaction
     */
    protected $transactionUuid;

    public function __construct(PlanSubscription $subscription, UuidInterface $transactionUuid)
    {
        $this->subscription = $subscription;
        $this->transactionUuid = $transactionUuid;
    }

    /**
     * @return PlanSubscription
     */
    public function getSubscription(): PlanSubscription
    {
        return $this->subscription;
    }

    /**
     * @return UuidInterface
     */
    public function getTransactionUuid(): UuidInterface
    {
        return $this->transactionUuid;
    }
}