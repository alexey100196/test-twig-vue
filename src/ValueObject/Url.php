<?php


namespace App\ValueObject;


class Url implements \ArrayAccess
{
    private $parts;

    public function __construct(string $url)
    {
        $parts = explode('://', $url);

        if (count($parts) >= 2) {
            $this->parts['protocol'] = $parts[0];
            $this->parts['url'] = $parts[1];
        } else {
            $this->parts['protocol'] = 'http';
            $this->parts['url'] = $parts[0];
        }
    }

    /**
     * @return mixed
     */
    public function getProtocol()
    {
        return $this->parts['protocol'];
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->parts[] = $value;
        } else {
            $this->parts[$offset] = $value;
        }
    }

    public function offsetExists($offset)
    {
        return isset($this->parts[$offset]);
    }

    public function offsetUnset($offset)
    {
        unset($this->parts[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->parts[$offset]) ? $this->parts[$offset] : null;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->parts['url'];
    }

    public function __toString()
    {
        return $this->parts['protocol'] . '://' . $this->parts['url'];
    }

}