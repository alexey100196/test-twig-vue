<?php


namespace App\Service\SystemCommission\ShopCommission\Strategies;


use App\Entity\Shop;
use App\Helper\Math;
use App\Repository\ShopPurchaseRepository;
use App\Service\SystemCommission\ValueObjects\TaxedAmount;
use Carbon\Carbon;
use Psr\Log\LoggerInterface;

class EcommerceStrategy extends SaasStrategy
{
    protected $shopPurchaseRepository;

    protected $logger;

    protected $steps = [
        '0.04' => [0, 1000],
        '0.03' => [1000, 10000],
        '0.02' => [10000, 50000],
        '0.01' => [50000, null]
    ];

    public function __construct(ShopPurchaseRepository $shopPurchaseRepository, LoggerInterface $logger)
    {
        $this->shopPurchaseRepository = $shopPurchaseRepository;
        $this->logger = $logger;
    }

    public function apply(Shop $shop, $amount, $fee)
    {
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now()->endOfMonth();
        $turnover = $this->shopPurchaseRepository->sumShopTurnover($shop, $start, $end);
        $percent = $this->getPercentForAmount($turnover);

        $fee = Math::mul($amount, $percent);

        return new TaxedAmount($fee, $percent);
    }

    /**
     * @param $amount
     * @return float
     */
    private function getPercentForAmount($amount)
    {
        foreach ($this->steps as $percent => $range) {
            if ($amount >= $range[0] && ($range[1] ? $amount < $range[1] : true)) {
                return $percent;
            }
        }
    }
}
