<?php

namespace App\Repository;

use App\Entity\PasswordReminder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PasswordReminder|null find($id, $lockMode = null, $lockVersion = null)
 * @method PasswordReminder|null findOneBy(array $criteria, array $orderBy = null)
 * @method PasswordReminder[]    findAll()
 * @method PasswordReminder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PasswordReminderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PasswordReminder::class);
    }

    public function findByHash(string $hash)
    {
        return $this->findOneBy(['hash' => $hash]);
    }
}
