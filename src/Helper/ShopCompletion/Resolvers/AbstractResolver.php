<?php

namespace App\Helper\ShopCompletion\Resolvers;

use App\Entity\Shop;
use App\Helper\ShopCompletion\Result;
use App\Helper\ShopCompletion\ShopCompletionResolver;

abstract class AbstractResolver
{
    /**
     * @var Shop
     */
    protected $shop;

    /**
     * @var integer
     */
    protected $score = 0;

    /**
     * @var ShopCompletionResolver
     */
    protected $resolver;

    /**
     * @return mixed
     */
    abstract public function resolve();

    /**
     * @param Shop $shop
     * @return AbstractResolver
     */
    public static function getInstance(Shop $shop)
    {
        $obj = new static;
        $obj->setShop($shop);

        return $obj;
    }

    /**
     * @param Shop $shop
     */
    public function setShop(Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return Result
     */
    public function getResult(): Result
    {
        $result = new Result();
        $result->setName($this->getName());
        $result->setScore($this->getScore());

        return $result;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        $name = explode('\\', static::class);

        return array_pop($name);
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @param int $score
     */
    public function addScore(int $score)
    {
        $this->setScore($this->getScore() + $score);

        if ($this->score > 100) {
            throw new \Exception('Invalid score. Max is 100.');
        }
    }

    /**
     * @param int $score
     */
    public function setScore(int $score)
    {
        $this->score = $score;
    }

    /**
     * @param ShopCompletionResolver $resolver
     */
    public function setResolver(ShopCompletionResolver $resolver)
    {
        $this->resolver = $resolver;
    }

    /**
     * @return ShopCompletionResolver
     */
    public function getResolver()
    {
        return $this->resolver;
    }
}
