<?php

namespace App\Command;

use App\Entity\Subscription\PlanSubscription;
use App\Repository\Subscription\PlanSubscriptionRepository;
use App\Service\Admin\Reports\ReferrerPayoutsService;
use App\Service\Exchange\ExchangeService;
use App\Service\Subscription\ShopSubscriptionService;
use Carbon\Carbon;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NotifyUsersAboutExpiredTrialCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:notify-users-about-expired-trial';

    /**
     * @var PlanSubscriptionRepository
     */
    private $planSubscriptionRepository;

    /**
     * @var ShopSubscriptionService
     */
    private $shopSubscriptionService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        string $name = null,
        PlanSubscriptionRepository $planSubscriptionRepository,
        ShopSubscriptionService $shopSubscriptionService,
        LoggerInterface $logger
    )
    {
        parent::__construct($name);

        $this->planSubscriptionRepository = $planSubscriptionRepository;
        $this->shopSubscriptionService = $shopSubscriptionService;
        $this->logger = $logger;
    }

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $now = Carbon::now();

        $subscriptions = $this->planSubscriptionRepository->findEndedTrialSubscriptions(
            $now->startOfHour(),
            $now->clone()->endOfHour()
        );

        foreach ($subscriptions as $subscription) {
            $this->shopSubscriptionService->notifyUserAboutEndedTrial($subscription);
            $this->logNotification($subscription);
        }
    }

    private function logNotification(PlanSubscription $subscription)
    {
        $this->logger->info('Send trial expiration notification.', [
            'user' => $subscription->getUser()->getId(),
            'subscription' => $subscription->getId(),
        ]);
    }
}
