<?php

namespace App\Repository\ShortLinks;

use App\Entity\CustomDomain;
use App\Entity\ReferrerLink;
use App\Entity\ShortLinks\AffiliateShortLink;
use App\Entity\ShortLinks\AffiliateShortLinkStats;
use App\Entity\ShortLinks\AffiliateShortLinkTax;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AffiliateShortLink|null find($id, $lockMode = null, $lockVersion = null)
 * @method AffiliateShortLink|null findOneBy(array $criteria, array $orderBy = null)
 * @method AffiliateShortLink[]    findAll()
 * @method AffiliateShortLink[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AffiliateShortLinkRepository extends ServiceEntityRepository
{
    /** @var EntityManagerInterface $em */
    private $em;

    /**
     * AffiliateShortLinkRepository constructor.
     * @param RegistryInterface $registry
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(RegistryInterface $registry, EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        parent::__construct($registry, AffiliateShortLink::class);
    }

    /**
     * @param $user
     * @param string|null $from
     * @param string|null $to
     * @param string|null $tags
     * @param string|null $folders
     * @param string|null $companies
     * @param string|null $page
     * @param string|null $pageSize
     * @param object|null $sort
     * @return array
     */
    public function findByFilters($user, ?string $from = null, ?string $to = null, ?string $tags = null, ?string $folders = null, ?string $companies, ?string $page = null, ?string $pageSize = null, ?object $sort = null) {
        $host = (isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])) ? $_SERVER['HTTP_HOST'] : 'http://marfil-app.local';
        $query = $this->em->getRepository(ReferrerLink::class)->createQueryBuilder('r')
            ->select(
                'a.id as id, a.short as short, CONCAT(\'https://\', rs.urlName, \'.\', \''.$host.'\', \'/\', r.nickname, \'/\', r.slug) as original, r.id as referrerLinkId, a.description as description, COUNT(s.id) as clicks, r.createdAt as createdAt, a.updatedAt as updatedAt,
                cd.id as customDomainId, cd.name as customDomainName,
                GROUP_CONCAT(DISTINCT tt.name SEPARATOR :separator) as tags, GROUP_CONCAT(DISTINCT tf.name SEPARATOR :separator) as folders, 
                GROUP_CONCAT(DISTINCT us.name SEPARATOR :separator) as utm_source, GROUP_CONCAT(DISTINCT um.name SEPARATOR :separator) as utm_medium, GROUP_CONCAT(DISTINCT uca.name SEPARATOR :separator) as utm_campaign, GROUP_CONCAT(DISTINCT uc.name SEPARATOR :separator) as utm_content, GROUP_CONCAT(DISTINCT uk.name SEPARATOR :separator) as utm_keyword,
                a.facebookPixel as facebookPixel, a.twitterPixel as twitterPixel, a.pinterestPixel as pinterestPixel, a.linkedinPixel as linkedinPixel, a.googlePixel as googlePixel, a.quoraPixel as quoraPixel,
                a.metaTitle as metaTitle, a.metaDesc as metaDesc, a.metaImage as metaImage, a.metaIframe as metaIframe,
                SUM(a.facebookPixel + a.twitterPixel + a.pinterestPixel + a.linkedinPixel + a.googlePixel + a.quoraPixel) as pixelsCount'
            )
            ->leftJoin(AffiliateShortLink::class, 'a', Join::WITH, 'r = a.referrerLink')
            ->leftJoin(CustomDomain::class, 'cd', Join::WITH, 'a.customDomain = cd')
            ->leftJoin(AffiliateShortLinkStats::class, 's', Join::WITH, 'a = s.affiliateShortLink')
            ->leftJoin(AffiliateShortLinkTax::class, 'tt', Join::WITH, 'a = tt.link AND tt.type = \'tag\'')
            ->leftJoin(AffiliateShortLinkTax::class, 'tf', Join::WITH, 'a = tf.link AND tf.type = \'folder\'')
            ->leftJoin(AffiliateShortLinkTax::class, 'us', Join::WITH, 'a = us.link AND us.type = \'utm_source\'')
            ->leftJoin(AffiliateShortLinkTax::class, 'um', Join::WITH, 'a = um.link AND um.type = \'utm_medium\'')
            ->leftJoin(AffiliateShortLinkTax::class, 'uca', Join::WITH, 'a = uca.link AND uca.type = \'utm_campaign\'')
            ->leftJoin(AffiliateShortLinkTax::class, 'uc', Join::WITH, 'a = uc.link AND uc.type = \'utm_content\'')
            ->leftJoin(AffiliateShortLinkTax::class, 'uk', Join::WITH, 'a = uk.link AND uk.type = \'utm_keyword\'')
            ->innerJoin('r.shop', 'rs')
            ->where('r.referrer = :user');

        if ($from) $query->andWhere('r.createdAt >= STR_TO_DATE(:from, \'%Y-%m-%d %H:%i:%s\')');
        if ($to) $query->andWhere('r.createdAt <= STR_TO_DATE(:to, \'%Y-%m-%d %H:%i:%s\')');
        if ($tags) $query->andWhere('tt.name IN (\''.str_replace(', ', '\', \'', strip_tags($tags)).'\')');
        if ($folders) $query->andWhere('tf.name IN (\''.str_replace(', ', '\', \'', strip_tags($folders)).'\')');
        if ($companies) $query->andWhere('r.shop IN (\''.str_replace(', ', '\', \'', strip_tags($companies)).'\')');

        $query->setParameter('user', $user);
        $query->setParameter('separator', ', ');

        if ($from) $query->setParameter('from', $from);
        if ($to) $query->setParameter('to', $to);

        if ($sort && isset($sort->order)) {
            $mSort = ($sort->order === 'ascending') ? 'ASC' : 'DESC';
            if (!isset($sort->prop) || empty($sort->prop)) {
                $query->orderBy('pixelsCount', $mSort);
            } else {
                $query->orderBy(
                    (in_array($sort->prop, ['clicks', 'original']) || substr($sort->prop, 0, 3) === 'utm') ? $sort->prop : 'a.'.$sort->prop,
                    $mSort
                );
            }
        } else {
            $query->orderBy('a.updatedAt', 'DESC');
        }

        $mPageSize = $pageSize ? ((int) $pageSize) : 20;
        return (array) $query->groupBy('r')->addGroupBy('a')
            ->setFirstResult($page ? ($page-1)*$mPageSize : 0)
            ->setMaxResults($page ? $page*$mPageSize : $mPageSize)
            ->getQuery()
            ->getResult();
    }

    public function getUserTotal($user) {
        return $this->createQueryBuilder('a')
            ->select('COUNT(a.id)')
            ->where('a.referrer = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findDuplicate(object $link) {
        $query = $this->createQueryBuilder('a')
            ->where('a.short = :short');

        if(get_class($link) === 'App\Entity\ShortLinks\AffiliateShortLink') {
            $query->andWhere('a.id != :id');
        }

        $query->setParameter('short', $link->getShort());

        if(get_class($link) === 'App\Entity\ShortLinks\AffiliateShortLink') {
            $query->setParameter('id', $link->getId());
        }

        $query = $query->getQuery()->getResult();

        return $query;
    }
}
