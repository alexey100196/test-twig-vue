<?php

namespace App\Entity\Widget;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Widget\WidgetReferenceLinkBrowserFingerprintRepository")
 */
class WidgetReferenceLinkBrowserFingerprint
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Widget\WidgetReferenceLink", inversedBy="browserFingerprints")
     * @ORM\JoinColumn(nullable=false)
     */
    private $widgetReferenceLink;

    public static function create(string $content):self
    {
        $obj = new self();
        $obj->setContent($content);

        return $obj;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getWidgetReferenceLink(): ?WidgetReferenceLink
    {
        return $this->widgetReferenceLink;
    }

    public function setWidgetReferenceLink(?WidgetReferenceLink $widgetReferenceLink): void
    {
        $this->widgetReferenceLink = $widgetReferenceLink;
    }
}
