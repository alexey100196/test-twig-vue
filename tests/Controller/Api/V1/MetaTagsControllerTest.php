<?php

namespace App\Tests\Controller\Api\V1;


use App\Entity\Shop;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DomCrawler\Crawler;

class MetaTagsControllerTest extends WebTestCase
{
    /**
     * @var Shop
     */
    protected $shop;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $client = static::createClient();
        $em = $client->getKernel()->getContainer()->get('doctrine')->getManager();

        $this->shop = $em->getRepository(Shop::class)->findOneBy(['name' => 'Demo']);
    }

    /**
     * @testdox Redirect to login if user isn't referrer
     */
    public function testMetaTags()
    {
        $client = static::createClient();
        $domain = $client->getKernel()->getContainer()->getParameter('domain');
        $client->setServerParameter('HTTP_HOST', $domain);

        // Without data
        $client->request('POST', '/api/v1/meta', []);
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
        $this->assertEquals([
            'errors' => [
                'link' => 'This value should not be blank.'
            ]
        ], json_decode($client->getResponse()->getContent(), true));

        // Invalid link
        $client->request('POST', '/api/v1/meta', [
            'companyId' => $this->shop->getPartnerNumber(),
            'link' => 'asdasd'
        ]);
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_UNPROCESSABLE_ENTITY, $client->getResponse()->getStatusCode());
        $this->assertEquals([
            'errors' => [
                'link' => 'This value is not a valid URL.'
            ]
        ], json_decode($client->getResponse()->getContent(), true));

        // Check meta
        $client->request('POST', '/api/v1/meta', [
            'companyId' => $this->shop->getPartnerNumber(),
            'link' => 'https://metatags.io/'
        ]);
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertEquals([
            'title' => 'Meta Tags — Preview, Edit and Generate',
            'description' => 'With Meta Tags you can edit and experiment with your content then preview how your webpage will look on Google, Facebook, Twitter and more!',
            'type' => 'link',
            'url' => 'https://metatags.io/',
            'image' => 'https://metatags.io/assets/meta-tags-16a33a6a8531e519cc0936fbba0ad904e52d35f34a46c97a2c9f6f7dd7d336f2.png',
            'iframe' => null,
            'keywords' => [],
            'company' => 'metatags',
            'companyUrl' => 'https://metatags.io',
            'companyLogo' => 'https://metatags.io/assets/favicon-71ffc45e665afe1eb5447e86787278b28f56dfb39a6d53c8603d8477bdee7b6f.png'
        ], json_decode($client->getResponse()->getContent(), true));

        // Check meta (youtube)
        $client->request('POST', '/api/v1/meta', [
            'companyId' => $this->shop->getPartnerNumber(),
            'link' => 'https://www.youtube.com/watch?v=ScMzIvxBSi4'
        ]);
        $this->assertJson($client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent(), true);
        $this->assertEquals('Placeholder Video', $response['title']);
        $this->assertEquals('video', $response['type']);
        $html = '<!DOCTYPE html><html><body>'.$response['iframe'].'</body></html>';
        $crawler = new Crawler($html);
        $url = $crawler->filterXPath('descendant-or-self::body/iframe/@src')->text();
        $this->assertStringStartsWith('https://www.youtube.com/embed/ScMzIvxBSi4', $url);
    }
}
