<?php


namespace App\Service\Exchange;


interface CurrencyExchangeInterface
{
    public function exchange(string $fromAmount, string $fromCurrency, string $toCurrency);
}