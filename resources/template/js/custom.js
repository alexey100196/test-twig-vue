/*!
RETINA
 */

!function(){function t(){}function e(t){return r.retinaImageSuffix+t}function i(t,i){if(this.path=t||"","undefined"!=typeof i&&null!==i)this.at_2x_path=i,this.perform_check=!1;else{if(void 0!==document.createElement){var n=document.createElement("a");n.href=this.path,n.pathname=n.pathname.replace(o,e),this.at_2x_path=n.href}else{var a=this.path.split("?");a[0]=a[0].replace(o,e),this.at_2x_path=a.join("?")}this.perform_check=!0}}function n(t){this.el=t,this.path=new i(this.el.getAttribute("src"),this.el.getAttribute("data-at2x"));var e=this;this.path.check_2x_variant(function(t){t&&e.swap()})}var a="undefined"==typeof exports?window:exports,r={retinaImageSuffix:"@2x",check_mime_type:!0,force_original_dimensions:!0};a.Retina=t,t.configure=function(t){null===t&&(t={});for(var e in t)t.hasOwnProperty(e)&&(r[e]=t[e])},t.init=function(t){null===t&&(t=a);var e=t.onload||function(){};t.onload=function(){var t,i,a=document.getElementsByTagName("img"),r=[];for(t=0;t<a.length;t+=1)i=a[t],i.getAttributeNode("data-no-retina")||r.push(new n(i));e()}},t.isRetina=function(){var t="(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)";return a.devicePixelRatio>1?!0:a.matchMedia&&a.matchMedia(t).matches?!0:!1};var o=/\.\w+$/;a.RetinaImagePath=i,i.confirmed_paths=[],i.prototype.is_external=function(){return!(!this.path.match(/^https?\:/i)||this.path.match("//"+document.domain))},i.prototype.check_2x_variant=function(t){var e,n=this;return this.is_external()?t(!1):this.perform_check||"undefined"==typeof this.at_2x_path||null===this.at_2x_path?this.at_2x_path in i.confirmed_paths?t(!0):(e=new XMLHttpRequest,e.open("HEAD",this.at_2x_path),e.onreadystatechange=function(){if(4!==e.readyState)return t(!1);if(e.status>=200&&e.status<=399){if(r.check_mime_type){var a=e.getResponseHeader("Content-Type");if(null===a||!a.match(/^image/i))return t(!1)}return i.confirmed_paths.push(n.at_2x_path),t(!0)}return t(!1)},e.send(),void 0):t(!0)},a.RetinaImage=n,n.prototype.swap=function(t){function e(){i.el.complete?(r.force_original_dimensions&&(i.el.setAttribute("width",i.el.offsetWidth),i.el.setAttribute("height",i.el.offsetHeight)),i.el.setAttribute("src",t)):setTimeout(e,5)}"undefined"==typeof t&&(t=this.path.at_2x_path);var i=this;e()},t.isRetina()&&t.init(a)}();

/*!
ANIMATIONS
 */
(function(){var t,e,n,i,o,r=function(t,e){return function(){return t.apply(e,arguments)}},s=[].indexOf||function(t){for(var e=0,n=this.length;n>e;e++)if(e in this&&this[e]===t)return e;return-1};e=function(){function t(){}return t.prototype.extend=function(t,e){var n,i;for(n in e)i=e[n],null==t[n]&&(t[n]=i);return t},t.prototype.isMobile=function(t){return/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(t)},t.prototype.createEvent=function(t,e,n,i){var o;return null==e&&(e=!1),null==n&&(n=!1),null==i&&(i=null),null!=document.createEvent?(o=document.createEvent("CustomEvent"),o.initCustomEvent(t,e,n,i)):null!=document.createEventObject?(o=document.createEventObject(),o.eventType=t):o.eventName=t,o},t.prototype.emitEvent=function(t,e){return null!=t.dispatchEvent?t.dispatchEvent(e):e in(null!=t)?t[e]():"on"+e in(null!=t)?t["on"+e]():void 0},t.prototype.addEvent=function(t,e,n){return null!=t.addEventListener?t.addEventListener(e,n,!1):null!=t.attachEvent?t.attachEvent("on"+e,n):t[e]=n},t.prototype.removeEvent=function(t,e,n){return null!=t.removeEventListener?t.removeEventListener(e,n,!1):null!=t.detachEvent?t.detachEvent("on"+e,n):delete t[e]},t.prototype.innerHeight=function(){return"innerHeight"in window?window.innerHeight:document.documentElement.clientHeight},t}(),n=this.WeakMap||this.MozWeakMap||(n=function(){function t(){this.keys=[],this.values=[]}return t.prototype.get=function(t){var e,n,i,o,r;for(r=this.keys,e=i=0,o=r.length;o>i;e=++i)if(n=r[e],n===t)return this.values[e]},t.prototype.set=function(t,e){var n,i,o,r,s;for(s=this.keys,n=o=0,r=s.length;r>o;n=++o)if(i=s[n],i===t)return void(this.values[n]=e);return this.keys.push(t),this.values.push(e)},t}()),t=this.MutationObserver||this.WebkitMutationObserver||this.MozMutationObserver||(t=function(){function t(){"undefined"!=typeof console&&null!==console&&console.warn("MutationObserver is not supported by your browser."),"undefined"!=typeof console&&null!==console&&console.warn("WOW.js cannot detect dom mutations, please call .sync() after loading new content.")}return t.notSupported=!0,t.prototype.observe=function(){},t}()),i=this.getComputedStyle||function(t,e){return this.getPropertyValue=function(e){var n;return"float"===e&&(e="styleFloat"),o.test(e)&&e.replace(o,function(t,e){return e.toUpperCase()}),(null!=(n=t.currentStyle)?n[e]:void 0)||null},this},o=/(\-([a-z]){1})/g,this.WOW=function(){function o(t){null==t&&(t={}),this.scrollCallback=r(this.scrollCallback,this),this.scrollHandler=r(this.scrollHandler,this),this.resetAnimation=r(this.resetAnimation,this),this.start=r(this.start,this),this.scrolled=!0,this.config=this.util().extend(t,this.defaults),this.animationNameCache=new n,this.wowEvent=this.util().createEvent(this.config.boxClass)}return o.prototype.defaults={boxClass:"wow",animateClass:"animated",offset:0,mobile:!0,live:!0,callback:null},o.prototype.init=function(){var t;return this.element=window.document.documentElement,"interactive"===(t=document.readyState)||"complete"===t?this.start():this.util().addEvent(document,"DOMContentLoaded",this.start),this.finished=[]},o.prototype.start=function(){var e,n,i,o;if(this.stopped=!1,this.boxes=function(){var t,n,i,o;for(i=this.element.querySelectorAll("."+this.config.boxClass),o=[],t=0,n=i.length;n>t;t++)e=i[t],o.push(e);return o}.call(this),this.all=function(){var t,n,i,o;for(i=this.boxes,o=[],t=0,n=i.length;n>t;t++)e=i[t],o.push(e);return o}.call(this),this.boxes.length)if(this.disabled())this.resetStyle();else for(o=this.boxes,n=0,i=o.length;i>n;n++)e=o[n],this.applyStyle(e,!0);return this.disabled()||(this.util().addEvent(window,"scroll",this.scrollHandler),this.util().addEvent(window,"resize",this.scrollHandler),this.interval=setInterval(this.scrollCallback,50)),this.config.live?new t(function(t){return function(e){var n,i,o,r,s;for(s=[],n=0,i=e.length;i>n;n++)r=e[n],s.push(function(){var t,e,n,i;for(n=r.addedNodes||[],i=[],t=0,e=n.length;e>t;t++)o=n[t],i.push(this.doSync(o));return i}.call(t));return s}}(this)).observe(document.body,{childList:!0,subtree:!0}):void 0},o.prototype.stop=function(){return this.stopped=!0,this.util().removeEvent(window,"scroll",this.scrollHandler),this.util().removeEvent(window,"resize",this.scrollHandler),null!=this.interval?clearInterval(this.interval):void 0},o.prototype.sync=function(e){return t.notSupported?this.doSync(this.element):void 0},o.prototype.doSync=function(t){var e,n,i,o,r;if(null==t&&(t=this.element),1===t.nodeType){for(t=t.parentNode||t,o=t.querySelectorAll("."+this.config.boxClass),r=[],n=0,i=o.length;i>n;n++)e=o[n],s.call(this.all,e)<0?(this.boxes.push(e),this.all.push(e),this.stopped||this.disabled()?this.resetStyle():this.applyStyle(e,!0),r.push(this.scrolled=!0)):r.push(void 0);return r}},o.prototype.show=function(t){return this.applyStyle(t),t.className=t.className+" "+this.config.animateClass,null!=this.config.callback&&this.config.callback(t),this.util().emitEvent(t,this.wowEvent),this.util().addEvent(t,"animationend",this.resetAnimation),this.util().addEvent(t,"oanimationend",this.resetAnimation),this.util().addEvent(t,"webkitAnimationEnd",this.resetAnimation),this.util().addEvent(t,"MSAnimationEnd",this.resetAnimation),t},o.prototype.applyStyle=function(t,e){var n,i,o;return i=t.getAttribute("data-wow-duration"),n=t.getAttribute("data-wow-delay"),o=t.getAttribute("data-wow-iteration"),this.animate(function(r){return function(){return r.customStyle(t,e,i,n,o)}}(this))},o.prototype.animate=function(){return"requestAnimationFrame"in window?function(t){return window.requestAnimationFrame(t)}:function(t){return t()}}(),o.prototype.resetStyle=function(){var t,e,n,i,o;for(i=this.boxes,o=[],e=0,n=i.length;n>e;e++)t=i[e],o.push(t.style.visibility="visible");return o},o.prototype.resetAnimation=function(t){var e;return t.type.toLowerCase().indexOf("animationend")>=0?(e=t.target||t.srcElement,e.className=e.className.replace(this.config.animateClass,"").trim()):void 0},o.prototype.customStyle=function(t,e,n,i,o){return e&&this.cacheAnimationName(t),t.style.visibility=e?"hidden":"visible",n&&this.vendorSet(t.style,{animationDuration:n}),i&&this.vendorSet(t.style,{animationDelay:i}),o&&this.vendorSet(t.style,{animationIterationCount:o}),this.vendorSet(t.style,{animationName:e?"none":this.cachedAnimationName(t)}),t},o.prototype.vendors=["moz","webkit"],o.prototype.vendorSet=function(t,e){var n,i,o,r;i=[];for(n in e)o=e[n],t[""+n]=o,i.push(function(){var e,i,s,l;for(s=this.vendors,l=[],e=0,i=s.length;i>e;e++)r=s[e],l.push(t[""+r+n.charAt(0).toUpperCase()+n.substr(1)]=o);return l}.call(this));return i},o.prototype.vendorCSS=function(t,e){var n,o,r,s,l,a;for(l=i(t),s=l.getPropertyCSSValue(e),r=this.vendors,n=0,o=r.length;o>n;n++)a=r[n],s=s||l.getPropertyCSSValue("-"+a+"-"+e);return s},o.prototype.animationName=function(t){var e;try{e=this.vendorCSS(t,"animation-name").cssText}catch(n){e=i(t).getPropertyValue("animation-name")}return"none"===e?"":e},o.prototype.cacheAnimationName=function(t){return this.animationNameCache.set(t,this.animationName(t))},o.prototype.cachedAnimationName=function(t){return this.animationNameCache.get(t)},o.prototype.scrollHandler=function(){return this.scrolled=!0},o.prototype.scrollCallback=function(){var t;return!this.scrolled||(this.scrolled=!1,this.boxes=function(){var e,n,i,o;for(i=this.boxes,o=[],e=0,n=i.length;n>e;e++)t=i[e],t&&(this.isVisible(t)?this.show(t):o.push(t));return o}.call(this),this.boxes.length||this.config.live)?void 0:this.stop()},o.prototype.offsetTop=function(t){for(var e;void 0===t.offsetTop;)t=t.parentNode;for(e=t.offsetTop;t=t.offsetParent;)e+=t.offsetTop;return e},o.prototype.isVisible=function(t){var e,n,i,o,r;return n=t.getAttribute("data-wow-offset")||this.config.offset,r=window.pageYOffset,o=r+Math.min(this.element.clientHeight,this.util().innerHeight())-n,i=this.offsetTop(t),e=i+t.clientHeight,o>=i&&e>=r},o.prototype.util=function(){return null!=this._util?this._util:this._util=new e},o.prototype.disabled=function(){return!this.config.mobile&&this.util().isMobile(navigator.userAgent)},o}()}).call(this),wow=new WOW({boxClass:"wow",animateClass:"animated",offset:0,mobile:!1,live:!0}),wow.init();

new WOW({
	boxClass:     'wow',      // default
	animateClass: 'animated', // default
	offset:       0,          // default
	mobile:       false,       // default
	live:         true        // default
}).init();

/*!
BACK TOP
 */
jQuery(window).scroll(function(){
	if (jQuery(this).scrollTop() > 1) {
		jQuery('.topbutton').css({bottom:"25px"});
	} else {
		jQuery('.topbutton').css({bottom:"-100px"});
	}
});
jQuery('.topbutton').click(function(){
	jQuery('html, body').animate({scrollTop: '0px'}, 800);
	return false;
});

/*!
SELECT DROPDOWN
 */
!function(a,b){"function"==typeof define&&define.amd?define(["jquery"],function(a){return b(a)}):"object"==typeof exports?module.exports=b(require("jquery")):b(jQuery)}(this,function(a){!function(a){"use strict";function b(b){var c=[{re:/[\xC0-\xC6]/g,ch:"A"},{re:/[\xE0-\xE6]/g,ch:"a"},{re:/[\xC8-\xCB]/g,ch:"E"},{re:/[\xE8-\xEB]/g,ch:"e"},{re:/[\xCC-\xCF]/g,ch:"I"},{re:/[\xEC-\xEF]/g,ch:"i"},{re:/[\xD2-\xD6]/g,ch:"O"},{re:/[\xF2-\xF6]/g,ch:"o"},{re:/[\xD9-\xDC]/g,ch:"U"},{re:/[\xF9-\xFC]/g,ch:"u"},{re:/[\xC7-\xE7]/g,ch:"c"},{re:/[\xD1]/g,ch:"N"},{re:/[\xF1]/g,ch:"n"}];return a.each(c,function(){b=b.replace(this.re,this.ch)}),b}function c(a){var b={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&#x27;","`":"&#x60;"},c="(?:"+Object.keys(b).join("|")+")",d=new RegExp(c),e=new RegExp(c,"g"),f=null==a?"":""+a;return d.test(f)?f.replace(e,function(a){return b[a]}):f}function d(b,c){var d=arguments,f=b,g=c;[].shift.apply(d);var h,i=this.each(function(){var b=a(this);if(b.is("select")){var c=b.data("selectpicker"),i="object"==typeof f&&f;if(c){if(i)for(var j in i)i.hasOwnProperty(j)&&(c.options[j]=i[j])}else{var k=a.extend({},e.DEFAULTS,a.fn.selectpicker.defaults||{},b.data(),i);k.template=a.extend({},e.DEFAULTS.template,a.fn.selectpicker.defaults?a.fn.selectpicker.defaults.template:{},b.data().template,i.template),b.data("selectpicker",c=new e(this,k,g))}"string"==typeof f&&(h=c[f]instanceof Function?c[f].apply(c,d):c.options[f])}});return"undefined"!=typeof h?h:i}String.prototype.includes||!function(){var a={}.toString,b=function(){try{var a={},b=Object.defineProperty,c=b(a,a,a)&&b}catch(d){}return c}(),c="".indexOf,d=function(b){if(null==this)throw new TypeError;var d=String(this);if(b&&"[object RegExp]"==a.call(b))throw new TypeError;var e=d.length,f=String(b),g=f.length,h=arguments.length>1?arguments[1]:void 0,i=h?Number(h):0;i!=i&&(i=0);var j=Math.min(Math.max(i,0),e);return g+j>e?!1:-1!=c.call(d,f,i)};b?b(String.prototype,"includes",{value:d,configurable:!0,writable:!0}):String.prototype.includes=d}(),String.prototype.startsWith||!function(){var a=function(){try{var a={},b=Object.defineProperty,c=b(a,a,a)&&b}catch(d){}return c}(),b={}.toString,c=function(a){if(null==this)throw new TypeError;var c=String(this);if(a&&"[object RegExp]"==b.call(a))throw new TypeError;var d=c.length,e=String(a),f=e.length,g=arguments.length>1?arguments[1]:void 0,h=g?Number(g):0;h!=h&&(h=0);var i=Math.min(Math.max(h,0),d);if(f+i>d)return!1;for(var j=-1;++j<f;)if(c.charCodeAt(i+j)!=e.charCodeAt(j))return!1;return!0};a?a(String.prototype,"startsWith",{value:c,configurable:!0,writable:!0}):String.prototype.startsWith=c}(),Object.keys||(Object.keys=function(a,b,c){c=[];for(b in a)c.hasOwnProperty.call(a,b)&&c.push(b);return c}),a.fn.triggerNative=function(a){var b,c=this[0];c.dispatchEvent?("function"==typeof Event?b=new Event(a,{bubbles:!0}):(b=document.createEvent("Event"),b.initEvent(a,!0,!1)),c.dispatchEvent(b)):(c.fireEvent&&(b=document.createEventObject(),b.eventType=a,c.fireEvent("on"+a,b)),this.trigger(a))},a.expr[":"].icontains=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.text()).toUpperCase();return f.includes(d[3].toUpperCase())},a.expr[":"].ibegins=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.text()).toUpperCase();return f.startsWith(d[3].toUpperCase())},a.expr[":"].aicontains=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.data("normalizedText")||e.text()).toUpperCase();return f.includes(d[3].toUpperCase())},a.expr[":"].aibegins=function(b,c,d){var e=a(b),f=(e.data("tokens")||e.data("normalizedText")||e.text()).toUpperCase();return f.startsWith(d[3].toUpperCase())};var e=function(b,c,d){d&&(d.stopPropagation(),d.preventDefault()),this.$element=a(b),this.$newElement=null,this.$button=null,this.$menu=null,this.$lis=null,this.options=c,null===this.options.title&&(this.options.title=this.$element.attr("title")),this.val=e.prototype.val,this.render=e.prototype.render,this.refresh=e.prototype.refresh,this.setStyle=e.prototype.setStyle,this.selectAll=e.prototype.selectAll,this.deselectAll=e.prototype.deselectAll,this.destroy=e.prototype.destroy,this.remove=e.prototype.remove,this.show=e.prototype.show,this.hide=e.prototype.hide,this.init()};e.VERSION="1.9.4",e.DEFAULTS={noneSelectedText:"Nothing selected",noneResultsText:"No results matched {0}",countSelectedText:function(a,b){return 1==a?"{0} item selected":"{0} items selected"},maxOptionsText:function(a,b){return[1==a?"Limit reached ({n} item max)":"Limit reached ({n} items max)",1==b?"Group limit reached ({n} item max)":"Group limit reached ({n} items max)"]},selectAllText:"Select All",deselectAllText:"Deselect All",doneButton:!1,doneButtonText:"Close",multipleSeparator:", ",styleBase:"btn",style:"btn-default",size:"auto",title:null,selectedTextFormat:"values",width:!1,container:!1,hideDisabled:!1,showSubtext:!1,showIcon:!0,showContent:!0,dropupAuto:!0,header:!1,liveSearch:!1,liveSearchPlaceholder:null,liveSearchNormalize:!1,liveSearchStyle:"contains",actionsBox:!1,iconBase:"glyphicon",tickIcon:"glyphicon-ok",template:{caret:'<span class="caret"></span>'},maxOptions:!1,mobile:!1,selectOnTab:!1,dropdownAlignRight:!1},e.prototype={constructor:e,init:function(){var b=this,c=this.$element.attr("id");this.liObj={},this.multiple=this.$element.prop("multiple"),this.autofocus=this.$element.prop("autofocus"),this.$newElement=this.createView(),this.$element.after(this.$newElement).appendTo(this.$newElement),this.$button=this.$newElement.children("button"),this.$menu=this.$newElement.children(".dropdown-menu"),this.$menuInner=this.$menu.children(".inner"),this.$searchbox=this.$menu.find("input"),this.options.dropdownAlignRight&&this.$menu.addClass("dropdown-menu-right"),"undefined"!=typeof c&&(this.$button.attr("data-id",c),a('label[for="'+c+'"]').click(function(a){a.preventDefault(),b.$button.focus()})),this.checkDisabled(),this.clickListener(),this.options.liveSearch&&this.liveSearchListener(),this.render(),this.setStyle(),this.setWidth(),this.options.container&&this.selectPosition(),this.$menu.data("this",this),this.$newElement.data("this",this),this.options.mobile&&this.mobile(),this.$newElement.on({"hide.bs.dropdown":function(a){b.$element.trigger("hide.bs.select",a)},"hidden.bs.dropdown":function(a){b.$element.trigger("hidden.bs.select",a)},"show.bs.dropdown":function(a){b.$element.trigger("show.bs.select",a)},"shown.bs.dropdown":function(a){b.$element.trigger("shown.bs.select",a)}}),b.$element[0].hasAttribute("required")&&this.$element.on("invalid",function(){b.$button.addClass("bs-invalid").focus(),b.$element.on({"focus.bs.select":function(){b.$button.focus(),b.$element.off("focus.bs.select")},"shown.bs.select":function(){b.$element.val(b.$element.val()).off("shown.bs.select")},"rendered.bs.select":function(){this.validity.valid&&b.$button.removeClass("bs-invalid"),b.$element.off("rendered.bs.select")}})}),setTimeout(function(){b.$element.trigger("loaded.bs.select")})},createDropdown:function(){var b=this.multiple?" show-tick":"",d=this.$element.parent().hasClass("input-group")?" input-group-btn":"",e=this.autofocus?" autofocus":"",f=this.options.header?'<div class="popover-title"><button type="button" class="close" aria-hidden="true">&times;</button>'+this.options.header+"</div>":"",g=this.options.liveSearch?'<div class="bs-searchbox"><input type="text" class="form-control" autocomplete="off"'+(null===this.options.liveSearchPlaceholder?"":' placeholder="'+c(this.options.liveSearchPlaceholder)+'"')+"></div>":"",h=this.multiple&&this.options.actionsBox?'<div class="bs-actionsbox"><div class="btn-group btn-group-sm btn-block"><button type="button" class="actions-btn bs-select-all btn btn-default">'+this.options.selectAllText+'</button><button type="button" class="actions-btn bs-deselect-all btn btn-default">'+this.options.deselectAllText+"</button></div></div>":"",i=this.multiple&&this.options.doneButton?'<div class="bs-donebutton"><div class="btn-group btn-block"><button type="button" class="btn btn-sm btn-default">'+this.options.doneButtonText+"</button></div></div>":"",j='<div class="btn-group bootstrap-select'+b+d+'"><button type="button" class="'+this.options.styleBase+' dropdown-toggle" data-toggle="dropdown"'+e+'><span class="filter-option pull-left"></span>&nbsp;<span class="bs-caret">'+this.options.template.caret+'</span></button><div class="dropdown-menu open">'+f+g+h+'<ul class="dropdown-menu inner" role="menu"></ul>'+i+"</div></div>";return a(j)},createView:function(){var a=this.createDropdown(),b=this.createLi();return a.find("ul")[0].innerHTML=b,a},reloadLi:function(){this.destroyLi();var a=this.createLi();this.$menuInner[0].innerHTML=a},destroyLi:function(){this.$menu.find("li").remove()},createLi:function(){var d=this,e=[],f=0,g=document.createElement("option"),h=-1,i=function(a,b,c,d){return"<li"+("undefined"!=typeof c&""!==c?' class="'+c+'"':"")+("undefined"!=typeof b&null!==b?' data-original-index="'+b+'"':"")+("undefined"!=typeof d&null!==d?'data-optgroup="'+d+'"':"")+">"+a+"</li>"},j=function(a,e,f,g){return'<a tabindex="0"'+("undefined"!=typeof e?' class="'+e+'"':"")+("undefined"!=typeof f?' style="'+f+'"':"")+(d.options.liveSearchNormalize?' data-normalized-text="'+b(c(a))+'"':"")+("undefined"!=typeof g||null!==g?' data-tokens="'+g+'"':"")+">"+a+'<span class="'+d.options.iconBase+" "+d.options.tickIcon+' check-mark"></span></a>'};if(this.options.title&&!this.multiple&&(h--,!this.$element.find(".bs-title-option").length)){var k=this.$element[0];g.className="bs-title-option",g.appendChild(document.createTextNode(this.options.title)),g.value="",k.insertBefore(g,k.firstChild),void 0===a(k.options[k.selectedIndex]).attr("selected")&&(g.selected=!0)}return this.$element.find("option").each(function(b){var c=a(this);if(h++,!c.hasClass("bs-title-option")){var g=this.className||"",k=this.style.cssText,l=c.data("content")?c.data("content"):c.html(),m=c.data("tokens")?c.data("tokens"):null,n="undefined"!=typeof c.data("subtext")?'<small class="text-muted">'+c.data("subtext")+"</small>":"",o="undefined"!=typeof c.data("icon")?'<span class="'+d.options.iconBase+" "+c.data("icon")+'"></span> ':"",p="OPTGROUP"===this.parentNode.tagName,q=this.disabled||p&&this.parentNode.disabled;if(""!==o&&q&&(o="<span>"+o+"</span>"),d.options.hideDisabled&&q&&!p)return void h--;if(c.data("content")||(l=o+'<span class="text">'+l+n+"</span>"),p&&c.data("divider")!==!0){var r=" "+this.parentNode.className||"";if(0===c.index()){f+=1;var s=this.parentNode.label,t="undefined"!=typeof c.parent().data("subtext")?'<small class="text-muted">'+c.parent().data("subtext")+"</small>":"",u=c.parent().data("icon")?'<span class="'+d.options.iconBase+" "+c.parent().data("icon")+'"></span> ':"";s=u+'<span class="text">'+s+t+"</span>",0!==b&&e.length>0&&(h++,e.push(i("",null,"divider",f+"div"))),h++,e.push(i(s,null,"dropdown-header"+r,f))}if(d.options.hideDisabled&&q)return void h--;e.push(i(j(l,"opt "+g+r,k,m),b,"",f))}else c.data("divider")===!0?e.push(i("",b,"divider")):c.data("hidden")===!0?e.push(i(j(l,g,k,m),b,"hidden is-hidden")):(this.previousElementSibling&&"OPTGROUP"===this.previousElementSibling.tagName&&(h++,e.push(i("",null,"divider",f+"div"))),e.push(i(j(l,g,k,m),b)));d.liObj[b]=h}}),this.multiple||0!==this.$element.find("option:selected").length||this.options.title||this.$element.find("option").eq(0).prop("selected",!0).attr("selected","selected"),e.join("")},findLis:function(){return null==this.$lis&&(this.$lis=this.$menu.find("li")),this.$lis},render:function(b){var c,d=this;b!==!1&&this.$element.find("option").each(function(a){var b=d.findLis().eq(d.liObj[a]);d.setDisabled(a,this.disabled||"OPTGROUP"===this.parentNode.tagName&&this.parentNode.disabled,b),d.setSelected(a,this.selected,b)}),this.tabIndex();var e=this.$element.find("option").map(function(){if(this.selected){if(d.options.hideDisabled&&(this.disabled||"OPTGROUP"===this.parentNode.tagName&&this.parentNode.disabled))return;var b,c=a(this),e=c.data("icon")&&d.options.showIcon?'<i class="'+d.options.iconBase+" "+c.data("icon")+'"></i> ':"";return b=d.options.showSubtext&&c.data("subtext")&&!d.multiple?' <small class="text-muted">'+c.data("subtext")+"</small>":"","undefined"!=typeof c.attr("title")?c.attr("title"):c.data("content")&&d.options.showContent?c.data("content"):e+c.html()+b}}).toArray(),f=this.multiple?e.join(this.options.multipleSeparator):e[0];if(this.multiple&&this.options.selectedTextFormat.indexOf("count")>-1){var g=this.options.selectedTextFormat.split(">");if(g.length>1&&e.length>g[1]||1==g.length&&e.length>=2){c=this.options.hideDisabled?", [disabled]":"";var h=this.$element.find("option").not('[data-divider="true"], [data-hidden="true"]'+c).length,i="function"==typeof this.options.countSelectedText?this.options.countSelectedText(e.length,h):this.options.countSelectedText;f=i.replace("{0}",e.length.toString()).replace("{1}",h.toString())}}void 0==this.options.title&&(this.options.title=this.$element.attr("title")),"static"==this.options.selectedTextFormat&&(f=this.options.title),f||(f="undefined"!=typeof this.options.title?this.options.title:this.options.noneSelectedText),this.$button.attr("title",a.trim(f.replace(/<[^>]*>?/g,""))),this.$button.children(".filter-option").html(f),this.$element.trigger("rendered.bs.select")},setStyle:function(a,b){this.$element.attr("class")&&this.$newElement.addClass(this.$element.attr("class").replace(/selectpicker|mobile-device|bs-select-hidden|validate\[.*\]/gi,""));var c=a?a:this.options.style;"add"==b?this.$button.addClass(c):"remove"==b?this.$button.removeClass(c):(this.$button.removeClass(this.options.style),this.$button.addClass(c))},liHeight:function(b){if(b||this.options.size!==!1&&!this.sizeInfo){var c=document.createElement("div"),d=document.createElement("div"),e=document.createElement("ul"),f=document.createElement("li"),g=document.createElement("li"),h=document.createElement("a"),i=document.createElement("span"),j=this.options.header&&this.$menu.find(".popover-title").length>0?this.$menu.find(".popover-title")[0].cloneNode(!0):null,k=this.options.liveSearch?document.createElement("div"):null,l=this.options.actionsBox&&this.multiple&&this.$menu.find(".bs-actionsbox").length>0?this.$menu.find(".bs-actionsbox")[0].cloneNode(!0):null,m=this.options.doneButton&&this.multiple&&this.$menu.find(".bs-donebutton").length>0?this.$menu.find(".bs-donebutton")[0].cloneNode(!0):null;if(i.className="text",c.className=this.$menu[0].parentNode.className+" open",d.className="dropdown-menu open",e.className="dropdown-menu inner",f.className="divider",i.appendChild(document.createTextNode("Inner text")),h.appendChild(i),g.appendChild(h),e.appendChild(g),e.appendChild(f),j&&d.appendChild(j),k){var n=document.createElement("span");k.className="bs-searchbox",n.className="form-control",k.appendChild(n),d.appendChild(k)}l&&d.appendChild(l),d.appendChild(e),m&&d.appendChild(m),c.appendChild(d),document.body.appendChild(c);var o=h.offsetHeight,p=j?j.offsetHeight:0,q=k?k.offsetHeight:0,r=l?l.offsetHeight:0,s=m?m.offsetHeight:0,t=a(f).outerHeight(!0),u="function"==typeof getComputedStyle?getComputedStyle(d):!1,v=u?null:a(d),w=parseInt(u?u.paddingTop:v.css("paddingTop"))+parseInt(u?u.paddingBottom:v.css("paddingBottom"))+parseInt(u?u.borderTopWidth:v.css("borderTopWidth"))+parseInt(u?u.borderBottomWidth:v.css("borderBottomWidth")),x=w+parseInt(u?u.marginTop:v.css("marginTop"))+parseInt(u?u.marginBottom:v.css("marginBottom"))+2;document.body.removeChild(c),this.sizeInfo={liHeight:o,headerHeight:p,searchHeight:q,actionsHeight:r,doneButtonHeight:s,dividerHeight:t,menuPadding:w,menuExtras:x}}},setSize:function(){if(this.findLis(),this.liHeight(),this.options.header&&this.$menu.css("padding-top",0),this.options.size!==!1){var b,c,d,e,f=this,g=this.$menu,h=this.$menuInner,i=a(window),j=this.$newElement[0].offsetHeight,k=this.sizeInfo.liHeight,l=this.sizeInfo.headerHeight,m=this.sizeInfo.searchHeight,n=this.sizeInfo.actionsHeight,o=this.sizeInfo.doneButtonHeight,p=this.sizeInfo.dividerHeight,q=this.sizeInfo.menuPadding,r=this.sizeInfo.menuExtras,s=this.options.hideDisabled?".disabled":"",t=function(){d=f.$newElement.offset().top-i.scrollTop(),e=i.height()-d-j};if(t(),"auto"===this.options.size){var u=function(){var i,j=function(b,c){return function(d){return c?d.classList?d.classList.contains(b):a(d).hasClass(b):!(d.classList?d.classList.contains(b):a(d).hasClass(b))}},p=f.$menuInner[0].getElementsByTagName("li"),s=Array.prototype.filter?Array.prototype.filter.call(p,j("hidden",!1)):f.$lis.not(".hidden"),u=Array.prototype.filter?Array.prototype.filter.call(s,j("dropdown-header",!0)):s.filter(".dropdown-header");t(),b=e-r,f.options.container?(g.data("height")||g.data("height",g.height()),c=g.data("height")):c=g.height(),f.options.dropupAuto&&f.$newElement.toggleClass("dropup",d>e&&c>b-r),f.$newElement.hasClass("dropup")&&(b=d-r),i=s.length+u.length>3?3*k+r-2:0,g.css({"max-height":b+"px",overflow:"hidden","min-height":i+l+m+n+o+"px"}),h.css({"max-height":b-l-m-n-o-q+"px","overflow-y":"auto","min-height":Math.max(i-q,0)+"px"})};u(),this.$searchbox.off("input.getSize propertychange.getSize").on("input.getSize propertychange.getSize",u),i.off("resize.getSize scroll.getSize").on("resize.getSize scroll.getSize",u)}else if(this.options.size&&"auto"!=this.options.size&&this.$lis.not(s).length>this.options.size){var v=this.$lis.not(".divider").not(s).children().slice(0,this.options.size).last().parent().index(),w=this.$lis.slice(0,v+1).filter(".divider").length;b=k*this.options.size+w*p+q,f.options.container?(g.data("height")||g.data("height",g.height()),c=g.data("height")):c=g.height(),f.options.dropupAuto&&this.$newElement.toggleClass("dropup",d>e&&c>b-r),g.css({"max-height":b+l+m+n+o+"px",overflow:"hidden","min-height":""}),h.css({"max-height":b-q+"px","overflow-y":"auto","min-height":""})}}},setWidth:function(){if("auto"===this.options.width){this.$menu.css("min-width","0");var a=this.$menu.parent().clone().appendTo("body"),b=this.options.container?this.$newElement.clone().appendTo("body"):a,c=a.children(".dropdown-menu").outerWidth(),d=b.css("width","auto").children("button").outerWidth();a.remove(),b.remove(),this.$newElement.css("width",Math.max(c,d)+"px")}else"fit"===this.options.width?(this.$menu.css("min-width",""),this.$newElement.css("width","").addClass("fit-width")):this.options.width?(this.$menu.css("min-width",""),this.$newElement.css("width",this.options.width)):(this.$menu.css("min-width",""),this.$newElement.css("width",""));this.$newElement.hasClass("fit-width")&&"fit"!==this.options.width&&this.$newElement.removeClass("fit-width")},selectPosition:function(){this.$bsContainer=a('<div class="bs-container" />');var b,c,d=this,e=function(a){d.$bsContainer.addClass(a.attr("class").replace(/form-control|fit-width/gi,"")).toggleClass("dropup",a.hasClass("dropup")),b=a.offset(),c=a.hasClass("dropup")?0:a[0].offsetHeight,d.$bsContainer.css({top:b.top+c,left:b.left,width:a[0].offsetWidth})};this.$button.on("click",function(){var b=a(this);d.isDisabled()||(e(d.$newElement),d.$bsContainer.appendTo(d.options.container).toggleClass("open",!b.hasClass("open")).append(d.$menu))}),a(window).on("resize scroll",function(){e(d.$newElement)}),this.$element.on("hide.bs.select",function(){d.$menu.data("height",d.$menu.height()),d.$bsContainer.detach()})},setSelected:function(a,b,c){c||(c=this.findLis().eq(this.liObj[a])),c.toggleClass("selected",b)},setDisabled:function(a,b,c){c||(c=this.findLis().eq(this.liObj[a])),b?c.addClass("disabled").children("a").attr("href","#").attr("tabindex",-1):c.removeClass("disabled").children("a").removeAttr("href").attr("tabindex",0)},isDisabled:function(){return this.$element[0].disabled},checkDisabled:function(){var a=this;this.isDisabled()?(this.$newElement.addClass("disabled"),this.$button.addClass("disabled").attr("tabindex",-1)):(this.$button.hasClass("disabled")&&(this.$newElement.removeClass("disabled"),this.$button.removeClass("disabled")),-1!=this.$button.attr("tabindex")||this.$element.data("tabindex")||this.$button.removeAttr("tabindex")),this.$button.click(function(){return!a.isDisabled()})},tabIndex:function(){this.$element.data("tabindex")!==this.$element.attr("tabindex")&&-98!==this.$element.attr("tabindex")&&"-98"!==this.$element.attr("tabindex")&&(this.$element.data("tabindex",this.$element.attr("tabindex")),this.$button.attr("tabindex",this.$element.data("tabindex"))),this.$element.attr("tabindex",-98)},clickListener:function(){var b=this,c=a(document);this.$newElement.on("touchstart.dropdown",".dropdown-menu",function(a){a.stopPropagation()}),c.data("spaceSelect",!1),this.$button.on("keyup",function(a){/(32)/.test(a.keyCode.toString(10))&&c.data("spaceSelect")&&(a.preventDefault(),c.data("spaceSelect",!1))}),this.$button.on("click",function(){b.setSize(),b.$element.on("shown.bs.select",function(){if(b.options.liveSearch||b.multiple){if(!b.multiple){var a=b.liObj[b.$element[0].selectedIndex];if("number"!=typeof a||b.options.size===!1)return;var c=b.$lis.eq(a)[0].offsetTop-b.$menuInner[0].offsetTop;c=c-b.$menuInner[0].offsetHeight/2+b.sizeInfo.liHeight/2,b.$menuInner[0].scrollTop=c}}else b.$menuInner.find(".selected a").focus()})}),this.$menuInner.on("click","li a",function(c){var d=a(this),e=d.parent().data("originalIndex"),f=b.$element.val(),g=b.$element.prop("selectedIndex");if(b.multiple&&c.stopPropagation(),c.preventDefault(),!b.isDisabled()&&!d.parent().hasClass("disabled")){var h=b.$element.find("option"),i=h.eq(e),j=i.prop("selected"),k=i.parent("optgroup"),l=b.options.maxOptions,m=k.data("maxOptions")||!1;if(b.multiple){if(i.prop("selected",!j),b.setSelected(e,!j),d.blur(),l!==!1||m!==!1){var n=l<h.filter(":selected").length,o=m<k.find("option:selected").length;if(l&&n||m&&o)if(l&&1==l)h.prop("selected",!1),i.prop("selected",!0),b.$menuInner.find(".selected").removeClass("selected"),b.setSelected(e,!0);else if(m&&1==m){k.find("option:selected").prop("selected",!1),i.prop("selected",!0);var p=d.parent().data("optgroup");b.$menuInner.find('[data-optgroup="'+p+'"]').removeClass("selected"),b.setSelected(e,!0)}else{var q="function"==typeof b.options.maxOptionsText?b.options.maxOptionsText(l,m):b.options.maxOptionsText,r=q[0].replace("{n}",l),s=q[1].replace("{n}",m),t=a('<div class="notify"></div>');q[2]&&(r=r.replace("{var}",q[2][l>1?0:1]),s=s.replace("{var}",q[2][m>1?0:1])),i.prop("selected",!1),b.$menu.append(t),l&&n&&(t.append(a("<div>"+r+"</div>")),b.$element.trigger("maxReached.bs.select")),m&&o&&(t.append(a("<div>"+s+"</div>")),b.$element.trigger("maxReachedGrp.bs.select")),setTimeout(function(){b.setSelected(e,!1)},10),t.delay(750).fadeOut(300,function(){a(this).remove()})}}}else h.prop("selected",!1),i.prop("selected",!0),b.$menuInner.find(".selected").removeClass("selected"),b.setSelected(e,!0);b.multiple?b.options.liveSearch&&b.$searchbox.focus():b.$button.focus(),(f!=b.$element.val()&&b.multiple||g!=b.$element.prop("selectedIndex")&&!b.multiple)&&(b.$element.triggerNative("change"),b.$element.trigger("changed.bs.select",[e,i.prop("selected"),j]))}}),this.$menu.on("click","li.disabled a, .popover-title, .popover-title :not(.close)",function(c){c.currentTarget==this&&(c.preventDefault(),c.stopPropagation(),b.options.liveSearch&&!a(c.target).hasClass("close")?b.$searchbox.focus():b.$button.focus())}),this.$menuInner.on("click",".divider, .dropdown-header",function(a){a.preventDefault(),a.stopPropagation(),b.options.liveSearch?b.$searchbox.focus():b.$button.focus()}),this.$menu.on("click",".popover-title .close",function(){b.$button.click()}),this.$searchbox.on("click",function(a){a.stopPropagation()}),this.$menu.on("click",".actions-btn",function(c){b.options.liveSearch?b.$searchbox.focus():b.$button.focus(),c.preventDefault(),c.stopPropagation(),a(this).hasClass("bs-select-all")?b.selectAll():b.deselectAll(),b.$element.triggerNative("change")}),this.$element.change(function(){b.render(!1)})},liveSearchListener:function(){var d=this,e=a('<li class="no-results"></li>');this.$button.on("click.dropdown.data-api touchstart.dropdown.data-api",function(){d.$menuInner.find(".active").removeClass("active"),d.$searchbox.val()&&(d.$searchbox.val(""),d.$lis.not(".is-hidden").removeClass("hidden"),e.parent().length&&e.remove()),d.multiple||d.$menuInner.find(".selected").addClass("active"),setTimeout(function(){d.$searchbox.focus()},10)}),this.$searchbox.on("click.dropdown.data-api focus.dropdown.data-api touchend.dropdown.data-api",function(a){a.stopPropagation()}),this.$searchbox.on("input propertychange",function(){if(d.$searchbox.val()){var f=d.$lis.not(".is-hidden").removeClass("hidden").children("a");f=d.options.liveSearchNormalize?f.not(":a"+d._searchStyle()+'("'+b(d.$searchbox.val())+'")'):f.not(":"+d._searchStyle()+'("'+d.$searchbox.val()+'")'),f.parent().addClass("hidden"),d.$lis.filter(".dropdown-header").each(function(){var b=a(this),c=b.data("optgroup");0===d.$lis.filter("[data-optgroup="+c+"]").not(b).not(".hidden").length&&(b.addClass("hidden"),d.$lis.filter("[data-optgroup="+c+"div]").addClass("hidden"))});var g=d.$lis.not(".hidden");g.each(function(b){var c=a(this);c.hasClass("divider")&&(c.index()===g.first().index()||c.index()===g.last().index()||g.eq(b+1).hasClass("divider"))&&c.addClass("hidden")}),d.$lis.not(".hidden, .no-results").length?e.parent().length&&e.remove():(e.parent().length&&e.remove(),e.html(d.options.noneResultsText.replace("{0}",'"'+c(d.$searchbox.val())+'"')).show(),d.$menuInner.append(e))}else d.$lis.not(".is-hidden").removeClass("hidden"),e.parent().length&&e.remove();d.$lis.filter(".active").removeClass("active"),d.$searchbox.val()&&d.$lis.not(".hidden, .divider, .dropdown-header").eq(0).addClass("active").children("a").focus(),a(this).focus()})},_searchStyle:function(){var a={begins:"ibegins",startsWith:"ibegins"};return a[this.options.liveSearchStyle]||"icontains"},val:function(a){return"undefined"!=typeof a?(this.$element.val(a),this.render(),this.$element):this.$element.val()},changeAll:function(b){"undefined"==typeof b&&(b=!0),this.findLis();for(var c=this.$element.find("option"),d=this.$lis.not(".divider, .dropdown-header, .disabled, .hidden").toggleClass("selected",b),e=d.length,f=[],g=0;e>g;g++){var h=d[g].getAttribute("data-original-index");f[f.length]=c.eq(h)[0]}a(f).prop("selected",b),this.render(!1)},selectAll:function(){return this.changeAll(!0)},deselectAll:function(){return this.changeAll(!1)},keydown:function(c){var d,e,f,g,h,i,j,k,l,m=a(this),n=m.is("input")?m.parent().parent():m.parent(),o=n.data("this"),p=":not(.disabled, .hidden, .dropdown-header, .divider)",q={32:" ",48:"0",49:"1",50:"2",51:"3",52:"4",53:"5",54:"6",55:"7",56:"8",57:"9",59:";",65:"a",66:"b",67:"c",68:"d",69:"e",70:"f",71:"g",72:"h",73:"i",74:"j",75:"k",76:"l",77:"m",78:"n",79:"o",80:"p",81:"q",82:"r",83:"s",84:"t",85:"u",86:"v",87:"w",88:"x",89:"y",90:"z",96:"0",97:"1",98:"2",99:"3",100:"4",101:"5",102:"6",103:"7",104:"8",105:"9"};if(o.options.liveSearch&&(n=m.parent().parent()),o.options.container&&(n=o.$menu),d=a("[role=menu] li",n),l=o.$newElement.hasClass("open"),!l&&(c.keyCode>=48&&c.keyCode<=57||c.keyCode>=96&&c.keyCode<=105||c.keyCode>=65&&c.keyCode<=90)&&(o.options.container?o.$button.trigger("click"):(o.setSize(),o.$menu.parent().addClass("open"),l=!0),o.$searchbox.focus()),o.options.liveSearch&&(/(^9$|27)/.test(c.keyCode.toString(10))&&l&&0===o.$menu.find(".active").length&&(c.preventDefault(),o.$menu.parent().removeClass("open"),o.options.container&&o.$newElement.removeClass("open"),o.$button.focus()),d=a("[role=menu] li"+p,n),m.val()||/(38|40)/.test(c.keyCode.toString(10))||0===d.filter(".active").length&&(d=o.$menuInner.find("li"),d=o.options.liveSearchNormalize?d.filter(":a"+o._searchStyle()+"("+b(q[c.keyCode])+")"):d.filter(":"+o._searchStyle()+"("+q[c.keyCode]+")"))),d.length){if(/(38|40)/.test(c.keyCode.toString(10)))e=d.index(d.find("a").filter(":focus").parent()),g=d.filter(p).first().index(),h=d.filter(p).last().index(),f=d.eq(e).nextAll(p).eq(0).index(),i=d.eq(e).prevAll(p).eq(0).index(),j=d.eq(f).prevAll(p).eq(0).index(),o.options.liveSearch&&(d.each(function(b){a(this).hasClass("disabled")||a(this).data("index",b)}),e=d.index(d.filter(".active")),g=d.first().data("index"),h=d.last().data("index"),f=d.eq(e).nextAll().eq(0).data("index"),i=d.eq(e).prevAll().eq(0).data("index"),j=d.eq(f).prevAll().eq(0).data("index")),k=m.data("prevIndex"),38==c.keyCode?(o.options.liveSearch&&e--,e!=j&&e>i&&(e=i),g>e&&(e=g),e==k&&(e=h)):40==c.keyCode&&(o.options.liveSearch&&e++,-1==e&&(e=0),e!=j&&f>e&&(e=f),e>h&&(e=h),e==k&&(e=g)),m.data("prevIndex",e),o.options.liveSearch?(c.preventDefault(),m.hasClass("dropdown-toggle")||(d.removeClass("active").eq(e).addClass("active").children("a").focus(),m.focus())):d.eq(e).children("a").focus();else if(!m.is("input")){var r,s,t=[];d.each(function(){a(this).hasClass("disabled")||a.trim(a(this).children("a").text().toLowerCase()).substring(0,1)==q[c.keyCode]&&t.push(a(this).index())}),r=a(document).data("keycount"),r++,a(document).data("keycount",r),s=a.trim(a(":focus").text().toLowerCase()).substring(0,1),s!=q[c.keyCode]?(r=1,a(document).data("keycount",r)):r>=t.length&&(a(document).data("keycount",0),r>t.length&&(r=1)),d.eq(t[r-1]).children("a").focus()}if((/(13|32)/.test(c.keyCode.toString(10))||/(^9$)/.test(c.keyCode.toString(10))&&o.options.selectOnTab)&&l){if(/(32)/.test(c.keyCode.toString(10))||c.preventDefault(),o.options.liveSearch)/(32)/.test(c.keyCode.toString(10))||(o.$menuInner.find(".active a").click(),m.focus());else{var u=a(":focus");u.click(),u.focus(),c.preventDefault(),a(document).data("spaceSelect",!0)}a(document).data("keycount",0)}(/(^9$|27)/.test(c.keyCode.toString(10))&&l&&(o.multiple||o.options.liveSearch)||/(27)/.test(c.keyCode.toString(10))&&!l)&&(o.$menu.parent().removeClass("open"),o.options.container&&o.$newElement.removeClass("open"),o.$button.focus())}},mobile:function(){this.$element.addClass("mobile-device")},refresh:function(){this.$lis=null,this.liObj={},this.reloadLi(),this.render(),this.checkDisabled(),this.liHeight(!0),this.setStyle(),this.setWidth(),this.$lis&&this.$searchbox.trigger("propertychange"),this.$element.trigger("refreshed.bs.select")},hide:function(){this.$newElement.hide()},show:function(){this.$newElement.show()},remove:function(){this.$newElement.remove(),this.$element.remove()},destroy:function(){this.$newElement.before(this.$element).remove(),this.$bsContainer?this.$bsContainer.remove():this.$menu.remove(),this.$element.off(".bs.select").removeData("selectpicker").removeClass("bs-select-hidden selectpicker")}};var f=a.fn.selectpicker;a.fn.selectpicker=d,a.fn.selectpicker.Constructor=e,a.fn.selectpicker.noConflict=function(){return a.fn.selectpicker=f,this},a(document).data("keycount",0).on("keydown.bs.select",'.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bs-searchbox input',e.prototype.keydown).on("focusin.modal",'.bootstrap-select [data-toggle=dropdown], .bootstrap-select [role="menu"], .bs-searchbox input',function(a){a.stopPropagation()}),a(window).on("load.bs.select.data-api",function(){a(".selectpicker").each(function(){var b=a(this);d.call(b,b.data())})})}(a)});
//# sourceMappingURL=bootstrap-select.js.map

$('.selectpicker').selectpicker({
	style: 'btn-default'
});

/*!
PUSH MENU
 */
// function disableOther(s){"showLeft"!==s&&classie.toggle(showLeft,"disabled")}!function(s){"use strict";function e(s){return new RegExp("(^|\\s+)"+s+"(\\s+|$)")}function t(s,e){var t=n(s,e)?c:a;t(s,e)}var n,a,c;"classList"in document.documentElement?(n=function(s,e){return s.classList.contains(e)},a=function(s,e){s.classList.add(e)},c=function(s,e){s.classList.remove(e)}):(n=function(s,t){return e(t).test(s.className)},a=function(s,e){n(s,e)||(s.className=s.className+" "+e)},c=function(s,t){s.className=s.className.replace(e(t)," ")}),s.classie={hasClass:n,addClass:a,removeClass:c,toggleClass:t,has:n,add:a,remove:c,toggle:t}}(window);var menuLeft=document.getElementById("cbp-spmenu-s1"),body=document.body;showLeft.onclick=function(){classie.toggle(this,"active"),classie.toggle(menuLeft,"cbp-spmenu-open"),disableOther("showLeft")};

/*!
PROGRESS
 */
!function(t){"use strict";var e=function(n,a){this.$element=t(n),this.options=t.extend({},e.defaults,a)};e.defaults={transition_delay:300,refresh_speed:50,display_text:"none",use_percentage:!0,percent_format:function(t){return t+"%"},amount_format:function(t,e,n){return t+" / "+e},update:t.noop,done:t.noop,fail:t.noop},e.prototype.transition=function(){var n=this.$element,a=n.parent(),s=this.$back_text,r=this.$front_text,i=this.options,o=parseInt(n.attr("data-transitiongoal")),h=parseInt(n.attr("aria-valuemin"))||0,d=parseInt(n.attr("aria-valuemax"))||100,f=a.hasClass("vertical"),p=i.update&&"function"==typeof i.update?i.update:e.defaults.update,u=i.done&&"function"==typeof i.done?i.done:e.defaults.done,c=i.fail&&"function"==typeof i.fail?i.fail:e.defaults.fail;if(isNaN(o))return void c("data-transitiongoal not set");var l=Math.round(100*(o-h)/(d-h));if("center"===i.display_text&&!s&&!r){this.$back_text=s=t("<span>").addClass("progressbar-back-text").prependTo(a),this.$front_text=r=t("<span>").addClass("progressbar-front-text").prependTo(n);var g;f?(g=a.css("height"),s.css({height:g,"line-height":g}),r.css({height:g,"line-height":g}),t(window).resize(function(){g=a.css("height"),s.css({height:g,"line-height":g}),r.css({height:g,"line-height":g})})):(g=a.css("width"),r.css({width:g}),t(window).resize(function(){g=a.css("width"),r.css({width:g})}))}setTimeout(function(){var t,e,c,g,_;f?n.css("height",l+"%"):n.css("width",l+"%");var x=setInterval(function(){f?(c=n.height(),g=a.height()):(c=n.width(),g=a.width()),t=Math.round(100*c/g),e=Math.round(h+c/g*(d-h)),t>=l&&(t=l,e=o,u(n),clearInterval(x)),"none"!==i.display_text&&(_=i.use_percentage?i.percent_format(t):i.amount_format(e,d,h),"fill"===i.display_text?n.text(_):"center"===i.display_text&&(s.text(_),r.text(_))),n.attr("aria-valuenow",e),p(t,n)},i.refresh_speed)},i.transition_delay)};var n=t.fn.progressbar;t.fn.progressbar=function(n){return this.each(function(){var a=t(this),s=a.data("bs.progressbar"),r="object"==typeof n&&n;s||a.data("bs.progressbar",s=new e(this,r)),s.transition()})},t.fn.progressbar.Constructor=e,t.fn.progressbar.noConflict=function(){return t.fn.progressbar=n,this}}(window.jQuery);

$('.progress .progress-bar').progressbar({transition_delay: 800});

/*!
FUN FACTS
 */
function count($this){
	var current = parseInt($this.html(), 10);
	current = current + 50;
	$this.html(++current);
	if(current > $this.data('count')){
		$this.html($this.data('count'));
	}
	else {
		setTimeout(function(){count($this)}, 50);
	}
}
$(".stat-count").each(function() {
	$(this).data('count', parseInt($(this).html(), 10));
	$(this).html('0');
	count($(this));
});

/*!
FILE UPLOAD
 */
!function(e){var t=function(t,n){this.$element=e(t),this.type=this.$element.data("uploadtype")||(this.$element.find(".thumbnail").length>0?"image":"file"),this.$input=this.$element.find(":file");if(this.$input.length===0)return;this.name=this.$input.attr("name")||n.name,this.$hidden=this.$element.find('input[type=hidden][name="'+this.name+'"]'),this.$hidden.length===0&&(this.$hidden=e('<input type="hidden" />'),this.$element.prepend(this.$hidden)),this.$preview=this.$element.find(".fileupload-preview");var r=this.$preview.css("height");this.$preview.css("display")!="inline"&&r!="0px"&&r!="none"&&this.$preview.css("line-height",r),this.original={exists:this.$element.hasClass("fileupload-exists"),preview:this.$preview.html(),hiddenVal:this.$hidden.val()},this.$remove=this.$element.find('[data-dismiss="fileupload"]'),this.$element.find('[data-trigger="fileupload"]').on("click.fileupload",e.proxy(this.trigger,this)),this.listen()};t.prototype={listen:function(){this.$input.on("change.fileupload",e.proxy(this.change,this)),e(this.$input[0].form).on("reset.fileupload",e.proxy(this.reset,this)),this.$remove&&this.$remove.on("click.fileupload",e.proxy(this.clear,this))},change:function(e,t){if(t==="clear")return;var n=e.target.files!==undefined?e.target.files[0]:e.target.value?{name:e.target.value.replace(/^.+\\/,"")}:null;if(!n){this.clear();return}this.$hidden.val(""),this.$hidden.attr("name",""),this.$input.attr("name",this.name);if(this.type==="image"&&this.$preview.length>0&&(typeof n.type!="undefined"?n.type.match("image.*"):n.name.match(/\.(gif|png|jpe?g)$/i))&&typeof FileReader!="undefined"){var r=new FileReader,i=this.$preview,s=this.$element;r.onload=function(e){i.html('<img src="'+e.target.result+'" '+(i.css("max-height")!="none"?'style="max-height: '+i.css("max-height")+';"':"")+" />"),s.addClass("fileupload-exists").removeClass("fileupload-new")},r.readAsDataURL(n)}else this.$preview.text(n.name),this.$element.addClass("fileupload-exists").removeClass("fileupload-new")},clear:function(e){this.$hidden.val(""),this.$hidden.attr("name",this.name),this.$input.attr("name","");if(navigator.userAgent.match(/msie/i)){var t=this.$input.clone(!0);this.$input.after(t),this.$input.remove(),this.$input=t}else this.$input.val("");this.$preview.html(""),this.$element.addClass("fileupload-new").removeClass("fileupload-exists"),e&&(this.$input.trigger("change",["clear"]),e.preventDefault())},reset:function(e){this.clear(),this.$hidden.val(this.original.hiddenVal),this.$preview.html(this.original.preview),this.original.exists?this.$element.addClass("fileupload-exists").removeClass("fileupload-new"):this.$element.addClass("fileupload-new").removeClass("fileupload-exists")},trigger:function(e){this.$input.trigger("click"),e.preventDefault()}},e.fn.fileupload=function(n){return this.each(function(){var r=e(this),i=r.data("fileupload");i||r.data("fileupload",i=new t(this,n)),typeof n=="string"&&i[n]()})},e.fn.fileupload.Constructor=t,e(document).on("click.fileupload.data-api",'[data-provides="fileupload"]',function(t){var n=e(this);if(n.data("fileupload"))return;n.fileupload(n.data());var r=e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');r.length>0&&(r.trigger("click.fileupload"),t.preventDefault())})}(window.jQuery)

/*
 * Url preview script
 * powered by jQuery (http://www.jquery.com)
 *
 * written by Alen Grakalic (http://cssglobe.com)
 *
 * for more info visit http://cssglobe.com/post/1695/easiest-tooltip-and-image-preview-using-jquery
 *
 */

this.screenshotPreview = function(){
	/* CONFIG */

	xOffset = 100;
	yOffset = 30;

	// these 2 variable determine popup's distance from the cursor
	// you might want to adjust to get the right result

	/* END CONFIG */
	$("a.screenshot").hover(function(e){
			this.t = this.title;
			this.title = "";
			var c = (this.t != "") ? "<br/>" + this.t : "";
			$("body").append("<p id='screenshot'><img src='"+ this.rel +"' alt='url preview' />"+ c +"</p>");
			$("#screenshot")
				.css("top",(e.pageY - xOffset) + "px")
				.css("left",(e.pageX + yOffset) + "px")
				.fadeIn("fast");
		},
		function(){
			this.title = this.t;
			$("#screenshot").remove();
		});
	$("a.screenshot").mousemove(function(e){
		$("#screenshot")
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",(e.pageX + yOffset) + "px");
	});
};


jQuery('a[data-gal]').each(function() {
	jQuery(this).attr('rel', jQuery(this).data('gal')); });
jQuery("a[data-rel^='screenshot']").hover();



// VARIABLES
var CARD_CHANGED = false;
var cardChangeTempCode = '';


// FUNCTIONS
// CHECKING IF CARD CHANGED
var checkCardChanges = function(){
	if(cardChangeTempCode != ''){
		var currCardContainer = $('.card-editor-preview-container').html();
		currCardContainer = currCardContainer.replace(/\s/g, '');

		if(cardChangeTempCode === currCardContainer){
			CARD_CHANGED = false;
		} else {
			CARD_CHANGED = true;
		}
	}
};

// CARD WIDTH VALIDATION WITH OPTIONAL AUTORESIZE
var validateCardWidth = function(width, autoResize){
	var restrictions = {
		min: 100,
		max: 1500
	};

	if(width < restrictions.min || width > restrictions.max){
		$("div#card_width_error_dialog").dialog({
			width: 350,
			classes: {
				"ui-dialog": "card-dialog-content"
			},
			open: function() {
				var item = $(this);
				setTimeout(function() {
					item.dialog('close');
				}, 6000);
			}
		});

		if(autoResize === true){
			$('input#referrer_link_card_width').val(width < restrictions.min ? restrictions.min : restrictions.max).trigger('input');
		}

		return false;
	}

	return true;
};

// SET PROPS FOR SLOGANS FROM INPUTS
var setSlogansProps = function(){
	$('.slogan-element').each(function(){
		var $this = $(this);
		var $rel = $this.attr('rel');

		if($('#referrer_link_card_'+$rel+'Size').val() != '' && $('#referrer_link_card_'+$rel+'Size').val() > 0){
			$this.css({ 'font-size': parseInt($('#referrer_link_card_'+$rel+'Size').val())});
		} else {
			$this.css({ 'font-size': 15});
		}

		if($('#referrer_link_card_'+$rel+'Italic').is(':checked')){
			$this.css({'font-style': 'italic'});
		}

		if($('#referrer_link_card_'+$rel+'Underline').is(':checked')){
			$this.css({'text-decoration': 'underline'});
		}

		if($('#referrer_link_card_'+$rel+'Bold').is(':checked')){
			$this.css({'font-weight': '700'});
		}

		if($('#referrer_link_card_'+$rel+'Color').val() != ''){
			$this.css({ color:  $('#referrer_link_card_'+$rel+'Color').val()});
		}
	});
};

// CALCULATING RATIO FOR HEIGHT
var calculateAspectRatio = function(width, props){
	props = props.split(':');

	return parseInt(width*props[1]/props[0]);
};

// RESET ELEMENTS POSITION WHEN ITS OUTSIDE CARD PREVIEW CONTAINER
var setElementsVisible = function(){
	$('.card-editor-preview .ui-draggable').each(function(){
		var $this = $(this);

		if(($this.position().left + $this.width()) > ($('.card-editor-preview').width() + $('.card-editor-preview').position().left)){
			$this.css({ "left": 0 });
		}

		if(($this.position().top + $this.height()) > ($('.card-editor-preview').height() + $('.card-editor-preview').position().top)){
			$this.css({ "bottom": 0, "top": "initial" });
		}
	});
};

// CHANGE CARD AREA WIDTH AND HEIGHT
var recalculateAreaSize = function(width, height){
	$('.card-editor-preview').css({ 'width': width, 'height': height });
	$('#referrer_link_card_width').val(width);
	$('#referrer_link_card_height').val(height);

	setElementsVisible();
	checkCardChanges();
	centerCardPreview();
	validateCardWidth(width, true);
};

// SET SOCIAL MEDIA VISIBILITY
var setSocialMediaIcons = function(mode){
	if(mode == 1){
		$('.card-editor-preview .social-icons').addClass('show');
	} else {
		$('.card-editor-preview .social-icons').removeClass('show');
	}

	checkCardChanges();
};

// RGB TO HEX CONVERTER
var rgbToHex = function(r, g, b) {
	return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
};

// SET BACKGROUND IMAGE
var setBackgroundImage = function(image){
	$('img#cardBackgroundImage').remove();

	image = image.replace('url("', '').replace(/"\)$/g, '');

	if(image !== 'none'){
		$('.card-editor-preview').prepend('<img id="cardBackgroundImage" src="'+image+'" alt="background image"/>');

		var cardBackgroundImage = $('#cardBackgroundImage');
		var cardEditorPreview = $('.card-editor-preview');

		var th = cardEditorPreview.height(),
			tw = cardEditorPreview.width(),
			im = cardEditorPreview.find('img#cardBackgroundImage'),
			ih = im.height(),
			iw = im.width();

		ih > iw ? im.css({ width: '100%' }) : im.css({ height: '100%' });

		var nh = im.height(),
			nw = im.width(),
			hd = (nh-th)/2,
			wd = (nw-tw)/2;


		// nh < nw ? im.css({marginLeft: '-'+wd+'px', marginTop: 0}) : im.css({marginTop: '-'+hd+'px', marginLeft: 0});

		cardBackgroundImage.draggable({
			drag: function(event, ui){
				$('#referrer_link_card_backgroundPosition_x').val(ui.position.left);
				$('#referrer_link_card_backgroundPosition_y').val(ui.position.top);
			}
		});

		var changeBackgroundSizeSlider = $('#changeBackgroundSize');
		changeBackgroundSizeSlider.find('.config_content').slider({
			min: 0,
			max: 1600,
			value:$('#referrer_link_card_backgroundWidth').val(),
			slide: function(event, ui){
				cardBackgroundImage.width(ui.value).height('auto');
				cardBackgroundImage.attr({ 'data-width': ui.value });

				$('#referrer_link_card_backgroundWidth').val(cardBackgroundImage.width());
				$('#referrer_link_card_backgroundHeight').val(cardBackgroundImage.height());
			}
		});

		//Cards margins
		//Card margin top
		$('#changeMarginTop .config_content').slider({
			min: 0,
			max: 100,
			value: document.querySelector('#referrer_link_card_marginTop').value,
			slide: function(event, ui) {
				document.querySelector('#referrer_link_card_marginTop').value = ui.value
				$('#changeMarginTop label span').html(ui.value)
			}
		})
		$('#changeMarginRight .config_content').slider({
			min: 0,
			max: 100,
			value: document.querySelector('#referrer_link_card_marginRight').value,
			slide: function(event, ui) {
				document.querySelector('#referrer_link_card_marginRight').value = ui.value
				$('#changeMarginRight label span').html(ui.value)
			}
		})
		$('#changeMarginBottom .config_content').slider({
			min: 0,
			max: 100,
			value: document.querySelector('#referrer_link_card_marginBottom').value,
			slide: function(event, ui) {
				document.querySelector('#referrer_link_card_marginBottom').value = ui.value
				$('#changeMarginBottom label span').html(ui.value)
			}
		})
		$('#changeMarginLeft .config_content').slider({
			min: 0,
			max: 100,
			value: document.querySelector('#referrer_link_card_marginLeft').value,
			slide: function(event, ui) {
				document.querySelector('#referrer_link_card_marginLeft').value = ui.value
				$('#changeMarginLeft label span').html(ui.value)
			}
		})

		$('.card-editor-actions > div:first-child').addClass('active');

		if($('.card-background-item.card-background-item--upload').is('.active') && $('.card-background-item.card-background-item--upload').css('background-image') != 'none'){
			$('label[for="referrer_link_card_background"]').last().hide();
		}
	}
};

// CHANGING AREA BACKGROUND
var changeAreaBackground = function(element){
	var prevBackground = $('.card-editor-preview').attr('data-color-hex');
	var backgroundImage = element.css('background-image') || '';
	var backgroundColor = element.css('background-color') || '';

	if(backgroundColor != ''){
		backgroundColor = backgroundColor.replace('rgb(', '').replace(')', '');

		if(backgroundColor.indexOf('#') === -1){
			backgroundColor = backgroundColor.split(',').map(function(x){
				return x.replace(/\s/g, '');
			});

			backgroundColor = rgbToHex(parseInt(backgroundColor[0]), parseInt(backgroundColor[1]), parseInt(backgroundColor[2]));
		}

		if((backgroundImage == 'none' || backgroundImage == '')){
			$('input#referrer_link_card_backgroundColor').val(backgroundColor);

			var backgroundPicker = $.farbtastic('#background-picker-container');
			backgroundPicker.setColor(backgroundColor);

			$('.card-background-item.active:not(.card-background-item-og):not(.card-background-item--upload)').removeClass('active');
			element.addClass('active');
		}
	} else {
		$(".card-editor-preview").css({'background-color': prevBackground});
	}

	if(backgroundImage != '' && backgroundImage != 'none'){
		setBackgroundImage(backgroundImage);
	}

	checkCardChanges();
};

// EVENTS FOR SLOGANS
var attachSlogansStyleEvents = function(){
	$('.changeFontItalic').on('click', function() {
		var $this = $(this);
		var currSlogan = $this.parent().parent();
		var sloganStyle = currSlogan.css('font-style') == 'italic' ? 'normal' : 'italic';
		var checked = sloganStyle == 'normal' ? false : true;

		currSlogan.css({ 'font-style': sloganStyle });

		switch($this.attr('href')){
			case '#changeFontItalicBig':
				$('#referrer_link_card_titleItalic').prop('checked', checked);
				break;
			case '#changeFontItalicSmall':
				$('#referrer_link_card_subtitleItalic').prop('checked', checked);
				break;
			case '#changeFontItalicText1':
				$('#referrer_link_card_text1Italic').prop('checked', checked);
				break;
			case '#changeFontItalicText2':
				$('#referrer_link_card_text2Italic').prop('checked', checked);
				break;
		}

		return false;
	});

	$('.changeFontBold').on('click', function(){
		var $this = $(this);

		var currSlogan = $this.parent().parent();
		var fontWeight = currSlogan.css('font-weight') == '700' ? '400' : '700';
		var checked = fontWeight == '400' ? false : true;

		currSlogan.css({'font-weight': fontWeight});

		switch($().attr('href')){
			case '#changeFontBoldBig':
				$('#referrer_link_card_titleBold').prop('checked', checked);
				break;
			case '#changeFontBoldSmall':
				$('#referrer_link_card_subtitleBold').prop('checked', checked);
				break;
			case '#changeFontBoldText1':
				$('#referrer_link_card_text1Bold').prop('checked', checked);
				break;
			case '#changeFontBoldText2':
				$('#referrer_link_card_text2Bold').prop('checked', checked);
				break;
		}

		return false;
	});

	$('.changeFontUnderline').on('click', function(){
		var $this = $(this);
		var currSlogan = $this.parent().parent();
		var underline = currSlogan.css('text-decoration').includes('underline') ? '' : 'underline';
		var checked = underline == '' ? false : true;

		currSlogan.css({'text-decoration': underline});

		switch($this.attr('href')){
			case '#changeFontUnderlineBig':
				$('#referrer_link_card_titleUnderline').prop('checked', checked);
				break;
			case '#changeFontUnderlineSmall':
				$('#referrer_link_card_subtitleUnderline').prop('checked', checked);
				break;
			case '#changeFontUnderlineText1':
				$('#referrer_link_card_text1Underline').prop('checked', checked);
				break;
			case '#changeFontUnderlineText2':
				$('#referrer_link_card_text2Underline').prop('checked', checked);
				break;
		}

		return false;
	});
};

var setToolbarEvents = function($link){
	var target = $link.attr('href');

	$('.changeSizeContainer, .changeColorContainer, .changeFontFamilyContainer').removeClass('show');
	$(target).addClass('show');

	return false;
};

// FILL SLOGANS
var fillSlogan = function(bigger, smaller, text1, text2){

	bigger = bigger.replace(/\n/g, '<br/>');
	smaller = smaller.replace(/\n/g, '<br/>');
	text1 = text1.replace(/\n/g, '<br/>');
	text2 = text2.replace(/\n/g, '<br/>');

	$('.slogan-element').each(function(){
		var $this = $(this);
		var sloganType = '';
		var toolbarClone = $this.find('.card-editor-preview-toolbar').clone();

		if($this.is('.bigger-slogan')){
			$this.html(bigger);
		} else if($this.is('.smaller-slogan')){
			$this.html(smaller);
		} else if($this.is('.text1-slogan')){
			$this.html(text1);
		} else if($this.is('.text2-slogan')){
			$this.html(text2);
		}

		$this.append(toolbarClone);

		$this.draggable({
			drag: function(event, ui){
				var currentEl = $(ui.helper[0]);
				var currentPicker = currentEl.find('a.changeColor').attr('href');
				var currentSlider = currentEl.find('a.changeFontSize').attr('href');
				var currenFontFamilySelect = currentEl.find('a.changeFontFamily').attr('href');

				var scale = $('.card-editor-preview-container').css('zoom') || $('.card-editor-preview-container').css('transform');

				if($('.card-editor-preview-container').css('transform') && $('.card-editor-preview-container').css('transform').indexOf('matrix') > -1){
					scale = $('.card-editor-preview-container').css('transform').replace('matrix(', '').replace(')').split(', ')[0];
				}

				var pos = {
					left: (ui.position.left * scale) + $('.card-editor').position().left + $('.card-editor-preview-container').position().left + $('.card-editor-preview').position().left,
					top: (ui.position.top * scale) + $('.card-editor').position().top + $('.card-editor-preview-container').position().top + $('.card-editor-preview').position().top
				};

				$(currentPicker).css({ left: pos.left + (((currentEl.outerWidth() * scale) - $(currentPicker).outerWidth())/* / 2*/), top: pos.top + (currentEl.outerHeight() * scale) + 30 });
				$(currentSlider).css({ left: pos.left + (((currentEl.outerWidth() * scale) - $(currentSlider).outerWidth())/* / 2*/), top: pos.top + (currentEl.outerHeight() * scale) + 30 });
				$(currenFontFamilySelect).css({ left: pos.left + (((currentEl.outerWidth() * scale) - $(currenFontFamilySelect).outerWidth())/* / 2*/), top: pos.top + (currentEl.outerHeight() * scale) + 30 });

				$('#referrer_link_card_'+$this.attr('rel')+'Position_x').val(ui.position.left);
				$('#referrer_link_card_'+$this.attr('rel')+'Position_y').val(ui.position.top);
			}
		});

		$('.card-editor-preview-toolbar').find('a').on('click', function(){
			setToolbarEvents($(this));

			return false;
		});

		if(bigger == ''){
			$('.bigger-slogan').find('.card-editor-preview-toolbar a').each(function(){
				$($(this).attr('href')).removeClass('show');
			});
		}

		if(smaller == ''){
			$('.smaller-slogan').find('.card-editor-preview-toolbar a').each(function(){
				$($(this).attr('href')).removeClass('show');
			});
		}

		if(text1 == ''){
			$('.text1-slogan').find('.card-editor-preview-toolbar a').each(function(){
				$($(this).attr('href')).removeClass('show');
			});
		}

		if(text2 == ''){
			$('.text2-slogan').find('.card-editor-preview-toolbar a').each(function(){
				$($(this).attr('href')).removeClass('show');
			});
		}

		// SET TOOLBARS POSITION ON TYPE TEXT

		var toolbars_scale = $('.card-editor-preview-container').css('zoom') || $('.card-editor-preview-container').css('transform');

		if($('.card-editor-preview-container').css('transform') && $('.card-editor-preview-container').css('transform').indexOf('matrix') > -1){
			toolbars_scale = $('.card-editor-preview-container').css('transform').replace('matrix(', '').replace(')').split(', ')[0];
		}

		var currentPicker = $this.find('a.changeColor').attr('href');
		var currentSlider = $this.find('a.changeFontSize').attr('href');
		var currenFontFamilySelect = $this.find('a.changeFontFamily').attr('href');
		var pos = {
			left: ($this.position().left * toolbars_scale) + $('.card-editor').position().left + $('.card-editor-preview-container').position().left + $('.card-editor-preview').position().left,
			top: ($this.position().top * toolbars_scale) + $('.card-editor').position().top + $('.card-editor-preview-container').position().top + $('.card-editor-preview').position().top
		};

		$(currentPicker).css({ left: pos.left + ((($this.outerWidth() * toolbars_scale) - $(currentPicker).outerWidth())/* / 2*/), top: pos.top + ($this.outerHeight() & toolbars_scale) + 30 });
		$(currentSlider).css({ left: pos.left + ((($this.outerWidth() * toolbars_scale) - $(currentSlider).outerWidth())/* / 2*/), top: pos.top + ($this.outerHeight() * toolbars_scale) + 30 });
		$(currenFontFamilySelect).css({ left: pos.left + ((($this.outerWidth() * toolbars_scale) - $(currenFontFamilySelect).outerWidth())/* / 2*/), top: pos.top + ($this.outerHeight() * toolbars_scale) + 30 });
	});

	$('.changeSizeContainer, .changeColorContainer, .changeFontFamilyContainer').removeClass('show');

	attachSlogansStyleEvents();
	setSlogansProps();
	checkCardChanges();
};

// REMOVE ALL CARD ELEMS AND RESET BACKGROUND
var clearAll = function(){
	$(".card-editor-preview > *").remove();
	$('.card-editor-preview').removeAttr('style');
	$('.card-editor-preview').css('background-color', '#fff');
};

// CENTER CARD BODY ON CONTAINER
var centerCardPreview = function (scale){
	scale = scale || 1;

	var previewContent = $('.card-editor-preview-container');
	var previewContainer = $('.card-editor');

	var contentSize = {
		width: previewContent.outerWidth() * scale,
		height: previewContent.outerHeight() * scale
	};

	var containerSize = {
		width: previewContainer.innerWidth(),
		height: previewContainer.innerHeight()
	};

	var newPosLeft = ((containerSize.width - contentSize.width)/2) > 0 ? (containerSize.width - contentSize.width)/2 : 0;
	var newPosTop = ((containerSize.height - contentSize.height)/2) > 0 ? (containerSize.height - contentSize.height)/2 : 0;

	previewContent.css({ left: newPosLeft, top: newPosTop });
};

// VARIABLE FOR TOOLBAR SCALLING ON ZOOM
var DEFAULT_TOOLBAR_WIDTH = $('.card-editor-preview-toolbar').outerWidth();

// CHANGE ZOOM AND SCALE TOOLBAR
var changeZoom = function(zoom, reset){
	var checkZoom = $('.card-editor-preview-container').css('zoom') || $('.card-editor-preview-container').css('transform');

	if($('.card-editor-preview-container').css('transform') && $('.card-editor-preview-container').css('transform').indexOf('matrix') > -1){
		checkZoom = $('.card-editor-preview-container').css('transform').replace('matrix(', '').replace(')').split(', ')[0];
	}

	var newZoomVal = checkZoom ? parseFloat(checkZoom) + zoom : 1 + zoom;

	if(reset === true){
		newZoomVal = 1;
	}

	if(newZoomVal > 1.4){
		newZoomVal = 1.4;
	} else if (newZoomVal < 0.6){
		newZoomVal = 0.6;
	}

	if(newZoomVal === 1){
		$('.slogan-element .card-editor-preview-toolbar').css({ 'transform': 'scale(1)', 'left': '0', 'bottom': '-102px', 'top': 'initial' });
		$('.social-icons .card-editor-preview-toolbar').css({ 'transform': 'scale(1)', 'left': '0', 'bottom': '-40px', 'top': 'initial' });
		$('.shop-logo .card-editor-preview-toolbar').css({ 'transform': 'scale(1)', 'left': '0', 'bottom': '-40px', 'top': 'initial' });
	} else {
		var toolbarScale = DEFAULT_TOOLBAR_WIDTH / (DEFAULT_TOOLBAR_WIDTH * newZoomVal);
		var toolbarMarginBottom = 120 * toolbarScale;

		$('.card-editor-preview-toolbar').css({ 'transform': 'scale('+toolbarScale+')', 'left': '0', 'bottom': -toolbarMarginBottom + 'px', 'top': 'initial' });
	}

	$('.card-editor-preview-container').css({ transform: 'scale('+newZoomVal+')' });

	var zoomLevel = 0;
	if(newZoomVal.toFixed(1) > 1){
		zoomLevel = '+' + ((newZoomVal.toFixed(1) - 1).toFixed(1)) * 10;
	} else if (newZoomVal.toFixed(1) === 1){
		zoomLevel = 0;
	} else {
		switch(newZoomVal.toFixed(1)){
			case '0.9':
				zoomLevel = '-1';
				break;
			case '0.8':
				zoomLevel = '-2';
				break;
			case '0.7':
				zoomLevel = '-3';
				break;
			case '0.6':
				zoomLevel = '-4';
				break;
		}
	}

	$('span#zoomLvl').find('strong').text(zoomLevel);

	centerCardPreview(newZoomVal);
};

// SET DRAGGABLE ON ALL ELEMENTS
var initDraggableOnAll = function(){
	$('.card-editor-preview > *').draggable({
		drag: function(event, ui){
			var toolbars_scale = $('.card-editor-preview-container').css('zoom') || $('.card-editor-preview-container').css('transform');

			if($('.card-editor-preview-container').css('transform') && $('.card-editor-preview-container').css('transform').indexOf('matrix') > -1){
				toolbars_scale = $('.card-editor-preview-container').css('transform').replace('matrix(', '').replace(')').split(', ')[0];
			}

			var currentEl = $(ui.helper[0]);
			var currentPicker = currentEl.find('a.changeColor').attr('href');
			var currentSlider = currentEl.find('a.changeFontSize').attr('href') || currentEl.find('a.changeSocialSize').attr('href') || currentEl.find('a.changeLogoSize').attr('href');
			var currenFontFamilySelect = currentEl.find('a.changeFontFamily').attr('href');
			var pos = {
				left: (ui.position.left * toolbars_scale) + $('.card-editor').position().left + $('.card-editor-preview-container').position().left + $('.card-editor-preview').position().left,
				top: (ui.position.top * toolbars_scale) + $('.card-editor').position().top + $('.card-editor-preview-container').position().top + $('.card-editor-preview').position().top
			};

			$(currentPicker).css({ left: pos.left + (((currentEl.outerWidth() * toolbars_scale) - $(currentPicker).outerWidth())/* / 2*/), top: pos.top + ((currentEl.outerHeight() * toolbars_scale)) + 30 });
			$(currentSlider).css({ left: pos.left + (((currentEl.outerWidth() * toolbars_scale) - $(currentSlider).outerWidth())/* / 2*/), top: pos.top + ((currentEl.outerHeight() * toolbars_scale)) + 30 });
			$(currenFontFamilySelect).css({ left: pos.left + (((currentEl.outerWidth() * toolbars_scale) - $(currenFontFamilySelect).outerWidth()) / 2), top: pos.top + ((currentEl.outerHeight() * toolbars_scale)) + 30 });

			if($(ui.helper[0]).is('#cardBackgroundImage')){
				$('input#referrer_link_card_backgroundPosition_x').val(ui.position.left);
				$('input#referrer_link_card_backgroundPosition_y').val(ui.position.top);
			}

		}
	});
};

// VARIABLES FOR INIT DATA
var initValues = {};
var initRadioButtons = {
	socialMedia: '',
	logo: ''
};

var tempBgWidth, tempBgHeight, tempBgPosX, tempBgPosY, tempTitlePosX, tempTitlePosY, tempSubtitlePosX, tempSubtitlePosY, tempTitleColor, tempSubtitleColor, tempText1PosX, tempText1PosY, tempText2PosX, tempText2PosY, tempText1Color, tempText2Color;
var DEFAULT_SOCIAL_FONTSIZE = 12;
var DEFAULT_SOCIAL_WIDTH = 24;
var DEFAULT_SOCIAL_MARGIN = 10;


// SOCIAL MEDIA SCALLING
scaleSocialMedia = function(ui, ratio){
	var social_size_ratio = ratio || ui.value / DEFAULT_SOCIAL_FONTSIZE;

	var newSocialSize = DEFAULT_SOCIAL_WIDTH*social_size_ratio/2;

	$('.card-editor-preview .social-icons .social-icon a')
		.width(newSocialSize)
		.height(newSocialSize)
		.css({ "font-size": DEFAULT_SOCIAL_FONTSIZE*social_size_ratio, "line-height": "1" });

	$('.card-editor-preview .social-icon').css({ 'margin-right': DEFAULT_SOCIAL_MARGIN*social_size_ratio });

	$('input#referrer_link_card_socialMediaSize').val(social_size_ratio);
};

// FILL DATA ON INIT
var fillInitData = function(){
	changeZoom(1, true);

	var fieldWidth = $(".edit-card-width input").val();
	var fieldHeight = $(".edit-card-height input").val();
	var fieldBackground = $(".card-background-item.active");
	var fieldBiggerText = $(".card-slogan.slogan-bigger textarea").val();
	var fieldSmallerText = $(".card-slogan.slogan-smaller textarea").val();
	var socialMediaEnable = $('input[name="referrer_link_card[socialMedia]"]:checked').val();

	// IF CARD HAS INIT OBJECT - ONCE INITIATED
	if (!$.isEmptyObject(initValues)) {
		var tmp = $(initValues['initHtml']).clone();
		$('.card-editor-preview').replaceWith(initValues['initHtml']);
		initValues['initHtml'] = tmp;

		$(".edit-card-width input").val($('.card-editor-preview').width());
		$(".edit-card-height input").val($('.card-editor-preview').height());

		$('.card-background-item.active').removeClass('active');
	} else {
		if ($('.edit-card-width input').val() > 0) {
			$('.card-editor-preview').width($('.edit-card-width input').val());
		} else {
			$('.edit-card-width input').val($('.card-editor-preview').width());
		}

		if ($('.edit-card-height input').val() > 0) {
			$('.card-editor-preview').height($('.edit-card-height input').val());
		} else {
			$('.edit-card-height input').val($('.card-editor-preview').height());
		}

		if ($('.card-editor-preview').css('background-color') === 'rgba(0, 0, 0, 0)' && $('.card-background-item.active').length) {
			if ($('.card-background-item.active').css('background-image') !== 'none'){
				$('.card-editor-preview').css({ 'background-image': $(".card-background-item.active").css('background-image') });
			} else {
				$('.card-editor-preview').css({ 'background-color': $(".card-background-item.active").css('background-color') });
			}
		}
	}

	if ($('.card-editor-preview .social-icons').is('.show')) {
		$('input[name="socialradio"][value="1"]').prop("checked", true);
	} else {
		$('input[name="socialradio"][value="0"]').prop("checked", true);
	}

	if ($(".card-editor-preview .bigger-slogan").length){
		$(".card-slogan.slogan-bigger textarea").val($.trim($(".card-editor-preview .bigger-slogan").text()));
	}

	if ($(".card-editor-preview .smaller-slogan").length) {
		$(".card-slogan.slogan-smaller textarea").val($.trim($(".card-editor-preview .smaller-slogan").text()));
	}

	if ($(".card-editor-preview .text1-slogan").length){
		$(".card-slogan.slogan-text1 textarea").val($.trim($(".card-editor-preview .text1-slogan").text()));
	}

	if ($(".card-editor-preview .text2-slogan").length){
		$(".card-slogan.slogan-text2 textarea").val($.trim($(".card-editor-preview .text2-slogan").text()));
	}

	$('.changeColorContainer').each(function(){
		var $this = $(this);

		var trigger = $('a[href="#' + $this.attr('id') + '"]').parent().parent();
		var pos = {
			left: trigger.position().left + $('.card-editor').position().left + $('.card-editor-preview-container').position().left + $('.card-editor-preview').position().left,
			top: trigger.position().top + $('.card-editor').position().top + $('.card-editor-preview-container').position().top + $('.card-editor-preview').position().top
		};

		if($.fn.farbtastic){
			$this.find('.config_content').farbtastic(function(color){
				var trigger = $('a[href="#' + $this.attr('id') + '"]').parent().parent();
				var pos = {
					left: trigger.position().left + $('.card-editor').position().left + $('.card-editor-preview-container').position().left + $('.card-editor-preview').position().left,
					top: trigger.position().top + $('.card-editor').position().top + $('.card-editor-preview-container').position().top + $('.card-editor-preview').position().top
				};

				trigger.css({ color: color });
				trigger.attr('data-color-hex', color);

				if($this.attr('id') == 'changeColorSmall'){
					$('#referrer_link_card_subtitleColor').val(color);
				}

				if($this.attr('id') == 'changeColorBig'){
					$('#referrer_link_card_titleColor').val(color);
				}

				if($this.attr('id') == 'changeColorText1'){
					$('#referrer_link_card_text1Color').val(color);
				}

				if($this.attr('id') == 'changeColorText2'){
					$('#referrer_link_card_text2Color').val(color);
				}
			});
		}
		$this.css({ left: pos.left, top: pos.top+60 });
	});

	$('.changeSizeContainer').each(function(){
		var $this = $(this);
		var trigger = $('a[href="#' + $this.attr('id') + '"]').parent().parent();
		var pos = {
			left: trigger.position().left + $('.card-editor').position().left + $('.card-editor-preview-container').position().left + $('.card-editor-preview').position().left,
			top: trigger.position().top + $('.card-editor').position().top + $('.card-editor-preview-container').position().top + $('.card-editor-preview').position().top
		};
		var currFontSize = trigger.css('fontSize');

		if(!$this.is('#changeBackgroundSize')) {
			$this.find('.config_content').slider({
				min: 10,
				max: 70,
				value: parseInt(currFontSize),
				slide: function (event, ui) {
					if(trigger.is('.slogan-element')){
						trigger.css({'font-size': ui.value});
						trigger.attr({'data-font-size': ui.value});

						if($this.attr('id') == 'changeFontSizeSmall'){
							$('input#referrer_link_card_subtitleSize').val(ui.value);
						} else if ($this.attr('id') == 'changeFontSizeBig'){
							$('input#referrer_link_card_titleSize').val(ui.value);
						} else if ($this.attr('id') == 'changeFontSizeText1'){
							$('input#referrer_link_card_text1Size').val(ui.value);
						} else if ($this.attr('id') == 'changeFontSizeText2'){
							$('input#referrer_link_card_text2Size').val(ui.value);
						}
					} else if(trigger.is('.social-icons')){
						scaleSocialMedia(ui);
					} else if(trigger.is('.shop-logo')){
						$('.shop-logo img').width(ui.value);
						$('input#referrer_link_card_logoSize').val(ui.value);
					}
				},
				stop: function(event, ui){
					var scale = $('.card-editor-preview-container').css('zoom') || $('.card-editor-preview-container').css('transform');

					if($('.card-editor-preview-container').css('transform') && $('.card-editor-preview-container').css('transform').indexOf('matrix') > -1){
						scale = $('.card-editor-preview-container').css('transform').replace('matrix(', '').replace(')').split(', ')[0];
					}


					var pos = {
						left: trigger.position().left + $('.card-editor').position().left + $('.card-editor-preview-container').position().left + $('.card-editor-preview').position().left,
						top: trigger.position().top + $('.card-editor').position().top + $('.card-editor-preview-container').position().top + $('.card-editor-preview').position().top
					};

					$this.css({ left: pos.left + (((trigger.outerWidth() * scale) - $this.outerWidth())/* / 2*/), top: pos.top + (trigger.outerHeight() * scale) + 30 });
				}
			});
		}
		$this.css({ left: pos.left, top: pos.top+60 });
	});

	$('.changeFontFamilyContainer').each(function(){
		var $this = $(this);
		var trigger = $('a[href="#' + $this.attr('id') + '"]').parent().parent();
		var pos = {
			left: trigger.position().left + $('.card-editor').position().left + $('.card-editor-preview-container').position().left + $('.card-editor-preview').position().left,
			top: trigger.position().top + $('.card-editor').position().top + $('.card-editor-preview-container').position().top + $('.card-editor-preview').position().top
		};

		$this.find('select').selectmenu({
			change: function(event, ui){
				trigger.css({ 'font-family': ui.item.value });
			}
		});
		trigger.css({ 'font-family': $this.find('select').val() });
		$this.css({ left: pos.left, top: pos.top+60 });
	});

	$('.card-editor-preview-toolbar').find('a').on('click', function(){
		setToolbarEvents($(this));

		return false;
	});

	$('.changeColorContainer a[href="#close"], .changeSizeContainer a[href="#close"], .changeFontFamilyContainer a[href="#close"]').on('click', function(){
		$(this).parent().removeClass('show');
	});

	if($('.card-background-item.card-background-item--upload').css('background-image') != 'none'){
		$('.card-background-item.card-background-item--upload').click();
	} else if($('.card-background-item.card-background-item-og').css('background-image') != 'none'){
		$('.card-background-item.card-background-item-og').click();
	}

	if(cardChangeTempCode){
		cardChangeTempCode = $('.card-editor-preview-container').html();
		cardChangeTempCode = cardChangeTempCode.replace(/\s/g, '');
	}

	if($('input#referrer_link_card_backgroundWidth').val() != ''){
		$('img#cardBackgroundImage').width($('input#referrer_link_card_backgroundWidth').val());

		if(!tempBgWidth || tempBgWidth == ''){
			tempBgWidth = $('input#referrer_link_card_backgroundWidth').val();
		}
	}

	if($('input#referrer_link_card_backgroundHeight').val() != ''){
		$('img#cardBackgroundImage').height($('input#referrer_link_card_backgroundHeight').val());

		if(!tempBgHeight || tempBgHeight == ''){
			tempBgHeight = $('input#referrer_link_card_backgroundHeight').val();
		}
	}

	if($('input#referrer_link_card_backgroundPosition_x').val() != ''){
		$('img#cardBackgroundImage').css({ left: parseInt($('input#referrer_link_card_backgroundPosition_x').val()) });

		if(!tempBgPosX || tempBgPosX == ''){
			tempBgPosX = $('input#referrer_link_card_backgroundPosition_x').val();
		}
	}

	if($('input#referrer_link_card_backgroundPosition_y').val() != ''){
		$('img#cardBackgroundImage').css({ top: parseInt($('input#referrer_link_card_backgroundPosition_y').val()) });

		if(!tempBgPosY || tempBgPosY == ''){
			tempBgPosY = $('input#referrer_link_card_backgroundPosition_y').val();
		}
	}

	if($('#referrer_link_card_titlePosition_x').val() != ''){
		var posValue = parseInt($('#referrer_link_card_titlePosition_x').val());

		$('.bigger-slogan').css({ left: posValue });

		if(!tempTitlePosX || tempTitlePosX == ''){
			tempTitlePosX = $('#referrer_link_card_titlePosition_x').val();
		}
	}

	if($('#referrer_link_card_titlePosition_y').val() != ''){
		var posValue = parseInt($('#referrer_link_card_titlePosition_y').val());

		$('.bigger-slogan').css({ top: posValue });

		if(!tempTitlePosY || tempTitlePosY == ''){
			tempTitlePosY = $('#referrer_link_card_titlePosition_y').val();
		}
	}

	if($('#referrer_link_card_subtitlePosition_x').val() != ''){
		var posValue = parseInt($('#referrer_link_card_subtitlePosition_x').val());

		$('.smaller-slogan').css({ left: posValue });

		if(!tempSubtitlePosX || tempSubtitlePosX == ''){
			tempSubtitlePosX = $('#referrer_link_card_subtitlePosition_x').val();
		}
	}

	if($('#referrer_link_card_subtitlePosition_y').val() != ''){
		var posValue = parseInt($('#referrer_link_card_subtitlePosition_y').val());

		$('.smaller-slogan').css({ top: posValue });

		if(!tempSubtitlePosY || tempSubtitlePosY == ''){
			tempSubtitlePosY = $('#referrer_link_card_subtitlePosition_y').val();
		}
	}

	if($('#referrer_link_card_titleColor').val() != ''){
		var colorValue = parseInt($('#referrer_link_card_titleColor').val());

		$('.bigger-slogan').css({ top: colorValue });

		if(!tempTitleColor || tempTitleColor == ''){
			tempTitleColor = $('#referrer_link_card_titleColor').val();
		}
	}

	if($('#referrer_link_card_text1Color').val() != ''){
		var colorValue = parseInt($('#referrer_link_card_text1Color').val());

		$('.text1-slogan').css({ top: colorValue });

		if(!tempText1Color || tempText1Color == ''){
			tempText1Color = $('#referrer_link_card_text1Color').val();
		}
	}

	if($('#referrer_link_card_text2Color').val() != ''){
		var colorValue = parseInt($('#referrer_link_card_text2Color').val());

		$('.text2-slogan').css({ top: colorValue });

		if(!tempText2Color || tempText2Color == ''){
			tempText2Color = $('#referrer_link_card_text2Color').val();
		}
	}

	if($('#referrer_link_card_subtitleColor').val() != ''){
		var colorValue = parseInt($('#referrer_link_card_subtitleColor').val());

		$('.smaller-slogan').css({ top: colorValue });

		if(!tempSubtitleColor || tempSubtitleColor == ''){
			tempSubtitleColor = $('#referrer_link_card_subtitleColor').val();
		}
	}

	if($('#referrer_link_card_text1Position_x').val() != ''){
		var posValue = parseInt($('#referrer_link_card_text1Position_x').val());

		$('.text1-slogan').css({ left: posValue });

		if(!tempText1PosX || tempText1PosX == ''){
			tempText1PosX = $('#referrer_link_card_text1Position_x').val();
		}
	}

	if($('#referrer_link_card_text1Position_y').val() != ''){
		var posValue = parseInt($('#referrer_link_card_text1Position_y').val());

		$('.text1-slogan').css({ top: posValue });

		if(!tempText1PosY || tempText1PosY == ''){
			tempText1PosY = $('#referrer_link_card_text1Position_y').val();
		}
	}

	if($('#referrer_link_card_text2Position_x').val() != ''){
		var posValue = parseInt($('#referrer_link_card_text2Position_x').val());

		$('.text2-slogan').css({ left: posValue });

		if(!tempText2PosX || tempText2PosX == ''){
			tempText2PosX = $('#referrer_link_card_text2Position_x').val();
		}
	}

	if($('#referrer_link_card_text2Position_y').val() != ''){
		var posValue = parseInt($('#referrer_link_card_text2Position_y').val());

		$('.text2-slogan').css({ top: posValue });

		if(!tempText2PosY || tempText2PosY == ''){
			tempText2PosY = $('#referrer_link_card_text2Position_y').val();
		}
	}

	setSlogansProps();

	$('input[name="referrer_link_card[showShopLogo]"]:checked').change();

	if ($('.card-editor-preview .social-icons').is('.show')) {
		$('input[name="socialradio"][value="1"]').prop("checked", true);
	} else {
		$('input[name="socialradio"][value="0"]').prop("checked", true);
	}

	if($('.card-editor-preview div.shop-logo').length){
		$('input#referrer_link_card_showShopLogo_0').click();
	} else {
		$('input#referrer_link_card_showShopLogo_1').click();
	}

	if (!initValues["initHtml"]){
		initValues["initHtml"] = $(".card-editor-preview").clone();
	}

	centerCardPreview();

	initDraggableOnAll();

	$('.changeColorContainer').each(function(){
		var $this = $(this);
		var trigger = $('a[href="#' + $this.attr('id') + '"]').parent().parent();
		var pos = {
			left: trigger.position().left + $('.card-editor').position().left + $('.card-editor-preview-container').position().left + $('.card-editor-preview').position().left,
			top: trigger.position().top + $('.card-editor').position().top + $('.card-editor-preview-container').position().top + $('.card-editor-preview').position().top
		};

		if($.fn.farbtastic){
			$this.find('.config_content').farbtastic(function(color){
				trigger.css({ color: color });
				trigger.attr('data-color-hex', color);

				if($this.attr('id') == 'changeColorSmall'){
					$('#referrer_link_card_subtitleColor').val(color);
				}

				if($this.attr('id') == 'changeColorBig'){
					$('#referrer_link_card_titleColor').val(color);
				}
			});
		}
		$this.css({ left: pos.left, top: pos.top+60 });
	});

	$('.changeSizeContainer').each(function(){
		var $this = $(this);
		var trigger = $('a[href="#' + $this.attr('id') + '"]').parent().parent();
		var pos = {
			left: trigger.position().left + $('.card-editor').position().left + $('.card-editor-preview-container').position().left + $('.card-editor-preview').position().left,
			top: trigger.position().top + $('.card-editor').position().top + $('.card-editor-preview-container').position().top + $('.card-editor-preview').position().top
		};

		if(!$this.is('#changeBackgroundSize')) {

			var minVal, maxVal, initialVal;

			if(trigger.is('.slogan-element')){
				minVal = 10;
				maxVal = 70;
				initialVal = parseInt(trigger.css('fontSize'));
			} else if(trigger.is('.social-icons')){
				var social_size_ratio = $('input#referrer_link_card_socialMediaSize').val() || 1;
				var newSocialSize = DEFAULT_SOCIAL_WIDTH*social_size_ratio;

				minVal = 20;
				maxVal = 60;
				initialVal = newSocialSize;
			} else if(trigger.is('.shop-logo')){
				minVal = 20;
				maxVal = 500;
				initialVal = $('img.shop-logo').width();
			}

			$this.find('.config_content').slider({
				min: minVal,
				max: maxVal,
				value: initialVal,
				slide: function (event, ui) {
					if(trigger.is('.slogan-element')){
						trigger.css({'font-size': ui.value});
						trigger.attr({'data-font-size': ui.value});

						if($this.attr('id') == 'changeFontSizeSmall'){
							$('input#referrer_link_card_subtitleSize').val(ui.value);
						} else if ($this.attr('id') == 'changeFontSizeBig'){
							$('input#referrer_link_card_titleSize').val(ui.value);
						} else if ($this.attr('id') == 'changeFontSizeText1'){
							$('input#referrer_link_card_text1Size').val(ui.value);
						} else if ($this.attr('id') == 'changeFontSizeText2'){
							$('input#referrer_link_card_text2Size').val(ui.value);
						}
					} else if(trigger.is('.social-icons')){
						scaleSocialMedia(ui);
					} else if(trigger.is('.shop-logo')){
						$('.shop-logo img').width(ui.value);
						$('input#referrer_link_card_logoSize').val(ui.value);
					}
				}
			});
		}
		$this.css({ left: pos.left, top: pos.top+60 });
	});

	$('.changeFontFamilyContainer').each(function(){
		var $this = $(this);
		var trigger = $('a[href="#' + $this.attr('id') + '"]').parent().parent();
		var pos = {
			left: trigger.position().left + $('.card-editor').position().left + $('.card-editor-preview-container').position().left + $('.card-editor-preview').position().left,
			top: trigger.position().top + $('.card-editor').position().top + $('.card-editor-preview-container').position().top + $('.card-editor-preview').position().top
		};

		$this.find('select').selectmenu({
			change: function(event, ui){
				trigger.css({ 'font-family': ui.item.value });
			}
		});
		trigger.css({ 'font-family': $this.find('select').val() });
		$this.css({ left: pos.left, top: pos.top+60 });
	});
};

// STARTING THE SCRIPT ON PAGE LOAD
$(document).ready(function(){
	screenshotPreview();

	// CHANGE AREA SIZE WITH TIMEOUT ON INPUT
	var recalculateTimeout;
	$('.edit-card-width input, .edit-card-height input').on('input', function(){
		var width = $('.edit-card-width input').val();
		var height = calculateAspectRatio(width, $('label.edit-card-aspect_ratio-block input[checked]').val());

		clearTimeout(recalculateTimeout);
		recalculateTimeout = setTimeout(function () {
			recalculateAreaSize(width, height);
		}, 500);
	});

	// FILL/UNFILL SOCIAL MEDIA ON CARD
	$('input[name="referrer_link_card[socialMedia]"]').on("change", function() {
		setSocialMediaIcons($(this).val());
	});

	// CHANGE BACKGROUND
	$('.card-background-item').on('click', function(){
		var $this = $(this);

		if($this.is('.card-background-item-og') || $this.is('.card-background-item--upload')){
			$('.card-background-item.active').removeClass('active');
		} else {
			$('.card-background-item.active:not(.card-background-item-og):not(.card-background-item--upload)').removeClass('active');
		}
		$this.addClass('active');

		changeAreaBackground($this);
	});

	// FILL SLOGANS
	var fillSloganTimeout;
	$('.card-slogan textarea').on('input', function(){
		clearTimeout(fillSloganTimeout);
		fillSloganTimeout = setTimeout(function(){
			fillSlogan($(".card-slogan.slogan-bigger textarea").val(), $(".card-slogan.slogan-smaller textarea").val(), $(".card-slogan.slogan-text1 textarea").val(), $(".card-slogan.slogan-text2 textarea").val());
		}, 500);
	});

	// RESET ALL
	$('a.reset_area').on('click', function(){
		clearAll();
		fillInitData();
		CARD_CHANGED = false;

		if(tempBgWidth && tempBgWidth != ''){
			$('img#cardBackgroundImage').width(tempBgWidth);
		}

		if(tempBgHeight && tempBgHeight != ''){
			$('img#cardBackgroundImage').height(tempBgHeight);
		}

		if(tempBgPosX && tempBgPosX != ''){
			$('img#cardBackgroundImage').css({ left: parseInt(tempBgPosX) });
		}

		if(tempBgPosY && tempBgPosY != ''){
			$('img#cardBackgroundImage').css({ top: parseInt(tempBgPosY) });
		}

		if(tempTitlePosX && tempTitlePosX != ''){
			var posValue = parseInt(tempTitlePosX);

			$('.bigger-slogan').css({ left: posValue });
		}

		if(tempTitlePosY && tempTitlePosY != ''){
			var posValue = parseInt(tempTitlePosY);

			$('.bigger-slogan').css({ top: posValue });

			if(!tempTitlePosY || tempTitlePosY == ''){
				tempTitlePosY = $('#referrer_link_card_titlePosition_y').val();
			}
		}

		if(tempSubtitlePosX && tempSubtitlePosX != ''){
			var posValue = parseInt(tempSubtitlePosX);

			$('.smaller-slogan').css({ left: posValue });
		}

		if(tempSubtitlePosY && tempSubtitlePosY != ''){
			var posValue = parseInt(tempSubtitlePosY);

			$('.smaller-slogan').css({ top: posValue });
		}

		if(tempTitleColor && tempTitleColor != ''){
			$('.bigger-slogan').css({ color: tempTitleColor });
		}

		if(tempSubtitleColor && tempSubtitleColor != ''){
			$('.smaller-slogan').css({ color: tempSubtitleColor });
		}

		if(tempText1Color && tempText1Color != ''){
			$('.text1-slogan').css({ color: tempText1Color });
		}

		if(tempText2Color && tempText2Color != ''){
			$('.text2-slogan').css({ color: tempText2Color });
		}

		if(tempText1PosX && tempText1PosX != ''){
			var posValue = parseInt(tempText1PosX);

			$('.text1-slogan').css({ left: posValue });
		}

		if(tempText1PosY && tempText1PosY != ''){
			var posValue = parseInt(tempText1PosY);

			$('.text1-slogan').css({ top: posValue });

			if(!tempText1PosY || tempText1PosY == ''){
				tempText1PosY = $('#referrer_link_card_text1Position_y').val();
			}
		}

		if(tempText2PosX && tempText2PosX != ''){
			var posValue = parseInt(tempText2PosX);

			$('.text2-slogan').css({ left: posValue });
		}

		if(tempText2PosY && tempText2PosY != ''){
			var posValue = parseInt(tempText2PosY);

			$('.text2-slogan').css({ top: posValue });

			if(!tempText2PosY || tempText2PosY == ''){
				tempText2PosY = $('#referrer_link_card_text2Position_y').val();
			}
		}

		attachSlogansStyleEvents();

		return false;
	});

	// CHANGE ZOOM IN
	$('a.zoom-in').on('click', function(){
		changeZoom(0.1);
		return false;
	});

	// CHANGE ZOOM OUT
	$('a.zoom-out').on('click', function(){
		changeZoom(-0.1);
		return false;
	});

	// SET BACKGROUND COLOR
	var backgroundInput = $('input#referrer_link_card_backgroundColor');
	if(backgroundInput.length){
		var currColor = backgroundInput.val();

		$('.card-editor-preview').css({ 'background-color': currColor });
	}

	// INIT FARBTASTIC FOR BACKGROUND COLOR
	if($.fn.farbtastic && document.querySelector('#background-picker-container')){
		$('#background-picker-container').farbtastic(function(color){
			$('.card-editor-preview').css({ backgroundColor: color });
			$('.card-editor-preview').attr('data-color-hex', color);

			$('.card-background-item:not(.card-background-item-og):not(.card-background-item--upload)').removeClass('active');

			if(backgroundInput.length){
				backgroundInput.val(color);
			}

			var backgroundPicker = $.farbtastic('#background-picker-container');
			backgroundPicker.setColor(backgroundInput.val());

			checkCardChanges();
		});
	}

	$('.background-color-picker').on('click', function(){
		$('#background-picker-container').addClass('active');

		return false;
	});

	if(currColor === '#ffffff' || currColor === 'rgb(255, 255, 255)') {
		$('.card-background-item').addClass('active');
	}

	$('#background-picker-container').on('mouseleave', function(){
		$(this).removeClass('active');
	});

	// CHANGE ASPECT RATIO ON INIT
	if($('label.edit-card-aspect_ratio-block input[checked]').length){
		var newHeight = calculateAspectRatio($('.edit-card-width input').val(), $('label.edit-card-aspect_ratio-block input[checked]').val());
		$('.edit-card-height input').val(newHeight);
	}

	// CHANGE ASPECT RATIO ON CLICK
	$('label.edit-card-aspect_ratio-block input').on('change', function(){
		var $this = $(this);

		$('label.edit-card-aspect_ratio-block input[checked]').attr('checked', false);
		$this.attr('checked', true);

		var newHeight = calculateAspectRatio($('.edit-card-width input').val(), $this.val());
		$('.edit-card-height input').trigger('input');

		changeZoom(1, true);
	});

	// TOGGLE SHOP LOGO EVENT
	$('input[name="referrer_link_card[showShopLogo]"]').on('change', function(){
		var $this = $(this);

		switch($this.val()){
			case "0":
				$('.card-editor-preview').find('div.shop-logo').remove();
				break;
			case "1":
				if(!$('.card-editor-preview').find('div.shop-logo').length){
					$('.card-editor-preview').append($('div.shop-logo').clone());
					$('.card-editor-preview div.shop-logo').draggable();
					$('.card-editor-preview-toolbar').find('a').on('click', function(){
						setToolbarEvents($(this));
						initDraggableOnAll();

						return false;
					});
				}
				break;
		}
	});

	if($('input#referrer_link_card_logoSize').val() != '')
		$('img.shop-logo').width($('input#referrer_link_card_logoSize').val());

	// VALIDATE WIDTH ON SUBMIT
	$('form[name="referrer_link_card"]').on('submit', function(){
		return validateCardWidth($('input#referrer_link_card_width').val());
	});

	if($('input#referrer_link_card_socialMediaSize').val() != '')
		scaleSocialMedia('', $('input#referrer_link_card_socialMediaSize').val());

	attachSlogansStyleEvents();
	fillInitData();
	if($('.slogan-element').length){
        fillSlogan($(".card-slogan.slogan-bigger textarea").val(), $(".card-slogan.slogan-smaller textarea").val(), $(".card-slogan.slogan-text1 textarea").val(), $(".card-slogan.slogan-text2 textarea").val());
    }

	$('.card-editor-preview > *').each(function(){
		var $this = $(this);

		var currentPicker = $this.find('a.changeColor').attr('href');
		var currentSlider = $this.find('a.changeFontSize').attr('href') || $this.find('a.changeLogoSize').attr('href') || $this.find('a.changeSocialSize').attr('href');
		var currenFontFamilySelect = $this.find('a.changeFontFamily').attr('href');

		var toolbars_scale = $('.card-editor-preview-container').css('zoom') || $('.card-editor-preview-container').css('transform');

		if($('.card-editor-preview-container').css('transform') && $('.card-editor-preview-container').css('transform').indexOf('matrix') > -1){
			toolbars_scale = $('.card-editor-preview-container').css('transform').replace('matrix(', '').replace(')').split(', ')[0];
		}

		var pos = {
			left: ($this.position().left * toolbars_scale) + $('.card-editor').position().left + $('.card-editor-preview-container').position().left + $('.card-editor-preview').position().left,
			top: ($this.position().top * toolbars_scale) + $('.card-editor').position().top + $('.card-editor-preview-container').position().top + $('.card-editor-preview').position().top
		};

		$(currentPicker).css({ left: pos.left + ((($this.outerWidth() * toolbars_scale) - $(currentPicker).outerWidth())/* / 2*/), top: pos.top + ($this.outerHeight() * toolbars_scale) + 30 });
		$(currentSlider).css({ left: pos.left + ((($this.outerWidth() * toolbars_scale) - $(currentSlider).outerWidth())/* / 2*/), top: pos.top + ($this.outerHeight() * toolbars_scale) + 30 });
		$(currenFontFamilySelect).css({ left: pos.left + ((($this.outerWidth() * toolbars_scale) - $(currenFontFamilySelect).outerWidth())/* / 2*/), top: pos.top + ($this.outerHeight() * toolbars_scale) + 30 });
	});

	if($('input[name="referrer_link_card[ratio]"]').length && !$('input[name="referrer_link_card[ratio]"]:checked').length){
		$('input[name="referrer_link_card[ratio]"][value="16:9"]').click();
	}




	// JS FOR PRICING PLANS

	if($('.pricing').length){
		/*$('.pricing-tabs li').on('click', function(){
			var $this = $(this);

			$this.siblings('li').removeClass('active');
			$this.addClass('active');

			$('.pricing.active').removeClass('active');
			$($this.find('a').attr('href')).addClass('active');

			return false;
		});*/

		$('a.showdetails-button').on('click', function(){
			var $this = $(this);

			$this.hide();
			$this.siblings('div').removeClass('hidden');

			return false;
		});
	}

	var copyToClipboard = function (inputElem) {
		inputElem.select();

		document.execCommand("copy");
	};

	/*$('button#copy_companyName_to_clipboard').on('click', function(){
		copyToClipboard(document.querySelector('input#edit_shop_budget_companyNameId'));

		return false;
	});

	$('button#copy_accountNumber_to_clipboard').on('click', function(){
		copyToClipboard(document.querySelector('input#edit_shop_budget_bankAccountNumber'));

		return false;
	});*/

	$('a[href="#showPaymentDetails"]').on('click', function(){
		$('.payment-details').removeClass('hidden');

		$(this).remove();

		return false;
	});

	if($(window).innerWidth() > 767 && $(window).innerHeight() > $('#app').outerHeight()){
		$('footer').addClass('fixed-bottom');
	} else if ($(window).innerWidth() <= 767 && $(window).innerHeight() > $('#app').outerHeight() && $('.error-404').length){
		$('footer').addClass('fixed-bottom');
	}

	if($('.mystore--referrable-links__builder__preview__content p').length){
		$('.mystore--referrable-links__builder__preview__content p').each(function(){
			$(this).html($(this).text());
		});
	}

	$('.clipboard-copy').on('click', function(){
		var $this = $(this);

		$this.addClass('clicked');
		$this.text('Copied to clipboard');
	});

	$('.customize-row .social-icons a').on('click', function(){
		return false;
	});

	var scaleMyStoreImage = function(){

		$('.mystore--referrable-links__builder__preview').each(function(){
			var $this = $(this);

			var currWidth = $this.width();
			$this.find('.mystore--referrable-links__builder__preview__img-container').height(currWidth);

			var newScale = currWidth/300;

			$this.find('.mystore--referrable-links__builder__preview__img-container > *').css('transform', 'scale('+newScale+')');
		});

	}

	if($(window).innerWidth() <= 767){
		scaleMyStoreImage();
	}

	$(window).resize(function(){
		if($(window).innerWidth() <= 767){
			scaleMyStoreImage();
		} else {
			$('.mystore--referrable-links__builder__preview__img-container').height(300);
			$('.mystore--referrable-links__builder__preview__img-container > *').css({ 'transform': 'scale(1)' });
		}
	})

	if($('.login-register-page').length){
		var newLoginRegisterPos = ($(window).innerHeight() - $('footer.footer').outerHeight() - $('.login-register-page').outerHeight())/2;

		$('.login-register-page').css({ 'margin-top': newLoginRegisterPos >= 0 ? newLoginRegisterPos : 0 });

		$(window).on('resize', function(){
			var newLoginRegisterPos = ($(window).innerHeight() - $('footer.footer').outerHeight() - $('.login-register-page').outerHeight())/2;

			$('.login-register-page').css({ 'margin-top': newLoginRegisterPos >= 0 ? newLoginRegisterPos : 0 });
		});
	}

	$('.notification-toggle-button').on('click', function(){
		$($(this).data('target')).show();
		$(this).addClass('hidden');
		window.localStorage.removeItem('globalNotification');
	});

	$('.notification-close').on('click', function(){
		window.localStorage.setItem('globalNotification', 'hidden');
		$(this).parent().hide();
		$(this).parent().siblings('.notification-toggle-button').removeClass('hidden');

		return false;
	});
});

window.addEventListener('load', function() {
	if (document.querySelector('.companyMenu')) {
		const windowLocation = window.location.search
		const searchParams = new URLSearchParams(windowLocation)

		const hasBalance = searchParams.has('hasBalance')
		const toggleButton = searchParams.has('toggleButton')
		const testedConnection = searchParams.has('testedConnection')
		const referralsNodesComplete = searchParams.has('referralsNodesComplete')

		if (hasBalance) {
			document.querySelectorAll('.companyMenu__heading__btn__affiliate').forEach((el) => {
				el.style.opacity = '1'
			})
		}

		if (toggleButton) {
			document.querySelectorAll('.companyMenu__heading__btn__referral').forEach((el) => {
				el.style.opacity = '1'
			})
		}

		if (!testedConnection) {
			document.querySelector('.companyMenu__boxes__box__btn__referral').style.borderColor = 'grey'
			document.querySelector('.companyMenu__boxes__box__btn__referral').style.color = 'grey'
			$('[data-toggle="tooltip"]').tooltip()
		}

		if (testedConnection) {
			document.querySelector('.companyMenu__boxes__box__btn__company').innerHTML = 'Preview'
		}

		if (referralsNodesComplete) {
			document.querySelector('.companyMenu__boxes__box__btn__referral').innerHTML = 'Preview'
		}
	}
})

