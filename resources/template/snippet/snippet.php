<?php ?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>SNIPPET</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
        <meta name="og:image" content="https://mediamarkt.pl/media/cache/resolve/gallery/images/20/20982476/apple-iphone-11-czarny-1.jpg">
        <meta name="og:description" content="Test description...">

        <script src='http://marfil-app.local:8042/snippet/ValueLink.js'></script>
        <script>const ValueLinkID = '7ECAEAFEB3FBD04753D6';</script>



        <meta name="app_url" content="//marfil-app.local">
    </head>

    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
       <style>
        
            body{
                display: flex;
                background: #c6c6c6;
                align-items: center;
                justify-content: center;
            }

            body > *{
                padding: 25px;
                /*width: 50%;*/
                border: 1px solid white;
                border-radius: 5px;
            }

            .purchase,
            .signup{
                display: flex;
                justify-content: center;
                align-items: center;
                flex-direction: column;
                margin:0 15px;
            }

            .purchase input[type='text'],
            .signup input[type='text']{
               /* width: 50%;*/
                height: 35px;
                margin: 5px 0;
                padding: 0 10px;
                border-radius: 15px;
                border: 1px solid white;
            }

            .purchase div,
            .signup div{
                padding: 5px 15px;
                border: 1px solid white;
                background-color: lightgray;
                cursor: pointer;
                margin-top: 15px;
            }
        </style>


        <div class="purchase">
            <input type="text" placeholder="Product" id="pProduct">
            <input value="USD" type="text" placeholder="Currency" id="pCurrency">
            <input type="checkbox" id="pTest">Test
            <input value="1" type="text" placeholder="Amount" id="pAmount">
            <input value="100" type="text" placeholder="Price" id="pPrice">
            <input type="text" placeholder="Coupon" id="pCoupon">

            <div id="buy-product">BUY PRODUCT</div>
        </div>


        <div class="signup">

            <input type="checkbox" id="sTest">Test
            <div id="register">REGISTER </div>
        </div>

        <script>
            document.getElementById('buy-product').onclick = function(e) {

                var pProduct = document.querySelector('#pProduct').value;
                var pAmount = document.querySelector('#pAmount').value;
                var pCurrency = document.querySelector('#pCurrency').value;
                var pPrice = document.querySelector('#pPrice').value;
                var pTest = document.querySelector('#pTest').checked;
                var pCoupon = document.querySelector('#pCoupon').value;
                ValueLink.push({
                    "event" : "purchase",
                    "test" : pTest,
                    "data": {
                        "currency": pCurrency,
                        "product": pProduct,
                        "price": pPrice,
                        "amount": pAmount,
                        "coupon": pCoupon
                    }
                });
            };

            document.getElementById('register').onclick = function(e) {
                var sTest = document.querySelector('#sTest').checked;
                ValueLink.push({
                    "event" : "signup",
                    "test" : sTest
                });
            };
        </script>

    </body>
</html>
