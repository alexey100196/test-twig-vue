<?php

namespace App\Controller\Ajax;

use App\Repository\PushNotificationRepository;
use App\Repository\ShopRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\AjaxAbstractController;

class PushNotificationController extends AjaxAbstractController
{
    /**
     * @Route("/ajax/push-notifications", name="ajax_push-notifications_index")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function index(PushNotificationRepository $notificationRepository, ShopRepository $shopRepository, Request $request)
    {
        $data = [];

        $shop = $shopRepository->find($request->query->getInt('shop_id'));

        $notifications = $notificationRepository->getAll($shop ? $shop->getUser() : null);

        foreach ($notifications as $notification) {
            $data[] = [
                'icon' => $notification->getIcon(),
                'text' => $notification->getTextWithVariables(),
                'type' => $notification->getType()
            ];
        }

        return new JsonResponse($data);
    }
}