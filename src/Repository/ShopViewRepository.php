<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\Shop;
use App\Entity\ShopView;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShopView|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShopView|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShopView[]    findAll()
 * @method ShopView[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopViewRepository extends ServiceEntityRepository
{
    /**
     * ShopViewRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShopView::class);
    }

    /**
     * @param User $referrer
     * @param ArrayCollection $shops
     * @param \DateTime|null $startDate
     * @param \DateTime|null $endDate
     * @return mixed
     */
    public function getReferrerShopsViews(
        User $referrer,
        ArrayCollection $shops,
        \DateTime $startDate = null,
        \DateTime $endDate = null
    )
    {
        $query = $this->createQueryBuilder('sv')
            ->select('s as shop, MAX(sv.createdAt) as createdAt, COUNT(sv.id) as viewsCount')
            ->where('sv.referrer = :referrer')
            ->setParameter('referrer', $referrer)
            ->leftJoin(Shop::class, 's', Join::WITH, 'sv.shop = s')
            ->andWhere('sv.shop in (:shops)')->setParameter('shops', $shops);

        return $this->addDateFiltersToQueryBuilder($query, $startDate, $endDate)
            ->groupBy('sv.shop')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param User $referrer
     * @param ArrayCollection $products
     * @param \DateTime|null $startDate
     * @param \DateTime|null $endDate
     * @return mixed
     */
    public function getReferrerProductsViews(
        User $referrer,
        ArrayCollection $products,
        \DateTime $startDate = null,
        \DateTime $endDate = null
    )
    {
        $query = $this->createQueryBuilder('sv')
            ->select('p as product, MAX(sv.createdAt) as createdAt, COUNT(sv.id) as viewsCount')
            ->where('sv.referrer = :referrer')
            ->setParameter('referrer', $referrer)
            ->leftJoin(Product::class, 'p', Join::WITH, 'sv.product = p')
            ->andWhere('sv.product in (:products)')->setParameter('products', $products);

        return $this->addDateFiltersToQueryBuilder($query, $startDate, $endDate)
            ->groupBy('sv.product')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param QueryBuilder $query
     * @param \DateTime|null $startDate
     * @param \DateTime|null $endDate
     * @return QueryBuilder
     */
    private function addDateFiltersToQueryBuilder(QueryBuilder $query, \DateTime $startDate = null, \DateTime $endDate = null): QueryBuilder
    {
        if ($startDate) {
            $query->andWhere('sv.createdAt >= :startDate')
                ->setParameter('startDate', $startDate);
        }

        if ($endDate) {
            $query->andWhere('sv.createdAt <= :endDate')
                ->setParameter('endDate', $endDate);
        }

        return $query;
    }
}
