<?php


namespace App\Service\IncomeStatistics\ReferrerIncomeStatistics;


use App\Service\IncomeStatistics\ReferrerIncomeStatistics\Commands\GenerateShopStats;
use App\Service\IncomeStatistics\ReferrerIncomeStatistics\Commands\GenerateReferrerLinkStats;
use App\Service\IncomeStatistics\ReferrerIncomeStatistics\Commands\GenerateShopsSummaryStats;
use App\Service\IncomeStatistics\ReferrerIncomeStatistics\Handlers\ShopStatsHandler;
use App\Service\IncomeStatistics\ReferrerIncomeStatistics\Handlers\ReferrerLinkStatsHandler;
use App\Service\IncomeStatistics\ReferrerIncomeStatistics\Handlers\ShopsStatsSummaryHandler;
use App\Service\IncomeStatistics\StatisticsService;

class ReferrerStatisticsService extends StatisticsService
{
    /**
     * @var array
     */
    protected $handlers = [
        GenerateShopStats::class => ShopStatsHandler::class,
        GenerateReferrerLinkStats::class => ReferrerLinkStatsHandler::class,
        GenerateShopsSummaryStats::class => ShopsStatsSummaryHandler::class,
    ];
}