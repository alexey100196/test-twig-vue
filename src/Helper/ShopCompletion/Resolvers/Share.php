<?php

namespace App\Helper\ShopCompletion\Resolvers;

use App\Helper\ShopCompletion\Resolvers\AbstractResolver;

class Share extends AbstractResolver
{
    protected $result = 0;

    public function resolve()
    {
        if ($this->getResolver()->getResult('Preview')->getScore() == 100 && $this->shop->getUrlName()) {
            $this->addScore(100);
        }
    }
}
