<?php


namespace App\Controller\Ajax;


use App\Controller\AjaxAbstractController;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Repository\ShopConversionRepository;
use App\Repository\ShopRepository;
use App\Service\IncomeStatistics\ReferrerIncomeStatistics\Commands\GenerateReferrerLinkStats;
use App\Service\IncomeStatistics\ReferrerIncomeStatistics\Commands\GenerateShopsSummaryStats;
use App\Service\IncomeStatistics\ReferrerIncomeStatistics\Commands\GenerateShopStats;
use App\Service\IncomeStatistics\ReferrerIncomeStatistics\ReferrerStatisticsService;
use App\Service\Referrer\ReferrerShop;
use App\ValueObject\DatePeriod;
use Carbon\Carbon;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ReferrerIncomeController extends AjaxAbstractController
{
    /**
     * @var ReferrerStatisticsService
     */
    private $referrerStatisticsService;

    /**
     * @var ShopConversionRepository
     */
    private $shopConversionRepository;

    /**
     * ReferrerIncomeController constructor.
     * @param ReferrerStatisticsService $referrerStatisticsService
     * @param ShopConversionRepository $shopConversionRepository
     */
    public function __construct(ReferrerStatisticsService $referrerStatisticsService, ShopConversionRepository $shopConversionRepository)
    {
        $this->referrerStatisticsService = $referrerStatisticsService;
        $this->shopConversionRepository = $shopConversionRepository;
    }

    /**
     * @Route("/ajax/referrer/income/companies", name="ajax_referrer_income_shops_index")
     * @param Request $request
     * @param ShopRepository $shopRepository
     * @return JsonResponse
     * @throws \Exception
     */
    public function shopsIndex(Request $request, ShopRepository $shopRepository, ReferrerShop $referrerShop)
    {
        $datePeriod = $this->buildDatePeriodFromDateStrings($request->query->get('start_at'), $request->query->get('end_at'));

        if ($request->query->getBoolean('allShops')) {
            $userShops = $referrerShop->getReferrerShops($this->getUser());
            $shops = [];
            foreach ($userShops as $s) {
                $shops[] = $s->getId();
            }
            $shops = $shopRepository->findById($shops);
        } else {
            $shops = $shopRepository->findById((array)$request->query->get('shops'));
        }

        $stats = $this->referrerStatisticsService->generate(
            new GenerateShopsSummaryStats(
                new ArrayCollection($shops),
                $this->getUser(),
                $datePeriod->getStart(),
                $datePeriod->getEnd(),
                $request->query->get('filterBy', '')
            )
        );

        $response = $stats->map(function ($item) {
            return $item->toArray();
        })->toArray();

        if ((bool) $request->query->getBoolean('withDaily')) {
            $daily = [];
            $newStart = clone $datePeriod->getStart();
            $newEnd = clone $datePeriod->getStart();
            $newEnd->modify('+1 day');
            do {
                $command = new GenerateShopsSummaryStats(
                    new ArrayCollection($shops),
                    $this->getUser(),
                    $newStart,
                    $newEnd,
                    $request->query->get('filterBy', '')
                );

                $res = $this->referrerStatisticsService->generate($command)->map(function ($item) {
                    return $item->toArray();
                })->toArray();
                if (!empty($res)) $daily[$newEnd->format('d')] = $res;
                $newStart->modify('+1 day');
                $newEnd->modify('+1 day');
            } while(
                ((int) $newStart->format('d')) <= ((int) $datePeriod->getEnd()->format('d')) &&
                ((int) $newStart->format('m')) === ((int) $datePeriod->getEnd()->format('m'))
            );

            $response['daily'] = $daily;
            return new JsonResponse($response);
        }

        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/ajax/referrer/income/company/{id}", name="ajax_referrer_income_shop_index")
     * @param Request $request
     * @return JsonResponse
     */
    public function shopStatsIndex(Request $request, $id)
    {
        $shop = $this->getDoctrine()->getRepository(Shop::class)->find($id);
        $datePeriod = $this->buildDatePeriodFromDateStrings($request->query->get('start_at'), $request->query->get('end_at'));

        $stats = $this->referrerStatisticsService->generate(
            new GenerateShopStats($shop, $this->getUser(), $datePeriod->getStart(), $datePeriod->getEnd(), $request->query->get('filterBy', ''))
        );

        $response = $stats->map(function ($item) {
            return $item->toArray();
        })->toArray();

        return new JsonResponse($response, 200);
    }


    /**
     * @Route("/ajax/referrer/income/referrer-link/{id}", name="ajax_referrer_income_referrer_link_index")
     * @param Request $request
     * @return JsonResponse
     */
    public function shopReferrerLinkIndex(Request $request, $id)
    {
        $datePeriod = $this->buildDatePeriodFromDateStrings($request->query->get('start_at'), $request->query->get('end_at'));
        /** @var ReferrerLink $referrerLink */
        $referrerLink = $this->getDoctrine()->getRepository(ReferrerLink::class)->find($id);

        if ($referrerLink->getReferrer() !== $this->getUser()) {
            throw new NotFoundHttpException('Invalid referrer link');
        }

        $stats = $this->referrerStatisticsService->generate(
            new GenerateReferrerLinkStats($referrerLink, $datePeriod->getStart(), $datePeriod->getEnd())
        );

        $response = $stats->map(function ($item) {
            return $item->toArray();
        })->toArray();

        return new JsonResponse($response, 200);
    }

    /**
     * @Route("/ajax/referrer/income/referrer-link/{id}/exchange-rates", name="ajax_referrer_income_referrer_link_exchange_rates_index")
     * @param Request $request
     * @return JsonResponse
     */
    public function shopReferrerLinkExchangeRatesIndex(Request $request, $id)
    {
        // @todo: check access
        $datePeriod = $this->buildDatePeriodFromDateStrings($request->query->get('start_at'), $request->query->get('end_at'));

        /** @var ReferrerLink $referrerLink */
        $referrerLink = $this->getDoctrine()->getRepository(ReferrerLink::class)->find($id);

        if ($referrerLink->getReferrer() !== $this->getUser()) {
            throw new NotFoundHttpException('Invalid referrer link');
        }

        $price = $request->query->get('purchase_price');

        $data = $this->shopConversionRepository->getConversionsRatesForReferrerLink(
            $referrerLink,
            $datePeriod->getStart(),
            $datePeriod->getEnd(),
            $request->query->getAlpha('offer_type'),
            $request->query->getAlpha('commission_type'),
            (float)$request->query->get('commission'),
            $request->query->get('source_link'),
            $price ? (float) $price : null
        );

        return new JsonResponse($data);
    }

    /**
     * @param string $startDate
     * @param string $endDate
     * @return DatePeriod
     */
    private function buildDatePeriodFromDateStrings(string $startDate, string $endDate): DatePeriod
    {
        return new DatePeriod((new Carbon($startDate))->startOfDay(), (new Carbon($endDate))->endOfDay());
    }
}
