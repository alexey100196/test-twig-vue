<?php


namespace App\Service\Coupon;

use App\Entity\Coupon\Coupon;
use App\Entity\Coupon\CouponDiscountType;
use App\Entity\Coupon\CouponPackage;
use App\Entity\Shop;
use App\Form\Shop\Widget\FormData\CouponPackage as CouponPackageRequest;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CreateCouponPackage
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function create(
        array $coupons,
        float $discount,
        CouponDiscountType $discountType,
        bool $multipleUsage,
        Shop $shop,
        int $allocation
    )
    {
        $this->validateCodes($coupons);
        $shop->getCouponPackagesWithCoupons();
        $package = $this->createCouponPackage($discount, $discountType, $multipleUsage, $shop, $allocation);
        $package->addCoupons($this->createCouponsFromArrayData($coupons));
        try {
            $this->em->persist($package);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $exception) {
            throw new \LogicException('Invalid CSV format. Code must be unique.');
        }

        return $package;
    }

    public function createFromCsv(
        UploadedFile $csv,
        float $discount,
        CouponDiscountType $discountType,
        bool $multipleUsage,
        Shop $shop,
        int $allocation
    )
    {

        $data = $this->readCodesFromCVS($csv);

        $this->create(
            $data,
            $discount,
            $discountType,
            $multipleUsage,
            $shop,
            $allocation
        );
    }

    private function createCouponPackage(
        float $discount,
        CouponDiscountType $discountType,
        bool $multipleUsage,
        Shop $shop,
        int $allocation
    )
    {
        $package = new CouponPackage();
        $package->setDiscount($discount);
        $package->setDiscountType($discountType);
        $package->setMultipleUsage($multipleUsage);
        $package->setShop($shop);
        $package->setAllocation($allocation);

        return $package;
    }

    private function validateCodes($data)
    {
        if (count($data) === 0) {
            throw new \LogicException('Invalid CSV format. No codes provided.');
        }

        foreach ($data as $item) {
            if (strlen($item) > Coupon::CODE_MAX_LENGTH) {
                throw new \LogicException('Invalid CSV format. Maximum length of code is ' . Coupon::CODE_MAX_LENGTH . ' characters.');
            }
        }
    }

    private function createCouponsFromArrayData(array $data)
    {
        return array_map(function ($item) {
            $coupon = new Coupon();
            $coupon->setCode($item);
            return $coupon;
        }, $data);
    }

    private function readCodesFromCVS(UploadedFile $csv)
    {
        $data = [];

        $file = fopen($csv->getRealPath(), 'r');

        while (!feof($file)) {
            $data[] = fgetcsv($file);
        }

        return array_filter(array_column($data, '0'));
    }
}