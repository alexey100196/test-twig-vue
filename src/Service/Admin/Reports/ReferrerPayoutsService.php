<?php


namespace App\Service\Admin\Reports;


use App\Entity\Shop;
use App\Repository\ShopConversionRepository;
use App\Repository\UserBalancePayoutRepository;
use App\Repository\UserRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;

class ReferrerPayoutsService
{
    /**
     * @var ShopConversionRepository
     */
    private $shopConversionRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @see services.yaml (min_referrer_payout)
     * @var float
     */
    private $minPayout;

    /**
     * @see services.yaml (referrer_payout_addition_months)
     * @var int
     */
    private $referrerPayoutAdditionMonths;

    /**
     * @var UserBalancePayoutRepository
     */
    private $userBalancePayoutRepository;

    public function __construct(
        ShopConversionRepository $shopConversionRepository,
        EntityManagerInterface $em,
        UserRepository $userRepository,
        UserBalancePayoutRepository $userBalancePayoutRepository,
        float $minPayout,
        int $referrerPayoutAdditionMonths
    )
    {
        $this->shopConversionRepository = $shopConversionRepository;
        $this->userRepository = $userRepository;
        $this->userBalancePayoutRepository = $userBalancePayoutRepository;
        $this->em = $em;
        $this->minPayout = $minPayout;
        $this->referrerPayoutAdditionMonths = $referrerPayoutAdditionMonths;
    }

    /**
     * @param Shop $shop
     * @param \DateTime $start
     * @param \DateTime $end
     * @return ReferrerPayoutsCollection
     */
    public function getShopReferrersPayouts(Shop $shop, \DateTime $start, \DateTime $end): ReferrerPayoutsCollection
    {
        $rows = $this->shopConversionRepository->getShopReferrersPayouts($shop, $start, $end);

        $payouts = new ReferrerPayoutsCollection();

        foreach ($rows as $item) {
            $payouts->add(
                new ReferrerPayout(
                    $item['referrerEmail'],
                    (string)$item['amount'],
                    $item['referrerName'],
                    $item['referrerSurname'],
                    $item['currency']
                )
            );
        }

        return $payouts;
    }

    public function updateShopLastMonthPendingPayouts()
    {
        $start = Carbon::now()->subMonth()->startOfMonth();
        $end = Carbon::now()->subMonth()->endOfMonth();

        $rows = $this->shopConversionRepository->getShopsTotalReferrerPayouts($start, $end);

        foreach ($rows as $item) {
            $item['shop']->setLastMonthPayout((float)$item['amount']);
        }

        $this->em->flush();
    }

    public function getReferrersPayouts(\DateTime $start, \DateTime $end): ReferrerPayoutsCollection
    {
        $payouts = $this->userBalancePayoutRepository->findAllAcceptedForDatePeriod($start, $end);
        $payoutsResult = new ReferrerPayoutsCollection();

        foreach ($payouts as $payout) {
            $user = $payout->getBalance()->getUser();
            $currency = $payout->getBalance()->getCurrency();

            $payoutsResult->add(new ReferrerPayout($user->getEmail(), (string)$payout->getAmount(), $user->getName(), $user->getSurname(), $currency->getName()));
        }

        return $payoutsResult;
    }

    public function updateUsersLastMonthIncome(): void
    {
        $start = Carbon::now()->subMonth()->startOfMonth();
        $end = Carbon::now()->subMonth()->endOfMonth();

        $rows = $this->shopConversionRepository->getReferrersPayouts($start, $end);

        foreach ($rows as $item) {
            if ($user = $this->userRepository->find($item['referrerId'])) {
                $user->setLastMonthIncome((float)$item['amount']);
            }
        }

        $this->em->flush();
    }

}
