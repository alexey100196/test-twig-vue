var gulp = require('gulp');
var sass = require('gulp-sass');
var notify = require('gulp-notify');
var sourcemaps = require('gulp-sourcemaps');
var prefix = require('gulp-autoprefixer');
var path = require('path');
var info = require('./dige/info.json');
var gulpif = require('gulp-if');
var cleanCSS = require('gulp-clean-css');
var data = require('gulp-data');
var nunjucks = require('gulp-nunjucks');
var ext_replace = require('gulp-ext-replace');
var babel = require('gulp-babel');
var rename = require("gulp-rename");
var minify = require('gulp-minify');

function gulp_compile__sass(dev, cb){
    let pipe = gulp.src(info.paths.raw.sass)
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: info.packages.sass,
            onError: function(err) {
                return notify({title: info.projectname, icon: path.join(__dirname, 'logo.jpg') }).write(err);
            }
        }))
        .pipe(prefix(info.compatibility))
        .pipe(gulpif(dev, sourcemaps.write()))
        .pipe(gulpif(!dev, cleanCSS()))
        .pipe(gulp.dest(info.paths.compiled.sass))
        .pipe(notify({
            title: info.projectname,
            message: info.messages.sass_compiled,
            icon: path.join(__dirname, 'logo.jpg')
        }));
     if (cb !== undefined){
	pipe.on('finish', cb)
     }
}

function gulp_widget_compile(){
  gulp.src("../js/widget-init.js")
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(minify())
    .pipe(gulp.dest("../snippet"))
    .pipe(notify({
      title: info.projectname,
      message: 'Compiled widget js',
      icon: path.join(__dirname, 'logo.jpg')
    }));
}

function gulp_nunjucks_compile(){

  var getJsonData = function(file) {
    return require('../templates/dataFile.json');
  };

  gulp.src(["../templates/pages/**/*.nunjucks", "!../templates/pages/partials/**/*.nunjucks"])
    .pipe(data(getJsonData))
    .pipe(nunjucks.compile())
    .pipe(ext_replace('.html'))
    .pipe(rename({dirname: ''}))
    .pipe(gulp.dest('../'))
    .pipe(notify({
      title: info.projectname,
      message: 'Templates compiled',
      icon: path.join(__dirname, 'logo.jpg')
    }));
}

gulp.task('nunjucks', function() {
  gulp_nunjucks_compile();
});

gulp.task('widgets', function() {
  gulp_widget_compile();
});

gulp.task('widgets:watch', function() {
  gulp_widget_compile();
  gulp.watch('../js/widget-init.js', function(){gulp_widget_compile()});
});


gulp.task('scss', function(cb) {
    gulp_compile__sass(true, cb);
});

gulp.task('default', function(cb) {
    gulp_compile__sass(true);
    gulp_nunjucks_compile();

  gulp.watch(info.paths.raw.sass, function(){gulp_compile__sass(true)});
  gulp.watch(['../templates/pages/**/*.nunjucks', "../templates/pages/**/*.jinja"], function(){gulp_nunjucks_compile()});
  gulp.watch('../templates/dataFile.json', function(){gulp_nunjucks_compile()});
});
