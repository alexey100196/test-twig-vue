<?php

namespace App\DataFixtures;

use App\DataFixtures\BaseGlossaryFixtures;
use App\Entity\Role;

class RoleFixtures extends BaseGlossaryFixtures
{
    public function getData(): array
    {
        return [
            Role::ROLE_SHOP_ID => Role::ROLE_SHOP,
            Role::ROLE_REFERRER_ID => Role::ROLE_REFERRER,
            Role::ROLE_ADMIN_ID => Role::ROLE_ADMIN,
            Role::ROLE_FULL_PROFILE_ID => Role::ROLE_FULL_PROFILE,
        ];
    }

    public function getEntity()
    {
        return new Role();
    }
}
