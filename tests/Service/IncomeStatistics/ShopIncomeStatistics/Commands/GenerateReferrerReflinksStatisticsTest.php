<?php


namespace App\Tests\Service\IncomeStatistics\ShopIncomeStatistics\Commands;


use App\Entity\Shop;
use App\Entity\User;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateReferrerReflinksStatistics;
use PHPUnit\Framework\TestCase;

class GenerateReferrerReflinksStatisticsTest extends TestCase
{
    public function testCommand()
    {
        $start = new \DateTime('2018-01-01 10:01:01');
        $end = new \DateTime('2019-02-01 12:00:01');
        $shop = new Shop();
        $referrer = new User();

        $command = new GenerateReferrerReflinksStatistics($shop, $referrer, $start, $end);

        $this->assertEquals($start, $command->getStartDate());
        $this->assertEquals($end, $command->getEndDate());
        $this->assertEquals($shop, $command->getShop());
        $this->assertEquals($referrer, $command->getReferrer());
    }
}