<?php


namespace App\Tests\Service\IncomeStatistics\ShopIncomeStatistics\Commands;


use App\Entity\ReferrerLink;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateReferrerLinkStatistics;
use PHPUnit\Framework\TestCase;

class GenerateReferrerLinkStatisticsTest extends TestCase
{
    public function testCommand()
    {
        $start = new \DateTime('2018-01-01 10:01:01');
        $end = new \DateTime('2019-02-01 12:00:01');
        $referrerLink = new ReferrerLink();

        $command = new GenerateReferrerLinkStatistics($referrerLink, $start, $end);

        $this->assertEquals($start, $command->getStartDate());
        $this->assertEquals($end, $command->getEndDate());
        $this->assertEquals($referrerLink, $command->getReferrerLink());
    }
}