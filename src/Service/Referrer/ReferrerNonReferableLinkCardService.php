<?php


namespace App\Service\Referrer;

use App\Entity\ReferrerStoreNonReferableLink;
use App\Entity\ReferrerNonReferableLinkCard;
use App\Repository\ReferrerNonReferableLinkCardRepository;
use Doctrine\ORM\EntityManagerInterface;

class ReferrerNonReferableLinkCardService
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var ReferrerNonReferableLinkCardRepository */
    private $referrerNonReferableLinkCardRepository;

    public function __construct(
        EntityManagerInterface $em,
        ReferrerNonReferableLinkCardRepository $referrerNonReferableLinkCardRepository
    )
    {
        $this->em = $em;
        $this->referrerNonReferableLinkCardRepository = $referrerNonReferableLinkCardRepository;

    }

    /**
     * @param ReferrerNonReferableLinkCard $card
     * @throws \Exception
     */
    public function save(ReferrerNonReferableLinkCard $card)
    {
        $this->em->persist($card);
        $this->em->flush();
    }

    /**
     * @param ReferrerNonReferableLinkCard|null $referrerLinkCard
     * @return array
     */
    public function convertObjectToArray(?ReferrerNonReferableLinkCard $referrerLinkCard) {
        return $referrerLinkCard ? [
            'width' => $referrerLinkCard->getWidth(),
            'socialMedia' => $referrerLinkCard->getSocialMedia(),
            'name' => $referrerLinkCard->getName(),
            'url' => $referrerLinkCard->getUrl(),
            'logo' => $referrerLinkCard->getLogo(),
            'title' => $referrerLinkCard->getTitle(),
            'description' => $referrerLinkCard->getDescription(),
            'image' => $referrerLinkCard->getImage(),
            'iframe' => $referrerLinkCard->getIframe(),
            'layout' => $referrerLinkCard->getLayout(),
            'showShopLogo' => $referrerLinkCard->getShowShopLogo(),
            'showCompanyName' => $referrerLinkCard->getShowCompanyName(),
            'show3DEffect' => $referrerLinkCard->getShow3DEffect(),
            'showVideo' => $referrerLinkCard->getShowVideo()
        ] : [];
    }
}