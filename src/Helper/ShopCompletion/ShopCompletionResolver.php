<?php

namespace App\Helper\ShopCompletion;

use App\Entity\Shop;
use App\Helper\ShopCompletion\Resolvers\BasicData;

class ShopCompletionResolver
{
    /**
     * @var Shop
     */
    protected $shop;

    /**
     * @var \AbstractResolver[]
     */
    protected $resolvers = [
        \App\Helper\ShopCompletion\Resolvers\BasicData::class,
        \App\Helper\ShopCompletion\Resolvers\CookieLifespan::class,
        \App\Helper\ShopCompletion\Resolvers\MainOffer::class,
        \App\Helper\ShopCompletion\Resolvers\OptionalOffer::class,
        \App\Helper\ShopCompletion\Resolvers\Snipset::class,
        \App\Helper\ShopCompletion\Resolvers\Budget::class,
        \App\Helper\ShopCompletion\Resolvers\Preview::class,
        \App\Helper\ShopCompletion\Resolvers\TestConnection::class,
        \App\Helper\ShopCompletion\Resolvers\ProductNumber::class,
        \App\Helper\ShopCompletion\Resolvers\Subdomain::class,
        \App\Helper\ShopCompletion\Resolvers\Finish::class,
        \App\Helper\ShopCompletion\Resolvers\Share::class,
    ];

    /**
     * @var array
     */
    protected $results = [];

    /**
     * @var int
     */
    protected $completedStepsCount = 0;

    /**
     * @var int
     */
    protected $completionPercent = 0;

    /**
     * @param Shop $shop
     */
    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
    }

    /**
     * Run resolvers
     * @return void
     */
    public function resolve(): void
    {
        foreach ($this->resolvers as $resolver) {
            $instance = $resolver::getInstance($this->shop);
            $instance->setResolver($this);
            $instance->resolve();
            $result = $instance->getResult();
            $this->results[$result->getName()] = $result;
        }

        $this->resolved();
    }

    /**
     * Get all results.
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * Get single result.
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function getResult(string $name)
    {
        $this->validateResolver($name);

        return $this->results[$name];
    }

    public function allBeforeCompleted(string $name): bool
    {
        $this->validateResolver($name);

        foreach ($this->getResults() as $result) {
            if ($name === $result->getName()) {
                break;
            }

            if (!$result->isComplete()) {
                return false;
            }
        }

        return true;
    }


    /**
     * @return int
     */
    public function getStepsCount()
    {
        return count($this->resolvers);
    }

    /**
     * @return mixed
     */
    public function getCompletedStepsCount()
    {
        return $this->completedStepsCount;
    }

    /**
     * @return int|float
     */
    public function getCompletionPercent()
    {
        return $this->completionPercent;
    }

    /**
     * @return void
     */
    protected function setStepsCount()
    {
        $this->stepsCount = count($this->resolvers);
    }

    /**
     * @return void
     */
    public function setCompletedStepsCount()
    {
        $this->completedStepsCount = array_reduce($this->getResults(), function ($sum, $result) {
            return $result->isComplete() ? $sum + 1 : $sum;
        }, 0);
    }

    /**
     * @return void
     */
    public function setCompletionPercent()
    {
        $this->completionPercent = $this->getCompletedStepsCount() * 100 / $this->getStepsCount();
    }

    /**
     * What to do when resolved.
     */
    private function resolved(): void
    {
        $this->setCompletedStepsCount();
        $this->setCompletionPercent();
    }

    private function validateResolver(string $name)
    {
        if (!isset($this->results[$name])) {
            throw new \Exception('Invalid result type given ' . $name . '. Valid types: ' . implode(', ', array_keys($this->results)));
        }
    }
}
