<?php


namespace App\Entity;


class CookieValidityType
{
    private const prefix = "COOKIE_VALIDITY_TYPE_";
    const DATE = CookieValidityType::prefix . "DATE";
    const TIMESPAN = CookieValidityType::prefix . "TIMESPAN";
    const USAGE = CookieValidityType::prefix . "USAGE";

    public static function Types()
    {
        $types = [];
        array_push($types, self::DATE, self::TIMESPAN, self::USAGE);
        return $types;
    }
}