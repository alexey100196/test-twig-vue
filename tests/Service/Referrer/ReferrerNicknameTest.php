<?php

namespace App\Tests\Service\Referrer;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\Referrer\ReferrerNickname;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class ReferrerNicknameTest extends TestCase
{
    private $service;
    private $em;
    private $userRepository;

    public function setUp(): void
    {
        $this->em = $this->createMock(EntityManagerInterface::class);
        $this->userRepository = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->service = new ReferrerNickname($this->em, $this->userRepository);
    }

    public function testUpdateNickname()
    {
        $user = (new User())
            ->setName('John')
            ->setSurname('Doe');

        $this->em->expects($this->once())
            ->method('flush');

        $this->userRepository->expects($this->once())
            ->method('findNickNameExceptUser')
            ->with($user, 'john.doe')
            ->willReturn(null);

        $updatedUser = $this->service->updateNickname($user);

        $this->assertEquals('john.doe', $updatedUser->getNickname());
    }

    public function testUpdateNicknameWhenNicknameIsTaken()
    {
        $user = (new User())
            ->setName('John')
            ->setSurname('Doe');

        $existingUser = (new User())
            ->setName('John')
            ->setSurname('Doe');

        $this->em->expects($this->once())
            ->method('flush');

        $this->userRepository->expects($this->at(0))
            ->method('findNickNameExceptUser')
            ->with($user, 'john.doe')
            ->willReturn($existingUser);

        $this->userRepository->expects($this->at(1))
            ->method('findNickNameExceptUser')
            ->with($user, 'john.doe1')
            ->willReturn(null);

        $updatedUser = $this->service->updateNickname($user);

        $this->assertEquals('john.doe1', $updatedUser->getNickname());
    }
}