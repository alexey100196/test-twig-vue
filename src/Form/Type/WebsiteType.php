<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class WebsiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('protocol', ChoiceType::class, [
                'choices' => ['http' => 'http', 'https' => 'https'],
            ])
            ->add('url')
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
                $data = $event->getData();

                if ($data['url'] ?? null) {
                    $protocol = $data['protocol'] ?? 'http';
                    $protocol = rtrim($protocol,'/');

                    $url = $data['url'];
                    $url = str_replace('http://', '', $url);
                    $url = str_replace('https://', '', $url);

                    $event->setData($protocol .'://' . $url);
                }
            });
    }
}