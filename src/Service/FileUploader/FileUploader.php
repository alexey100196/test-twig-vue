<?php

namespace App\Service\FileUploader;

use App\Service\FileUploader\Filesystems\FilesystemManager;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    /**
     * @var FilesystemManager
     */
    private $filesystemManager;

    /**
     * @param FilesystemManager $filesystemManager
     */
    public function __construct(FilesystemManager $filesystemManager)
    {
        $this->filesystemManager = $filesystemManager;
    }

    /**
     * @param UploadedFile $file
     * @param string $filesystem
     * @param string $name
     * @return File
     */
    public function upload(File $file, string $filesystem = 'public', string $name = '')
    {
        /*try {
            $file = $this->filesystemManager->get($filesystem)->store($file, $name ?: $file->getClientOriginalName());
        } catch (FileException $e) {
            // @todo
        }

        return $file;
        */
    }
}
