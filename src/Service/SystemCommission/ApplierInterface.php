<?php


namespace App\Service\SystemCommission;


use App\Entity\Shop;
use App\Service\SystemCommission\ValueObjects\TaxedAmount;

interface ApplierInterface
{
    public function apply(Shop $shop, $amount): TaxedAmount;
}