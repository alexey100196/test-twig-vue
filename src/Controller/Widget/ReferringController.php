<?php

namespace App\Controller\Widget;

use App\Controller\AjaxAbstractController;
use App\Entity\User;
use App\Entity\Widget\WidgetReferenceLink;
use App\Form\Widget\InviteFriendByEmailType;
use App\Form\Widget\ReferrerGenerateLinkType;
use App\Repository\CampaignRepository;
use App\Repository\ShopRepository;
use App\Repository\UserRepository;
use App\Repository\Widget\WidgetReferenceLinkRepository;
use App\Service\Widget\ReferenceLinkService;
use App\Service\Widget\InviteFriendByEmailService;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/widget/referring")
 */
class ReferringController extends AjaxAbstractController
{
    /**
     * @Route("/{number}/generate-link", name="widget_referring_link", methods={"POST"})
     *
     * @param string $number
     * @param ReferenceLinkService $generateReferenceLinkService
     * @param WidgetReferenceLinkRepository $widgetReferenceLinkRepository
     * @param CampaignRepository $campaignRepository
     * @param ShopRepository $shopRepository
     * @param UserRepository $userRepository
     * @param Request $request
     * @return JsonResponse
     */
    public function generateLink(
        string $number,
        ReferenceLinkService $generateReferenceLinkService,
        WidgetReferenceLinkRepository $widgetReferenceLinkRepository,
        CampaignRepository $campaignRepository,
        ShopRepository $shopRepository,
        UserRepository $userRepository,
        Request $request
    )
    {
        $shop = $shopRepository->findOneBy(['partnerNumber' => $number]);

        if (!$shop) {
            throw new NotFoundHttpException('Shop not found.');
        }

        $form = $this->createForm(ReferrerGenerateLinkType::class, null, ['shop' => $shop]);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_BAD_REQUEST);
        }

        if ($referrerEmail = $form->get('referrer_email')->getData()) {
            $user = $userRepository->findOneBy(['email' => $referrerEmail]);
        } elseif ($this->getUser()) {
            $user = $this->getUser();
        } else {
            return new JsonResponse('Invalid user.', Response::HTTP_NOT_FOUND);
        }

        if(!($activeCampaign = $campaignRepository->findActiveByShop($shop)) || empty($activeCampaign)) {
            return new JsonResponse('This shop doesn\'t have any active campaign.', Response::HTTP_NOT_FOUND);
        }

        $type = WidgetReferenceLink::TYPE_NAMES[$form->get('type')->getData()];

        $referenceLink = $generateReferenceLinkService->generate(
            $shop, $user, $type, $form->get('url')->getData(), $form->get('fingerprint')->getData(),
            $form->get('image')->getData(), $form->get('description')->getData(), $activeCampaign[0]
        );

        $metaUrl = null;
        if ($referenceLink->getReferrerLink()) {
            $url = $this->generateUrl('home_reflink', [
                'shopName' => $referenceLink->getReferrerLink()->getShopUrlName(),
                'nickname' => $referenceLink->getReferrerLink()->getNickname(),
                'reflinkSlug' => $referenceLink->getReferrerLink()->getSlug(),
            ], UrlGeneratorInterface::ABSOLUTE_URL);
            $metaUrl = $this->generateUrl('widget_share', [
                'slug' => $referenceLink->getSlug()
            ], UrlGeneratorInterface::ABSOLUTE_URL);
        } else {
            $url = $this->generateUrl('widget_share', [
                'slug' => $referenceLink->getSlug()
            ], UrlGeneratorInterface::ABSOLUTE_URL);
        }

        $response = new JsonResponse([
            'link' => $url,
            'metaUrl' => ($metaUrl) ? $metaUrl : $url,
            'cookie' => [
                'key' => 'widgetReferrerLink' . $referenceLink->getId(),
                'value' => $referenceLink->getId(),
            ]
        ]);

        $response->headers->setCookie(new Cookie('widgetReferrerLink' . $referenceLink->getId() , $referenceLink->getId()));

        return $response;
    }

    /**
     * @Route("/{number}/invite-friend/email", name="widget_referring_invite_friend_email", methods={"POST"})
     *
     * @param string $number
     * @param Request $request
     * @param InviteFriendByEmailService $inviteFriendByEmailService
     * @param ShopRepository $shopRepository
     * @param UserRepository $userRepository
     * @return JsonResponse
     */
    public function inviteFriendByEmail(
        string $number,
        Request $request,
        InviteFriendByEmailService $inviteFriendByEmailService,
        ShopRepository $shopRepository,
        UserRepository $userRepository
    )
    {
        $shop = $shopRepository->findOneBy(['partnerNumber' => $number]);

        if (!$shop) {
            throw new NotFoundHttpException('Shop not found.');
        }

        $form = $this->createForm(InviteFriendByEmailType::class);
        $form->handleRequest($request);

        if (!$form->isValid()) {
            return new JsonResponse($this->getFormErrorMessages($form), 400);
        }

        if ($referrerEmail = $form->get('referrer_email')->getData()) {
            $user = $userRepository->findOneBy(['email' => $referrerEmail]);
        } elseif ($this->getUser()) {
            $user = $this->getUser();
        } else {
            return new JsonResponse('Invalid user.', Response::HTTP_NOT_FOUND);
        }

        /** @var User $data */
        $data = $form->getData();

        try {
            $inviteFriendByEmailService($shop, $user, $data['email'], $data['url']);
        } catch (\LogicException $exception) {
            return new JsonResponse(['message' => $exception->getMessage()], 400);
        }

        return new JsonResponse();
    }
}
