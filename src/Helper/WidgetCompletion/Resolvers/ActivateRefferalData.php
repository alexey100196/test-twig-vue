<?php

namespace App\Helper\WidgetCompletion\Resolvers;

use App\Helper\WidgetCompletion\Resolvers\AbstractResolver;

class ActivateRefferalData extends AbstractResolver
{
    protected $result = 0;
    public function resolve()
    {
        if ($this->widgetShopSetting->getWizardStep() >= 3) {
            $this->addScore(100);
        }
    }
}