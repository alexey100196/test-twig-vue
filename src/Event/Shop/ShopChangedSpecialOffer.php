<?php


namespace App\Event\Shop;

class ShopChangedSpecialOffer extends ShopChangedOffer
{
    /** @var string */
    const NAME = 'shop.changed-special-offer';
}