<?php

namespace App\Entity;

use App\Entity\Widget\WidgetReferenceLink;
use App\ValueObject\Url;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReferrerLinkRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class ReferrerLink
{
    use SoftDeleteableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"card-sets", "referrer"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="referrerLinks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $referrer;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"card-sets", "referrer"})
     */
    private $destinationUrl;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"card-sets", "referrer"})
     */
    private $shop;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Campaign")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"card-sets", "referrer"})
     */
    private $campaign;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductPurchase", mappedBy="referrerLink")
     */
    private $productPurchases;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="referrerLinks")
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $nickname;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ReferrerLinkCard", inversedBy="referrerLink", cascade={"persist", "remove"})
     * @Groups({"card-sets", "referrer"})
     */
    private $card;

    /**
     * @ORM\Column(type="boolean")
     */
    private $showInMyStore;

    /**
     * @ORM\Column(type="boolean")
     */
    private $showInMyStoreAsCard;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $shopUrlName;

    /**
     * @ORM\Column(name="is_generated", type="boolean")
     */
    private $isGenerated = false;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ReferrerLinkStoreCard", inversedBy="referrerLink", cascade={"persist", "remove"})
     */
    private $storeCard;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Widget\WidgetReferenceLink", mappedBy="referrerLink", cascade={"persist", "remove"})
     */
    private $widgetReferenceLink;

    public function __construct()
    {
        $this->productPurchases = new ArrayCollection();
        $this->showInMyStore = 0;
        $this->showInMyStoreAsCard = 0;
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReferrer(): ?User
    {
        return $this->referrer;
    }

    public function setReferrer(?User $referrer): self
    {
        $this->referrer = $referrer;

        return $this;
    }

    public function getDestinationUrl():? Url
    {
        return $this->destinationUrl ? new Url($this->destinationUrl) : null;
    }

    public function setDestinationUrl(string $destinationUrl): self
    {
        $this->destinationUrl = $destinationUrl;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }

    public function setCampaign(?Campaign $campaign): self
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getFullUrl()
    {
        if ($this->shop && $this->shop->getUrlName()) {
            return '/' . implode('/', ['reflink', $this->shop->getUrlName(), $this->getNickname(), $this->getSlug()]);
        }

        return null;
    }

    /**
     * @return Collection|ProductPurchase[]
     */
    public function getProductPurchases(): Collection
    {
        return $this->productPurchases;
    }

    public function addProductPurchase(ProductPurchase $productPurchase): self
    {
        if (!$this->productPurchases->contains($productPurchase)) {
            $this->productPurchases[] = $productPurchase;
            $productPurchase->setReferrerLink($this);
        }

        return $this;
    }

    public function removeProductPurchase(ProductPurchase $productPurchase): self
    {
        if ($this->productPurchases->contains($productPurchase)) {
            $this->productPurchases->removeElement($productPurchase);
            // set the owning side to null (unless already changed)
            if ($productPurchase->getReferrerLink() === $this) {
                $productPurchase->setReferrerLink(null);
            }
        }

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname(?string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getCard(): ?ReferrerLinkCard
    {
        return $this->card;
    }

    public function setCard(?ReferrerLinkCard $card): self
    {
        $this->card = $card;
    }

    public function getShowInMyStore(): ?bool
    {
        return $this->showInMyStore;
    }

    public function setShowInMyStore($showInMyStore): self
    {
        $this->showInMyStore = $showInMyStore;

        return $this;
    }

    /**
     * @return ReferrerLinkCard
     */
    public function getCardOrCreate()
    {
        if (!$this->card) {
            $this->card = new ReferrerLinkCard();
            $this->card->setReferrerLink($this);
        }

        return $this->card;
    }

    public function getShowInMyStoreAsCard(): ?bool
    {
        return $this->showInMyStoreAsCard;
    }

    public function setShowInMyStoreAsCard($showInMyStoreAsCard): self
    {
        $this->showInMyStoreAsCard = $showInMyStoreAsCard;

        return $this;
    }

    public function getShopUrlName(): ?string
    {
        return $this->shopUrlName;
    }

    public function setShopUrlName(string $shopUrlName): self
    {
        $this->shopUrlName = $shopUrlName;

        return $this;
    }

    public function getIsGenerated(): ?bool
    {
        return $this->isGenerated;
    }

    public function setIsGenerated($isGenerated): self
    {
        $this->isGenerated = $isGenerated;

        return $this;
    }

    public function getStoreCard(): ?ReferrerLinkStoreCard
    {
        return $this->storeCard;
    }

    public function setStoreCard(?ReferrerLinkStoreCard $storeCard): self
    {
        $this->storeCard = $storeCard;

        return $this;
    }

    public function getStoreCardOrCreate()
    {
        if (!$this->storeCard) {
            $this->storeCard = new ReferrerLinkStoreCard();
            $this->storeCard->setReferrerLink($this);
        }

        return $this->storeCard;
    }

    public function delete()
    {
        $this->setDeletedAt(new \DateTime());
        $this->anonymize();
    }

    private function anonymize()
    {
        $this->slug .= '.deleted';
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @Groups({"card-sets", "referrer"})
     * @VirtualProperty()
     */
    public function getShareableLink()
    {
        return $this->getShopUrlName() . '/' . $this->getNickname() . '/' . $this->getSlug();
    }

    public function getWidgetReferenceLink(): ?WidgetReferenceLink
    {
        return $this->widgetReferenceLink;
    }

    public function setWidgetReferenceLink(?WidgetReferenceLink $widgetReferenceLink): self
    {
        $this->widgetReferenceLink = $widgetReferenceLink;

        // set (or unset) the owning side of the relation if necessary
        $newReferrerLink = $widgetReferenceLink === null ? null : $this;
        if ($newReferrerLink !== $widgetReferenceLink->getReferrerLink()) {
            $widgetReferenceLink->setReferrerLink($newReferrerLink);
        }

        return $this;
    }
}
