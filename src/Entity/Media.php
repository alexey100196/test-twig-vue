<?php

namespace App\Entity;

use App\Entity\Widget\WidgetShopSetting;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\EntityListeners;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MediaRepository")
 * @ORM\EntityListeners({"App\Listener\Entity\MediaListener"})
 */
class Media implements \Serializable
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"referrer"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"referrer"})
     */
    private $fileName;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $mimeType;

    /**
     * @ORM\Column(type="integer")
     */
    private $size;

    /**
     * @var string
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"referrer"})
     */
    private $originalName;

    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $filesystem;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Widget\WidgetShopSetting", mappedBy="welcomePicture")
     */
    private $widgetShopSetting;

    public function getId():  ? int
    {
        return $this->id;
    }

    public function getFileName() :  ? string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName) : self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getMimeType():  ? string
    {
        return $this->mimeType;
    }

    public function setMimeType(string $mimeType) : self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function getSize():  ? int
    {
        return $this->size;
    }

    public function setSize(int $size) : self
    {
        $this->size = $size;

        return $this;
    }

    public function setFile(UploadedFile $file)
    {

        if ($file) {
            $base64 = base64_encode(file_get_contents( $file));
            $logoMimeType = $file->getMimeType();
            $this->setContent('data:'.$logoMimeType.';base64,'.$base64);
        }
        $this->setMimeType($file->getClientMimeType());
        $this->setSize($file->getClientSize());
        $this->setOriginalName($file->getClientOriginalName());
        $this->setOriginalName($file->getClientOriginalName());
        $this->file = $file;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function getOriginalName():  ? string
    {
        return $this->originalName;
    }

    public function setOriginalName(string $originalName) : self
    {
        $this->originalName = $originalName;

        return $this;
    }

    public function getFilesystem():  ? string
    {
        return $this->filesystem;
    }

    public function setFilesystem(string $filesystem) : self
    {
        $this->filesystem = $filesystem;

        return $this;
    }

    public function getPublicUrl()
    {
        if ($this->fileName) {
            return $this->getContent();
        }
        return null;
    }

    public function getContent(): ?String
    {
        return $this->content;
    }

    public function setContent(?String $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getWidgetShopSetting(): ?String
    {
        return $this->widgetShopSetting;
    }

    public function getWidgetShopSettingImg(): ?self
    {
        return $this->widgetShopSetting->getWelcomePicture()->getId();
    }

    public function setWidgetShopSetting($widgetShopSetting): self
    {
        $this->widgetShopSetting = $widgetShopSetting;

        return $this;
    }

        /** @see \Serializable::serialize() */
      public function serialize()
      {
              return serialize(array(
                  $this->id,
                  $this->fileName,
                  $this->mimeType,
                  $this->size,
                  $this->path,
                  $this->originalName,
                  $this->content
              ));

      }


/** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
            list(
                $this->id,
                $this->fileName,
                $this->mimeType,
                $this->size,
                $this->path,
                $this->originalName,
                $this->content
                ) = unserialize($serialized, array('allowed_classes' => false));
    }

}
