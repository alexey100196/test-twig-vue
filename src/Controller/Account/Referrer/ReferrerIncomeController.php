<?php


namespace App\Controller\Account\Referrer;

use App\Service\Referrer\ReferrerShop;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReferrerIncomeController
 * @package App\Controller\Account\Referrer
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerIncomeController extends AbstractController
{
    /**
     * @var ReferrerShop $referrerShop
     */
    private $referrerShop;

    /**
     * ReferrerShopController constructor.
     * @param ReferrerShop $referrerShop
     */
    public function __construct(ReferrerShop $referrerShop)
    {
        $this->referrerShop = $referrerShop;
    }

    /**
     * @Route("/account/referrer/income", name="account_referrer_income_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $userShops = $this->referrerShop->getReferrerShopsWithStats($this->getUser());
        $shops = [];

        foreach ($userShops as $userShop) {
            $shop = $userShop['userShop']->getShop();
            array_push($shops, [
                'id' => $shop->getId(),
                'logo' => $shop->getLogo(),
                'name' => $shop->getName(),
                'description' => $shop->getDescription()
            ]);
        }

        return $this->render('account/referrer/income/index.html.twig', compact('shops'));
    }
}