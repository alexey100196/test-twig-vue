<?php

namespace App\Repository\Widget;

use App\Entity\Widget\WidgetButton;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WidgetButton|null find($id, $lockMode = null, $lockVersion = null)
 * @method WidgetButton|null findOneBy(array $criteria, array $orderBy = null)
 * @method WidgetButton[]    findAll()
 * @method WidgetButton[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WidgetButtonRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WidgetButton::class);
    }
}
