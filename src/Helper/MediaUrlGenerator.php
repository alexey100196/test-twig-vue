<?php

namespace App\Helper;

use App\Entity\Media;
use App\Service\FileUploader\Filesystems\FilesystemManager;

/**
 *
 */
class MediaUrlGenerator
{
    /**
     * Media entity instance.
     * @var App\Entity\Media
     */
    private $media;

    private $filesystemManager;

    /**
     * @param Media $media
     */
    public function __construct(FilesystemManager $filesystemManager)
    {
        $this->filesystemManager = $filesystemManager;
    }

    public function getRealPath(Media $media): string
    {
        return implode('/', [
            $this->filesystemManager->get($media->getFilesystem())->getPath(),
            $media->getFileName(),
        ]);
    }

}
