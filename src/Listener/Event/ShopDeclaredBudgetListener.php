<?php


namespace App\Listener\Event;


use App\Adapter\MailSender;
use App\Event\Shop\ShopDeclaredBudget;

class ShopDeclaredBudgetListener
{
    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var MailSender
     */
    private $mailSender;

    /**
     * @var string
     */
    private $adminEmail;

    /**
     * UserRegistrationListener constructor.
     * @param \Twig_Environment $twig
     * @param MailSender $mailSender
     */
    public function __construct(\Twig_Environment $twig, MailSender $mailSender)
    {
        $this->twig = $twig;
        $this->mailSender = $mailSender;
    }

    /**
     * @param string $adminEmail
     */
    public function setAdminEmail(string $adminEmail)
    {
        $this->adminEmail = $adminEmail;
    }

    /**
     * @param ShopDeclaredBudget $event
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendEmailToAdmin(ShopDeclaredBudget $event)
    {
        $subject = 'Notification Email for Budget - Shop #' . $event->getShop()->getId();

        if ($event->getDeclared()) {
            $subject .= ' (Front Verified)';
        }

        $this->mailSender->send(
            'hello@superinterface.com',
            $this->adminEmail,
            $subject,
            $this->twig->render('email/admin/shop-declared-budget.html.twig', [
                'budget' => $event->getBudget(),
                'shop' => $event->getShop(),
            ])
        );
    }
}