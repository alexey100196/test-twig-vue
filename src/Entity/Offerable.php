<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

class Offerable
{
    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Offer", orphanRemoval=true, fetch="EAGER", cascade={"persist"})
     */
    protected $offers;

    public function __construct()
    {
        $this->offers = new ArrayCollection();
    }

    /**
     * @param Offer $offer
     */
    public function addOffer(Offer $offer)
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
        }

        return $this;
    }

    public function removeOffer(Offer $offer)
    {
        if ($this->offers->contains($offer)) {
            $this->offers->removeElement($offer);
            // set the owning side to null (unless already changed)
            if ($offer->getUser() === $this) {
                $offer->setUser(null);
            }
        }

        return $this;
    }

    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param OfferType $offerType
     * @return ArrayCollection
     */
    public function getOffersByType(OfferType $offerType): ArrayCollection
    {
        return $this->getOffers()->filter(function ($offer) use ($offerType) {
            return $offer->getType()->getName() == $offerType->getName();
        });
    }

    public function findOrCreate(OfferType $offerType = null)
    {
        $offers = $offerType ? $this->getOffersByType($offerType) : $this->getOffers();

        if (!$offer = $offers->first()) {
            $offer = new Offer();
            if ($offerType) {
                $offer->setType($offerType);
            }
        }

        return $offer;
    }
}