<?php


namespace App\Util\TokenGenerator;


class NumericTokenGenerator implements TokenGeneratorInterface
{
    /**
     * @param int $length
     * @return string
     */
    public function generate(int $length = 32): string
    {
        $number = '';

        while (strlen($number) < $length) {
            $number .= (string)mt_rand(0, 9);
        }

        return $number;
    }
}