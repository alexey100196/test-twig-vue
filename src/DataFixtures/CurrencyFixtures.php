<?php

namespace App\DataFixtures;


use App\Entity\Currency;

class CurrencyFixtures extends BaseGlossaryFixtures
{
    public function getData(): array
    {
        return [
            1 => Currency::EUR,
        ];
    }

    public function getEntity()
    {
        return new Currency();
    }
}