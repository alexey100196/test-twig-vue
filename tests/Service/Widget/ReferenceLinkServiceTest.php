<?php


namespace App\Tests\Service\Widget;


use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\User;
use App\Entity\UserShop;
use App\Entity\Widget\WidgetReferenceLink;
use App\Service\Widget\ReferenceLinkService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ReferenceLinkServiceTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var Shop
     */
    protected $shop;

    /**
     * @var User
     */
    protected $referrer;

    /**
     * @var User
     */
    protected $anotherReferrer;

    /**
     * @var ReferrerLink
     */
    protected $referrerLink;

    /**
     * @var ReferenceLinkService
     */
    protected $referenceLinkService;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->referenceLinkService = $kernel->getContainer()->get(ReferenceLinkService::class);

        $this->shop = $this->em->getRepository(Shop::class)->findOneBy(['name' => 'Demo']);
        $this->referrer = $this->em->getRepository(User::class)->findOneBy(['nickname' => 'john']);
        $this->anotherReferrer = $this->em->getRepository(User::class)->findOneBy(['nickname' => 'kate']);

        $this->em->persist((new UserShop())->setShop($this->shop)->setUser($this->referrer));

        $this->referrerLink = (new ReferrerLink())
            ->setReferrer($this->referrer)
            ->setShop($this->shop)
            ->setShopUrlName($this->shop->getUrlName())
            ->setSlug('anyslug')
            ->setDestinationUrl('https://google.com/');

        $this->em->persist($this->referrerLink);

        $this->em->flush();
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null;
    }

    public function testCreatedReferralLinkIsGeneratedFlag()
    {
        $this->assertFalse($this->referrerLink->getIsGenerated());
    }

}