<?php


namespace App\Service\Exchange;


use App\Entity\ExchangeRate;
use App\Repository\ExchangeRateRepository;
use Doctrine\ORM\EntityManagerInterface;

class ExchangeService implements CurrencyExchangeInterface
{
    /**
     * @var PricesReceiver
     */
    private $pricesReceiver;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ExchangeRateRepository
     */
    private $exchangeRateRepository;

    /**
     * ExchangeService constructor.
     * @param PricesReceiver $pricesReceiver
     * @param EntityManagerInterface $em
     * @param ExchangeRateRepository $exchangeRateRepository
     */
    public function __construct(
        PricesReceiver $pricesReceiver,
        EntityManagerInterface $em,
        ExchangeRateRepository $exchangeRateRepository
    )
    {
        $this->pricesReceiver = $pricesReceiver;
        $this->em = $em;
        $this->exchangeRateRepository = $exchangeRateRepository;
    }

    public function updatePrices(string $currency)
    {
        $pairs = $this->pricesReceiver->get($currency);

        foreach ($pairs as $pair) {
            $this->em->merge(new ExchangeRate($pair->getBaseCurrency(), $pair->getQuotedCurrency(), $pair->getRate()));
        }

        $this->em->flush();
    }

    /**
     * @param string $fromAmount
     * @param string $fromCurrency
     * @param string $toCurrency
     * @return float|int|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function exchange(string $fromAmount, string $fromCurrency, string $toCurrency)
    {
        $exchangeRate = $this->exchangeRateRepository->findForCurrencies($fromCurrency, $toCurrency);

        // @todo: bcmath

        if ($exchangeRate->getBaseCurrency() === $fromCurrency) {
            return $fromAmount / $exchangeRate->getRate();
        } elseif ($exchangeRate->getQuotedCurrency() === $fromCurrency) {
            return $fromAmount * $exchangeRate->getRate();
        }

        return null;
    }
}