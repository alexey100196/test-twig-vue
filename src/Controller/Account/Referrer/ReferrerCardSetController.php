<?php

namespace App\Controller\Account\Referrer;

use App\Entity\CardSet\CardSet;
use App\Entity\CardSet\CardSetCard;
use App\Entity\CardSet\CardSetCardBackground;
use App\Entity\ReferrerCoupon;
use App\Entity\User;
use App\Form\Referrer\CardSetCardType;
use App\Form\Referrer\CardSetType;
use App\Repository\ReferrerCouponRepository;
use App\Repository\ReferrerLinkCardRepository;
use App\Repository\ReferrerLinkRepository;
use App\Repository\ShopRepository;
use App\Service\MediaUploader;
use App\Service\MediaUrlFileUploader;
use App\Service\UrlFileUploader;
use App\Traits\Controller\FormResponseArray;
use App\Traits\Controller\HasJsonResponse;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Security("has_role('ROLE_REFERRER') && is_granted('FULL_PROFILE')")
 */
class ReferrerCardSetController extends AbstractController
{
    use FormResponseArray;
    use HasJsonResponse;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var MediaUploader
     */
    private $mediaUploader;

    /**
     * @var MediaUrlFileUploader
     */
    private $mediaUrlFileUploader;

    public function __construct(
        EntityManagerInterface $em,
        MediaUploader $mediaUploader,
        MediaUrlFileUploader $mediaUrlFileUploader
    )
    {
        $this->em = $em;
        $this->mediaUploader = $mediaUploader;
        $this->mediaUrlFileUploader = $mediaUrlFileUploader;
    }

    /**
     * @Route(
     *     "/account/referrer/card-set/referrer-links",
     *     name="account_referrer_card_set_referrer-links",
     *     methods={"GET"}
     * )
     * @param ReferrerLinkRepository $referrerLinkRepository
     * @return JsonResponse
     */
    public function referrerLinks(ReferrerLinkRepository $referrerLinkRepository)
    {
        $links = $referrerLinkRepository->findForReferrer($this->getUser());

        return $this->jsonResponse($links, ['card-sets']);
    }

    /**
     * @Route(
     *     "/account/referrer/card-set/coupon",
     *     name="account_referrer_card_set_coupon",
     *     methods={"GET"}
     * )
     * @param ShopRepository $shopRepository
     * @return JsonResponse
     */
    public function coupon(ShopRepository $shopRepository, Request $request)
    {
        $shopId = $request->query->get('shop_id');

        if (!$shopId || !$shop = $shopRepository->find($shopId)) {
            return new JsonResponse(['message' => 'Shop not found'], Response::HTTP_NOT_FOUND);
        }

        /** @var User $user */
        $user = $this->getUser();

        $referrerCoupon = $user->getReferrerCouponByShop($shop);

        if (!$referrerCoupon || !$referrerCoupon->getSetOfCardsEnabledAndNotExpired()) {
            return new JsonResponse(['message' => 'Coupon not found'], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(
            [
                'code' => $referrerCoupon->getCode(),
                'discount' => $referrerCoupon->getValue(),
                'discount_type' => (string)$referrerCoupon->getValueType()
            ]
        );
    }

    /**
     * @Route(
     *     "/account/referrer/card-set/{cardSet}",
     *     name="account_referrer_card_set_shows",
     *     methods={"GET"}
     * )
     * @param CardSet $cardSet
     * @return JsonResponse
     */
    public function show(CardSet $cardSet)
    {
        $this->validateUserPermissionToEditCardSet($this->getUser(), $cardSet);

        return $this->jsonResponse($cardSet, ['referrer']);
    }

    /**
     * @Route(
     *     "/account/referrer/card-set",
     *     name="account_referrer_card_set_store",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {
        $form = $this->createForm(CardSetType::class);
        $form->submit($request->request->all());

        if ($form->isValid() === false) {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_BAD_REQUEST);
        }

        /** @var CardSet $cardSet */
        $cardSet = $form->getData();
        $cardSet->setUser($this->getUser());

        $this->em->persist($cardSet);
        $this->em->flush();

        return new JsonResponse(['id' => $cardSet->getId()], Response::HTTP_CREATED);
    }

    /**
     * @Route(
     *     "/account/referrer/card-set/{cardSet}",
     *     name="account_referrer_card_set_update",
     *     methods={"PATCH"}
     * )
     * @param CardSet $cardSet
     * @param Request $request
     * @return JsonResponse
     */
    public function update(CardSet $cardSet, Request $request)
    {
        $this->validateUserPermissionToEditCardSet($this->getUser(), $cardSet);

        $params = $request->request->all();
        unset($params['_method']);

        $form = $this->createForm(CardSetType::class, $cardSet, ['method' => 'PATCH']);
        $form->submit($params);

        if ($form->isValid() === false) {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_BAD_REQUEST);
        }

        $this->em->flush();

        return new JsonResponse([], Response::HTTP_OK);
    }

    /**
     * @Route(
     *     "/account/referrer/card-set/{cardSet}/card",
     *     name="account_referrer_card_set_card_create",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @param CardSet $cardSet
     * @return JsonResponse
     * @throws \Exception
     */
    public function createCard(Request $request, CardSet $cardSet)
    {
        $this->validateUserPermissionToEditCardSet($this->getUser(), $cardSet);

        $params = $request->request->all();
        if ($request->files->get('background') && isset($params['background'])) {
            $params['background'] = array_merge($params['background'], $request->files->get('background'));
        }

        $form = $this->createForm(CardSetCardType::class, $card = $cardSet->createEmptyCard());
        $form->submit($params);

        if ($form->isValid() === false) {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_BAD_REQUEST);
        }

        /** @var CardSetCard $card */
        $card = $form->getData();

        if ($media = $this->proceedCardBackgroundMediaUpload($card->getBackground())) {
            $card->getBackground()->setMedia($media);
        }

        $this->em->persist($card);
        $this->em->flush();

        return new JsonResponse(['id' => $card->getId()], Response::HTTP_CREATED);
    }

    /**
     * @Route(
     *     "/account/referrer/card-set/{cardSet}/card/{card}",
     *     name="account_referrer_card_set_card_update",
     *     methods={"PATCH"}
     * )
     * @param Request $request
     * @param CardSet $cardSet
     * @param CardSetCard $card
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateCard(Request $request, CardSet $cardSet, CardSetCard $card)
    {
        $this->validateUserPermissionToEditCard($this->getUser(), $card);

        $params = $request->request->all();
        unset($params['_method']);

        if ($request->files->get('background') && isset($params['background'])) {
            $params['background'] = array_merge($params['background'], $request->files->get('background'));
        }

        $form = $this->createForm(CardSetCardType::class, $card, ['method' => 'PATCH']);
        $form->submit($params);

        if ($form->isValid() === false) {
            return new JsonResponse($this->getFormErrorMessages($form), Response::HTTP_BAD_REQUEST);
        }

        if ($media = $this->proceedCardBackgroundMediaUpload($card->getBackground())) {
            $card->getBackground()->setMedia($media);
        }

        $this->em->persist($card);
        $this->em->flush();

        return new JsonResponse([], Response::HTTP_OK);
    }


    /**
     * @Route(
     *     "/account/referrer/card-set/{cardSet}/card/{card}",
     *     name="account_referrer_card_set_card_delete",
     *     methods={"DELETE"}
     * )
     * @param CardSet $cardSet
     * @param CardSetCard $card
     * @return JsonResponse
     */
    public function deleteCard(CardSet $cardSet, CardSetCard $card)
    {
        $this->validateUserPermissionToEditCard($this->getUser(), $card);

        $this->em->remove($card);
        $this->em->flush();

        return new JsonResponse([], Response::HTTP_OK);
    }

    /**
     * @param CardSetCardBackground $background
     * @return null
     * @throws \Exception
     */
    private function proceedCardBackgroundMediaUpload(CardSetCardBackground $background)
    {
        $media = $background->getMedia();

        if ($media && $media->getFile()) {
            return $this->mediaUploader->upload($media);
        } else if ($background->getUrl() !== null) {
            return $this->mediaUrlFileUploader->upload($background->getUrl());
        }

        return null;
    }

    /**
     * @param User $user
     * @param CardSetCard $card
     */
    private function validateUserPermissionToEditCard(User $user, CardSetCard $card)
    {
        if ($card->getCardSet()->getUser() !== $user) {
            throw new HttpException(Response::HTTP_FORBIDDEN);
        }
    }

    /**
     * @param User $user
     * @param CardSet $cardSet
     */
    private function validateUserPermissionToEditCardSet(User $user, CardSet $cardSet)
    {
        if ($cardSet->getUser() !== $user) {
            throw new HttpException(Response::HTTP_FORBIDDEN);
        }
    }
}