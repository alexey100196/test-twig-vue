<?php

namespace App\Entity;

use App\Helper\Math;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfferRepository")
 */
class Offer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OfferType", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"shop"})
     */
    private $type;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"shop"})
     */
    private $commission;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CommissionType", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"shop"})
     */
    private $commissionType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?OfferType
    {
        return $this->type;
    }

    public function setType(? OfferType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCommission()
    {
        return $this->commission;
    }

    public function setCommission($commission): self
    {
        $this->commission = $commission;

        return $this;
    }

    public function getCommissionType(): ?CommissionType
    {
        return $this->commissionType;
    }

    public function setCommissionType(? CommissionType $commissionType): self
    {
        $this->commissionType = $commissionType;

        return $this;
    }

    public function isCommissionFiexed()
    {
        return $this->getCommissionType()->getName() == CommissionType::FIXED;
    }

    /**
     * @param $value
     * @return string
     */
    public function calculateCommissionValue($price, $amount)
    {
        if ($this->isCommissionFiexed()) {
            return Math::mul($this->getCommission(), $amount);
        } else {
            return Math::div(Math::mul($this->getCommission(), Math::mul($price, $amount)), '100');
        }
    }

    public function getShortLabel()
    {
        $commissionValue = (string)$this->getCommission();
        $commissionValue = str_replace('.00', '', $commissionValue);
        if ($this->isPercentage()) {
            return $commissionValue . '%';
        } else {
            return '€' . $commissionValue;
        }
    }

    public function isPercentage()
    {
        if ($commissionType = $this->getCommissionType()) {
            return $commissionType->getName() == CommissionType::PERCENT;
        }

        return false;
    }
}
