<?php


namespace App\Event\Conversion;

use App\Entity\ShopConversion;
use App\Entity\ShopSignUp;
use Symfony\Component\EventDispatcher\Event;

class ShopSignUpCreated extends Event implements ConversionEventInterface
{
    /** @var string */
    const NAME = 'conversion.shop-sign-up-created';

    private $shopSignUp;

    public function __construct(ShopSignUp $shopSignUp)
    {
        $this->shopSignUp = $shopSignUp;
    }

    /**
     * @return ShopSignUp
     */
    public function getConversion(): ShopConversion
    {
        return $this->shopSignUp;
    }
}