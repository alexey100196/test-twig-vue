<?php


namespace App\Form\Referrer;

use App\Entity\ReferrerStoreNonReferableLink;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Url;

class ReferrerStoreNonReferableLinkType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'constraints' => [
                    new Length(['max' => 255]),
                ],
            ])
            ->add('customTitle', TextType::class, [
                'constraints' => [
                    new Length(['max' => 255]),
                ],
            ])
            ->add('image', TextType::class, [
                'constraints' => [
                    new Length(['max' => 255]),
                ],
            ])
            ->add('video', TextType::class, [
                //
            ])
            ->add('layout', TextType::class, [
                'constraints' => [
                    new Length(['max' => 255]),
                ],
            ])
            ->add('destinationUrl', UrlType::class, [
                'default_protocol' => 'https',
                'required' => true,
                'constraints' => [
                    new Url([
                        'message' => 'This value is not valid url: {{ value }}'
                    ]),
                    new NotBlank()
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'allow_extra_fields' => false,
        ]);
    }
}
