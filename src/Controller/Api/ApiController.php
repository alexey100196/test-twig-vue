<?php


namespace App\Controller\Api;


use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Form;

/**
 * @todo: resolve duplicated methods @see AjaxAbstractController
 * Class ApiController
 * @package App\Controller\Api
 */
abstract class ApiController extends FOSRestController
{
    /**
     * Return form errors as array.
     *
     * @param Form $form
     * @return array
     */
    protected function getFormErrorMessages(Form $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrorMessages($child);
            }
        }

        return $errors;
    }

    /**
     * Make validation response.
     * @param array $errors
     * @return JsonResponse
     */
    protected function validationErrorResponse(array $errors): JsonResponse
    {
        return new JsonResponse(['errors' => $errors], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param $data
     * @return JsonResponse
     */
    public function makeResponse($data = null, int $status = 200, array $headers = array(), bool $json = false): JsonResponse
    {
        return new JsonResponse($data, $status, $headers, $json);
    }
}