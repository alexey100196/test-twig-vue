<?php


namespace App\Form\ShopEvent;

use App\Entity\EventEntry;
use App\Entity\EventEntryType;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

abstract class AbstractEventEntryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('referrer_link', EntityType::class, [
                'class' => ReferrerLink::class,
                'choice_value' => 'slug',
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('shop', EntityType::class, [
                'class' => Shop::class,
                'choice_value' => 'partner_number',
                'constraints' => [
                    new NotNull()
                ]
            ])
            ->add('type', EntityType::class, [
                'class' => EventEntryType::class,
                'choice_value' => 'name',
                'constraints' => [
                    new NotNull()
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => EventEntry::class,
            'csrf_protection' => false,
        ));
    }
}