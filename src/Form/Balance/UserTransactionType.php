<?php


namespace App\Form\Balance;


use App\Entity\Balance;
use App\Entity\Currency;
use App\Entity\TransactionType;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserTransactionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('currency', EntityType::class, ['class' => Currency::class])
            ->add('amount', NumberType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }
}