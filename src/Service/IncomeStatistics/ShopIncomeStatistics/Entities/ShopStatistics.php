<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Entities;

use App\Entity\User;

class ShopStatistics
{
    /**
     * @var User
     */
    private $referrer;

    /**
     * @var \DateTime|null
     */
    private $lastPurchaseDate;

    /**
     * @var \DateTime|null
     */
    private $lastSignUpDate;

    /**
     * @var int
     */
    private $purchaseCount;

    /**
     * @var int
     */
    private $signUpCount;

    /**
     * @var int
     */
    private $viewsCount;

    /**
     * @var int
     */
    private $usedCouponsCount;

    /**
     * @var int
     */
    private $inviteFriendEmails;

    /**
     * @var float
     */
    private $signUpReferrerProfit;

    /**
     * @var float
     */
    private $purchaseReferrerProfit;

    /**
     * @var float
     */
    private $systemCommission;

    /**
     * @var float
     */
    private $purchaseTotal;

    /**
     * @param User $referrer
     * @return ShopStatistics
     */
    public function setReferrer(User $referrer): ShopStatistics
    {
        $this->referrer = $referrer;

        return $this;
    }

    /**
     * @param \DateTime|null $lastPurchaseDate
     * @return ShopStatistics
     */
    public function setLastPurchaseDate(?\DateTime $lastPurchaseDate): ShopStatistics
    {
        $this->lastPurchaseDate = $lastPurchaseDate;

        return $this;
    }

    /**
     * @param \DateTime|null $lastSignUpDate
     * @return ShopStatistics
     */
    public function setLastSignUpDate(?\DateTime $lastSignUpDate): ShopStatistics
    {
        $this->lastSignUpDate = $lastSignUpDate;

        return $this;
    }

    /**
     * @param int $purchaseCount
     * @return ShopStatistics
     */
    public function setPurchaseCount(int $purchaseCount): ShopStatistics
    {
        $this->purchaseCount = $purchaseCount;

        return $this;
    }

    /**
     * @param int $signUpCount
     * @return ShopStatistics
     */
    public function setSignUpCount(int $signUpCount): ShopStatistics
    {
        $this->signUpCount = $signUpCount;

        return $this;
    }

    /**
     * @param int $viewsCount
     * @return ShopStatistics
     */
    public function setViewsCount(int $viewsCount): ShopStatistics
    {
        $this->viewsCount = $viewsCount;

        return $this;
    }

    /**
     * @param int $usedCouponsCount
     * @return ShopStatistics
     */
    public function setUsedCouponsCount(int $usedCouponsCount): ShopStatistics
    {
        $this->usedCouponsCount = $usedCouponsCount;

        return $this;
    }

    /**
     * @param int $inviteFriendEmails
     * @return ShopStatistics
     */
    public function setInviteFriendEmails(int $inviteFriendEmails): ShopStatistics
    {
        $this->inviteFriendEmails = $inviteFriendEmails;

        return $this;
    }

    /**
     * @param float $signUpReferrerProfit
     * @return ShopStatistics
     */
    public function setSignUpReferrerProfit(float $signUpReferrerProfit): ShopStatistics
    {
        $this->signUpReferrerProfit = $signUpReferrerProfit;

        return $this;
    }

    /**
     * @param float $purchaseReferrerProfit
     * @return ShopStatistics
     */
    public function setPurchaseReferrerProfit(float $purchaseReferrerProfit): ShopStatistics
    {
        $this->purchaseReferrerProfit = $purchaseReferrerProfit;

        return $this;
    }

    /**
     * @return null|string
     */
    protected function lastPurchaseDateString()
    {
        if ($this->lastPurchaseDate) {
            return $this->lastPurchaseDate->format('Y-m-d H:i:s');
        }

        return null;
    }

    /**
     * @param float $systemCommission
     * @return ShopStatistics
     */
    public function setSystemCommission(float $systemCommission): ShopStatistics
    {
        $this->systemCommission = $systemCommission;

        return $this;
    }

    /**
     * @return null|string
     */
    protected function lastSignUpDateString()
    {
        if ($this->lastSignUpDate) {
            return $this->lastSignUpDate->format('Y-m-d H:i:s');
        }

        return null;
    }

    /**
     * @param float $purchaseTotal
     * @return ShopStatistics
     */
    public function setPurchaseTotal(float $purchaseTotal): ShopStatistics
    {
        $this->purchaseTotal = $purchaseTotal;

        return $this;
    }

    public function toArray()
    {
        return [
            'referrer' => [
                'id' => $this->referrer->getId(),
                'name' => $this->referrer->getName(),
                'surname' => $this->referrer->getSurname(),
                'email' => $this->referrer->getEmail(),
            ],
            'last_purchase_date' => $this->lastPurchaseDateString(),
            'last_sign_up_date' => $this->lastSignUpDateString(),
            'purchase_count' => $this->purchaseCount,
            'sign_up_count' => $this->signUpCount,
            'views_count' => $this->viewsCount,
            'invite_friend_emails_count' => $this->inviteFriendEmails,
            'used_coupons_count' => $this->usedCouponsCount,
            'sign_up_referrer_profit' => $this->signUpReferrerProfit,
            'purchase_referrer_profit' => $this->purchaseReferrerProfit,
            'system_commission' => $this->systemCommission,
            'purchase_total' => $this->purchaseTotal,
        ];
    }
}