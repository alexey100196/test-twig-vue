<?php

namespace App\Repository;

use App\Entity\ProductOfferType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProductOfferType|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductOfferType|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductOfferType[]    findAll()
 * @method ProductOfferType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductOfferRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProductOfferType::class);
    }

//    /**
    //     * @return ProductOfferType[] Returns an array of ProductOfferType objects
    //     */
    /*
    public function findByExampleField($value)
    {
    return $this->createQueryBuilder('p')
    ->andWhere('p.exampleField = :val')
    ->setParameter('val', $value)
    ->orderBy('p.id', 'ASC')
    ->setMaxResults(10)
    ->getQuery()
    ->getResult()
    ;
    }
     */

    /*
public function findOneBySomeField($value): ?ProductOfferType
{
return $this->createQueryBuilder('p')
->andWhere('p.exampleField = :val')
->setParameter('val', $value)
->getQuery()
->getOneOrNullResult()
;
}
 */
}
