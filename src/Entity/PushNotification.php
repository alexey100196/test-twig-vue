<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PushNotificationRepository")
 */
class PushNotification
{
    const COMPANY_NAME_VARIABLE = 'company';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $icon;

    /**
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @ORM\Column(type="text")
     */
    private $textVariables;

    /**
     * @ORM\Column(type="string", length=24)
     */
    private $type;

    /**
     *
     * @var User|null
     * @ORM\ManyToOne(targetEntity="App\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getTextVariables(): ?string
    {
        return $this->textVariables;
    }

    public function setTextVariables(string $textVariables): self
    {
        $this->textVariables = $textVariables;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     */
    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    public function getTextWithVariables()
    {
        $text = $this->getText();
        $variables = json_decode($this->getTextVariables(), true);
        foreach ($variables as $key => $value) {
            $text = str_replace(':' . $key, $value, $text);
        }

        $text = str_replace(':'. static::COMPANY_NAME_VARIABLE, $this->getUser()->getShop()->getName(), $text);

        return $text;
    }
}
