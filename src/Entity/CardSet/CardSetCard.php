<?php

namespace App\Entity\CardSet;

use App\Entity\ReferrerLink;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardSet\CardSetCardRepository")
 */
class CardSetCard
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"card-sets", "referrer"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     * @Groups({"card-sets", "referrer"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"card-sets", "referrer"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ReferrerLink")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"card-sets", "referrer"})
     */
    private $referrerLink;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CardSet\CardSet", inversedBy="cards")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cardSet;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\CardSet\CardSetCardBackground", inversedBy="card", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"card-sets", "referrer"})
     */
    private $background;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Groups({"card-sets", "referrer"})
     */
    private $articleUrl;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Groups({"card-sets", "referrer"})
     */
    private $articleUrlName;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getReferrerLink(): ?ReferrerLink
    {
        return $this->referrerLink;
    }

    public function setReferrerLink(?ReferrerLink $referrerLink): self
    {
        $this->referrerLink = $referrerLink;

        return $this;
    }

    public function getCardSet(): ?CardSet
    {
        return $this->cardSet;
    }

    public function setCardSet(?CardSet $cardSet): self
    {
        $this->cardSet = $cardSet;

        return $this;
    }

    public function getBackground(): ?CardSetCardBackground
    {
        return $this->background;
    }

    public function setBackground(CardSetCardBackground $background): self
    {
        $this->background = $background;

        return $this;
    }

    public function getArticleUrl(): ?string
    {
        return $this->articleUrl;
    }

    public function setArticleUrl(?string $articleUrl): self
    {
        $this->articleUrl = $articleUrl;

        return $this;
    }

    public function getArticleUrlName(): ?string
    {
        return $this->articleUrlName;
    }

    public function setArticleUrlName(?string $articleUrlName): self
    {
        $this->articleUrlName = $articleUrlName;

        return $this;
    }
}
