<?php


namespace App\Service\Widget;


use App\Entity\Campaign;
use App\Entity\ReferrerLink;
use App\Entity\Shop;
use App\Entity\User;
use App\Entity\UserShop;
use App\Entity\Widget\WidgetReferenceLink;
use App\Entity\Widget\WidgetReferenceLinkBrowserFingerprint;
use App\Repository\ReferrerLinkRepository;
use App\Repository\Widget\WidgetReferenceLinkRepository;
use Doctrine\ORM\EntityManagerInterface;

class ReferenceLinkService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var WidgetReferenceLinkRepository
     */
    private $widgetReferenceLinkRepository;

    /**
     * @var ReferrerLinkRepository
     */
    private $referrerLinkRepository;

    /**
     * ReferenceLinkService constructor.
     * @param EntityManagerInterface $em
     * @param WidgetReferenceLinkRepository $widgetReferenceLinkRepository
     * @param ReferrerLinkRepository $referrerLinkRepository
     */
    public function __construct(
        EntityManagerInterface $em,
        WidgetReferenceLinkRepository $widgetReferenceLinkRepository,
        ReferrerLinkRepository $referrerLinkRepository
    )
    {
        $this->em = $em;
        $this->widgetReferenceLinkRepository = $widgetReferenceLinkRepository;
        $this->referrerLinkRepository = $referrerLinkRepository;
    }

    private function getByShopAndUser(Shop $shop, User $user, int $type, string $destinationUrl): ?WidgetReferenceLink
    {
        return $this->widgetReferenceLinkRepository->findOneForShopAndUserAndDestinationUrl($shop, $user, $type, $destinationUrl);
    }

    /**
     * @param Shop $shop
     * @param User $user
     * @param int $type
     * @param string $destinationUrl
     * @param string|null $fingerprint
     * @param string|null $image
     * @param string|null $description
     * @param Campaign|null $campaign
     * @return WidgetReferenceLink
     */
    public function generate(Shop $shop, User $user, int $type, string $destinationUrl = null, string $fingerprint = null, string $image = null, string $description = null, Campaign $campaign = null): WidgetReferenceLink
    {
        $destinationUrl = $destinationUrl ?: $shop->getWebsite();

        if ($link = $this->getByShopAndUser($shop, $user, $type, $destinationUrl)) {
            return $link;
        }

        if (!$shop->hasJoinedUser($user)) {
            $userShop = (new UserShop)
                ->setUser($user)
                ->setShop($shop);

            $this->em->persist($userShop);
        }

        if ($type === WidgetReferenceLink::TYPE_CASH_REWARD) {
            $referrerLink = $this->generateWidgetReferenceLink($shop, $user, $destinationUrl, $campaign);
        }

        return $this->generateWidgetReferenceLink(
            $shop,
            $user,
            $referrerLink ?? null,
            $destinationUrl,
            $fingerprint ?? null,
            $image,
            $description
        );
    }

    private function generateRewardLink(Shop $shop, User $user, string $destinationUrl, ?Campaign $campaign = null): ReferrerLink
    {
        $rand = '';

        for ($i = 0; $i < 20; $i++) {
            $rand .= strval(rand(0, 9));
        }

        $referrerLink = new ReferrerLink();
        $referrerLink->setShopUrlName($shop->getUrlName());
        $referrerLink->setSlug($rand);
        $referrerLink->setDestinationUrl($destinationUrl);
        $referrerLink->setNickname($user->getNickname());
        $referrerLink->setReferrer($user);
        $referrerLink->setShop($shop);
        $referrerLink->setNickname($user->getNickname());
        $referrerLink->setIsGenerated(true);
        $referrerLink->setCampaign($campaign);

        return $referrerLink;
    }

    private function generateWidgetReferenceLink(
        Shop $shop,
        User $user,
        ReferrerLink $referrerLink = null,
        string $destinationUrl = null,
        string $fingerprint = null,
        string $image = null,
        string $description = null
    ): WidgetReferenceLink
    {
        $activeCampaign = $shop->getCampaigns();
        $activeCampaign = ($activeCampaign) ? $activeCampaign[0] : $activeCampaign;

        $referenceLink = new WidgetReferenceLink();
        $referenceLink->setCampaignId($activeCampaign->getId());
        $referenceLink->setShop($shop);
        $referenceLink->setUser($user);
        $referenceLink->setDestinationUrl($destinationUrl);
        $referenceLink->setImage($image);
        $referenceLink->setDescription($description);

        if ($fingerprint) {
            $referenceLink->addBrowserFingerprint(
                WidgetReferenceLinkBrowserFingerprint::create($fingerprint)
            );
        }

        if ($referrerLink) {
            $referenceLink->setReferrerLink($referrerLink);
            $referenceLink->setType(WidgetReferenceLink::TYPE_CASH_REWARD);
            $this->em->persist($referrerLink);
        } else {
            $referenceLink->setType(WidgetReferenceLink::TYPE_COUPON_REWARD);
        }

        $this->em->persist($referenceLink);
        $this->em->flush();

        return $referenceLink;
    }
}
