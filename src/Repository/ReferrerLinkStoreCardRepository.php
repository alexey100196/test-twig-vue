<?php

namespace App\Repository;

use App\Entity\ReferrerLinkStoreCard;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ReferrerLinkStoreCard|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReferrerLinkStoreCard|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReferrerLinkStoreCard[]    findAll()
 * @method ReferrerLinkStoreCard[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReferrerLinkStoreCardRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ReferrerLinkStoreCard::class);
    }

    // /**
    //  * @return ReferrerLinkStoreCard[] Returns an array of ReferrerLinkStoreCard objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ReferrerLinkStoreCard
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
