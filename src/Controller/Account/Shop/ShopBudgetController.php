<?php

namespace App\Controller\Account\Shop;

use App\Entity\Budget;
use App\Entity\Currency;
use App\Entity\Shop;
use App\Form\Shop\EditShopBudget;
use App\Repository\CurrencyRepository;
use App\Service\Shop\ShopBudget;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ShopBudgetController extends AbstractController
{
    /**
     * @var ShopBudget
     */
    private $shopBudget;

    /**
     * ShopBudgetNumberController constructor.
     * @param ShopBudget $shopBudget
     */
    public function __construct(ShopBudget $shopBudget)
    {
        $this->shopBudget = $shopBudget;
    }

    /**
     * @Route("/account/company/budget", name="account_shop_edit_budget", methods={"GET", "POST"})
     * @Security("has_role('ROLE_SHOP')")
     * @param CurrencyRepository $currencyRepository
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(CurrencyRepository $currencyRepository, Request $request)
    {
        /** @var Shop $shop */
        $shop = $this->getUser()->getShop();

        $budget = $this->shopBudget->getOrCreateBudgetForCurrency($shop, $currencyRepository->findOneByName(Currency::EUR));

        $form = $this->createForm(EditShopBudget::class, $budget);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Budget $data */
            $data = $form->getData();
            $shop->setWizardStep(5);
            $this->shopBudget->updateBudget($shop, $data->getCurrency(), $data->getAmount(), (bool)$data->isDeclared());
            $this->addFlash(Consts::COMPANY_DETAILS_SUCCESS, 'Budget: Data saved.');

            return $this->redirectToRoute('account_shop_edit_preview');
        }

        return $this->render('account/shop/edit-budget.html.twig', [
            'shop' => $shop,
            'form' => $form->createView(),
        ]);
    }
}