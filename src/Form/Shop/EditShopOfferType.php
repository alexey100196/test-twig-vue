<?php

namespace App\Form\Shop;

use App\Entity\Shop;
use App\Form\Type\OfferType;
use App\Validator\Constraints\ShopOffer;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditShopOfferType extends OfferType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Shop::class,
            'constraints' => [
                new ShopOffer()
            ]
        ]);
    }
}
