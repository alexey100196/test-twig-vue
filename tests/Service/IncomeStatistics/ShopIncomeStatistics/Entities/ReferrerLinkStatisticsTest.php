<?php


namespace App\Tests\Service\IncomeStatistics\ShopIncomeStatistics\Entities;


use App\Service\IncomeStatistics\ShopIncomeStatistics\Entities\ReferrerLinkStatistics;
use PHPUnit\Framework\TestCase;

class ReferrerLinkStatisticsTest extends TestCase
{
    public function testToArray()
    {
        $entity = new ReferrerLinkStatistics();
        $entity->setOfferType('CPS');
        $entity->setCommissionType('fixed');
        $entity->setCommission(1);
        $entity->setTransactionsCount(1);
        $entity->setPurchasesAmount(2);
        $entity->setPurchasesPrice(10);
        $entity->setReferrerProfit(1);
        $entity->setSourceLink('https://google.com');

        $this->assertEquals([
            'offer_type' => 'CPS',
            'commission_type' => 'fixed',
            'commission' => 1,
            'transactions_count' => 1,
            'purchases_amount' => 2,
            'purchases_price' => 10,
            'referrer_profit' => 1,
            'source_link' => 'https://google.com',
        ], $entity->toArray());
    }
}