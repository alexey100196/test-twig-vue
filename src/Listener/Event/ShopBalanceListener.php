<?php


namespace App\Listener\Event;


use App\Entity\Balance;
use App\Entity\Shop;
use App\Event\Shop\ShopBalanceDecreased;
use App\Adapter\MailSender;
use App\Service\Balance\UserBalanceService;
use App\Service\Shop\ShopStatusService;
use Psr\Log\LoggerInterface;

class ShopBalanceListener
{
    /**
     * @var MailSender
     */
    private $mailSender;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var UserBalanceService
     */
    private $userBalanceService;

    /**
     * @var ShopStatusService
     */
    private $shopStatusService;

    public function __construct(
        \Twig_Environment $twig,
        MailSender $mailSender,
        LoggerInterface $logger,
        UserBalanceService $userBalanceService,
        ShopStatusService $shopStatusService
    )
    {
        $this->twig = $twig;
        $this->mailSender = $mailSender;
        $this->logger = $logger;
        $this->userBalanceService = $userBalanceService;
        $this->shopStatusService = $shopStatusService;
    }

    public function onShopBalanceDecreased(ShopBalanceDecreased $event)
    {
        $shop = $event->getShop();
        $balance = $shop->getUser()->getCurrencyBalance($event->getCurrency()->getName());
        $budget = $shop->getCurrencyBudget($event->getCurrency());
        $this->logger->warning('Shop balance under');
        if ($balance && $budget && $budget->getAmount()) {
            $percent = $balance->getAmount() * 100 / $budget->getAmount();
            $this->logger->warning('Shop balance under ' . $percent);
            if ($percent <= 20) {
                $this->informShopOwnerAboutLowBalance($shop, $balance);
            }
        }
    }

    public function onShopBalanceCompletelyConsumed(ShopBalanceDecreased $event)
    {
        $shop = $event->getShop();
        $currency = $event->getCurrency();

        if ($this->userBalanceService->hasFunds($shop->getUser(), $currency) === false) {
            $this->shopStatusService->makeShopSuspended($shop);
            $this->informShopOwnerAboutCompletelyConsumedBalance($shop);
        }
    }

    private function informShopOwnerAboutCompletelyConsumedBalance(Shop $shop)
    {
        $this->mailSender->send(
            'hello@superinterface.com',
            $shop->getUser()->getEmail(),
            'Remaining budget completely consumed.',
            $this->twig->render('email/shop/balance-consumed.html.twig')
        );
    }

    private function informShopOwnerAboutLowBalance(Shop $shop, Balance $balance)
    {
        $this->mailSender->send(
            'hello@superinterface.com',
            $shop->getUser()->getEmail(),
            sprintf('Remaining budget %s %s', $balance->getAmount(), $balance->getCurrency()->getName()),
            $this->twig->render('email/shop/low-balance.html.twig', ['balance' => $balance])
        );
    }
}