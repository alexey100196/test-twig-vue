<?php

namespace App\Command;

use App\Service\Admin\Reports\ReferrerPayoutsService;
use App\Service\Exchange\ExchangeService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateUserLastMonthIncomeCommand extends ContainerAwareCommand
{
    protected static $defaultName = 'app:update-user-last-month-income';

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get(ReferrerPayoutsService::class)->updateUsersLastMonthIncome();
    }
}
