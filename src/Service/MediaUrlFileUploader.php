<?php

namespace App\Service;

use App\Entity\Media;

class MediaUrlFileUploader
{
    /**
     * @var UrlFileUploader
     */
    private $uploader;

    /**
     * MediaUrlFileUploader constructor.
     * @param UrlFileUploader $uploader
     */
    public function __construct(UrlFileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    /**
     * @param string $url
     * @param array $allowedMimeTypes
     * @return Media
     * @throws \Exception
     */
    public function upload(string $url, array $allowedMimeTypes = [])
    {
        $fileName = $this->uploader->upload($url, $allowedMimeTypes);

        $media = new Media();
        $media->setFileName($fileName);
        // @todo
        $media->setMimeType('image/png');
        $media->setOriginalName($fileName);
        $media->setFilesystem('public');
        $media->setSize(0);

        return $media;
    }
}