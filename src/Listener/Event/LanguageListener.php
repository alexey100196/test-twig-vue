<?php

namespace App\Listener\Event;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class LanguageListener
{
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $language = $request->headers->get('widget-language', 'en');
        $request->setLocale($language);
    }
}