<?php

namespace App\Entity\CardSet;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CardSet\CardSetRepository")
 */
class CardSet
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"card-sets", "referrer"})
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CardSet\CardSetCard", mappedBy="cardSet", cascade={"persist"})
     * @Groups({"card-sets", "referrer"})
     */
    private $cards;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Groups({"card-sets", "referrer"})
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"card-sets", "referrer"})
     */
    private $columnsCount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"card-sets", "referrer"})
     */
    private $cardsWidth;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="cardSets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct()
    {
        $this->cards = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|CardSetCard[]
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    public function addCard(CardSetCard $card): self
    {
        if (!$this->cards->contains($card)) {
            $this->cards[] = $card;
            $card->setCardSet($this);
        }

        return $this;
    }

    public function removeCard(CardSetCard $card): self
    {
        if ($this->cards->contains($card)) {
            $this->cards->removeElement($card);
            // set the owning side to null (unless already changed)
            if ($card->getCardSet() === $this) {
                $card->setCardSet(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getColumnsCount(): ?int
    {
        return $this->columnsCount;
    }

    public function setColumnsCount(int $columnsCount): self
    {
        $this->columnsCount = $columnsCount;

        return $this;
    }

    public function getCardsWidth(): ?int
    {
        return $this->cardsWidth;
    }

    public function setCardsWidth(?int $cardsWidth): self
    {
        $this->cardsWidth = $cardsWidth;

        return $this;
    }

    public function createEmptyCard()
    {
        $card = new CardSetCard();
        $card->setCardSet($this);

        return $card;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @Groups({"card-sets", "referrer"})
     * @VirtualProperty()
     */
    public function getGeneratedAt()
    {
        return $this->createdAt;
    }
}
