<?php


namespace App\Service\IncomeStatistics\ShopIncomeStatistics;


use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateReferrerLinkStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateReferrerReflinksStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateReferrersStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateSingleSourceTrafficStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateSourceTrafficStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Commands\GenerateSummaryStatistics;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Handlers\ReferrerLinkStatisticsHandler;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Handlers\ReferrerReflinksStatisticsHandler;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Handlers\ReferrersStatisticsHandler;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Handlers\SingleSourceTrafficStatisticsHandler;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Handlers\SourceTrafficStatisticsHandler;
use App\Service\IncomeStatistics\ShopIncomeStatistics\Handlers\SummaryStatisticsHandler;
use App\Service\IncomeStatistics\StatisticsService;

class ShopStatisticsService extends StatisticsService
{
    /**
     * @var array
     */
    protected $handlers = [
        GenerateReferrersStatistics::class => ReferrersStatisticsHandler::class,
        GenerateReferrerReflinksStatistics::class => ReferrerReflinksStatisticsHandler::class,
        GenerateReferrerLinkStatistics::class => ReferrerLinkStatisticsHandler::class,
        GenerateSummaryStatistics::class => SummaryStatisticsHandler::class,
        GenerateSourceTrafficStatistics::class => SourceTrafficStatisticsHandler::class,
        GenerateSingleSourceTrafficStatistics::class => SingleSourceTrafficStatisticsHandler::class,
    ];
}
