<?php

namespace App\Form\Referrer\Parts\ReferrerLinkCard;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Range;

class CardDimensionType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'constraints' => [
                new Range(['min' => 200, 'max' => 1000])
            ],
        ]);
    }

    public function getParent()
    {
        return IntegerType::class;
    }
}