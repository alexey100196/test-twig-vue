<?php


namespace App\Service\SystemCommission\ShopCommission\Strategies;


use App\Entity\Shop;
use App\Helper\Math;
use App\Service\SystemCommission\ValueObjects\TaxedAmount;

class SaasStrategy implements StrategyInterface
{
    public function apply(Shop $shop, $amount, $fee)
    {
        $amountAfterFee = Math::mul($amount, $fee, 2);

        return new TaxedAmount($amountAfterFee, $fee);
    }
}
