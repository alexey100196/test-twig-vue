<?php

namespace App\Controller\Account\Shop;

use App\Entity\Shop;
use App\Entity\Widget\WidgetShopSetting;
use App\Event\ContactRequest\ContactRequestCreated;
use App\Form\ContactRequestType;
use App\Form\Shop\EditShopCookieType;
use App\Form\Shop\EditShopType;
use App\Form\Shop\ShopOfferType;
use App\Repository\OfferTypeRepository;
use App\Repository\ShopRepository;
use App\Repository\UserShopRepository;
use App\Service\Shop\ShopUpdate;
use App\Service\Shop\ShopCookie;
use App\Service\Shop\ShopMainOffer;
use App\Util\Consts;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ShopController extends AbstractController
{
    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * @var ShopUpdate
     */
    private $shopUpdate;

    /**
     * @var OfferTypeRepository
     */
    private $offerTypeRepository;

    /**
     * ShopController constructor.
     * @param ShopRepository $shopRepository
     * @param ShopUpdate $shopUpdate
     * @param OfferTypeRepository $offerTypeRepository
     */
    public function __construct(
        ShopRepository $shopRepository,
        ShopUpdate $shopUpdate,
        OfferTypeRepository $offerTypeRepository
    )
    {
        $this->shopRepository = $shopRepository;
        $this->shopUpdate = $shopUpdate;
        $this->offerTypeRepository = $offerTypeRepository;
    }

    private function updateShop(Shop $shop)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($shop);
        $entityManager->flush();
    }


    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company", name="account_shop_home")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function company(Request $request)
    {
        return $this->render('account/shop/home/index.html.twig');
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account", name="account_modals_list")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $shop = $this->getUser()->getShop();

        return $this->render('account/shop/modals-list.html.twig', ['shop' => $shop]);
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/edit", name="account_shop_edit")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, LoggerInterface $logger)
    {
        $shop = $this->getUser()->getShop();

        try {
            $form = $this->createForm(EditShopType::class, $shop);
            $form->handleRequest($request);
        }catch (\Exception $e) {
            $logger->error($e);
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $this->shopUpdate->save($shop, $form);

            $this->addFlash(Consts::COMPANY_DETAILS_SUCCESS, 'Company Details: Data saved.');

            return $this->redirectToRoute('account_shop_edit_offer');
        }

        return $this->render('account/shop/edit.html.twig', ['form' => $form->createView(), 'shop' => $shop]);
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/logo/delete", name="account_shop_logo_delete", methods={"POST"})
     */
    public function deleteLogo(Request $request)
    {
        $shop = $this->getUser()->getShop();

        if ($logo = $shop->getLogo()) {
            $shop->setLogo(null);
            $this->updateShop($shop);
        }

        return new JsonResponse(['message' => 'Logo deleted.']);
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/edit/offer", name="account_shop_edit_offer")
     * @param Request $request
     * @param ShopMainOffer $mainOffer
     * @param UserShopRepository $userShopRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editOffer(
        Request $request,
        ShopMainOffer $mainOffer,
        UserShopRepository $userShopRepository,
        EntityManagerInterface $entityManager
    )
    {
        /** @var Shop $shop */
        $shop = $this->getUser()->getShop();
        $originalCpsOffer = clone $shop->getCpsOffer();
        $originalCplOffer = $shop->getCplOffer() ? clone $shop->getCplOffer() : null;
        $oldRewardMode = $shop->getRewardsMode();

        $newRewardMode = (int)($request->request->get('shop_offer')['rewardsMode'] ?? Shop::REWARD_MODE_CASH);
        $changedToCashReward = in_array($newRewardMode, [Shop::REWARD_MODE_CASH, Shop::REWARD_MODE_COUPON_CODES_AND_CASH]);

        $form = $this->createForm(ShopOfferType::class, $shop, [
            'validate_min_amount_cps' => $changedToCashReward,
            'validate_min_amount_cpl' => $changedToCashReward
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shop->setWizardStep(
                2,
                $oldRewardMode !== $newRewardMode && in_array(Shop::REWARD_MODE_COUPON_CODES, [$oldRewardMode, $newRewardMode])
            );

            if ($changedToCashReward) {
                $mainOffer->saveMainOffer($shop);
            } else {
                $shop->setCpsOffer($originalCpsOffer);
                $shop->setCplOffer($originalCplOffer);
                $entityManager->persist($shop);
                $entityManager->flush();
            }
            $this->updateWidgetSettingRewardsEnabled($shop->getRewardsMode(), $shop->getWidgetSettingOrCreateNew());
            $this->addFlash(Consts::COMPANY_DETAILS_SUCCESS, 'Main Offer: Data saved.');
            return $this->redirectToRoute($shop->hasCashRewards() ? 'account_shop_edit_products' : 'account_shop_edit_snipset');
        }

        return $this->render('account/shop/edit-offer.html.twig', [
            'shop' => $shop,
            'form' => $form->createView(),
            'joinedUsersCount' => $userShopRepository->countByShop($shop),
        ]);
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/edit/products", name="account_shop_edit_products")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editProducts(Request $request)
    {
        $shop = $this->getUser()->getShop();

        if ($shop->setWizardStep(3)) {
            $this->updateShop($shop);
        }

        if (!$shop->hasProducts() && $shop->isEcommerce()) {
            $this->addFlash('info', 'This step is optional, so you can skip it by clicking on the button \'Preview\'.');
            return $this->redirectToRoute('shop_product_create');
        }

        return $this->render('account/shop/edit-products.html.twig', [
            'shop' => $shop,
        ]);
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/edit/cookie", name="account_shop_edit_cookie")
     * @param Request $request
     * @param ShopCookie $shopCookie
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editCookie(Request $request, ShopCookie $shopCookie)
    {
        $shop = $this->getUser()->getShop();

        $form = $this->createForm(EditShopCookieType::class, $shop);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shop->setWizardStep(6);
            $shopCookie->updateCookieLifeTime($shop, $shop->getCookieLifetime(), $shop->getBannerCookieLifetime());
            $this->addFlash(Consts::COMPANY_DETAILS_SUCCESS, 'Cookie lifespan: Data saved.');

            return $this->redirectToRoute('account_shop_edit_snipset');
        }

        return $this->render('account/shop/edit-cookies.html.twig', ['form' => $form->createView(), 'shop' => $shop]);
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @param Request $request
     * @param EventDispatcherInterface $eventDispatcher
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/account/company/edit/snipset", name="account_shop_edit_snipset")
     */
    public function editSnipset(Request $request, EventDispatcherInterface $eventDispatcher)
    {
        $shop = $this->getUser()->getShop();

        $contactRequestForm = $this->createForm(ContactRequestType::class);
        $contactRequestForm->handleRequest($request);

        if ($shop->setWizardStep(7)) {
            $this->updateShop($shop);
        }

        if ($contactRequestForm->isSubmitted() && $contactRequestForm->isValid()) {
            $contactRequestForm->getData()->setShop($shop);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contactRequestForm->getData());
            $entityManager->flush();

            $eventDispatcher->dispatch(ContactRequestCreated::NAME, new ContactRequestCreated($contactRequestForm->getData()));

            $this->addFlash(Consts::SUCCESS, 'Message sent.');

            return $this->redirectToRoute('account_shop_edit_snipset');
        }

        return $this->render('account/shop/edit-snipset.html.twig', [
            'shop' => $shop,
            'contactRequestForm' => $contactRequestForm->createView(),
        ]);
    }

    /**
     * @Security("has_role('ROLE_SHOP') and is_granted('SHARE_SHOP', user.getShop())")
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/account/company/edit/share", name="account_shop_edit_share")
     */
    public function editShare()
    {
        $shop = $this->getUser()->getShop();

        if ($shop->setWizardStep(12)) {
            $this->updateShop($shop);
        }

        return $this->render('account/shop/edit-share.html.twig', compact('shop'));
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/account/company/edit/finish", name="account_shop_edit_finish")
     */
    public function editFinish()
    {
        $shop = $this->getUser()->getShop();

        if ($shop->setWizardStep(11)) {
            $this->updateShop($shop);
        }

        return $this->render('account/shop/edit-finish.html.twig', compact('shop'));
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/edit/entry", name="account_shop_edit_entry")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editEntry(Request $request)
    {
        $shop = $this->getUser()->getShop();

        return $this->render('account/shop/edit-entry.html.twig', ['shop' => $shop]);
    }

    private function updateWidgetSettingRewardsEnabled(int $rewardsMode, ?WidgetShopSetting $widgetShopSetting)
    {
        if ($widgetShopSetting) {
            if ($rewardsMode === Shop::REWARD_MODE_COUPON_CODES_AND_CASH) {
                $widgetShopSetting->setReferrerRewardCashEnabled(true);
                $widgetShopSetting->setReferrerRewardCouponEnabled(true);
            } else {
                $widgetShopSetting->setReferrerRewardCashEnabled(Shop::REWARD_MODE_CASH === $rewardsMode);
                $widgetShopSetting->setReferrerRewardCouponEnabled(Shop::REWARD_MODE_COUPON_CODES === $rewardsMode);
            }

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($widgetShopSetting);
            $entityManager->flush();
        }
    }
}
