<?php

namespace App\Service\IncomeStatistics\ShopIncomeStatistics\Commands;

use App\Entity\Shop;
use App\Service\IncomeStatistics\CommandInterface;

class GenerateSourceTrafficStatistics implements CommandInterface
{
    /** @var Shop */
    public $shop;

    /** @var \DateTime */
    public $startDate;

    /** @var \DateTime */
    public $endDate;

    /** @var array */
    public $productsIds;

    /** @var bool */
    public $includeMainOffer;

    public function __construct(
        Shop $shop,
        \DateTime $startDate,
        \DateTime $endDate,
        array $productsIds = [],
        bool $includeMainOffer = false
    )
    {
        $this->shop = $shop;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->productsIds = $productsIds;
        $this->includeMainOffer = $includeMainOffer;
    }
}