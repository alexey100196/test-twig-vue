<?php

namespace App\Controller\Account\Shop;

use App\Entity\OfferType;
use App\Entity\Product;
use App\Entity\Shop;
use App\Form\Shop\Product\ProductType;
use App\Manager\ShopManager;
use App\Repository\ProductRepository;
use App\Repository\ShopRepository;
use App\Repository\UserShopRepository;
use App\Service\Shop\ShopProduct;
use App\Util\Consts;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ShopProductController extends AbstractController
{
    /**
     * @var ShopRepository
     */
    private $shopRepository;

    /**
     * @var ShopManager
     */
    private $shopManager;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(
        ShopRepository $shopRepository,
        ShopManager $shopManager,
        ProductRepository $productRepository
    )
    {
        $this->shopRepository = $shopRepository;
        $this->shopManager = $shopManager;
        $this->productRepository = $productRepository;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function stepNotAvailableForSaasResponse()
    {
        $this->addFlash('info', 'This option is available only for E-commerce.');

        return $this->redirectToRoute('account_shop_edit_preview');
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/product/create", name="shop_product_create")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        /** @var Shop $shop */
        $shop = $this->getUser()->getShop();

        if ($shop->hasMainOffer() === false) {
            $this->addFlash(Consts::ERROR, 'You have to pass Main Offer first.');
            return $this->redirectToRoute('account_shop_edit_offer');
        }

        if ($shop->isEcommerce() === false) {
            return $this->stepNotAvailableForSaasResponse();
        }

        $form = $this->createForm(ProductType::class, $product = $shop->createNewProduct());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $offerType = $this->getDoctrine()->getRepository(OfferType::class)->findOneBy(['name' => OfferType::CPS]);
            $form->getData()->getCpsOffer()->setType($offerType);

            $shop->addProduct($product);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($shop);
            $entityManager->flush();

            $this->addFlash(Consts::SUCCESS, 'Offer created.');

            return $this->redirectToRoute('account_shop_edit_products');
        }

        return $this->render('account/shop/product/create.html.twig', [
            'form' => $form->createView(),
            'shop' => $shop,
        ]);
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/product/{product_id}/edit", name="shop_product_edit")
     * @param Request $request
     * @param UserShopRepository $userShopRepository
     * @param ShopProduct $shopProductService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function edit(Request $request, UserShopRepository $userShopRepository, ShopProduct $shopProductService)
    {
        $shop = $this->getUser()->getShop();
        $product = $shop->getProductById($request->get('product_id'));

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $shopProductService->save($product);
            $this->addFlash(Consts::SUCCESS, 'Changes saved.');

            return $this->redirectToRoute('account_shop_edit_products');
        }

        return $this->render('account/shop/product/edit.html.twig', [
            'form' => $form->createView(),
            'shop' => $shop,
            'joinedUsersCount' => $userShopRepository->countByShop($shop),
        ]);
    }

    /**
     * @Security("has_role('ROLE_SHOP')")
     * @Route("/account/company/product/{product_id}", name="shop_product_delete", methods={"DELETE"})
     */
    public function delete(Request $request)
    {
        $product = $this->productRepository->find($request->get('product_id'));
        $shop = $this->getUser()->getShop();

        if ($product->getShop() !== $shop) {
            throw new \Exception('No permissions.', 1);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($product);
        $entityManager->flush();

        $this->addFlash(Consts::SUCCESS, 'Product removed.');

        return $this->redirectToRoute('account_shop_edit_products');
    }
}
