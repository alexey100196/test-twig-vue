<?php


namespace App\Controller;


use App\Entity\ReferrerStoreNonReferableLink;
use App\Entity\ReferrerNonReferableLinkCard;
use App\Service\Referrer\ReferrerStoreNonReferableLinkService;
use App\Service\Referrer\ReferrerNonReferableLinkCardService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ReferrerNonReferableLinkController extends AbstractController
{
    /**
     * @var ReferrerStoreNonReferableLinkService
     */
    private $referrerStoreNonReferableLinkService;

    /**
     * @var ReferrerNonReferableLinkCardService
     */
    private $referrerNonReferableLinkCardService;

    /**
     * ReferrerLinkController constructor.
     * @param ReferrerStoreNonReferableLinkService $referrerStoreNonReferableLinkService
     * @param ReferrerNonReferableLinkCardService $referrerNonReferableLinkCardService
     */
    public function __construct(
        ReferrerStoreNonReferableLinkService $referrerStoreNonReferableLinkService,
        ReferrerNonReferableLinkCardService $referrerNonReferableLinkCardService
    ) {
        $this->referrerStoreNonReferableLinkService = $referrerStoreNonReferableLinkService;
        $this->referrerNonReferableLinkCardService = $referrerNonReferableLinkCardService;
    }

    /**
     * @Route("/card", name="referrer_not_logged_link_card")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function card()
    {
        return $this->render('notlogged/card.html.twig', []);
    }
}