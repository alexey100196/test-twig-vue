<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use App\ValueObject\Url;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReferrerStoreNonReferableLinkRepository")
 */
class ReferrerStoreNonReferableLink
{
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ReferrerMyStore", inversedBy="nonReferableLinks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $mystore;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ReferrerNonReferableLinkCard", inversedBy="nonReferableLinks")
     * @ORM\JoinColumn(nullable=true)
     */
    private $card;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $destinationUrl;

    /**
     * @ORM\Column(type="smallint")
     */
    private $orderColumn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $customTitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $video;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $layout;

    public function __construct()
    {
        $this->orderColumn = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMyStore(): ?ReferrerMyStore
    {
        return $this->mystore;
    }

    public function setMyStore(?ReferrerMyStore $mystore): self
    {
        $this->mystore = $mystore;

        return $this;
    }

    public function getDestinationUrl(): ?Url
    {
        return $this->destinationUrl ? new Url($this->destinationUrl) : null;
    }

    public function setDestinationUrl(string $destinationUrl): self
    {
        $this->destinationUrl = $destinationUrl;

        return $this;
    }

    public function getOrderColumn(): ?int
    {
        return $this->orderColumn;
    }

    public function setOrderColumn(int $orderColumn): self
    {
        $this->orderColumn = $orderColumn;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getCustomTitle()
    {
        return $this->customTitle;
    }

    /**
     * @param mixed $customTitle
     */
    public function setCustomTitle($customTitle): void
    {
        $this->customTitle = $customTitle;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    public function getVideo(): ?string
    {
        return htmlspecialchars_decode($this->video);
    }

    public function setVideo(?string $video): self
    {
        $this->video = ($video ? htmlspecialchars($video) : null);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param mixed $layout
     */
    public function setLayout($layout): void
    {
        $this->layout = $layout;
    }

    /**
     * @return ReferrerNonReferableLinkCard
     */
    public function getCardOrCreate()
    {
        if (!$this->card) {
            $this->card = new ReferrerNonReferableLinkCard();
            $this->card->setReferrerLink($this);
        }

        return $this->card;
    }

    public function getCard(): ?ReferrerNonReferableLinkCard
    {
        return $this->card;
    }

    public function setCard(?ReferrerNonReferableLinkCard $card): self
    {
        $this->card = $card;
    }
}
