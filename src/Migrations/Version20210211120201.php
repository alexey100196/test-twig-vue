<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210211120201 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE campaigns CHANGE setting_id setting_id INT DEFAULT NULL, CHANGE shop_id shop_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE media ADD content LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE widget_shop_setting DROP INDEX UNIQ_78772D33C0D8FDFA, ADD INDEX IDX_78772D33C0D8FDFA (welcome_picture_id)');
        $this->addSql('ALTER TABLE widget_shop_setting DROP FOREIGN KEY FK_78772D33891232B8');
        $this->addSql('ALTER TABLE widget_shop_setting DROP FOREIGN KEY FK_78772D33CCBE0094');
        $this->addSql('ALTER TABLE widget_shop_setting DROP referrer_link_validity_days');
        $this->addSql('ALTER TABLE widget_shop_setting ADD CONSTRAINT FK_78772D33891232B8 FOREIGN KEY (invitee_reward_coupon_package_id) REFERENCES coupon_package (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE widget_shop_setting ADD CONSTRAINT FK_78772D33CCBE0094 FOREIGN KEY (referrer_reward_coupon_package_id) REFERENCES coupon_package (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE campaigns CHANGE setting_id setting_id INT NOT NULL, CHANGE shop_id shop_id INT NOT NULL');
        $this->addSql('ALTER TABLE media DROP content');
        $this->addSql('ALTER TABLE widget_shop_setting DROP INDEX IDX_78772D33C0D8FDFA, ADD UNIQUE INDEX UNIQ_78772D33C0D8FDFA (welcome_picture_id)');
        $this->addSql('ALTER TABLE widget_shop_setting DROP FOREIGN KEY FK_78772D33CCBE0094');
        $this->addSql('ALTER TABLE widget_shop_setting DROP FOREIGN KEY FK_78772D33891232B8');
        $this->addSql('ALTER TABLE widget_shop_setting ADD referrer_link_validity_days SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE widget_shop_setting ADD CONSTRAINT FK_78772D33CCBE0094 FOREIGN KEY (referrer_reward_coupon_package_id) REFERENCES coupon_package (id)');
        $this->addSql('ALTER TABLE widget_shop_setting ADD CONSTRAINT FK_78772D33891232B8 FOREIGN KEY (invitee_reward_coupon_package_id) REFERENCES coupon_package (id)');
    }
}
