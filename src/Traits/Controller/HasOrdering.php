<?php


namespace App\Traits\Controller;


use App\ValueObject\Sort;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

trait HasOrdering
{
    /**
     * @param Sort $sort
     * @param array $allowed
     */
    private function validateOrderByField(Sort $sort, array $allowed): void
    {
        if (in_array($sort->getField(), $allowed) === false) {
            throw new HttpException(Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param Request $request
     * @param array $allowed
     * @return Sort|null
     * @throws \Exception
     */
    protected function getOrderByFromRequest(Request $request, array $allowed = []): ?Sort
    {
        if ($request->query->has('orderBy')) {
            $sort = new Sort($request->query->get('orderBy'));

            if (0 !== count($allowed)) {
                $this->validateOrderByField($sort, $allowed);
            }

            return $sort;
        }

        return null;
    }
}