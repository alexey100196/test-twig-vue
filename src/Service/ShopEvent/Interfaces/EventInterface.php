<?php


namespace App\Service\ShopEvent\Interfaces;


use App\Service\ShopEvent\Events\Event;

interface EventInterface
{
    public function createFromArray(): Event;
}