<?php


namespace App\Event\Conversion;


use App\Entity\ShopConversion;

interface ConversionEventInterface
{
    public function getConversion(): ShopConversion;
}