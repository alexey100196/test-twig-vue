<?php


namespace App\Tests\Helper;


use App\Helper\UrlHelpers;
use PHPUnit\Framework\TestCase;

class UrlHelpersTest extends TestCase
{
    public function testAddGetParam()
    {
        $result = UrlHelpers::addGetParam('http://anywebsite.com', 'test', 'ok');

        $this->assertEquals($result, 'http://anywebsite.com?test=ok');
    }

    public function getUrlsToCompare()
    {
        yield ['http://anwebsite.com', 'http://anwebsite.com', false, true];
        yield ['http://anwebsite.com', 'https://anwebsite.com', false, true];
        yield ['http://anwebsite.com', 'https://anwebsite.com', true, false];
        yield ['https://anwebsite.com', 'https://anwebsite.com', true, true];
        yield ['https://anwebsite.com', 'https://anyotherwebsite.com', false, false];
    }

    /**
     * @dataProvider getUrlsToCompare
     */
    public function testCompareUrls($leftUrl, $rightUrl, $checkHttps, $expectedResult)
    {
        $result = UrlHelpers::compareUrls($leftUrl, $rightUrl, $checkHttps);

        $this->assertEquals($result, $expectedResult);
    }
}