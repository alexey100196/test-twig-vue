<?php


namespace App\Listener\Event;


use App\Event\Subscription\CantChargeUserForSubscription;
use App\Event\Subscription\UserChargedForSubscription;
use App\Service\Shop\ShopStatusService;
use App\Service\Subscription\SubscriptionService;

class SubscriptionPaymentListener
{
    /**
     * @var SubscriptionService
     */
    private $subscriptionService;

    /**
     * @var ShopStatusService
     */
    private $shopStatusService;

    public function __construct(
        SubscriptionService $subscriptionService,
        ShopStatusService $shopStatusService
    )
    {
        $this->subscriptionService = $subscriptionService;
        $this->shopStatusService = $shopStatusService;
    }

    /**
     * @param CantChargeUserForSubscription $event
     * @throws \Doctrine\ORM\ORMException
     */
    public function onSubscriptionChargeFailed(CantChargeUserForSubscription $event)
    {
        $user = $event->getSubscription()->getUser();

        if ($shop = $user->getShop()) {
            $this->shopStatusService->makeShopSuspended($shop);
        }
    }

    /**
     * @param UserChargedForSubscription $event
     * @throws \Doctrine\ORM\ORMException
     */
    public function onSubscriptionCharge(UserChargedForSubscription $event)
    {
        $user = $event->getSubscription()->getUser();

        $this->subscriptionService->renewPlan($event->getSubscription());

        if ($shop = $user->getShop()) {
            $this->shopStatusService->makeShopActive($shop);
        }
    }
}