<?php


namespace App\Service\Subscription\SubscriptionValidation\Validators;


use App\Entity\Shop;
use App\Entity\Subscription\Plan;
use App\Entity\Subscription\PlanType;
use App\Entity\User;

class CanSubscribePlanWithType implements ValidatorInterface
{
    /**
     * {@inheritdoc}
     */
    public function validate(User $user, Plan $plan): void
    {
        /** @var Shop $shop */
        $shop = $user->getShop();

        if (null === $shop) {
            throw new SubscriptionValidationException('Pricing plan for Referral Partners is not needed.');
        }

        /** @var PlanType $planType */
        if ($planType = $plan->getType()) {
            if ($shop->isEcommerce() && $planType->getId() !== PlanType::SHOP_ECOMMERCE) {
                throw new SubscriptionValidationException('Plan available only for e-commerce shops.');
            } elseif ($shop->isSaas() && $planType->getId() !== PlanType::SHOP_SAAS) {
                throw new SubscriptionValidationException('Plan available only for saas shops.');
            }
        }
    }
}
