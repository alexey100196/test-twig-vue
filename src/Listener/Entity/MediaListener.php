<?php
namespace App\Listener\Entity;

use App\Entity\Media;
use App\Helper\MediaUrlGenerator;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class MediaListener
{
    /** @var String */
    private $path;

    /** @var array */
    private $entityChangeSet = null;

    /** @var  EventDispatcher */
    protected $dispatcher;

    /**
     * @var MediaUrlGenerator
     */
    private $mediaUrlGenerator;

    public function __construct(MediaUrlGenerator $mediaUrlGenerator)
    {
        $this->mediaUrlGenerator = $mediaUrlGenerator;
    }

    public function preUpdate(Media $media, PreUpdateEventArgs $args): void
    {
    /*    if ($args->getOldValue('fileName') != $args->getNewValue('fileName')) {
            $old = clone $media;
            $old->setFileName($args->getOldValue('fileName'));
            if (file_exists($path = $this->mediaUrlGenerator->getRealPath($old))) {
                unlink($path);
            }
        }*/
    }

    public function postUpdate(Media $media, LifecycleEventArgs $args): void
    {
        //
    }

    public function postRemove(Media $media, LifecycleEventArgs $args): void
    {
    }

    public function preRemove(Media $media, LifecycleEventArgs $args): void
    {
        if (file_exists($path = $this->mediaUrlGenerator->getRealPath($media))) {
            unlink($path);
        }
    }

    /**
     * @param EventDispatcher $dispatcher
     * @required
     */
    public function setDispatcher(EventDispatcherInterface $dispatcher): void
    {
        $this->dispatcher = $dispatcher;
    }
}
