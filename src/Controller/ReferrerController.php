<?php


namespace App\Controller;


use App\Entity\ReferrerLinkStoreCard;
use App\Entity\ReferrerMyStore;
use App\Entity\ReferrerStoreNonReferableLink;
use App\Entity\User;
use App\Form\Referrer\ReferrerLinkStoreCardType;
use App\Form\Referrer\ReferrerMyStoreType;
use App\Form\Referrer\ReferrerStoreNonReferableLinkType;
use App\Repository\ReferrerStoreNonReferableLinkRepository;
use App\Repository\UserRepository;
use App\Service\Referrer\ReferrerLinkStoreCardService;
use App\Service\Referrer\ReferrerMyStoreService;
use App\Service\Referrer\ReferrerShop;
use App\Service\Referrer\ReferrerShopLink;
use App\Service\Referrer\ReferrerStoreNonReferableLinkService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ReferrerController extends AbstractController
{
    /**
     * @var ReferrerShopLink
     */
    private $referrerShopLink;
    /**
     * @var ReferrerMyStoreService
     */
    private $referrerMyStoreService;

    /**
     * @var ReferrerLinkStoreCardService
     */
    private $referrerLinkStoreCardService;

    /**
     * @var ReferrerStoreNonReferableLinkService
     */
    private $referrerStoreNonReferableLinkService;

    /**
     * @var ReferrerStoreNonReferableLinkRepository
     */
    private $nonReferableLinkRepository;

    /**
     * ReferrerController constructor.
     * @param ReferrerMyStoreService $referrerMyStoreService
     * @param ReferrerLinkStoreCardService $referrerLinkStoreCardService
     * @param ReferrerStoreNonReferableLinkRepository $nonReferableLinkRepository
     */
    public function __construct(
        ReferrerShopLink $referrerShopLink,
        ReferrerMyStoreService $referrerMyStoreService,
        ReferrerLinkStoreCardService $referrerLinkStoreCardService,
        ReferrerStoreNonReferableLinkService $referrerStoreNonReferableLinkService,
        ReferrerStoreNonReferableLinkRepository $nonReferableLinkRepository
    ) {
        $this->referrerShopLink = $referrerShopLink;
        $this->referrerMyStoreService = $referrerMyStoreService;
        $this->referrerLinkStoreCardService = $referrerLinkStoreCardService;
        $this->referrerStoreNonReferableLinkService = $referrerStoreNonReferableLinkService;
        $this->nonReferableLinkRepository = $nonReferableLinkRepository;
    }

    /**
     * @Security("has_role('ROLE_REFERRER')")
     * @Route("/store/me", name="referrer_show_me", methods={"GET"})
     * @param ReferrerShop $referrerShop
     * @return Response
     */
    public function showMe(ReferrerShop $referrerShop)
    {
        $user = $this->getUser();
        $myStore = $user->getReferrerMyStore();
        $sort = (Criteria::create())->orderBy(['createdAt' => Criteria::DESC]);
        $globalStats = $referrerShop->getReferrerStats($user);
        $nonReferableLinksArray = $this->referrerStoreNonReferableLinkService->loadItemsToArray(
            $this->nonReferableLinkRepository->findBy(['mystore' => $myStore], ['orderColumn' => 'DESC'])
        );
        $myStoreReferrerLinksArray = $this->referrerLinkStoreCardService->loadItemsToArray(
            $user,
            $user->getMyStoreReferrerLinks()->matching($sort)
        );
        usort($myStoreReferrerLinksArray, [$this, 'sortByOrderColumn']);
        $companies = $referrerShop->getReferrerShops($user);

        $companiesArray = [];
        foreach($companies as $company) {
            $companiesArray[] = [
                'id' => $company->getId(),
                'name' => $company->getName(),
            ];
        }

        return $this->render('referrer/show.html.twig', [
            'referrer' => $user,
            'companies' => $companiesArray,
            'referrerMyStore' => $this->referrerMyStoreService->objectToArray($user->getReferrerMyStore()),
            'nonReferableLinksArray' => $nonReferableLinksArray,
            'myStoreReferrerLinksArray' => $myStoreReferrerLinksArray,
            'globalStats' => $globalStats,
        ]);
    }

    /**
     * @Security("has_role('ROLE_REFERRER')")
     * @Route("/referrer/me", name="referrer_update_me", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function updateMe(Request $request)
    {
        $user = $this->getUser();
        $sort = (Criteria::create())->orderBy(['createdAt' => Criteria::DESC]);
        $data = $request->request->all();
        $data['avatar'] = $request->files->get('avatar');
        $form = $this->createForm(ReferrerMyStoreType::class);
        $myStore = $user->getReferrerMyStore();
        if (!$myStore) {
            $myStore = $this->referrerMyStoreService->generate($user, new ReferrerMyStore());
        }
        $yourLinks = $request->get('yourLinks', []);
        unset($data['yourLinks']);
        $affiliateLinks = $request->get('affiliateLinks', []);
        unset($data['affiliateLinks']);

        $form->submit($data);
        if (!$form->isValid()) {
            $errorMessages = $this->getFormErrorMessages($form);

            return new JsonResponse(['errors' => $errorMessages], Response::HTTP_OK);
        } else if (count($yourLinks) > ReferrerMyStore::MAX_CUSTOM_LINKS) {
            return new JsonResponse(['errors' => [
                'Your links' => 'You can add only '.ReferrerMyStore::MAX_CUSTOM_LINKS.' custom links (Present count: '.count($yourLinks).').'
            ]], Response::HTTP_OK);
        }

        $yourOldLinks = ($myStore && $myStore->getNonReferableLinks()) ? $myStore->getNonReferableLinks()->getValues() : [];
        foreach($yourLinks as $key => $link) {
            $linkData = $link;
            if (isset($linkData['id'])) unset($linkData['id']);
            $ylForm = $this->createForm(ReferrerStoreNonReferableLinkType::class);
            $ylForm->submit($linkData);

            if (!$ylForm->isValid()) {
                $ylErrorMessages = $this->getFormErrorMessages($ylForm);

                return new JsonResponse(['errors' => $ylErrorMessages], Response::HTTP_OK);
            }

            if (!isset($yourOldLinks[$key])) {
                $yourOldLinks[$key] = new ReferrerStoreNonReferableLink();
                $yourOldLinks[$key]->setMyStore($myStore);
            }
            $yourOldLinks[$key]->setDestinationUrl($linkData['destinationUrl']);
            $yourOldLinks[$key]->setImage(isset($linkData['image']) ? $linkData['image'] : null);
            $yourOldLinks[$key]->setVideo(isset($linkData['video']) ? $linkData['video'] : null);
            $yourOldLinks[$key]->setLayout(isset($linkData['layout']) ? $linkData['layout'] : null);
            $yourOldLinks[$key]->setCustomTitle(isset($linkData['customTitle']) ? $linkData['customTitle'] : null);
            if (isset($linkData['title'])) {
                $yourOldLinks[$key]->setTitle($linkData['title']);
                $yourOldLinks[$key]->setCustomTitle(null);
            }
            $yourOldLinks[$key]->setOrderColumn($key);
        }

        $myStoreOldReferrerLinks = $user->getMyStoreReferrerLinks()->matching($sort)->toArray();
        $newDataDestinationUrls = array_map(array($this, 'getDestinationUrl'), $affiliateLinks);
        $affiliateResults = [];
        foreach ($myStoreOldReferrerLinks as $key => $link) {
            $lDestUrl = $link->getDestinationUrl() ? $link->getDestinationUrl()->getProtocol().'://'.$link->getDestinationUrl()->getUrl() : '';
            $foundKey = array_search($lDestUrl, $newDataDestinationUrls);

            if ($lDestUrl && $foundKey >= 0) {
                $newData = $affiliateLinks[$foundKey];
                $myStoreOldReferrerLinks[$key]->setShowInMyStoreAsCard((bool) $newData['showInMyStoreAsCard']);
                $oldStoreCard = $myStoreOldReferrerLinks[$key]->getStoreCard() ?? new ReferrerLinkStoreCard();
                if (isset($newData['showInMyStoreAsCard']) && $newData['showInMyStoreAsCard'] == 'true') {
                    $storeCard = $newData['storeCard'];
                    $alForm = $this->createForm(ReferrerLinkStoreCardType::class);
                    $alForm->submit($storeCard);

                    if (!$alForm->isValid()) {
                        $alErrorMessages = $this->getFormErrorMessages($alForm);

                        return new JsonResponse(['errors' => $alErrorMessages], Response::HTTP_OK);
                    }

                    $oldStoreCard->setTitle($storeCard['title'] ?? '-');
                    $oldStoreCard->setDescription($storeCard['description'] ?? null);
                    $oldStoreCard->setImage($storeCard['image'] ?? null);
                    $oldStoreCard->setImageWidth($storeCard['imageWidth'] ?? null);
                    $oldStoreCard->setOrderColumn($foundKey);

                    array_push($affiliateResults, $oldStoreCard);
                }
                if (!$oldStoreCard->getId()) {
                    $myStoreOldReferrerLinks[$key]->setStoreCard($oldStoreCard);
                }
                $this->referrerShopLink->save($myStoreOldReferrerLinks[$key]);
            }
        }

        $currentAvatar = false;
        if (!$data['avatar'] && substr($request->get('avatar'), 0, 10) === 'data:image') {
            $currentAvatar = $request->get('avatar');
        }

        $this->referrerLinkStoreCardService->saveItemsFromArray($affiliateResults);
        $this->referrerStoreNonReferableLinkService->saveItemsFromArray($yourLinks, $yourOldLinks);
        $referrerMyStore = $this->referrerMyStoreService->saveOrCreateFromArray($user, $form, $currentAvatar);

        return new JsonResponse([
            'success' => true,
            'referrerMyStore' => $this->referrerMyStoreService->objectToArray($referrerMyStore)
        ], Response::HTTP_OK);
    }

    /**
     * @param string $nickname
     * @param UserRepository $repository
     * @return Response
     */
    public function show(string $nickname, UserRepository $repository)
    {
        if (!$user = $repository->findOneBy(['nickname' => $nickname])) {
            return $this->redirectToRoute('home_index');
        }

        if ($user === $this->getUser()) {
            return $this->redirectToRoute('referrer_show_me');
        }

        $sort = (Criteria::create())->orderBy(['createdAt' => Criteria::DESC]);

        return $this->render('referrer/show-guest.html.twig', [
            'referrer' => $user,
            'nonReferableLinks' => $user->getReferrerMyStore()->getNonReferableLinks()->matching($sort),
            'myStoreReferrerLinks' => $user->getMyStoreReferrerLinks()->matching($sort),
            'disableSnippet' => true,
        ]);
    }

    /**
     * Return form errors as array.
     *
     * @param Form $form
     * @return array
     */
    protected function getFormErrorMessages(Form $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $key => $error) {
            $params = $error->getMessageParameters();
            if ($params && isset($params['{{ value }}'])) {
                $errors[] = $error->getMessage() .' ('. $params['{{ value }}'].')';
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getFormErrorMessages($child);
            }
        }

        return $errors;
    }

    private function getDestinationUrl($link) {
        return $link['destinationUrl'] ?? '';
    }

    private function sortByOrderColumn($a, $b) {
        if ($a['orderColumn'] == $b['orderColumn']) {
            return 0;
        }
        return ($a['orderColumn'] < $b['orderColumn']) ? -1 : 1;
    }
}