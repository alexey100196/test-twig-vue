<?php


namespace App\Tests\Entity;

use App\Entity\Cookie;
use App\Entity\CookieType;
use PHPUnit\Framework\TestCase;

class CookieTest extends TestCase
{

    public function testIfCanSetCookie_ValidCookieType_Affiliate()
    {
        $cookie = new Cookie();
        $cookie->setCookieType(CookieType::AFFILIATE);

        $this->assertEquals(CookieType::AFFILIATE, $cookie->getCookieType());
    }
    public function testIfCanSetCookie_ValidCookieType_Referral()
    {
        $cookie = new Cookie();
        $cookie->setCookieType(CookieType::REFERRAL);

        $this->assertEquals(CookieType::REFERRAL, $cookie->getCookieType());
    }
    public function testIfCanSetCookie_InValidCookieType()
    {
        $cookie = new Cookie();
        $cookie->setCookieType("NOT A COOKIE TYPE");

        $this->assertNotContains($cookie->getCookieType(), CookieType::Types());
    }
}