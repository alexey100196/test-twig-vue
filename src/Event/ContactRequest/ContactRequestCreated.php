<?php


namespace App\Event\ContactRequest;


use App\Entity\ContactRequest;
use Symfony\Component\EventDispatcher\Event;

class ContactRequestCreated extends Event
{
    /**
     * Event name.
     * @var string
     */
    const NAME = 'contact-request.created';

    /**
     * Registered contactRequest.
     * @var ContactRequest
     */
    protected $contactRequest;

    /**
     * @param ContactRequest $contactRequest
     */
    public function __construct(ContactRequest $contactRequest)
    {
        $this->contactRequest = $contactRequest;
    }

    /**
     * @return ContactRequest
     */
    public function getContactRequest(): ContactRequest
    {
        return $this->contactRequest;
    }
}
