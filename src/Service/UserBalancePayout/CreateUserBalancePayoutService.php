<?php


namespace App\Service\UserBalancePayout;


use App\Entity\Currency;
use App\Entity\TransactionType;
use App\Entity\User;
use App\Entity\UserBalancePayout;
use App\Repository\UserBalanceRepository;
use App\Service\Balance\UserBalanceService;
use App\Service\Exchange\ExchangeService;
use App\Service\UserBalancePayout\CreateUserBalancePayoutService\InsufficientFundsException;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;

class CreateUserBalancePayoutService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserBalanceRepository
     */
    private $userBalanceRepository;

    /**
     * @var UserBalanceService
     */
    private $userBalanceService;

    /**
     * @var float
     */
    private $minPayout;

    /**
     * @var ExchangeService
     */
    private $exchangeService;

    /**
     * @var UserPendingPayoutAmountUpdater
     */
    private $userPendingPayoutAmountUpdater;

    public function __construct(
        EntityManagerInterface $em,
        UserBalanceRepository $userBalanceRepository,
        UserBalanceService $userBalanceService,
        ExchangeService $exchangeService,
        UserPendingPayoutAmountUpdater $userPendingPayoutAmountUpdater,
        float $minPayout
    )
    {
        $this->em = $em;
        $this->userBalanceRepository = $userBalanceRepository;
        $this->userBalanceService = $userBalanceService;
        $this->exchangeService = $exchangeService;
        $this->userPendingPayoutAmountUpdater = $userPendingPayoutAmountUpdater;
        $this->minPayout = $minPayout;
    }

    /**
     * @param User $user
     * @param Currency $currency
     * @param float $amount
     * @throws \InvalidArgumentException
     */
    public function __invoke(User $user, Currency $currency, float $amount)
    {
        if (!$this->userBalanceService->hasFunds($user, $currency, $amount)) {
            throw new InsufficientFundsException($currency);
        }

        $payout = new UserBalancePayout();
        $payout->setAmount($amount);
        $payout->setBalance($user->getCurrencyBalance($currency->getName()));

        $this->validateMinimumPayout($payout);

        $this->em->persist($payout);

        $this->createTransactionForPayout($payout);

        $user = $payout->getBalance()->getUser();

        $this->em->persist($user);

        $this->em->flush();

        $this->userPendingPayoutAmountUpdater->update($user);

    }

    private function createTransactionForPayout(UserBalancePayout $payout)
    {
        /** @var TransactionType $payoutTransactionType */
        $payoutTransactionType = $this->em->getReference(TransactionType::class, TransactionType::USER_PAYOUT);

        $this->userBalanceService->addTransaction(
            Uuid::uuid4(),
            $payout->getBalance()->getUser(),
            $payout->getBalance()->getCurrency(),
            -1 * $payout->getAmount(),
            $payoutTransactionType
        );
    }

    /**
     * @param UserBalancePayout $payout
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \InvalidArgumentException
     */
    private function validateMinimumPayout(UserBalancePayout $payout)
    {
        if ($payout->getBalance()->getCurrency() === 'EUR') {
            $eurValue = $payout->getAmount();
        } else {
            $eurValue = $this->exchangeService->exchange(
                $payout->getAmount(),
                $payout->getBalance()->getCurrency()->getName(),
                'EUR'
            );
        }

        if ($eurValue < $this->minPayout) {
            throw new \InvalidArgumentException('Minimum amount €' . $this->minPayout . ' not reached');
        }
    }
}
