<?php


namespace App\Helper;


class Number
{
    /***
     * @param $number
     * @return string
     */
    public static function toString($number): string
    {
        return sprintf('%01.2f', $number);
    }
}